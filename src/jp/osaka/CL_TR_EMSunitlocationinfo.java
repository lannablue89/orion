package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_TR_EMSunitlocationinfo {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_TR_EMSunitlocationinfo(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createtr_emsunitlocationinfo(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("tr_emsunitlocationinfo", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor gettr_emsunitlocationinfo() {
		Cursor mCursor = null;
		try {

			String sql = "Select * from tr_emsunitlocationinfo";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor gettr_emsunitlocationinfo(String lastinsert_datetime) {
		Cursor mCursor = null;
		try {
			String[] args = { lastinsert_datetime };
			String sql = "Select * from tr_emsunitlocationinfo_result Where insert_datetime>?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getlastinsert_datetime() {
		String insert_datetime = "19990101010101";
		Cursor mCursor = null;
		try {
			// String [] args={lastinsert_datetime};
			String sql = "Select * from tr_emsunitlocationinfo_result order by insert_datetime DESC limit 1";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					insert_datetime = mCursor.getString(mCursor
							.getColumnIndex("insert_datetime"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return insert_datetime;
	}

	public boolean CopyTR_EMSunitlocationinfo() {
		boolean result = false;
		try {
			// Mote table result
			String sql = "Insert Into tr_emsunitlocationinfo_result(emsunitcode,gpsacquisition_datetime,latitude_is,longitude_is,accuracy,altitude,speed,incidentscene,straightlinedistance,arrivaltime,networkinfosubtype,networkinfotypename,insert_datetime) "
					+ "Select emsunitcode,gpsacquisition_datetime,latitude_is,longitude_is,accuracy,altitude,speed,incidentscene,straightlinedistance,arrivaltime,networkinfosubtype,networkinfotypename,update_datetime From tr_emsunitlocationinfo";
			database.execSQL(sql);

			// Delete old data
			result = database.delete("tr_emsunitlocationinfo", null, null) > 0;

		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			result = database.delete("tr_emsunitlocationinfo", null, null) > 0;
		} finally {
			database.close();
		}
		return result;
	}
}
