﻿package jp.osaka;

import java.util.ArrayList;
import java.util.HashMap;

import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class E011 extends Activity implements OnTouchListener {

	LinearLayout layoutchk;
	String FireStationCode;
	String CaseCode;
	boolean ex = false;
	Button btntop;
	HashMap<String, String> list;
	ArrayList<DTO_MCode> listitem;

	Dialog dialoge;
	adapterActivity adapter1;
	adapterActivity adapter2;
	Spinner cbo1;
	Spinner cbo2;
	EditText txtHopital;
	RadioButton rdi1;
	RadioButton rdi2;
	RadioButton rdi3;
	String rdoSearch = "1";
	String hopitalname;
	String SHCASearch;
	String AreaCode;
	String SHCACode;
	String KamokuName = "";
	double lat = 0;
	double longi = 0;
	boolean isruning = false;
	boolean isShow12 = false;
	Handler handler;
	LocationManager locationManager;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E011.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute011);
		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");

		// Slide menu
		utl.setMenu_open(false);
		initSileMenu();
		handler = new Handler();
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Cursor cur = null;
		Cursor cor = null;
		try {

			// createMenu();// menu
			// getConfig();
			// Testmenu
			// TextView txtHeader=(TextView)findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			listitem = new ArrayList<DTO_MCode>();
			list = new HashMap<String, String>();
			Cl_Kamoku cl1 = new Cl_Kamoku(this);
			cur = cl1.getMS_Kamoku();
			do {
				String kamokucode = cur.getString(cur
						.getColumnIndex("KamokuCode"));
				String kamokuname = cur.getString(cur
						.getColumnIndex("KamokuName"));
				DTO_MCode dt = new DTO_MCode();
				dt.setCodeId(kamokucode);
				dt.setCodeName(kamokuname);
				listitem.add(dt);

			} while (cur.moveToNext());
			Cl_Kamoku cl = new Cl_Kamoku(this);
			cor = cl.getTR_SelectionKamoku(FireStationCode, CaseCode);
			if (cor != null && cor.getCount() > 0) {
				do {
					String code = cor.getString(cor
							.getColumnIndex("KamokuCode"));
					String name = cor.getString(cor
							.getColumnIndex("KamokuName"));
					list.put(code, name);
				} while (cor.moveToNext());
			}

			layoutchk = (LinearLayout) findViewById(R.id.LayoutE11CheckBox);
			layoutchk.setOnTouchListener(this);
			this.loadcontrolCheckBox();
			btntop = (Button) findViewById(R.id.btnE11Top);
			btntop.setOnTouchListener(this);
			ex = true;
			btntop.setBackgroundResource(R.drawable.btnblueup);
			btntop.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (ex) {
						ex = false;
						btntop.setBackgroundResource(R.drawable.btnbluedown);
						layoutchk.removeAllViews();
					} else {
						ex = true;
						btntop.setBackgroundResource(R.drawable.btnblueup);
						loadcontrolCheckBox();
					}
				}
			});
			Button btnNext = (Button) findViewById(R.id.btnE11Next);
			btnNext.setOnTouchListener(this);
			btnNext.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// 20131115 Bug 46 Not select -> Search all
					// Cl_Kamoku cl = new Cl_Kamoku(E011.this);
					// Cursor cor = cl.getTR_SelectionKamoku(FireStationCode,
					// CaseCode);
					// if (cor != null && cor.getCount() > 0) {

					Cl_EMSUnit em = new Cl_EMSUnit(E011.this);
					Cursor cur = em.getSHCACode();
					if (cur != null && cur.getCount() > 0) {
						SHCACode = cur.getString(cur.getColumnIndex("SHCACode"));
					}
					cur.close();
					if (!isShow12) {
						isShow12 = true;
						showE012();
					}
					// }
					// else // Bug 97
					// {
					// // Bug 135 change Id SE0012 -> SE0015
					// E022.showErrorDialog(E011.this,
					// ContantMessages.NotCheckE11,
					// ContantMessages.NotCheckE11, false);
					// }
					// cor.close();

				}
			});
			ScrollView view = (ScrollView) findViewById(R.id.scrollView1);
			view.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E011", true);
		} finally {
			cur.close();
			cor.close();
		}
	}

	private void getKamokuName() {
		Cursor cur = null;
		try {
			KamokuName = "";
			Cl_Kamoku cl = new Cl_Kamoku(E011.this);
			cur = cl.get3TR_SelectionKamoku(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {

					String name = cur.getString(cur
							.getColumnIndex("KamokuName"));
					if (KamokuName.equals(""))
						KamokuName += name;
					else
						KamokuName += "," + name;
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E011.this);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	// Bug 187,188
	public void showDialogERR(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myinten = new Intent(E011.this, E013B.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putString("SHCACode", SHCACode);
				b.putString("rdoSearch", "4");
				b.putString("hopitalname", hopitalname);
				b.putString("SHCASearch", SHCASearch);
				b.putString("AreaCode", AreaCode);
				b.putString("E", "E012");
				b.putString("KamokuName", KamokuName);
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();

			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}

	public void showE012() {
		try {
			SHCASearch = SHCACode;
			dialoge = new Dialog(E011.this);
			dialoge.setOnKeyListener(new OnKeyListener() {

				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						isShow12 = false;
					}
					return false;
				}
			});
			dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialoge.setContentView(R.layout.layoute012);
			dialoge.setCanceledOnTouchOutside(false);// Bug 146
			// Bug 55 txtHopital
			cbo1 = (Spinner) dialoge.findViewById(R.id.cboE121);
			cbo2 = (Spinner) dialoge.findViewById(R.id.cboE122);
			txtHopital = (EditText) dialoge.findViewById(R.id.txtE121);
			//
			Button btnCacnel = (Button) dialoge.findViewById(R.id.btnE12Cancel);
			btnCacnel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialoge.cancel();
					isruning = false;
					isShow12 = false;
				}
			});
			Button btnOk = (Button) dialoge.findViewById(R.id.btnE12Ok);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					isruning = false;
					isShow12 = false;
					getKamokuName();
					if (rdoSearch.equals("1")) {

						// Criteria cri = new Criteria();
						// String tower = locationManager.getBestProvider(cri,
						// false);
						// Location location = locationManager
						// .getLastKnownLocation(tower);
						Location location = null;
						CL_TRCase cl = new CL_TRCase(E011.this);
						Cursor cur = cl
								.getItemTRcase(FireStationCode, CaseCode);
						if (cur != null && cur.getCount() > 0) {
							String lat = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Latitude_IS));
							String longi = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Longitude_IS));
							if (lat != null && longi != null) {
								location = new Location("A");
								location.setLatitude(Double.parseDouble(lat));
								location.setLongitude(Double.parseDouble(longi));
							}
						}
						if (cur != null)
							cur.close();
						if (location != null) {
							// lat = (double) (location.getLatitude() * 1E6);
							// longi = (double) (location.getLongitude() * 1E6);
							lat = location.getLatitude();
							longi = location.getLongitude();
							Intent myinten = new Intent(E011.this, E013B.class);
							Bundle b = new Bundle();
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putString("SHCACode", SHCACode);
							b.putString("rdoSearch", rdoSearch);
							b.putString("hopitalname", hopitalname);
							b.putString("SHCASearch", SHCASearch);
							b.putString("AreaCode", AreaCode);
							b.putString("E", "E012");
							b.putDouble("Lat", lat);
							b.putDouble("Longi", longi);
							b.putString("KamokuName", KamokuName);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
							dialoge.cancel();

						} else {
							// Bug 187,188
							// E022.showErrorDialog(E011.this,
							// ContantMessages.GPSDisiable,
							// ContantMessages.GPSDisiable, false);
							// isruning = true;
							// startThread();
							showDialogERR(ContantMessages.GPSDisiable);
							dialoge.cancel();
							//
						}

					} else {
						EditText txtHN = (EditText) dialoge
								.findViewById(R.id.txtE121);
						hopitalname = txtHN.getText().toString();
						Intent myinten = new Intent(E011.this, E013B.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString("SHCACode", SHCACode);
						b.putString("rdoSearch", rdoSearch);
						b.putString("hopitalname", hopitalname);
						b.putString("SHCASearch", SHCASearch);
						b.putString("AreaCode", AreaCode);
						b.putString("E", "E012");
						b.putString("KamokuName", KamokuName);
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						dialoge.cancel();
					}
				}
			});
			rdi1 = (RadioButton) dialoge.findViewById(R.id.rdioE121);
			rdi1.setButtonDrawable(R.drawable.radio);
			rdi1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "1";
						rdi1.setTextColor(Color.WHITE);
						rdi2.setTextColor(Color.parseColor("#777777"));
						rdi3.setTextColor(Color.parseColor("#777777"));
						rdi2.setChecked(false);
						rdi3.setChecked(false);
						// Bug 55
						txtHopital.setEnabled(false);
						cbo1.setEnabled(false);
						cbo2.setEnabled(false);
					}
				}
			});
			rdi2 = (RadioButton) dialoge.findViewById(R.id.rdioE122);
			rdi2.setButtonDrawable(R.drawable.radio);
			rdi2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "2";
						rdi2.setTextColor(Color.WHITE);
						rdi1.setTextColor(Color.parseColor("#777777"));
						rdi3.setTextColor(Color.parseColor("#777777"));
						rdi1.setChecked(false);
						rdi3.setChecked(false);
						// Bug 55
						txtHopital.setEnabled(true);
						cbo1.setEnabled(false);
						cbo2.setEnabled(false);
					}
				}
			});
			rdi3 = (RadioButton) dialoge.findViewById(R.id.rdioE123);
			rdi3.setButtonDrawable(R.drawable.radio);
			rdi3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "3";
						rdi3.setTextColor(Color.WHITE);
						rdi1.setTextColor(Color.parseColor("#777777"));
						rdi2.setTextColor(Color.parseColor("#777777"));
						rdi1.setChecked(false);
						rdi2.setChecked(false);

						// Bug 55
						txtHopital.setEnabled(false);
						cbo1.setEnabled(true);
						cbo2.setEnabled(true);

					}
				}
			});
			adapter1 = new adapterActivity(dialoge.getContext(),
					R.layout.layoutcomboxe12);
			adapter2 = new adapterActivity(dialoge.getContext(),
					R.layout.layoutcomboxe12);
			// cbo1 = (Spinner) dialoge.findViewById(R.id.cboE121);
			// cbo2 = (Spinner) dialoge.findViewById(R.id.cboE122);
			cbo1.setAdapter(adapter1);
			cbo1.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapter1.getItem(arg2);
					loadCombobox2(dt.getCodeId());
					SHCASearch = dt.getCodeId();

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			cbo2.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapter2.getItem(arg2);
					AreaCode = dt.getCodeId();
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			cbo2.setAdapter(adapter2);
			loadCombobox1();
			loadCombobox2(SHCACode);
			// Bug 55
			txtHopital.setEnabled(false);
			cbo1.setEnabled(false);
			cbo2.setEnabled(false);
			rdi1.setChecked(true);
			//
			dialoge.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E011.this, e.getMessage(), "E012", true);
		} finally {
		}
	}

	// Start thread check GPS
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			while (isruning) {
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						if (isruning)
							checkGPS();
					}
				});

			}
		}
	};

	void checkGPS() {

		Criteria cri = new Criteria();
		String tower = locationManager.getBestProvider(cri, false);
		Location location = locationManager.getLastKnownLocation(tower);

		if (location != null) {
			// lat = (double) (location.getLatitude() * 1E6);
			// longi = (double) (location.getLongitude() * 1E6);
			lat = location.getLatitude();
			longi = location.getLongitude();
			Intent myinten = new Intent(E011.this, E013B.class);
			Bundle b = new Bundle();
			b.putString("FireStationCode", FireStationCode);
			b.putString("CaseCode", CaseCode);
			b.putString("SHCACode", SHCACode);
			b.putString("rdoSearch", rdoSearch);
			b.putString("hopitalname", hopitalname);
			b.putString("SHCASearch", SHCASearch);
			b.putString("AreaCode", AreaCode);
			b.putString("E", "E012");
			b.putDouble("Lat", lat);
			b.putDouble("Longi", longi);
			myinten.putExtras(b);
			startActivityForResult(myinten, 0);
			dialoge.cancel();
			isruning = false;
		}

	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private void loadCombobox1() {
		Cursor cur = null;
		try {
			Cl_Code cl = new Cl_Code(E011.this);
			cur = cl.getMS_SecondaryHealthCareArea();
			if (cur != null && cur.getCount() > 0) {
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("SHCACode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("SHCAName")));
					adapter1.add(code);
				} while (cur.moveToNext());
				for (int i = 0; i < adapter1.getCount(); i++) {
					if (adapter1.getItem(i).getCodeId().equals(SHCACode)) {
						cbo1.setSelection(i);
						break;
					}
				}
			}
			Cl_Code cl2 = new Cl_Code(E011.this);
			cur = cl2.getMS_Area(SHCACode);
			if (cur != null && cur.getCount() > 0) {
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("AreaCode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("AreaName")));
					adapter2.add(code);
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E011.this, e.getMessage(), "E012", true);
		} finally {
			cur.close();
		}
	}

	private void loadCombobox2(String shcacode) {
		Cursor cur = null;
		try {
			if (adapter2 != null)
				adapter2.clear();
			Cl_Code cl2 = new Cl_Code(E011.this);
			cur = cl2.getMS_Area(shcacode);
			if (cur != null && cur.getCount() > 0) {
				DTO_MCode code1 = new DTO_MCode();
				code1.setCodeId("");
				code1.setCodeName("");
				adapter2.add(code1);
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("AreaCode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("AreaName")));
					adapter2.add(code);
				} while (cur.moveToNext());
				// 20121008 Bug 53
				cbo2.setSelection(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E011.this, e.getMessage(), "E012", true);
		} finally {
			cur.close();
		}
	}

	public class adapterActivity extends ArrayAdapter<DTO_MCode> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, true);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraper mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutcomboxe12, null);
				mwp = new ViewWraper(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraper) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = mwp.getLable();
			DTO_MCode dt = new DTO_MCode();
			dt = this.getItem(position);
			txt.setText("   " + dt.getCodeName());
			LinearLayout layout = mwp.getLayoutrow();
			if (drop) {
				layout.setBackgroundResource(R.drawable.bgrowcboboxe12);
			} else {
				layout.setBackgroundResource(R.drawable.dropdowne12);
			}
			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		LinearLayout layout = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtcboE12);
			}
			return lable1;
		}

		LinearLayout getLayoutrow() {
			if (layout == null) {
				layout = (LinearLayout) base.findViewById(R.id.lrCombox);
			}
			return layout;
		}
	}

	// Load list checkbox
	private void loadcontrolCheckBox() {

		try {

			if (listitem.size() > 0) {

				for (int i = 0; i < listitem.size(); i++) {
					DTO_MCode dt = new DTO_MCode();
					dt = listitem.get(i);
					final String codeid = dt.getCodeId();
					final String kamokuname = dt.getCodeName();
					// 20121005 daitran modify : B-011
					// LinearLayout.LayoutParams paramChh = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.MATCH_PARENT, 40);
					// paramChh.setMargins(15, 15, 0, 15);
					// Bug 86
					// LinearLayout.LayoutParams paramChh = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.MATCH_PARENT,
					// LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramChh.setMargins(15, 20, 0, 0);
					// end

					CheckBox chk = new CheckBox(E011.this);
					chk.setLayoutParams(paramChh);
					chk.setGravity(Gravity.CENTER | Gravity.LEFT);
					chk.setTextColor(Color.BLACK);
					// chk.setId(Id);
					chk.setText(kamokuname);
					chk.setTextSize(18);
					chk.setButtonDrawable(this.getResources().getDrawable(
							R.drawable.checkbox));
					if (list.containsKey(codeid)) {
						chk.setChecked(true);
					}
					layoutchk.addView(chk);
					chk.setOnTouchListener(this);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							Cl_Kamoku ck = new Cl_Kamoku(E011.this);
							if (isChecked) {
								ck.createTR_SelectionKamoku(FireStationCode,
										CaseCode, codeid, kamokuname);
								if (!list.containsKey(codeid)) {
									list.put(codeid, kamokuname);
								}
							} else {
								ck.deleteTR_SelectionKamoku(FireStationCode,
										CaseCode, codeid);
								if (list.containsKey(codeid)) {
									list.remove(codeid);
								}
							}
						}
					});
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E011.this, e.getMessage(), "E011", true);
		} finally {

		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
