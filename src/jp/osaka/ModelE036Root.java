package jp.osaka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModelE036Root {

	private String title;
	private List<Object> data;
	private boolean isExpan;
	private boolean isEnable;
	int tag;
	private boolean background;
	
	
	
	public boolean isBackground() {
		return background;
	}

	public void setBackground(boolean background) {
		this.background = background;
	}

	public ModelE036Root(String title,int tag) {
		super();
		this.title = title;
		data=new ArrayList<Object>();
		this.tag=tag;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public void add(Object model) {
		data.add(model);
	}

	public boolean isExpan() {
		return isExpan;
	}

	public void setExpan(boolean isExpan) {
		this.isExpan = isExpan;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}
	
	

}
