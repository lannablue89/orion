﻿package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_Message {

	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_Message(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}	

	public String getMessage(String MessageID) {
		String message="";
		Cursor mCursor = null;
		try {

			String[] args = { MessageID };
			String sql = "Select * from MS_Message Where MessageID=? and DeleteFLG=0";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null&&mCursor.getCount()>0)
			{
				mCursor.moveToFirst();
				message=mCursor.getString(mCursor.getColumnIndex("MessageText"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return message;
	}
}
