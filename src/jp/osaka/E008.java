﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class E008 extends Activity implements OnClickListener, OnTouchListener {

	String FireStationCode;
	String CaseCode;
	int id = 1;
	boolean showCheckbox = false;
	boolean showcombo = false;

	adapterActivity adapterjsc;
	adapterActivity adaptergsce;
	adapterActivity adaptergscv;
	adapterActivity adaptergscm;

	EditText txt1;
	EditText txt2;
	EditText txt3;
	EditText txt4;
	EditText txt5;
	Spinner cbojsc;
	Spinner cbogsce;
	Spinner cbogscv;
	Spinner cbogscm;
	Button btnShowCheckbox;
	Button btnShowCombo;
	RadioButton rdioFemale;
	RadioButton rdioMan;
	SlideMenu utl = new SlideMenu();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX = 0, marginX_temp;
	// end menu
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		v.requestFocusFromTouch();
		@SuppressWarnings("static-access")
		InputMethodManager inputManager = (InputMethodManager) getSystemService(E008.this.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(E008.this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			// Sile menu
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E008.this,
					progressBar, pro_2, FireStationCode, CaseCode);

		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute008);
		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		boolean isShow15 = b.getBoolean("isShow15");

		// Sile menu
		utl.setMenu_open(false);
		initSileMenu();
		try {
			Button btn1 = (Button) findViewById(R.id.button1);
			btn1.setOnTouchListener(this);

			// createMenu();// menu
			// getConfig();
			// Testmenu
			// TextView txtHeader=(TextView)findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });

			btnShowCheckbox = (Button) findViewById(R.id.btnE8ShowCheckBox);
			btnShowCheckbox.setOnTouchListener(this);
			btnShowCheckbox.setOnClickListener(this);
			btnShowCombo = (Button) findViewById(R.id.btnE8ShowCombo);
			btnShowCombo.setOnTouchListener(this);
			btnShowCombo.setOnClickListener(this);
			Button btnE8ToE91 = (Button) findViewById(R.id.btnE8NextE91);
			btnE8ToE91.setOnClickListener(this);
			btnE8ToE91.setOnTouchListener(this);
			Button btnE8ToE92 = (Button) findViewById(R.id.btnE8NextE92);
			btnE8ToE92.setOnClickListener(this);
			btnE8ToE92.setOnTouchListener(this);
			Button btnE8ShowE11 = (Button) findViewById(R.id.btnE8ShowE11);
			btnE8ShowE11.setOnClickListener(this);
			btnE8ShowE11.setOnTouchListener(this);
			Button btnE8ShowE13 = (Button) findViewById(R.id.btnE8ShowE13);
			btnE8ShowE13.setOnClickListener(this);
			btnE8ShowE13.setOnTouchListener(this);
			Button btnE8ShowE31 = (Button) findViewById(R.id.btnE8ShowE31);
			btnE8ShowE31.setOnClickListener(this);
			btnE8ShowE31.setOnTouchListener(this);
			SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
			int mode = sh.getInt("Mode", 0);
			if (mode != 0) {
				btnE8ToE91.setText("START法 トリアージ評価");
				btnE8ToE92.setText("PAT法 トリアージ評価");
			} else {
				btnE8ToE91.setText("疾病　初期評価");
				btnE8ToE92.setText("外傷　初期評価");
			}

			// Had select Hopital
			if (isShow15) {
				btnE8ToE91.setEnabled(false);
				btnE8ToE92.setEnabled(false);
				btnE8ShowE11.setEnabled(false);
				btnE8ShowE13.setEnabled(false);
				btnE8ShowE31.setEnabled(false);
				btnE8ToE91
						.setBackgroundResource(R.drawable.btngreennextdisable);
				btnE8ToE92
						.setBackgroundResource(R.drawable.btngreennextdisable);
				btnE8ShowE11
						.setBackgroundResource(R.drawable.btnbluenextdisable);
				btnE8ShowE31
						.setBackgroundResource(R.drawable.btnbluenextdisable);
				btnE8ShowE13
						.setBackgroundResource(R.drawable.btnrednextdisable);
				btnE8ToE91.setTextColor(Color.parseColor("#DDD9C3"));
				btnE8ToE92.setTextColor(Color.parseColor("#DDD9C3"));
				btnE8ShowE11.setTextColor(Color.parseColor("#DDD9C3"));
				btnE8ShowE31.setTextColor(Color.parseColor("#DDD9C3"));
				btnE8ShowE13.setTextColor(Color.parseColor("#DDD9C3"));
			} else {
				btnE8ToE91.setEnabled(true);
				btnE8ToE92.setEnabled(true);
				btnE8ShowE11.setEnabled(true);
				btnE8ShowE13.setEnabled(true);
				btnE8ShowE31.setEnabled(true);
				btnE8ToE91.setBackgroundResource(R.drawable.btngreennext);
				btnE8ToE92.setBackgroundResource(R.drawable.btngreennext);
				btnE8ShowE11.setBackgroundResource(R.drawable.btnbluenext);
				btnE8ShowE31.setBackgroundResource(R.drawable.btnbluenext);
				btnE8ShowE13.setBackgroundResource(R.drawable.btnrednext);
				btnE8ToE91.setTextColor(Color.parseColor("#ffffff"));
				btnE8ToE92.setTextColor(Color.parseColor("#ffffff"));
				btnE8ShowE11.setTextColor(Color.parseColor("#ffffff"));
				btnE8ShowE31.setTextColor(Color.parseColor("#ffffff"));
				btnE8ShowE13.setTextColor(Color.parseColor("#ffffff"));
			}

			rdioMan = (RadioButton) findViewById(R.id.rdioMan);
			rdioMan.setOnTouchListener(this);
			rdioMan.setButtonDrawable(this.getResources().getDrawable(
					R.drawable.radiospace));
			rdioMan.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					updateSex(BaseCount.Man);
					rdioMan.setBackgroundResource(R.drawable.rdiogreen);
					rdioFemale.setBackgroundResource(R.drawable.rdiowhite);
					rdioMan.setTextColor(Color.WHITE);
					rdioFemale.setTextColor(Color.BLACK);
				}
			});

			rdioFemale = (RadioButton) findViewById(R.id.rdioFemale);
			rdioFemale.setOnTouchListener(this);
			rdioFemale.setButtonDrawable(this.getResources().getDrawable(
					R.drawable.radiospace));
			rdioFemale.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					updateSex(BaseCount.Nu);
					rdioFemale.setBackgroundResource(R.drawable.rdiogreen);
					rdioMan.setBackgroundResource(R.drawable.rdiowhite);
					rdioFemale.setTextColor(Color.WHITE);
					rdioMan.setTextColor(Color.BLACK);
				}
			});
			final EditText txtAge = (EditText) findViewById(R.id.txtE8Age);
			txtAge.setOnTouchListener(this);
			txtAge.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String age = txtAge.getText().toString();
					if (!age.equals("")) {
						if (checkAge(age)) {
							age = String.valueOf(Integer.parseInt(age));
							CL_TRCase tr = new CL_TRCase(E008.this);
							tr.updateColumsTRCase(FireStationCode, CaseCode,
									TR_CaseCount.Age, age);
						} else {
							E022.showErrorDialog(E008.this,
									ContantMessages.AgeIsInValid,
									ContantMessages.AgeIsInValid, false);
						}
					} else {
						CL_TRCase tr = new CL_TRCase(E008.this);
						tr.updateColumsTRCase(FireStationCode, CaseCode,
								TR_CaseCount.Age, null);
					}
				}
			});
			// exit keyboard when press Next
			// txtAge.setOnKeyListener(new OnKeyListener() {
			//
			// public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
			// // TODO Auto-generated method stub
			// if (arg2 != null&& (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER))
			// {
			// @SuppressWarnings("static-access")
			// InputMethodManager inputManager = (InputMethodManager)
			// getSystemService(E008.this.INPUT_METHOD_SERVICE);
			// inputManager.hideSoftInputFromWindow(E008.this
			// .getCurrentFocus().getWindowToken(),
			// InputMethodManager.HIDE_NOT_ALWAYS);
			//
			// // Must return true here to consume event
			// return true;
			//
			// }
			// return false;
			// }
			// });

			txt1 = (EditText) findViewById(R.id.txtPulse);
			txt1.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String value = txt1.getText().toString();
					if (value != null && !value.equals("")
							&& !checkInput(value)) {
						E022.showErrorDialog(E008.this,
								ContantMessages.ValueNotValid,
								ContantMessages.ValueNotValid, false);
						return;
					}
					CL_TRCase tr = new CL_TRCase(E008.this);
					tr.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.VitalSign_Pulse, txt1.getText()
									.toString());

				}
			});

			txt2 = (EditText) findViewById(R.id.txtBreath);
			txt2.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String value = txt2.getText().toString();
					if (value != null && !value.equals("")
							&& !checkInput(value)) {
						E022.showErrorDialog(E008.this,
								ContantMessages.ValueNotValid,
								ContantMessages.ValueNotValid, false);
						return;
					}
					CL_TRCase tr = new CL_TRCase(E008.this);
					tr.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.VitalSign_Breath, txt2.getText()
									.toString());

				}
			});

			txt3 = (EditText) findViewById(R.id.txtBloodPressure);
			txt3.setOnTouchListener(this);
			txt3.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String value = txt3.getText().toString();
					if (value != null && !value.equals("")
							&& !checkInput(value)) {
						E022.showErrorDialog(E008.this,
								ContantMessages.ValueNotValid,
								ContantMessages.ValueNotValid, false);
						return;
					}
					CL_TRCase tr = new CL_TRCase(E008.this);
					tr.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.VitalSign_BloodPressure, txt3
									.getText().toString());

				}
			});

			txt4 = (EditText) findViewById(R.id.txtSpO2);
			txt4.setOnTouchListener(this);
			txt4.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String value = txt4.getText().toString();
					if (value != null && !value.equals("")
							&& !checkInput(value)) {
						E022.showErrorDialog(E008.this,
								ContantMessages.ValueNotValid,
								ContantMessages.ValueNotValid, false);
						return;
					}
					CL_TRCase cl = new CL_TRCase(E008.this);
					cl.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.VitalSign_SpO2, txt4.getText()
									.toString());

				}
			});

			txt5 = (EditText) findViewById(R.id.txtTemperature);
			txt5.setOnTouchListener(this);
			txt5.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					String value = txt5.getText().toString();
					if (value != null && !value.equals("")
							&& !checkInput(value)) {
						E022.showErrorDialog(E008.this,
								ContantMessages.ValueNotValid,
								ContantMessages.ValueNotValid, false);
						return;
					}
					CL_TRCase cl = new CL_TRCase(E008.this);
					cl.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.VitalSign_Temperature, txt5.getText()
									.toString());

				}
			});

			CL_TRCase tr = new CL_TRCase(E008.this);
			Cursor ctr = tr.getItemTRcase(FireStationCode, CaseCode);
			if (ctr != null && ctr.getCount() > 0) {
				String sex = ctr
						.getString(ctr.getColumnIndex(TR_CaseCount.Sex));
				if (sex != null) {
					if (sex != null && sex.equals(BaseCount.Nu)) {
						rdioFemale.setChecked(true);
						rdioMan.setChecked(false);
						rdioFemale.setBackgroundResource(R.drawable.rdiogreen);
						rdioMan.setBackgroundResource(R.drawable.rdiowhite);
						rdioFemale.setTextColor(Color.WHITE);
						rdioMan.setTextColor(Color.BLACK);
					} else {
						rdioFemale.setChecked(false);
						rdioMan.setChecked(true);
						rdioMan.setBackgroundResource(R.drawable.rdiogreen);
						rdioFemale.setBackgroundResource(R.drawable.rdiowhite);
						rdioMan.setTextColor(Color.WHITE);
						rdioFemale.setTextColor(Color.BLACK);
					}
				}
				String age = ctr
						.getString(ctr.getColumnIndex(TR_CaseCount.Age));
				txtAge.setText(age);
			}
			ctr.close();

			ScrollView view = (ScrollView) findViewById(R.id.scrollViewe8);
			view.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
			view.setFocusable(true);
			view.setFocusableInTouchMode(true);
			view.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E008", true);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	private boolean checkAge(String age) {
		try {
			Integer.parseInt(age);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	private boolean checkInput(String input) {
		try {
			Double.parseDouble(input);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	private void updateSex(String sex) {
		CL_TRCase tr = new CL_TRCase(E008.this);
		tr.updateColumsTRCase(FireStationCode, CaseCode, TR_CaseCount.Sex, sex);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnE8ShowCheckBox:
			if (!showCheckbox) {
				showCheckbox = true;
				showCheckBox();
				btnShowCheckbox.setBackgroundResource(R.drawable.btngreyup);

			} else {
				showCheckbox = false;
				hideCheckBox();
				btnShowCheckbox.setBackgroundResource(R.drawable.btngreydown);
			}
			break;
		case R.id.btnE8ShowCombo:
			if (!showcombo) {
				showcombo = true;
				// showCombobox(750);
				showCombobox(1020);
				this.loadComboJSC();
				this.loadComboGSC();
				setOldValue();
				// 20121008 :Bug 092
				cbojsc.setVisibility(View.VISIBLE);
				cbogsce.setVisibility(View.VISIBLE);
				cbogscm.setVisibility(View.VISIBLE);
				cbogscv.setVisibility(View.VISIBLE);
				txt1.setVisibility(View.VISIBLE);
				txt2.setVisibility(View.VISIBLE);
				txt3.setVisibility(View.VISIBLE);
				txt4.setVisibility(View.VISIBLE);
				txt5.setVisibility(View.VISIBLE);
				//
				btnShowCombo.setBackgroundResource(R.drawable.btngreyup);

			} else {
				showcombo = false;
				// hideCombobox(88);
				hideCombobox(108);
				// 20121008 :Bug 092
				cbojsc.setVisibility(View.INVISIBLE);
				cbogsce.setVisibility(View.INVISIBLE);
				cbogscm.setVisibility(View.INVISIBLE);
				cbogscv.setVisibility(View.INVISIBLE);
				txt1.setVisibility(View.INVISIBLE);
				txt2.setVisibility(View.INVISIBLE);
				txt3.setVisibility(View.INVISIBLE);
				txt4.setVisibility(View.INVISIBLE);
				txt5.setVisibility(View.INVISIBLE);
				//
				// hideCheckBox();
				btnShowCombo.setBackgroundResource(R.drawable.btngreydown);
			}
			break;
		case R.id.btnE8NextE91:
//			Intent myinten = new Intent(v.getContext(), E009.class);
//			Bundle b = new Bundle();
//			b.putString("FireStationCode", FireStationCode);
//			b.putString("CaseCode", CaseCode);
//			b.putString(BaseCount.Flag, "1");
//			myinten.putExtras(b);
//			startActivityForResult(myinten, 0);
			
			//Phu: goto E036
			Intent myinten = new Intent(v.getContext(), E036.class);
			Bundle b = new Bundle();
			b.putString("FireStationCode", FireStationCode);
			b.putString("CaseCode", CaseCode);
			b.putString("kin_no", "100");
			b.putString("kin_branch_no","1");
			b.putString(BaseCount.Flag, "1");
			myinten.putExtras(b);
			startActivityForResult(myinten, 0);
			break;
		case R.id.btnE8NextE92:
			Intent myinten2 = new Intent(v.getContext(), E009.class);
			Bundle b2 = new Bundle();
			b2.putString("FireStationCode", FireStationCode);
			b2.putString("CaseCode", CaseCode);
			b2.putString(BaseCount.Flag, "2");
			myinten2.putExtras(b2);
			startActivityForResult(myinten2, 0);
			break;
		case R.id.btnE8ShowE11:
			Intent myinten3 = new Intent(v.getContext(), E011.class);
			Bundle b3 = new Bundle();
			b3.putString("FireStationCode", FireStationCode);
			b3.putString("CaseCode", CaseCode);
			myinten3.putExtras(b3);
			startActivityForResult(myinten3, 0);
			break;
		case R.id.btnE8ShowE13:

			// Bug 166 20121031
			Location location = null;
			CL_TRCase clr = new CL_TRCase(E008.this);
			Cursor cur = clr.getItemTRcase(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				String lat = cur.getString(cur
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi = cur.getString(cur
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat != null && longi != null) {
					location = new Location("A");
					location.setLatitude(Double.parseDouble(lat));
					location.setLongitude(Double.parseDouble(longi));
				}
			}
			if (cur != null)
				cur.close();
			if (location == null) {

				showDialogERR(ContantMessages.GPSDisiable);

			} else {
				//
				Intent myinten4 = new Intent(v.getContext(), E013C.class);
				Bundle b4 = new Bundle();
				b4.putString("FireStationCode", FireStationCode);
				b4.putString("CaseCode", CaseCode);
				// b4.putString("E", "E008");
				myinten4.putExtras(b4);
				startActivityForResult(myinten4, 0);
			}
			break;
		case R.id.btnE8ShowE31:
			showE031();
			break;
		default:
			break;
		}
	}

	public void showDialogERR(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myinten4 = new Intent(v.getContext(), E013C.class);
				Bundle b4 = new Bundle();
				b4.putString("FireStationCode", FireStationCode);
				b4.putString("CaseCode", CaseCode);
				// b4.putString("E", "E008");
				myinten4.putExtras(b4);
				startActivityForResult(myinten4, 0);
				dialog.cancel();
			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}

	private void setOldValue() {
		try {
			CL_TRCase tr = new CL_TRCase(E008.this);
			Cursor ctr = tr.getItemTRcase(FireStationCode, CaseCode);
			if (ctr != null && ctr.getCount() > 0) {
				String jcs = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_JCS));
				String gsce = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_GCS_E));
				String gscv = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_GCS_V));
				String gscm = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_GCS_M));
				String Pulse = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_Pulse));
				String Breath = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_Breath));
				String BloodPressure = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_BloodPressure));
				String SpO2 = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_SpO2));
				String Temperature = ctr.getString(ctr
						.getColumnIndex(TR_CaseCount.VitalSign_Temperature));
				if (jcs != null) {

					for (int i = 0; i < adapterjsc.getCount(); i++) {
						if (adapterjsc.getItem(i).getCodeId().equals(jcs)) {
							cbojsc.setSelection(i);
							break;
						}
					}
				}
				if (gsce != null) {

					for (int i = 0; i < adaptergsce.getCount(); i++) {
						if (adaptergsce.getItem(i).getCodeId().equals(gsce)) {
							cbogsce.setSelection(i);
							break;
						}
					}
					for (int i = 0; i < adaptergscv.getCount(); i++) {
						if (adaptergscv.getItem(i).getCodeId().equals(gscv)) {
							cbogscv.setSelection(i);
							break;
						}
					}
					for (int i = 0; i < adaptergscm.getCount(); i++) {
						if (adaptergscm.getItem(i).getCodeId().equals(gscm)) {
							cbogscm.setSelection(i);
							break;
						}
					}
				}
				if (Pulse != null) {

					txt1.setText(Pulse);
				}
				if (Breath != null) {

					txt2.setText(Breath);
				}
				if (BloodPressure != null) {

					txt3.setText(BloodPressure);
				}
				if (SpO2 != null) {

					txt4.setText(SpO2);
				}
				if (Temperature != null) {

					txt5.setText(Temperature);
				}

			}
			ctr.close();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E008.this, e.getMessage(), "E008", true);
		}
	}

	private void hideCheckBox() {

		LinearLayout layoutChk = (LinearLayout) findViewById(R.id.layoutCheckBox);
		LinearLayout layoutOther = (LinearLayout) findViewById(R.id.layoutE8Other);
		layoutChk.removeAllViews();
		layoutOther.removeAllViews();
	}

	private void showCheckBox() {
		Cl_Code code = new Cl_Code(E008.this);
		Cursor cor = code.getMS_Code("00005");
		try {

			if (cor != null && cor.getCount() > 0) {
				Cl_TRPatient pa = new Cl_TRPatient(E008.this);
				Cursor cor2 = pa.getTR_Patient(FireStationCode, CaseCode);
				HashMap<String, String> listChecked = new HashMap<String, String>();
				if (cor2 != null && cor2.getCount() > 0) {
					do {
						String codeId = cor2.getString(cor2
								.getColumnIndex("PatientBackground_CD"));
						String text = cor2.getString(cor2
								.getColumnIndex("PatientBackground_TEXT"));
						listChecked.put(codeId, text);
					} while (cor2.moveToNext());
				}

				LinearLayout layoutChk = (LinearLayout) findViewById(R.id.layoutCheckBox);
				layoutChk.setOnTouchListener(this);
				LinearLayout layoutOther = (LinearLayout) findViewById(R.id.layoutE8Other);
				layoutOther.setOnTouchListener(this);
				layoutOther.setOrientation(LinearLayout.HORIZONTAL);

				final HashMap<String, String> listItem = new HashMap<String, String>();

				do {

					String codeId = cor.getString(cor.getColumnIndex("CodeID"));
					String codeName = cor.getString(cor
							.getColumnIndex("CodeName"));

					LinearLayout.LayoutParams paraml = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout l = new LinearLayout(this);
					paraml.setMargins(20, 20, 0, 0);
					l.setOrientation(LinearLayout.HORIZONTAL);
					l.setLayoutParams(paraml);
					LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramChh.setMargins(20, 0, 0, 0);
					final CheckBox chk = new CheckBox(this);
					chk.setLayoutParams(paramChh);
					chk.setTextColor(Color.BLACK);
					chk.setId(id);
					chk.setText(codeName);
					chk.setButtonDrawable(R.drawable.checkbox);
					// Bug 86
					// chk.setWidth(300);
					chk.setWidth(250);
					//
					if (listChecked.containsKey(codeId)) {
						chk.setChecked(true);
					}
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							Cl_TRPatient pa = new Cl_TRPatient(E008.this);
							if (chk.isChecked()) {
								String codeselect = listItem.get(String
										.valueOf(chk.getId()));
								pa.createTR_Patient(FireStationCode, CaseCode,
										codeselect, "");
								// 20121008 : bug 078
								if (codeselect.equals(BaseCount.Other)) {
									EditText txtNote = (EditText) findViewById(99999);
									txtNote.setEnabled(true);
								}
								//
							} else {
								String codeselect = listItem.get(String
										.valueOf(chk.getId()));
								pa.deleteTR_Patient(FireStationCode, CaseCode,
										codeselect);
								if (codeselect.equals(BaseCount.Other)) {
									EditText txtNote = (EditText) findViewById(99999);
									txtNote.setText("");
									// 20121008 : bug 078
									txtNote.setEnabled(false);
								}
							}
						}
					});
					if (codeId.equals(BaseCount.Other)) {

						// LinearLayout.LayoutParams paramtext = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT, 60);
						LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, 80);
						final EditText txtOther = new EditText(this);
						txtOther.setLayoutParams(paramtext);
						txtOther.setBackgroundResource(R.drawable.txtother08);
						txtOther.setTextColor(Color.BLACK);
						txtOther.setId(99999);
						txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
								200) });
						txtOther.setSingleLine(true);// Bug 115
						if (listChecked.containsKey(codeId)) {
							String text = listChecked.get(codeId);
							txtOther.setText(text);
							// 20121008 : bug 078
							txtOther.setEnabled(true);
						} else {
							// 20121008 : bug 078
							txtOther.setEnabled(false);
						}
						txtOther.setTextSize(14);
						// 20121005 dai modify: Bug 010
						// txtOther.setGravity(Gravity.BOTTOM | Gravity.LEFT);
						txtOther.setGravity(Gravity.CENTER | Gravity.LEFT);
						txtOther.setPadding(1, 0, 0, 0);
						// end
						txtOther.addTextChangedListener(new TextWatcher() {

							public void onTextChanged(CharSequence s,
									int start, int before, int count) {
								// TODO Auto-generated method stub

							}

							public void beforeTextChanged(CharSequence s,
									int start, int count, int after) {
								// TODO Auto-generated method stub

							}

							public void afterTextChanged(Editable s) {
								// TODO Auto-generated method stub
								String text = txtOther.getText().toString();
								Cl_TRPatient pa = new Cl_TRPatient(E008.this);
								pa.updateTR_Patient(FireStationCode, CaseCode,
										BaseCount.Other, text);
							}
						});
						chk.setWidth(180);
						layoutOther.setGravity(Gravity.CENTER | Gravity.LEFT);
						layoutOther.addView(chk);
						layoutOther.addView(txtOther);
					} else {
						l.addView(chk);
					}
					listItem.put(String.valueOf(id), codeId);
					id++;
					if (cor.moveToNext()) {
						codeId = cor.getString(cor.getColumnIndex("CodeID"));
						codeName = cor
								.getString(cor.getColumnIndex("CodeName"));
						LinearLayout.LayoutParams paramChh2 = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						// 86
						// paramChh2.setMargins(20, 0, 0, 0);
						paramChh2.setMargins(70, 0, 0, 0);

						final CheckBox chk2 = new CheckBox(this);
						chk2.setLayoutParams(paramChh2);
						chk2.setTextColor(Color.BLACK);
						chk2.setId(id);
						chk2.setText(codeName);
						chk2.setButtonDrawable(R.drawable.checkbox);
						if (listChecked.containsKey(codeId)) {
							chk2.setChecked(true);
						}
						chk2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							public void onCheckedChanged(
									CompoundButton buttonView, boolean isChecked) {
								// TODO Auto-generated method stub
								Cl_TRPatient cl = new Cl_TRPatient(E008.this);
								if (chk2.isChecked()) {
									String codeselect = listItem.get(String
											.valueOf(chk2.getId()));
									cl.createTR_Patient(FireStationCode,
											CaseCode, codeselect, "");
									// 20121008 : bug 078
									if (codeselect.equals(BaseCount.Other)) {
										EditText txtNote = (EditText) findViewById(99999);
										txtNote.setEnabled(true);
									}
									//
								} else {
									String codeselect = listItem.get(String
											.valueOf(chk2.getId()));
									cl.deleteTR_Patient(FireStationCode,
											CaseCode, codeselect);
									if (codeselect.equals(BaseCount.Other)) {
										EditText txtNote = (EditText) findViewById(99999);
										txtNote.setText("");
										txtNote.setEnabled(false); // 20121008 :
																	// bug 078
									}
								}
							}
						});
						if (codeId.equals(BaseCount.Other)) {

							// LinearLayout.LayoutParams paramtext = new
							// LinearLayout.LayoutParams(
							// LinearLayout.LayoutParams.MATCH_PARENT, 60);
							LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.MATCH_PARENT, 80);
							final EditText txtOther = new EditText(this);
							txtOther.setLayoutParams(paramtext);
							txtOther.setBackgroundResource(R.drawable.txtother08);
							txtOther.setTextColor(Color.BLACK);
							txtOther.setId(99999);
							txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
									200) });
							txtOther.setSingleLine(true);// Bug 115
							if (listChecked.containsKey(codeId)) {
								String text = listChecked.get(codeId);
								txtOther.setText(text);
							} else {
								txtOther.setEnabled(false); // 20121008 : bug
															// 078
							}
							txtOther.setTextSize(14);
							// 20121005 dai modify: Bug 010
							// txtOther.setGravity(Gravity.BOTTOM |
							// Gravity.LEFT);
							txtOther.setGravity(Gravity.CENTER | Gravity.LEFT);
							txtOther.setPadding(1, 0, 0, 0);
							// end
							txtOther.addTextChangedListener(new TextWatcher() {

								public void onTextChanged(CharSequence s,
										int start, int before, int count) {
									// TODO Auto-generated method stub

								}

								public void beforeTextChanged(CharSequence s,
										int start, int count, int after) {
									// TODO Auto-generated method stub

								}

								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									String text = txtOther.getText().toString();
									Cl_TRPatient cl = new Cl_TRPatient(
											E008.this);
									cl.updateTR_Patient(FireStationCode,
											CaseCode, BaseCount.Other, text);
								}
							});
							chk2.setWidth(180);
							layoutOther.setGravity(Gravity.CENTER
									| Gravity.LEFT);
							layoutOther.addView(chk2);
							layoutOther.addView(txtOther);
						} else {
							// chk2.setLeft(10);
							l.addView(chk2);
						}
						listItem.put(String.valueOf(id), codeId);
						id++;
					}

					layoutChk.addView(l);
				} while (cor.moveToNext());

			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E008.this, e.getMessage(), "E008", true);
		} finally {
			cor.close();
		}

	}

	private void showCombobox(int hight) {
		LinearLayout ldown = (LinearLayout) findViewById(R.id.layoutCombobox);
		ldown.setOnTouchListener(this);
		TranslateAnimation anim = new TranslateAnimation(0, 0, 0, 0);
		anim.setFillAfter(true);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, hight);
		params.setMargins(5, 5, 5, 0);
		ldown.setLayoutParams(params);
		anim.setDuration(1000);
		ldown.setAnimation(anim);

	}

	private void hideCombobox(int hight) {
		LinearLayout ldown = (LinearLayout) findViewById(R.id.layoutCombobox);
		TranslateAnimation anim = new TranslateAnimation(0, 0, 0, 0);
		anim.setFillAfter(true);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, hight);
		params.setMargins(5, 5, 5, 0);
		ldown.setLayoutParams(params);
		anim.setDuration(1000);
		ldown.setAnimation(anim);

	}

	private void loadComboJSC() {
		// adapterjsc.clear();
		Cursor cor = null;
		try {
			cbojsc = (Spinner) findViewById(R.id.cboE8JSC);
			cbojsc.setOnTouchListener(this);
			adapterjsc = new adapterActivity(E008.this, R.layout.layoute8jsc);
			cbojsc.setAdapter(adapterjsc);
			cbojsc.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapterjsc.getItem(arg2);
					if (dt != null) {
						CL_TRCase cl = new CL_TRCase(E008.this);
						cl.updateColumsTRCase(FireStationCode, CaseCode,
								TR_CaseCount.VitalSign_JCS, dt.getCodeId());
					}

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			Cl_Code cd = new Cl_Code(E008.this);
			cor = cd.getMS_Code("00009");
			DTO_MCode codenull = new DTO_MCode();
			codenull.setCodeId("");
			codenull.setCodeName("");
			adapterjsc.add(codenull);
			if (cor != null && cor.getCount() > 0) {

				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cor.getString(cor.getColumnIndex("CodeID")));
					code.setCodeName(cor.getString(cor
							.getColumnIndex("CodeName")));
					adapterjsc.add(code);
				} while (cor.moveToNext());
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E008.this, e.getMessage(), "E008", true);
		} finally {
			cor.close();
		}
	}

	private void loadComboGSC() {
		// adaptergsce.clear();
		// adaptergscv.clear();
		// adaptergscm.clear();
		Cursor cor = null;
		try {

			cbogsce = (Spinner) findViewById(R.id.cboGSVE);
			cbogsce.setOnTouchListener(this);
			adaptergsce = new adapterActivity(this, R.layout.layoute8jsc);
			cbogsce.setAdapter(adaptergsce);
			cbogsce.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adaptergsce.getItem(arg2);
					if (dt != null) {
						CL_TRCase cl = new CL_TRCase(E008.this);
						cl.updateColumsTRCase(FireStationCode, CaseCode,
								TR_CaseCount.VitalSign_GCS_E, dt.getCodeId());
					}
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			Cl_Code cd = new Cl_Code(E008.this);
			cor = cd.getMS_Code("00010");
			DTO_MCode codenull = new DTO_MCode();
			codenull.setCodeId("");
			codenull.setCodeName("");
			adaptergsce.add(codenull);
			if (cor != null && cor.getCount() > 0) {

				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cor.getString(cor.getColumnIndex("CodeID")));
					code.setCodeName(cor.getString(cor
							.getColumnIndex("CodeName")));
					adaptergsce.add(code);
				} while (cor.moveToNext());
			}
			cbogscv = (Spinner) findViewById(R.id.cboGSCV);
			cbogscv.setOnTouchListener(this);
			adaptergscv = new adapterActivity(this, R.layout.layoute8jsc);
			cbogscv.setAdapter(adaptergscv);
			cbogscv.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adaptergscv.getItem(arg2);
					if (dt != null) {
						CL_TRCase cl = new CL_TRCase(E008.this);
						cl.updateColumsTRCase(FireStationCode, CaseCode,
								TR_CaseCount.VitalSign_GCS_V, dt.getCodeId());
					}
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			Cl_Code cd2 = new Cl_Code(E008.this);
			cor = cd2.getMS_Code("00011");
			DTO_MCode codenullv = new DTO_MCode();
			codenullv.setCodeId("");
			codenullv.setCodeName("");
			adaptergscv.add(codenullv);
			if (cor != null && cor.getCount() > 0) {

				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cor.getString(cor.getColumnIndex("CodeID")));
					code.setCodeName(cor.getString(cor
							.getColumnIndex("CodeName")));
					adaptergscv.add(code);
				} while (cor.moveToNext());
			}
			cbogscm = (Spinner) findViewById(R.id.cboGSCM);
			cbogscm.setOnTouchListener(this);
			adaptergscm = new adapterActivity(this, R.layout.layoute8jsc);
			cbogscm.setAdapter(adaptergscm);
			cbogscm.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adaptergscm.getItem(arg2);
					if (dt != null) {
						CL_TRCase cl = new CL_TRCase(E008.this);
						cl.updateColumsTRCase(FireStationCode, CaseCode,
								TR_CaseCount.VitalSign_GCS_M, dt.getCodeId());
					}
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			Cl_Code cd3 = new Cl_Code(E008.this);
			cor = cd3.getMS_Code("00012");
			DTO_MCode codenullm = new DTO_MCode();
			codenullm.setCodeId("");
			codenullm.setCodeName("");
			adaptergscm.add(codenullm);
			if (cor != null && cor.getCount() > 0) {

				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cor.getString(cor.getColumnIndex("CodeID")));
					code.setCodeName(cor.getString(cor
							.getColumnIndex("CodeName")));
					adaptergscm.add(code);
				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E008.this, e.getMessage(), "E008", true);
		} finally {
			cor.close();
		}
	}

	public class adapterActivity extends ArrayAdapter<DTO_MCode> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, true);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraper mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoute8jsc, null);
				mwp = new ViewWraper(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraper) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = (TextView) findViewById(R.id.txtE8NameCB);
			// Bug 129 20121012
			LinearLayout layout = mwp.getLayoutrow();
			if (drop) {
				layout.setBackgroundResource(R.drawable.bgrowcbbox);
			} else {
				layout.setBackgroundResource(R.drawable.cbobox);
			}
			//
			// img = mwp.getIcon();
			txt = mwp.getLable();
			DTO_MCode dt = new DTO_MCode();
			dt = this.getItem(position);
			txt.setText("   " + dt.getCodeName());

			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		LinearLayout layout = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtE8NameCB);
			}
			return lable1;
		}

		LinearLayout getLayoutrow() {
			if (layout == null) {
				layout = (LinearLayout) base.findViewById(R.id.lrCombox);
			}
			return layout;
		}

	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	// 20131211
	EditText txtHopital;
	RadioButton rdi1;
	RadioButton rdi2;
	String rdoSearch = "1";
	double lat = 0;
	double longi = 0;

	public void showE031() {
		try {

			final Dialog dialoge = new Dialog(E008.this);
			dialoge.setOnKeyListener(new OnKeyListener() {

				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						// isShow12 = false;
					}
					return false;
				}
			});
			dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialoge.setContentView(R.layout.layoute031);
			dialoge.setCanceledOnTouchOutside(false);// Bug 146
			// Bug 55 txtHopital
			txtHopital = (EditText) dialoge.findViewById(R.id.txtE121);
			//
			Button btnCacnel = (Button) dialoge.findViewById(R.id.btnCancel);
			btnCacnel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialoge.cancel();
					// isruning = false;
					// isShow12 = false;
				}
			});
			Button btnOk = (Button) dialoge.findViewById(R.id.btnOK);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// isruning = false;
					// isShow12 = false;
					// getKamokuName();
					if (rdoSearch.equals("1")) {

						// Criteria cri = new Criteria();
						// String tower = locationManager.getBestProvider(cri,
						// false);
						// Location location = locationManager
						// .getLastKnownLocation(tower);
						Location location = null;
						CL_TRCase cl = new CL_TRCase(E008.this);
						Cursor cur = cl
								.getItemTRcase(FireStationCode, CaseCode);
						if (cur != null && cur.getCount() > 0) {
							String lat = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Latitude_IS));
							String longi = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Longitude_IS));
							if (lat != null && longi != null) {
								location = new Location("A");
								location.setLatitude(Double.parseDouble(lat));
								location.setLongitude(Double.parseDouble(longi));
							}
						}
						if (cur != null)
							cur.close();
						if (location != null) {
							// lat = (double) (location.getLatitude() * 1E6);
							// longi = (double) (location.getLongitude() * 1E6);
							lat = location.getLatitude();
							longi = location.getLongitude();
							Intent myinten = new Intent(E008.this, E032.class);
							Bundle b = new Bundle();
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putString("rdoSearch", rdoSearch);
							b.putDouble("Lat", lat);
							b.putDouble("Longi", longi);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
							dialoge.cancel();

						} else {

							showDialogConfirmNOTGPS(ContantMessages.GPSDisiable);
							dialoge.cancel();
							//
						}

					} else {
						EditText txtHN = (EditText) dialoge
								.findViewById(R.id.txtE121);
						String hopitalname = txtHN.getText().toString();
						Intent myinten = new Intent(E008.this, E032.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString("rdoSearch", rdoSearch);
						b.putString("hopitalname", hopitalname);

						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						dialoge.cancel();
					}
				}
			});
			rdi1 = (RadioButton) dialoge.findViewById(R.id.rdioE121);
			rdi1.setButtonDrawable(R.drawable.radio);
			rdi1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "1";
						rdi1.setTextColor(Color.WHITE);
						rdi2.setTextColor(Color.parseColor("#777777"));
						rdi2.setChecked(false);
						txtHopital.setEnabled(false);

					}
				}
			});
			rdi2 = (RadioButton) dialoge.findViewById(R.id.rdioE122);
			rdi2.setButtonDrawable(R.drawable.radio);
			rdi2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "2";
						rdi2.setTextColor(Color.WHITE);
						rdi1.setTextColor(Color.parseColor("#777777"));
						rdi1.setChecked(false);
						txtHopital.setEnabled(true);

					}
				}
			});

			txtHopital.setEnabled(false);
			rdi1.setChecked(true);
			//
			dialoge.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E008.this);
		} finally {
		}
	}

	public void showDialogConfirmNOTGPS(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myinten = new Intent(E008.this, E032.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putString("rdoSearch", rdoSearch);
				b.putString("hopitalname", "");
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();
			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}
}
