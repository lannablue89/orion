package jp.osaka;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
	private static MediaPlayer mp = null;

	/** Stop old song and start new one */
	public static void playDesaster(Context context) {
		stop(context);
		CL_User cl = new CL_User(context);
		String res = cl.getAllowPlaySoundOnDisaster();
		if (res.equals("1")) {
			try {
				mp = MediaPlayer.create(context, R.raw.money);
				mp.setLooping(true);
				mp.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public static void playNotification(Context context) {
		stop(context);
		CL_User cl = new CL_User(context);
		String res = cl.getAllowPlaySoundOnNotification();
		if (res.equals("1")) {
			try {
				mp = MediaPlayer.create(context, R.raw.notification);
				mp.setLooping(true);
				mp.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	public static void playCapture(Context context) {
		stop(context);
		CL_User cl = new CL_User(context);
		String res = cl.getAllowPlaySoundOnNotification();
		if (res.equals("1")) {
			try {
				mp = MediaPlayer.create(context, R.raw.flash);
				mp.setLooping(true);
				mp.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	public static void playVideo(Context context) {
		stop(context);
		CL_User cl = new CL_User(context);
		String res = cl.getAllowPlaySoundOnNotification();
		if (res.equals("1")) {
			try {
				mp = MediaPlayer.create(context, R.raw.video);
				//mp.setLooping(true);
				mp.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	/** Stop the music */
	public static void stop(Context context) {
		if (mp != null) {
			mp.stop();
			mp.release();
			mp = null;
		}
	}

}
