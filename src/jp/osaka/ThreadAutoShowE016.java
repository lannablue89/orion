﻿package jp.osaka;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;

public class ThreadAutoShowE016 {

	Context con;
	String FireStationCode;
	String CaseCode;
	String codeIdSelect = "";
	int mycountId = 1;
	int MNETTIME = 0;
	int ThirdCoordinateTime = 0;
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";
	boolean hadLoc = false;
	private boolean Running = true;

	public boolean isRunning() {
		return Running;
	}

	public void setRunning(boolean running) {
		Running = running;
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");
			if (key.equals("1")) {
				// ShowE016();
				E015Dialog dg = new E015Dialog();
				dg.ShowE016((Activity) con, FireStationCode, CaseCode, MICode);

			} else if (key.equals("2")) {
				showE017();
			} else if (key.equals("3")) {
				showComfirmMessage();
			}
			// else if (key.equals("4")) {
			// getAddressJian();
			// }
		}

	};

	public void showConfirmAuto(Context context, String fireStationCode,
			String caseCode) {
		con = context;
		FireStationCode = fireStationCode;
		CaseCode = caseCode;
		Running = true;
		// Bug 161
		CL_TRCase tr = new CL_TRCase(con);
		hadLoc = tr.checkLocationJian(FireStationCode, CaseCode);
		//
		// Cl_ContactResultRecord cl = new Cl_ContactResultRecord(con);
		// int sl = cl.checkExistTR_ContactResultRecord00001(FireStationCode,
		// CaseCode);
		// if (sl == 0) {
		getConfig();
		startThread();
		// }
	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub

			try {
				while (Running) {
					Thread.sleep(30000);
					// Bug 140 show confirm open message
					@SuppressWarnings("static-access")
					SharedPreferences sh = con.getSharedPreferences("app",
							con.MODE_PRIVATE);
					String syndb = sh.getString("SysDB", "0");
					if (syndb.equals("1")) {
						continue;
					}
					// String newsms = sh.getString("NewSMS", "0");
					// if (newsms.equals("1")) {
					// // Editor e = sh.edit();
					// // e.putString("NewSMS", "0");
					// // e.commit();
					//
					// Message mg = handler.obtainMessage();
					// Bundle b = new Bundle();
					// b.putString("Key", "3");
					// mg.setData(b);
					// handler.sendMessage(mg);
					// }
					// //////////////////////////////////// show 16,17
					Cl_MovementTime cl = new Cl_MovementTime(con);
					String time = cl.getMinTime(FireStationCode, CaseCode);
					// Bug 161
					if (time != null && !time.equals("")) {
						// Bug 166 20121025
						// if (!hadLoc && isOnline()) {
						if (!hadLoc) {
							// Message mg = handler.obtainMessage();
							// Bundle b = new Bundle();
							// b.putString("Key", "4");
							// mg.setData(b);
							// handler.sendMessage(mg);
							getAddressJian();
						}
					}
					//
					Cl_ContactResultRecord cl2 = new Cl_ContactResultRecord(con);
					int sl = cl2.checkExistTR_ContactResultRecord00001(
							FireStationCode, CaseCode);
					if (sl == 0) {
						if (time != null && !time.equals("")) {

							String dateMNETTIME = sh.getString("MNETTIME", "");
							String dateThirdCoordinateTime = sh.getString(
									"ThirdCoordinateTime", "");
							// 20121006 : bug 045
							String ShowMNETTIME = sh.getString("ShowMNETTIME",
									"0");
							String ShowThirdCoordinateTime = sh.getString(
									"ShowThirdCoordinateTime", "0");
							if (ShowThirdCoordinateTime.equals("1")
									&& ShowMNETTIME.equals("1")) {
								// Running = false;
								continue;
							}
							boolean find = false;
							if (!ShowThirdCoordinateTime.equals("1")) {
								if (dateThirdCoordinateTime != null
										&& !dateThirdCoordinateTime.equals("")) {
									time = dateThirdCoordinateTime;
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter.parse(time);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double minute = diff * 1.0 / (1000 * 60);// doi
																				// sang
																				// p
									int min = (int) minute;
									// int tg = MNETTIME;
									int third = ThirdCoordinateTime;
									if (min >= third && Running) {
										find = true;
										Editor e = sh.edit();
										e.putString("ThirdCoordinateTime",
												Utilities.getDateTimeNow());
										e.putString("ShowThirdCoordinateTime",
												"1"); // 20121006
										// Bug
										// 045
										e.putString("MNETTIME",
												Utilities.getDateTimeNow());
										e.commit();

										Message mg = handler.obtainMessage();
										Bundle b = new Bundle();
										b.putString("Key", "2");
										mg.setData(b);
										handler.sendMessage(mg);
										// idx2++;
									}
								} else {
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter.parse(time);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());

									long diff = date2.getTime()
											- date1.getTime();
									double minute = diff * 1.0 / (1000 * 60);// doi
																				// sang
																				// p
									int min = (int) minute;
									// int tg = MNETTIME;
									int third = ThirdCoordinateTime;
									if (min >= third && Running) {
										find = true;
										Editor e = sh.edit();
										e.putString("ThirdCoordinateTime",
												Utilities.getDateTimeNow());
										e.putString("ShowThirdCoordinateTime",
												"1"); // 20121006
														// Bug
														// 045
										e.putString("MNETTIME",
												Utilities.getDateTimeNow());
										e.commit();

										Message mg = handler.obtainMessage();
										Bundle b = new Bundle();
										b.putString("Key", "2");
										mg.setData(b);
										handler.sendMessage(mg);
										// idx2++;
									}
								}
							}
							if (!find && !ShowMNETTIME.equals("1")) {
								if (dateMNETTIME != null
										&& !dateMNETTIME.equals("")) {
									time = dateMNETTIME;
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter.parse(time);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double minute = diff * 1.0 / (1000 * 60);// doi
																				// sang
																				// p
									int min = (int) minute;
									int tg = MNETTIME;
									// int third = ThirdCoordinateTime * idx2;
									if (min >= tg && Running) {
										Editor e = sh.edit();
										e.putString("MNETTIME",
												Utilities.getDateTimeNow());
										e.putString("ShowMNETTIME", "1");// 20121006
																			// Bug
																			// 045
										e.commit();

										Message mg = handler.obtainMessage();
										Bundle b = new Bundle();
										b.putString("Key", "1");
										mg.setData(b);
										handler.sendMessage(mg);
										// idx++;
									}
								} else {

									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter.parse(time);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double minute = diff * 1.0 / (1000 * 60);// doi
																				// sang
																				// p
									int min = (int) minute;
									int tg = MNETTIME;
									// int third = ThirdCoordinateTime * idx2;
									if (min >= tg && Running) {
										Editor e = sh.edit();
										e.putString("MNETTIME",
												Utilities.getDateTimeNow());
										e.putString("ShowMNETTIME", "1");// 20121006
																			// Bug
																			// 045
										e.commit();

										Message mg = handler.obtainMessage();
										Bundle b = new Bundle();
										b.putString("Key", "1");
										mg.setData(b);
										handler.sendMessage(mg);
										// idx++;
									}
								}
							}

						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");

			} finally {

			}

		}
	};

	// 20121018 bug 167
	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// 20121018 bug 161
	private void getAddressJian() {
		Cursor cur = null;
		try {
			LocationManager locationManager;

			locationManager = (LocationManager) con
					.getSystemService(Context.LOCATION_SERVICE);
			boolean gps_enabled = false;
			boolean network_enabled = false;
			try {
				gps_enabled = locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
			} catch (Exception ex) {
			}
			try {
				network_enabled = locationManager
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
			}
			// Criteria cri = new Criteria();
			// String tower = locationManager.getBestProvider(cri, false);
			boolean exitloc = false;
			Location location = null;
			CL_TRCase cl = new CL_TRCase(con);
			cur = cl.getItemTRcase(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				String lat = cur.getString(cur
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi = cur.getString(cur
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat != null && longi != null) {
					location = new Location("A");
					location.setLatitude(Double.parseDouble(lat));
					location.setLongitude(Double.parseDouble(longi));
					exitloc = true;
				} else {
					if (gps_enabled) {
						location = locationManager
								.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					}
					if (location == null && network_enabled) {
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					}
				}
			}

			GeoPoint p;

			if (location != null) {
				double lat = location.getLatitude();
				double longi = location.getLongitude();
				String addressjian = "";
				String country = "";
				if (isOnline()) {
					Geocoder geocoder = new Geocoder(con, Locale.getDefault());
					p = new GeoPoint((int) (location.getLatitude() * 1E6),
							(int) (location.getLongitude() * 1E6));
					List<Address> add = null;
					try {
						add = geocoder.getFromLocation(p.getLatitudeE6() / 1E6,
								p.getLongitudeE6() / 1E6, 1);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					if (add != null && add.size() > 0) {
						// Get address
						String post = "";
						for (int i = 0; i <= add.get(0)
								.getMaxAddressLineIndex(); i++) {
							country = add.get(0).getCountryName();
							post = add.get(0).getPostalCode();
							if (!add.get(0).getAddressLine(i).equals(country)) {
								if (addressjian != null
										&& !addressjian.equals("")) {
									addressjian = addressjian + ", ";
								}
								addressjian += add.get(0).getAddressLine(i);
							}

						}
						if (post != null && addressjian.contains(post)) {
							addressjian = addressjian.replace(post, "");
						}
					}
				}
				// Bug 160 Remove country
				if (addressjian != null && !addressjian.equals("")
						&& !addressjian.equals("〒")) {
					CL_TRCase cl2 = new CL_TRCase(con);
					cl2.updateAddressJIAN(FireStationCode, CaseCode,
							String.valueOf(lat), String.valueOf(longi),
							addressjian);

					hadLoc = true;
				} else {
					if (!exitloc) {
						CL_TRCase cl2 = new CL_TRCase(con);
						cl2.updateAddressJIAN(FireStationCode, CaseCode,
								String.valueOf(lat), String.valueOf(longi), "");
					}
				}
			}
			// LocationListener locationListenerGps = new LocationListener() {
			//
			// public void onLocationChanged(Location location) {
			// // TODO Auto-generated method stub
			// if (location != null) {
			// GeoPoint p;
			// double lat = location.getLatitude();
			// double longi = location.getLongitude();
			// String addressjian = "";
			// String country = "";
			// Geocoder geocoder = new Geocoder(con,
			// Locale.getDefault());
			// p = new GeoPoint((int) (location.getLatitude() * 1E6),
			// (int) (location.getLongitude() * 1E6));
			// List<Address> add = null;
			// try {
			// add = geocoder.getFromLocation(
			// p.getLatitudeE6() / 1E6,
			// p.getLongitudeE6() / 1E6, 1);
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// // e.printStackTrace();
			// }
			//
			// if (add != null && add.size() > 0) {
			// // Get address
			// String post = "";
			// for (int i = 0; i <= add.get(0)
			// .getMaxAddressLineIndex(); i++) {
			// country = add.get(0).getCountryName();
			// post = add.get(0).getPostalCode();
			// if (!add.get(0).getAddressLine(i)
			// .equals(country)) {
			// if (addressjian != null
			// && !addressjian.equals("")) {
			// addressjian = addressjian + ", ";
			// }
			// addressjian += add.get(0).getAddressLine(i);
			// }
			//
			// }
			// if (post != null && addressjian.contains(post)) {
			// addressjian = addressjian.replace(post, "");
			// }
			// }
			//
			// // Bug 160 Remove country
			// if (addressjian != null && !addressjian.equals("")) {
			// CL_TRCase cl = new CL_TRCase(con);
			// cl.updateAddressJIAN(FireStationCode, CaseCode,
			// String.valueOf(lat), String.valueOf(longi),
			// addressjian);
			//
			// hadLoc = true;
			// }
			// }
			// }
			//
			// public void onProviderDisabled(String arg0) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// public void onProviderEnabled(String provider) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// public void onStatusChanged(String provider, int status,
			// Bundle extras) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// };
			// locationManager.requestLocationUpdates(tower, 0, 0,
			// locationListenerGps);

		} catch (Exception ex) {
			// ex.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(con);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(con);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETTIME)) != null) {
					MNETTIME = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETTIME)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateTime)) != null) {
					ThirdCoordinateTime = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateTime)));

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}

	private void showComfirmMessage() {
		try {
			final Dialog dialog = new Dialog(con);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute016);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					SharedPreferences sh = con.getSharedPreferences("app",
							Context.MODE_PRIVATE);
					Editor e = sh.edit();
					e.putString("NewSMS", "0");
					e.commit();
					Intent myinten = new Intent(v.getContext(), E021.class);
					Bundle b7 = new Bundle();
					b7.putString("FireStationCode", FireStationCode);
					b7.putString("CaseCode", CaseCode);
					myinten.putExtras(b7);
					con.startActivity(myinten);

					dialog.cancel();

				}
			});
			// 20121006: bug 027
			TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
			CL_Message me = new CL_Message(con);
			String mg = me.getMessage(ContantMessages.OpenMessage);
			lbmg.setText(mg);

			dialog.show();
		} catch (Exception e) {
			E022.saveErrorLog(e.getMessage(), con);
		}
	}

	// private void showE015() {
	// final Dialog dialog = new Dialog(con);
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.layoute015);
	// dialog.setCanceledOnTouchOutside(false);// Bug 146
	// Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
	// btnCancel.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// dialog.cancel();
	// }
	// });
	// Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
	// btnOK.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// String note = "";
	// if (codeIdSelect.equals(BaseCount.Other)) {
	// EditText txtother = (EditText) dialog.findViewById(99999);
	// note = txtother.getText().toString();
	// }
	// Cl_ContactResultRecord cl1 = new Cl_ContactResultRecord(con);
	// DTO_TRCase dtcase = new DTO_TRCase();
	// int ContactCount = Integer.parseInt(dtcase.getContactCount());
	// Cl_ContactResultRecord cl = new Cl_ContactResultRecord(con);
	// //Bug 339 20121114
	// // long res = cl.createTR_ContactResultRecord(FireStationCode,
	// // CaseCode, ContactCount + 1, MICode, codeIdSelect, note);
	// long res = cl.createTR_ContactResultRecord(FireStationCode,
	// CaseCode, ContactCount + 1, MICode, codeIdSelect, note,"E017");
	// //
	// if (res >= 0) {
	// dtcase.setContactCount(ContactCount + 1 + "");
	// if (codeIdSelect.equals("00001")) {
	// // E015 up = new E015();
	// // JSONObject json = new JSONObject();
	// // json.put("FireStationCode", FireStationCode);
	// // json.put("CaseCode", CaseCode);
	// // json.put("ContactCount", ContactCount + 1);
	// // json.put("MICode", MICode);
	// // json.put("ContactResult_CD", codeIdSelect);
	// // json.put("ContactResult_TEXT", note);
	// // json.put("Flag", "0");
	// // String val = json.toString();
	// // up.upLoadDataToServer(val, con);
	// E015_0001 up = new E015_0001();
	// up.UpdateLoad(con, FireStationCode, CaseCode);
	// }
	// MICode = BaseCount.Other;
	// if (codeIdSelect.equals("00001")) {
	// dialog.cancel();
	// Intent myinten = new Intent(v.getContext(), E005.class);
	// Bundle b = new Bundle();
	// b.putString("FireStationCode", FireStationCode);
	// b.putString("CaseCode", CaseCode);
	// b.putString(BaseCount.IsOldData, "1");
	// myinten.putExtras(b);
	// ((Activity) con).startActivityForResult(myinten, 0);
	// ((Activity) con).finish();
	// } else {
	// Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
	// con);
	// int count = clc.countTR_ContactResultRecord(
	// FireStationCode, CaseCode);
	// if (count > ThirdCoordinateCount) {
	// showE017();
	// } else if (count > MNETCount) {
	// dialog.cancel();
	// ShowE016();
	// } else {
	// dialog.cancel();
	// }
	//
	// }
	// }
	// }
	// });
	// // dialog.setTitle("不搬送理由");
	// // dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
	// // R.drawable.o);
	// Cl_Code code = new Cl_Code(con);
	// Cursor cor = code.getMS_Code("00002");
	// if (cor != null && cor.getCount() > 0) {
	// mycountId = 1;
	// RadioGroup group = (RadioGroup) dialog
	// .findViewById(R.id.radioGroup1);
	// LinearLayout layout = (LinearLayout) dialog
	// .findViewById(R.id.layoutE73);
	// final HashMap<String, String> listItem = new HashMap<String, String>();
	// do {
	// final String codeId = cor.getString(cor
	// .getColumnIndex("CodeID"));
	// String codeName = cor.getString(cor.getColumnIndex("CodeName"));
	// listItem.put(codeId, codeName);
	// LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
	// LinearLayout.LayoutParams.WRAP_CONTENT,
	// LinearLayout.LayoutParams.WRAP_CONTENT);
	// paramCheck.setMargins(0, 10, 0, 10);
	// RadioButton rdi = new RadioButton(con);
	// rdi.setLayoutParams(paramCheck);
	// rdi.setText(codeName);
	// rdi.setTextColor(Color.BLACK);
	// rdi.setButtonDrawable(R.drawable.radio);
	// rdi.setTextSize(24);
	// rdi.setId(mycountId);
	// mycountId++;
	// if (codeId.equals("00001")) {
	// rdi.setChecked(true);
	// codeIdSelect = codeId;
	// }
	// rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	//
	// public void onCheckedChanged(CompoundButton buttonView,
	// boolean isChecked) {
	// // TODO Auto-generated method stub
	// if (isChecked) {
	// if (!codeId.equals("00001")) {
	// RadioButton radio = (RadioButton) dialog
	// .findViewById(1);
	// radio.setChecked(false);
	// }
	// codeIdSelect = codeId;
	// // 20121008 Bug 079
	// if (listItem.containsKey(BaseCount.Other)) {
	// EditText txtother = (EditText) dialog
	// .findViewById(99999);
	// txtother.setEnabled(false);
	// if (codeIdSelect.equals("99999")) {
	//
	// txtother.setEnabled(true);
	// } else {
	// txtother.setText("");
	// }
	// }
	// //
	// }
	// }
	// });
	// // listItem.put(String.valueOf(id), codeId);
	// group.addView(rdi);
	//
	// if (codeId.equals("99999")) {
	//
	// LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
	// 520, LinearLayout.LayoutParams.WRAP_CONTENT);
	// paramtext.setMargins(0, 5, 20, 0);
	// EditText txtOther = new EditText(con);
	// txtOther.setLayoutParams(paramtext);
	// txtOther.setBackgroundResource(R.drawable.inpute7);
	// txtOther.setTextColor(Color.BLACK);
	// txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
	// 200) });
	// txtOther.setSingleLine(true);//
	// txtOther.setEnabled(false);//
	// txtOther.setId(99999);
	// layout.addView(txtOther);
	// }
	// } while (cor.moveToNext());
	// dialog.show();
	// }
	//
	// }
	//
	// private void ShowE016() {
	// try {
	// getConfig();
	// final Dialog dialog = new Dialog(con);
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.layoute016);
	// dialog.setCanceledOnTouchOutside(false);// Bug 146
	// Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
	// btnCancel.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// dialog.cancel();
	// }
	// });
	// Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
	// btnOK.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// if (!MNetURL.startsWith("http://")
	// && !MNetURL.startsWith("https://"))
	// MNetURL = "http://" + MNetURL;
	// Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
	// .parse(MNetURL));
	// con.startActivity(browserIntent);
	// dialog.cancel();
	// CL_TRCase tr=new CL_TRCase(con);
	// tr.updateKin_kbn(FireStationCode, CaseCode, "0");
	//
	// }
	// });
	// // 20121006: bug 027
	// TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
	// CL_Message me = new CL_Message(con);
	// String mg = me.getMessage(ContantMessages.OpenBrowes);
	// if (mg != null && !mg.equals("")) {
	// lbmg.setText(mg);
	// }
	// dialog.show();
	// } catch (Exception e) {
	// E022.saveErrorLog(e.getMessage(), con);
	// }
	// }

	String phonecall = "";

	private void showE017() {
		Cursor cur = null;
		try {

			final Dialog dialog = new Dialog(con);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute017);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel17);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// 20121018 Bug 177
					phonecall = "";
					dialog.cancel();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOK17);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!phonecall.equals("")) {
						callPhone(phonecall);
						dialog.cancel();
						// showE015();
						E015Dialog dg = new E015Dialog();
						dg.ShowE015((Activity) con, FireStationCode, CaseCode,
								MICode, "E017");

					}

				}
			});
			Cl_FireStationStandard cl = new Cl_FireStationStandard(con);
			cur = cl.getHopital17();
			if (cur != null && cur.getCount() > 0) {
				mycountId = 1;
				LinearLayout layoutRdi = (LinearLayout) dialog
						.findViewById(R.id.layoutItem17);

				do {
					final String mICode = cur.getString(cur
							.getColumnIndex("MICode"));
					String MIName_Kanji = cur.getString(cur
							.getColumnIndex("MIName_Kanji"));
					String ThirdCoordinateTelNo = cur.getString(cur
							.getColumnIndex("ThirdCoordinateTelNo"));

					LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramCheck.setMargins(0, 30, 0, 0);
					final RadioButton rdi = new RadioButton(con);
					rdi.setLayoutParams(paramCheck);
					rdi.setText(ThirdCoordinateTelNo);
					rdi.setTextColor(Color.BLACK);
					rdi.setButtonDrawable(R.drawable.radio);
					rdi.setTextSize(24);
					rdi.setId(mycountId++);
					rdi.setTextColor(Color.parseColor("#777777"));

					LinearLayout.LayoutParams paramtxt = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtxt.setMargins(45, 0, 0, 0);
					TextView txtName = new TextView(con);
					txtName.setLayoutParams(paramtxt);
					txtName.setText(MIName_Kanji);
					txtName.setTextColor(Color.parseColor("#777777"));
					txtName.setId(mycountId++);
					txtName.setTextSize(24);
					layoutRdi.addView(rdi);
					layoutRdi.addView(txtName);
					// Bug 163
					txtName.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							rdi.setChecked(true);
						}
					});
					//
					rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								for (int i = 1; i < mycountId; i = i + 2) {
									RadioButton radio = (RadioButton) dialog
											.findViewById(i);
									if (radio.isChecked() && rdi.getId() != i)
										radio.setChecked(false);
								}
								rdi.setChecked(true);
								rdi.setTextColor(Color.WHITE);
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color.WHITE);
								MICode = mICode;
								phonecall = rdi.getText().toString();
							} else {
								rdi.setTextColor(Color.parseColor("#777777"));
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color
										.parseColor("#777777"));
							}
						}
					});

				} while (cur.moveToNext());
			}

			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			// E022.showErrorDialog(con, e.getMessage(), "E017", true);
			E022.saveErrorLog(e.getMessage(), con);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void callPhone(String phonenumber) {
		try {
			phonenumber = phonenumber.replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", "").trim();
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + phonenumber));
			con.startActivity(callIntent);
		} catch (Exception e) {
			// TODO: handle exception
			// E022.showErrorDialog(con, e.getMessage(), "E013A", true);
			E022.saveErrorLog(e.getMessage(), con);
		}
	}

}
