package jp.osaka;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class E029 extends Activity {
	private static final String LOG_TAG = "AudioRecordTest";

	private static String mFileName = null;
	static Context context;
	// private RecordButton mRecordButton = null;
	private MediaRecorder mRecorder = null;

	// private PlayButton mPlayButton = null;
	private MediaPlayer mPlayer = null;

	// private void onRecord(boolean start) {
	// if (start) {
	// startRecording();
	// } else {
	// stopRecording();
	// }
	// }

	private void onPlay(boolean start) {
		if (start) {
			startPlaying();
		} else {
			stopPlaying();
		}
	}

	private void startPlaying() {
		mPlayer = new MediaPlayer();
		try {
			mPlayer.setDataSource(mFileName);
			mPlayer.prepare();
			mPlayer.start();
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}
	}

	private void stopPlaying() {
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	private void startRecording() {
		mRecorder = new MediaRecorder();
		if (AudioSource > -1)
			mRecorder.setAudioSource(AudioSource);
		else
			mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		if (OutputFormat > -1)
			mRecorder.setOutputFormat(OutputFormat);
		else
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mRecorder.setOutputFile(mFileName);
		if (AudioEncoder > -1)
			mRecorder.setAudioEncoder(AudioEncoder);
		else
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

		try {
			mRecorder.prepare();
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}

		mRecorder.start();
	}

	private void stopRecording() {
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
	}

	Button btnRecord, btnPlay;
	boolean bRecording, bPlaying, bStart;
	ArrayList<String> dsFile;
	ListView lvFile;
	profileAdapter myAdapterFile;
	String FolderApp = "multimedia/voices";
	String PathFolder = "";
	boolean brun;
	Handler handler;
	TextView txtTime;
	TextView txtMessage;
	TextView txtImage;
	int time;
	DTO_FileInfo dtDel;
	int FileTime;
	String FireStationCode, CaseCode;
	int AudioSource = -1, OutputFormat = -1, AudioEncoder = -1;

	class myrunable implements Runnable {
		public void run() {
			// TODO Auto-generated method stub

			while (brun) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						if (brun) {
							time++;
							FileTime = time;
							txtTime.setText(Utilities.converttoStringTime(time));
						}
					}
				});
			}
		}
	}

	class myrunableplay implements Runnable {
		public void run() {
			// TODO Auto-generated method stub
			time = 0;
			brun = true;
			while (brun) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						if (brun) {
							time++;
							txtTime.setText(Utilities.converttoStringTime(time));
						}
						if (time == FileTime) {
							brun = false;
							bPlaying = true;
							txtMessage.setText("再生が完了しました。");
							// btnPlay.setText("▶");
							btnPlay.setBackgroundResource(R.drawable.btnplay);
							btnRecord.setEnabled(true);
							// btnRecord.setTextColor(Color.parseColor("#C63E3B"));
							btnRecord.setBackgroundResource(R.drawable.btnghi);

						}

					}
				});
			}
		}
	}

	void startThreadTime() {
		myrunable runable = new myrunable();
		Thread th = new Thread(runable);
		th.start();
	}

	void startThreadPlay() {
		txtMessage.setText("再生中…");
		myrunableplay runable = new myrunableplay();
		Thread th = new Thread(runable);
		th.start();
	}

	private void getModelSetting() {
		Cursor cur = null;
		try {
			CL_ModelSetting cl = new CL_ModelSetting(E029.this);
			cur = cl.getModelSetting(android.os.Build.MODEL);
			if (cur != null && cur.getCount() > 0) {
				PathFolder = cur.getString(cur.getColumnIndex("Location_Name"));
				if (cur.getString(cur.getColumnIndex("Audio_source")) != null)
					AudioSource = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Audio_source")));
				if (cur.getString(cur.getColumnIndex("Audio_outputformat")) != null)
					OutputFormat = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Audio_outputformat")));
				if (cur.getString(cur.getColumnIndex("Audio_encorder")) != null)
					AudioEncoder = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Audio_encorder")));
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();

		}
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoutsound);

		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		// Init
		context = this;
		bRecording = true;
		bPlaying = true;
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		handler = new Handler();

		// get SEtting
		getModelSetting();

		txtTime = (TextView) findViewById(R.id.txtTime);
		txtMessage = (TextView) findViewById(R.id.txtMessage);
		txtImage = (TextView) findViewById(R.id.txtImage);
		dsFile = new ArrayList<String>();
		btnRecord = (Button) findViewById(R.id.btnRecord);
		btnRecord.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				// File mediaStorageDir = new File(Environment
				// .getExternalStorageDirectory(), FolderApp);
				//
				// // Create the storage directory if it does not exist
				// if (!mediaStorageDir.exists()) {
				// if (!mediaStorageDir.mkdirs()) {
				// Log.d("MyCameraApp", "failed to create directory");
				// return;
				// }
				// }
				if (bRecording) {
					createFilename();
					// time
					time = 0;
					brun = true;
					startThreadTime();
					txtMessage.setText("録音中…");
					// record
					startRecording();
					bRecording = false;
					bStart = true;
					// btnRecord.setText("■");
					// btnRecord.setTextColor(Color.parseColor("#000000"));
					btnRecord.setBackgroundResource(R.drawable.btnstop);
					// Not click play
					btnPlay.setEnabled(false);
					// btnPlay.setTextColor(Color.parseColor("#777777"));
					btnPlay.setBackgroundResource(R.drawable.btnplaydisable);
					txtImage.setVisibility(View.VISIBLE);
				} else {
					txtMessage.setText("録音が完了しました。");
					// btnRecord.setText("●");
					// btnRecord.setTextColor(Color.parseColor("#C63E3B"));
					btnRecord.setBackgroundResource(R.drawable.btnghi);
					// click play
					btnPlay.setEnabled(true);
					// btnPlay.setTextColor(Color.parseColor("#000000"));
					btnPlay.setBackgroundResource(R.drawable.btnplay);
					txtImage.setVisibility(View.INVISIBLE);
					// Stop time
					brun = false;
					// Stop record
					stopRecording();
					bRecording = true;

					// dsFile.add(mFileName);
					addfileToList(mFileName);

				}
			}
		});
		btnPlay = (Button) findViewById(R.id.btnPlay);
		btnPlay.setEnabled(false);
		btnPlay.setTextColor(Color.parseColor("#777777"));
		btnPlay.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				onPlay(bPlaying);
				if (bPlaying) {
					startThreadPlay();
					// btnPlay.setText("■");
					btnPlay.setBackgroundResource(R.drawable.btnstop);
					btnRecord.setEnabled(false);
					// btnRecord.setTextColor(Color.parseColor("#777777"));
					btnRecord
							.setBackgroundResource(R.drawable.btnrecorddisable);
				} else {
					brun = false;
					btnPlay.setText("▶");
					btnRecord.setEnabled(true);
					// btnRecord.setTextColor(Color.parseColor("#C63E3B"));
					btnRecord.setBackgroundResource(R.drawable.btnghi);
				}
				bPlaying = !bPlaying;
			}
		});
		Button btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		lvFile = (ListView) findViewById(R.id.listFile);
		myAdapterFile = new profileAdapter(this, R.layout.layoutrawsound);
		lvFile.setAdapter(myAdapterFile);
		lvFile.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				dtDel = myAdapterFile.getItem(arg2);
				// mFileName = dt.FullName;
				confirmDeleteItem();

			}
		});
		getListFile();

	}

	private void confirmDeleteItem() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					CL_FileInfo cl = new CL_FileInfo(E029.this);
					cl.deleteTR_FileInfo(FireStationCode, CaseCode,
							dtDel.MultimediaFileNo, "1");

					File f = new File(dtDel.FileName);
					if (f.exists())
						f.delete();

					myAdapterFile.remove(dtDel);
					// String memory = getAvailableExternalMemorySize();
					// txtMemory.setText(memory);
					// getListFile();
					dialog.cancel();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteItem);
		lbmg.setText(mg);

		dialog.show();
	}

	private void createFilename() {
		if (PathFolder==null||PathFolder.equals("")) {
			mFileName = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
			mFileName += "/" + FolderApp;
		} else {
			mFileName = PathFolder;
		}
		File mediaStorageDir = new File(mFileName);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
				return;
			}
		}
		mFileName = mFileName + "/" + FireStationCode + "_" + CaseCode + "_"
				+ Utilities.getDateTimeNow() + ".aac";
		// File mydir = ERECORDSOUND.this.getDir(FolderApp,
		// Context.MODE_PRIVATE);
		// File fileWithinMyDir = new File(mydir,Utilities.getDateTimeNow() +
		// ".amr");
		// mFileName=fileWithinMyDir.getPath();
	}

	void addfileToList(String fileName) {
		try {
			CL_FileInfo cl = new CL_FileInfo(E029.this);
			int maxfileno = cl.getMaxFileNo(FireStationCode, CaseCode);
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseCode);
			values.put("MultimediaFileNo", maxfileno + 1);
			String FileRegistered_DateTime = Utilities.getDateTimeNow();
			values.put("FileRegistered_DateTime", FileRegistered_DateTime);

			values.put("FileType", "1");
			File inFile = new File(fileName);
			values.put("FileName", inFile.getName());
			mPlayer = MediaPlayer.create(this, Uri.parse(inFile.getPath()));
			if (mPlayer != null) {

				int size = (int) (inFile.length() / 1024);
				values.put("FileTime", time);
				values.put("FileSize", size);
				cl = new CL_FileInfo(E029.this);
				long res = cl.createFileInfo(values);
				if (res > 0) {
					DTO_FileInfo dt = new DTO_FileInfo();
					dt.MultimediaFileNo = String.valueOf(maxfileno + 1);
					dt.FileRegistered_DateTime = FileRegistered_DateTime;
					dt.FileName = fileName;
					dt.FileSize = String.valueOf(size);
					dt.FileTime = String.valueOf(time);
					myAdapterFile.insert(dt, 0);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	void getListFile() {
		Cursor cur = null;
		try {
			if (myAdapterFile != null)
				myAdapterFile.clear();
			CL_FileInfo cl = new CL_FileInfo(E029.this);
			cur = cl.getTR_FileInfo(FireStationCode, CaseCode, "1");
			if (cur != null && cur.getCount() > 0) {
				do {
					String MultimediaFileNo = cur.getString(cur
							.getColumnIndex("MultimediaFileNo"));
					String FileRegistered_DateTime = cur.getString(cur
							.getColumnIndex("FileRegistered_DateTime"));
					String FileName = cur.getString(cur
							.getColumnIndex("FileName"));
					String FileTime = cur.getString(cur
							.getColumnIndex("FileTime"));
					String FileSize = cur.getString(cur
							.getColumnIndex("FileSize"));
					DTO_FileInfo dt = new DTO_FileInfo();
					dt.MultimediaFileNo = MultimediaFileNo;
					dt.FileRegistered_DateTime = FileRegistered_DateTime;
					String path = "";
					if (PathFolder==null||PathFolder.equals("")) {
						path = Environment.getExternalStorageDirectory()
								.getAbsolutePath();
						path += "/" + FolderApp;
					} else {
						path = PathFolder;
					}
					dt.FileName = path + "/" + FileName;
					dt.FileSize = FileSize;
					dt.FileTime = FileTime;
					myAdapterFile.add(dt);
				} while (cur.moveToNext());
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		if (mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}

		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
	}

	public class profileAdapter extends ArrayAdapter<DTO_FileInfo> {

		public profileAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraperDV mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutrawsound, null);
				mwp = new ViewWraperDV(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraperDV) convertView.getTag();
			}

			TextView txtName = mwp.getName();
			TextView txtTime = mwp.getTime();
			TextView txtSize = mwp.getSize();
			TextView txtSTT = mwp.getSTT();

			DTO_FileInfo dt = this.getItem(position);
			String date = dt.FileRegistered_DateTime.substring(4, 6) + "/"
					+ dt.FileRegistered_DateTime.substring(6, 8) + " "
					+ dt.FileRegistered_DateTime.substring(8, 10) + ":"
					+ dt.FileRegistered_DateTime.substring(10, 12);
			txtName.setText(date);
			txtTime.setText(dt.FileTime + "秒");
			txtSize.setText(dt.FileSize + "KB");
			txtSTT.setText(dt.MultimediaFileNo);
			return v;
		}
	}

	class ViewWraperDV {

		View base;

		TextView txt1 = null;
		TextView txt2 = null;
		TextView txt3 = null;
		TextView txt4 = null;

		ViewWraperDV(View base) {

			this.base = base;
		}

		TextView getName() {
			if (txt1 == null) {
				txt1 = (TextView) base.findViewById(R.id.txtName);
			}
			return txt1;
		}

		TextView getTime() {
			if (txt2 == null) {
				txt2 = (TextView) base.findViewById(R.id.txtFileTime);
			}
			return txt2;
		}

		TextView getSize() {
			if (txt3 == null) {
				txt3 = (TextView) base.findViewById(R.id.txtSize);
			}
			return txt3;
		}

		TextView getSTT() {
			if (txt4 == null) {
				txt4 = (TextView) base.findViewById(R.id.STT);
			}
			return txt4;
		}

	}
}
