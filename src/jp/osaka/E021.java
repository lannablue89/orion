﻿package jp.osaka;

import java.util.Calendar;

import jp.osaka.ContantTable.M_UserCount;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class E021 extends Activity implements OnTouchListener {
	String FireStationCode;
	String CaseCode;
	String EMSUnitCode = "";
	MyAdapter mydapter;
	String ReportDate = "99999999999999";
	String UserName;
	String DateFirst;
	String EMSUnitCodeFirst;
	String FlagMsg = "1";
	adapterActivity adapter1;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	Calendar c = Calendar.getInstance();
	boolean refesh = true;// Q065
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E021.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute021);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		Cursor cur = null;
		try {

			Cl_ReportingAll rp = new Cl_ReportingAll(this);
			rp.UpdateAllReportingAll();

			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			refesh = false;// Q065
			// createMenu();// menu
			// getConfig();
			// Testmenu
			// TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			LinearLayout laymain = (LinearLayout) findViewById(R.id.layoutMain);
			laymain.setOnTouchListener(this);

			ListView list = (ListView) findViewById(R.id.list021);
			list.setOnTouchListener(this);
			mydapter = new MyAdapter(this, R.layout.grid021);
			list.setAdapter(mydapter);

			Cl_ReportingAll r = new Cl_ReportingAll(this);
			cur = r.getTR_ReportingFirst();
			if (cur != null && cur.getCount() > 0) {
				DateFirst = cur
						.getString(cur.getColumnIndex("Report_DATETIME"));
				EMSUnitCodeFirst = cur.getString(cur
						.getColumnIndex("EMSUnitCode"));
			}

			Button btntop = (Button) findViewById(R.id.btnTop21);
			btntop.setOnTouchListener(this);
			btntop.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					showE021A();
				}
			});
			CL_User cl = new CL_User(this);
			cur = cl.fetchLastUSer();
			if (cur != null && cur.getCount() > 0) {
				EMSUnitCode = cur.getString(cur
						.getColumnIndex(M_UserCount.UserName));
			}
			UserName = EMSUnitCode;
			getData(ReportDate, UserName);

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	// bug 275 20121031
	private void reLoadData(String limit) {

		getFirtDate();

		Cursor cur = null;
		try {
			Cl_ReportingAll cl = new Cl_ReportingAll(this);
			cur = cl.getTR_ReportingReload("99999999999999", UserName, limit);
			if (cur != null && cur.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_ReportingAll dto = new DTO_ReportingAll();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_ReportingAll dt = new DTO_ReportingAll();
					dt.setReport_DATETIME(cur.getString(cur
							.getColumnIndex("Report_DATETIME")));
					dt.setReportTitle(cur.getString(cur
							.getColumnIndex("ReportTitle")));
					dt.setReportText(cur.getString(cur
							.getColumnIndex("ReportText")));
					dt.setEMSUnitCode(cur.getString(cur
							.getColumnIndex("EMSUnitCode")));
					dt.setEMSUnitName(cur.getString(cur
							.getColumnIndex("EMSUnitName")));
					mydapter.add(dt);
					ReportDate = dt.getReport_DATETIME();
					UserName = dt.getEMSUnitCode();
				} while (cur.moveToNext());
				if (!ReportDate.equals(DateFirst)
						|| !UserName.equals(EMSUnitCodeFirst)) {
					mydapter.add(null);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception

		} finally {
			cur.close();
		}
	}

	private void getFirtDate() {
		Cursor cur = null;
		try {
			Cl_ReportingAll r = new Cl_ReportingAll(this);
			cur = r.getTR_ReportingFirst();
			if (cur != null && cur.getCount() > 0) {
				DateFirst = cur
						.getString(cur.getColumnIndex("Report_DATETIME"));
				EMSUnitCodeFirst = cur.getString(cur
						.getColumnIndex("EMSUnitCode"));
			}
		} catch (Exception e) {

		} finally {
			if (cur != null)
				cur.close();
		}

	}

	public class adapterActivity extends ArrayAdapter<DTO_MCode> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, true);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraperCBB mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutcomboxe12, null);
				mwp = new ViewWraperCBB(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraperCBB) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = mwp.getLable();
			DTO_MCode dt = new DTO_MCode();
			dt = this.getItem(position);
			txt.setText(dt.getCodeName());
			LinearLayout layout = mwp.getLayoutrow();
			if (drop) {
				layout.setBackgroundResource(R.drawable.bgcbobox21);
			} else {
				layout.setBackgroundResource(R.drawable.cbobox21);
			}

			return v;
		}
	}

	class ViewWraperCBB {

		View base;
		TextView lable1 = null;
		LinearLayout layout = null;

		ViewWraperCBB(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtcboE12);
			}
			return lable1;
		}

		LinearLayout getLayoutrow() {
			if (layout == null) {
				layout = (LinearLayout) base.findViewById(R.id.lrCombox);
			}
			return layout;
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
		th.showConfirmAuto(this, FireStationCode, CaseCode);

		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		if (refesh) {
			// onCreate(null);
			String limit = String.valueOf(mydapter.getCount());
			if (mydapter != null)
				mydapter.clear();
			reLoadData(limit);
		}
		refesh = true;// Q065 20121027
	}

	private void getData(String report_DATETIME, String eMSUnitCode) {

		if (DateFirst == null) {
			getFirtDate();
		}
		Cursor cur = null;
		try {
			Cl_ReportingAll cl = new Cl_ReportingAll(this);
			cur = cl.getTR_ReportingAll(report_DATETIME, eMSUnitCode);
			if (cur != null && cur.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_ReportingAll dto = new DTO_ReportingAll();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_ReportingAll dt = new DTO_ReportingAll();
					dt.setReport_DATETIME(cur.getString(cur
							.getColumnIndex("Report_DATETIME")));
					dt.setReportTitle(cur.getString(cur
							.getColumnIndex("ReportTitle")));
					dt.setReportText(cur.getString(cur
							.getColumnIndex("ReportText")));
					dt.setEMSUnitCode(cur.getString(cur
							.getColumnIndex("EMSUnitCode")));
					dt.setEMSUnitName(cur.getString(cur
							.getColumnIndex("EMSUnitName")));
					mydapter.add(dt);
					ReportDate = dt.getReport_DATETIME();
					UserName = dt.getEMSUnitCode();
				} while (cur.moveToNext());
				if (!ReportDate.equals(DateFirst)
						|| !UserName.equals(EMSUnitCodeFirst)) {
					mydapter.add(null);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception

		} finally {
			cur.close();
		}

	}

	public class MyAdapter extends ArrayAdapter<DTO_ReportingAll> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.grid021, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}
			DTO_ReportingAll dt = new DTO_ReportingAll();
			dt = this.getItem(position);

			TextView txtTitle = myw.getTitle();
			TextView txtName = myw.getName();
			TextView txtDate = myw.getDate();
			TextView txtHour = myw.getHour();
			TextView txtNote = myw.getNote();
			LinearLayout row = myw.getRow();
			if (dt != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(0, 0, 0, 0);
				row.setLayoutParams(param);
				row.setOnClickListener(null);
				txtTitle.setVisibility(View.VISIBLE);
				txtName.setVisibility(View.VISIBLE);
				txtDate.setVisibility(View.VISIBLE);
				txtHour.setVisibility(View.VISIBLE);
				txtNote.setVisibility(View.VISIBLE);
				row.setBackgroundColor(Color.WHITE);

				txtTitle.setText(dt.getReportTitle());
				txtNote.setText(dt.getReportText());
				txtName.setText(dt.getEMSUnitName());
				String date = dt.getReport_DATETIME().substring(0, 8);
				String month = date.substring(4, 6);
				String day = date.substring(6, 8);
				String hour = dt.getReport_DATETIME().substring(8, 10);
				String minute = dt.getReport_DATETIME().substring(10, 12);
				txtHour.setText(hour + ":" + minute);
				if (date.equals(Utilities.getDateNow())) {
					txtDate.setText("今日");
				} else {
					int mount = Integer.parseInt(month);
					int daynow = Integer.parseInt(day);
					txtDate.setText(String.valueOf(mount) + "/"
							+ String.valueOf(daynow));
				}
			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 0);
				row.setLayoutParams(param);
				txtTitle.setVisibility(View.INVISIBLE);
				txtName.setVisibility(View.INVISIBLE);
				txtDate.setVisibility(View.INVISIBLE);
				txtHour.setVisibility(View.INVISIBLE);
				txtNote.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow21);
				row.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						getData(ReportDate, UserName);

					}
				});
			}
			return v;
		}

	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		TextView lable2 = null;
		TextView lable3 = null;
		TextView lable4 = null;
		TextView lable5 = null;
		LinearLayout layoutrow = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}

		TextView getTitle() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txt1);
			}
			return lable1;
		}

		TextView getName() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtName);
			}
			return lable2;
		}

		TextView getDate() {
			if (lable3 == null) {
				lable3 = (TextView) base.findViewById(R.id.txtDate);
			}
			return lable3;
		}

		TextView getHour() {
			if (lable4 == null) {
				lable4 = (TextView) base.findViewById(R.id.txtHour);
			}
			return lable4;
		}

		TextView getNote() {
			if (lable5 == null) {
				lable5 = (TextView) base.findViewById(R.id.txtNote);
			}
			return lable5;
		}

	}

	String title = "";
	String message = "";

	private void showE021A() {
		Cursor cur = null;
		try {

			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layout021a);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			Button btnCancel = (Button) dialog.findViewById(R.id.btnE21Cancel);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnE21Ok);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditText txtTitlte = (EditText) dialog
							.findViewById(R.id.txtTitle);
					EditText txtMessage = (EditText) dialog
							.findViewById(R.id.txtMessge);
					title = txtTitlte.getText().toString();
					message = txtMessage.getText().toString();
					if (title != null && !title.equals("") && message != null
							&& !message.equals("")) {
						// Cl_ReportingAll cl = new Cl_ReportingAll(E021.this);
						// String date = Utilities.getDateTimeNow();
						//
						// long res = cl.createTR_ReportingAll(date,
						// EMSUnitCode,
						// txtTitlte.getText().toString(), txtMessage
						// .getText().toString());
						// if (res > 0) {
						// if (mydapter != null)
						// mydapter.clear();
						// getData("99999999999999", UserName);
						//
						// try {
						// E021A e = new E021A();
						// JSONObject json = new JSONObject();
						// json.put("Report_DATETIME", date);
						// json.put("EMSUnitCode", EMSUnitCode);
						// json.put("ReportTitle", txtTitlte.getText()
						// .toString());
						// json.put("ReportText", txtMessage.getText()
						// .toString());
						// String val = json.toString();
						// e.upLoadDataToServer(val, E021.this);
						// } catch (JSONException e1) {
						// // TODO Auto-generated catch block
						// e1.printStackTrace();
						// }
						//
						// }
						// dialog.cancel();
						ShowConfirm(dialog);
					}
				}
			});
			// 20121017 Bug 170
			Spinner cbo = (Spinner) dialog.findViewById(R.id.cboMSG);
			adapter1 = new adapterActivity(E021.this, R.layout.layoutcbobox21);
			cbo.setAdapter(adapter1);
			DTO_MCode code = new DTO_MCode();
			code.setCodeId("1");
			code.setCodeName("自消防本部内の救急隊");
			adapter1.add(code);
			code = new DTO_MCode();
			code.setCodeId("9");
			code.setCodeName("大阪府全ての救急隊");
			adapter1.add(code);
			cbo.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapter1.getItem(arg2);
					FlagMsg = dt.getCodeId();

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			//
			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E021.this, e.getMessage(), "E021", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void ShowConfirm(final Dialog dia) {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isOnline()) {
					if (title != null && !title.equals("") && message != null
							&& !message.equals("")) {
						Cl_ReportingAll cl = new Cl_ReportingAll(E021.this);
						String date = Utilities.getDateTimeNow();

						long res = cl.createTR_ReportingAll(date, EMSUnitCode,
								title, message, FlagMsg);
						if (res > 0) {
							if (mydapter != null)
								mydapter.clear();
							getData("99999999999999", UserName);

							try {
								E021A e = new E021A();
								JSONObject json = new JSONObject();
								json.put("Report_DATETIME", date);
								json.put("EMSUnitCode", EMSUnitCode);
								json.put("ReportTitle", title);
								json.put("ReportText", message);
								json.put("Destination", FlagMsg);
								String val = json.toString();
								e.upLoadDataToServer(val, E021.this);
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

						}

					}

					dialog.cancel();
					dia.cancel();
				} else { // Bug 111 20121012
					dialog.cancel();
					E022.showErrorDialog(E021.this,
							ContantMessages.CheckNotOnline,
							ContantMessages.CheckNotOnline, false);
				}

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.ConfirmMessage);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	// /
	// Bug 111
	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
