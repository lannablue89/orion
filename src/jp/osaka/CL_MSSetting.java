﻿package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import jp.osaka.ContantTable.MS_SettingCount;

public class CL_MSSetting {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_MSSetting(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	// MS_Setting
	public long createSetting(String MNETCount, String MNETTIME,
			String ThirdCoordinateCount, String ThirdCoordinateTime,
			String DBTime, String CaseCount, String PastCaseCount,
			String ScreenSensitivity, String ResultCount) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put(MS_SettingCount.MNETCount, MNETCount);
			values.put(MS_SettingCount.MNETTIME, MNETTIME);
			values.put(MS_SettingCount.ThirdCoordinateCount, ThirdCoordinateCount);
			values.put(MS_SettingCount.ThirdCoordinateTime, ThirdCoordinateTime);
			values.put(MS_SettingCount.DBTime, DBTime);
			values.put(MS_SettingCount.CaseCount, CaseCount);
			values.put(MS_SettingCount.PastCaseCount, PastCaseCount);
			values.put(MS_SettingCount.ResultCount, ResultCount);
			values.put(MS_SettingCount.ScreenSensitivity, ScreenSensitivity);
			String indate = Utilities.getDateTimeNow();
			values.put(MS_SettingCount.Update_DATETIME, indate);
			database.insert(MS_SettingCount.TableName, null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long createSetting(ContentValues values) {
		long result = -1;
		try {
			result = database.insert(MS_SettingCount.TableName, null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
		// database.close();
	}

	public Cursor fetchLastSeting() {
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database
					.rawQuery(
							"Select * from MS_Setting  order by Id DESC  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
			// database.close();
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	public long getDisasterDBTime() {
		long value = 10000;
		Cursor mCursor = null;
		try {
			mCursor = database.rawQuery(
					"Select * from MS_Setting order by Id DESC Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("DisasterDBTime")) != null) {
					long res = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("DisasterDBTime")));
					if (res > 0)
						value = res;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public long getTimerIntervalNotification() {
		long value = 10000;
		Cursor mCursor = null;
		try {
			mCursor = database.rawQuery(
					"Select * from MS_Setting order by Id DESC Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("TimerIntervalNotification")) != null) {
					long res = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("TimerIntervalNotification")));
					if (res > 0)
						value = res;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public int getGPSDBTime() {
		int value = 60000;
		Cursor mCursor = null;
		try {
			mCursor = database.rawQuery(
					"Select * from MS_Setting order by Id DESC Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("GPSDBTime")) != null) {
					int res = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("GPSDBTime")));
					if (res > 0)
						value = res * 1000;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public int updateSetting(String id, String MNETCount, String MNETTIME,
			String ThirdCoordinateCount, String ThirdCoordinateTime,
			String DBTime, String CaseCount, String PastCaseCount,
			String ScreenSensitivity, String ResultCount) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(MS_SettingCount.MNETCount, MNETCount);
			values.put(MS_SettingCount.MNETTIME, MNETTIME);
			values.put(MS_SettingCount.ThirdCoordinateCount,
					ThirdCoordinateCount);
			values.put(MS_SettingCount.ThirdCoordinateTime, ThirdCoordinateTime);
			values.put(MS_SettingCount.DBTime, DBTime);
			values.put(MS_SettingCount.CaseCount, CaseCount);
			values.put(MS_SettingCount.PastCaseCount, PastCaseCount);
			values.put(MS_SettingCount.ResultCount, ResultCount);
			values.put(MS_SettingCount.ScreenSensitivity, ScreenSensitivity);
			String indate = Utilities.getDateTimeNow();
			values.put(MS_SettingCount.Update_DATETIME, indate);
			result = database.update(MS_SettingCount.TableName, values,
					MS_SettingCount.Id + "=?", new String[] { id });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteAll() {
		boolean result = false;
		try {
			result = database.delete(MS_SettingCount.TableName, null, null) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public void close() {
		dbHelper.close();
	}
}
