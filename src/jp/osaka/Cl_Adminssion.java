package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_Adminssion {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_Adminssion(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public Cursor getAdmission(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getAdmission0002(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and Admission_CD='00002' ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	// //////
	public Cursor getAdmissionofHoppital(String MICode, String CaseCode,
			String timepre30) {
		Cursor mCursor = null;
		try {

			String[] args = { MICode, CaseCode, Utilities.getDateNow(), MICode,
					CaseCode, Utilities.getDateNow(), timepre30 };
			String sql = "Select a.*,b.EMSUnitName,c.FireStationName from TR_MedicalInstitutionAdmission a Inner Join MS_EMSUnit b on a.EMSUnitCode=b.EMSUnitCode  ";
			sql += " Inner Join MS_FireStation c on a.FireStationCode=c.FireStationCode ";
			sql += " Where (a.Admission_CD='00001'  and a.MICode=? and  a.CaseNo !=?  And substr(a.Movement_DATETIME,0,9)=? ) Or (a.Admission_CD='00002' and a.MICode=? and  a.CaseNo !=?  And substr(a.Movement_DATETIME,0,9)=? and a.Movement_DATETIME>?)";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public boolean checkTR_MedicalInstitutionAdmission00002(
			String FireStationCode, String CaseNo, String MICode,
			String TerminalNo) {
		boolean res = false;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, MICode, "00002",
					TerminalNo };
			String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0)
					res = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return res;
	}
}
