﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import android.app.Activity;
import android.app.Dialog;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class E015Dialog {
	Activity con;
	String codeIdSelect15;
	String FireStationCode, CaseCode;
	String MICode = "";
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	int mycountId = 1;
	String ClassName;
	boolean chk15 = false;
	CheckBox chk;
	public void ShowE015(Activity c, String fireStationCode, String caseCode,
			String micode, String className) {
		con = c;
		FireStationCode = fireStationCode;
		CaseCode = caseCode;
		MICode = micode;
		ClassName = className;
		final Dialog dialog = new Dialog(con);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute015);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String note = "";
				if (codeIdSelect15.equals(BaseCount.Other)) {
					EditText txtother = (EditText) dialog.findViewById(99999);
					note = txtother.getText().toString();
				}
				Cl_ContactResultRecord cl1 = new Cl_ContactResultRecord(con);
				DTO_TRCase dtcase = new DTO_TRCase();
				int ContactCount = 0;
				if (dtcase.getContactCount() == null) {
					ContactCount = Integer.parseInt(cl1.getContactCount(
							FireStationCode, CaseCode));
				} else {
					ContactCount = Integer.parseInt(dtcase.getContactCount());
				}
				Cl_ContactResultRecord cl = new Cl_ContactResultRecord(con);
				// Bug 339 20121114
				// long res = cl.createTR_ContactResultRecord(FireStationCode,
				// CaseCode, ContactCount + 1, MICode, codeIdSelect, note);

				long res = cl.createTR_ContactResultRecord(FireStationCode,
						CaseCode, ContactCount + 1, MICode, codeIdSelect15,
						note, ClassName, chk15);
				// end
				if (res >= 0) {
					dtcase.setContactCount(ContactCount + 1 + "");
					if (codeIdSelect15.equals("00001")) {
						E015_0001 up = new E015_0001();
						up.UpdateLoad(con, FireStationCode, CaseCode);

						dialog.cancel();
						Intent myinten = new Intent(v.getContext(), E005.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString(BaseCount.IsOldData, "1");
						myinten.putExtras(b);
						con.startActivityForResult(myinten, 0);
						con.finish();
					} else {
						Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
								con);
						int count = clc.countTR_ContactResultRecord(
								FireStationCode, CaseCode);
						// 20121006: daitran Bug 045
						// if (count >= ThirdCoordinateCount) {
						// showE017();
						// } else
						if (count == MNETCount) {
							dialog.cancel();
							ShowE016(con, FireStationCode, CaseCode, MICode);
						} else {
							dialog.cancel();
						}

					}
				}
			}
		});
		// dialog.setTitle("不搬送理由");
		// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
		// R.drawable.o);
		Cl_Code code = new Cl_Code(con);
		Cursor cor = code.getMS_Code("00002");
		if (cor != null && cor.getCount() > 0) {
			mycountId = 1;
			final RadioGroup group = (RadioGroup) dialog
					.findViewById(R.id.radioGroup1);
			LinearLayout layout = (LinearLayout) dialog
					.findViewById(R.id.layoutE73);
			final HashMap<String, String> listItem = new HashMap<String, String>();
			do {
				final String codeId = cor.getString(cor
						.getColumnIndex("CodeID"));
				String codeName = cor.getString(cor.getColumnIndex("CodeName"));
				listItem.put(codeId, codeName);
				LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				paramCheck.setMargins(0, 10, 0, 10);
				RadioButton rdi = new RadioButton(con);
				rdi.setLayoutParams(paramCheck);
				rdi.setText(codeName);
				rdi.setTextColor(Color.BLACK);
				rdi.setButtonDrawable(R.drawable.radio);
				rdi.setTextSize(24);
				rdi.setId(mycountId);
				mycountId++;
				
				rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							if (!codeId.equals("00001")) {
								RadioButton radio = (RadioButton) dialog
										.findViewById(1);
								radio.setChecked(false);
								chk.setChecked(false);
								chk.setEnabled(false);
							}
							else
							{
								chk.setEnabled(true);
								group.clearCheck();
							}
							codeIdSelect15 = codeId;
							// 20121008 Bug 079
							if (listItem.containsKey(BaseCount.Other)) {
								EditText txtother = (EditText) dialog
										.findViewById(99999);
								txtother.setEnabled(false);
								if (codeIdSelect15.equals("99999")) {

									txtother.setEnabled(true);
								} else {
									txtother.setText("");
								}
							}
							//
						}
					}
				});
				// listItem.put(String.valueOf(id), codeId);
			
				if (codeId.equals("00001")) {
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout lay = new LinearLayout(con);
					lay.setLayoutParams(paramrow);
					lay.setOrientation(LinearLayout.HORIZONTAL);
					chk = new CheckBox(con);
					chk.setLayoutParams(paramrow);
					chk.setTextColor(Color.BLACK);
					chk.setText("三次コーディネート");
					chk.setTextSize(16);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub
							chk15 = arg1;
						}
					});
					lay.addView(rdi);
					lay.addView(chk);
					group.addView(lay);
				} else {
					group.addView(rdi);
				}
				if (codeId.equals("00001")) {
					rdi.setChecked(true);
					codeIdSelect15 = codeId;
				}
				if (codeId.equals("99999")) {

					LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtext.setMargins(0, 5, 20, 0);
					EditText txtOther = new EditText(con);
					txtOther.setLayoutParams(paramtext);
					txtOther.setBackgroundResource(R.drawable.inpute7);
					txtOther.setTextColor(Color.BLACK);
					txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
							200) });
					txtOther.setSingleLine(true);//
					txtOther.setEnabled(false);//
					txtOther.setId(99999);
					layout.addView(txtOther);
				}
			} while (cor.moveToNext());
			dialog.show();
		}

	}

	public void ShowE016(Activity c, String fireStationCode, String caseCode,
			String micode) {

		con = c;
		FireStationCode = fireStationCode;
		CaseCode = caseCode;
		MICode = micode;
		getConfig();
		final Dialog dialog = new Dialog(con);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!MNetURL.startsWith("http://")
						&& !MNetURL.startsWith("https://"))
					MNetURL = "http://" + MNetURL;
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(MNetURL));
				con.startActivity(browserIntent);
				dialog.cancel();
				CL_TRCase tr = new CL_TRCase(con);
				tr.updateKin_kbn(FireStationCode, CaseCode, "0");

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(con);
		String mg = me.getMessage(ContantMessages.OpenBrowes);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(con);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(con);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}
}
