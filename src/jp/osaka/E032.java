﻿package jp.osaka;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

public class E032 extends Activity implements LocationListener, OnTouchListener {

	ListView list;
	MyAdapter mydapter;
	String FireStationCode;
	String CaseCode;
	String rdoSearch = "1";
	boolean brdiSearch = true;
	String hopitalname = "";
	String SHCACode;
	ArrayList<DTO_NightClinic> listItem;
	double lat = 0;
	double longi = 0;
	String destition = "";
	HashMap<String, String> listDic;
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";

	LocationManager locationManager;
	Location locationA;
	String tower;
	ProgressDialog p;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	int numberitem = 0;
	int numberDisplay = 0;
	int limit = 10;
	boolean refesh = true;// Q065
	int lastPosition = 0;
	int topPostion = 0;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");

			// showProccess();

			DTO_NightClinic dt = (DTO_NightClinic) b.getSerializable("DTO");
			if (dt != null)
				listItem.add(dt);
			// mydapter.add(dt);

			if (key.equals("1")) {
				// E032.this.sortAdapter();
				// mydapter.notifyDataSetChanged();
				//
				if (rdoSearch.equals("1"))
					sortList();
				else
					sortListBySortNo();
				loadData();
				exitProccess();
				if (mydapter != null && mydapter.getCount() < 1) {
					E022.showErrorDialog(E032.this, ContantMessages.NotData,
							ContantMessages.NotData, false);
				}
			}
		}

	};

	void exitProccess() {
		p.dismiss();
		if (lastPosition > 0 && mydapter.getCount() >= lastPosition) {
			// list.setSelection(lastPosition);
			list.setSelectionFromTop(lastPosition, topPostion);
		}
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E032.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E032.this);
		String mg = m.getMessage(ContantMessages.DataLoading);
		p.setMessage(mg);
		// 20121127 bug 393
		// p.setCancelable(false);
		p.setCanceledOnTouchOutside(false);
		//
		p.show();
	}

	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E032.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layoute032);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		Cursor cur = null;
		Cursor cur2 = null;
		refesh = false;// Q065
		numberitem = 0;
		lastPosition = 0;
		listItem = new ArrayList<DTO_NightClinic>();
		try {
			listDic = new HashMap<String, String>();
			// bug 393 20121128
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			// String e = b.getString("E");
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			rdoSearch = b.getString("rdoSearch");
			hopitalname = b.getString("hopitalname");

			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			CL_TRCase cl = new CL_TRCase(E032.this);
			cur2 = cl.getItemTRcase(FireStationCode, CaseCode);
			if (cur2 != null && cur2.getCount() > 0) {
				String lat2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat2 != null && longi2 != null) {
					locationA = new Location("A");
					locationA.setLatitude(Double.parseDouble(lat2));
					locationA.setLongitude(Double.parseDouble(longi2));
					lat = locationA.getLatitude();
					longi = locationA.getLongitude();
				}
			}
			// /
			getConfig();

			numberDisplay = limit;
			mydapter = new MyAdapter(E032.this, R.layout.layoutgrid32);
			list = (ListView) findViewById(R.id.list013);
			list.setAdapter(mydapter);
			list.setOnTouchListener(this);
			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					if (isOnline() && !check) {

						DTO_NightClinic dt = mydapter.getItem(arg2);
						Intent myinten = new Intent(E032.this, EMaps2.class);
						Bundle b = new Bundle();
						b.putString("Flat", String.valueOf(lat));
						b.putString("Flongi", String.valueOf(longi));
						b.putString("Tlat", dt.Latitude);
						b.putString("Tlongi", dt.Longitude);

						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
					}
					return false;
				}
			});

			list.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub

					DTO_NightClinic dt = mydapter.getItem(arg2);
					lastPosition = list.getFirstVisiblePosition();
					View v = list.getChildAt(0);
					topPostion = (v == null) ? 0 : v.getTop();
					if (dt != null) {
						if (!brdiSearch) {
							Intent myinten = new Intent(E032.this, E033A.class);
							Bundle b = new Bundle();
							b.putSerializable("myobject", dt);
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putInt("MNETCount", MNETCount);
							b.putInt("ThirdCoordinateCount",
									ThirdCoordinateCount);
							b.putString("MNetURL", MNetURL);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
						} else {
							Intent myinten = new Intent(E032.this, E033B.class);
							Bundle b = new Bundle();
							b.putSerializable("myobject", dt);
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putInt("MNETCount", MNETCount);
							b.putInt("ThirdCoordinateCount",
									ThirdCoordinateCount);
							b.putString("MNetURL", MNetURL);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
						}

					} else {
						numberDisplay += limit;
						loadData();
					}
				}
			});

			Button btnBack = (Button) findViewById(R.id.btnBack);
			btnBack.setOnTouchListener(this);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();

				}
			});
			RadioButton rdioFire = (RadioButton) findViewById(R.id.rdoFirestation);
			rdioFire.setButtonDrawable(R.drawable.radio);
			RadioButton rdioNight = (RadioButton) findViewById(R.id.rdoNightClinic);
			rdioNight.setButtonDrawable(R.drawable.radio);
			rdioFire.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					brdiSearch = isChecked;
					loadNewData();
				}
			});
			TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			txtHeader.setOnTouchListener(this);

			TextView txtOption = (TextView) findViewById(R.id.txtOptions13);
			txtOption.setOnTouchListener(this);
			if (rdoSearch.equals("1")) {
				txtOption.setText("現在地から直近順で検索");
			} else if (rdoSearch.equals("2")) {
				txtOption.setText("病院名を指定して検索");
			}

			showProccess();
			getlistDic();
			startThread();
			// createMenu();
			LinearLayout laymain = (LinearLayout) findViewById(R.id.layout_main);
			// laymain.setOnTouchListener(new OnTouchListener() {
			//
			// public boolean onTouch(View v, MotionEvent event) {
			// // TODO Auto-generated method stub
			// if (mMenu.isShowing()) {
			// mMenu.hide();
			// }
			// return false;
			// }
			// });
			laymain.setOnTouchListener(this);
			//RadioButton rdiofire = (RadioButton) findViewById(R.id.rdoFirestation);
			rdioNight.setOnTouchListener(this);
			rdioFire.setOnTouchListener(this);
			LinearLayout layout_1 = (LinearLayout) findViewById(R.id.layout_1);
			layout_1.setOnTouchListener(this);
			LinearLayout layout_2 = (LinearLayout) findViewById(R.id.layout_2);
			layout_2.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E032", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();

		}
	}

	private void getlistDic() {
		Cursor cor = null;
		try {
			Cl_Code code = new Cl_Code(this);
			cor = code.getMS_Code("00014");
			if (cor != null && cor.getCount() > 0) {
				do {
					String codeid = cor.getString(cor.getColumnIndex("CodeID"));
					String codename = cor.getString(cor
							.getColumnIndex("CodeName"));
					listDic.put(codeid, codename);
				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E032.this);
		} finally {
			if (cor != null)
				cor.close();
		}
	}

	private void loadNewData() {

		numberitem = 0;
		numberDisplay = limit;
		if (mydapter != null)
			mydapter.clear();
		listItem = new ArrayList<DTO_NightClinic>();
		try {

			showProccess();
			startThread();

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E032", true);
		} finally {
		}
	}

	// 20131115 Bug 45: refesh to last postion
	private void refeshScreen() {

		numberitem = 0;
		if (mydapter != null)
			mydapter.clear();
		listItem = new ArrayList<DTO_NightClinic>();
		try {

			getConfig();
			showProccess();
			startThread();

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E032", true);
		} finally {
		}
	}

	// Get distance for list items
	// private void getDistanceList()
	// {
	//
	// }
	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;
			Cursor cur2 = null;
			Cursor cur3 = null;
			try {
				// String dayofweek = Utilities.getDayOfWeek();
				// Cl_FireStationStandard cl1 = new Cl_FireStationStandard(
				// E032.this);
				// // if (cl1.checkHoliday()) {
				// // ko co format 5 so
				// // dayofweek = "00008";
				// dayofweek = "8";
				// }
				if (!brdiSearch) {
					CL_NightClinic cl = new CL_NightClinic(E032.this);
					cur = cl.getMS_NightClinic(hopitalname, rdoSearch);
					if (cur != null && cur.getCount() > 0) {
						int idx = 0;
						do {
							// 20121006 bug 044
							// try {
							// Thread.sleep(100);
							// } catch (InterruptedException e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }

							String MICode = cur.getString(cur
									.getColumnIndex("MICode"));
							String MIName_Kanji = cur.getString(cur
									.getColumnIndex("MIName_Kanji"));
							String Latitude = cur.getString(cur
									.getColumnIndex("Latitude"));
							String Longitude = cur.getString(cur
									.getColumnIndex("Longitude"));
							String MIType = cur.getString(cur
									.getColumnIndex("MIType"));
							String Address_Kanji = cur.getString(cur
									.getColumnIndex("Address_Kanji"));
							String TelNo1 = cur.getString(cur
									.getColumnIndex("TelNo1"));
							String TelNo2 = cur.getString(cur
									.getColumnIndex("TelNo2"));
							String SortNo = cur.getString(cur
									.getColumnIndex("SortNo"));
							DTO_NightClinic dt = new DTO_NightClinic();
							dt.MICode = MICode;
							dt.MIName_Kanji = MIName_Kanji;
							dt.Latitude = Latitude;
							dt.Longitude = Longitude;
							dt.MIType = MIType;
							dt.Address_Kanji = Address_Kanji;
							dt.TelNo1 = TelNo1;
							dt.TelNo2 = TelNo2;
							dt.Memo = cur.getString(cur.getColumnIndex("Memo"));
							if (SortNo != null) {
								try {
									dt.SortNo = Integer.parseInt(SortNo);
								} catch (Exception e) {
									// TODO: handle exception
									dt.SortNo = 0;
								}
							}
							// Bug 044 20121012 Dont get from google
							// if (isOnline()) {
							// String dis = getKhoangCach(String.valueOf(lat),
							// String.valueOf(longi), Latitude, Longitude);
							// double distance = Double.parseDouble(dis);
							// dt.setDistance(distance);
							// } else {

							if (locationA != null && rdoSearch.equals("1")) {
								Location locationB = new Location("point B");
								locationB.setLatitude(Double
										.parseDouble(Latitude));
								locationB.setLongitude(Double
										.parseDouble(Longitude));
								double distance = (double) locationA
										.distanceTo(locationB);
								distance = Utilities
										.roundTwoDecimals(distance / 1000);
								dt.Distance = distance;
							} else {
								dt.Distance = 0;
							}
							// }
							// if(destition!=null &&!destition.equals(""))
							// {
							// destition+="|";
							// }
							// destition=Latitude+","+Longitude;
							idx++;
							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putSerializable("DTO", dt);
							if (idx == cur.getCount()) {
								b.putString("Key", "1");
							} else {
								b.putString("Key", "0");
							}
							mg.setData(b);
							handler.sendMessage(mg);

						} while (cur.moveToNext());

					} else {
						Message mg = handler.obtainMessage();
						Bundle b = new Bundle();
						b.putSerializable("DTO", null);
						b.putString("Key", "1");
						mg.setData(b);
						handler.sendMessage(mg);

					}
				} else {
					CL_FireStationOriginalMI cl = new CL_FireStationOriginalMI(
							E032.this);
					cur = cl.getMS_FireStationOriginalMI(FireStationCode,
							hopitalname, rdoSearch);
					if (cur != null && cur.getCount() > 0) {

						String MICodetmp = "";
						do {

							// 20121006 bug 044
							// try {
							// Thread.sleep(100);
							// } catch (InterruptedException e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							String FireStationOriginalCode = cur.getString(cur
									.getColumnIndex("FireStationOriginalCode"));
							String MICode = cur.getString(cur
									.getColumnIndex("MICode"));
							String MIName_Kanji = cur.getString(cur
									.getColumnIndex("MIName_Kanji"));
							String Latitude = cur.getString(cur
									.getColumnIndex("Latitude"));
							String Longitude = cur.getString(cur
									.getColumnIndex("Longitude"));
							String MIType = cur.getString(cur
									.getColumnIndex("MIType"));
							String Address_Kanji = cur.getString(cur
									.getColumnIndex("Address_Kanji"));
							String TelNo1 = cur.getString(cur
									.getColumnIndex("TransportationTelNo"));
							String PrefectureCode = cur.getString(cur
									.getColumnIndex("PrefectureCode"));
							String SortNo = cur.getString(cur
									.getColumnIndex("SortNo"));
							if (MICode.equals(MICodetmp)) {
								continue;
							}
							MICodetmp = MICode;
							if (rdoSearch.equals("2") && hopitalname != null
									&& !hopitalname.trim().equals("")) {
								String MIName_Kana = cur.getString(cur
										.getColumnIndex("MIName_Kana"));
								if (MIName_Kanji == null)
									MIName_Kanji = "";
								if (MIName_Kana == null)
									MIName_Kana = "";
								if (!MIName_Kanji.toLowerCase().contains(
										hopitalname.toLowerCase())
										&& !MIName_Kana.toLowerCase().contains(
												hopitalname.toLowerCase())) {
									continue;
								}
							}

							DTO_NightClinic dt = new DTO_NightClinic();
							dt.FireStationOriginalCode = FireStationOriginalCode;
							dt.MICode = MICode;
							dt.MIName_Kanji = MIName_Kanji;
							dt.Latitude = Latitude;
							dt.Longitude = Longitude;
							dt.MIType = MIType;
							dt.Address_Kanji = Address_Kanji;
							dt.TelNo1 = TelNo1;
							if (listDic.containsKey(PrefectureCode)) {
								dt.Dictrict = listDic.get(PrefectureCode);
							}
							if (SortNo != null) {
								try {
									dt.SortNo = Integer.parseInt(SortNo);
								} catch (Exception e) {
									// TODO: handle exception
									dt.SortNo = 0;
								}

							}
							// Bug 044 20121012 Dont get from google
							// if (isOnline()) {
							// String dis = getKhoangCach(String.valueOf(lat),
							// String.valueOf(longi), Latitude, Longitude);
							// double distance = Double.parseDouble(dis);
							// dt.setDistance(distance);
							// } else {

							if (locationA != null && rdoSearch.equals("1")) {
								Location locationB = new Location("point B");
								if (Latitude != null && Longitude != null) {
									locationB.setLatitude(Double
											.parseDouble(Latitude));
									locationB.setLongitude(Double
											.parseDouble(Longitude));
									double distance = (double) locationA
											.distanceTo(locationB);
									distance = Utilities
											.roundTwoDecimals(distance / 1000);
									dt.Distance = distance;
								}
							} else {
								dt.Distance = 0;
							}
							// }
							// if(destition!=null &&!destition.equals(""))
							// {
							// destition+="|";
							// }
							// destition=Latitude+","+Longitude;

							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putSerializable("DTO", dt);
							// if (idx == cur.getCount()) {
							// b.putString("Key", "1");
							// } else {
							b.putString("Key", "0");
							// }
							mg.setData(b);
							handler.sendMessage(mg);

						} while (cur.moveToNext());

					} else {
						// Message mg = handler.obtainMessage();
						// Bundle b = new Bundle();
						// b.putSerializable("DTO", null);
						// b.putString("Key", "1");
						// mg.setData(b);
						// handler.sendMessage(mg);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");
				// Message mg = handler.obtainMessage();
				// Bundle b = new Bundle();
				// b.putSerializable("DTO", null);
				// b.putString("Key", "1");
				// mg.setData(b);
				// handler.sendMessage(mg);
			} finally {
				if (cur3 != null)
					cur3.close();
				if (cur2 != null)
					cur2.close();
				if (cur != null)
					cur.close();
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);

			}

		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private void loadData() {
		// int old = numberitem;
		if (mydapter.getCount() > 0) {

			DTO_NightClinic dto = mydapter.getItem(mydapter.getCount() - 1);
			if (dto == null) {
				mydapter.remove(dto);
			}
		}
		for (int i = 0; i < listItem.size(); i++) {
			if (i == numberDisplay) {
				break;
			}
			if (i >= numberitem) {
				numberitem++;

				DTO_NightClinic dti = listItem.get(i);
				mydapter.add(dti);
			}

		}
		if (listItem.size() > numberitem) {
			mydapter.add(null);
		}
		mydapter.notifyDataSetChanged();
	}

	private void sortList() {
		try {
			if (listItem != null && listItem.size() > 1) {

				Collections.sort(listItem, new Comparator<DTO_NightClinic>() {
					public int compare(DTO_NightClinic dt1, DTO_NightClinic dt2) {

						return Double.valueOf(dt1.Distance).compareTo(
								Double.valueOf(dt2.Distance));
					}
				});
				// Bug 14 20140120
				if (listItem.get(listItem.size() - 1).Distance > 0) {
					while (listItem.get(0).Distance == 0) {
						// DTO_NightClinic dt = listItem.get(0);
						DTO_NightClinic dt = listItem.remove(0);
						listItem.add(dt);
						// Collections.rotate(listItem, -1);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void sortListBySortNo() {
		try {

			if (listItem != null && listItem.size() > 1) {

				Collections.sort(listItem, new Comparator<DTO_NightClinic>() {
					public int compare(DTO_NightClinic dt1, DTO_NightClinic dt2) {

						return Integer.valueOf(dt1.SortNo).compareTo(
								Integer.valueOf(dt2.SortNo));
					}
				});
				// Bug 14 20140120
				if (listItem.get(listItem.size() - 1).SortNo > 0) {
					while (listItem.get(0).SortNo == 0) {
						// Collections.rotate(listItem, -1);
						DTO_NightClinic dt = listItem.remove(0);
						listItem.add(dt);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public class MyAdapter extends ArrayAdapter<DTO_NightClinic> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.layoutgrid32, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}

			TextView txtMiType = myw.getSTT();
			TextView txtHopital = myw.getName();
			TextView txtPhone = myw.getTXTPhone();
			TextView txtDic = myw.getTXTDic();
			TextView txtKm = myw.getTXTKM();
			LinearLayout row = myw.getRow();
			DTO_NightClinic dt = this.getItem(position);
			if (dt != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(0, 0, 0, 0);
				row.setLayoutParams(param);
				Cl_ContactResultRecord tr = new Cl_ContactResultRecord(
						E032.this);
				boolean exist = tr.checkExistTR_ContactResultRecord(
						FireStationCode, CaseCode, dt.MICode);
				if (exist) {
					row.setBackgroundResource(R.drawable.bgselected13c);
				} else
					row.setBackgroundResource(R.drawable.bggrid13c);
				// row.setOnClickListener(null);
				txtMiType.setVisibility(View.VISIBLE);
				txtHopital.setVisibility(View.VISIBLE);
				txtPhone.setVisibility(View.VISIBLE);
				txtDic.setVisibility(View.VISIBLE);
				if (rdoSearch.equals("1"))
					txtKm.setVisibility(View.VISIBLE);
				else
					txtKm.setVisibility(View.INVISIBLE);
				if (dt.MIType != null && dt.MIType.equals("5")) {
					txtMiType.setText("");
					txtMiType.setBackgroundResource(R.drawable.headerrow13c);
				} else if (dt.MIType != null && dt.MIType.equals("3")) {

					txtMiType.setText(dt.MIType);
					txtMiType.setBackgroundResource(R.drawable.headerrowred);
				} else if (dt.MIType != null && dt.MIType.equals("2")) {

					txtMiType.setText(dt.MIType);
					txtMiType.setBackgroundResource(R.drawable.headerrowe13);
				} else {
					txtMiType.setVisibility(View.INVISIBLE);
				}

				txtHopital.setText(dt.MIName_Kanji);
				txtPhone.setText(dt.TelNo1);
				if (!brdiSearch)
					txtDic.setText("");
				else
					txtDic.setText(dt.Dictrict);
				double dis = dt.Distance;
				if (dis > 0) {
					txtKm.setText(String.valueOf(dis) + " km");
				} else {
					txtKm.setText("? km");
				}

			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				row.setLayoutParams(param);
				txtMiType.setVisibility(View.INVISIBLE);
				txtHopital.setVisibility(View.INVISIBLE);
				txtPhone.setVisibility(View.INVISIBLE);
				txtDic.setVisibility(View.INVISIBLE);
				txtKm.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow);

			}

			return v;
		}

	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		TextView lable2 = null;
		TextView lable18 = null;
		TextView lable19 = null;
		TextView lable3 = null;
		LinearLayout layoutrow = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}

		TextView getSTT() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtGrid13STT);
			}
			return lable1;
		}

		TextView getName() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtGrid13Name);
			}
			return lable2;
		}

		TextView getTXTKM() {
			if (lable18 == null) {
				lable18 = (TextView) base.findViewById(R.id.txtGrid13117);
			}
			return lable18;
		}

		TextView getTXTPhone() {
			if (lable19 == null) {
				lable19 = (TextView) base.findViewById(R.id.txtPhone);
			}
			return lable19;
		}

		TextView getTXTDic() {
			if (lable3 == null) {
				lable3 = (TextView) base.findViewById(R.id.txtDic);
			}
			return lable3;
		}
	}

	String codeIdSelect = "";
	int mycountId = 1;

	// private void showE015() {
	// final Dialog dialog = new Dialog(this);
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.layoute015);
	// dialog.setCanceledOnTouchOutside(false);// Bug 146
	// Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
	// btnCancel.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// dialog.cancel();
	// }
	// });
	// Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
	// btnOK.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// String note = "";
	// if (codeIdSelect.equals(BaseCount.Other)) {
	// EditText txtother = (EditText) dialog.findViewById(99999);
	// note = txtother.getText().toString();
	// }
	// Cl_ContactResultRecord cl1 = new Cl_ContactResultRecord(
	// E032.this);
	// DTO_TRCase dtcase = new DTO_TRCase();
	// int ContactCount = 0;
	// if (dtcase.getContactCount() == null) {
	// ContactCount = Integer.parseInt(cl1.getContactCount(
	// FireStationCode, CaseCode));
	// } else {
	// ContactCount = Integer.parseInt(dtcase.getContactCount());
	// }
	// Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
	// E032.this);
	// // Bug 339 20121114
	// // long res = cl.createTR_ContactResultRecord(FireStationCode,
	// // CaseCode, ContactCount + 1, MICode, codeIdSelect, note);
	// long res = cl.createTR_ContactResultRecord(FireStationCode,
	// CaseCode, ContactCount + 1, MICode, codeIdSelect, note,
	// "E017");
	// // end
	// if (res >= 0) {
	// dtcase.setContactCount(ContactCount + 1 + "");
	// if (codeIdSelect.equals("00001")) {
	// // E015 up = new E015();
	// // JSONObject json = new JSONObject();
	// // json.put("FireStationCode", FireStationCode);
	// // json.put("CaseCode", CaseCode);
	// // json.put("ContactCount", ContactCount + 1);
	// // json.put("MICode", MICode);
	// // json.put("ContactResult_CD", codeIdSelect);
	// // json.put("ContactResult_TEXT", note);
	// // json.put("Flag", "0");
	// // String val = json.toString();
	// // up.upLoadDataToServer(val, E032.this);
	// E015_0001 up = new E015_0001();
	// up.UpdateLoad(E032.this, FireStationCode, CaseCode);
	// }
	// MICode = BaseCount.Other;
	// if (codeIdSelect.equals("00001")) {
	// dialog.cancel();
	// Intent myinten = new Intent(v.getContext(), E005.class);
	// Bundle b = new Bundle();
	// b.putString("FireStationCode", FireStationCode);
	// b.putString("CaseCode", CaseCode);
	// b.putString(BaseCount.IsOldData, "1");
	// myinten.putExtras(b);
	// startActivityForResult(myinten, 0);
	// finish();
	// } else {
	// Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
	// E032.this);
	// int count = clc.countTR_ContactResultRecord(
	// FireStationCode, CaseCode);
	// // 20121006: daitran Bug 045
	// // if (count >= ThirdCoordinateCount) {
	// // showE017();
	// // } else
	// if (count == MNETCount) {
	// dialog.cancel();
	// ShowE016();
	// } else {
	// dialog.cancel();
	// }
	// }
	// }
	// }
	// });
	// // dialog.setTitle("不搬送理由");
	// // dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
	// // R.drawable.o);
	// Cl_Code code = new Cl_Code(E032.this);
	// Cursor cor = code.getMS_Code("00002");
	// if (cor != null && cor.getCount() > 0) {
	// mycountId = 1;
	// RadioGroup group = (RadioGroup) dialog
	// .findViewById(R.id.radioGroup1);
	// LinearLayout layout = (LinearLayout) dialog
	// .findViewById(R.id.layoutE73);
	// final HashMap<String, String> listItem = new HashMap<String, String>();
	// do {
	// final String codeId = cor.getString(cor
	// .getColumnIndex("CodeID"));
	// String codeName = cor.getString(cor.getColumnIndex("CodeName"));
	// listItem.put(codeId, codeName);
	// LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
	// LinearLayout.LayoutParams.WRAP_CONTENT,
	// LinearLayout.LayoutParams.WRAP_CONTENT);
	// paramCheck.setMargins(0, 10, 0, 10);
	// RadioButton rdi = new RadioButton(this);
	// rdi.setLayoutParams(paramCheck);
	// rdi.setText(codeName);
	// rdi.setTextColor(Color.BLACK);
	// rdi.setButtonDrawable(R.drawable.radio);
	// rdi.setTextSize(24);
	// rdi.setId(mycountId);
	// mycountId++;
	// if (codeId.equals("00001")) {
	// rdi.setChecked(true);
	// codeIdSelect = codeId;
	// }
	// rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	//
	// public void onCheckedChanged(CompoundButton buttonView,
	// boolean isChecked) {
	// // TODO Auto-generated method stub
	// if (isChecked) {
	// if (!codeId.equals("00001")) {
	// RadioButton radio = (RadioButton) dialog
	// .findViewById(1);
	// radio.setChecked(false);
	// }
	// codeIdSelect = codeId;
	// // 20121008 Bug 079
	// if (listItem.containsKey(BaseCount.Other)) {
	// EditText txtother = (EditText) dialog
	// .findViewById(99999);
	// txtother.setEnabled(false);
	// if (codeIdSelect.equals("99999")) {
	//
	// txtother.setEnabled(true);
	// } else {
	// txtother.setText("");
	// }
	// }
	// //
	// }
	// }
	// });
	// // listItem.put(String.valueOf(id), codeId);
	// group.addView(rdi);
	//
	// if (codeId.equals("99999")) {
	//
	// LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
	// 520, LinearLayout.LayoutParams.WRAP_CONTENT);
	// paramtext.setMargins(0, 5, 20, 0);
	// EditText txtOther = new EditText(this);
	// txtOther.setLayoutParams(paramtext);
	// txtOther.setBackgroundResource(R.drawable.inpute7);
	// txtOther.setTextColor(Color.BLACK);
	// txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
	// 200) });
	// txtOther.setSingleLine(true);
	// txtOther.setEnabled(false);
	// txtOther.setId(99999);
	// layout.addView(txtOther);
	// }
	// } while (cor.moveToNext());
	// dialog.show();
	// }
	//
	// }

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				//
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// locationManager.removeUpdates(this); //Bug 166 20121025
		th.setRunning(false);
		bt.setRunning(false);
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(E032.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// locationManager.requestLocationUpdates(tower, 500, 1, this);//Bug 166
		// 20121025
		// SharedPreferences sh = this.getSharedPreferences("app",
		// this.MODE_PRIVATE);
		String syndb = getStatusSysDB();// sh.getString("SysDB", "0");
		if (refesh && syndb.equals("0")) {
			// onCreate(null);
			refeshScreen();
		}
		refesh = true;// Q065 20121027
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		// Bug 166 20121025
		// lat = location.getLatitude();
		// longi = location.getLongitude();

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
