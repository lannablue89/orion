package jp.osaka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.R.layout;

import com.google.android.gms.internal.da;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class E036 extends Activity implements IOnRadioChecked {
	AdapterE036 adapter;
	Context mContext;
	LinearLayout scroll;
	public static IOnRadioChecked iOnRadioChecked;
	public static List<IDataChange> listeners = new ArrayList<IDataChange>();
	ModelE036Result result;
	String FireStationCode, CaseCode, kin_no, kin_branch_no;
	Cursor tr_case;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// b.putString("FireStationCode", FireStationCode);
		// b.putString("CaseCode", CaseCode);
		// b.putString("kin_no", "100");
		// b.putString("kin_branch_no","1");
		mContext = this;
		setContentView(R.layout.layoute036);
		initView();
		initData();
		initDataFromDB();
	}

	private void initDataFromDB() {

		FireStationCode = getIntent().getStringExtra("FireStationCode");
		CaseCode = getIntent().getStringExtra("CaseCode");
		kin_no = getIntent().getStringExtra("kin_no");
		kin_branch_no = getIntent().getStringExtra("kin_branch_no");

		CL_TRCase cl_TRCase = new CL_TRCase(mContext);
		tr_case = cl_TRCase.getItemTRcase(FireStationCode, CaseCode);
		HeaderView header = (HeaderView) findViewById(R.id.header);
		header.initData(kin_no, kin_branch_no, FireStationCode, CaseCode);

		updateUi(19, true);

		String vitalsign_breath = tr_case.getString(tr_case
				.getColumnIndex("VitalSign_Breath"));
		if (vitalsign_breath != null && !vitalsign_breath.equals("0")) {
			updateUi(27, true);
			int temp = Integer.parseInt(vitalsign_breath);
			if (temp < 10 || temp > 30) {
				updateUi(30, true);
			} else {
				updateUi(31, true);
			}
		} else {
			updateUi(28, true);
		}

		String vitalsign_pulse = tr_case.getString(tr_case
				.getColumnIndex("VitalSign_Pulse"));
		if (vitalsign_pulse != null && !vitalsign_pulse.equals(0)) {
			int temp = Integer.parseInt(vitalsign_pulse);
			if (temp > 120) {
				updateUi(33, true);
			} else {
				updateUi(34, true);
			}
		}
	}

	private void initView() {
		scroll = (LinearLayout) findViewById(R.id.expanable);
		iOnRadioChecked = this;
	}

	private void initData() {

		ModelE036Root sub1 = new ModelE036Root("START法評価", 16);
		HashMap<String, Integer> submodel17 = new HashMap<String, Integer>();
		submodel17.put("可能", 1);
		submodel17.put("不可能", 0);
		ModelE036 model17 = new ModelE036("歩行", submodel17, -1, 17, 18, 19);
		sub1.add(model17);

		// init list20
		ModelE036Root list20 = new ModelE036Root("呼吸", 20);
		list20.setBackground(true);
		HashMap<String, Integer> submodel23 = new HashMap<String, Integer>();
		submodel23.put("sub", 1);
		submodel23.put("sub1", 0);
		ModelE036 model23 = new ModelE036("hehe", submodel23, -1, 21, 22, 23);
		list20.add(model23);// waiting for anh Son

		HashMap<String, Integer> submodel25 = new HashMap<String, Integer>();
		submodel25.put("気道を確保して", -1);
		submodel25.put("「呼吸あり", 1);
		submodel25.put("呼吸なし", 0);
		ModelE036 model25 = new ModelE036("呼吸の有無", submodel25, -1, 25, 27, 28);
		list20.add(model25);

		HashMap<String, Integer> submodel29 = new HashMap<String, Integer>();
		submodel29.put("10回/分未満　30回/分以上 \n(3回/6秒)以上", 1);
		submodel29.put("10回/分以上　　30回/分未満\n(3回/6秒)未満", 0);
		ModelE036 model29 = new ModelE036("呼吸回数", submodel29, -1, 29, 30, 31);
		model29.setMode(1);
		list20.add(model29);

		sub1.add(list20);

		HashMap<String, Integer> submodel32 = new HashMap<String, Integer>();
		submodel32.put("CRT2秒以上　　120回/分以上", 1);
		submodel32.put("CRT2秒未満　　120回/分未満", 0);
		ModelE036 model32 = new ModelE036("循環", submodel32, -1, 32, 33, 34);
		model32.setMode(1);
		sub1.add(model32);

		HashMap<String, Integer> submodel35 = new HashMap<String, Integer>();
		submodel35.put("有り", 1);
		submodel35.put("無し", 0);
		ModelE036 model35 = new ModelE036("従命反応", submodel35, -1, 35, 36, 37);
		sub1.add(model35);

		ModelE036Root sub2 = new ModelE036Root("START法」XX「回目」「　　評価判定", 38);

		result = new ModelE036Result("gg", -1, "", "", "");
		sub2.add(result);
		adapter = new AdapterE036(mContext, sub1, R.layout.groupviewexpanalble,
				R.layout.inflater_e036);
		scroll.addView(adapter.getView());
		adapter = new AdapterE036(mContext, sub2, R.layout.groupviewexpanalble,
				R.layout.inflater_e036);
		scroll.addView(adapter.getView());
	}

	@Override
	public void onItencheck(int id, int tag) {
		Log.d("onItencheck", "id="+id+" tag="+tag);
		switch (tag) {
		case 18:
			result.setChoosenValue(3);
			updateUi(20, false);
			updateUi(32, false);
			updateUi(35, false);
			break;
		case 19:
			updateUi(20, true);
			updateUi(25, true);
			break;
		case 28:
			result.setChoosenValue(0);
			updateUi(29, false);
			updateUi(32, false);
			updateUi(35, false);
			break;
		case 27:
			updateUi(29, true);
			break;
		case 30:
			result.setChoosenValue(1);
			updateUi(32, false);
			updateUi(35, false);
			break;
		case 31:
			updateUi(32, true);
		case 33:
			result.setChoosenValue(1);
			updateUi(35, false);
			break;
		case 34:
			updateUi(35, true);
			break;
		case 37:
			result.setChoosenValue(1);
		case 36:
			result.setChoosenValue(2);
			break;
		default:
			break;
		}
		result.initView();
	}

	private void updateUi(int tag, boolean value) {
		for (IDataChange iDataChange : listeners) {
			iDataChange.OnDataChange(tag, value);
		}
	}
}
