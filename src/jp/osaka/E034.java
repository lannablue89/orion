package jp.osaka;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import jp.osaka.ContantTable.TR_CaseCount;
import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class E034 {

	static Activity ac;
	Dialog dialog;

	String PathFolder = "";
	String FolderPicture = "multimedia/pictures";
	String FolderVideo = "multimedia/videos";
	String FolderVoice = "multimedia/voices";
	String FireStationCode, CaseCode;
	public static boolean bStop = false;
	int totalfile;
	int idxFile;
	int totalfilesize;
	int Outbound_No;
	ArrayList<DTO_MedicalInstitution> listHopital;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			int res = b.getInt("Result");
			if (res == 1) {

			} else if (res == 10)// Bug 116
			{
				updateCaseCode();

			} else if (res == 12) {
				showDialogERR(ContantMessages.ExistTR_Case);
			} else {
				E022.saveErrorLog("Send data error when upload file", ac);
			}

		}

	};

	public void showDialogERR(String code) {
		try {

			final Dialog dialog = new Dialog(ac);
			String error = "";
			CL_Message m = new CL_Message(ac);
			String meg = m.getMessage(code);
			if (meg != null && !meg.equals(""))
				error = meg;

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layouterror22);
			Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog.cancel();

				}
			});

			TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
			if (error != null && !error.equals(""))
				lbmg.setText(error);
			else
				lbmg.setText("Unknow...");
			TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
			if (code != null && !code.equals("")) {
				lbcode.setText(code);
			}
			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), ac);
		}
	}

	private void updateCaseCode() {
		try {
			CL_TRCase cl = new CL_TRCase(ac);
			int STT = cl.getMaxJANNumber(FireStationCode,
					CaseCode.substring(0, 18));

			int res = 0;
			String newcascode = "";
			do {
				STT++;
				String num = Utilities.format4Number(STT);
				newcascode = CaseCode.substring(0, 18) + num;
				CL_TRCase tr = new CL_TRCase(ac);
				res = tr.changeCase(FireStationCode, CaseCode, newcascode);
			} while (res == 0);

			CaseCode = newcascode;
			//CL_TRCase tr = new CL_TRCase(ac);
			startThread();

		} catch (Exception ex) {
			// exitProccess();
		} finally {

			// dShow = false;
		}
	}

	private void getModelSetting() {
		Cursor cur = null;
		try {
			CL_ModelSetting cl = new CL_ModelSetting(ac);
			cur = cl.getModelSetting(android.os.Build.MODEL);
			if (cur != null && cur.getCount() > 0) {
				PathFolder = cur.getString(cur.getColumnIndex("Location_Name"));

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();

		}
	}

	public static void SetActivity(Activity activity) {
		ac = activity;
	}

	public void UpdateLoad(Activity activity, String fireStationCode,
			String caseCode, int totalsize, int outbound_No,
			ArrayList<DTO_MedicalInstitution> listMINam) {
		ac = activity;
		bStop = false;
		FireStationCode = fireStationCode;
		CaseCode = caseCode;
		totalfilesize = totalsize;
		Outbound_No = outbound_No;
		listHopital = listMINam;
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		getModelSetting();
		
		if (isOnline()) {
			
			showDialogProcess();
			uploadfile();
			startThread();
			
		}

	}

	public void showDialogProcess() {
		try {

			dialog = new Dialog(ac);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoutdialogprocess);
			// dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			TextView txtClose = (TextView) dialog
					.findViewById(R.id.txtCloseDialog);
			txtClose.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			TextView txtStop = (TextView) dialog.findViewById(R.id.txtStop);
			txtStop.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					SharedPreferences sh = ac.getSharedPreferences("app",
							Context.MODE_PRIVATE);
					Editor e = sh.edit();
					e.putBoolean("Stop", true);
					e.commit();
					bStop = true;
					dialog.cancel();
				}
			});

			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// *************************
	// Send Data
	// **************************
	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;

			try {

				CL_TRCase tr = new CL_TRCase(ac);
				cur = tr.getItemTRcaseUpdateServer(FireStationCode, CaseCode);
				if (cur != null && cur.getCount() > 0 && isOnline()) {
					JSONArray array = new JSONArray();
					JSONObject trcase = ParseToJson(cur, "TR_Case");
					array.put(trcase);

					Cl_ContactResultRecord ctr = new Cl_ContactResultRecord(ac);
					cur = ctr.getTR_ContactResultRecordPatient(FireStationCode,
							CaseCode);
					JSONObject contact = ParseToJson(cur,
							"TR_ContactResultRecord");
					array.put(contact);

					Cl_FireStationStandard fs = new Cl_FireStationStandard(ac);
					cur = fs.getTR_FireStationStandardPatient(FireStationCode,
							CaseCode);
					JSONObject fire = ParseToJson(cur, "TR_FireStationStandard");
					array.put(fire);

					Cl_TRPatient pa = new Cl_TRPatient(ac);
					cur = pa.getTR_Patient(FireStationCode, CaseCode);
					JSONObject patient = ParseToJson(cur, "TR_Patient");
					array.put(patient);

					Cl_MovementTime mom = new Cl_MovementTime(ac);
					cur = mom.getTR_MovementTimeAll(FireStationCode, CaseCode);
					JSONObject movemnt = ParseToJson(cur, "TR_MovementTime");
					array.put(movemnt);

					Cl_FirstAid fir = new Cl_FirstAid(ac);
					cur = fir.getTR_FirstAid(FireStationCode, CaseCode);
					JSONObject FA = ParseToJson(cur, "TR_FirstAid");
					array.put(FA);

					Cl_FireStationStandard fs2 = new Cl_FireStationStandard(ac);
					cur = fs2.getTR_SelectionKamoku(FireStationCode, CaseCode);
					JSONObject select = ParseToJson(cur, "TR_SelectionKamoku");
					array.put(select);

					CL_ErrorLog er = new CL_ErrorLog(ac);
					String phone = getPhoneNumber();
					SharedPreferences sh = ac.getSharedPreferences("app",
							Context.MODE_PRIVATE);
					String Output_DATETIME = sh.getString("Output_DATETIME",
							"00010101010101");
					cur = er.getTR_ErrorLog(phone, Output_DATETIME);
					JSONObject log = ParseToJson(cur, "TR_ErrorLog");
					array.put(log);

					CL_FileInfo file = new CL_FileInfo(ac);
					cur = file.getTR_FileInfo(FireStationCode, CaseCode);
					JSONObject jfile = ParseToJson(cur, "TR_FileManageInfo");
					array.put(jfile);

					CL_AllReqMedicalInstitution Re = new CL_AllReqMedicalInstitution(
							ac);
					cur = Re.getAllReqMedicalInstitution(FireStationCode,
							CaseCode);
					JSONObject jReq = ParseToJson(cur,
							"TR_AllReqMedicalInstitution");
					array.put(jReq);

					Re = new CL_AllReqMedicalInstitution(ac);
					cur = Re.getAllReqMedicalInstitution_Result(
							FireStationCode, CaseCode);
					JSONObject jReq_re = ParseToJson(cur,
							"TR_AllReqMedicalInstitution_Result");
					array.put(jReq_re);

					int res = uploadDataToServer(array.toString());

					if (res == 1) {
						// b.putSerializable("DTO", dt);

						Editor e = sh.edit();
						e.putString("Output_DATETIME",
								Utilities.getDateTimeNow());
						e.commit();
					}
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("Result", res);
					mg.setData(b);
					handler.sendMessage(mg);

				} else {
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					// b.putSerializable("DTO", null);
					b.putInt("Result", 0);
					mg.setData(b);
					handler.sendMessage(mg);

				}
			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				// b.putSerializable("DTO", null);
				b.putInt("Result", 0);
				mg.setData(b);
				handler.sendMessage(mg);
			} finally {

				if (cur != null)
					cur.close();
			}

		}
	};

	public JSONObject ParseToJson(Cursor cor, String table) {
		JSONObject ob = new JSONObject();
		JSONArray arr = new JSONArray();
		try {
			if (cor != null && cor.getCount() > 0) {
				do {
					JSONObject json = new JSONObject();
					for (int i = 0; i < cor.getColumnCount(); i++) {
						String colname = cor.getColumnName(i);
						String value = cor.getString(i);
						if (colname.equals("patientstate")
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "00");
							}
						} else if (colname.equals(TR_CaseCount.ContactCount)
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
						} else {
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "");
							}
						}
					}
					arr.put(json);
				} while (cor.moveToNext());
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			E022.saveErrorLog(e1.getMessage(), ac);
		}
		try {
			ob.put(table, arr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return ob;
	}

	// Bug 116 Boolean -> int
	public int uploadDataToServer(String json) {
		// myadapter.clear();

		String encoded = "";
		try {
			encoded = URLEncoder.encode(json, "UTF-8");
			// String de= URLDecoder.decode(encoded, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
		}
		int result = 0;
		TrustManagerManipulator.allowAllSSL();
		String NAMESPACE = ContantSql.NameSpace;
		String METHOD_NAME = ContantSql.sendTRTable;
		String URL = ContantSystem.Endpoint;
		String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		request.addProperty("Json", encoded);
		// Param log 20130103
		CL_User cl = new CL_User(ac);
		String user = cl.getUserName();
		String loginfo = getLogInfo(SOAP_ACTIONS, "OK", user, "確認");
		request.addProperty("smpLog", loginfo);
		//
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		// envelope.dotNet=true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
				ContantSystem.TimeOut);

		try {
			androidhttpTranport.call(SOAP_ACTIONS, envelope);
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			result = 0;
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			result = 0;
		}
		Object responseBody = null;
		try {
			responseBody = envelope.getResponse();
			String t = responseBody.toString();
			if (t.equals("1")) {
				result = 1;
			} else if (t.equals("10")) {
				result = 10;
			} else if (t.equals("12")) {
				result = 12;
			}
		} catch (SoapFault e2) {
			// TODO Auto-generated catch block
			result = 0;
		}

		return result;
	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	// *************************
	// Upload File
	// **************************
	int serverResponseCode = 0;

	class UpLoadServer extends AsyncTask<String, Void, Integer> {

		// private Exception exception;

		protected Integer doInBackground(String... urls) {
			String fileName = urls[0];
			TrustManagerManipulator.allowAllSSL();
			HttpURLConnection conn = null;
			DataOutputStream dos = null;
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			File sourceFile = new File(fileName);

			if (!sourceFile.isFile()) {

				return 0;

			} else {
				try {

					// open a URL connection to the Servlet
					FileInputStream fileInputStream = new FileInputStream(
							sourceFile);
					String upLoadServerUri = "uploadfiles";
					upLoadServerUri = ContantSystem.Server + "/"
							+ upLoadServerUri;
					URL url = new URL(upLoadServerUri);

					// Open a HTTP connection to the URL
					conn = (HttpURLConnection) url.openConnection();
					if (Build.VERSION.SDK_INT > 13) {
						conn.setRequestProperty("Connection", "close");
					}
					conn.setDoInput(true); // Allow Inputs
					conn.setDoOutput(true); // Allow Outputs
					conn.setUseCaches(false); // Don't use a Cached Copy
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Connection", "Keep-Alive");
					conn.setRequestProperty("ENCTYPE", "multipart/form-data");
					conn.setRequestProperty("Content-Type",
							"multipart/form-data;boundary=" + boundary);
					conn.setRequestProperty("uploaded_file", fileName);

					dos = new DataOutputStream(conn.getOutputStream());

					dos.writeBytes(twoHyphens + boundary + lineEnd);
					// dos.writeBytes("Content-Disposition: form-data; name="
					// + fileName + ";filename=your_image_name.jpeg" + ""
					// + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
							+ fileName + "\"" + lineEnd);
					dos.writeBytes(lineEnd);

					// create a buffer of maximum size
					bytesAvailable = fileInputStream.available();

					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					buffer = new byte[bufferSize];

					// read file and write it into form...
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

					while (bytesRead > 0) {
						if (bStop) {
							return 10;
						}
						dos.write(buffer, 0, bufferSize);
						bytesAvailable = fileInputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);

					}
					// if (bStop) {
					//
					// return 10;
					// }
					// send multipart form data necesssary after file data...
					dos.writeBytes(lineEnd);
					dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

					// Responses from the server (code and message)
					serverResponseCode = conn.getResponseCode();
					String serverResponseMessage = conn.getResponseMessage();

					Log.i("uploadFile", "HTTP Response is : "
							+ serverResponseMessage + ": " + serverResponseCode);

					fileInputStream.close();
					dos.flush();
					dos.close();

				} catch (MalformedURLException ex) {

					Log.e("Upload file to server", "error: " + ex.getMessage(),
							ex);
				} catch (Exception e) {

					Log.e("Upload file to server Exception",
							"Exception : " + e.getMessage(), e);
				}

				// End else block
			}
			// Log.i("serverResponseCode", String.valueOf(serverResponseCode));
			return serverResponseCode;
		}

		protected void onPostExecute(Integer result) {
			// TODO: check this.exception
			// TODO: do something with the feed
			// SharedPreferences sh = ac.getSharedPreferences("app",
			// ac.MODE_PRIVATE);
			// boolean b = sh.getBoolean("Stop", false);
			if (!bStop) {
				idxFile++;

				if (serverResponseCode == 200 || serverResponseCode == 500) {
					if (idxFile == totalfile) {
						if (dialog != null)
							dialog.dismiss();
						ShowDialogOK();
					}
				} else {
					if (idxFile == totalfile) {
						if (dialog != null)
							dialog.dismiss();

						ShowDialogFail();
					}
				}
			}

		}
	}

	void uploadfile() {
		Cursor cur = null;

		try {
			CL_FileInfo cl = new CL_FileInfo(ac);
			cur = cl.getTR_FileInfo(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				totalfile = cur.getCount();
				do {

					String FileName = cur.getString(cur
							.getColumnIndex("FileName"));
					DTO_FileInfo dt = new DTO_FileInfo();
					dt.FileType = cur.getString(cur.getColumnIndex("FileType"));

					String mFileName;
					if (PathFolder == null || PathFolder.equals("")) {
						mFileName = Environment.getExternalStorageDirectory()
								.getAbsolutePath();
						if (dt.FileType.equals("1")) {
							mFileName += "/" + FolderVoice;
						} else if (dt.FileType.equals("2")) {
							mFileName += "/" + FolderPicture;
						} else {
							mFileName += "/" + FolderVideo;
						}
					} else {
						mFileName = PathFolder;
					}
					String fileName;
					fileName = mFileName + "/" + FileName;
					new UpLoadServer().execute(fileName);
					// Thread.sleep(1000);
				} while (cur.moveToNext());
			} else {
				if (dialog != null)
					dialog.dismiss();
			}
		} catch (Exception e) {
			// TODO: handle exception
			// Log.i("UploadServer", e.getMessage());
			if (dialog != null)
				dialog.dismiss();
			E022.saveErrorLog(e.getMessage(), ac);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	public void ShowDialogFail() {
		try {

			//
			CL_FileInfo cl = new CL_FileInfo(ac);
			cl.updateTR_FileInfoFlag(FireStationCode, CaseCode);
			//
			final Dialog dialogok = new Dialog(ac);
			dialogok.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogok.setContentView(R.layout.layoute34ok);
			// dialog.setCanceledOnTouchOutside(false);
			dialogok.setCancelable(false);
			Button btnOk = (Button) dialogok.findViewById(R.id.btnOK34);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialogok.cancel();
				}
			});

			dialogok.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), ac);
		}
	}

	public void ShowDialogOK() {
		try {

			final Dialog dialogok = new Dialog(ac);
			dialogok.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogok.setContentView(R.layout.layoute034fail);
			// dialog.setCanceledOnTouchOutside(false);
			dialogok.setCancelable(false);
			Button btnOk = (Button) dialogok.findViewById(R.id.btnOK34);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialogok.cancel();
				}
			});
			TextView txtOutbound_No = (TextView) dialogok
					.findViewById(R.id.txtOutbound_No);
			txtOutbound_No.setText(String.valueOf(Outbound_No));
			TextView txtFileSize = (TextView) dialogok
					.findViewById(R.id.txtFileSize);
			txtFileSize.setText(formatSize(totalfilesize));

			TextView txtUnit = (TextView) dialogok.findViewById(R.id.txtUnit);
			if (totalfilesize < 1024)
				txtUnit.setText("KB");
			else
				txtUnit.setText("MB");
			ListView lvMiName = (ListView) dialogok.findViewById(R.id.lvMiName);
			final E034Adapter adapter = new E034Adapter(ac,
					R.layout.layoutrowminame);
			lvMiName.setAdapter(adapter);
			for (int i = 0; i < listHopital.size(); i++) {
				DTO_MedicalInstitution dt = listHopital.get(i);
				adapter.add(dt);

			}
			dialogok.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), ac);
		}
	}

	private String formatSize(int size) {
		if (size < 1024)
			return String.valueOf(size);
		else {
			double res = Utilities.roundTwoDecimals(size / 1024);
			return String.valueOf(res);
		}
	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), ac);
		}
		return log;
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) ac
//					.getApplicationContext().getSystemService(
//							Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Get Ip
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) ac
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), ac);
		}
		return "";
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) ac
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}
