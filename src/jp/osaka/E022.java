﻿package jp.osaka;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class E022 {

	static Context con;
	static boolean ishow = false;

	public static void showErrorDialog(Context contect, String error,
			String code, boolean ex) {
		final Dialog dialog = new Dialog(contect);
		dialog.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					ishow = false;
				}
				return false;
			}
		});
		dialog.setCanceledOnTouchOutside(false);
		con = contect;
		if (ex) {
			String Message = error;
			try {

				if (error.equals(code)) {
					CL_Message m = new CL_Message(con);
					error = m.getMessage(code);
					Message = error;
				} else {
					CL_Message m = new CL_Message(con);
					error = m.getMessage(ContantMessages.Exception);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			// String machineid = getMachineID();
			String phone = getPhoneNumber();
			CL_ErrorLog cl = new CL_ErrorLog(con);
			int maxqno = cl.getMaxTR_ErrorLog(phone);
			CL_ErrorLog cln = new CL_ErrorLog(con);
			cln.createTR_ErrorLog(phone, String.valueOf(maxqno + 1), "1",
					Message);
		} else {
			if (error != null && error.equals(code)) {
				try {
					CL_Message m = new CL_Message(con);
					String meg = m.getMessage(code);
					if (meg != null && !meg.equals(""))
						error = meg;
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

		}
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				ishow = false;
				dialog.cancel();
			}
		});
		// dialog.setTitle("Message");
		// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
		// R.drawable.o);
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null)
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		if (!ishow) {
			ishow = true;
			dialog.show();
		}
	}

	public static void saveErrorLog(String error, Context context) {
		try {

			con = context;
			// String machineid = getMachineID();
			if (error != null && !error.equals("")) {
				String phone = getPhoneNumber();
				CL_ErrorLog cl = new CL_ErrorLog(con);
				int maxqno = cl.getMaxTR_ErrorLog(phone);
				CL_ErrorLog cln = new CL_ErrorLog(con);
				cln.createTR_ErrorLog(phone, String.valueOf(maxqno + 1), "1",
						error);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static String getMachineID() {
		TelephonyManager tManager = (TelephonyManager) con
				.getApplicationContext().getSystemService(
						Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();
		return uid;

	}

	// get phone
	private static String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) con
//					.getSystemService(Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}
}
