﻿package jp.osaka;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.M_UserCount;
import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class E001 extends Activity {

	EditText txtuser;
	EditText txtpass;
	boolean isOk = false;
	// Bug 158 start 20121019
	ProgressDialog p;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// System.exit(0);

	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("currentvalue");
			if (key.equals("1")) {

				Intent myinten = new Intent(E001.this, E002.class);
				startActivityForResult(myinten, 0);
				exitProccess();
				finish();
			} else if (key.equals("2")) {
				E022.showErrorDialog(E001.this, ContantMessages.NotInternet2,
						ContantMessages.NotInternet2, false);
				txtuser.setCursorVisible(true);
				txtpass.setCursorVisible(true);
				exitProccess();
			} else {
				E022.showErrorDialog(E001.this, ContantMessages.LoginFalse,
						ContantMessages.LoginFalse, false);
				txtuser.setCursorVisible(true);
				txtpass.setCursorVisible(true);
				exitProccess();
			}
		}

	};

	void exitProccess() {
		p.dismiss();
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E001.this);
		String title = m.getMessage(ContantMessages.LogIn);
		p.setTitle(title);
		m = new CL_Message(E001.this);
		String mg = m.getMessage(ContantMessages.PleaseWait);
		p.setMessage(mg);
		p.setCanceledOnTouchOutside(false);
		p.show();
	}

	class myrunable implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			String key = "0";
			Cursor c2 = null;
			try {

				String user = txtuser.getText().toString();
				String pass = txtpass.getText().toString();
				CL_User clu = new CL_User(E001.this);
				c2 = clu.fetchLastUSer();
				if (isOnline()) {

					if (!user.equals("") && !pass.equals("")) {
						if (checkLogin(user, pass)) {
							// 20121108 khong ma hoa pass
							// pass = Utilities.hashPassword(pass);
							CL_User cl = new CL_User(E001.this);
							if (c2 != null && c2.getCount() > 0) {
								String id = c2.getString(c2
										.getColumnIndex(M_UserCount.Id));
								cl.updateUser(id, user, pass, "1");
							} else {
								cl.createUser(user, pass, "1");
							}
							key = "1";
						} else {
							key = "0";
						}

					} else {
						key = "0";
					}
				} else {

					// if (c2.getCount() > 0) {
					// key = "1";
					// }
					key = "2";
				}
			} catch (Exception ex) {

			} finally {
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putString("currentvalue", key);
				mg.setData(b);
				handler.sendMessage(mg);
			}

		}

	}

	void startThread() {
		myrunable runable = new myrunable();
		Thread th = new Thread(runable);
		th.start();
	}

	// Bug 158 end
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		setContentView(R.layout.layoute001);
		E022.ishow = false;
		txtuser = (EditText) findViewById(R.id.txtUserName);
		txtpass = (EditText) findViewById(R.id.txtPass);
		// txtuser.setText(getLocalIpAddress());
		
		// test
		txtuser.setText(ContantSystem.HARD_USER);
		txtpass.setText(ContantSystem.HARD_PASS);

		
		final Button btnLogin = (Button) findViewById(R.id.btnLogin);

		btnLogin.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Animation a3 = AnimationUtils.loadAnimation(E001.this,
				// R.anim.demo_scale);
				// v.startAnimation(a3);
				String user = txtuser.getText().toString();
				String pass = txtpass.getText().toString();

				if (!user.equals("") && !pass.equals("")) {

					showProccess();
					startThread();
				} else {
					E022.showErrorDialog(E001.this, ContantMessages.LoginFalse,
							ContantMessages.LoginFalse, false);
					txtuser.setCursorVisible(true);
					txtpass.setCursorVisible(true);

				}
				// Cursor c2 = null;
				// try {
				//
				// btnLogin.setEnabled(false);
				// txtuser.setCursorVisible(false);
				// txtpass.setCursorVisible(false);
				// String user = txtuser.getText().toString();
				// String pass = txtpass.getText().toString();
				// CL_User clu = new CL_User(E001.this);
				// c2 = clu.fetchLastUSer();
				// if (isOnline()) {
				//
				// if (!user.equals("") && !pass.equals("")) {
				//
				// pass = Utilities.hashPassword(pass);
				// CL_User cl = new CL_User(E001.this);
				// if (c2 != null && c2.getCount() > 0) {
				// String id = c2.getString(c2
				// .getColumnIndex(M_UserCount.Id));
				// cl.updateUser(id, user, pass, "1");
				// } else {
				// cl.createUser(user, pass, "1");
				// }
				// if (!isOk) {// bug 158
				// isOk = true;
				// Intent myinten = new Intent(v.getContext(),
				// E002.class);
				// // Bundle b = new Bundle();
				// // b.putInt("LoadLast", 1);
				// // myinten.putExtras(b);
				// finish();
				// startActivityForResult(myinten, 0);
				// }
				//
				// } else {
				//
				// E022.showErrorDialog(E001.this,
				// ContantMessages.LoginFalse,
				// ContantMessages.LoginFalse, false);
				// txtuser.setCursorVisible(true);
				// txtpass.setCursorVisible(true);
				//
				// }
				// } else {
				//
				// if (c2.getCount() > 0) {
				// finish();
				// Intent myinten = new Intent(v.getContext(),
				// E002.class);
				// startActivityForResult(myinten, 0);
				// }
				// }
				//
				// } catch (Exception ex) {
				// } finally {
				// if (c2 != null)
				// c2.close();
				// btnLogin.setEnabled(true);
				//
				// }
			}
		});
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public boolean checkLogin(String UserName, String Password) {
		// myadapter.clear();
		boolean result = false;
		try {
			TrustManagerManipulator.allowAllSSL();
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.authenticateEMSUnit;
			String URL = ContantSystem.Endpoint;
			String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			String userencoded = "";
			String hashpass = "";
			try {
				userencoded = URLEncoder.encode(UserName, "UTF-8");
				hashpass = URLEncoder.encode(Password, "UTF-8");
				// String de= URLDecoder.decode(encoded, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				// E022.showErrorDialog(E001.this, e1.getMessage(), "E001",
				// true);
				result = false;
				E022.saveErrorLog(e1.getMessage(), E001.this);
			}
			request.addProperty("user", userencoded);
			// Khong ma hoa 20121108
			// String hashpass = Utilities.hashPassword(Password);

			request.addProperty("password", hashpass);
			String phone = getPhoneNumber();
			request.addProperty("terminotelno", phone);
			// Param log 20130103
			CL_User cl = new CL_User(E001.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS,
					getResources().getString(R.string.txtTitleE001), user,
					getResources().getString(R.string.txtTitleE001));
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);

			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL);

			try {
				androidhttpTranport.call(SOAP_ACTIONS, envelope);
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				// E022.showErrorDialog(E001.this, e3.getMessage(), "E001",
				// true);
				E022.saveErrorLog(e3.getMessage(), E001.this);
				result = false;
			} catch (XmlPullParserException e3) {
				// TODO Auto-generated catch block
				// E022.showErrorDialog(E001.this, e3.getMessage(), "E001",
				// true);
				E022.saveErrorLog(e3.getMessage(), E001.this);
				result = false;
			}
			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
				if (responseBody != null) {
					String t = responseBody.toString();
					if (!t.equals("")) {
						result = true;
						JSONObject jsonRes = new JSONObject(t);
						// JSONArray jArray=
						// jsonRes.g.getJSONArray("ms_emsunitterminalmanagement");
						JSONObject json = jsonRes
								.getJSONObject("ms_emsunitterminalmanagement");
						ContentValues values = new ContentValues();
						values.put("TerminalNo",
								json.getString("TerminalNo".toLowerCase()));
						values.put("MachineID",
								json.getString("MachineID".toLowerCase()));
						values.put("EMSUnitCode",
								json.getString("EMSUnitCode".toLowerCase()));
						values.put("TerminalTelNo",
								json.getString("TerminalTelNo".toLowerCase()));
						if (!json.isNull("Start_DATETIME".toLowerCase()))
							values.put("Start_DATETIME", json
									.getString("Start_DATETIME".toLowerCase()));
						else
							values.putNull("Start_DATETIME");
						if (!json.isNull("End_DATETIME".toLowerCase()))
							values.put("End_DATETIME", json
									.getString("End_DATETIME".toLowerCase()));
						else
							values.putNull("End_DATETIME");
						if (!json.isNull("SortNo".toLowerCase()))
							values.put(BaseCount.SortNo, json
									.getString(BaseCount.SortNo.toLowerCase()));
						else
							values.put(BaseCount.SortNo, 0);
						values.put(BaseCount.DeleteFLG, json
								.getString(BaseCount.DeleteFLG.toLowerCase()));
						values.put(BaseCount.Update_DATETIME, json
								.getString(BaseCount.Update_DATETIME
										.toLowerCase()));
						if (!json.isNull("JianNo_Initial".toLowerCase()))
							values.put("JianNo_Initial", json
									.getString("JianNo_Initial".toLowerCase()));
						values.put("JianNo_InitialFLG", json
								.getString("JianNo_InitialFLG".toLowerCase()));
						values.put("PlaySoundOnDisaster", json
								.getString("PlaySoundOnDisaster".toLowerCase()));
						values.put("PlaySoundOnNotification", json
								.getString("PlaySoundOnNotification"
										.toLowerCase()));
						if (!json.isNull("AmbulanceSpeed".toLowerCase()))
							values.put("AmbulanceSpeed", json
									.getString("AmbulanceSpeed".toLowerCase()));
						if (!json.isNull("DefaultPictureSizeHeight"
								.toLowerCase()))
							values.put("DefaultPictureSizeHeight", json
									.getString("DefaultPictureSizeHeight"
											.toLowerCase()));
						if (!json.isNull("DefaultPictureSizeWidth"
								.toLowerCase()))
							values.put("DefaultPictureSizeWidth", json
									.getString("DefaultPictureSizeWidth"
											.toLowerCase()));
						if (!json.isNull("DefaultVideQuality".toLowerCase()))
							values.put("DefaultVideQuality", json
									.getString("DefaultVideQuality"
											.toLowerCase()));
						CL_User us = new CL_User(E001.this);
						us.DeleteMS_EMSUnitTerminalManagement();
						us = new CL_User(E001.this);
						us.createMS_EMSUnitTerminalManagement(values);
					}
				}
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// E022.showErrorDialog(E001.this, e2.getMessage(), "E001",
				// true);
				E022.saveErrorLog(e2.getMessage(), E001.this);
				result = false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			// E022.showErrorDialog(E001.this, e.getMessage(), "E001", true);
			E022.saveErrorLog(e.getMessage(), E001.this);
			result = false;
		}
		return result;
	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), E001.this);
		}
		return log;
	}

	// Get Ip
	// public String getLocalIpAddress() {
	// try {
	// for (Enumeration<NetworkInterface> en = NetworkInterface
	// .getNetworkInterfaces(); en.hasMoreElements();) {
	// NetworkInterface intf = en.nextElement();
	// for (Enumeration<InetAddress> enumIpAddr = intf
	// .getInetAddresses(); enumIpAddr.hasMoreElements();) {
	// InetAddress inetAddress = enumIpAddr.nextElement();
	// if (!inetAddress.isLoopbackAddress()) {
	// return inetAddress.getHostAddress().toString();
	// }
	// }
	// }
	// } catch (SocketException e) {
	// E022.saveErrorLog(e.getMessage(),E001.this);
	// }
	// return "";
	// }
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), E001.this);
		}
		return "";
	}

}
