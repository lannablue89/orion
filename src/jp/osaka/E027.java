﻿package jp.osaka;

import java.util.ArrayList;
import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class E027 extends Activity implements LocationListener, OnTouchListener {
	Dialog dialoge;
	ListView list;
	MyAdapter mydapter;
	static String FireStationCode;
	static String CaseCode;
	static String ContactCount;
	static int row_num = 0;
	String SHCACode;
	ArrayList<DTO_ContactResultRecord> listItem;
	double lat = 0;
	double longi = 0;
	String destition = "";

	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";

	LocationManager locationManager;
	Location locationA;
	String tower;
	ProgressDialog p;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	int numberitem = 0;
	int numberDisplay = 0;
	int limit = 4;
	boolean refesh = true;// Q065
	int lastPosition = 0;
	int topPostion = 0;
	boolean isShowPopup = false;
	String codeIdSelect = "";
	int mycountId = 1;
	String phonecall = "";
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E027.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			Bundle b = msg.getData();
			String key = b.getString("Key");

			DTO_ContactResultRecord dt = new DTO_ContactResultRecord();
			dt = (DTO_ContactResultRecord) b.getSerializable("DTO");
			if (dt != null)
				listItem.add(dt);

			if (key.equals("1")) {
				sortList();
				loadData();
				exitProccess();
				if (mydapter != null && mydapter.getCount() < 1) {
					E022.showErrorDialog(E027.this, ContantMessages.NotData,
							ContantMessages.NotData, false);
				}
			}
		}

	};

	void exitProccess() {
		p.dismiss();
		if (lastPosition > 0 && mydapter.getCount() >= lastPosition) {
			// list.setSelection(lastPosition);
			list.setSelectionFromTop(lastPosition, topPostion);
		}
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E027.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E027.this);
		String mg = m.getMessage(ContantMessages.DataLoading);
		p.setMessage(mg);
		p.setCanceledOnTouchOutside(false);
		p.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute027);

		Cursor cur = null;
		Cursor cur2 = null;
		refesh = false;
		numberitem = 0;
		lastPosition = 0;
		listItem = new ArrayList<DTO_ContactResultRecord>();
		try {
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");

			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}

			numberDisplay = limit;
			mydapter = new MyAdapter(E027.this, R.layout.layoutgrid027);
			list = (ListView) findViewById(R.id.list013);
			list.setAdapter(mydapter);
			list.setOnTouchListener(this);
			list.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					lastPosition = list.getFirstVisiblePosition();
					View v = list.getChildAt(0);
					topPostion = (v == null) ? 0 : v.getTop();
					numberDisplay += limit;
					loadData();
				}
			});

			Button btnBack = (Button) findViewById(R.id.btnE027Back);
			btnBack.setOnTouchListener(this);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten = new Intent(v.getContext(), E005.class);
					myinten.putExtras(getIntent().getExtras());
					startActivityForResult(myinten, 0);
				}
			});

			TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			txtHeader.setOnTouchListener(this);
			// txtHeader.setText("病院検索結果(3次)");

			if (mydapter != null && mydapter.getCount() > 0) {
			} else
				showProccess();

			startThread();
			// createMenu();
			LinearLayout laymain = (LinearLayout) findViewById(R.id.layout_main);
			laymain.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E027", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();

		}
	}

	private void refeshScreen() {
		Cursor cur = null;
		Cursor cur2 = null;
		numberitem = 0;
		if (mydapter != null)
			mydapter.clear();
		listItem = new ArrayList<DTO_ContactResultRecord>();
		try {

			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}
			showProccess();
			startThread();

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E027", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();

		}
	}

	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;
			Cursor cur2 = null;
			Cursor cur3 = null;
			try {
				Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
						E027.this);
				cur = cl.getTR_ContactResultRecord027(FireStationCode, CaseCode);
				if (cur != null && cur.getCount() > 0) {
					int idx = 0;
					do {
						String micode = cur.getString(cur
								.getColumnIndex("MICode"));
						String contact_datetime = cur.getString(cur
								.getColumnIndex("Contact_DATETIME"));
						String contactresult_cd = cur.getString(cur
								.getColumnIndex("ContactResult_CD"));
						String contactresult_text = cur.getString(cur
								.getColumnIndex("ContactResult_TEXT"));
						String update_datetime = cur.getString(cur
								.getColumnIndex("Update_DATETIME"));
						int contactcount = cur.getInt(cur
								.getColumnIndex("ContactCount"));
						String miname_kanji = cur.getString(cur
								.getColumnIndex("MIName_Kanji"));
						String contactresult_name = cur.getString(cur
								.getColumnIndex("ContactResult_Name"));
						String mitype = cur.getString(cur
								.getColumnIndex("MIType"));
						DTO_ContactResultRecord dt = new DTO_ContactResultRecord();
						dt.setMicode(micode);
						dt.setContact_datetime(contact_datetime);
						dt.setContactcount(contactcount);
						dt.setContactresult_cd(contactresult_cd);
						dt.setContactresult_text(contactresult_text);
						dt.setUpdate_datetime(update_datetime);
						dt.setMiname_kanji(miname_kanji);
						dt.setContactresult_name(contactresult_name);
						dt.setMitype(mitype);
						idx++;
						Message mg = handler.obtainMessage();
						Bundle b = new Bundle();
						b.putSerializable("DTO", dt);
						if (idx == cur.getCount()) {
							b.putString("Key", "1");
						} else {
							b.putString("Key", "0");
						}
						mg.setData(b);
						handler.sendMessage(mg);

					} while (cur.moveToNext());

				} else {
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					b.putSerializable("DTO", null);
					b.putString("Key", "1");
					mg.setData(b);
					handler.sendMessage(mg);

				}
			} catch (Exception e) {
				// TODO: handle exception
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);
			} finally {
				if (cur3 != null)
					cur3.close();
				if (cur2 != null)
					cur2.close();
				if (cur != null)
					cur.close();
			}
		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	void sortAdapter() {
		try {
			if (mydapter != null && mydapter.getCount() > 1) {
				for (int i = 0; i < mydapter.getCount() - 1; i++) {
					DTO_ContactResultRecord dti = new DTO_ContactResultRecord();
					dti = mydapter.getItem(i);
					for (int j = i + 1; j < mydapter.getCount(); j++) {
						DTO_ContactResultRecord dtj = new DTO_ContactResultRecord();
						dtj = mydapter.getItem(j);
						String micode = dti.getMicode();
						String contact_datetime = dti.getContact_datetime();
						String contactresult_cd = dti.getContactresult_cd();
						String contactresult_text = dti.getContactresult_text();
						String update_datetime = dti.getUpdate_datetime();
						int contactcount = dti.getContactcount();
						String miname_kanji = dti.getMiname_kanji();
						String contactresult_name = dti.getContactresult_name();
						String mitype = dti.getMitype();

						dti.setMicode(dtj.getMicode());
						dti.setContact_datetime(dtj.getContact_datetime());
						dti.setContactresult_cd(dtj.getContactresult_cd());
						dti.setContactresult_text(dtj.getContactresult_text());
						dti.setUpdate_datetime(dtj.getUpdate_datetime());
						dti.setContactcount(dtj.getContactcount());
						dti.setMiname_kanji(dtj.getMiname_kanji());
						dti.setContactresult_name(dtj.getContactresult_name());
						dti.setMitype(dtj.getMitype());
						dti.setContactresult_name(dtj.getContactresult_name());

						dtj.setMicode(micode);
						dtj.setContact_datetime(contact_datetime);
						dtj.setContactresult_cd(contactresult_cd);
						dtj.setContactresult_text(contactresult_text);
						dtj.setUpdate_datetime(update_datetime);
						dtj.setContactcount(contactcount);
						dtj.setMiname_kanji(miname_kanji);
						dtj.setContactresult_name(contactresult_name);
						dtj.setMitype(mitype);
						mydapter.notifyDataSetChanged();
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void loadData() {
		if (mydapter.getCount() > 0) {
			DTO_ContactResultRecord dto = new DTO_ContactResultRecord();
			dto = mydapter.getItem(mydapter.getCount() - 1);
			if (dto == null) {
				mydapter.remove(dto);
			}
		}
		for (int i = 0; i < listItem.size(); i++) {
			if (i == numberDisplay) {
				break;
			}
			if (i >= numberitem) {
				numberitem++;
				DTO_ContactResultRecord dti = new DTO_ContactResultRecord();
				dti = listItem.get(i);
				mydapter.add(dti);
			}

		}
		if (listItem.size() > numberitem) {
			mydapter.add(null);
		}
		mydapter.notifyDataSetChanged();
	}

	private void sortList() {
		try {
			if (listItem != null && listItem.size() > 1) {
				for (int i = 0; i < listItem.size() - 1; i++) {
					DTO_ContactResultRecord dti = new DTO_ContactResultRecord();
					dti = listItem.get(i);
					for (int j = i + 1; j < listItem.size(); j++) {
						DTO_ContactResultRecord dtj = new DTO_ContactResultRecord();
						dtj = listItem.get(j);
						String micode = dti.getMicode();
						String contact_datetime = dti.getContact_datetime();
						String contactresult_cd = dti.getContactresult_cd();
						String contactresult_text = dti.getContactresult_text();
						String update_datetime = dti.getUpdate_datetime();
						int contactcount = dti.getContactcount();
						String miname_kanji = dti.getMiname_kanji();
						String contactresult_name = dti.getContactresult_name();
						String mitype = dti.getMitype();

						dti.setMicode(dtj.getMicode());
						dti.setContact_datetime(dtj.getContact_datetime());
						dti.setContactresult_cd(dtj.getContactresult_cd());
						dti.setContactresult_text(dtj.getContactresult_text());
						dti.setUpdate_datetime(dtj.getUpdate_datetime());
						dti.setContactcount(dtj.getContactcount());
						dti.setMiname_kanji(dtj.getMiname_kanji());
						dti.setContactresult_name(dtj.getContactresult_name());
						dti.setMitype(dtj.getMitype());

						dtj.setMicode(micode);
						dtj.setContact_datetime(contact_datetime);
						dtj.setContactresult_cd(contactresult_cd);
						dtj.setContactresult_text(contactresult_text);
						dtj.setUpdate_datetime(update_datetime);
						dtj.setContactcount(contactcount);
						dtj.setMiname_kanji(miname_kanji);
						dtj.setContactresult_name(contactresult_name);
						dtj.setMitype(mitype);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public class MyAdapter extends ArrayAdapter<DTO_ContactResultRecord> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.layoutgrid027, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}
			TextView txtGrid13Num = myw.getNum();
			TextView txtGrid13STT = myw.getSTT();
			TextView txtGrid13ResultName = myw.getPhone();
			TextView txtGrid13Name = myw.getName();
			TextView txtGrid13Note = myw.getNote();
			TextView cb_option = myw.getcb_option();
			LinearLayout row = myw.getRow();
			if (this.getItem(position) != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(0, 0, 0, 0);
				row.setLayoutParams(param);
				row.setBackgroundResource(R.drawable.bggrid13c);
				txtGrid13Num.setVisibility(View.VISIBLE);
				txtGrid13STT.setVisibility(View.VISIBLE);
				txtGrid13ResultName.setVisibility(View.VISIBLE);
				txtGrid13Name.setVisibility(View.VISIBLE);
				txtGrid13Note.setVisibility(View.VISIBLE);
				cb_option.setVisibility(View.VISIBLE);

				if (this.getItem(position).getMitype() != null
						&& this.getItem(position).getMitype().equals("5")) {
					txtGrid13STT.setText("");
					txtGrid13STT.setBackgroundResource(R.drawable.headerrow13c);
				} else if (this.getItem(position).getMitype() != null
						&& this.getItem(position).getMitype().equals("3")) {

					txtGrid13STT.setText(this.getItem(position).getMitype());
					txtGrid13STT.setBackgroundResource(R.drawable.headerrowred);
				} else if (this.getItem(position).getMitype() != null
						&& this.getItem(position).getMitype().equals("2")) {

					txtGrid13STT.setText(this.getItem(position).getMitype());
					txtGrid13STT.setBackgroundResource(R.drawable.headerrowe13);
				} else {
					txtGrid13STT.setVisibility(View.INVISIBLE);
				}

				txtGrid13Num.setText(this.getItem(position).getContactcount()
						+ "回");
				txtGrid13ResultName.setText(this.getItem(position)
						.getContactresult_name());
				txtGrid13Name.setText(this.getItem(position).getMiname_kanji());
				if (this.getItem(position).getContactresult_cd()
						.equals("99999")) {
					txtGrid13Note.setText(this.getItem(position)
							.getContactresult_text());
				} else {
					txtGrid13Note.setText("");
				}
				if (this.getItem(position).getContactresult_cd()
						.equals("00001")) {
					cb_option.setTextColor(Color.parseColor("#DDD9C3"));
				} else {
					cb_option.setTextColor(Color.parseColor("#FFFFFF"));
				}
				cb_option.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						DTO_ContactResultRecord dto = new DTO_ContactResultRecord();
						dto = listItem.get(position);
						if (dto.getContactresult_cd().equals("00001")) {
						} else {
							if (!isShowPopup) {

								dto = listItem.get(position);
								ContactCount = dto.getContactcount() + "";

								isShowPopup = true;
								showPopup();
							}
						}
					}
				});
			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				row.setLayoutParams(param);
				txtGrid13Num.setVisibility(View.INVISIBLE);
				txtGrid13STT.setVisibility(View.INVISIBLE);
				txtGrid13ResultName.setVisibility(View.INVISIBLE);
				txtGrid13Name.setVisibility(View.INVISIBLE);
				txtGrid13Note.setVisibility(View.INVISIBLE);
				cb_option.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow);
			}
			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView txtGrid13Num = null;
		TextView txtGrid13STT = null;
		TextView txtGrid13Name = null;
		TextView txtGrid13ResultName = null;
		TextView txtGrid13Note = null;
		TextView cb_option = null;
		LinearLayout layoutrow = null;

		ViewWraper(View base) {

			this.base = base;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}

		TextView getNum() {
			if (txtGrid13Num == null) {
				txtGrid13Num = (TextView) base.findViewById(R.id.txtGrid13Num);
			}
			return txtGrid13Num;
		}

		TextView getSTT() {
			if (txtGrid13STT == null) {
				txtGrid13STT = (TextView) base.findViewById(R.id.txtGrid13STT);
			}
			return txtGrid13STT;
		}

		TextView getName() {
			if (txtGrid13Name == null) {
				txtGrid13Name = (TextView) base
						.findViewById(R.id.txtGrid13Name);
			}
			return txtGrid13Name;
		}

		TextView getPhone() {
			if (txtGrid13ResultName == null) {
				txtGrid13ResultName = (TextView) base
						.findViewById(R.id.txtGrid13ResultName);
			}
			return txtGrid13ResultName;
		}

		TextView getNote() {
			if (txtGrid13Note == null) {
				txtGrid13Note = (TextView) base
						.findViewById(R.id.txtGrid13Note);
			}
			return txtGrid13Note;
		}

		TextView getcb_option() {
			if (cb_option == null) {
				cb_option = (TextView) base.findViewById(R.id.cb_option);
			}
			return cb_option;
		}
	}

	public void showPopup() {
		try {
			dialoge = new Dialog(E027.this);
			dialoge.setOnKeyListener(new OnKeyListener() {

				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						isShowPopup = false;
					}
					return false;
				}
			});
			dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialoge.setContentView(R.layout.layoutdialoge027);
			dialoge.setCanceledOnTouchOutside(false);

			Button cb_update = (Button) dialoge.findViewById(R.id.cb_update);
			Button cb_cancel = (Button) dialoge.findViewById(R.id.cb_cancel);

			Button btnE027Back = (Button) dialoge
					.findViewById(R.id.btnE027Back);
			btnE027Back.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					isShowPopup = false;
					ContactCount = "";
					dialoge.cancel();
				}
			});

			cb_update.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showE315update();
					isShowPopup = false;
					showE015(false);
				}
			});
			cb_cancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					showE027delete();
					isShowPopup = false;
				}
			});
			dialoge.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E027.this, e.getMessage(), "E027", true);
		}
	}

	boolean chk15 = false;
	CheckBox chk;

	private void showE015(boolean check) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute015);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		TextView textViewcontactcount = (TextView) dialog
				.findViewById(R.id.textViewcontactcount);
		textViewcontactcount.setText(ContactCount);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String note = "";
				if (codeIdSelect.equals(BaseCount.Other)) {
					EditText txtother = (EditText) dialog.findViewById(99999);
					note = txtother.getText().toString();
				}
				Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
						E027.this);
				long res = cl.updateTR_ContactResultRecordE027(FireStationCode,
						CaseCode, ContactCount, codeIdSelect, note);
				if (res >= 0) {
					if (codeIdSelect.equals("00001")) {
						// E015 up = new E015();
						// JSONObject json = new JSONObject();
						// json.put("FireStationCode", FireStationCode);
						// json.put("CaseCode", CaseCode);
						// json.put("ContactCount", ContactCount + 1);
						// json.put("MICode", MICode);
						// json.put("ContactResult_CD", codeIdSelect);
						// json.put("ContactResult_TEXT", note);
						// json.put("Flag", "0");
						// String val = json.toString();
						// up.upLoadDataToServer(val, E027.this);
						E015_0001 up = new E015_0001();
						up.UpdateLoad(E027.this, FireStationCode, CaseCode);
					}
					MICode = BaseCount.Other;
					dialog.cancel();
					dialoge.cancel();
					if (codeIdSelect.equals("00001")) {
						Intent myinten = new Intent(v.getContext(), E005.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString(BaseCount.IsOldData, "1");
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						finish();
					}
				}
				refeshScreen();
			}
		});

		Cl_ContactResultRecord cl_select = new Cl_ContactResultRecord(E027.this);
		Cursor cur_select = cl_select.getTR_ContactResultRecord027_Select(
				FireStationCode, CaseCode, ContactCount);
		String code_select = cur_select.getString(cur_select
				.getColumnIndex("ContactResult_CD"));
		String code_name = cur_select.getString(cur_select
				.getColumnIndex("ContactResult_TEXT"));
		Cl_Code code = new Cl_Code(E027.this);
		Cursor cor = code.getMS_Code("00002");
		if (cor != null && cor.getCount() > 0) {
			mycountId = 1;
			RadioGroup group = (RadioGroup) dialog
					.findViewById(R.id.radioGroup1);
			LinearLayout layout = (LinearLayout) dialog
					.findViewById(R.id.layoutE73);
			final HashMap<String, String> listItem = new HashMap<String, String>();
			do {
				final String codeId = cor.getString(cor
						.getColumnIndex("CodeID"));
				String codeName = cor.getString(cor.getColumnIndex("CodeName"));
				listItem.put(codeId, codeName);
				LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				paramCheck.setMargins(0, 10, 0, 10);
				RadioButton rdi = new RadioButton(this);
				rdi.setLayoutParams(paramCheck);
				rdi.setText(codeName);
				rdi.setTextColor(Color.BLACK);
				rdi.setButtonDrawable(R.drawable.radio);
				rdi.setTextSize(24);
				rdi.setId(mycountId);
				mycountId++;

				if (check || (!check && !codeId.equals("00001"))) {
					rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								if (!codeId.equals("00001")) {
									RadioButton radio = (RadioButton) dialog
											.findViewById(1);
									radio.setChecked(false);
									chk.setChecked(false);
									chk.setEnabled(false);
								} else {
									chk.setEnabled(true);
								}
								codeIdSelect = codeId;
								if (listItem.containsKey(BaseCount.Other)) {
									EditText txtother = (EditText) dialog
											.findViewById(99999);
									txtother.setEnabled(false);
									if (codeIdSelect.equals("99999")) {
										txtother.setEnabled(true);
									} else {
										txtother.setText("");
									}
								}
							}
						}
					});
				}
				if (codeId.equals("00001")) {
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout lay = new LinearLayout(this);
					lay.setLayoutParams(paramrow);
					lay.setOrientation(LinearLayout.HORIZONTAL);
					chk = new CheckBox(this);
					chk.setLayoutParams(paramrow);
					chk.setTextColor(Color.BLACK);
					chk.setText("三次コーディネート");
					chk.setTextSize(16);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub
							chk15 = arg1;
						}
					});
					lay.addView(rdi);
					lay.addView(chk);
					group.addView(lay);
				} else {
					group.addView(rdi);
				}
				if (codeId.equals("00001") && !check) {
					rdi.setClickable(false);
					chk.setEnabled(false);
				} else if (codeId.equals("00001")) {
					rdi.setChecked(true);
					codeIdSelect = codeId;
				} else {
					chk.setEnabled(false);
				}
				if (codeId.equals(code_select) && !check) {
					rdi.setChecked(true);
					codeIdSelect = codeId;
				}
				if (codeId.equals("99999")) {

					LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtext.setMargins(0, 5, 20, 0);
					EditText txtOther = new EditText(this);
					txtOther.setLayoutParams(paramtext);
					txtOther.setBackgroundResource(R.drawable.inpute7);
					txtOther.setTextColor(Color.BLACK);
					txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
							200) });
					txtOther.setSingleLine(true);
					txtOther.setEnabled(false);
					txtOther.setId(99999);
					layout.addView(txtOther);
				}
			} while (cor.moveToNext());
			if (codeIdSelect.equals("99999")) {
				EditText txtother = (EditText) dialog.findViewById(99999);
				txtother.setEnabled(true);
				txtother.setText(code_name);
			}
			dialog.show();
		}

	}

	private void showE027delete() {
		Cursor cur = null;
		try {
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute016);
			dialog.setCanceledOnTouchOutside(false);
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
					dialoge.cancel();
					ContactCount = "";
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
							E027.this);
					cl.deleteTR_ContactResultRecord(FireStationCode, CaseCode,
							ContactCount);
					ContactCount = "";
					refeshScreen();
					dialog.cancel();
					dialoge.cancel();
				}
			});
			TextView lblMessageErr = (TextView) dialog
					.findViewById(R.id.lblMessageErr);
			CL_Message me = new CL_Message(this);
			String mg = me.getMessage(ContantMessages.DeleteE027);
			lblMessageErr.setText(mg);
			TextView textView1 = (TextView) dialog.findViewById(R.id.textView1);
			textView1.setText("確認");
			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E027.this, e.getMessage(), "E017", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void loaddataAgain() {
		Cursor cur2 = null;
		Cursor cur3 = null;
		try {
			if (mydapter != null) {
				for (int i = 0; i < mydapter.getCount(); i++) {
					DTO_ContactResultRecord dt = new DTO_ContactResultRecord();
					dt = mydapter.getItem(i);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur2 != null)
				cur2.close();
			if (cur3 != null)
				cur3.close();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		this.loaddataAgain();
		mydapter.notifyDataSetChanged();
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// locationManager.removeUpdates(this); //Bug 166 20121025
		th.setRunning(false);
		bt.setRunning(false);
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(E027.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// locationManager.requestLocationUpdates(tower, 500, 1, this);//Bug 166
		// 20121025
		// SharedPreferences sh = this.getSharedPreferences("app",
		// this.MODE_PRIVATE);
		String syndb = getStatusSysDB();// sh.getString("SysDB", "0");
		if (refesh && syndb.equals("0")) {
			// onCreate(null);
			refeshScreen();
		}
		refesh = true;// Q065 20121027
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
