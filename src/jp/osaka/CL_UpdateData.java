﻿package jp.osaka;

import java.util.Calendar;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_AreaCount;
import jp.osaka.ContantTable.MS_SecondaryHealthCareAreaCount;
import jp.osaka.ContantTable.MS_SettingCount;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_UpdateData {

	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_UpdateData(Context context) {
		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public String updateDataBase(JSONObject json, String username) {
		String datetime = "";
		boolean newsms = false;
		database.beginTransaction();
		try {

			JSONObject json1 = json.getJSONObject("AllTable");
			datetime = json.getString("DateTime");
			// MS_SecondaryHealthCareArea

			JSONArray jArray;

			if (!json1.isNull("ms_secondaryhealthcarearea")) {
				jArray = json1.getJSONArray("ms_secondaryhealthcarearea");

				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);
					String SHCACode = e
							.getString(MS_SecondaryHealthCareAreaCount.SHCACode
									.toLowerCase());
					String SHCAName = e
							.getString(MS_SecondaryHealthCareAreaCount.SHCAName
									.toLowerCase());
					String SortNo = e
							.getString(MS_SecondaryHealthCareAreaCount.SortNo
									.toLowerCase());
					String DeleteFLG = e
							.getString(MS_SecondaryHealthCareAreaCount.DeleteFLG
									.toLowerCase());
					String updatdate = e
							.getString(MS_SecondaryHealthCareAreaCount.Update_DATETIME
									.toLowerCase());
					ContentValues values = new ContentValues();
					values.put(MS_SecondaryHealthCareAreaCount.SHCACode,
							SHCACode);
					values.put(MS_SecondaryHealthCareAreaCount.SHCAName,
							SHCAName);
					values.put(MS_SecondaryHealthCareAreaCount.SortNo, SortNo);
					values.put(MS_SecondaryHealthCareAreaCount.DeleteFLG,
							DeleteFLG);
					values.put(MS_SecondaryHealthCareAreaCount.Update_DATETIME,
							updatdate);
					database.replace(MS_SecondaryHealthCareAreaCount.TableName,
							null, values);
				}
			}
			// MS_Area

			if (!json1.isNull(MS_AreaCount.TableName.toLowerCase())) {
				jArray = json1.getJSONArray(MS_AreaCount.TableName
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					String AreaName = e.getString(MS_AreaCount.AreaName
							.toLowerCase());
					String AreaCode = e.getString(MS_AreaCount.AreaCode
							.toLowerCase());
					String SHCACode = e.getString(MS_AreaCount.SHCACode
							.toLowerCase());
					String SortNo = e.getString(MS_AreaCount.SortNo
							.toLowerCase());
					String DeleteFLG = e.getString(MS_AreaCount.DeleteFLG
							.toLowerCase());
					String updatdate = e.getString(MS_AreaCount.Update_DATETIME
							.toLowerCase());
					ContentValues values = new ContentValues();
					values.put(MS_AreaCount.AreaCode, AreaCode);
					values.put(MS_AreaCount.AreaName, AreaName);
					values.put(MS_AreaCount.SHCACode, SHCACode);
					values.put(MS_AreaCount.SortNo, SortNo);
					values.put(MS_AreaCount.DeleteFLG, DeleteFLG);
					values.put(MS_AreaCount.Update_DATETIME, updatdate);
					database.replace(MS_AreaCount.TableName, null, values);
				}
			}
			// MS_FireStation

			if (!json1.isNull("MS_FireStation".toLowerCase())) {
				jArray = json1.getJSONArray("MS_FireStation".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("FireStationName",
							e.getString("FireStationName".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put("AreaCode",
							e.getString("AreaCode".toLowerCase()));
					values.put("ReportFireStationCode",
							e.getString("ReportFireStationCode".toLowerCase()));
					values.put("TerminalPassword",
							e.getString("TerminalPassword".toLowerCase()));
					values.put("Latitude",
							e.getString("Latitude".toLowerCase()));
					values.put("Longitude",
							e.getString("Longitude".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					if (!e.isNull("JianNo_Initial".toLowerCase()))
						values.put("JianNo_Initial",
								e.getString("JianNo_Initial".toLowerCase()));
					else
						values.putNull("JianNo_Initial");
					values.put("FDMAUpload_Initial_1",
							e.getString("FDMAUpload_Initial_1".toLowerCase()));
					values.put("FDMAUpload_Initial_2",
							e.getString("FDMAUpload_Initial_2".toLowerCase()));
					values.put("FDMAUpload_Initial_3",
							e.getString("FDMAUpload_Initial_3".toLowerCase()));
					values.put("FDMAUpload_Initial_4",
							e.getString("FDMAUpload_Initial_4".toLowerCase()));
					database.replace("MS_FireStation", null, values);
				}
			}
			// MS_Instructor

			if (!json1.isNull("MS_Instructor".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Instructor".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("MICode", e.getString("MICode".toLowerCase()));
					// 20121019 bug 178
					// values.put("MITelNo",
					// e.getString("MITelNo".toLowerCase()));
					if (!e.isNull("MITelNo".toLowerCase())
							&& e.getString("MITelNo".toLowerCase()).trim() != null
							&& !e.getString("MITelNo".toLowerCase()).equals(""))
						values.put("MITelNo",
								e.getString("MITelNo".toLowerCase()));
					else
						values.putNull("MITelNo");
					//
					values.put("MIDetailName",
							e.getString("MIDetailName".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Instructor", null, values);
				}
			}
			// MS_EMSUnit

			if (!json1.isNull("MS_EMSUnit".toLowerCase())) {
				jArray = json1.getJSONArray("MS_EMSUnit".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("EMSUnitCode",
							e.getString("EMSUnitCode".toLowerCase()));
					values.put("EMSUnitName",
							e.getString("EMSUnitName".toLowerCase()));
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("ReportEMSUnitCode",
							e.getString("ReportEMSUnitCode".toLowerCase()));
					values.put("Password",
							e.getString("Password".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_EMSUnit", null, values);
				}
			}
			// MS_EMSUnitTerminalManagement update 1 row
			// Khong co trong tai lieu dong bo
			// if (!json1.isNull("MS_EMSUnitTerminalManagement".toLowerCase()))
			// {
			// jArray = json1.getJSONArray("MS_EMSUnitTerminalManagement"
			// .toLowerCase());
			// for (int i = 0; i < jArray.length(); i++) {
			//
			// JSONObject e = jArray.getJSONObject(i);
			//
			// ContentValues values = new ContentValues();
			// values.put("TerminalNo",
			// e.getString("TerminalNo".toLowerCase()));
			// values.put("MachineID",
			// e.getString("MachineID".toLowerCase()));
			// values.put("EMSUnitCode",
			// e.getString("EMSUnitCode".toLowerCase()));
			// values.put("TerminalTelNo",
			// e.getString("TerminalTelNo".toLowerCase()));
			// if (!e.isNull("Start_DATETIME".toLowerCase()))
			// values.put("Start_DATETIME",
			// e.getString("Start_DATETIME".toLowerCase()));
			// else
			// values.putNull("Start_DATETIME");
			// if (!e.isNull("End_DATETIME".toLowerCase()))
			// values.put("End_DATETIME",
			// e.getString("End_DATETIME".toLowerCase()));
			// else
			// values.putNull("End_DATETIME");
			// values.put(BaseCount.SortNo,
			// e.getString(BaseCount.SortNo.toLowerCase()));
			// values.put(BaseCount.DeleteFLG,
			// e.getString(BaseCount.DeleteFLG.toLowerCase()));
			// values.put(BaseCount.Update_DATETIME, e
			// .getString(BaseCount.Update_DATETIME.toLowerCase()));
			// if (!e.isNull("JianNo_Initial".toLowerCase()))
			// values.put("JianNo_Initial",
			// e.getString("JianNo_Initial".toLowerCase()));
			// else
			// values.putNull("JianNo_Initial");
			// database.replace("MS_EMSUnitTerminalManagement", null,
			// values);
			// }
			// }
			// MS_MedicalInstitution

			if (!json1.isNull("MS_MedicalInstitution".toLowerCase())) {
				jArray = json1.getJSONArray("MS_MedicalInstitution"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					if (!e.isNull("Opening_DATE".toLowerCase()))
						values.put("Opening_DATE",
								e.getString("Opening_DATE".toLowerCase()));
					else
						values.putNull("Opening_DATE");
					if (!e.isNull("Closing_DATE".toLowerCase()))
						values.put("Closing_DATE",
								e.getString("Closing_DATE".toLowerCase()));
					else
						values.putNull("Closing_DATE");
					values.put("StopFLG", e.getString("StopFLG".toLowerCase()));
					values.put("MIClass", e.getString("MIClass".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put("AreaCode",
							e.getString("AreaCode".toLowerCase()));
					if (!e.isNull("MIName_Kanji".toLowerCase()))
						values.put("MIName_Kanji",
								e.getString("MIName_Kanji".toLowerCase()));
					else
						values.putNull("MIName_Kanji");
					values.put("MIName_Kana",
							e.getString("MIName_Kana".toLowerCase()));
					values.put("PostNo", e.getString("PostNo".toLowerCase()));
					values.put("Address_Kanji",
							e.getString("Address_Kanji".toLowerCase()));
					values.put("Address_Kana",
							e.getString("Address_Kana".toLowerCase()));
					values.put("EMailAddress",
							e.getString("EMailAddress".toLowerCase()));
					if (!e.isNull("TelNo1".toLowerCase()))
						values.put("TelNo1",
								e.getString("TelNo1".toLowerCase()));
					else
						values.putNull("TelNo1");
					if (!e.isNull("TelNo2".toLowerCase()))
						values.put("TelNo2",
								e.getString("TelNo2".toLowerCase()));
					else
						values.putNull("TelNo2");
					if (!e.isNull("TransportationTelNo".toLowerCase()))
						values.put("TransportationTelNo", e
								.getString("TransportationTelNo".toLowerCase()));
					else
						values.putNull("TransportationTelNo");
					values.put("FaxNo", e.getString("FaxNo".toLowerCase()));
					if (!e.isNull("ThirdCoordinateTelNo".toLowerCase()))
						values.put("ThirdCoordinateTelNo",
								e.getString("ThirdCoordinateTelNo"
										.toLowerCase()));
					else
						values.putNull("ThirdCoordinateTelNo");
					values.put("Latitude",
							e.getString("Latitude".toLowerCase()));
					values.put("Longitude",
							e.getString("Longitude".toLowerCase()));
					values.put("EmergencyMIFLG",
							e.getString("EmergencyMIFLG".toLowerCase()));
					values.put("MIType", e.getString("MIType".toLowerCase()));
					values.put("EmergencyCenterFLG",
							e.getString("EmergencyCenterFLG".toLowerCase()));
					values.put("CalamityMIFLG",
							e.getString("CalamityMIFLG".toLowerCase()));
					if (!e.isNull("CCUTelNo".toLowerCase()))
						values.put("CCUTelNo",
								e.getString("CCUTelNo".toLowerCase()));
					else
						values.putNull("CCUTelNo");
					if (!e.isNull("SCUTelNo".toLowerCase()))
						values.put("SCUTelNo",
								e.getString("SCUTelNo".toLowerCase()));
					else
						values.putNull("SCUTelNo");
					if (!e.isNull("HelicopterTelNo".toLowerCase()))
						values.put("HelicopterTelNo",
								e.getString("HelicopterTelNo".toLowerCase()));
					else
						values.putNull("HelicopterTelNo");
					if (!e.isNull("HotLineTelNo".toLowerCase()))
						values.put("HotLineTelNo",
								e.getString("HotLineTelNo".toLowerCase()));
					else
						values.putNull("HotLineTelNo");
					if (!e.isNull("Fdmamicode".toLowerCase()))
						values.put("Fdmamicode",
								e.getString("Fdmamicode".toLowerCase()));
					else
						values.putNull("Fdmamicode");
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put("SortNo", e.getString("SortNo".toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_MedicalInstitution", null, values);
				}
			}
			// MS_KamokuTelNo

			if (!json1.isNull("MS_KamokuTelNo".toLowerCase())) {
				jArray = json1.getJSONArray("MS_KamokuTelNo".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("KamokuCode",
							e.getString("KamokuCode".toLowerCase()));
					values.put("OujyuTelNo",
							e.getString("OujyuTelNo".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_KamokuTelNo", null, values);
				}
			}
			// MS_Kamoku

			if (!json1.isNull("MS_Kamoku".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Kamoku".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("KamokuCode",
							e.getString("KamokuCode".toLowerCase()));
					values.put("KamokuName",
							e.getString("KamokuName".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Kamoku", null, values);
				}
			}
			// MS_MedicalInstitutionClosed

			if (!json1.isNull("MS_MedicalInstitutionClosed".toLowerCase())) {
				jArray = json1.getJSONArray("MS_MedicalInstitutionClosed"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("Closed_MMDD",
							e.getString("Closed_MMDD".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_MedicalInstitutionClosed", null,
							values);
				}
			}
			// MS_ViewingPermission

			if (!json1.isNull("MS_ViewingPermission".toLowerCase())) {
				jArray = json1.getJSONArray("MS_ViewingPermission"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("PermissionFLG",
							e.getString("PermissionFLG".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_ViewingPermission", null, values);
				}
			}
			// MS_MovementTime

			if (!json1.isNull("MS_MovementTime".toLowerCase())) {
				jArray = json1.getJSONArray("MS_MovementTime".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("MovementTimeCode_CD",
							e.getString("MovementTimeCode_CD".toLowerCase()));
					values.put("MovementTimeName",
							e.getString("MovementTimeName".toLowerCase()));
					values.put("TransferFLG",
							e.getString("TransferFLG".toLowerCase()));
					values.put("ControlType",
							e.getString("ControlType".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_MovementTime", null, values);
				}
			}
			// MS_FireStationStandard

			if (!json1.isNull("MS_FireStationStandard".toLowerCase())) {
				jArray = json1.getJSONArray("MS_FireStationStandard"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("StandardClass",
							e.getString("StandardClass".toLowerCase()));
					values.put("StandardCode",
							e.getString("StandardCode".toLowerCase()));
					values.put("StandardBranchCode",
							e.getString("StandardBranchCode".toLowerCase()));
					values.put("StandardBranchName",
							e.getString("StandardBranchName".toLowerCase()));
					if (!e.isNull("ParentStandardClass".toLowerCase()))
						values.put("ParentStandardClass", e
								.getString("ParentStandardClass".toLowerCase()));
					else
						values.putNull("ParentStandardClass");
					if (!e.isNull("ParentStandardCode".toLowerCase()))
						values.put("ParentStandardCode",
								e.getString("ParentStandardCode".toLowerCase()));
					else
						values.putNull("ParentStandardCode");
					if (!e.isNull("ParentStandardBranchCode".toLowerCase()))
						values.put("ParentStandardBranchCode", e
								.getString("ParentStandardBranchCode"
										.toLowerCase()));
					else
						values.putNull("ParentStandardBranchCode");
					values.put("DiseaseFLG",
							e.getString("DiseaseFLG".toLowerCase()));
					values.put("ExternalWoundFLG",
							e.getString("ExternalWoundFLG".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_FireStationStandard", null, values);
				}
			}
			// MS_FireStationStandardSort

			if (!json1.isNull("MS_FireStationStandardSort".toLowerCase())) {
				jArray = json1.getJSONArray("MS_FireStationStandardSort"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("StandardClass",
							e.getString("StandardClass".toLowerCase()));
					values.put("StandardCode",
							e.getString("StandardCode".toLowerCase()));
					values.put("StandardBranchCode",
							e.getString("StandardBranchCode".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_FireStationStandardSort", null, values);
				}
			}
			// MS_SHCAFireStationStandard

			if (!json1.isNull("MS_SHCAFireStationStandard".toLowerCase())) {
				jArray = json1.getJSONArray("MS_SHCAFireStationStandard"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("StandardClass",
							e.getString("StandardClass".toLowerCase()));
					values.put("StandardCode",
							e.getString("StandardCode".toLowerCase()));
					values.put("StandardBranchCode",
							e.getString("StandardBranchCode".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put("CategoryCode",
							e.getString("CategoryCode".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_SHCAFireStationStandard", null, values);
				}
			}
			// MS_Category

			if (!json1.isNull("MS_Category".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Category".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("CategoryCode",
							e.getString("CategoryCode".toLowerCase()));
					values.put("CategoryName",
							e.getString("CategoryName".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Category", null, values);
				}
			}
			// MS_SHCACondition

			if (!json1.isNull("MS_SHCACondition".toLowerCase())) {
				jArray = json1.getJSONArray("MS_SHCACondition".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("ConditionClass",
							e.getString("ConditionClass".toLowerCase()));
					values.put("ConditionCode",
							e.getString("ConditionCode".toLowerCase()));
					values.put("ConditionBranchCode",
							e.getString("ConditionBranchCode".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put("CategoryCode",
							e.getString("CategoryCode".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_SHCACondition", null, values);
				}
			}
			// MS_Condition

			if (!json1.isNull("MS_Condition".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Condition".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("ConditionClass",
							e.getString("ConditionClass".toLowerCase()));
					values.put("ConditionCode",
							e.getString("ConditionCode".toLowerCase()));
					values.put("ConditionBranchCode",
							e.getString("ConditionBranchCode".toLowerCase()));
					values.put("ConditionBranchName",
							e.getString("ConditionBranchName".toLowerCase()));
					if (!e.isNull("ParentConditionClass".toLowerCase()))
						values.put("ParentConditionClass",
								e.getString("ParentConditionClass"
										.toLowerCase()));
					else
						values.putNull("ParentConditionClass");
					if (!e.isNull("ParentConditionCode".toLowerCase()))
						values.put("ParentConditionCode", e
								.getString("ParentConditionCode".toLowerCase()));
					else
						values.putNull("ParentConditionCode");
					if (!e.isNull("ParentConditionBranchCode".toLowerCase()))
						values.put("ParentConditionBranchCode", e
								.getString("ParentConditionBranchCode"
										.toLowerCase()));
					else
						values.putNull("ParentConditionBranchCode");
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Condition", null, values);
				}
			}
			// MS_ConditionSort

			if (!json1.isNull("MS_ConditionSort".toLowerCase())) {
				jArray = json1.getJSONArray("MS_ConditionSort".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("ConditionClass",
							e.getString("ConditionClass".toLowerCase()));
					values.put("ConditionCode",
							e.getString("ConditionCode".toLowerCase()));
					values.put("ConditionBranchCode",
							e.getString("ConditionBranchCode".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_ConditionSort", null, values);
				}
			}
			// MS_ConditionSupportTime

			if (!json1.isNull("MS_ConditionSupportTime".toLowerCase())) {
				jArray = json1.getJSONArray("MS_ConditionSupportTime"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("ConditionClass",
							e.getString("ConditionClass".toLowerCase()));
					values.put("ConditionCode",
							e.getString("ConditionCode".toLowerCase()));
					values.put("ConditionBranchCode",
							e.getString("ConditionBranchCode".toLowerCase()));
					values.put("DayGroupNo",
							e.getString("DayGroupNo".toLowerCase()));
					values.put("Day_CD", e.getString("Day_CD".toLowerCase()));
					values.put("TimeZoneNo",
							e.getString("TimeZoneNo".toLowerCase()));
					if (!e.isNull("SupportStart_TIME".toLowerCase()))
						values.put("SupportStart_TIME",
								e.getString("SupportStart_TIME".toLowerCase()));
					else
						values.putNull("SupportStart_TIME");
					if (!e.isNull("SupportEnd_TIME".toLowerCase()))
						values.put("SupportEnd_TIME",
								e.getString("SupportEnd_TIME".toLowerCase()));
					else
						values.putNull("SupportEnd_TIME");
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_ConditionSupportTime", null, values);
				}
			}
			// MS_Holiday

			if (!json1.isNull("MS_Holiday".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Holiday".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("Holiday_DATE",
							e.getString("Holiday_DATE".toLowerCase()));
					values.put("HolidayName",
							e.getString("HolidayName".toLowerCase()));
					values.put("HolidayFLG",
							e.getString("HolidayFLG".toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Holiday", null, values);
				}
			}
			// MS_Code

			if (!json1.isNull("MS_Code".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Code".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("CodeType",
							e.getString("CodeType".toLowerCase()));
					values.put("CodeID", e.getString("CodeID".toLowerCase()));
					values.put("CodeName",
							e.getString("CodeName".toLowerCase()));
					values.put(BaseCount.SortNo,
							e.getString(BaseCount.SortNo.toLowerCase()));
					if (!e.isNull("Start_DATE".toLowerCase()))
						values.put("Start_DATE",
								e.getString("Start_DATE".toLowerCase()));
					if (!e.isNull("End_DATE".toLowerCase()))
						values.put("End_DATE",
								e.getString("End_DATE".toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Code", null, values);
				}
			}
			// MS_Message

			if (!json1.isNull("MS_Message".toLowerCase())) {
				jArray = json1.getJSONArray("MS_Message".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MessageID",
							e.getString("MessageID".toLowerCase()));
					values.put("MessageText",
							e.getString("MessageText".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_Message", null, values);
				}
			}
			// TR_Rotation
			if (!json1.isNull("TR_Rotation".toLowerCase())) {
				jArray = json1.getJSONArray("TR_Rotation".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("KamokuCode",
							e.getString("KamokuCode".toLowerCase()));
					values.put("Rotation_DATE",
							e.getString("Rotation_DATE".toLowerCase()));
					values.put("RotationStart_DATETIME",
							e.getString("RotationStart_DATETIME".toLowerCase()));
					values.put("RotationEnd_DATETIME",
							e.getString("RotationEnd_DATETIME".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("TR_Rotation", null, values);
				}
			}
			// TR_Oujyu
			if (!json1.isNull("TR_Oujyu".toLowerCase())) {
				jArray = json1.getJSONArray("TR_Oujyu".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("KamokuCode",
							e.getString("KamokuCode".toLowerCase()));
					values.put("Oujyu", e.getString("Oujyu".toLowerCase()));
					values.put("Bed_Male",
							e.getString("Bed_Male".toLowerCase()));
					values.put("Bed_Female",
							e.getString("Bed_Female".toLowerCase()));
					values.put("Operation",
							e.getString("Operation".toLowerCase()));
					values.put("Doctor", e.getString("Doctor".toLowerCase()));
					values.put("Remarks", e.getString("Remarks".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("TR_Oujyu", null, values);
				}
			}
			// TR_ReportingAll
			if (!json1.isNull("TR_ReportingAll".toLowerCase())) {
				jArray = json1.getJSONArray("TR_ReportingAll".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

//					JSONObject e = jArray.getJSONObject(i);
//					// Bug 140
//					if (!e.getString("EMSUnitCode".toLowerCase()).equals(
//							username)) {
//						newsms = true;
//					}
//					//
//					ContentValues values = new ContentValues();
//					values.put("Report_DATETIME",
//							e.getString("Report_DATETIME".toLowerCase()));
//					values.put("EMSUnitCode",
//							e.getString("EMSUnitCode".toLowerCase()));
//					values.put("ReportTitle",
//							e.getString("ReportTitle".toLowerCase()));
//					values.put("ReportText",
//							e.getString("ReportText".toLowerCase()));
//					values.put(BaseCount.DeleteFLG,
//							e.getString(BaseCount.DeleteFLG.toLowerCase()));
//					if (e.isNull("Destination".toLowerCase())) {
//						values.put("Destination", "9");
//					} else
//						values.put("Destination",
//								e.getString("Destination".toLowerCase()));
//					values.put(BaseCount.Update_DATETIME, e
//							.getString(BaseCount.Update_DATETIME.toLowerCase()));
//					database.replace("TR_ReportingAll", null, values);
					JSONObject e = jArray.getJSONObject(i);
					ContentValues values = new ContentValues();
					values.put("Report_DATETIME",
							e.getString("Report_DATETIME".toLowerCase()));
					values.put("EMSUnitCode",
							e.getString("EMSUnitCode".toLowerCase()));
					values.put("ReportTitle",
							e.getString("ReportTitle".toLowerCase()));
					values.put("ReportText",
							e.getString("ReportText".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					if (e.isNull("Destination".toLowerCase())) {
						values.put("Destination", "9");
					} else
						values.put("Destination",
								e.getString("Destination".toLowerCase()));

					if (!e.getString("EMSUnitCode".toLowerCase()).equals(
							username)) {
						newsms = true;
					}
					String[] args = {
							e.getString("Report_DATETIME".toLowerCase()),
							e.getString("EMSUnitCode".toLowerCase()) };
					String sql = "Select * from TR_ReportingAll Where Report_DATETIME=? and EMSUnitCode=?";
					Cursor mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {
						String where = "Report_DATETIME=? and EMSUnitCode=?";
						database.update("TR_ReportingAll", values, where, args);
					} else {
						values.put(BaseCount.Update_DATETIME,
								Utilities.getDateTimeNow());
						values.put("isshow", 1);
						database.insert("TR_ReportingAll", null, values);
					}
				}
			}
			// 20121005: Bug 48,49
			// TR_MedicalInstitutionAdmission
			if (!json1.isNull("TR_MedicalInstitutionAdmission".toLowerCase())) {
				jArray = json1.getJSONArray("TR_MedicalInstitutionAdmission"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("CaseNo", e.getString("CaseNo".toLowerCase()));
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("Admission_CD",
							e.getString("Admission_CD".toLowerCase()));
					values.put("TerminalNo",
							e.getString("TerminalNo".toLowerCase()));// Bug 150
																		// add
																		// col
																		// TerminalNo
					values.put("Insert_DATETIME",
							e.getString("Insert_DATETIME".toLowerCase()));
					values.put("DeleteFLG",
							e.getString("DeleteFLG".toLowerCase()));
					values.put("Update_DATETIME",
							e.getString("Update_DATETIME".toLowerCase()));
					if (!e.isNull("EMSUnitCode".toLowerCase()))
						values.put("EMSUnitCode",
								e.getString("EMSUnitCode".toLowerCase()));
					else
						values.putNull("EMSUnitCode");
					if (!e.isNull("KamokuName01".toLowerCase()))
						values.put("KamokuName01",
								e.getString("KamokuName01".toLowerCase()));
					else
						values.putNull("KamokuName01");
					if (!e.isNull("KamokuName02".toLowerCase()))
						values.put("KamokuName02",
								e.getString("KamokuName02".toLowerCase()));
					else
						values.putNull("KamokuName02");
					if (!e.isNull("KamokuName03".toLowerCase()))
						values.put("KamokuName03",
								e.getString("KamokuName03".toLowerCase()));
					else
						values.putNull("KamokuName03");
					if (!e.isNull("Movement_DATETIME".toLowerCase()))
						values.put("Movement_DATETIME",
								e.getString("Movement_DATETIME".toLowerCase()));
					else
						values.putNull("Movement_DATETIME");
					database.replace("TR_MedicalInstitutionAdmission", null,
							values);
				}
			}
			// MS_LauncherItem
			if (!json1.isNull("MS_LauncherItem".toLowerCase())) {
				jArray = json1.getJSONArray("MS_LauncherItem".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("LauncherItemCode",
							e.getString("LauncherItemCode".toLowerCase()));
					values.put("LauncherItemName",
							e.getString("LauncherItemName".toLowerCase()));
					values.put("LauncherItemFileName",
							e.getString("LauncherItemFileName".toLowerCase()));
					values.put("LauncherStateTransitionType", e
							.getString("LauncherStateTransitionType"
									.toLowerCase()));
					values.put("DisplayModeType",
							e.getString("DisplayModeType".toLowerCase()));
					values.put("ScreenTransitionInfo",
							e.getString("ScreenTransitionInfo".toLowerCase()));
					values.put("SortNo", e.getString("SortNo".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					values.put(BaseCount.Update_DATETIME, e
							.getString(BaseCount.Update_DATETIME.toLowerCase()));
					database.replace("MS_LauncherItem", null, values);
				}
			}
			// MS_NightClinic
			if (!json1.isNull("MS_NightClinic".toLowerCase())) {
				jArray = json1.getJSONArray("MS_NightClinic".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("SHCACode",
							e.getString("SHCACode".toLowerCase()));
					values.put("AreaCode",
							e.getString("AreaCode".toLowerCase()));
					if (!e.isNull("MIName_Kanji".toLowerCase()))
						values.put("MIName_Kanji",
								e.getString("MIName_Kanji".toLowerCase()));
					if (!e.isNull("MIName_Kana".toLowerCase()))
						values.put("MIName_Kana",
								e.getString("MIName_Kana".toLowerCase()));
					values.put("PostNo", e.getString("PostNo".toLowerCase()));
					if (!e.isNull("Address_Kanji".toLowerCase()))
						values.put("Address_Kanji",
								e.getString("Address_Kanji".toLowerCase()));
					if (!e.isNull("Address_Kana".toLowerCase()))
						values.put("Address_Kana",
								e.getString("Address_Kana".toLowerCase()));
					values.put("EMailAddress",
							e.getString("EMailAddress".toLowerCase()));
					if (!e.isNull("TelNo1".toLowerCase()))
						values.put("TelNo1",
								e.getString("TelNo1".toLowerCase()));
					if (!e.isNull("TelNo2".toLowerCase()))
						values.put("TelNo2",
								e.getString("TelNo2".toLowerCase()));
					values.put("FaxNo", e.getString("FaxNo".toLowerCase()));
					if (!e.isNull("Latitude".toLowerCase()))
						values.put("Latitude",
								e.getString("Latitude".toLowerCase()));
					if (!e.isNull("Longitude".toLowerCase()))
						values.put("Longitude",
								e.getString("Longitude".toLowerCase()));
					values.put("MIType", e.getString("MIType".toLowerCase()));
					if (!e.isNull("KamokuName01".toLowerCase()))
						values.put("KamokuName01",
								e.getString("KamokuName01".toLowerCase()));
					if (!e.isNull("WeekdayOfficeHours01".toLowerCase()))
						values.put("WeekdayOfficeHours01",
								e.getString("WeekdayOfficeHours01"
										.toLowerCase()));
					if (!e.isNull("SaturdayOfficeHours01".toLowerCase()))
						values.put("SaturdayOfficeHours01", e
								.getString("SaturdayOfficeHours01"
										.toLowerCase()));
					if (!e.isNull("HolidayOfficeHours01".toLowerCase()))
						values.put("HolidayOfficeHours01",
								e.getString("HolidayOfficeHours01"
										.toLowerCase()));
					if (!e.isNull("KamokuName02".toLowerCase()))
						values.put("KamokuName02",
								e.getString("KamokuName02".toLowerCase()));
					if (!e.isNull("WeekdayOfficeHours02".toLowerCase()))
						values.put("WeekdayOfficeHours02",
								e.getString("WeekdayOfficeHours02"
										.toLowerCase()));
					if (!e.isNull("SaturdayOfficeHours02".toLowerCase()))
						values.put("SaturdayOfficeHours02", e
								.getString("SaturdayOfficeHours02"
										.toLowerCase()));
					if (!e.isNull("HolidayOfficeHours02".toLowerCase()))
						values.put("HolidayOfficeHours02",
								e.getString("HolidayOfficeHours02"
										.toLowerCase()));
					if (!e.isNull("KamokuName03".toLowerCase()))
						values.put("KamokuName03",
								e.getString("KamokuName03".toLowerCase()));
					if (!e.isNull("WeekdayOfficeHours03".toLowerCase()))
						values.put("WeekdayOfficeHours03",
								e.getString("WeekdayOfficeHours03"
										.toLowerCase()));
					if (!e.isNull("SaturdayOfficeHours03".toLowerCase()))
						values.put("SaturdayOfficeHours03", e
								.getString("SaturdayOfficeHours03"
										.toLowerCase()));
					if (!e.isNull("HolidayOfficeHours03".toLowerCase()))
						values.put("HolidayOfficeHours03",
								e.getString("HolidayOfficeHours03"
										.toLowerCase()));
					if (!e.isNull("KamokuName04".toLowerCase()))
						values.put("KamokuName04",
								e.getString("KamokuName04".toLowerCase()));
					if (!e.isNull("WeekdayOfficeHours04".toLowerCase()))
						values.put("WeekdayOfficeHours04",
								e.getString("WeekdayOfficeHours04"
										.toLowerCase()));
					if (!e.isNull("SaturdayOfficeHours04".toLowerCase()))
						values.put("SaturdayOfficeHours04", e
								.getString("SaturdayOfficeHours04"
										.toLowerCase()));
					if (!e.isNull("HolidayOfficeHours04".toLowerCase()))
						values.put("HolidayOfficeHours04",
								e.getString("HolidayOfficeHours04"
										.toLowerCase()));
					if (!e.isNull("SortNo".toLowerCase()))
						values.put("SortNo",
								e.getString("SortNo".toLowerCase()));
					values.put("DeleteFLG",
							e.getString("DeleteFLG".toLowerCase()));
					values.put("Update_DATETIME",
							e.getString("Update_DATETIME".toLowerCase()));
					if (!e.isNull("Memo".toLowerCase()))
						values.put("Memo", e.getString("Memo".toLowerCase()));
					database.replace("MS_NightClinic", null, values);
				}
			}
			// MS_FireStationOriginalMI
			if (!json1.isNull("MS_FireStationOriginalMI".toLowerCase())) {
				jArray = json1.getJSONArray("MS_FireStationOriginalMI"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("FireStationOriginalCode", e
							.getString("FireStationOriginalCode".toLowerCase()));
					values.put("MICode", e.getString("MICode".toLowerCase()));
					if (!e.isNull("TransportationTelNo".toLowerCase()))
						values.put("TransportationTelNo", e
								.getString("TransportationTelNo".toLowerCase()));
					if (!e.isNull("MIName_Kanji".toLowerCase()))
						values.put("MIName_Kanji",
								e.getString("MIName_Kanji".toLowerCase()));
					if (!e.isNull("MIDetailName".toLowerCase()))
						values.put("MIDetailName",
								e.getString("MIDetailName".toLowerCase()));
					if (!e.isNull("MIName_Kana".toLowerCase()))
						values.put("MIName_Kana",
								e.getString("MIName_Kana".toLowerCase()));
					values.put("PostNo", e.getString("PostNo".toLowerCase()));
					if (!e.isNull("Address_Kanji".toLowerCase()))
						values.put("Address_Kanji",
								e.getString("Address_Kanji".toLowerCase()));
					values.put("Address_Kana",
							e.getString("Address_Kana".toLowerCase()));
					if (!e.isNull("Latitude".toLowerCase()))
						values.put("Latitude",
								e.getString("Latitude".toLowerCase()));
					if (!e.isNull("Longitude".toLowerCase()))
						values.put("Longitude",
								e.getString("Longitude".toLowerCase()));
					values.put("FDMAMICode",
							e.getString("FDMAMICode".toLowerCase()));
					values.put("MIType", e.getString("MIType".toLowerCase()));
					if (!e.isNull("PrefectureCode".toLowerCase()))
						values.put("PrefectureCode",
								e.getString("PrefectureCode".toLowerCase()));
					values.put("SortNo", e.getString("SortNo".toLowerCase()));
					values.put("DeleteFLG",
							e.getString("DeleteFLG".toLowerCase()));
					values.put("Update_DATETIME",
							e.getString("Update_DATETIME".toLowerCase()));
					database.replace("MS_FireStationOriginalMI", null, values);
				}
			}
			if (!json1.isNull("tr_firestationstandard_oujyu".toLowerCase())) {
				jArray = json1.getJSONArray("tr_firestationstandard_oujyu"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("micode", e.getString("micode".toLowerCase()));
					values.put("standardclass",
							e.getString("standardclass".toLowerCase()));
					values.put("standardcode",
							e.getString("standardcode".toLowerCase()));
					values.put("standardbranchcode",
							e.getString("standardbranchcode".toLowerCase()));
					values.put("update_datetime",
							e.getString("update_datetime".toLowerCase()));
					database.replace("tr_firestationstandard_oujyu", null,
							values);
				}
			}
			if (!json1.isNull("MS_ModelSetting".toLowerCase())) {
				jArray = json1.getJSONArray("MS_ModelSetting".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("Model_Name",
							e.getString("Model_Name".toLowerCase()));
					values.put("Location_Name",
							e.getString("Default_Path".toLowerCase()));
					values.put("Codec_Type",
							e.getString("Codec_Type".toLowerCase()));
					values.put("File_Type",
							e.getString("File_Type".toLowerCase()));
					values.put("Audio_source",
							e.getString("Audio_source".toLowerCase()));
					values.put("Audio_outputformat",
							e.getString("Audio_outputformat".toLowerCase()));
					values.put("Audio_encorder",
							e.getString("Audio_encorder".toLowerCase()));
					values.put("Video_source",
							e.getString("Video_source".toLowerCase()));
					values.put("Video_outputformat",
							e.getString("Video_outputformat".toLowerCase()));
					values.put("Video_encorder",
							e.getString("Video_encorder".toLowerCase()));
					values.put("Video_framerate",
							e.getString("Video_framerate".toLowerCase()));
					values.put("Image_outputformat",
							e.getString("Image_outputformat".toLowerCase()));
					database.replace("MS_ModelSetting", null, values);
				}
			}
			// Bug 535 20130131
			// Calendar calendar = Calendar.getInstance();
			// calendar.setTimeInMillis(System.currentTimeMillis());
			// calendar.add(Calendar.MINUTE, -30);
			// String LastTime = Utilities.getDateTimeNow(calendar);
			// boolean result =
			// database.delete("TR_MedicalInstitutionAdmission",
			// "Update_DATETIME<" + LastTime, null) > 0;
			// TR_MovementTime
			// 20130319 beta2 remove
			// if (!json1.isNull("TR_MovementTime".toLowerCase())) {
			// jArray = json1.getJSONArray("TR_MovementTime".toLowerCase());
			// for (int i = 0; i < jArray.length(); i++) {
			// JSONObject e = jArray.getJSONObject(i);
			//
			// ContentValues values = new ContentValues();
			// values.put("FireStationCode",
			// e.getString("FireStationCode".toLowerCase()));
			// values.put("CaseNo", e.getString("CaseNo".toLowerCase()));
			// values.put("MovementCode",
			// e.getString("MovementCode".toLowerCase()));
			// values.put("Movement_DATETIME",
			// e.getString("Movement_DATETIME".toLowerCase()));
			// values.put("Update_DATETIME",
			// e.getString("Update_DATETIME".toLowerCase()));
			// database.replace("TR_MovementTime", null, values);
			// }
			// }
			// MS_Setting
			if (!json1.isNull("MS_Setting".toLowerCase())) {

				jArray = json1.getJSONArray("MS_Setting".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put(MS_SettingCount.MNET_URL,
							e.getString("MNET_URL".toLowerCase()));
					values.put(MS_SettingCount.MNETCount,
							e.getString("MNETCount".toLowerCase()));
					values.put(MS_SettingCount.MNETTIME,
							e.getString("MNETTIME".toLowerCase()));
					values.put(MS_SettingCount.ThirdCoordinateCount,
							e.getString("ThirdCoordinateCount".toLowerCase()));
					values.put(MS_SettingCount.ThirdCoordinateTime,
							e.getString("ThirdCoordinateTime".toLowerCase()));
					values.put(MS_SettingCount.DBTime,
							e.getString("DBTime".toLowerCase()));
					values.put(MS_SettingCount.CaseCount,
							e.getString("CaseCount".toLowerCase()));
					values.put(MS_SettingCount.PastCaseCount,
							e.getString("PastCaseCount".toLowerCase()));
					values.put(MS_SettingCount.ResultCount,
							e.getString("ResultCount".toLowerCase()));
					values.put(MS_SettingCount.FTP_IP,
							e.getString("FTP_IP".toLowerCase()));
					values.put(MS_SettingCount.Update_DATETIME,
							e.getString("Update_DATETIME".toLowerCase()));
					values.put(MS_SettingCount.ScreenSensitivity,
							e.getString("ScreenSensitivity".toLowerCase()));
					values.put("GPSDBTime",
							e.getString("GPSDBTime".toLowerCase()));
					if (!e.isNull("TimeInrvalKahi".toLowerCase())) {
						int t = Integer.parseInt(e
								.getString("TimeInrvalKahi"
										.toLowerCase())) * 1000;
						values.put("TimeInrvalKahi", t);
					}
					if (!e.isNull("soundplaytimeondisaster".toLowerCase())) {
						int t=Integer.parseInt(e
								.getString("soundplaytimeondisaster"
										.toLowerCase())) *1000;
						values.put("soundplaytimeondisaster", t);
					}
				
					if (!e.isNull("soundplaytimeonnotification".toLowerCase())) {
						int t=Integer.parseInt(e
								.getString("soundplaytimeonnotification"
										.toLowerCase())) *1000;
						values.put("soundplaytimeonnotification", t);
					}
					if (!e.isNull("TimerIntervalDisaster".toLowerCase())) {
						int t=Integer.parseInt(e
								.getString("TimerIntervalDisaster"
										.toLowerCase())) *1000;
						values.put("TimerIntervalDisaster", t);
					}
					if (!e.isNull("TimerIntervalNotification".toLowerCase())) {
						int t=Integer.parseInt(e
								.getString("TimerIntervalNotification"
										.toLowerCase())) *1000;
						values.put("TimerIntervalNotification", t);
					}
					if (!e.isNull("TimeIntervalPush".toLowerCase())) {
						int t=Integer.parseInt(e
								.getString("TimeIntervalPush"
										.toLowerCase())) *1000;
						values.put("TimeIntervalPush", t);
					}
					if (!e.isNull("DisasterDBTime".toLowerCase())) {
						int t = Integer.parseInt(e.getString("DisasterDBTime"
								.toLowerCase())) * 1000;
						values.put("DisasterDBTime", t);
					}
					values.put(
							"HeaderBackcolorOnDisaster",
							"#"
									+ e.getString("HeaderBackcolorOnDisaster"
											.toLowerCase()));
					values.put(
							"HeaderBackcolorOnNotification",
							"#"
									+ e.getString("HeaderBackcolorOnNotification"
											.toLowerCase()));
					
					values.put(
							"HeaderBackcolorOnEmergency",
							"#"
									+ e.getString("HeaderBackcolorOnEmergency"
											.toLowerCase()));
					database.delete("MS_Setting", null, null);
					database.insert(MS_SettingCount.TableName, null, values);

				}
			}
			database.setTransactionSuccessful();
			if (newsms) {
				datetime += ",1";
			} else {
				datetime += ",0";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return datetime;
	}

	public boolean DailyGC() {
		boolean res = false;
		database.beginTransaction();
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.add(Calendar.MINUTE, -30);
			String LastTime = Utilities.getDateTimeNow(calendar);
			res = database.delete("TR_MedicalInstitutionAdmission",
					"Update_DATETIME<" + LastTime, null) > 0;

			calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.add(Calendar.MONTH, -1);
			String LastMonth = Utilities.getDateTimeNow(calendar);
			res = database.delete("TR_Rotation", "RotationStart_DATETIME<"
					+ LastMonth, null) > 0;
			res = true;
			// bug 598
			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			res = false;
		} finally {
			database.endTransaction();
			database.close();
		}
		return res;
	}
}
