﻿package jp.osaka;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;

public class AutoStart extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			SetAlarm(context);
		}		

	}

	public void SetAlarm(Context context) {
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, ServiceUpdateData.class);
		// PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		PendingIntent pi = PendingIntent.getService(context, 0, i,
				PendingIntent.FLAG_CANCEL_CURRENT);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, 30);// sau 30 giay thi bat dau
		int dbtime = 10;
		CL_MSSetting cl = new CL_MSSetting(context);
		Cursor c = cl.fetchLastSeting();

		if (c != null && c.getCount() > 0) {
			String DBtimer = c.getString(c.getColumnIndex("DBTime"));
			if (Utilities.IsNumber(DBtimer)) {
				dbtime = Integer.parseInt(DBtimer);
			}
		}
		int time = 1000 * 60 * dbtime;
		am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
				time, pi); // Millisec * Second * Minute
		CL_Message m = new CL_Message(context);
		String mg = m.getMessage(ContantMessages.ServiceStartAgain);
		Toast.makeText(context, mg, Toast.LENGTH_LONG).show();
	}
}
