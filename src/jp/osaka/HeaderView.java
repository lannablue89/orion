package jp.osaka;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jp.osaka.db.DatabaseQuery;
import jp.osaka.db.model.QQ_tr_kin_yousei_Model;
import jp.osaka.db.model.QQ_tr_kin_yousei_kahi_Model;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HeaderView extends LinearLayout {

	LayoutInflater inflater;
	QQ_tr_kin_yousei_Model qq_tr_kin_yousei_Model;
	List<QQ_tr_kin_yousei_kahi_Model> kahi_Models;
	public HeaderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		inflater=LayoutInflater.from(context);
		inflater.inflate(R.layout.view_header_e036, this);
	}
	
	public void initData(String skin_no,String skin_branch_no,String firestationcaode,String casecode){
		int kin_no=Integer.parseInt(skin_no);
		int kin_branch_no=Integer.parseInt(skin_branch_no);
		 qq_tr_kin_yousei_Model=DatabaseQuery.getInstance(getContext()).get_QQ_tr_kin_yousei_Model(kin_no, kin_branch_no);
		kahi_Models=DatabaseQuery.getInstance(getContext()).get_QQ_tr_kin_yousei_kahi_Model(kin_no, kin_branch_no);
		 TextView no8=(TextView) findViewById(R.id.no8);
		no8.setText("No."+kin_no);
		
		TextView no9=(TextView) findViewById(R.id.no9);
		String time=Utilities.formatDateTime(qq_tr_kin_yousei_Model.kin_date+qq_tr_kin_yousei_Model.kin_time);
		Log.d("time", time);
		no9.setText("発生日時:"+time);
		
		TextView no10=(TextView) findViewById(R.id.no10);
		no10.setText(qq_tr_kin_yousei_Model.title);
		
		TextView no11=(TextView) findViewById(R.id.no11);
		no11.setText(DatabaseQuery.getInstance(getContext()).get_ms_firestation_Model_FireStationName(firestationcaode));
		
		TextView no12=(TextView) findViewById(R.id.no12);
		no12.setText(getCountDownTime(time)+"秒");
		
		int value13=getNo13();
		TextView no13=(TextView) findViewById(R.id.no13);
		no13.setText(value13+"件");
		
		int value14=getNo14();
		TextView no14=(TextView) findViewById(R.id.no14);
		no14.setText(value14+"件");
		
		int value15=kahi_Models.size()-value13-value14;
		TextView no15=(TextView) findViewById(R.id.no15);
		no15.setText(value15+"件");
	}
	
	private int getCountDownTime(String sdate){
		SimpleDateFormat dt = new SimpleDateFormat("yyyyy/mm/dd hh:mm"); 
		try {
			Date date=dt.parse(sdate);
			long temp = System.currentTimeMillis()-date.getTime();
			return (int) (temp/ 1000);
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getNo13(){
		int result=0;
		for(QQ_tr_kin_yousei_kahi_Model model:kahi_Models){
			if(model.ukeire_kahi.equals("1")){
				result++;
			}
		}
		return result;
	}
	
	public int getNo14(){
		int result=0;
		for(QQ_tr_kin_yousei_kahi_Model model:kahi_Models){
			if(model.ukeire_kahi.equals("3")){
				result++;
			}
		}
		return result;
	}
}
