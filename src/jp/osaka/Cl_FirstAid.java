﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_FirstAid {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_FirstAid(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createTR_FirstAid(String FireStationCode, String CaseNo,
			String FirstAid_CD, String FirstAid_TEXT, String FirstAid_DATETIME) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("FirstAid_CD", FirstAid_CD);
			if (FirstAid_TEXT != null && !FirstAid_TEXT.equals(""))
				values.put("FirstAid_TEXT", FirstAid_TEXT);
			else
				values.putNull("FirstAid_TEXT");
			values.put("FirstAid_DATETIME", FirstAid_DATETIME);
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			result = database.insert("TR_FirstAid", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long updateTR_FirstAid(String FireStationCode, String CaseNo,
			String FirstAid_CD, String FirstAid_TEXT, String FirstAid_DATETIME) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			if (FirstAid_TEXT != null && !FirstAid_TEXT.equals(""))
				values.put("FirstAid_TEXT", FirstAid_TEXT);
			else
				values.putNull("FirstAid_TEXT");
			values.put("FirstAid_DATETIME", FirstAid_DATETIME);
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			String where = "FireStationCode=? and CaseNo=? and FirstAid_CD=?";
			String[] args = { FireStationCode, CaseNo, FirstAid_CD };
			result = database.update("TR_FirstAid", values, where, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteTR_FirstAid(String FireStationCode, String CaseNo,
			String FirstAid_CD) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and FirstAid_CD=?";
			String[] args = { FireStationCode, CaseNo, FirstAid_CD };
			result = database.delete("TR_FirstAid", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_FirstAid(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_FirstAid Where  FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
