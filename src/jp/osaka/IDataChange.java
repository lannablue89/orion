package jp.osaka;

public interface IDataChange {

	public void OnDataChange(int tag, boolean explan);
}
