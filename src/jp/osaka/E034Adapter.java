package jp.osaka;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class E034Adapter extends ArrayAdapter<DTO_MedicalInstitution> {
	Context c;

	public E034Adapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		c = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		// return super.getView(position, convertView, parent);
		View v = convertView;
		ViewWraperDV mwp;

		if (v == null) {
			LayoutInflater l = LayoutInflater.from(c);
			v = l.inflate(R.layout.layoutrowminame, null);
			mwp = new ViewWraperDV(v);
			v.setTag(mwp);
		} else {

			mwp = (ViewWraperDV) convertView.getTag();
		}
		TextView txtMiName = mwp.getMIName();
		DTO_MedicalInstitution dt=this.getItem(position);
		txtMiName.setText(dt.getMIName_Kanji());

		return v;
	}

}

class ViewWraperDV {

	View base;

	TextView txt1 = null;

	ViewWraperDV(View base) {

		this.base = base;
	}

	TextView getMIName() {
		if (txt1 == null) {
			txt1 = (TextView) base.findViewById(R.id.txtRowMIName);
		}
		return txt1;
	}

}
