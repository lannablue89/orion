﻿package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_ErrorLog {

	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_ErrorLog(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	//Bug 547 20130204 change TerminalTelNo->TerminalTelNo
	public long createTR_ErrorLog(String TerminalTelNo, String SEQNo,
			String ErrorLevel, String ErrorText) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("TerminalTelNo", TerminalTelNo);
			values.put("SEQNo", SEQNo);
			values.put("ErrorLevel", ErrorLevel);
			values.put("ErrorText", ErrorText);
			values.put("Output_DATETIME", Utilities.getDateTimeNow());
			result = database.insert("TR_ErrorLog", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int getMaxTR_ErrorLog(String TerminalTelNo) {

		int result = 0;
		Cursor mCursor = null;
		try {

			String[] args = { TerminalTelNo };
			String sql = "Select Max(SEQNo) as SEQNo from TR_ErrorLog Where TerminalTelNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor.getColumnIndex("SEQNo")) != null) {
					result = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("SEQNo")));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_ErrorLog(String TerminalTelNo, String Output_DATETIME) {
		Cursor mCursor = null;
		try {

			String[] args = { TerminalTelNo, Output_DATETIME };
			String sql = "Select * from TR_ErrorLog Where TerminalTelNo=? and Output_DATETIME>? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

}
