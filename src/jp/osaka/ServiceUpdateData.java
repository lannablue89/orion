﻿package jp.osaka;

import java.io.IOException;
import java.net.URLEncoder;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class ServiceUpdateData extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		// super.onStart(intent, startId);
		try {
			// Daily GC: bug 598
			SharedPreferences sh1 = getSharedPreferences("app", MODE_PRIVATE);
			String datenowgc = sh1.getString(BaseCount.DateNowGC, "");
			String datenow = Utilities.getDateNow();
			if (!datenow.equals(datenowgc)) {
				CL_UpdateData cl = new CL_UpdateData(ServiceUpdateData.this);
				boolean res = cl.DailyGC();
				if (res) {
					// Toast.makeText(
					// this,
					// "Deleted data of tr_rotation and tr_medicalinstitutionadmission is successfull",
					// Toast.LENGTH_LONG).show();
					// E022.saveErrorLog(
					// "Deleted data of tr_rotation and tr_medicalinstitutionadmission is successfull",
					// this);
					SharedPreferences sh2 = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e2 = sh2.edit();
					e2.putString(BaseCount.DateNowGC, datenow);
					e2.commit();

				} else {
					// Toast.makeText(
					// this,
					// "Delete tr_rotation and tr_medicalinstitutionadmission is failed",
					// Toast.LENGTH_LONG).show();
					E022.saveErrorLog(
							"Delete tr_rotation and tr_medicalinstitutionadmission is failed",
							this);
				}
			}
			// update data
			if (isOnline()) {
				// SharedPreferences sh = getSharedPreferences("app",
				// MODE_PRIVATE);
				// String syndb = sh.getString("SysDB", "0");
				String syndb = getStatusSysDB();
				if (syndb.equals("0")) {
					// Editor e = sh.edit();
					// e.putString("SysDB", "1");
					// e.commit();
					startThread();

				}
			} else {
				stopSelf();
			}
		} catch (Exception ex) {
			if (ex != null)
				E022.saveErrorLog(ex.getMessage(), this);
		} finally {

		}

	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(ServiceUpdateData.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			JSONObject json = getSynDB();
			if (json != null) {
				String[] s = null;
				String dateServer;
				try {

					CL_User us = new CL_User(ServiceUpdateData.this);
					String username = us.getUserName();
					CL_UpdateData cl = new CL_UpdateData(ServiceUpdateData.this);
					dateServer = cl.updateDataBase(json, username);
					// cl.close();
					s = dateServer.split(",");

				} catch (Exception e) {
					// TODO: handle exception
				} finally {
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					String sms = sh.getString("NewSMS", "0");
					Editor e = sh.edit();
					if (s != null && s.length > 1) {
						e.putString(BaseCount.Update_DATETIME, s[0]);
						// Bug 140
						// if (sms.equals("0")) {
						// e.putString("NewSMS", s[1]);
						// }
					}
					//
					e.putString("SysDB", "0"); // Bug 393
					e.commit();
				}
			} else {
				resetFlatSysDB();
			}
			stopSelf();
		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), this);
		}
		return log;
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) this
//					.getApplicationContext().getSystemService(
//							Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Get Ip
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), this);
		}
		return "";
	}

	private JSONObject getSynDB() {

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		JSONObject json = null;

		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		String date = sh.getString(BaseCount.Update_DATETIME, "00010101010101");
		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataAllTableWithDateTime;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("datetime", date);
			CL_User us = new CL_User(this);
			String Fire = us.getFirestationCode();
			request.addProperty("FireStationCode", Fire);
			// Param log 20130103
			CL_User cl = new CL_User(this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user, "");
			request.addProperty("smpLog", loginfo);
			String userid = "0";
			request.addProperty("userid", user);
			request.addProperty("phonenumber", getPhoneNumber());
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);

			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();

				// CL_Message m = new CL_Message(ServiceUpdateData.this);
				// String mg =
				// m.getMessage(ContantMessages.GetReponseServerError);
				// Toast.makeText(ServiceUpdateData.this, mg,
				// Toast.LENGTH_SHORT)
				// .show();
				resetFlatSysDB();
				E022.saveErrorLog(e2.getMessage(), ServiceUpdateData.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// CL_Message m = new CL_Message(ServiceUpdateData.this);
				// String mg = m.getMessage(ContantMessages.JSonError);
				// Toast.makeText(ServiceUpdateData.this, mg,
				// Toast.LENGTH_SHORT)
				// .show();
				resetFlatSysDB();
				E022.saveErrorLog(e1.getMessage(), ServiceUpdateData.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// e3.printStackTrace();
			// Toast.makeText(this, e3.getMessage(), Toast.LENGTH_SHORT).show();
			resetFlatSysDB();
			E022.saveErrorLog(e3.getMessage(), ServiceUpdateData.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// e3.printStackTrace();

			// Toast.makeText(this, e3.getMessage(), Toast.LENGTH_SHORT).show();
			resetFlatSysDB();
			E022.saveErrorLog(e3.getMessage(), ServiceUpdateData.this);
		}
		return json;

	}

	private void resetFlatSysDB() {
		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		Editor e = sh.edit();
		e.putString("SysDB", "0");
		e.commit();
	}
}
