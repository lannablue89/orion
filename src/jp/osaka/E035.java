﻿package jp.osaka;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jp.osaka.ContantTable.BaseCount;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class E035 extends Activity implements OnTouchListener {
	MyAdapter mydapter;
	ListView list;
	int KeyCode;
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;
	int limit = 4;
	int numberDisplay = 0;
	int numberitem = 0;
	String FirestationName = "";
	ProgressDialog p;
	ArrayList<DTO_Kin_Youse> listItem;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");

			// showProccess();
			DTO_Kin_Youse dt = new DTO_Kin_Youse();
			dt = (DTO_Kin_Youse) b.getSerializable("DTO");
			if (dt != null)
				listItem.add(dt);
			// mydapter.add(dt);

			if (key.equals("1")) {

				loadData();
				exitProccess();
				if (mydapter != null && mydapter.getCount() < 1) {
					E022.showErrorDialog(E035.this, ContantMessages.NotData,
							ContantMessages.NotData, false);
				}
			}
		}

	};

	void exitProccess() {
		p.dismiss();

	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E035.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E035.this);
		String mg = m.getMessage(ContantMessages.DataLoading);
		p.setMessage(mg);
		// 20121127 bug 393
		// p.setCancelable(false);
		p.setCanceledOnTouchOutside(false);
		//
		p.show();
	}

	private void loadData() {
		// int old = numberitem;
		if (mydapter.getCount() > 0) {
			DTO_Kin_Youse dto = new DTO_Kin_Youse();
			dto = mydapter.getItem(mydapter.getCount() - 1);
			if (dto == null) {
				mydapter.remove(dto);
			}
		}
		for (int i = 0; i < listItem.size(); i++) {
			if (i == numberDisplay) {
				break;
			}
			if (i >= numberitem) {
				numberitem++;
				DTO_Kin_Youse dti = new DTO_Kin_Youse();
				dti = listItem.get(i);
				mydapter.add(dti);
			}

		}
		if (listItem.size() > numberitem) {
			mydapter.add(null);
		}
		mydapter.notifyDataSetChanged();
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E035.this,
					progressBar, pro_2, "", "");
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute035);
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		KeyCode = b.getInt("Key");
		// Slide menu
		utl.setMenu_open(false);
		initSileMenu();
		numberDisplay = limit;
		numberitem = 0;
		listItem = new ArrayList<DTO_Kin_Youse>();
		try {

			CL_User cl = new CL_User(this);
			FirestationName = cl.getFireStationName();

			mydapter = new MyAdapter(this, R.layout.layoutgrid35);
			list = (ListView) findViewById(R.id.list035);
			list.setAdapter(mydapter);
			list.setOnTouchListener(this);
			// list.setOnItemClickListener(new OnItemClickListener() {
			//
			// public void onItemClick(AdapterView<?> arg0, View arg1,
			// int arg2, long arg3) {
			// // TODO Auto-generated method stub
			// DTO_Kin_Youse dt = mydapter.getItem(arg2);
			// if (dt != null) {
			// Intent t = new Intent();
			// t.putExtra("TR_Kin_Youse", dt);
			// setResult(KeyCode, t);
			// finish();
			// } else {
			// numberDisplay += limit;
			// loadData();
			//
			// }
			// }
			// });
			// getListDisaster();
			showProccess();
			startThread();

			Button btnBack = (Button) findViewById(R.id.btnBack);
			btnBack.setOnTouchListener(this);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();

				}
			});
			Button btnRefesh = (Button) findViewById(R.id.btnRefesh);
			btnRefesh.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					numberDisplay = limit;
					numberitem = 0;
					listItem.clear();
					mydapter.clear();
					showProccess();
					startThread();
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
	}

	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;
			try {

				CL_TR_kin_yose cl = new CL_TR_kin_yose(E035.this);
				cur = cl.getAllDisaster();
				if (cur != null && cur.getCount() > 0) {

					String kinno = "";
					do {
						try {

							DTO_Kin_Youse dt = new DTO_Kin_Youse();
							dt.kin_no = cur.getString(cur
									.getColumnIndex("kin_no"));
							if (dt.kin_no.equals(kinno))
								continue;
							kinno = dt.kin_no;
							dt.kin_branch_no = cur.getString(cur
									.getColumnIndex("kin_branch_no"));
							dt.kin_date = cur.getString(cur
									.getColumnIndex("kin_date"));
							dt.kin_time = cur.getString(cur
									.getColumnIndex("kin_time"));
							dt.title = cur.getString(cur
									.getColumnIndex("title"));
							dt.firestationame = FirestationName;
							DateFormat formatter = new SimpleDateFormat(
									BaseCount.DateTimeFormat);
							String kintime = dt.kin_date + dt.kin_time;
							if (kintime.length() == 12)
								kintime = kintime + "00";
							if (kintime.length() == 14) {
								Date date1 = (Date) formatter.parse(kintime);
								Date date2 = (Date) formatter.parse(Utilities
										.getDateTimeNow());
								long diff = date2.getTime() - date1.getTime();
								int time = (int) (diff * 1.0 / (1000));// s
								dt.TimeNow = time;
							}
							CL_TR_kin_yose kh = new CL_TR_kin_yose(E035.this);
							dt.kahi1 = kh.countyousei_kahi("1", dt.kin_no,
									dt.kin_branch_no);
							kh = new CL_TR_kin_yose(E035.this);
							dt.kahi3 = kh.countyousei_kahi("3", dt.kin_no,
									dt.kin_branch_no);
							kh = new CL_TR_kin_yose(E035.this);
							dt.kahi = kh.countyousei_kahi("", dt.kin_no,
									dt.kin_branch_no);

							dt.yousei_level = cur.getString(cur
									.getColumnIndex("yousei_level"));
							dt.dmat_yousei_f = cur.getString(cur
									.getColumnIndex("dmat_yousei_f"));
							dt.kin_no = cur.getString(cur
									.getColumnIndex("kin_no"));

							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putSerializable("DTO", dt);
							b.putString("Key", "0");
							mg.setData(b);
							handler.sendMessage(mg);
						} catch (Exception e) {
							// TODO: handle exception
							E022.saveErrorLog(e.getMessage(), E035.this);
						}
					} while (cur.moveToNext());

				}
			} catch (Exception e) {
				// TODO: handle exception
				E022.saveErrorLog(e.getMessage(), E035.this);

			} finally {
				if (cur != null)
					cur.close();
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);

			}

		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	// private void getListDisaster() {
	// Cursor cur = null;
	// try {
	// CL_TR_kin_yose cl = new CL_TR_kin_yose(E035.this);
	// cur = cl.getAllDisaster();
	// if (cur != null && cur.getCount() > 0) {
	// do {
	// DTO_Kin_Youse dt = new DTO_Kin_Youse();
	// dt.kin_no = cur.getString(cur.getColumnIndex("kin_no"));
	// dt.kin_branch_no = cur.getString(cur
	// .getColumnIndex("kin_branch_no"));
	// mydapter.add(dt);
	// } while (cur.moveToNext());
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// E022.saveErrorLog(e.getMessage(), E035.this);
	// } finally {
	// if (cur != null)
	// cur.close();
	// }
	// }

	public class MyAdapter extends ArrayAdapter<DTO_Kin_Youse> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.layoutgrid35, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}

			TextView txtKinNo = myw.getSTT();
			final Button btnDetail = myw.getButtonDetail();
			TextView txtKinDate = myw.getKinDate();
			TextView txtTitle = myw.getTitle();
			TextView txtFireStation = myw.getFireStation();
			TextView txtTime = myw.getTime();
			TextView txtKahi1 = myw.getkahi1();
			TextView txtKahi3 = myw.getkahi3();
			TextView txtKahiNot = myw.getKahiNot();
			LinearLayout row = myw.getRow();
			LinearLayout rowMain = myw.getRowMain();
			final LinearLayout rowDetail = myw.getRowDetail();
			final DTO_Kin_Youse dt = this.getItem(position);
			rowMain.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if (dt != null) {
						Intent t = new Intent();
						t.putExtra("TR_Kin_Youse", dt);
						setResult(KeyCode, t);
						finish();
					} else {
						numberDisplay += limit;
						loadData();

					}
				}
			});
			if (dt != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(0, 0, 0, 0);
				rowMain.setLayoutParams(param);

				row.setVisibility(View.VISIBLE);
				rowDetail.setVisibility(View.VISIBLE);

				btnDetail.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if (dt.isOpen == false) {
							btnDetail.setText("Λ");
							CL_TR_kin_yose cl = new CL_TR_kin_yose(E035.this);
							Cursor cur = cl.getDisasterByKinno(dt.kin_no);
							if (cur != null && cur.getCount() > 0) {
								do {
									try {

										String kin_branch_no = cur.getString(cur
												.getColumnIndex("kin_branch_no"));
										String kin_date = cur.getString(cur
												.getColumnIndex("kin_date"));
										String kin_time = cur.getString(cur
												.getColumnIndex("kin_time"));
										String cre_date = cur.getString(cur
												.getColumnIndex("cre_date"));
										String jusho_name = cur.getString(cur
												.getColumnIndex("jusho_name"));
										String qqsbt2 = cur.getString(cur
												.getColumnIndex("qqsbt2"));
										String qqsbt3 = cur.getString(cur
												.getColumnIndex("qqsbt3"));
										String yousei_level = cur.getString(cur
												.getColumnIndex("yousei_level"));
										Cl_Code code = new Cl_Code(E035.this);
										String codename = code.getMS_CodeName(
												"00066", yousei_level);
										String dmat_yousei_f = cur.getString(cur
												.getColumnIndex("dmat_yousei_f"));
										LinearLayout.LayoutParams param = new LayoutParams(
												LayoutParams.MATCH_PARENT,
												LayoutParams.WRAP_CONTENT);
										LinearLayout layoutitem = new LinearLayout(
												E035.this);
										layoutitem
												.setOrientation(LinearLayout.VERTICAL);
										layoutitem
												.setBackgroundResource(R.drawable.border35);
										layoutitem.setLayoutParams(param);
										layoutitem.setPadding(10, 10, 10, 10);
										LinearLayout layrow1 = new LinearLayout(
												E035.this);
										layrow1.setLayoutParams(param);
										layrow1.setGravity(Gravity.CENTER);

										LinearLayout.LayoutParams paramitem = new LayoutParams(
												LayoutParams.WRAP_CONTENT,
												LayoutParams.WRAP_CONTENT);
										TextView txtKinBranchNo = new TextView(
												E035.this);
										txtKinBranchNo
												.setLayoutParams(paramitem);
										txtKinBranchNo.setText(kin_branch_no);
										txtKinBranchNo.setTextColor(Color
												.parseColor("#000000"));
										layrow1.addView(txtKinBranchNo);
										layoutitem.addView(layrow1);

										LinearLayout layrow2 = new LinearLayout(
												E035.this);
										layrow2.setOrientation(LinearLayout.HORIZONTAL);
										layrow2.setLayoutParams(param);
										TextView txtTitleDate = new TextView(
												E035.this);
										txtTitleDate.setLayoutParams(paramitem);
										txtTitleDate.setText("発生日時：");
										txtTitleDate.setTextColor(Color
												.parseColor("#000000"));
										layrow2.addView(txtTitleDate);
										TextView txtDate = new TextView(
												E035.this);
										txtDate.setLayoutParams(paramitem);
										txtDate.setText(Utilities
												.formatDate(kin_date)
												+ " "
												+ Utilities
														.formatTime(kin_time));
										txtDate.setTextColor(Color
												.parseColor("#000000"));
										layrow2.addView(txtDate);
										layoutitem.addView(layrow2);

										LinearLayout layrow3 = new LinearLayout(
												E035.this);
										layrow3.setOrientation(LinearLayout.HORIZONTAL);
										layrow3.setLayoutParams(param);
										TextView txtTitleCreDate = new TextView(
												E035.this);
										txtTitleCreDate
												.setLayoutParams(paramitem);
										txtTitleCreDate.setText("要請日時：");
										txtTitleCreDate.setTextColor(Color
												.parseColor("#000000"));
										layrow3.addView(txtTitleCreDate);
										TextView txtCreDate = new TextView(
												E035.this);
										txtCreDate.setLayoutParams(paramitem);
										txtCreDate.setText(Utilities
												.formatDateTime(cre_date));
										txtCreDate.setTextColor(Color
												.parseColor("#000000"));
										layrow3.addView(txtCreDate);
										layoutitem.addView(layrow3);

										LinearLayout layrow4 = new LinearLayout(
												E035.this);
										layrow4.setOrientation(LinearLayout.HORIZONTAL);
										layrow4.setLayoutParams(param);
										TextView txtTitleBlock = new TextView(
												E035.this);
										txtTitleBlock
												.setLayoutParams(paramitem);
										txtTitleBlock.setText("連絡地域：");
										txtTitleBlock.setTextColor(Color
												.parseColor("#000000"));
										layrow4.addView(txtTitleBlock);
										layoutitem.addView(layrow4);

										LinearLayout.LayoutParams param5 = new LayoutParams(
												LayoutParams.MATCH_PARENT,
												LayoutParams.WRAP_CONTENT);
										param5.setMargins(10, 0, 0, 0);
										LinearLayout layrow5 = new LinearLayout(
												E035.this);
										layrow5.setOrientation(LinearLayout.HORIZONTAL);
										layrow5.setLayoutParams(param5);
										TextView txtBlock = new TextView(
												E035.this);
										txtBlock.setLayoutParams(paramitem);
										txtBlock.setText("");// chua ro noi dung
										txtBlock.setTextColor(Color
												.parseColor("#000000"));
										layrow5.addView(txtBlock);
										layoutitem.addView(layrow5);

										LinearLayout layrow6 = new LinearLayout(
												E035.this);
										layrow6.setOrientation(LinearLayout.HORIZONTAL);
										layrow6.setLayoutParams(param);
										TextView txtAddress = new TextView(
												E035.this);
										txtAddress.setLayoutParams(paramitem);
										txtAddress.setText("概要／状況");
										txtAddress.setTextColor(Color
												.parseColor("#000000"));
										layrow6.addView(txtAddress);
										layoutitem.addView(layrow6);

										LinearLayout.LayoutParams param7 = new LayoutParams(
												LayoutParams.MATCH_PARENT,
												LayoutParams.WRAP_CONTENT);
										param7.setMargins(20, 0, 0, 0);
										LinearLayout layrow7 = new LinearLayout(
												E035.this);
										layrow7.setOrientation(LinearLayout.HORIZONTAL);
										layrow7.setLayoutParams(param7);
										TextView txtTiteJuso = new TextView(
												E035.this);
										txtTiteJuso.setLayoutParams(paramitem);
										txtTiteJuso.setText("発生場所：");
										txtTiteJuso.setTextColor(Color
												.parseColor("#000000"));
										layrow7.addView(txtTiteJuso);
										TextView txtJuso = new TextView(
												E035.this);
										txtJuso.setLayoutParams(paramitem);
										txtJuso.setText(jusho_name);
										txtJuso.setTextColor(Color
												.parseColor("#000000"));
										layrow7.addView(txtJuso);
										layoutitem.addView(layrow7);

										LinearLayout layrow8 = new LinearLayout(
												E035.this);
										layrow8.setOrientation(LinearLayout.HORIZONTAL);
										layrow8.setLayoutParams(param7);
										TextView txtTiteqqsbt = new TextView(
												E035.this);
										txtTiteqqsbt.setLayoutParams(paramitem);
										txtTiteqqsbt.setText("機関種別：");
										txtTiteqqsbt.setTextColor(Color
												.parseColor("#000000"));
										layrow8.addView(txtTiteqqsbt);
										TextView txtqqsbt = new TextView(
												E035.this);
										txtqqsbt.setLayoutParams(paramitem);
										String qqsbt = "";
										if (qqsbt2 != null
												&& qqsbt2.equals("1")) {
											qqsbt = "2次";
										}
										if (qqsbt3 != null
												&& qqsbt3.equals("1")) {
											if (qqsbt.equals("") == false)
												qqsbt += "、";
											qqsbt += "3次";
										}
										txtqqsbt.setText(qqsbt);
										txtqqsbt.setTextColor(Color
												.parseColor("#000000"));
										layrow8.addView(txtqqsbt);
										layoutitem.addView(layrow8);

										LinearLayout layrow9 = new LinearLayout(
												E035.this);
										layrow9.setOrientation(LinearLayout.HORIZONTAL);
										layrow9.setLayoutParams(param7);
										TextView txtTiteLevel = new TextView(
												E035.this);
										txtTiteLevel.setLayoutParams(paramitem);
										txtTiteLevel.setText("レベル：");
										txtTiteLevel.setTextColor(Color
												.parseColor("#000000"));
										layrow9.addView(txtTiteLevel);
										TextView txtLevel = new TextView(
												E035.this);
										txtLevel.setLayoutParams(paramitem);
										txtLevel.setText(codename);
										txtLevel.setTextColor(Color
												.parseColor("#000000"));
										layrow9.addView(txtLevel);
										layoutitem.addView(layrow9);

										LinearLayout layrow10 = new LinearLayout(
												E035.this);
										layrow10.setOrientation(LinearLayout.HORIZONTAL);
										layrow10.setLayoutParams(param7);
										TextView txtTitedmat = new TextView(
												E035.this);
										txtTitedmat.setLayoutParams(paramitem);
										txtTitedmat.setText("DMAT要請：");
										txtTitedmat.setTextColor(Color
												.parseColor("#000000"));
										layrow10.addView(txtTitedmat);
										TextView txtDmat = new TextView(
												E035.this);
										txtDmat.setLayoutParams(paramitem);
										if (dmat_yousei_f != null
												&& dmat_yousei_f.equals("0"))
											txtDmat.setText("無");
										else if (dmat_yousei_f != null
												&& dmat_yousei_f.equals("1"))
											txtDmat.setText("有");
										txtDmat.setTextColor(Color
												.parseColor("#000000"));
										layrow10.addView(txtDmat);
										layoutitem.addView(layrow10);
										rowDetail.addView(layoutitem);
									} catch (Exception e) {
										// TODO: handle exception
										E022.saveErrorLog(e.getMessage(),
												E035.this);
									}
								} while (cur.moveToNext());
							}

							dt.isOpen = true;
						} else {
							dt.isOpen = false;
							btnDetail.setText("詳細");
							rowDetail.removeAllViews();
						}

					}
				});

				txtKinNo.setText(dt.kin_no);
				txtKinDate.setText(Utilities.formatDate(dt.kin_date) + " "
						+ Utilities.formatTime(dt.kin_time));
				txtTitle.setText(dt.title);
				txtFireStation.setText(dt.firestationame);
				txtTime.setText(String.valueOf(dt.TimeNow) + "秒");
				txtKahi1.setText(String.valueOf(dt.kahi1) + "件");
				txtKahi3.setText(String.valueOf(dt.kahi3) + "件");
				txtKahiNot.setText(String
						.valueOf(dt.kahi - dt.kahi1 - dt.kahi3) + "件");

			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				rowMain.setLayoutParams(param);
				row.setVisibility(View.INVISIBLE);
				rowDetail.setVisibility(View.INVISIBLE);

				rowMain.setBackgroundResource(R.drawable.btnnextrow);

			}

			return v;
		}

	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		Button btn1 = null;
		TextView lable2 = null;
		TextView lable18 = null;
		TextView lable19 = null;
		TextView lable3 = null;
		TextView lable4 = null;
		TextView lable5 = null;
		TextView lable6 = null;
		LinearLayout layoutrow = null;
		LinearLayout layoutrowMain = null;
		LinearLayout layoutrowdetail = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		LinearLayout getRowDetail() {
			if (layoutrowdetail == null) {
				layoutrowdetail = (LinearLayout) base
						.findViewById(R.id.layoutRowDetail);
			}
			return layoutrowdetail;
		}

		LinearLayout getRowMain() {
			if (layoutrowMain == null) {
				layoutrowMain = (LinearLayout) base
						.findViewById(R.id.layoutRowMain);
			}
			return layoutrowMain;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}

		TextView getSTT() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtKinNo);
			}
			return lable1;
		}

		Button getButtonDetail() {
			if (btn1 == null) {
				btn1 = (Button) base.findViewById(R.id.btnDetail);
			}
			return btn1;
		}

		TextView getKinDate() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtKinDate);
			}
			return lable2;
		}

		TextView getTitle() {
			if (lable18 == null) {
				lable18 = (TextView) base.findViewById(R.id.txtTitle);
			}
			return lable18;
		}

		TextView getFireStation() {
			if (lable19 == null) {
				lable19 = (TextView) base.findViewById(R.id.txtFirestation);
			}
			return lable19;
		}

		TextView getTime() {
			if (lable3 == null) {
				lable3 = (TextView) base.findViewById(R.id.txtTime);
			}
			return lable3;
		}

		TextView getkahi1() {
			if (lable4 == null) {
				lable4 = (TextView) base.findViewById(R.id.txtCountkahi1);
			}
			return lable4;
		}

		TextView getkahi3() {
			if (lable5 == null) {
				lable5 = (TextView) base.findViewById(R.id.txtCountkahi3);
			}
			return lable5;
		}

		TextView getKahiNot() {
			if (lable6 == null) {
				lable6 = (TextView) base.findViewById(R.id.txtCountkahiExist);
			}
			return lable6;
		}
	}
}
