﻿package jp.osaka;

import java.math.BigInteger;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.Calendar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utilities {
	public static String converttoStringTime(int time) {

		int h = time / (60 * 60);
		int m = (time - (h * 60 * 60)) / 60;
		int s = time - (h * 60 * 60) - (m * 60);
		if (h > 0)
			return formatNumber(h) + ":" + formatNumber(m) + ":"
					+ formatNumber(s);
		else
			return formatNumber(m) + ":" + formatNumber(s);

	}

	public static String formatTime(int time) {

		int h = time / (60 * 60);
		int m = (time - (h * 60 * 60)) / 60;
		int s = time - (h * 60 * 60) - (m * 60);
		return formatNumber(h) + ":" + formatNumber(m) + ":" + formatNumber(s);

	}

	public static String formatDate(String strdate) {
		if (strdate.length() == 8) {
			strdate = strdate.substring(0, 4) + "/" + strdate.substring(4, 6)
					+ "/" + strdate.substring(6, 8);

		}
		return strdate;
	}

	public static String formatDateTime(String strdate) {
		if (strdate.length() >= 12) {
			strdate = strdate.substring(0, 4) + "/" + strdate.substring(4, 6)
					+ "/" + strdate.substring(6, 8) + " "
					+ strdate.substring(8, 10) + ":"
					+ strdate.substring(10, 12);

		}
		return strdate;
	}

	public static String formatTime(String strTime) {
		if (strTime.length() >= 4) {
			strTime = strTime.substring(0, 2) + ":" + strTime.substring(2, 4);

		}
		return strTime;
	}

	public static String formatNumber(int num) {
		if (num < 10) {
			return "0" + String.valueOf(num);
		}
		return String.valueOf(num);
	}

	public static double roundTwoDecimals(double d) {
		try {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return Double.valueOf(twoDForm.format(d));
		} catch (Exception ex) {
			return d;
		}

	}

	public static String formatNumber(String a) {
		if (a != null && a != "") {
			int num = Integer.parseInt(a);
			if (num < 10) {
				return "0" + String.valueOf(num);
			}
			return String.valueOf(num);
		} else {
			return "00";
		}
	}

	public static String format4Number(int num) {

		if (num < 10) {
			return "000" + String.valueOf(num);
		} else if (num < 100) {
			return "00" + String.valueOf(num);
		} else if (num < 1000) {
			return "0" + String.valueOf(num);
		}
		return String.valueOf(num);
	}

	public static String format4Number(String a) {
		if (a != null && a != "") {
			int num = Integer.parseInt(a);
			if (num < 10) {
				return "000" + String.valueOf(num);
			} else if (num < 100) {
				return "00" + String.valueOf(num);
			} else if (num < 1000) {
				return "0" + String.valueOf(num);
			}
			return String.valueOf(num);
		}
		return "0000";
	}

	public static String hashPassword(String password) {
		String hashword = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(password.getBytes());
			BigInteger hash = new BigInteger(1, md5.digest());
			hashword = hash.toString(16);
		} catch (NoSuchAlgorithmException nsae) {
			// ignore
		}
		return hashword;
	}

	public static String getDateTimeNow() {

		String datetime = null;
		Calendar c = Calendar.getInstance();
		datetime = format4Number(c.get(Calendar.YEAR))
				+ formatNumber(c.get(Calendar.MONTH) + 1)
				+ formatNumber(c.get(Calendar.DAY_OF_MONTH))
				+ formatNumber(c.get(Calendar.HOUR_OF_DAY))
				+ formatNumber(c.get(Calendar.MINUTE))
				+ formatNumber(c.get(Calendar.SECOND));
		return datetime;

	}

	// B535 20130131
	public static String getDateTimeNow(Calendar c) {

		String datetime = null;
		// Calendar c = Calendar.getInstance();
		datetime = format4Number(c.get(Calendar.YEAR))
				+ formatNumber(c.get(Calendar.MONTH) + 1)
				+ formatNumber(c.get(Calendar.DAY_OF_MONTH))
				+ formatNumber(c.get(Calendar.HOUR_OF_DAY))
				+ formatNumber(c.get(Calendar.MINUTE))
				+ formatNumber(c.get(Calendar.SECOND));
		return datetime;

	}

	public static String getTimeNow() {

		String datetime = null;
		Calendar c = Calendar.getInstance();
		datetime = formatNumber(c.get(Calendar.HOUR_OF_DAY))
				+ formatNumber(c.get(Calendar.MINUTE))
				+ formatNumber(c.get(Calendar.SECOND));
		return datetime;

	}

	public static String getDateNow() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = format4Number(c.get(Calendar.YEAR))
				+ formatNumber(c.get(Calendar.MONTH) + 1)
				+ formatNumber(c.get(Calendar.DAY_OF_MONTH));
		return date;

	}

	public static String getYYYY() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = format4Number(c.get(Calendar.YEAR));
		return date;

	}

	public static String getYY() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = format4Number(c.get(Calendar.YEAR));
		return date.substring(2, 4);

	}

	public static String getY() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = format4Number(c.get(Calendar.YEAR));
		return date.substring(3, 4);

	}

	public static String getMM() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = formatNumber(c.get(Calendar.MONTH) + 1);
		return date;

	}

	public static String getDD() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = formatNumber(c.get(Calendar.DAY_OF_MONTH));
		return date;

	}

	public static String getDateNext() {

		String date = null;
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 1);
		date = format4Number(c.get(Calendar.YEAR))
				+ formatNumber(c.get(Calendar.MONTH) + 1)
				+ formatNumber(c.get(Calendar.DAY_OF_MONTH));
		return date;

	}

	public static String getMonthDateNow() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = formatNumber(c.get(Calendar.MONTH) + 1)
				+ formatNumber(c.get(Calendar.DAY_OF_MONTH));
		return date;

	}

	public static String getDayOfWeek() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
		// return "0000" + date;
		return date;
	}

	public static String getDayNextOfWeek() {

		String date = null;
		Calendar c = Calendar.getInstance();
		date = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
		if (date.equals("7")) {
			date = "1";
		} else {
			int day = c.get(Calendar.DAY_OF_WEEK);
			day = day + 1;
			date = String.valueOf(day);
		}
		// return "0000" + date;
		return date;
	}

	public static boolean IsNumber(String a) {

		try {
			Integer.parseInt(a);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	public static class TrustManagerManipulator implements X509TrustManager {

		private static TrustManager[] trustManagers = null;
		private static final X509Certificate[] acceptedIssuers = new X509Certificate[] {};

		public boolean isClientTrusted(X509Certificate[] chain) {
			return true;
		}

		public boolean isServerTrusted(X509Certificate[] chain) {
			return true;
		}

		public static void allowAllSSL() {
			HttpsURLConnection
					.setDefaultHostnameVerifier(new HostnameVerifier() {

						public boolean verify(String hostname,
								SSLSession session) {
							// TODO Auto-generated method stub
							return true;
						}

					});
			SSLContext context = null;
			if (trustManagers == null) {
				trustManagers = new TrustManager[] { new TrustManagerManipulator() };
			}
			try {
				context = SSLContext.getInstance("TLS");
				context.init(null, trustManagers, new SecureRandom());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (KeyManagementException e) {
				e.printStackTrace();
			}
			HttpsURLConnection.setDefaultSSLSocketFactory(context
					.getSocketFactory());
		}

		public void checkClientTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return acceptedIssuers;
		}
	}
}
