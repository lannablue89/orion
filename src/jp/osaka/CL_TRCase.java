﻿package jp.osaka;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.TR_CaseCount;

public class CL_TRCase {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_TRCase(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	// TRCase
	public long createTRCase(String FireStationCode, String CaseCode,
			String JianNo, String EMSUnitCode, String IncidentScene,
			String Latitude_IS, String Longitude_IS, String Sex, String Age,
			String VitalSign_JCS, String VitalSign_GCS_E,
			String VitalSign_GCS_V, String VitalSign_GCS_M,
			String VitalSign_Pulse, String VitalSign_Breath,
			String VitalSign_BloodPressure, String VitalSign_SpO2,
			String VitalSign_Temperature, String PatientLevel_CD,
			String Notice_CD, String Notice_TEXT, String NotTransportation_CD,
			String NotTransportation_TEXT, String SearchType_CD,
			String BedStatus_MALE, String BedStatus_FEMALE, String Status,
			String DeleteFLG, String ContactCount) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put(TR_CaseCount.FireStationCode, FireStationCode);
			values.put(TR_CaseCount.CaseCode, CaseCode);
			values.put(TR_CaseCount.JianNo, JianNo);
			values.put(TR_CaseCount.EMSUnitCode, EMSUnitCode);
			values.put(TR_CaseCount.IncidentScene, IncidentScene);
			values.put(TR_CaseCount.Latitude_IS, Latitude_IS);
			values.put(TR_CaseCount.Longitude_IS, Longitude_IS);
			values.put(TR_CaseCount.Sex, Sex);
			values.put(TR_CaseCount.Age, Age);
			values.put(TR_CaseCount.VitalSign_JCS, VitalSign_JCS);
			values.put(TR_CaseCount.VitalSign_GCS_E, VitalSign_GCS_E);
			values.put(TR_CaseCount.VitalSign_GCS_V, VitalSign_GCS_V);
			values.put(TR_CaseCount.VitalSign_GCS_M, VitalSign_GCS_M);
			values.put(TR_CaseCount.VitalSign_Pulse, VitalSign_Pulse);
			values.put(TR_CaseCount.VitalSign_Breath, VitalSign_Breath);
			values.put(TR_CaseCount.VitalSign_BloodPressure,
					VitalSign_BloodPressure);
			values.put(TR_CaseCount.VitalSign_SpO2, VitalSign_SpO2);
			values.put(TR_CaseCount.VitalSign_Temperature,
					VitalSign_Temperature);
			values.put(TR_CaseCount.PatientLevel_CD, PatientLevel_CD);
			values.put(TR_CaseCount.Notice_CD, Notice_CD);
			values.put(TR_CaseCount.Notice_TEXT, Notice_TEXT);
			values.put(TR_CaseCount.NotTransportation_CD, NotTransportation_CD);
			values.put(TR_CaseCount.NotTransportation_TEXT,
					NotTransportation_TEXT);
			values.put(TR_CaseCount.SearchType_CD, SearchType_CD);
			values.put(TR_CaseCount.BedStatus_MALE, BedStatus_MALE);
			values.put(TR_CaseCount.BedStatus_FEMALE, BedStatus_FEMALE);
			values.put(TR_CaseCount.Status, Status);
			values.put(TR_CaseCount.DeleteFLG, DeleteFLG);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Insert_DATETIME, indate);
			values.put(TR_CaseCount.Update_DATETIME, indate);
			values.put(TR_CaseCount.ContactCount, ContactCount);
			result = database.insert(TR_CaseCount.TableName, null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long createTRCase(ContentValues values) {
		long result = -1;
		try {
			result = database.insert(TR_CaseCount.TableName, null, values);
			// database.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor fetchTRCase(String Insert_DATETIME, String limit) {
		Cursor mCursor = null;
		try {
			String[] args = { Insert_DATETIME };
			String sql = "Select a.* from TR_Case a Inner Join M_User b On a.EMSUnitCode=b.UserName Where  a.Insert_DATETIME<? and a.Status='0' and a.DeleteFLG=0 order by a.Insert_DATETIME DESC  Limit "
					+ limit;
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}

		return mCursor;
	}

	public Cursor fetchTRCaseInActy(String Insert_DATETIME, String limit) {
		Cursor mCursor = null;
		try {
			String[] args = { Insert_DATETIME };
			String sql = "Select a.* from TR_Case a Inner Join M_User b On a.EMSUnitCode=b.UserName Where a.Insert_DATETIME<? and a.Status='1' and a.DeleteFLG=0  order by a.Insert_DATETIME DESC  Limit "
					+ limit;
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getTR_CaseFirst(String Status) {
		String insertDate = "99999999999999";
		Cursor mCursor = null;
		try {
			String[] args = { Status };
			String sql = "Select a.Insert_DATETIME  from TR_Case a Inner Join M_User b On a.EMSUnitCode=b.UserName Where a.Status=? and a.DeleteFLG=0  order by a.Insert_DATETIME ASC ";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					insertDate = mCursor.getString(mCursor
							.getColumnIndex("Insert_DATETIME"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return insertDate;
	}

	public int updateStatusTRCase(String FireStationCode, String CaseCode,
			String Status) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(TR_CaseCount.Status, Status);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateColumsTRCase(String FireStationCode, String CaseCode,
			String Col, String value) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			if (value != null && !value.equals("")) {
				values.put(Col, value);
			} else {
				values.putNull(Col);
			}
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateGCSTRCaseNull(String FireStationCode, String CaseCode) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.putNull(TR_CaseCount.VitalSign_GCS_E);
			values.putNull(TR_CaseCount.VitalSign_GCS_V);
			values.putNull(TR_CaseCount.VitalSign_GCS_M);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateE007TRCase(String FireStationCode, String CaseCode,
			String codeId, String note) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			if (codeId != null && !codeId.equals("")) {
				values.put(TR_CaseCount.NotTransportation_CD, codeId);
			} else {
				values.putNull("NotTransportation_CD");
			}
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			if (codeId.equals(BaseCount.Other) && note != null
					&& !note.equals("")) {
				values.put(TR_CaseCount.NotTransportation_TEXT, note);
			} else {

				values.putNull(TR_CaseCount.NotTransportation_TEXT);
			}
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateE008CheckBoxTRCase(String FireStationCode,
			String CaseCode, String col, String codeId, String note) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(col, codeId);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			if (codeId.equals(BaseCount.Other) && note != null
					&& !note.equals("")) {
				values.put(TR_CaseCount.NotTransportation_TEXT, note);
			} else {

				values.putNull(TR_CaseCount.NotTransportation_TEXT);
			}
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getItemTRcase(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}

		return mCursor;

	}

	public Cursor getItemTRcaseUpdateServer(String FireStationCode,
			String CaseCode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}

		return mCursor;

	}

	public boolean checkExistTRcase(String FireStationCode, String CaseCode) {

		boolean result = false;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=? ";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0)
				result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public int deleteTRCase(String FireStationCode, String CaseCode) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(TR_CaseCount.DeleteFLG, "1");
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=?";
			String[] args = { FireStationCode, CaseCode };

			result = database.update(TR_CaseCount.TableName, values, where,
					args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	// public Cursor getMaxJAN(String FireStationCode, String Date) {
	// // String[] cols = new String[] { M_UserCount.Id, M_UserCount.UserName,
	// // M_UserCount.Password, M_UserCount.Status };
	// Cursor mCursor = null;
	// try {
	// String[] args = { FireStationCode, Date };
	// String sql =
	// "Select max(substr(CaseCode,9,4)) as CaseCode2 from TR_Case Where FireStationCode=? and substr(CaseCode,0,9)=?";
	// mCursor = database.rawQuery(sql, args);
	// if (mCursor != null) {
	// mCursor.moveToFirst();
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// } finally {
	// database.close();
	// }
	// return mCursor;
	// }
	public String getMaxJAN(String FireStationCode, String Date) {
		// String[] cols = new String[] { M_UserCount.Id, M_UserCount.UserName,
		// M_UserCount.Password, M_UserCount.Status };
		String stt = "0001";
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, Date };
			String sql = "Select max(substr(CaseCode,19,4)) as CaseCode2 from TR_Case Where FireStationCode=? and substr(CaseCode,0,19)=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor != null && mCursor.getCount() > 0) {
					String casecode2 = mCursor.getString(mCursor
							.getColumnIndex("CaseCode2"));
					if (casecode2 != null && !casecode2.equals("")) {
						if (!casecode2.equals("9999")) {
							int casec = Integer.parseInt(casecode2);
							stt = Utilities.format4Number(casec + 1);

						}
					} else {
						// sql =
						// "Select max(substr(CaseNo,19,4)) as CaseCode2 from TR_MedicalInstitutionAdmission Where FireStationCode=? and substr(CaseNo,0,19)=?";
						// mCursor = database.rawQuery(sql, args);
						// if (mCursor != null) {
						// mCursor.moveToFirst();
						// if (mCursor != null && mCursor.getCount() > 0) {
						// casecode2 = mCursor.getString(mCursor
						// .getColumnIndex("CaseCode2"));
						//
						// if (casecode2 != null && !casecode2.equals("")) {
						// if (!casecode2.equals("9999")) {
						// int casec = Integer.parseInt(casecode2);
						// stt = Utilities
						// .format4Number(casec + 1);
						//
						// }
						// }
						// }
						// }
					}
				} else {
					// sql =
					// "Select max(substr(CaseNo,19,4)) as CaseCode2 from TR_MedicalInstitutionAdmission Where FireStationCode=? and substr(CaseNo,0,19)=?";
					// mCursor = database.rawQuery(sql, args);
					// if (mCursor != null) {
					// mCursor.moveToFirst();
					// if (mCursor != null && mCursor.getCount() > 0) {
					// String casecode2 = mCursor.getString(mCursor
					// .getColumnIndex("CaseCode2"));
					// if (casecode2 != null && !casecode2.equals("")) {
					// if (!casecode2.equals("9999")) {
					// int casec = Integer.parseInt(casecode2);
					// stt = Utilities.format4Number(casec + 1);
					//
					// }
					// }
					// }
					// }
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return stt;
	}

	public int getMaxJANNumber(String FireStationCode, String Date) {
		// String[] cols = new String[] { M_UserCount.Id, M_UserCount.UserName,
		// M_UserCount.Password, M_UserCount.Status };
		int stt = 1;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, Date };
			String sql = "Select max(substr(CaseCode,19,4)) as CaseCode2 from TR_Case Where FireStationCode=? and substr(CaseCode,0,19)=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor != null && mCursor.getCount() > 0) {
					String casecode2 = mCursor.getString(mCursor
							.getColumnIndex("CaseCode2"));

					if (casecode2 != null && !casecode2.equals("")) {
						if (!casecode2.equals("9999")) {
							stt = Integer.parseInt(casecode2);

						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return stt;
	}

	public int getOutbound_No(String FireStationCode, String CaseCode) {

		int stt = 1;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select Outbound_No from TR_Case Where FireStationCode=? and CaseCode=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor != null && mCursor.getCount() > 0) {
					String Outbound_No = mCursor.getString(mCursor
							.getColumnIndex("Outbound_No"));

					if (Outbound_No != null && !Outbound_No.equals("")) {
						stt = Integer.parseInt(Outbound_No);
					} else {
						sql = "Select max(Outbound_No) as ONo from TR_Case where Outbound_No is not null";
						mCursor = database.rawQuery(sql, args);
						if (mCursor != null) {
							mCursor.moveToFirst();
							if (mCursor != null && mCursor.getCount() > 0) {
								Outbound_No = mCursor.getString(mCursor
										.getColumnIndex("ONo"));
								if (Outbound_No != null
										&& !Outbound_No.equals("")) {
									stt = Integer.parseInt(Outbound_No);
								}

							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return stt;
	}

	// TR_MedicalInstitutionAdmission
	// Get MIName_Kanji
	public Cursor getMIName_Kanji(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select MS_MedicalInstitution.MIName_Kanji,MS_MedicalInstitution.Address_Kanji,TR_ContactResultRecord.ContactCount from TR_ContactResultRecord Inner Join MS_MedicalInstitution  on MS_MedicalInstitution.MICode=TR_ContactResultRecord.MICode  Where TR_ContactResultRecord.ContactResult_CD='00001' and  TR_ContactResultRecord.FireStationCode=? and TR_ContactResultRecord.CaseNo=? Order by TR_ContactResultRecord.ContactCount DESC Limit 1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public boolean deleteAll() {
		boolean result = false;
		try {
			result = database.delete(TR_CaseCount.TableName, null, null) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public void close() {
		dbHelper.close();
	}

	// 20121006: bug 80
	public int updateAddressJIAN(String FireStationCode, String CaseCode,
			String lat, String longi, String Address) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			if (Address != null)
				values.put(TR_CaseCount.IncidentScene, Address);
			else
				values.putNull(TR_CaseCount.IncidentScene);
			values.put(TR_CaseCount.Latitude_IS, lat);
			values.put(TR_CaseCount.Longitude_IS, longi);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	// end
	public String getTR_URL(String FireStationCode, String CaseCode) {
		String url = "";
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select a.*,b.TerminalPassword from TR_Case a Inner Join MS_FireStation b On a.FireStationCode=b.FireStationCode Where a.FireStationCode=? and a.CaseCode=? and b.DeleteFlg=0";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					url += "p="
							+ mCursor.getString(mCursor
									.getColumnIndex("TerminalPassword"));
					if (mCursor.getString(mCursor.getColumnIndex("Age")) != null)
						url += "&age="
								+ mCursor.getString(mCursor
										.getColumnIndex("Age"));
					else
						url += "&age=";
					if (mCursor.getString(mCursor.getColumnIndex("Sex")) != null)
						url += "&sex="
								+ mCursor.getString(mCursor
										.getColumnIndex("Sex"));
					else
						url += "&sex=";
					if (mCursor.getString(mCursor
							.getColumnIndex("VitalSign_JCS")) != null) {
						String jcs = mCursor.getString(mCursor
								.getColumnIndex("VitalSign_JCS"));
						String[] args1 = { jcs };
						sql = "Select * from MS_Code Where CodeType='00009' and CodeID=?";
						Cursor mCursor2 = database.rawQuery(sql, args1);
						if (mCursor2 != null && mCursor2.getCount() > 0) {
							mCursor2.moveToFirst();
							url += "&jcs="
									+ mCursor2.getString(mCursor2
											.getColumnIndex("CodeName"));
						} else {
							url += "&jcs=";
						}
					} else
						url += "&jcs=";
					if (mCursor.getString(mCursor
							.getColumnIndex("VitalSign_Pulse")) != null)
						url += "&pul="
								+ mCursor.getString(mCursor
										.getColumnIndex("VitalSign_Pulse"));
					else
						url += "&pul=";
					if (mCursor.getString(mCursor
							.getColumnIndex("VitalSign_Breath")) != null)
						url += "&bre="
								+ mCursor.getString(mCursor
										.getColumnIndex("VitalSign_Breath"));
					else
						url += "&bre=";
					if (mCursor.getString(mCursor
							.getColumnIndex("VitalSign_BloodPressure")) != null)
						url += "&bp="
								+ mCursor
										.getString(mCursor
												.getColumnIndex("VitalSign_BloodPressure"));
					else
						url += "&bp=";

					sql = "Select Max(ContactCount) as ContactCount from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? ";
					mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {
						mCursor.moveToFirst();
						if (mCursor.getString(mCursor
								.getColumnIndex("ContactCount")) != null) {
							url += "&cc="
									+ mCursor.getString(mCursor
											.getColumnIndex("ContactCount"));
						} else {
							url += "&cc=0";
						}
					} else {
						url += "&cc=0";
					}

					String time;
					sql = "Select Movement_DATETIME from TR_MovementTime Where  FireStationCode=? and CaseNo=?  and MovementCode='00001'";
					mCursor = database.rawQuery(sql, args);
					if (mCursor != null) {
						mCursor.moveToFirst();
						if (mCursor.getCount() > 0) {
							time = mCursor.getString(mCursor
									.getColumnIndex("Movement_DATETIME"));
							if (time != null && !time.equals("")) {
								DateFormat formatter = new SimpleDateFormat(
										BaseCount.DateTimeFormat);
								Date date1 = (Date) formatter.parse(time);
								Date date2 = (Date) formatter.parse(Utilities
										.getDateTimeNow());
								long diff = date2.getTime() - date1.getTime();
								double minute = diff * 1.0 / (1000 * 60);
								long m = Math.round(minute);
								url += "&ti=" + String.valueOf(m);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return url;
	}

	// Check for Kanyou : Bug 98
	public boolean checkKanyou(String FireStationCode, String CaseCode) {

		boolean result = false;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=? ";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				String notTran = mCursor.getString(mCursor
						.getColumnIndex(TR_CaseCount.NotTransportation_CD));
				if (notTran != null && !notTran.equals("")) {
					return true;
				} else {
					sql = "Select * from TR_ContactResultRecord Where ContactResult_CD='00001' and FireStationCode=? and CaseNo=? ";
					mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {

						return true;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public boolean checkLocationJian(String FireStationCode, String CaseCode) {

		boolean result = false;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=? ";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				String loc = mCursor.getString(mCursor
						.getColumnIndex(TR_CaseCount.IncidentScene));
				if (loc != null && !loc.equals("")) {
					return true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public boolean checkJianNo(String FireStationCode, String CaseCode,
			String JianNo) {

		boolean result = false;
		try {
			String[] args = { FireStationCode, CaseCode, JianNo };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode!=? and JianNo=? ";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				result = true;

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public boolean checkJianNo(String JianNo, String FireStationCode) {

		boolean result = false;
		try {
			String[] args = { JianNo, FireStationCode };
			String sql = "Select * from TR_Case where JianNo=? and FireStationCode=?";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				result = true;

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public int changeCase(String FireStationCode, String CaseCode,
			String NewCaseCode) {
		int result = 0;
		database.beginTransaction();
		try {
			ContentValues values = new ContentValues();
			values.put(TR_CaseCount.CaseCode, NewCaseCode);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });

			where = "FireStationCode=? and CaseNo=?";
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_Patient", values, where, new String[] {
					FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_MovementTime", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_FireStationStandard", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_MedicalInstitutionAdmission", values,
					where, new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_SelectionKamoku", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_ContactResultRecord", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_FirstAid", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_FileManageInfo", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_AllReqMedicalInstitution", values,
					where, new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_AllReqMedicalInstitution_Result",
					values, where, new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriagePATMethod", values, where,
					new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriagePATMethod_Result", values,
					where, new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriageSTARTMethod", values, where,
					new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriageSTARTMethod_Result", values,
					where, new String[] { FireStationCode, CaseCode });

			database.setTransactionSuccessful();
			result = 1;
		} catch (Exception e) {
			// TODO: handle exception
			result = 0;
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return result;
	}

	public int changeJIAN(String FireStationCode, String CaseCode,
			String NewCaseCode, String JianNo) {
		int result = 0;
		database.beginTransaction();
		try {
			ContentValues values = new ContentValues();
			values.put(TR_CaseCount.CaseCode, NewCaseCode);
			if (JianNo != null && !JianNo.equals("")) {
				values.put(TR_CaseCount.JianNo, JianNo);
			} else {
				values.putNull(TR_CaseCount.JianNo);
			}
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);
			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });

			where = "FireStationCode=? and CaseNo=?";
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_Patient", values, where, new String[] {
					FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_MovementTime", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_FireStationStandard", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_MedicalInstitutionAdmission", values,
					where, new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_SelectionKamoku", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_ContactResultRecord", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			values.put(BaseCount.Update_DATETIME, indate);
			result = database.update("TR_FirstAid", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_FileManageInfo", values, where,
					new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_AllReqMedicalInstitution", values,
					where, new String[] { FireStationCode, CaseCode });

			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_AllReqMedicalInstitution_Result",
					values, where, new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriagePATMethod", values, where,
					new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriagePATMethod_Result", values,
					where, new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriageSTARTMethod", values, where,
					new String[] { FireStationCode, CaseCode });
			values = new ContentValues();
			values.put("CaseNo", NewCaseCode);
			result = database.update("TR_TriageSTARTMethod_Result", values,
					where, new String[] { FireStationCode, CaseCode });

			database.setTransactionSuccessful();
			result = 1;
		} catch (Exception e) {
			// TODO: handle exception
			result = 0;
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return result;
	}

	public boolean deleteJIAN(String FireStationCode, String CaseNo) {
		boolean result = false;
		database.beginTransaction();
		try {
			String where = "FireStationCode=? and CaseCode=? ";
			String[] args = { FireStationCode, CaseNo };
			result = database.delete("TR_Case", where, args) > 0;

			where = "FireStationCode=? and CaseNo=? ";
			result = database.delete("TR_ContactResultRecord", where, args) > 0;
			result = database.delete("TR_FireStationStandard", where, args) > 0;
			result = database.delete("TR_Patient", where, args) > 0;
			result = database.delete("TR_MovementTime", where, args) > 0;
			result = database.delete("TR_FirstAid", where, args) > 0;
			result = database.delete("TR_SelectionKamoku", where, args) > 0;
			result = database.delete("TR_MedicalInstitutionAdmission", where,
					args) > 0;
			database.setTransactionSuccessful();
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			result = false;
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return result;
	}

	public DTO_TRCase CopyJIAN(String FireStationCode, String CaseCode,
			String NewCaseCode) {
		// boolean result = false;
		DTO_TRCase dt = new DTO_TRCase();
		database.beginTransaction();
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_Case where FireStationCode=? and CaseCode=?";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				String EMSUnitCode = mCursor.getString(mCursor
						.getColumnIndex("EMSUnitCode"));
				String IncidentScene = mCursor.getString(mCursor
						.getColumnIndex("IncidentScene"));
				String Latitude_IS = mCursor.getString(mCursor
						.getColumnIndex("Latitude_IS"));
				String Longitude_IS = mCursor.getString(mCursor
						.getColumnIndex("Longitude_IS"));
				ContentValues values = new ContentValues();
				values.put(TR_CaseCount.FireStationCode, FireStationCode);
				values.put(TR_CaseCount.CaseCode, NewCaseCode);
				values.put(TR_CaseCount.EMSUnitCode, EMSUnitCode);
				if (IncidentScene != null)
					values.put(TR_CaseCount.IncidentScene, IncidentScene);
				if (Latitude_IS != null)
					values.put(TR_CaseCount.Latitude_IS, Latitude_IS);
				if (Longitude_IS != null)
					values.put(TR_CaseCount.Longitude_IS, Longitude_IS);
				values.put(TR_CaseCount.Status, "0");
				values.put(TR_CaseCount.DeleteFLG, "0");
				values.put(TR_CaseCount.ContactCount, "0");
				String indate = Utilities.getDateTimeNow();
				values.put(TR_CaseCount.Insert_DATETIME, indate);
				values.put(TR_CaseCount.Update_DATETIME, indate);
				long i = database.insert(TR_CaseCount.TableName, null, values);
				if (i > 0) {
					dt.setFireStationCode(FireStationCode);
					dt.setCaseCode(NewCaseCode);
					dt.setIncidentScene(IncidentScene);
					dt.setInsert_DATETIME(indate);
					sql = "Select * from TR_MovementTime where FireStationCode=? and CaseNo=? and MovementCode='00001'";
					mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {
						mCursor.moveToFirst();
						String Movement_DATETIME = mCursor.getString(mCursor
								.getColumnIndex("Movement_DATETIME"));
						values = new ContentValues();
						values.put("FireStationCode", FireStationCode);
						values.put("CaseNo", NewCaseCode);
						values.put("MovementCode", "00001");
						values.put("Movement_DATETIME", Movement_DATETIME);
						values.put(BaseCount.Update_DATETIME,
								Utilities.getDateTimeNow());
						i = database.insert("TR_MovementTime", null, values);
					}
				}
			}
			database.setTransactionSuccessful();
			// result = true;
		} catch (Exception e) {
			// TODO: handle exception
			// result = false;
			dt = null;
			e.printStackTrace();

		} finally {
			database.endTransaction();
			database.close();
		}
		return dt;
	}

	public int ClearKinNo(String FireStationCode, String CaseCode) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.putNull("kin_no");
			values.putNull("kin_branch_no");
			values.putNull("kinLink_DateTime");
			values.putNull("kin_kbn");
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int LinkToKinNo(String FireStationCode, String CaseCode,
			String kin_no, String kin_branch_no) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put("kin_no", kin_no);
			values.put("kin_branch_no", kin_branch_no);
			String indate = Utilities.getDateTimeNow();
			values.put("kinLink_DateTime", indate);
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=?";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateKin_kbn(String FireStationCode, String CaseCode,
			String kin_kbn) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();

			values.put("kin_kbn", kin_kbn);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=? and (kin_kbn='' or kin_kbn is null or kin_kbn>=?)";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode, kin_kbn });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public String getKin_kbn(String FireStationCode, String CaseCode) {
		String result = "";
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			mCursor = database
					.rawQuery(
							"Select * from TR_Case Where FireStationCode=? and CaseCode=?",
							args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor.getColumnIndex("kin_kbn")) != null) {
					result = mCursor.getString(mCursor
							.getColumnIndex("kin_kbn"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return result;
	}

	public int updateSitearrival_datetime(String FireStationCode,
			String CaseCode, String time) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();

			values.put("sitearrival_datetime", time);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=? ";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateDoctortakeover_datetime(String FireStationCode,
			String CaseCode, String time) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();

			values.put("doctortakeover_datetime", time);
			String indate = Utilities.getDateTimeNow();
			values.put(TR_CaseCount.Update_DATETIME, indate);

			String where = "FireStationCode=? and CaseCode=? ";
			result = database.update(TR_CaseCount.TableName, values, where,
					new String[] { FireStationCode, CaseCode });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public String getMICode() {
		String MICode = "";
		Cursor mCursor = null;
		try {
			String sql = "Select FacilitiesCode from TR_Case order by Update_DATETIME DESC Limit 1";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FacilitiesCode")) != null) {
					MICode = mCursor.getString(mCursor
							.getColumnIndex("FacilitiesCode"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
		} finally {
			database.close();
		}
		return MICode;
	}

	public Cursor getHopital(String MICode) {

		Cursor mCursor = null;
		try {
			String[] args = { MICode };
			String sql = "Select * from MS_MedicalInstitution from MICode=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();

			}
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
