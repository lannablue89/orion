package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_ModelSetting {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_ModelSetting(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public Cursor getModelSetting(String Model) {
		Cursor mCursor = null;
		try {
			String[] args = { Model };
			String sql = "Select * from MS_ModelSetting where Model_Name=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
