﻿package jp.osaka;

import java.io.Serializable;

public class DTO_TRCase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5492749173003375228L;
	private String FireStationCode;
	private String CaseCode;
	private String JianNo;
	private String EMSUnitCode;
	private String IncidentScene;
	private String Latitude_IS;
	private String Longitude_IS;
	private String Sex;
	private String Age;
	private String VitalSign_JCS;
	private String VitalSign_GCS_E;
	private String VitalSign_GCS_V;
	private String VitalSign_GCS_M;
	private String VitalSign_Pulse;
	private String VitalSign_Breath;
	private String VitalSign_BloodPressure;
	private String VitalSign_SpO2;
	private String VitalSign_Temperature;
	private String PatientLevel_CD;
	private String Notice_CD;
	private String Notice_TEXT;
	private String NotTransportation_CD;
	private String NotTransportation_TEXT;
	private String SearchType_CD;
	private String BedStatus_MALE;
	private String BedStatus_FEMALE;
	private String UploadFLG;
	private String DeleteFLG;
	private String Insert_DATETIME;
	private String Update_DATETIME;
	private String CaseCode1;
	private String CaseCode2;
	private String CaseCode3;
	private String CaseCode4;
	private String MIName_Kanji;
	private String Address_Kanji;
	private static String ContactCount;
	
	public String getContactCount() {
		return ContactCount;
	}

	public void setContactCount(String contactCount) {
		ContactCount = contactCount;
	}

	public String getFireStationCode() {
		return FireStationCode;
	}

	public void setFireStationCode(String fireStationCode) {
		FireStationCode = fireStationCode;
	}

	public String getCaseCode() {
		return CaseCode;
	}

	public void setCaseCode(String caseCode) {
		CaseCode = caseCode;
	}

	public String getEMSUnitCode() {
		return EMSUnitCode;
	}

	public void setEMSUnitCode(String eMSUnitCode) {
		EMSUnitCode = eMSUnitCode;
	}

	public String getIncidentScene() {
		return IncidentScene;
	}

	public void setIncidentScene(String incidentScene) {
		IncidentScene = incidentScene;
	}

	public String getLatitude_IS() {
		return Latitude_IS;
	}

	public void setLatitude_IS(String latitude_IS) {
		Latitude_IS = latitude_IS;
	}

	public String getLongitude_IS() {
		return Longitude_IS;
	}

	public void setLongitude_IS(String longitude_IS) {
		Longitude_IS = longitude_IS;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getVitalSign_JCS() {
		return VitalSign_JCS;
	}

	public void setVitalSign_JCS(String vitalSign_JCS) {
		VitalSign_JCS = vitalSign_JCS;
	}

	public String getVitalSign_GCS_E() {
		return VitalSign_GCS_E;
	}

	public void setVitalSign_GCS_E(String vitalSign_GCS_E) {
		VitalSign_GCS_E = vitalSign_GCS_E;
	}

	public String getVitalSign_GCS_V() {
		return VitalSign_GCS_V;
	}

	public void setVitalSign_GCS_V(String vitalSign_GCS_V) {
		VitalSign_GCS_V = vitalSign_GCS_V;
	}

	public String getVitalSign_GCS_M() {
		return VitalSign_GCS_M;
	}

	public void setVitalSign_GCS_M(String vitalSign_GCS_M) {
		VitalSign_GCS_M = vitalSign_GCS_M;
	}

	public String getVitalSign_Pulse() {
		return VitalSign_Pulse;
	}

	public void setVitalSign_Pulse(String vitalSign_Pulse) {
		VitalSign_Pulse = vitalSign_Pulse;
	}

	public String getVitalSign_Breath() {
		return VitalSign_Breath;
	}

	public void setVitalSign_Breath(String vitalSign_Breath) {
		VitalSign_Breath = vitalSign_Breath;
	}

	public String getVitalSign_BloodPressure() {
		return VitalSign_BloodPressure;
	}

	public void setVitalSign_BloodPressure(String vitalSign_BloodPressure) {
		VitalSign_BloodPressure = vitalSign_BloodPressure;
	}

	public String getVitalSign_SpO2() {
		return VitalSign_SpO2;
	}

	public void setVitalSign_SpO2(String vitalSign_SpO2) {
		VitalSign_SpO2 = vitalSign_SpO2;
	}

	public String getVitalSign_Temperature() {
		return VitalSign_Temperature;
	}

	public void setVitalSign_Temperature(String vitalSign_Temperature) {
		VitalSign_Temperature = vitalSign_Temperature;
	}

	public String getPatientLevel_CD() {
		return PatientLevel_CD;
	}

	public void setPatientLevel_CD(String patientLevel_CD) {
		PatientLevel_CD = patientLevel_CD;
	}

	public String getNotice_CD() {
		return Notice_CD;
	}

	public void setNotice_CD(String notice_CD) {
		Notice_CD = notice_CD;
	}

	public String getNotTransportation_CD() {
		return NotTransportation_CD;
	}

	public void setNotTransportation_CD(String notTransportation_CD) {
		NotTransportation_CD = notTransportation_CD;
	}

	public String getNotice_TEXT() {
		return Notice_TEXT;
	}

	public void setNotice_TEXT(String notice_TEXT) {
		Notice_TEXT = notice_TEXT;
	}

	public String getNotTransportation_TEXT() {
		return NotTransportation_TEXT;
	}

	public void setNotTransportation_TEXT(String notTransportation_TEXT) {
		NotTransportation_TEXT = notTransportation_TEXT;
	}

	public String getSearchType_CD() {
		return SearchType_CD;
	}

	public void setSearchType_CD(String searchType_CD) {
		SearchType_CD = searchType_CD;
	}

	public String getBedStatus_MALE() {
		return BedStatus_MALE;
	}

	public void setBedStatus_MALE(String bedStatus_MALE) {
		BedStatus_MALE = bedStatus_MALE;
	}

	public String getBedStatus_FEMALE() {
		return BedStatus_FEMALE;
	}

	public void setBedStatus_FEMALE(String bedStatus_FEMALE) {
		BedStatus_FEMALE = bedStatus_FEMALE;
	}

	public String getUploadFLG() {
		return UploadFLG;
	}

	public void setUploadFLG(String uploadFLG) {
		UploadFLG = uploadFLG;
	}

	public String getDeleteFLG() {
		return DeleteFLG;
	}

	public void setDeleteFLG(String deleteFLG) {
		DeleteFLG = deleteFLG;
	}

	public String getInsert_DATETIME() {
		return Insert_DATETIME;
	}

	public void setInsert_DATETIME(String insert_DATETIME) {
		Insert_DATETIME = insert_DATETIME;
	}

	public String getUpdate_DATETIME() {
		return Update_DATETIME;
	}

	public void setUpdate_DATETIME(String update_DATETIME) {
		Update_DATETIME = update_DATETIME;
	}

	public String getCaseCode1() {
		return CaseCode1;
	}

	public void setCaseCode1(String caseCode1) {
		CaseCode1 = caseCode1;
	}

	public String getCaseCode2() {
		return CaseCode2;
	}

	public void setCaseCode2(String caseCode2) {
		CaseCode2 = caseCode2;
	}

	public String getCaseCode3() {
		return CaseCode3;
	}

	public void setCaseCode3(String caseCode3) {
		CaseCode3 = caseCode3;
	}

	public String getCaseCode4() {
		return CaseCode4;
	}

	public void setCaseCode4(String caseCode4) {
		CaseCode4 = caseCode4;
	}

	public String getMIName_Kanji() {
		return MIName_Kanji;
	}

	public void setMIName_Kanji(String mIName_Kanji) {
		MIName_Kanji = mIName_Kanji;
	}

	public String getAddress_Kanji() {
		return Address_Kanji;
	}

	public void setAddress_Kanji(String address_Kanji) {
		Address_Kanji = address_Kanji;
	}

	public String getJianNo() {
		return JianNo;
	}

	public void setJianNo(String jianNo) {
		JianNo = jianNo;
	}
	public String kin_kbn,kin_no,kin_branch_no,kinLink_DateTime;

}
