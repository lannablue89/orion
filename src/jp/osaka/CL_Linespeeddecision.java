package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_Linespeeddecision {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_Linespeeddecision(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public int getMaxFileNo(int networkinfosubtype) {
		int fileno = 0;
		Cursor mCursor = null;
		try {
			String[] args = { String.valueOf(networkinfosubtype) };
			String sql = "Select maxsizefile from ms_linespeeddecision where networkinfosubtype=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor != null && mCursor.getCount() > 0) {
					String maxsizefile = mCursor.getString(mCursor
							.getColumnIndex("maxsizefile"));

					if (maxsizefile != null && !maxsizefile.equals("")) {
						fileno = Integer.parseInt(maxsizefile);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return fileno;
	}
}
