﻿package jp.osaka;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

public class E020 extends Activity implements OnTouchListener {
	String FireStationCode;
	String CaseCode;
	static Calendar c = Calendar.getInstance();
	static TextView txtTime;
	static String codeId = "";
	static ArrayList<DTO_MCode> list;
	HashMap<String, String> map;
	String note = "";
	String noteinf = "";
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		v.requestFocusFromTouch();
		// v.setFocusable(true);
		// v.setFocusableInTouchMode(true);
		@SuppressWarnings("static-access")
		InputMethodManager inputManager = (InputMethodManager) getSystemService(E020.this.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(E020.this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E020.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute020);

		Cursor cur = null;
		try {
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			// createMenu();// menu
			// getConfig();
			// Testmenu
			// TextView txtHeader=(TextView)findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			list = new ArrayList<DTO_MCode>();
			map = new HashMap<String, String>();
			Cl_FirstAid cl = new Cl_FirstAid(this);
			cur = cl.getTR_FirstAid(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {
					String FirstAid_CD = cur.getString(cur
							.getColumnIndex("FirstAid_CD"));
					String FirstAid_TEXT = cur.getString(cur
							.getColumnIndex("FirstAid_TEXT"));
					String FirstAid_DATETIME = cur.getString(cur
							.getColumnIndex("FirstAid_DATETIME"));
					if (FirstAid_CD.equals(BaseCount.Other)) {
						note = FirstAid_TEXT;
						noteinf = note;
					}
					map.put(FirstAid_CD, FirstAid_DATETIME);
				} while (cur.moveToNext());

			}

			this.loadButton();
			Button btnOk = (Button) findViewById(R.id.btnOK20);
			btnOk.setOnTouchListener(this);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					for (int i = 0; i < list.size(); i++) {
						DTO_MCode dt = new DTO_MCode();
						dt = list.get(i);
						if (noteinf == null)
							noteinf = "";
						if (note == null)
							note = "";
						if ((dt.getFlag() != null && dt.getFlag().equals("1"))
								|| (dt.getCodeId().equals(BaseCount.Other) && !noteinf
										.equals(note))) {
							if (dt.getDatetime() != null
									&& !dt.getDatetime().equals("")) {

								Cl_FirstAid cl = new Cl_FirstAid(E020.this);
								note = "";
								if (dt.getCodeId().equals(BaseCount.Other)) {
									EditText txtnote = (EditText) findViewById(99999);
									note = txtnote.getText().toString();
								}
								if (map.containsKey(dt.getCodeId())) {
									cl.updateTR_FirstAid(FireStationCode,
											CaseCode, dt.getCodeId(), note,
											dt.getDatetime());
								} else {
									cl.createTR_FirstAid(FireStationCode,
											CaseCode, dt.getCodeId(), note,
											dt.getDatetime());
								}

							} else {
								if (map.containsKey(dt.getCodeId())) {
									Cl_FirstAid cl = new Cl_FirstAid(E020.this);
									cl.deleteTR_FirstAid(FireStationCode,
											CaseCode, dt.getCodeId());
								}
							}
						}
					}
					// Intent myinten = new Intent(E020.this, E021.class);
					// Bundle b = new Bundle();
					// b.putString("FireStationCode", FireStationCode);
					// b.putString("CaseCode", CaseCode);
					// myinten.putExtras(b);
					// startActivityForResult(myinten, 0);
					finish();
				}
			});
			Button btnCancel = (Button) findViewById(R.id.btnCancel20);
			btnCancel.setOnTouchListener(this);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			ScrollView view = (ScrollView) findViewById(R.id.scrollViewe20);
			view.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
			view.setFocusable(true);
			view.setFocusableInTouchMode(true);
			view.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E020", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	private static String formatNumber(int num) {
		if (num < 10) {
			return "0" + String.valueOf(num);
		}
		return String.valueOf(num);
	}

	// Bug 149
	private void confirmDeleteTime() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtTime.setVisibility(View.INVISIBLE);
				for (int i = 0; i < list.size(); i++) {
					DTO_MCode dt = new DTO_MCode();
					dt = list.get(i);
					if (dt.getCodeId().equals(codeId)) {
						dt.setDatetime(null);
						dt.setFlag("1");
						break;
					}
				}

				dialog.cancel();
			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteItem);
		lbmg.setText(mg);

		dialog.show();
	}

	private void loadButton() {
		Cursor cur = null;
		try {
			Cl_Code cl = new Cl_Code(E020.this);
			cur = cl.getMS_Code("00008");
			if (cur != null && cur.getCount() > 0) {
				LinearLayout layoutItem = (LinearLayout) findViewById(R.id.layoutE20Group1);
				layoutItem.setOnTouchListener(this);
				do {
					final String code = cur.getString(cur
							.getColumnIndex("CodeID"));
					String codeName = cur.getString(cur
							.getColumnIndex("CodeName"));
					DTO_MCode dt = new DTO_MCode();
					dt.setCodeId(code);
					list.add(dt);
					// Bug 171
					// LinearLayout.LayoutParams paramg1 = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.MATCH_PARENT, 90);
					LinearLayout.LayoutParams paramg1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, 120);
					//
					paramg1.setMargins(0, 15, 0, 15);
					LinearLayout layout1 = new LinearLayout(E020.this);
					layout1.setOnTouchListener(this);
					layout1.setOrientation(LinearLayout.HORIZONTAL);
					layout1.setLayoutParams(paramg1);
					layout1.setGravity(Gravity.LEFT);
					// LinearLayout.LayoutParams parambtn1 = new
					// LinearLayout.LayoutParams(
					// 450, 90);
					LinearLayout.LayoutParams parambtn1 = new LinearLayout.LayoutParams(
							450, 120);

					TextView btn1 = new TextView(this);
					btn1.setLayoutParams(parambtn1);
					btn1.setText("   " + codeName);
					btn1.setBackgroundResource(R.drawable.btnitem20);
					btn1.setTextColor(Color.WHITE);
					btn1.setTextSize(24);
					btn1.setGravity(Gravity.LEFT | Gravity.CENTER);

					// LinearLayout.LayoutParams paramtxt1 = new
					// LinearLayout.LayoutParams(
					// 194, 78);
					LinearLayout.LayoutParams paramtxt1 = new LinearLayout.LayoutParams(
							194, 100);
					paramtxt1.setMargins(10, 0, 0, 0);
					final TextView txt1 = new TextView(this);
					txt1.setLayoutParams(paramtxt1);
					txt1.setTextColor(Color.WHITE);
					txt1.setGravity(Gravity.CENTER);
					txt1.setBackgroundResource(R.drawable.txttime20);
					txt1.setTextSize(24);
					if (map.containsKey(code)) {
						txt1.setVisibility(View.VISIBLE);
						String hour = map.get(code).substring(8, 10);
						String minute = map.get(code).substring(10, 12);
						txt1.setText(hour + ":" + minute);
						dt.setDatetime(map.get(code));

					} else {
						txt1.setVisibility(View.INVISIBLE);
					}
					layout1.addView(btn1);
					layout1.addView(txt1);
					layoutItem.addView(layout1);
					if (code.equals(BaseCount.Other)) {
						LinearLayout.LayoutParams paramgtxt = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, 90);
						paramgtxt.setMargins(0, 15, 0, 15);
						EditText txtNote = new EditText(this);
						txtNote.setOnTouchListener(this);
						txtNote.setLayoutParams(paramgtxt);
						txtNote.setBackgroundResource(R.drawable.input);
						txtNote.setId(99999);
						txtNote.setText(note);
						txtNote.setTextColor(Color.BLACK);
						txtNote.setSingleLine(true);// Bug 139
						txtNote.addTextChangedListener(new TextWatcher() {

							public void onTextChanged(CharSequence s,
									int start, int before, int count) {
								// TODO Auto-generated method stub

							}

							public void beforeTextChanged(CharSequence s,
									int start, int count, int after) {
								// TODO Auto-generated method stub

							}

							public void afterTextChanged(Editable s) {
								// TODO Auto-generated method stub
								// note = txtNote.getText().toString();
							}
						});
						layoutItem.addView(txtNote);

					}
					btn1.setOnTouchListener(this);
					btn1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							if (txt1.getVisibility() == View.INVISIBLE) {
								c = Calendar.getInstance();
								String text = formatNumber(c
										.get(Calendar.HOUR_OF_DAY))
										+ ":"
										+ formatNumber(c.get(Calendar.MINUTE));
								txt1.setText(text);
								txt1.setVisibility(View.VISIBLE);
								String date = Utilities.getDateNow()
										+ txt1.getText().toString()
												.replace(":", "") + "00";

								for (int i = 0; i < list.size(); i++) {
									DTO_MCode dt = new DTO_MCode();
									dt = list.get(i);
									if (dt.getCodeId().equals(code)) {
										dt.setDatetime(date);
										dt.setFlag("1");
										break;
									}
								}
							} else {
								txtTime = txt1;
								codeId = code;
								// showTimePickerDialog1(v);
								showTimer();
							}
						}
					});
					txt1.setOnTouchListener(this);
					txt1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							txtTime = txt1;
							codeId = code;
							// showTimePickerDialog1(v);
							showTimer();
						}
					});
					txt1.setOnLongClickListener(new OnLongClickListener() {

						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							if (!check) {
								txtTime = txt1;
								codeId = code;
								confirmDeleteTime();
								return true;
							}
							return check;
						}
					});
				} while (cur.moveToNext());

			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E020.this, e.getMessage(), "E020", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private boolean checkVersion41() {
		try {
			if (Double.parseDouble(Build.VERSION.RELEASE.substring(0, 3)) < 4.1) {
				return false;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return true;
	}

	private void showTimer() {
		try {
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute006);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			final TimePicker timer = (TimePicker) dialog
					.findViewById(R.id.timePicker1);
			timer.setIs24HourView(true);
			if (checkVersion41() == false) {
				ViewGroup v = (ViewGroup) timer.getChildAt(0);
				ViewGroup numberPicker1 = (ViewGroup) v.getChildAt(0);
				ViewGroup numberPicker2 = (ViewGroup) v.getChildAt(1);
				EditText txthours = ((EditText) numberPicker1.getChildAt(1));
				EditText txtmins = ((EditText) numberPicker2.getChildAt(1));
				txthours.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						2) });
				txtmins.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						2) });
			}
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOk);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Update E007
					timer.clearFocus();
					c.set(Calendar.HOUR_OF_DAY, timer.getCurrentHour());
					c.set(Calendar.MINUTE, timer.getCurrentMinute());
					String text = setTimer(timer.getCurrentHour(),
							timer.getCurrentMinute());
					txtTime.setText(text);
					String date = Utilities.getDateNow()
							+ txtTime.getText().toString().replace(":", "")
							+ "00";
					for (int i = 0; i < list.size(); i++) {
						DTO_MCode dt = new DTO_MCode();
						dt = list.get(i);
						if (dt.getCodeId().equals(codeId)) {
							dt.setDatetime(date);
							dt.setFlag("1");
							break;
						}
					}

					dialog.cancel();
				}
			});
			String time = txtTime.getText().toString();
			String[] s = time.split(":");
			int hour = Integer.parseInt(s[0]);
			int minute = Integer.parseInt(s[1]);
			timer.setCurrentHour(hour);
			timer.setCurrentMinute(minute);
			dialog.show();
		} catch (Exception e) {
			E022.showErrorDialog(E020.this, e.getMessage(), "E020", true);
		}
	}

	private static String setTimer(int hour, int minute) {
		return formatNumber(hour) + ":" + formatNumber(minute);
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
