package jp.osaka.db;

import jp.osaka.DatabaseHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class BaseDatabaseQuery {
	
	protected DatabaseHelper dbHelper;
	protected SQLiteDatabase database;
	
	protected BaseDatabaseQuery(Context context) {
		try {
			dbHelper = new DatabaseHelper(context);//DatabaseHelper.getInstance(context);
			database = dbHelper.getWritableDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public static String sqlUpdate(String table_name, String[] columns, String[] values, String condition) throws Exception {
//		if (columns == null || columns.length == 0 || values == null || values.length == 0 || columns.length != values.length) {
//			throw new Exception("Invalid params");
//		}
//		
//		/*
//		 * UPDATE table_name
//			SET column1 = value1, column2 = value2...., columnN = valueN
//			WHERE [condition];
//		 */
//		// UPDATE table_name
//		StringBuilder queryString = new StringBuilder(" UPDATE "); 
//		queryString.append(table_name);
//		
//		// SET column1 = value1, column2 = value2...., columnN = valueN
//		queryString.append(" SET "); 
//		int count = columns.length;
//		for (int i = 0; i < count; i++) {
//			queryString.append(columns[i]);
//			queryString.append("=");
//			queryString.append(values[i]); 
//			if (i < count - 1)
//				queryString.append(",");
//		}
//		
//		// WHERE [condition];
//		if (condition != null && condition.length() > 0) {
//			queryString.append(" WHERE ");
//			queryString.append(condition);
//			queryString.append(";");
//		}
//		return queryString.toString();
//	}
	
}
