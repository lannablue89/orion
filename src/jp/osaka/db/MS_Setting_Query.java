package jp.osaka.db;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class MS_Setting_Query extends BaseDatabaseQuery {

	private static MS_Setting_Query mInstance;
	
	private MS_Setting_Query(Context context) {
		super(context);
	}
	
	public static MS_Setting_Query getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new MS_Setting_Query(context);
		}
		return mInstance;
	}
	/* =======================================
	 * 
	 */
	public String get_ms_setting_startmethodtimer() {
		String value = null;
		Cursor cursor = null;
		try {
			String queryString = String.format(" SELECT startmethodtimer FROM ms_setting");
			Log.d("db_test", "queryString=" + queryString);
			database = dbHelper.getWritableDatabase();
			cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				cursor.moveToFirst();
				if (cursor.getCount() > 0) {
					value = cursor.getString(cursor.getColumnIndex("startmethodtimer"));
				}
			} else { // hard code
				value = "20140101000";
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return value;
	}
	
	public void close() {
		dbHelper.close();
	}
}
