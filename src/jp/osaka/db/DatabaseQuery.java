package jp.osaka.db;

import java.util.ArrayList;
import java.util.List;

import jp.osaka.CL_TR_TriageSTARTMethod;
import jp.osaka.db.model.QQ_tr_kin_yousei_Model;
import jp.osaka.db.model.QQ_tr_kin_yousei_kahi_Model;
import jp.osaka.db.model.TR_TriageSTARTMethod_Model;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class DatabaseQuery extends BaseDatabaseQuery {

	private static DatabaseQuery mInstance;
	
	private DatabaseQuery(Context context) {
		super(context);
	}
	
	public static DatabaseQuery getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseQuery(context);
		}
		return mInstance;
	}
	/* =======================================
	 * 
	 */
	public QQ_tr_kin_yousei_Model get_QQ_tr_kin_yousei_Model(int kin_no, int kin_branch_no) {
		QQ_tr_kin_yousei_Model model = null;
		Cursor cursor = null;
		try {
			String queryString = String.format(" SELECT * " +
												" FROM qq_tr_kin_yousei q " +
												" WHERE q.kin_no=%d and q.kin_branch_no=%d", 
												kin_no, kin_branch_no);
			Log.d("db_test", "queryString=" + queryString);
			database = dbHelper.getWritableDatabase();
			cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				cursor.moveToFirst();
				if (cursor.getCount() > 0) {
					model = new QQ_tr_kin_yousei_Model(cursor);
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return model;
	}
	public String get_ms_firestation_Model_FireStationName(String FireStationCode) {
		String value = null;
		Cursor cursor = null;
		try {
			String queryString = " SELECT ms.FireStationName " +
									" FROM ms_firestation ms " +
									" WHERE ms.FireStationCode=" + FireStationCode;
			Log.d("db_test", "queryString=" + queryString);
			database = dbHelper.getWritableDatabase();
			cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				cursor.moveToFirst();
				if (cursor.getCount() > 0) {
					value = cursor.getString(cursor.getColumnIndex("FireStationName"));
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return value;
	}
	
	public List<QQ_tr_kin_yousei_kahi_Model> get_QQ_tr_kin_yousei_kahi_Model(int kin_no, int kin_branch_no) {
		List<QQ_tr_kin_yousei_kahi_Model> result = new ArrayList<QQ_tr_kin_yousei_kahi_Model>();
		Cursor cursor = null;
		try {
			String queryString = String.format(" SELECT * " +
												" FROM QQ_tr_kin_yousei_kahi q " +
												" WHERE q.kin_no=%d and q.kin_branch_no=%d", 
												kin_no, kin_branch_no);
			Log.d("db_test", "queryString=" + queryString);
			database = dbHelper.getWritableDatabase();
			cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						QQ_tr_kin_yousei_kahi_Model model = new QQ_tr_kin_yousei_kahi_Model(cursor);
						result.add(model);
					} while(cursor.moveToNext());
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public void close() {
		dbHelper.close();
	}
	
	// ========================================================
	// lanna: Testing data 
	// ========================================================

	public static void testQueriesAndSetupDB(Context context) {
		// 1 =================================================
//		QQ_tr_kin_yousei_Model model = DatabaseQuery.getInstance(context).get_QQ_tr_kin_yousei_Model(100, 1);
//		Log.i("db_test", "1 - " + model.toString());
		
		// 2 =================================================
//		String FireStationName = DatabaseQuery.getInstance(context).get_ms_firestation_Model_FireStationName("4270000010");
//		Log.i("db_test", "2 - " + "FireStationName is " + FireStationName);
		
		// 3 =================================================
		CL_TR_TriageSTARTMethod trTriageSTARTMethodQuery = new CL_TR_TriageSTARTMethod(context);
		TR_TriageSTARTMethod_Model triageStartMethodModel = trTriageSTARTMethodQuery
				.getTR_TriageSTARTMethod_Model("4270000010", "8902189174496952320");//"4270100070201403260001");
		Log.i("db_test", "3 - " + "triageSTARTMethod_Model is " + triageStartMethodModel);
		
		if (triageStartMethodModel != null) {
			triageStartMethodModel.walking = "f";
			int value = trTriageSTARTMethodQuery.createTR_TriageSTARTMethod_Model(triageStartMethodModel);
			Log.i("db_test", "3 - " + "triageSTARTMethod_Model saved " + value);
			
			triageStartMethodModel = trTriageSTARTMethodQuery
					.getTR_TriageSTARTMethod_Model("4270000010", "8902189174496952320");//"4270100070201403260001");
			Log.i("db_test", "3 - " + "saved triageSTARTMethod_Model is " + triageStartMethodModel);
		} else {
			triageStartMethodModel = new TR_TriageSTARTMethod_Model("4270000010", "4270100070201403260001",
					"EMSInsert_Datetime", "walking", "respiratory", "respiratorycount", 
					"circulatory", "response", "estimateresult");
			int value = trTriageSTARTMethodQuery.createTR_TriageSTARTMethod_Model(triageStartMethodModel);
			Log.i("db_test", "3 - " + "triageSTARTMethod_Model created " + value);
		}

		// 4 =================================================
//		String startmethodtimer = MS_Setting_Query.getInstance(context).get_ms_setting_startmethodtimer();
//		Log.i("db_test", "4 - " + "startmethodtimer is " + startmethodtimer);
		
		// 5 =================================================
		int value = trTriageSTARTMethodQuery.insertOrUpdateTR_TriageSTARTMethod_result_Model(triageStartMethodModel);
		Log.i("db_test", "5 - " + "triageSTARTMethod_Model insertOrUpdate " + value);
		triageStartMethodModel = trTriageSTARTMethodQuery.selectTR_TriageSTARTMethod_result_Model(
				triageStartMethodModel.FireStationCode, triageStartMethodModel.CaseNo, triageStartMethodModel.EMSInsert_Datetime);
		Log.i("db_test", "5 - " + "triageSTARTMethod_Model created " + triageStartMethodModel);
	}
}
