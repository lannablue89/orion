package jp.osaka.db.model;

import android.database.Cursor;

/*
 * public static String CREATE_tr_kin_yousei43 = "create table QQ_tr_kin_yousei  (
			kin_no integer, 
			kin_branch_no integer,  
			kin_kbn integer,  
			status_flg integer,
			isshow integer,
			Update_DateTime varchar(14),
			Insert_DateTime varchar(14), 
			kin_date varchar(8), 
			kin_time varchar(4), 
			title varchar(200), 
			cre_date varchar(14) , 
			jusho_name varchar(400)  , 
			qqsbt2 varchar(1) , 
			qqsbt3 varchar(1), 
			yousei_level varchar(2) , 
			dmat_yousei_f varchar(1),  
			PRIMARY KEY (kin_no, kin_branch_no));";
 */
public class QQ_tr_kin_yousei_Model {
	
	public int kin_no; //
	public int kin_branch_no;
	public int kin_kbn;
	public int status_flg;
	public int isshow;
	public String Update_DateTime;
	public String Insert_DateTime;
	public String kin_date; //
	public String kin_time; //
	public String title; //
	public String cre_date;
	public String jusho_name;
	public String qqsbt2;
	public String qqsbt3;
	public String yousei_level;
	public String dmat_yousei_f;

	public QQ_tr_kin_yousei_Model() { }
	
	public QQ_tr_kin_yousei_Model(Cursor cursor) {
		kin_no = cursor.getInt(cursor.getColumnIndex("kin_no"));
		kin_branch_no = cursor.getInt(cursor.getColumnIndex("kin_branch_no"));
		kin_kbn = cursor.getInt(cursor.getColumnIndex("kin_kbn"));
		status_flg = cursor.getInt(cursor.getColumnIndex("status_flg"));
		isshow = cursor.getInt(cursor.getColumnIndex("isshow"));
		Update_DateTime = cursor.getString(cursor.getColumnIndex("Update_DateTime"));
		Insert_DateTime = cursor.getString(cursor.getColumnIndex("Insert_DateTime"));
		kin_date = cursor.getString(cursor.getColumnIndex("kin_date"));
		kin_time = cursor.getString(cursor.getColumnIndex("kin_time"));
		title = cursor.getString(cursor.getColumnIndex("title"));
		cre_date = cursor.getString(cursor.getColumnIndex("cre_date"));
		jusho_name = cursor.getString(cursor.getColumnIndex("jusho_name"));
		qqsbt2 = cursor.getString(cursor.getColumnIndex("qqsbt2"));
		qqsbt3 = cursor.getString(cursor.getColumnIndex("qqsbt3"));
		yousei_level = cursor.getString(cursor.getColumnIndex("yousei_level"));
		dmat_yousei_f = cursor.getString(cursor.getColumnIndex("dmat_yousei_f"));
	}

	@Override
	public String toString() {
		return "kin_no=" + kin_no
				+ ", " + "kin_branch_no=" + kin_branch_no
				+ ", " + "kin_date=" + kin_date
				+ ", " + "kin_time=" + kin_time
				+ ", " + "title=" + title
				;
	}
	
}
