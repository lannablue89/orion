package jp.osaka.db.model;

//create table MS_FireStation ( 
//					FireStationCode varchar(10)  primary key ,
//					FireStationName varchar(100),
//					SHCACode varchar(5),
//					AreaCode varchar(5),
//					ReportFireStationCode varchar(10),
//					TerminalPassword varchar(50),
//					Latitude varchar(20),
//					Longitude varchar(20),
//					SortNo integer ,
//					DeleteFLG varchar(1) ,
//					Update_DATETIME varchar(14),
//					JianNo_Initial varchar(50),
//					FDMAUpload_Initial_1 varchar(6),
//					FDMAUpload_Initial_2 varchar(6),
//					FDMAUpload_Initial_3 varchar(6),
//					FDMAUpload_Initial_4 varchar(6));

public class MS_FireStation {
	public String fireStationCode;
	public String fireStationName;
	public String sHCACode;
	public String areaCode;
	public String reportFireStationCode;
	public String terminalPassword;
	public String latitude;
	public String longitude;
	public int sortNo;
	public String deleteFLG;
	public String update_DATETIME;
	public String jianNo_Initial;
	public String fDMAUpload_Initial_1;
	public String fDMAUpload_Initial_2;
	public String fDMAUpload_Initial_3;
	public String fDMAUpload_Initial_4;
}
