package jp.osaka.db.model;

import android.database.Cursor;

public class QQ_tr_kin_yousei_kahi_Model {
//	create table QQ_tr_kin_yousei_kahi  (
//	kin_no integer, 
//	kin_branch_no integer,
//	kikan_cd varchar(10) not null, 
//	ukeire_kahi varchar(8) , 
//	entrydate varchar(6)  , 
//	entrytime varchar(1)  , 
//	bikou varchar(400), 
//	tel_flg varchar(1)  , 
//	delivery_flg varchar(1), 
//	sick_h integer , 
//	sick_m integer, 
//	sick_l integer  , 
//	cre_kikan_cd varchar(10)  , 
//	upd_kikan_cd varchar(10), 
//	ap_upd_date varchar(7)  , 
//	data_upd_date varchar(7)  , 
//	cre_date varchar(7), 
//	upd_date varchar(7)  , 
//	primary key (kin_no,kin_branch_no,kikan_cd));";

	public int kin_no;
	public int kin_branch_no;
	public String kikan_cd;
	public String ukeire_kahi;
	public String entrydate;
	public String entrytime;
	public String bikou;
	public String tel_flg;
	public String delivery_flg;
	public int sick_h;
	public int sick_m;
	public int sick_l;
	public String cre_kikan_cd;
	public String upd_kikan_cd;
	public String ap_upd_date;
	public String data_upd_date;
	public String cre_date;
	public String upd_date;
	
	public QQ_tr_kin_yousei_kahi_Model() { }
	
	public QQ_tr_kin_yousei_kahi_Model(Cursor cursor) {
		kin_no = cursor.getInt(cursor.getColumnIndex("kin_no"));
		kin_branch_no = cursor.getInt(cursor.getColumnIndex("kin_branch_no"));
		kikan_cd = cursor.getString(cursor.getColumnIndex("kikan_cd"));
		ukeire_kahi = cursor.getString(cursor.getColumnIndex("ukeire_kahi"));
		entrydate = cursor.getString(cursor.getColumnIndex("entrydate"));
		entrytime = cursor.getString(cursor.getColumnIndex("entrytime"));
		bikou = cursor.getString(cursor.getColumnIndex("bikou"));
		tel_flg = cursor.getString(cursor.getColumnIndex("tel_flg"));
		delivery_flg = cursor.getString(cursor.getColumnIndex("delivery_flg"));
		sick_h = cursor.getInt(cursor.getColumnIndex("sick_h"));
		sick_h = cursor.getInt(cursor.getColumnIndex("sick_h"));
		sick_l = cursor.getInt(cursor.getColumnIndex("sick_l"));
		cre_kikan_cd = cursor.getString(cursor.getColumnIndex("cre_kikan_cd"));
		upd_kikan_cd = cursor.getString(cursor.getColumnIndex("upd_kikan_cd"));
		ap_upd_date = cursor.getString(cursor.getColumnIndex("ap_upd_date"));
		data_upd_date = cursor.getString(cursor.getColumnIndex("data_upd_date"));
		cre_date = cursor.getString(cursor.getColumnIndex("cre_date"));
		upd_date = cursor.getString(cursor.getColumnIndex("upd_date"));
	}
}
