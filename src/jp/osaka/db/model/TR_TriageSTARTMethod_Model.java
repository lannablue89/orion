package jp.osaka.db.model;

import android.content.ContentValues;
import android.database.Cursor;

/*
 * public static String CREATE_TR_TriageSTARTMethod  = "create table TR_TriageSTARTMethod  (
		FireStationCode varchar(10),'
		CaseNo varchar(22),
		EMSInsert_Datetime varchar(14),
		walking varchar(1),
		respiratory varchar(1),
		respiratorycount varchar(1),
		circulatory varchar(1), 
		response varchar(1), 
		estimateresult varchar(1), 
		primary key (FireStationCode,CaseNo));";
 */
public class TR_TriageSTARTMethod_Model {
	
	public String FireStationCode;
	public String CaseNo;
	public String EMSInsert_Datetime;
	public String walking;
	public String respiratory;
	public String respiratorycount;
	public String circulatory;
	public String response;
	public String estimateresult;
	
	public TR_TriageSTARTMethod_Model() { }

	public TR_TriageSTARTMethod_Model(String FireStationCode,  String CaseNo, 
			String EMSInsert_Datetime, String walking, String respiratory, String respiratorycount, 
			String circulatory, String response, String estimateresult) { 
		this.FireStationCode = FireStationCode;
		this.CaseNo = CaseNo;
		this.EMSInsert_Datetime = EMSInsert_Datetime;
		this.walking = walking;
		this.respiratory = respiratory;
		this.respiratorycount = respiratorycount;
		this.circulatory = circulatory;
		this.response = response;
		this.estimateresult = estimateresult;
	}

	public TR_TriageSTARTMethod_Model(Cursor cursor) {
		FireStationCode = cursor.getString(cursor.getColumnIndex("FireStationCode"));
		CaseNo = cursor.getString(cursor.getColumnIndex("CaseNo"));
		EMSInsert_Datetime = cursor.getString(cursor.getColumnIndex("EMSInsert_Datetime"));
		walking = cursor.getString(cursor.getColumnIndex("walking"));
		respiratory = cursor.getString(cursor.getColumnIndex("respiratory"));
		respiratorycount = cursor.getString(cursor.getColumnIndex("respiratorycount"));
		circulatory = cursor.getString(cursor.getColumnIndex("circulatory"));
		response = cursor.getString(cursor.getColumnIndex("response"));
		estimateresult = cursor.getString(cursor.getColumnIndex("estimateresult"));
	}
	
	public ContentValues getValues() {
		ContentValues values = new ContentValues();
	    values.put("FireStationCode", FireStationCode); 
	    values.put("CaseNo", CaseNo); 
	    values.put("EMSInsert_Datetime", EMSInsert_Datetime); 
	    values.put("walking", walking); 
	    values.put("respiratory", respiratory); 
	    values.put("respiratorycount", respiratorycount); 
	    values.put("circulatory", circulatory); 
	    values.put("response", response); 
		values.put("estimateresult", estimateresult);
		return values;
	}
	
	@Override
	public String toString() {
		return "FireStationCode=" + FireStationCode
				+ ", " + "CaseNo=" + CaseNo
				+ ", " + "EMSInsert_Datetime=" + EMSInsert_Datetime
				+ ", " + "walking=" + walking
				+ ", " + "respiratory=" + respiratory
				+ ", " + "respiratorycount=" + respiratorycount
				+ ", " + "circulatory=" + circulatory
				+ ", " + "response=" + response
				+ ", " + "estimateresult=" + estimateresult
				;
	}
}
