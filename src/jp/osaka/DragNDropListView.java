/*
 * Copyright (C) 2010 Eric Harlow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.osaka;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class DragNDropListView extends ListView {

	boolean mDragMode;
	public boolean bAllowEdit = false;
	int mStartPosition;
	int mEndPosition;
	int mDragPointOffset; // Used to adjust drag view location

	ImageView mDragView;
	GestureDetector mGestureDetector;

	DropListener mDropListener;
	RemoveListener mRemoveListener;
	DragListener mDragListener;

	public DragNDropListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setDropListener(DropListener l) {
		mDropListener = l;
	}

	public void setRemoveListener(RemoveListener l) {
		mRemoveListener = l;
	}

	public void setDragListener(DragListener l) {
		mDragListener = l;
	}

	int iPosStart;

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		final int x = (int) ev.getX();
		final int y = (int) ev.getY();

		// //Dai test
		if (action == MotionEvent.ACTION_DOWN && x < this.getWidth() / 1.5) {
			mDragMode = true;
		}
		// if (action == MotionEvent.ACTION_DOWN) {
		// mDragMode = true;
		// }

		if (!mDragMode || !bAllowEdit)
			return super.onTouchEvent(ev);

		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if (bAllowEdit) {
				iPosStart = y;

				mStartPosition = pointToPosition(x, y);
				if (mStartPosition != INVALID_POSITION) {
					int mItemPosition = mStartPosition
							- getFirstVisiblePosition();
					mDragPointOffset = y - getChildAt(mItemPosition).getTop();
					mDragPointOffset -= ((int) ev.getRawY()) - y;
					startDrag(mItemPosition, y);
					drag(0, y);// replace 0 with x if desired
				}
				// Rect r = mTempRect;
				// if (mRemoveMode == SLIDE && ev.getX() > r.right * 3 / 4) {
				//
				// unExpandViews(true);
				// } else {
				//
				// unExpandViews(false);
				// }
			}
			break;
		case MotionEvent.ACTION_MOVE:
			int a = (int) ev.getX();
			int b = (int) ev.getY();
			if (b > iPosStart) {
				// int itemnum = pointToPosition(a, b);
				// int lastitem = pointToPosition(a, getLastVisiblePosition());
				// if (itemnum == lastitem && itemnum < getCount()-1) {
				// itemnum = itemnum + 1;
				// setSelection(itemnum);
				// }

				if (b >=getHeight() +getFirstVisiblePosition()) {
					smoothScrollToPosition(b);
				}
			} else if (b < iPosStart) {

				// int itemnum = pointToPosition(a, b);
				// int fitem = pointToPosition(a, getFirstVisiblePosition());
				// if (itemnum == fitem && itemnum > 0) {
				// itemnum = itemnum - 1;
				// setSelection(itemnum);
				// }
				if (b <= getFirstVisiblePosition()) {
					smoothScrollToPosition(b);
				}
			}

			if (bAllowEdit)
				drag(0, y);// replace 0 with x if desired

			// int itemnum = getItemForPosition(b);
			// if (itemnum >= 0) {
			// if (action == MotionEvent.ACTION_DOWN || itemnum != mDragPos) {
			// if (mDragListener != null) {
			// // mDragListener.drag(mDragPos, itemnum);
			// }
			// mDragPos = itemnum;
			// doExpansion();
			// }
			// int speed = 0;
			// adjustScrollBounds(b);
			// if (b > mLowerBound) {
			// // scroll the list up a bit
			// speed = b > (mHeight + mLowerBound) / 2 ? 16 : 4;
			// } else if (b < mUpperBound) {
			// // scroll the list down a bit
			// speed = b < mUpperBound / 2 ? -16 : -4;
			// }
			// if (speed != 0) {
			// int ref = pointToPosition(0, mHeight / 2);
			// if (ref == AdapterView.INVALID_POSITION) {
			// // we hit a divider or an invisible view, check
			// // somewhere else
			// ref = pointToPosition(0, mHeight / 2
			// + getDividerHeight() + 64);
			// }
			// View v = getChildAt(ref - getFirstVisiblePosition());
			// if (v != null) {
			// int pos = v.getTop();
			// setSelectionFromTop(ref, pos - speed);
			// }
			// }
			// }
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
		default:
			if (bAllowEdit) {
				mDragMode = false;
				mEndPosition = pointToPosition(x, y);
				stopDrag(mStartPosition - getFirstVisiblePosition());
				if (mDropListener != null && mStartPosition != INVALID_POSITION
						&& mEndPosition != INVALID_POSITION)
					mDropListener.onDrop(mStartPosition, mEndPosition);
			}
			break;
		}
		return true;
	}

	// move the drag view
	private void drag(int x, int y) {
		if (mDragView != null) {
			WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mDragView
					.getLayoutParams();
			layoutParams.x = x;
			layoutParams.y = y - mDragPointOffset;
			WindowManager mWindowManager = (WindowManager) getContext()
					.getSystemService(Context.WINDOW_SERVICE);
			mWindowManager.updateViewLayout(mDragView, layoutParams);

			if (mDragListener != null)
				mDragListener.onDrag(x, y, null);// change null to "this" when
													// ready to use
		}
	}

	// enable the drag view for dragging
	private void startDrag(int itemIndex, int y) {
		stopDrag(itemIndex);

		View item = getChildAt(itemIndex);
		if (item == null)
			return;
		item.setDrawingCacheEnabled(true);
		if (mDragListener != null)
			mDragListener.onStartDrag(item);

		// Create a copy of the drawing cache so that it does not get recycled
		// by the framework when the list tries to clean up memory
		Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());

		WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
		mWindowParams.gravity = Gravity.TOP;
		mWindowParams.x = 0;
		mWindowParams.y = y - mDragPointOffset;

		mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
		mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
		mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
				| WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
		mWindowParams.format = PixelFormat.TRANSLUCENT;
		mWindowParams.windowAnimations = 0;

		Context context = getContext();
		ImageView v = new ImageView(context);
		v.setImageBitmap(bitmap);

		WindowManager mWindowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		mWindowManager.addView(v, mWindowParams);
		mDragView = v;
	}

	// destroy drag view
	private void stopDrag(int itemIndex) {
		if (mDragView != null) {
			if (mDragListener != null)
				mDragListener.onStopDrag(getChildAt(itemIndex));
			mDragView.setVisibility(GONE);
			WindowManager wm = (WindowManager) getContext().getSystemService(
					Context.WINDOW_SERVICE);
			wm.removeView(mDragView);
			mDragView.setImageDrawable(null);
			mDragView = null;
		}
	}

	// private GestureDetector createFlingDetector() {
	// return new GestureDetector(getContext(), new SimpleOnGestureListener() {
	// @Override
	// public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	// float velocityY) {
	// if (mDragView != null) {
	// int deltaX = (int)Math.abs(e1.getX()-e2.getX());
	// int deltaY = (int)Math.abs(e1.getY() - e2.getY());
	//
	// if (deltaX > mDragView.getWidth()/2 && deltaY < mDragView.getHeight()) {
	// mRemoveListener.onRemove(mStartPosition);
	// }
	//
	// stopDrag(mStartPosition - getFirstVisiblePosition());
	//
	// return true;
	// }
	// return false;
	// }
	// });
	// }
	/*
	 * pointToPosition() doesn't consider invisible views, but we need to, so
	 * implement a slightly different version.
	 */

	// private WindowManager mWindowManager;
	// private WindowManager.LayoutParams mWindowParams;
	private int mDragPos; // which item is being dragged
	private int mFirstDragPos; // where was the dragged item originally
	private int mDragPoint; // at what offset inside the item did the user grab
							// it
	// private int mCoordOffset; // the difference between screen coordinates
	// and
	// coordinates in this view
	private int mUpperBound;
	private int mLowerBound;
	private int mHeight;
	private static final int FLING = 0;
	private static final int SLIDE = 1;
	private int mRemoveMode = -1;
	private Rect mTempRect = new Rect();
	// private Bitmap mDragBitmap;
	// private final int mTouchSlop;
	private int mItemHeightNormal;
	private int mItemHeightExpanded;

	private int myPointToPosition(int x, int y) {
		Rect frame = mTempRect;
		final int count = getChildCount();
		for (int i = count - 1; i >= 0; i--) {
			final View child = getChildAt(i);
			child.getHitRect(frame);
			if (frame.contains(x, y)) {
				return getFirstVisiblePosition() + i;
			}
		}
		return INVALID_POSITION;
	}

	private int getItemForPosition(int y) {
		int adjustedy = y - mDragPoint - 32;
		int pos = myPointToPosition(0, adjustedy);
		if (pos >= 0) {
			if (pos <= mFirstDragPos) {
				pos += 1;
			}
		} else if (adjustedy < 0) {
			pos = 0;
		}
		return pos;
	}

	private void adjustScrollBounds(int y) {
		if (y >= mHeight / 3) {
			mUpperBound = mHeight / 3;
		}
		if (y <= mHeight * 2 / 3) {
			mLowerBound = mHeight * 2 / 3;
		}
	}

	private void unExpandViews(boolean deletion) {
		for (int i = 0;; i++) {
			View v = getChildAt(i);
			if (v == null) {
				if (deletion) {
					// HACK force update of mItemCount
					int position = getFirstVisiblePosition();
					int y = getChildAt(0).getTop();
					setAdapter(getAdapter());
					setSelectionFromTop(position, y);
					// end hack
				}
				layoutChildren(); // force children to be recreated where needed
				v = getChildAt(i);
				if (v == null) {
					break;
				}
			}
			ViewGroup.LayoutParams params = v.getLayoutParams();
			params.height = mItemHeightNormal;
			v.setLayoutParams(params);
			v.setVisibility(View.VISIBLE);
		}
	}

	private void doExpansion() {
		int childnum = mDragPos - getFirstVisiblePosition();
		if (mDragPos > mFirstDragPos) {
			childnum++;
		}

		View first = getChildAt(mFirstDragPos - getFirstVisiblePosition());

		for (int i = 0;; i++) {
			View vv = getChildAt(i);
			if (vv == null) {
				break;
			}
			int height = mItemHeightNormal;
			int visibility = View.VISIBLE;
			if (vv.equals(first)) {
				// processing the item that is being dragged
				if (mDragPos == mFirstDragPos) {
					// hovering over the original location
					// visibility = View.INVISIBLE;
				} else {
					// not hovering over it
					height = 1;
				}
			} else if (i == childnum) {
				if (mDragPos < getCount() - 1) {
					height = mItemHeightExpanded;
				}
			}
			ViewGroup.LayoutParams params = vv.getLayoutParams();
			params.height = height;
			vv.setLayoutParams(params);
			vv.setVisibility(visibility);
		}
	}
}
