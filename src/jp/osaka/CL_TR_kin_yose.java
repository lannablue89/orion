package jp.osaka;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_TR_kin_yose {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_TR_kin_yose(Context context) {
		try {
			dbHelper = new DatabaseHelper(context);
			database = dbHelper.getWritableDatabase();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public Cursor getAllDisaster() {
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from QQ_tr_kin_yousei Where kin_kbn=1 and  status_flg=1 order by kin_no asc,kin_branch_no desc",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getDisasterByKinno(String kin_no) {
		Cursor mCursor = null;
		try {
			String[] args = { kin_no };
			mCursor = database
					.rawQuery(
							"Select * from QQ_tr_kin_yousei Where kin_no=? and kin_kbn=1 and  status_flg=1",
							args);
			if (mCursor != null) {
				mCursor.moveToFirst();

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String checkExistDisaster(String SHCACode) {
		String value = "";
		Cursor mCursor = null;
		try {
			String condition = "";
			if (SHCACode.equals("99")) {
				condition = " and b.block_cd in ('00080', '00090','00100', '00110')";
			} else {
				condition = " and b.block_cd='000" + SHCACode+"'";
			}
			String sql = "Select a.* from QQ_tr_kin_yousei a inner join qq_tr_kin_yousei_block b on (a.kin_no=b.kin_no and a.kin_branch_no=b.kin_branch_no) where isshow=1 and kin_kbn=1 and status_flg=1 ";
			sql = sql + condition;
			sql += " order by Insert_DateTime ASC Limit 1";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					value = mCursor.getString(mCursor
							.getColumnIndex("Insert_DateTime"));

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return value;
	}

	public String UpdateDisaster(String SHCACode) {
		String value = "";
		Cursor mCursor = null;
		try {
			String condition = "";
			if (SHCACode.equals("99")) {
				condition = " and b.block_cd in ('00080', '00090','00100', '00110')";
			} else {
				condition = " and b.block_cd='000" + SHCACode+"'";
			}
			String sql = "Select a.* from QQ_tr_kin_yousei a inner join qq_tr_kin_yousei_block b on (a.kin_no=b.kin_no and a.kin_branch_no=b.kin_branch_no) where isshow=1 and kin_kbn=1 and status_flg=1 ";
			sql = sql + condition;
			sql += " order by Insert_DateTime ASC Limit 1";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					value = mCursor.getString(mCursor
							.getColumnIndex("Insert_DateTime"));

					String kin_no = mCursor.getString(mCursor
							.getColumnIndex("kin_no"));

					String where = "kin_no=?";
					String[] args = { kin_no };
					ContentValues values = new ContentValues();
					values.put("isshow", 0);
					database.update("QQ_tr_kin_yousei", values, where, args);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public int checkDisasterNotProcess(String SHCACode) {
		int value = 0;
		Cursor mCursor = null;
		try {
			String condition = "";
			if (SHCACode.equals("99")) {
				condition = " and b.block_cd in ('00080', '00090','00100', '00110')";
			} else {
				condition = " and b.block_cd='000" + SHCACode+"' ";
			}
			String sql = "Select a.* from QQ_tr_kin_yousei a inner join qq_tr_kin_yousei_block b on (a.kin_no=b.kin_no and a.kin_branch_no=b.kin_branch_no) where  kin_kbn=1 and status_flg=1 ";
			sql = sql + condition;
			sql += " order by Insert_DateTime ASC Limit 1";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					value = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("kin_no")));

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public int checkDisasterNotProcess(String kin_no, String SHCACode) {
		int value = 0;
		Cursor mCursor = null;
		try {

			if (kin_no.equals("") == false) {
				String[] args = { kin_no };
				String condition = "";
				if (SHCACode.equals("99")) {
					condition = " and b.block_cd in ('00080', '00090','00100', '00110')";
				} else {
					condition = " and b.block_cd='000" + SHCACode+"' ";
				}
				String sql = "Select a.* from QQ_tr_kin_yousei a inner join qq_tr_kin_yousei_block b on (a.kin_no=b.kin_no and a.kin_branch_no=b.kin_branch_no) where a.kin_no <> ?  and kin_kbn=1 and status_flg=1 ";
				sql = sql + condition;
				sql += " order by Insert_DateTime ASC Limit 1";
				mCursor = database.rawQuery(sql, args);
			} else {
				String condition = "";
				if (SHCACode.equals("99")) {
					condition = " and b.block_cd in ('00080', '00090','00100', '00110')";
				} else {
					condition = " and b.block_cd='000" + SHCACode+"'";
				}
				String sql = "Select a.* from QQ_tr_kin_yousei a inner join qq_tr_kin_yousei_block b on (a.kin_no=b.kin_no and a.kin_branch_no=b.kin_branch_no) where   kin_kbn=1 and status_flg=1 ";
				sql = sql + condition;
				sql += " order by Insert_DateTime ASC Limit 1";
				mCursor = database.rawQuery(sql, null);
			}
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor.getColumnIndex("kin_no")) != null) {
					value = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("kin_no")));

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public boolean deleteDisaster(int kin_no) {
		boolean result = false;
		try {
			String where = "kin_no=?";
			String[] args = { String.valueOf(kin_no) };
			result = database.delete("QQ_tr_kin_yousei", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public String updateDisaster(JSONObject json1) {
		// jsonTR: ms_code,tr_oujyu,ms_medicalinstitution
		String datetime = "";
		int flag = 0;
		database.beginTransaction();
		try {
			JSONObject json = json1.getJSONObject("AllTableYousei");
			datetime = json1.getString("DateTime");

			String DateTimeNow = Utilities.getDateTimeNow();
			JSONArray jArray;
			if (!json.isNull("QQ_tr_kin_yousei".toLowerCase())) {
				jArray = json.getJSONArray("QQ_tr_kin_yousei".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("kin_no", e.getString("kin_no".toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no".toLowerCase()));
					values.put("kin_kbn", e.getString("kin_kbn".toLowerCase()));
					values.put("status_flg",
							e.getString("status_flg".toLowerCase()));
					values.put("Update_DateTime", DateTimeNow);
					values.put("kin_date",
							e.getString("kin_date".toLowerCase()));
					values.put("kin_time",
							e.getString("kin_time".toLowerCase()));
					if (!e.isNull("title".toLowerCase()))
						values.put("title", e.getString("title".toLowerCase()));
					else
						values.putNull("title");
					values.put("cre_date",
							e.getString("cre_date2".toLowerCase()));
					if (!e.isNull("jusho_name".toLowerCase()))
						values.put("jusho_name",
								e.getString("jusho_name".toLowerCase()));
					else
						values.putNull("jusho_name");
					values.put("qqsbt2", e.getString("qqsbt2".toLowerCase()));
					values.put("qqsbt3", e.getString("qqsbt3".toLowerCase()));
					values.put("yousei_level",
							e.getString("yousei_level".toLowerCase()));
					values.put("dmat_yousei_f",
							e.getString("dmat_yousei_f".toLowerCase()));
					String kin_no = e.getString("kin_no".toLowerCase());
					String kin_branch_no = e.getString("kin_branch_no"
							.toLowerCase());
					String[] args = { kin_no, kin_branch_no };
					String sql = "Select * from QQ_tr_kin_yousei Where kin_no=? and kin_branch_no=?";
					Cursor mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {
						String where = "kin_no=? and kin_branch_no=?";
						database.update("QQ_tr_kin_yousei", values, where, args);
					} else {
						values.put("isshow", 1);
						values.put("Insert_DateTime", DateTimeNow);
						flag = 1;
						database.insert("QQ_tr_kin_yousei", null, values);
					}
				}
			}
			if (!json.isNull("QQ_tr_kin_yousei_kahi".toLowerCase())) {
				jArray = json.getJSONArray("QQ_tr_kin_yousei_kahi"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("kin_no", e.getString("kin_no".toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no".toLowerCase()));
					values.put("kikan_cd",
							e.getString("kikan_cd".toLowerCase()));
					values.put("ukeire_kahi",
							e.getString("ukeire_kahi".toLowerCase()));
					values.put("entrydate",
							e.getString("entrydate".toLowerCase()));
					values.put("entrytime",
							e.getString("entrytime".toLowerCase()));
					values.put("bikou", e.getString("bikou".toLowerCase()));
					values.put("tel_flg", e.getString("tel_flg".toLowerCase()));
					values.put("delivery_flg",
							e.getString("delivery_flg".toLowerCase()));
					values.put("sick_h", e.getString("sick_h".toLowerCase()));
					values.put("sick_m", e.getString("sick_m".toLowerCase()));
					values.put("sick_l", e.getString("sick_l".toLowerCase()));
					values.put("cre_kikan_cd",
							e.getString("cre_kikan_cd".toLowerCase()));
					values.put("upd_kikan_cd",
							e.getString("upd_kikan_cd".toLowerCase()));
					values.put("ap_upd_date",
							e.getString("ap_upd_date".toLowerCase()));
					values.put("data_upd_date",
							e.getString("data_upd_date".toLowerCase()));
					values.put("cre_date",
							e.getString("cre_date2".toLowerCase()));
					values.put("upd_date",
							e.getString("upd_date2".toLowerCase()));

					database.replace("QQ_tr_kin_yousei_kahi", null, values);

				}
			}
			if (!json.isNull("qq_tr_kin_yousei_block".toLowerCase())) {
				jArray = json.getJSONArray("qq_tr_kin_yousei_block"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("kin_no", e.getString("kin_no".toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no".toLowerCase()));
					values.put("block_cd",
							e.getString("block_cd".toLowerCase()));
					values.put("cre_kikan_cd",
							e.getString("cre_kikan_cd".toLowerCase()));
					values.put("upd_kikan_cd",
							e.getString("upd_kikan_cd".toLowerCase()));
					values.put("cre_date",
							e.getString("cre_date2".toLowerCase()));
					values.put("upd_date",
							e.getString("upd_date2".toLowerCase()));

					database.replace("qq_tr_kin_yousei_block", null, values);

				}
			}
			if (!json.isNull("qq_tr_kin_yousei_kamk".toLowerCase())) {
				jArray = json.getJSONArray("qq_tr_kin_yousei_kamk"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("kin_no", e.getString("kin_no".toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no".toLowerCase()));
					values.put("kamoku_cd",
							e.getString("kamoku_cd".toLowerCase()));
					values.put("cre_kikan_cd",
							e.getString("cre_kikan_cd".toLowerCase()));
					values.put("upd_kikan_cd",
							e.getString("upd_kikan_cd".toLowerCase()));
					values.put("cre_date",
							e.getString("cre_date2".toLowerCase()));
					values.put("upd_date",
							e.getString("upd_date2".toLowerCase()));

					database.replace("qq_tr_kin_yousei_kamk", null, values);

				}
			}
			if (!json.isNull("qq_tr_kin_yousei_kikan".toLowerCase())) {
				jArray = json.getJSONArray("qq_tr_kin_yousei_kikan"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("kin_no", e.getString("kin_no".toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no".toLowerCase()));
					values.put("kikan_cd",
							e.getString("kikan_cd".toLowerCase()));
					values.put("cre_kikan_cd",
							e.getString("cre_kikan_cd".toLowerCase()));
					values.put("upd_kikan_cd",
							e.getString("upd_kikan_cd".toLowerCase()));
					values.put("cre_date",
							e.getString("cre_date2".toLowerCase()));
					values.put("upd_date",
							e.getString("upd_date2".toLowerCase()));

					database.replace("qq_tr_kin_yousei_kikan", null, values);

				}
			}
			if (!json.isNull("TR_AllReqMedicalInstitution_kahi".toLowerCase())) {
				jArray = json.getJSONArray("TR_AllReqMedicalInstitution_kahi"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("CaseNo", e.getString("CaseNo".toLowerCase()));
					values.put("MICode", e.getString("MICode".toLowerCase()));
					values.put("ukeire_kahi",
							e.getString("ukeire_kahi".toLowerCase()));
					values.put("Insert_Datetime",
							e.getString("Insert_Datetime".toLowerCase()));
					values.put("EmergencyRecognition_kbn",
							e.getString("EmergencyRecognition_kbn"
									.toLowerCase()));

					database.replace("TR_AllReqMedicalInstitution_kahi", null,
							values);

				}
			}
			if (!json.isNull("tr_case_saisum".toLowerCase())) {
				jArray = json.getJSONArray("tr_case_saisum"
						.toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put("FireStationCode",
							e.getString("FireStationCode".toLowerCase()));
					values.put("CaseNo", e.getString("CaseNo".toLowerCase()));
					values.put("emsunitcode", e.getString("emsunitcode".toLowerCase()));
					values.put("deleteflg",
							e.getString("deleteflg".toLowerCase()));
					values.put("insert_datetime",
							e.getString("insert_datetime".toLowerCase()));
					values.put("update_datetime",
							e.getString("update_datetime"
									.toLowerCase()));
					values.put("status",
							e.getString("status"
									.toLowerCase()));
					values.put("facilitiescode",
							e.getString("facilitiescode"
									.toLowerCase()));
					values.put("facilitiesname",
							e.getString("facilitiesname"
									.toLowerCase()));
					values.put("facilities_datetime",
							e.getString("facilities_datetime"
									.toLowerCase()));
					values.put("status_h",
							e.getString("status_h"
									.toLowerCase()));
					values.put("patientstate",
							e.getString("patientstate"
									.toLowerCase()));
					values.put("starttriageassessment",
							e.getString("starttriageassessment"
									.toLowerCase()));
					values.put("starttriageassessmentdatetime",
							e.getString("starttriageassessmentdatetime"
									.toLowerCase()));
					values.put("pattriageassessment",
							e.getString("pattriageassessment"
									.toLowerCase()));
					values.put("pattriageassessmentdatetime",
							e.getString("pattriageassessmentdatetime"
									.toLowerCase()));
					values.put("kin_no",
							e.getString("kin_no"
									.toLowerCase()));
					values.put("kin_branch_no",
							e.getString("kin_branch_no"
									.toLowerCase()));
					values.put("kinlink_datetime",
							e.getString("kinlink_datetime"
									.toLowerCase()));
					values.put("kin_kbn",
							e.getString("kin_kbn"
									.toLowerCase()));
					values.put("lastmicord",
							e.getString("lastmicord"
									.toLowerCase()));
					values.put("lastminame",
							e.getString("lastminame"
									.toLowerCase()));
					database.replace("tr_case_saisum", null,
							values);

				}
			}
			// String where = "Update_DateTime<?";
			// String[] args = { DateTimeNow };
			// database.delete("QQ_tr_kin_yousei", where, args);

			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return datetime;

	}

	public int countyousei_kahi(String ukeire_kahi, String kin_no,
			String kin_branch_no) {
		int value = 0;
		Cursor mCursor = null;
		try {

			if (ukeire_kahi.equals("") == false) {
				String[] args = { kin_no, kin_branch_no, ukeire_kahi };
				mCursor = database
						.rawQuery(
								"Select count(*) as SL from QQ_tr_kin_yousei_kahi where kin_no = ? and kin_branch_no=? and ukeire_kahi=? ",
								args);
			} else {
				String[] args = { kin_no, kin_branch_no };
				mCursor = database
						.rawQuery(
								"Select count(*) as SL from QQ_tr_kin_yousei_kahi where kin_no = ? and kin_branch_no=?",
								args);
			}
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor.getColumnIndex("SL")) != null) {
					value = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("SL")));

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}
}
