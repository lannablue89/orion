﻿package jp.osaka;

import java.io.File;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class E030 extends ListActivity {

	String FolderPicture = "multimedia/pictures";
	String FolderVideo = "multimedia/videos";
	String FolderVoice = "multimedia/voices";
	String PathFolder = "";
	DTO_FileInfo dtDel;
	profileAdapter listFile;
	String FireStationCode;
	String CaseCode;
	TextView txtTotalSize, txtTotalSizeUp;
	int LimitSize = 4 * 1024;
	int totalsize;
	int modeedit = 0;
	Button btnEdit;
	ListView lvFile;
	boolean bFileSort = false;
	int Key;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layoute030);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		LimitSize=b.getInt("MaxSize");
		Key=b.getInt("Key");
		try {
			getModelSetting();
			txtTotalSize = (TextView) findViewById(R.id.txtTotalSize);
			txtTotalSize.setText(formatSize(LimitSize));
			txtTotalSizeUp = (TextView) findViewById(R.id.txtTotalSizeUp);

			btnEdit = (Button) findViewById(R.id.btnEdit);
			btnEdit.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if (modeedit == 0) {
						modeedit = 1;
						btnEdit.setText("完了");
						btnEdit.setBackgroundResource(R.drawable.btnedit);
						if (lvFile instanceof DragNDropListView) {
							((DragNDropListView) lvFile).bAllowEdit = true;

						}

					} else {
						modeedit = 0;
						btnEdit.setText("並び替え");
						btnEdit.setBackgroundResource(R.drawable.btnsended);
						if (lvFile instanceof DragNDropListView) {
							((DragNDropListView) lvFile).bAllowEdit = false;

						}
						for (int i = 0; i < listFile.getCount(); i++) {
							CL_FileInfo cl = new CL_FileInfo(E030.this);
							DTO_FileInfo dt = listFile.getItem(i);
							cl.updateTR_FileInfoSort(FireStationCode, CaseCode,
									dt.MultimediaFileNo, dt.FileType,
									dt.FileSort);
						}
						bFileSort = true;
					}
				}
			});
			Button btnOk = (Button) findViewById(R.id.btnOK);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent t = new Intent();
					t.putExtra("TotalSize", totalsize);
					setResult(Key, t);
					finish();
				}
			});
			listFile = new profileAdapter(this, R.layout.layoutgride30);
			// ListView lvFile = (ListView) findViewById(R.id.listFileInfo);
			lvFile = getListView();
			lvFile.setAdapter(listFile);
			lvFile.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					dtDel = listFile.getItem(arg2);
					if (modeedit == 0) {
						showDialogComment();
					}
				}
			});

			// lvFile.setOnItemLongClickListener(new OnItemLongClickListener() {
			//
			// public boolean onItemLongClick(AdapterView<?> arg0, View v,
			// int arg2, long arg3) {
			// // TODO Auto-generated method stub
			// if (modeedit == 1) {
			// dtDel = listFile.getItem(arg2);
			// DragShadow dragShadow = new DragShadow(v);
			//
			// ClipData data = ClipData.newPlainText("", "");
			// v.startDrag(data, dragShadow, v, 0);
			// return true;
			// } else {
			// return false;
			// }
			// }
			// });
			if (lvFile instanceof DragNDropListView) {
				((DragNDropListView) lvFile).setDropListener(mDropListener);
				((DragNDropListView) lvFile).setRemoveListener(mRemoveListener);
				((DragNDropListView) lvFile).setDragListener(mDragListener);
			}
			getListFile();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E030.this);
		}
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent t = new Intent();
			t.putExtra("TotalSize", totalsize);
			setResult(Key, t);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
	}


	private DropListener mDropListener = new DropListener() {
		public void onDrop(int from, int to) {
			if (modeedit == 1) {
				// ListAdapter adapter = getListAdapter();
				// if (adapter instanceof DragNDropAdapter) {
				// ((DragNDropAdapter) adapter).onDrop(from, to);
				// getListView().invalidateViews();
				// }
				DTO_FileInfo dt = listFile.getItem(from);
				dt.bHide = false;
				listFile.remove(dt);
				listFile.insert(dt, to);
				listFile.notifyDataSetChanged();
				for (int i = 0; i < listFile.getCount(); i++) {
					dt = listFile.getItem(i);
					dt.FileSort = String.valueOf(i + 1);
				}
				lvFile.setAdapter(listFile);
				// lvFile.in
				listFile.notifyDataSetChanged();
				lvFile.setSelection(to);
			}
		}

	};

	private RemoveListener mRemoveListener = new RemoveListener() {
		public void onRemove(int which) {
			if (modeedit == 1) {
				// ListAdapter adapter = getListAdapter();
				// if (adapter instanceof DragNDropAdapter) {
				// ((DragNDropAdapter) adapter).onRemove(which);
				// getListView().invalidateViews();
				// }

				// Hide item drag
				DTO_FileInfo dt = listFile.getItem(which);
				dt.bHide = true;
				listFile.remove(dt);
			}
		}

	};

	private DragListener mDragListener = new DragListener() {

		int backgroundColor = 0xe0103010;
		int defaultBackgroundColor;

		public void onDrag(int x, int y, ListView listView) {
			// TODO Auto-generated method stub
		}

		public void onStartDrag(View itemView) {
			if (modeedit == 1) {
				itemView.setVisibility(View.INVISIBLE);
				defaultBackgroundColor = itemView
						.getDrawingCacheBackgroundColor();
				itemView.setBackgroundColor(backgroundColor);
				// ImageView iv = (ImageView)
				// itemView.findViewById(R.id.imageView1);
				// if (iv != null)
				// iv.setVisibility(View.INVISIBLE);
			}
		}

		public void onStopDrag(View itemView) {
			if (modeedit == 1) {
				if (itemView != null) {
					itemView.setVisibility(View.VISIBLE);
					itemView.setBackgroundColor(defaultBackgroundColor);
					// ImageView iv = (ImageView)
					// itemView.findViewById(R.id.imageView1);
					// if (iv != null)
					// iv.setVisibility(View.VISIBLE);
				}
			}
		}

	};

	private void getListFile() {

		Cursor cur = null;
		try {
			if (listFile != null)
				listFile.clear();
			CL_FileInfo cl = new CL_FileInfo(E030.this);
			cur = cl.getTR_FileInfo(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				int stt = 0;
				do {
					stt++;
					String MultimediaFileNo = cur.getString(cur
							.getColumnIndex("MultimediaFileNo"));
					String FileRegistered_DateTime = cur.getString(cur
							.getColumnIndex("FileRegistered_DateTime"));
					String FileName = cur.getString(cur
							.getColumnIndex("FileName"));
					String FileTime = cur.getString(cur
							.getColumnIndex("FileTime"));
					String FileSize = cur.getString(cur
							.getColumnIndex("FileSize"));
					DTO_FileInfo dt = new DTO_FileInfo();
					dt.MultimediaFileNo = MultimediaFileNo;
					dt.FileRegistered_DateTime = FileRegistered_DateTime;
					dt.FileType = cur.getString(cur.getColumnIndex("FileType"));

					String mFileName;
					if (PathFolder==null||PathFolder.equals("")) {
						mFileName = Environment.getExternalStorageDirectory()
								.getAbsolutePath();
						if (dt.FileType.equals("1")) {
							mFileName += "/" + FolderVoice;
						} else if (dt.FileType.equals("2")) {
							mFileName += "/" + FolderPicture;
						} else {
							mFileName += "/" + FolderVideo;
						}
					} else {
						mFileName = PathFolder;
					}
					dt.FileName = mFileName + "/" + FileName;
					dt.FileSize = FileSize;
					totalsize += Integer.parseInt(FileSize);
					if (FileTime != null && FileTime.equals("") == false)
						dt.FileTime = Utilities.formatTime(Integer
								.parseInt(FileTime));
					else
						dt.FileTime = "--:--:--";
					if (cur.getString(cur.getColumnIndex("FileSort")) != null) {
						dt.FileSort = cur.getString(cur
								.getColumnIndex("FileSort"));
						bFileSort = true;
					} else
						dt.FileSort = String.valueOf(stt);
					listFile.add(dt);
				} while (cur.moveToNext());
				// listFile.notifyDataSetChanged();
			}
			txtTotalSizeUp.setText(formatSize(totalsize));
			if (totalsize > LimitSize) {
				txtTotalSizeUp.setTextColor(Color.RED);
			} else {
				txtTotalSizeUp.setTextColor(Color.BLACK);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	private String formatSize(int size) {
		if (size < 1024)
			return String.valueOf(size) + "KB";
		else {
			double res = Utilities.roundTwoDecimals(size / 1024);
			return String.valueOf(res) + "MB";
		}
	}

	private void getModelSetting() {
		Cursor cur = null;
		try {
			CL_ModelSetting cl = new CL_ModelSetting(E030.this);
			cur = cl.getModelSetting(android.os.Build.MODEL);
			if (cur != null && cur.getCount() > 0) {
				PathFolder = cur.getString(cur.getColumnIndex("Location_Name"));

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();

		}
	}

	private void confirmDeleteItem() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				CL_FileInfo cl = new CL_FileInfo(E030.this);
				cl.deleteTR_FileInfo(FireStationCode, CaseCode,
						dtDel.MultimediaFileNo, dtDel.FileType);

				File f = new File(dtDel.FileName);
				if (f.exists())
					f.delete();

				listFile.remove(dtDel);
				if (bFileSort) {
					cl = new CL_FileInfo(E030.this);
					cl.updateTR_FileInfoSortFrom(FireStationCode, CaseCode,
							dtDel.FileSort);
				}
				listFile.notifyDataSetChanged();
				for (int i = 0; i < listFile.getCount(); i++) {
					DTO_FileInfo dt = listFile.getItem(i);
					dt.FileSort = String.valueOf(i + 1);
				}
				listFile.notifyDataSetChanged();
				// listFile.notifyDataSetChanged();
				totalsize = totalsize - Integer.parseInt(dtDel.FileSize);
				txtTotalSizeUp.setText(formatSize(totalsize));
				if (totalsize > LimitSize) {
					txtTotalSizeUp.setTextColor(Color.RED);
				} else {
					txtTotalSizeUp.setTextColor(Color.BLACK);
				}
				// String memory = getAvailableInternalMemorySize();
				// txtMemory.setText(memory);
				dialog.cancel();
			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteItem);
		lbmg.setText(mg);

		dialog.show();
	}

	private void showDialogComment() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoutcomment);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditText txtComment = (EditText) dialog
						.findViewById(R.id.txtComment);
				CL_FileInfo cl = new CL_FileInfo(E030.this);
				long res = cl.updateTR_FileInfo(FireStationCode, CaseCode,
						dtDel.MultimediaFileNo, dtDel.FileType, txtComment
								.getText().toString());
				dtDel.FileComment = txtComment.getText().toString();
				listFile.notifyDataSetChanged();
				if (res > 0) {
					dialog.cancel();
				}
			}
		});

		dialog.show();
	}

	public class profileAdapter extends ArrayAdapter<DTO_FileInfo> {

		public profileAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			// View v = convertView;
			// ViewWraperDV mwp;
			//
			// if (v == null) {
			// LayoutInflater l = getLayoutInflater();
			// v = l.inflate(R.layout.layoutgride30, null);
			// mwp = new ViewWraperDV(v);
			// v.setTag(mwp);
			// } else {
			//
			// mwp = (ViewWraperDV) convertView.getTag();
			// }
			// TextView txtSTT = mwp.getSTT();
			// TextView txtDate = mwp.getDate();
			// TextView txtTime = mwp.getTime();
			// TextView txtSize = mwp.getSize();
			// TextView txtComment = mwp.getComment();
			// Button btnDel = mwp.getbtnDel();
			// ImageView img = mwp.getImage();
			// ImageView imgVideo = mwp.getImageVideo();

			View v;
			LayoutInflater l = getLayoutInflater();
			v = l.inflate(R.layout.layoutgride30, null);

			TextView txtSTT = (TextView) v.findViewById(R.id.txtSTT);
			TextView txtDate = (TextView) v.findViewById(R.id.txtDate);
			TextView txtTime = (TextView) v.findViewById(R.id.txtFileTime);
			TextView txtSize = (TextView) v.findViewById(R.id.txtFileSize);
			TextView txtComment = (TextView) v.findViewById(R.id.txtComment);
			Button btnDel = (Button) v.findViewById(R.id.btnDel);
			ImageView img = (ImageView) v.findViewById(R.id.imageView1);
			ImageView imgVideo = (ImageView) v.findViewById(R.id.imageVideo);
			//LinearLayout row = (LinearLayout) v.findViewById(R.id.layrow);

			final DTO_FileInfo dt = this.getItem(position);
			String date = dt.FileRegistered_DateTime.substring(4, 6) + "/"
					+ dt.FileRegistered_DateTime.substring(6, 8) + " "
					+ dt.FileRegistered_DateTime.substring(8, 10) + ":"
					+ dt.FileRegistered_DateTime.substring(10, 12);
			txtSTT.setText(dt.FileSort);
			txtDate.setText(date);
			txtTime.setText(dt.FileTime);
			txtSize.setText(formatSize(Integer.parseInt(dt.FileSize)));
			txtComment.setText(dt.FileComment);

			btnDel.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dtDel = dt;
					confirmDeleteItem();
				}
			});
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			if (dt.FileType.equals("1")) {
				// img.setBackgroundResource(R.drawable.notnhac);

				Bitmap bm = BitmapFactory.decodeResource(
						E030.this.getResources(), R.drawable.notnhac);
				bm = scaleDownBitmap(bm, 100, E030.this);
				img.setImageBitmap(bm);
				imgVideo.setVisibility(View.INVISIBLE);
			} else if (dt.FileType.equals("2")) {
				Bitmap bmp = Bitmap
						.createBitmap(decodeSampledBitmapFromResource(
								dt.FileName, 50, 50));
				img.setImageBitmap(bmp);
				imgVideo.setVisibility(View.INVISIBLE);
			} else {
				Bitmap bm = ThumbnailUtils.createVideoThumbnail(dt.FileName,
						MediaStore.Images.Thumbnails.MINI_KIND);
				img.setScaleType(ImageView.ScaleType.CENTER_CROP);
				img.setImageBitmap(bm);
				imgVideo.setVisibility(View.VISIBLE);
			}
			img.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					if (dt.FileType.equals("1")) {
						Intent intent = new Intent();
						intent.setAction(android.content.Intent.ACTION_VIEW);
						File file = new File(dt.FileName);
						intent.setDataAndType(Uri.fromFile(file), "audio/*");
						startActivity(intent);
					} else if (dt.FileType.equals("2")) {
						Intent intent = new Intent();
						intent.setAction(android.content.Intent.ACTION_VIEW);
						File file = new File(dt.FileName);
						intent.setDataAndType(Uri.fromFile(file), "image/*");
						startActivity(intent);
					} else {
						Intent intent = new Intent();
						intent.setAction(android.content.Intent.ACTION_VIEW);
						File file = new File(dt.FileName);
						intent.setDataAndType(Uri.fromFile(file), "video/*");
						startActivity(intent);
					}
				}
			});
			return v;
		}
	}

	class ViewWraperDV {

		View base;

		TextView txt1 = null;
		TextView txt2 = null;
		TextView txt3 = null;
		TextView txt4 = null;
		TextView txt5 = null;
		ImageView img = null;
		ImageView img2 = null;
		Button btnDel = null;
		LinearLayout row = null;

		ViewWraperDV(View base) {

			this.base = base;
		}

		LinearLayout getRow() {
			if (row == null)
				row = (LinearLayout) base.findViewById(R.id.layrow);
			return row;
		}

		ImageView getImage() {
			if (img == null)
				img = (ImageView) base.findViewById(R.id.imageView1);
			return img;
		}

		ImageView getImageVideo() {
			if (img2 == null) {
				img2 = (ImageView) base.findViewById(R.id.imageVideo);
			}
			return img2;
		}

		TextView getSTT() {
			if (txt1 == null) {
				txt1 = (TextView) base.findViewById(R.id.txtSTT);
			}
			return txt1;
		}

		TextView getDate() {
			if (txt2 == null) {
				txt2 = (TextView) base.findViewById(R.id.txtDate);
			}
			return txt2;
		}

		TextView getTime() {
			if (txt3 == null) {
				txt3 = (TextView) base.findViewById(R.id.txtFileTime);
			}
			return txt3;
		}

		TextView getSize() {
			if (txt4 == null) {
				txt4 = (TextView) base.findViewById(R.id.txtFileSize);
			}
			return txt4;
		}

		TextView getComment() {
			if (txt5 == null) {
				txt5 = (TextView) base.findViewById(R.id.txtComment);
			}
			return txt5;
		}

		Button getbtnDel() {
			if (btnDel == null) {
				btnDel = (Button) base.findViewById(R.id.btnDel);
			}
			return btnDel;
		}

	}

	public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight,
			Context context) {

		final float densityMultiplier = context.getResources()
				.getDisplayMetrics().density;

		int h = (int) (newHeight * densityMultiplier);
		int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

		photo = Bitmap.createScaledBitmap(photo, w, h, true);

		return photo;
	}

	public Bitmap decodeSampledBitmapFromResource(String fileName,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// BitmapFactory.decodeResource(res, resId, options);
		BitmapFactory.decodeFile(fileName, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(fileName, options);
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}


}
