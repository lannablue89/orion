﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_FireStationStandard {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_FireStationStandard(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	// MS_FireStationStandard
	public Cursor getMS_FireStationStandard(String DiseaseFLG,
			String ExternalWoundFLG, String SHCACode) {
		Cursor mCursor = null;
		try {
			// String[] args = { DiseaseFLG, ExternalWoundFLG, SHCACode };
			String[] args = { SHCACode };
			// Remove: ( a.ParentStandardClass is null and a.ParentStandardCode
			// is null and a.ParentStandardBranchCode is null ) or
			// (a.ParentStandardClass='' and a.ParentStandardCode='' and
			// a.ParentStandardBranchCode='')
			String sql = "Select a.* from MS_FireStationStandard a Inner Join MS_FireStationStandardSort b on"
					+ "(a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode and a.StandardBranchCode=b.StandardBranchCode)"
					+ " Where (a.ParentStandardClass='000' and a.ParentStandardCode='00000' and a.ParentStandardBranchCode='000') "
					+ "and a.DeleteFLG=0 and b.DeleteFLG=0  and b.SHCACode=? ";
			// Bug 150
			if (DiseaseFLG.equals("1")) {
				sql += " and a.DiseaseFLG=1";
			} else if (ExternalWoundFLG.equals("1")) {
				sql += " and a.ExternalWoundFLG=1";
			} else {
				sql += " and a.DiseaseFLG=0 and a.ExternalWoundFLG=0";
			}
			sql += " Order by a.StandardCode ASC";
			//
			// String sql =
			// "Select * from MS_FireStationStandard Where  DeleteFLG=0 and DiseaseFLG=? and ExternalWoundFLG=? limit 3 ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getMS_FireStationStandardChild(String DiseaseFLG,
			String ExternalWoundFLG, String SHCACode, String StandardClass,
			String StandardCode, String StandardBranchCode) {
		Cursor mCursor = null;
		try {
			String[] args = { StandardClass, StandardCode, StandardBranchCode,
					SHCACode };
			String sql = "Select a.* from MS_FireStationStandard a Inner Join MS_FireStationStandardSort b on"
					+ "(a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode and a.StandardBranchCode=b.StandardBranchCode)"
					+ " Where a.DeleteFLG=0 and b.DeleteFLG=0 and a.ParentStandardClass=? and a.ParentStandardCode =? and a.ParentStandardBranchCode =? "
					+ "  and b.SHCACode=? ";
			// Bug 150
			if (DiseaseFLG.equals("1")) {
				sql += " and a.DiseaseFLG=1";
			} else if (ExternalWoundFLG.equals("1")) {
				sql += " and a.ExternalWoundFLG=1";
			} else {
				sql += " and a.DiseaseFLG=0 and a.ExternalWoundFLG=0";
			}
			sql += " Order by b.SortNo ASC";
			//
			// String sql =
			// "Select * from MS_FireStationStandard Where  DeleteFLG=0 and DiseaseFLG=? and ExternalWoundFLG=? limit 3 ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public long updateMS_FireStationStandard() {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.putNull("ParentStandardClass");
			values.putNull("ParentStandardCode");
			values.putNull("ParentStandardBranchCode");

			result = database.update("MS_FireStationStandard", values, null,
					null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	// TR_FireStationStandard
	public long createTR_FireStationStandard(String FireStationCode,
			String CaseNo, String StandardClass, String StandardCode,
			String StandardBranchCode) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("StandardClass", StandardClass);
			values.put("StandardCode", StandardCode);
			values.put("StandardBranchCode", StandardBranchCode);
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			result = database.insert("TR_FireStationStandard", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteTR_FireStationStandard(String FireStationCode,
			String CaseNo, String StandardClass, String StandardCode,
			String StandardBranchCode) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and StandardClass=? and StandardCode=? and StandardBranchCode=?";
			String[] args = { FireStationCode, CaseNo, StandardClass,
					StandardCode, StandardBranchCode };
			result = database.delete("TR_FireStationStandard", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_FireStationStandard(String FireStationCode,
			String CaseNo, String DiseaseFLG, String ExternalWoundFLG,
			String SHCACode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, SHCACode };
			String sql = "Select a.* from MS_FireStationStandard a Inner Join MS_FireStationStandardSort b on"
					+ "(a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode and a.StandardBranchCode=b.StandardBranchCode)"
					+ " inner join TR_FireStationStandard c on (a.StandardClass=c.StandardClass and a.StandardCode=c.StandardCode and a.StandardBranchCode=c.StandardBranchCode)"
					+ " Where c.FireStationCode=? and c.CaseNo=? and a.DeleteFLG=0 and b.DeleteFLG=0  and b.SHCACode=?";
			if (DiseaseFLG.equals("1")) {
				sql += " and DiseaseFLG=1";
			} else if (ExternalWoundFLG.equals("1")) {
				sql += " and ExternalWoundFLG=1";
			} else {
				sql += " and DiseaseFLG=0 and ExternalWoundFLG=0";
			}
			sql += " Order by a.StandardCode ASC";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor get3TR_FireStationStandard(String FireStationCode,
			String CaseNo, String DiseaseFLG, String ExternalWoundFLG,
			String SHCACode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, SHCACode };
			String sql = "Select distinct a.* from MS_FireStationStandard a Inner Join MS_FireStationStandardSort b on"
					+ "(a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode and a.StandardBranchCode=b.StandardBranchCode)"
					+ " inner join TR_FireStationStandard c on (a.StandardClass=c.StandardClass and a.StandardCode=c.StandardCode and a.StandardBranchCode=c.StandardBranchCode)"
					+ " Where c.FireStationCode=? and c.CaseNo=? and a.DeleteFLG=0 and b.DeleteFLG=0  and b.SHCACode=?";
			if (DiseaseFLG.equals("1")) {
				sql += " and DiseaseFLG=1";
			} else if (ExternalWoundFLG.equals("1")) {
				sql += " and ExternalWoundFLG=1";
			} else {
				sql += " and DiseaseFLG=0 and ExternalWoundFLG=0";
			}
			sql += " Order by a.StandardCode ASC Limit 3";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getTR_FireStationStandardPatient(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_FireStationStandard Where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// Bug 107 20121010
	public boolean checkExistTR_FireStationStandard(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_FireStationStandard Where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return false;
	}

	public boolean checkHoliday() {
		boolean result = false;
		try {
			String datenow = Utilities.getDateNow();
			String[] args = { datenow };
			String sql = "Select * from MS_Holiday where Holiday_DATE=?";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				result = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	// Bug 153
	public boolean checkHoliday(String datenow) {
		boolean result = false;
		try {
			String[] args = { datenow };
			String sql = "Select * from MS_Holiday where Holiday_DATE=?";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				result = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;

	}

	public String getTimeDayNext(String MICode, String ConditionClass,
			String ConditionCode, String ConditionBranchCode,
			String DayGroupNo, String Day_CD, String TimeZoneNo) {
		String time = "-";
		try {
			String[] args = { MICode, ConditionClass, ConditionCode,
					ConditionBranchCode, DayGroupNo, Day_CD, TimeZoneNo };
			String sql = "Select * from MS_ConditionSupportTime where MICode=? and ConditionClass=? and ConditionCode=? and ConditionBranchCode=? and DayGroupNo=? and Day_CD=? and TimeZoneNo=? and DeleteFLG=0";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor
						.getColumnIndex("SupportStart_TIME")) != null) {
					time = mCursor.getString(mCursor
							.getColumnIndex("SupportStart_TIME"));
				} else
					time = "";
				if (mCursor
						.getString(mCursor.getColumnIndex("SupportEnd_TIME")) != null) {
					time += "-"
							+ mCursor.getString(mCursor
									.getColumnIndex("SupportEnd_TIME"));
				} else
					time += "-";

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return time;

	}

	//
	public Cursor getHopitalInfo13(String FireStationCode, String CaseNo,
			String SHCACode, String dayofweek,String daynext, String rdiSearch,
			String HopitalName, String SHAcodeSearch, String AreaCode) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();
			//String timenow = Utilities.getTimeNow();
			//String daynext = Utilities.getDayNextOfWeek();//BUG 153 20121026 
			// dayofweek=dayofweek.substring(4, 5);
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			// String[] args = { FireStationCode, CaseNo, SHCACode, dayofweek,
			// timenow, timenow, daynext, datenow, datenow,
			// FireStationCode, mountdate };
			String[] args = { FireStationCode, CaseNo, SHCACode, SHCACode,
					SHCACode, FireStationCode, dayofweek, daynext, datenow,
					datenow, mountdate };
			String sql = ContantSql.Select_HopitalInfo;
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " And (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			} else if (rdiSearch.equals("3")) {

				if (SHAcodeSearch != null && !SHAcodeSearch.equals("")) {
					sql += " And e.SHCACode='" + SHAcodeSearch + "'";

				}
				if (AreaCode != null && !AreaCode.equals("")) {
					sql += " And e.AreaCode='" + AreaCode + "'";

				}
			}
			// Change e.SortNo-> e.MIName_Kana //Bug 187,188 20121024
			sql += "  order by e.MIName_Kana,e.MICode,d.SortNo,d.ConditionClass,d.ConditionCode,d.ConditionBranchCode asc ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int checkHopitalValid13A(String FireStationCode, String CaseNo,
			String SHCACode) {
		int res = 0;
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();

			String[] args = { FireStationCode, CaseNo, SHCACode };
			String sql = "Select  distinct c.ConditionClass,c.ConditionCode,c.ConditionBranchCode    From    TR_FireStationStandard a "
					+ "	Inner Join   MS_SHCAFireStationStandard b "
					+ "	On (a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode "
					+ "and a.StandardBranchCode=b.StandardBranchCode) "
					+ "	Inner Join MS_SHCACondition c On b.CategoryCode=c.CategoryCode "
					+ "Where    a.FireStationCode=? and a. CaseNo=? "
					+ "and b.SHCACode=? "
					+ "and b.DeleteFLG=0 and c.DeleteFLG=0";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				res = mCursor.getCount();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return res;
	}

	public Cursor getHopitalInfo13B(String FireStationCode, String CaseNo,
			String dayofweek, String rdiSearch, String HopitalName,
			String SHAcodeSearch, String AreaCode) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();
			// String timenow = Utilities.getTimeNow();
			// String daynext = Utilities.getDayNextOfWeek();
			// dayofweek=dayofweek.substring(4, 5);
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			// String[] args = { FireStationCode, CaseNo, dayofweek, timenow,
			// timenow, daynext, datenow, datenow, FireStationCode,
			// mountdate };
			String[] args = { FireStationCode, CaseNo, datenow, datenow,
					mountdate };
			String sql = ContantSql.Select_Hopital13B;
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " And (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			} else if (rdiSearch.equals("3")) {

				if (SHAcodeSearch != null && !SHAcodeSearch.equals("")) {
					sql += " And e.SHCACode='" + SHAcodeSearch + "'";

				}
				if (AreaCode != null && !AreaCode.equals("")) {
					sql += " And e.AreaCode='" + AreaCode + "'";

				}
			}
			// Change e.SortNo-> e.MIName_Kana //bug 187,188
			sql += "  order by e.MIName_Kana,e.MICode,m.SortNo,m.KamokuCode asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getHopitalInfo13B_SearchALL(String dayofweek, String rdiSearch, String HopitalName,
			String SHAcodeSearch, String AreaCode) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();
			// String timenow = Utilities.getTimeNow();
			// String daynext = Utilities.getDayNextOfWeek();
			// dayofweek=dayofweek.substring(4, 5);
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			// String[] args = { FireStationCode, CaseNo, dayofweek, timenow,
			// timenow, daynext, datenow, datenow, FireStationCode,
			// mountdate };
			String[] args = {datenow, datenow,
					mountdate };
			String sql = ContantSql.Select_Hopital13B_SearchALL;
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " And (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			} else if (rdiSearch.equals("3")) {

				if (SHAcodeSearch != null && !SHAcodeSearch.equals("")) {
					sql += " And e.SHCACode='" + SHAcodeSearch + "'";

				}
				if (AreaCode != null && !AreaCode.equals("")) {
					sql += " And e.AreaCode='" + AreaCode + "'";

				}
			}
			// Change e.SortNo-> e.MIName_Kana //bug 187,188
			sql += "  order by e.MIName_Kana,e.MICode,m.SortNo,m.KamokuCode asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getHopitalInfo13B2(String FireStationCode, String CaseNo,
			String dayofweek, String rdiSearch, String HopitalName,
			String SHAcodeSearch, String AreaCode) {
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateTimeNow();
			// String timenow = Utilities.getTimeNow();
			// String daynext = Utilities.getDayNextOfWeek();
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			String[] args = { FireStationCode, CaseNo, date, date, datenow,
					datenow, mountdate };
			String sql = ContantSql.Select_Hopital13B2;
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " And (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			} else if (rdiSearch.equals("3")) {

				if (SHAcodeSearch != null && !SHAcodeSearch.equals("")) {
					sql += " And e.SHCACode='" + SHAcodeSearch + "'";

				}
				if (AreaCode != null && !AreaCode.equals("")) {
					sql += " And e.AreaCode='" + AreaCode + "'";

				}
			}
			// Change e.SortNo-> e.MIName_Kana //bug 187,188
			sql += "  order by e.MIName_Kana,e.MICode,m.SortNo,m.KamokuCode asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getHopitalInfo13B2_SearchAll(String dayofweek, String rdiSearch, String HopitalName,
			String SHAcodeSearch, String AreaCode) {
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateTimeNow();
			// String timenow = Utilities.getTimeNow();
			// String daynext = Utilities.getDayNextOfWeek();
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			String[] args = {  date, date, datenow,
					datenow, mountdate };
			String sql = ContantSql.Select_Hopital13B2_SearchALL;
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " And (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			} else if (rdiSearch.equals("3")) {

				if (SHAcodeSearch != null && !SHAcodeSearch.equals("")) {
					sql += " And e.SHCACode='" + SHAcodeSearch + "'";

				}
				if (AreaCode != null && !AreaCode.equals("")) {
					sql += " And e.AreaCode='" + AreaCode + "'";

				}
			}
			// Change e.SortNo-> e.MIName_Kana //bug 187,188
			sql += "  order by e.MIName_Kana,e.MICode,m.SortNo,m.KamokuCode asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public int checkHopitalValid13B(String FireStationCode, String CaseNo) {
		int res = 0;
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select  *  From  TR_SelectionKamoku Where FireStationCode=? and CaseNo=? ";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				res = mCursor.getCount();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return res;
	}

	public Cursor getTR_SelectionKamoku(String FireStationCode, String CaseNo) {

		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select  *  From  TR_SelectionKamoku Where FireStationCode=? and CaseNo=? ";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getHopitalInfo13C(String FireStationCode, String dayofweek) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();
			// String timenow = Utilities.getTimeNow();
			// String daynext = Utilities.getDayNextOfWeek();
			// dayofweek=dayofweek.substring(4, 5);
			String datenow = Utilities.getDateNow();
			String mountdate = Utilities.getMonthDateNow();
			// String[] args = { dayofweek, timenow, timenow, daynext, datenow,
			// datenow, FireStationCode, mountdate };
			String[] args = { datenow, datenow, mountdate };
			String sql = ContantSql.Select_Hopital13C;			
			//sql += "  order by e.SortNo asc";
			sql += "  order by e.MIName_Kana asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getMITElNo(String FireStationCode, String MICode) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();

			String[] args = { FireStationCode, MICode };
			String sql = "Select MITelNo from  MS_Instructor Where FireStationCode=? and MICode=? and DeleteFLG=0";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// Bug 144
	public Cursor getoujyutelno(String MICode) {
		Cursor mCursor = null;
		try {
			// String dayofweek=Utilities.getDayOfWeek();

			String[] args = { MICode };
			String sql = "Select distinct a.KamokuName,b.OujyuTelNo from  MS_Kamoku a Inner Join MS_KamokuTelNo b On a.KamokuCode=b.KamokuCode  Where b.MICode=? and b.OujyuTelNo is not null and a.DeleteFLG=0 and b.DeleteFLG=0 Order by b.SortNo ASC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// //////
	public Cursor getInfo14A(String MICode) {
		Cursor mCursor = null;
		try {

			String[] args = { MICode };
			String sql = "Select distinct a.*,b.KamokuName from TR_Oujyu a Inner Join MS_Kamoku b on a.KamokuCode=b.KamokuCode  Where a.MICode=? and a.DeleteFLG=0 and b.DeleteFLG=0 order by a.Update_DATETIME DESC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public boolean checkTR_Rotation(String MICode, String KamokuCode) {
		boolean reuslt = false;
		Cursor mCursor = null;
		try {
			// 20121017 Bug 169
			// String date = Utilities.getDateNow();
			// String[] args = { MICode, date };
			// String sql =
			// "Select *from TR_Rotation   Where MICode=? and KamokuCode in("
			// + KamokuCode + ") and Rotation_DATE=? and DeleteFLG=0 ";
			String date = Utilities.getDateTimeNow();
			String[] args = { MICode, date, date };
			String sql = "Select *from TR_Rotation   Where MICode=? and KamokuCode in("
					+ KamokuCode
					+ ") and RotationStart_DATETIME<=? and RotationEnd_DATETIME>=? and DeleteFLG=0 ";
			// End
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				reuslt = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return reuslt;
	}

	public Cursor getHopital17() {
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateNow();
			String[] args = { "''", date, date };
			String sql = "Select MICode,MIName_Kanji,ThirdCoordinateTelNo from MS_MedicalInstitution where DeleteFLG=0 and StopFLG=4 and ThirdCoordinateTelNo  is not null and ThirdCoordinateTelNo  !='' and ThirdCoordinateTelNo  !=? "
					+ " and ifnull(Opening_DATE,'00010101')<=? and ifnull(Closing_DATE,'99999999')>=? order by SortNo ASC"; // 20121005
																															// Bug:
																															// 024
																															// MICode
																															// asc->
																															// SortNo
																															// asc
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getHopital19(String FireStationCode) {
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateNow();
			String[] args = { FireStationCode, date, date };
			String sql = "Select distinct a.*,c.MITelNo,c.MIDetailName  from "
					+ "MS_MedicalInstitution  a "
					// +
					// "inner join MS_FireStation b On (a.SHCACode=b.SHCACode and a.AreaCode=b.AreaCode) "
					+ "Inner Join MS_Instructor c On a.MICode=c.MICode "
					+ "where c.FireStationCode=? "
					+ "and a.DeleteFLG=0  and a.StopFLG=4 and c.DeleteFLG=0 "
					+ " and ifnull(a.Opening_DATE,'00010101')<=? and ifnull(a.Closing_DATE,'99999999')>=? order by c.SortNo ASC";// 20121005
																																	// Bug:
																																	// 024
																																	// MICode
																																	// asc->
																																	// c.SortNo
																																	// asc
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getTimeOfHoliday(String MICode, String ConditionClass,
			String ConditionCode, String ConditionBranchCode) {
		String time = "";
		Cursor mCursor = null;
		try {
			String[] args = { MICode, ConditionClass, ConditionCode,
					ConditionBranchCode };
			String sql = "Select * from MS_ConditionSupportTime Where MICode=? and ConditionClass=? and ConditionCode=? and ConditionBranchCode=? and Day_CD='8' and DeleteFLG='0'";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					String SupportStart_TIME = mCursor.getString(mCursor
							.getColumnIndex("SupportStart_TIME"));
					String SupportEnd_TIME = mCursor.getString(mCursor
							.getColumnIndex("SupportEnd_TIME"));
					if (SupportStart_TIME == null && SupportEnd_TIME == null) {
						time = "Ｘ";
					} else if (SupportEnd_TIME.equals(SupportStart_TIME)) {
						time = "○";
					} else if (SupportStart_TIME != null
							&& !SupportStart_TIME.equals("")
							&& SupportEnd_TIME != null
							&& !SupportEnd_TIME.equals("")) {
						time = SupportStart_TIME.substring(0, 2) + ":"
								+ SupportStart_TIME.substring(2, 4) + "-"
								+ SupportEnd_TIME.substring(0, 2) + ":"
								+ SupportEnd_TIME.substring(2, 4);
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return time;
	}

	public HashMap<String, String> getOujuyInfo(String MICode, String KamokuCode) {
		HashMap<String, String> list = new HashMap<String, String>();
		Cursor mCursor = null;
		try {

			String[] args = { MICode, KamokuCode };
			String sql = "Select distinct * from tr_oujyu where MICode=? and KamokuCode=? and DeleteFLG='0'";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					String Oujyu = "";
					if (mCursor.getString(mCursor.getColumnIndex("Oujyu")) != null
							&& !mCursor.getString(
									mCursor.getColumnIndex("Oujyu")).equals("")) {
						if (mCursor.getString(mCursor.getColumnIndex("Oujyu"))
								.equals("1")) {
							Oujyu += "○";
						} else {
							Oujyu += "Ｘ";
						}
					} else {
						Oujyu += "-";
					}
					String Operation = "";
					if (mCursor.getString(mCursor.getColumnIndex("Operation")) != null
							&& !mCursor.getString(
									mCursor.getColumnIndex("Operation"))
									.equals("")) {
						if (mCursor.getString(
								mCursor.getColumnIndex("Operation"))
								.equals("1")) {
							Operation += "○";
						} else {
							Operation += "Ｘ";
						}
					} else {
						Operation += "-";
					}
					list.put("Oujyu", Oujyu);
					list.put("Operation", Operation);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return list;
	}

	public boolean deleteTR_FireStationStandardDONTSearch(
			String FireStationCode, String CaseNo, String DiseaseFLG,
			String ExternalWoundFLG) {
		boolean result = true;
		try {

			String[] args = { FireStationCode, CaseNo, DiseaseFLG,
					ExternalWoundFLG };
			String sql = "Delete From TR_FireStationStandard"
					+ " WHERE TR_FireStationStandard.FireStationCode=? and TR_FireStationStandard.CaseNo=? and   TR_FireStationStandard.standardclass||TR_FireStationStandard.standardcode ||TR_FireStationStandard.standardbranchcode "
					+ " In (Select b.standardclass||b.standardcode||b.standardbranchcode from "
					+ "  MS_FireStationStandard b  where b.diseaseflg=? and b.externalwoundflg=?)";
			database.execSQL(sql, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result = false;
		} finally {
			database.close();
		}
		return result;
	}

	public void close() {
		dbHelper.close();
	}

}
