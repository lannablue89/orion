package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_FireStationOriginalMI {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_FireStationOriginalMI(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public Cursor getMS_FireStationOriginalMI(String FirestationCode,
			String HopitalName, String rdiSearch) {
		Cursor mCursor = null;
		try {
			String[] args = { FirestationCode };
			String sql = "Select * from MS_FireStationOriginalMI e";
			sql += " Where DeleteFLG='0' and FirestationCode=?";
			// if (rdiSearch.equals("2") && HopitalName != null
			// && !HopitalName.trim().equals("")) {
			// sql += " and (e.MIName_Kanji Like '%"
			// + HopitalName.replace("'", "''")
			// + "%' or e.MIName_Kana Like '%"
			// + HopitalName.replace("'", "''") + "%') ";
			// }
			sql+=" Order By MIName_Kanji asc, MICode asc, SortNo asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getMS_FireStationOriginalMIDetail(String FirestationCode, String MICode) {
		Cursor mCursor = null;
		try {
			String[] args = { FirestationCode, MICode };
			String sql = "Select * from MS_FireStationOriginalMI ";
			sql += " Where DeleteFLG='0' and FirestationCode=? And MICode=?";

			sql += " Order By  SortNo asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getMS_FireStationOriginalMIGID014(String FirestationCode,String MICode) {
		Cursor mCursor = null;
		try {
			String[] args = { FirestationCode, MICode };
			String sql = "Select * from MS_FireStationOriginalMI ";
			sql += " Where DeleteFLG='0' and FirestationCode=? And MICode=?";

			sql += " Order By  SortNo asc limit 4";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
