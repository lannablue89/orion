package jp.osaka;

import java.util.HashMap;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class ModelE036 implements IDataChange {

	private String title;
	private HashMap<String, Integer> data;
	int subTag1, subTag2;
	private int choosenValue;
	private int mode;
	private View view;
	private int tag;
	private boolean isexpanable;

	public ModelE036(String title, HashMap<String, Integer> data,
			int choosenValue, int tag, int subTag1, int subTag2) {
		super();
		E036.listeners.add(this);
		this.title = title;
		this.data = data;
		this.choosenValue = choosenValue;
		this.tag = tag;
		this.subTag1 = subTag1;
		this.subTag2 = subTag2;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public HashMap<String, Integer> getData() {
		return data;
	}

	public void setData(HashMap<String, Integer> data) {
		this.data = data;
	}

	public int getChoosenValue() {
		return choosenValue;
	}

	public void setChoosenValue(int choosenValue) {
		this.choosenValue = choosenValue;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
		initView();
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	@Override
	public boolean equals(Object o) {
		String tag = (String) o;
		return (tag.equals(this.tag));
	}

	public void initView() {
		TextView tvTitle = (TextView) view.findViewById(R.id.title);
		tvTitle.setText(title);

		tvTitle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (view.findViewById(R.id.body).getVisibility() == View.GONE) {
					expand();
				} else {
					collapse();
				}
			}
		});

		if (data == null) {
			return;
		}
		for (String subdata : data.keySet()) {
			if (data.get(subdata) == -1) {
				TextView subtitle = (TextView) view.findViewById(R.id.subTitle);
				subtitle.setText(subdata);
				subtitle.setVisibility(View.VISIBLE);
			}
			if (data.get(subdata) == 1) {
				RadioButton radio = (RadioButton) view
						.findViewById(R.id.radioVer1);
				radio.setText(subdata);
				if (choosenValue == 1) {
					radio.setChecked(true);
				}
			} else {
				RadioButton radio = (RadioButton) view
						.findViewById(R.id.radioVer2);
				radio.setText(subdata);
				if (choosenValue == 0) {
					radio.setChecked(true);
				}
			}
			RadioGroup group = (RadioGroup) view.findViewById(R.id.radio_group);
			group.setOrientation(mode);
		}

		RadioGroup group = (RadioGroup) view.findViewById(R.id.radio_group);
		group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.radioVer1:
					choosenValue = 1;
					E036.iOnRadioChecked.onItencheck(checkedId, subTag1);
					break;
				case R.id.radioVer2:
					choosenValue = 0;
					E036.iOnRadioChecked.onItencheck(checkedId, subTag2);
					break;
				default:
					break;
				}
			}
		});

	}

	@Override
	public void OnDataChange(int tag, boolean explan) {
		if (tag == this.tag) {
			if (explan) {
				expand();
			} else {
				collapse();
			}
		}
		if(explan && tag==subTag1){
			RadioButton radio = (RadioButton) view
					.findViewById(R.id.radioVer1);
			radio.setChecked(true);
		}
		
		if(explan && tag==subTag2){
			RadioButton radio = (RadioButton) view
					.findViewById(R.id.radioVer2);
			radio.setChecked(true);
		}
	}

	public void collapse() {
		view.findViewById(R.id.body).setVisibility(View.GONE);
		view.findViewById(R.id.title).setBackgroundResource(
				R.drawable.btngreydown);
	}

	public void expand() {
		view.findViewById(R.id.body).setVisibility(View.VISIBLE);
		view.findViewById(R.id.title).setBackgroundResource(
				R.drawable.btngreyup);

	}
}
