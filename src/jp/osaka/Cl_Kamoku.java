﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_Kamoku {

	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_Kamoku(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public Cursor getMS_Kamoku() {
		Cursor mCursor = null;
		try {
			String sql = "Select * from MS_Kamoku Where DeleteFLG=0 order by SortNo ASC";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// TR_SelectionKamoku
	public long createTR_SelectionKamoku(String FireStationCode, String CaseNo,
			String KamokuCode, String KamokuName) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("KamokuCode", KamokuCode);
			values.put("KamokuName", KamokuName);			
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			result = database.insert("TR_SelectionKamoku", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteTR_SelectionKamoku(String FireStationCode,
			String CaseNo, String KamokuCode) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and KamokuCode=? ";
			String[] args = { FireStationCode, CaseNo, KamokuCode };
			result = database.delete("TR_SelectionKamoku", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_SelectionKamoku(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_SelectionKamoku where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor get3TR_SelectionKamoku(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select distinct a.* from TR_SelectionKamoku a Inner Join MS_Kamoku b on a.KamokuCode=b.KamokuCode where a.FireStationCode=? and a.CaseNo=? and b.DeleteFLG=0 order by b.SortNo ASC Limit 3";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public void close() {
		dbHelper.close();
	}
}
