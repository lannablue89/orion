﻿package jp.osaka;

import jp.osaka.ContantTable.M_UserCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_User {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_User(Context context) {
		try {
			dbHelper = new DatabaseHelper(context);
			database = dbHelper.getWritableDatabase();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	// M_User
	public long createUser(String login, String pass, String status) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put(M_UserCount.UserName, login);
			values.put(M_UserCount.Password, pass);
			values.put(M_UserCount.Status, status);
			// Date d=new Date();
			// SimpleDateFormat dateFormat = new
			// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// values.put("NgayChoi",dateFormat.format(d));
			result = database.insert(M_UserCount.TableName, null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long createms_emsunitterminalmanagement(String TerminalNo,
			String EMSUnitCode, String TerminalTelNo) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("TerminalNo", TerminalNo);
			values.put("EMSUnitCode", EMSUnitCode);
			values.put("TerminalTelNo", TerminalTelNo);
			values.put("Start_DATETIME", "00000000000000");
			values.put("End_DATETIME", "99999999999999");
			values.put("DeleteFLG", "0");
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			result = database.replace("MS_EMSUnitTerminalManagement", null,
					values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public void createMS_EMSUnitTerminalManagement(ContentValues values) {
		database.insert("MS_EMSUnitTerminalManagement", null, values);
		database.close();
	}

	public void DeleteMS_EMSUnitTerminalManagement() {
		database.delete("MS_EMSUnitTerminalManagement", null, null);
		database.close();
	}

	public void createUser(ContentValues values) {
		database.insert(M_UserCount.TableName, null, values);
		database.close();
	}

	public Cursor fetchAllUSer(String user, String pass) {
		Cursor mCursor = null;
		try {
			String[] cols = new String[] { M_UserCount.Id,
					M_UserCount.UserName, M_UserCount.Password,
					M_UserCount.Status };
			String where = "UserName=? and Password=?";
			String[] args = { user, pass };
			mCursor = database.query(true, M_UserCount.TableName, cols, where,
					args, null, null, null, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor fetchLastUSer() {
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database.rawQuery(
					"Select * from M_User  order by Id DESC  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	// Bug 140
	public String getUserName() {
		String user = "";
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database.rawQuery(
					"Select * from M_User  order by Id DESC  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					user = mCursor
							.getString(mCursor.getColumnIndex("UserName"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return user;
	}

	// Bug 170
	public String getFirestationCode() {
		String FireStationCode = "0";
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database
					.rawQuery(
							"Select b.FireStationCode from M_User a inner join MS_EMSUnit b on a.UserName=b.EMSUnitCode  order by a.Id DESC  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FireStationCode")) != null) {
					FireStationCode = mCursor.getString(mCursor
							.getColumnIndex("FireStationCode"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return FireStationCode;
	}

	public String getEMSUnitName() {
		String FireStationName = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select b.EMSUnitName from M_User a inner join MS_EMSUnit b on a.UserName=b.EMSUnitCode  "
									+ "order by a.Id DESC  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("EMSUnitName")) != null) {
					FireStationName = mCursor.getString(mCursor
							.getColumnIndex("EMSUnitName"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return FireStationName;
	}

	public String getFireStationName() {
		String FireStationName = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select b.FireStationName from MS_EMSUnitTerminalManagement a inner join ms_emsunit c on a.EMSUnitCode=c.EMSUnitCode inner join MS_FireStation b on c.FireStationCode=b.FireStationCode  "
									+ "  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FireStationName")) != null) {
					FireStationName = mCursor.getString(mCursor
							.getColumnIndex("FireStationName"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return FireStationName;
	}

	public String getFirestationJianoNo_Init(String FirestationCode) {
		String JianoNo_Init = "";
		Cursor mCursor = null;
		try {
			String[] args = { FirestationCode };
			mCursor = database.rawQuery(
					"Select * from MS_FireStation Where FireStationCode=? ",
					args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("JianNo_Initial")) != null) {
					JianoNo_Init = mCursor.getString(mCursor
							.getColumnIndex("JianNo_Initial"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return JianoNo_Init;
	}

	public String getUserJianoNo_Init() {
		String JianoNo_Init = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("JianNo_Initial")) != null) {
					JianoNo_Init = mCursor.getString(mCursor
							.getColumnIndex("JianNo_Initial"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return JianoNo_Init;
	}

	public String getAllowPlaySoundOnDisaster() {
		String value = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("PlaySoundOnDisaster")) != null) {
					value = mCursor.getString(mCursor
							.getColumnIndex("PlaySoundOnDisaster"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public String getAllowPlaySoundOnNotification() {
		String value = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("PlaySoundOnNotification")) != null) {
					value = mCursor.getString(mCursor
							.getColumnIndex("PlaySoundOnNotification"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;
	}

	public Cursor getUserJianoNo_InitFLG() {
		// String JianoNo_Init = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				// if (mCursor.getCount() > 0
				// && mCursor.getString(mCursor
				// .getColumnIndex("JianNo_InitialFLG")) != null) {
				// JianoNo_Init = mCursor.getString(mCursor
				// .getColumnIndex("JianNo_InitialFLG"));
				// }

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getEMSUnitTerminalManagement() {
		// String JianoNo_Init = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getJianoNo_InitFLG() {
		String JianNo_InitialFLG = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from MS_EMSUnitTerminalManagement  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("JianNo_InitialFLG")) != null) {
					JianNo_InitialFLG = mCursor.getString(mCursor
							.getColumnIndex("JianNo_InitialFLG"));
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return JianNo_InitialFLG;
	}

	public boolean Logout() {
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database.rawQuery(
					"Select * from M_User  order by Id DESC  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					String id = mCursor.getString(mCursor
							.getColumnIndex(M_UserCount.Id));
					ContentValues values = new ContentValues();
					values.put(M_UserCount.Status, "0");
					database.update(M_UserCount.TableName, values,
							M_UserCount.Id + "=?", new String[] { id });
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		} finally {
			database.close();
		}
		return true;
	}

	public int updateEMSUnitTerminalManagement(ContentValues values) {
		int result = 0;
		try {
			// String where = "";
			result = database.update("MS_EMSUnitTerminalManagement", values,
					null, null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int updateUser(String id, String user, String pass, String staus) {
		int reuslt = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(M_UserCount.UserName, user);
			values.put(M_UserCount.Password, pass);
			values.put(M_UserCount.Status, staus);
			reuslt = database.update(M_UserCount.TableName, values,
					M_UserCount.Id + "=?", new String[] { id });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return reuslt;
	}

	public boolean delete(String id) {
		boolean result = false;
		try {
			result = database.delete(M_UserCount.TableName, M_UserCount.Id
					+ "=" + id, null) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteAll() {
		boolean result = false;
		try {
			result = database.delete(M_UserCount.TableName, null, null) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public void close() {
		dbHelper.close();
	}
}
