﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class E010 extends Activity implements OnTouchListener {

	String FireStationCode;
	String CaseCode;
	String SHCACode;
	String KamokuName = "";
	HashMap<String, String> listmap;
	int Id = 1;
	EditText txtNote;
	Button btnNext;
	String codeIdold = "";

	Dialog dialoge;
	adapterActivity adapter1;
	adapterActivity adapter2;
	Spinner cbo1;
	Spinner cbo2;
	RadioButton rdi1;
	RadioButton rdi2;
	RadioButton rdi3;
	EditText txtHopital;
	String rdoSearch = "1";
	String hopitalname;
	String SHCASearch;
	String AreaCode;
	double lat = 0;
	double longi = 0;
	boolean isruning = false;
	boolean isShow12 = false;
	Handler handler;
	LocationManager locationManager;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		v.requestFocusFromTouch();
		@SuppressWarnings("static-access")
		InputMethodManager inputManager = (InputMethodManager) getSystemService(E010.this.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(E010.this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E010.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute010);
		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		SHCACode = b.getString("SHCACode");
		// Slide menu
		utl.setMenu_open(false);
		initSileMenu();
		//
		handler = new Handler();
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Cursor cur = null;
		Cursor cor = null;
		try {
			Button btnE10 = (Button) findViewById(R.id.btnE10Radio);
			btnE10.setOnTouchListener(this);

			txtNote = (EditText) findViewById(R.id.txtE10Note);
			txtNote.setOnTouchListener(this);
			// createMenu();// menu
			// getConfig();
			// Testmenu
			// TextView txtHeader=(TextView)findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			// Search olddata of Casecode
			CL_TRCase tr = new CL_TRCase(this);
			cor = tr.getItemTRcase(FireStationCode, CaseCode);
			if (cor != null && cor.getCount() > 0) {
				codeIdold = cor.getString(cor
						.getColumnIndex(TR_CaseCount.PatientLevel_CD));
				String note = cor.getString(cor
						.getColumnIndex(TR_CaseCount.Notice_TEXT));
				txtNote.setText(note);
			}
			Cl_EMSUnit em = new Cl_EMSUnit(E010.this);
			cur = em.getSHCACode();
			if (cur != null && cur.getCount() > 0) {
				SHCACode = cur.getString(cur.getColumnIndex("SHCACode"));
			}

			// New Object

			String DiseaseFLG = "0";
			String ExternalWoundFLG = "0";

			LinearLayout layout = (LinearLayout) findViewById(R.id.LayoutE10Expand);
			layout.setOnTouchListener(this);
			this.loadControlParent(DiseaseFLG, ExternalWoundFLG, null, null,
					null, layout, 0, 0);
			this.loadGroupRadio();

			btnNext = (Button) findViewById(R.id.btnE10Next);
			btnNext.setOnTouchListener(this);
			btnNext.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Cl_FireStationStandard cl = new Cl_FireStationStandard(
							E010.this);
					boolean check = cl.checkExistTR_FireStationStandard(
							FireStationCode, CaseCode);
					if (check) {
						if (!isShow12) {
							isShow12 = true;
							showE012();
						}
					} else {
						E022.showErrorDialog(E010.this,
								ContantMessages.NotCheck,
								ContantMessages.NotCheck, false);
					}
				}
			});
			txtNote.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					CL_TRCase tr = new CL_TRCase(E010.this);
					tr.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.Notice_TEXT, txtNote.getText()
									.toString());
				}
			});
			ScrollView view = (ScrollView) findViewById(R.id.scrollViewe10);
			view.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
			view.setFocusable(true);
			view.setFocusableInTouchMode(true);
			view.setOnTouchListener(this);

			CheckBox chk12 = (CheckBox) findViewById(R.id.chkTemp);
			chk12.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					// TODO Auto-generated method stub
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e = sh.edit();
					e.putBoolean("CHK12", arg1);
					e.commit();
				}
			});
			SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
			boolean chk = sh.getBoolean("CHK12", false);
			chk12.setChecked(chk);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E010", true);
		} finally {
			cur.close();
			cor.close();
		}
	}

	private void getKamokuName() {
		Cursor cur = null;
		try {
			KamokuName = "";
			Cl_FireStationStandard cl = new Cl_FireStationStandard(E010.this);
			cur = cl.get3TR_FireStationStandard(FireStationCode, CaseCode, "0",
					"0", SHCACode);
			if (cur != null && cur.getCount() > 0) {
				do {

					String name = cur.getString(cur
							.getColumnIndex("StandardBranchName"));
					if (KamokuName.equals(""))
						KamokuName += name;
					else
						KamokuName += "," + name;
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E010.this);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
	}

	// Bug 187,188
	public void showDialogERR(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myinten = new Intent(E010.this, E013A.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putString("SHCACode", SHCACode);
				b.putString("rdoSearch", "4");
				b.putString("hopitalname", hopitalname);
				b.putString("SHCASearch", SHCASearch);
				b.putString("AreaCode", AreaCode);
				b.putString("E", "E012");
				b.putString("KamokuName", KamokuName);
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();

			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}

	// Show dialog E12
	public void showE012() {
		try {
			SHCASearch = SHCACode;
			dialoge = new Dialog(E010.this);
			dialoge.setOnKeyListener(new OnKeyListener() {

				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						isShow12 = false;
					}
					return false;
				}
			});
			dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialoge.setContentView(R.layout.layoute012);
			dialoge.setCanceledOnTouchOutside(false);// Bug 146
			// Bug 55 txtHopital
			cbo1 = (Spinner) dialoge.findViewById(R.id.cboE121);
			cbo2 = (Spinner) dialoge.findViewById(R.id.cboE122);

			txtHopital = (EditText) dialoge.findViewById(R.id.txtE121);

			Button btnCacnel = (Button) dialoge.findViewById(R.id.btnE12Cancel);
			btnCacnel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialoge.cancel();
					isruning = false;
					isShow12 = false;
				}
			});
			Button btnOk = (Button) dialoge.findViewById(R.id.btnE12Ok);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					getKamokuName();
					isShow12 = false;
					isruning = false;
					if (rdoSearch.equals("1")) {

						// Criteria cri = new Criteria();
						// String tower = locationManager.getBestProvider(cri,
						// false);
						// Location location = locationManager
						// .getLastKnownLocation(tower);
						Location location = null;
						CL_TRCase cl = new CL_TRCase(E010.this);
						Cursor cur = cl
								.getItemTRcase(FireStationCode, CaseCode);
						if (cur != null && cur.getCount() > 0) {
							String lat = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Latitude_IS));
							String longi = cur.getString(cur
									.getColumnIndex(TR_CaseCount.Longitude_IS));
							if (lat != null && longi != null) {
								location = new Location("A");
								location.setLatitude(Double.parseDouble(lat));
								location.setLongitude(Double.parseDouble(longi));
							}
						}
						if (cur != null)
							cur.close();
						if (location != null) {
							// lat = (double) (location.getLatitude() * 1E6);
							// longi = (double) (location.getLongitude() * 1E6);
							lat = location.getLatitude();
							longi = location.getLongitude();
							Intent myinten = new Intent(E010.this, E013A.class);
							Bundle b = new Bundle();
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putString("SHCACode", SHCACode);
							b.putString("rdoSearch", rdoSearch);
							b.putString("hopitalname", hopitalname);
							b.putString("SHCASearch", SHCASearch);
							b.putString("AreaCode", AreaCode);
							b.putString("E", "E012");
							b.putDouble("Lat", lat);
							b.putDouble("Longi", longi);
							b.putString("KamokuName", KamokuName);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
							dialoge.cancel();

						} else {
							// Bug 187.188 20121023
							// E022.showErrorDialog(E010.this,
							// ContantMessages.GPSDisiable,
							// ContantMessages.GPSDisiable, false);
							// isruning = true;
							// startThread();
							showDialogERR(ContantMessages.GPSDisiable);
							dialoge.cancel();
							//
						}

					} else {
						EditText txtHN = (EditText) dialoge
								.findViewById(R.id.txtE121);
						hopitalname = txtHN.getText().toString();
						Intent myinten = new Intent(E010.this, E013A.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString("SHCACode", SHCACode);
						b.putString("rdoSearch", rdoSearch);
						b.putString("hopitalname", hopitalname);
						b.putString("SHCASearch", SHCASearch);
						b.putString("AreaCode", AreaCode);
						b.putString("E", "E012");
						b.putString("KamokuName", KamokuName);
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						dialoge.cancel();
					}
				}
			});
			rdi1 = (RadioButton) dialoge.findViewById(R.id.rdioE121);
			rdi1.setButtonDrawable(R.drawable.radio);

			rdi1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {

						rdoSearch = "1";
						rdi1.setTextColor(Color.WHITE);
						rdi2.setTextColor(Color.parseColor("#777777"));
						rdi3.setTextColor(Color.parseColor("#777777"));
						rdi2.setChecked(false);
						rdi3.setChecked(false);
						// Bug 55
						txtHopital.setEnabled(false);
						cbo1.setEnabled(false);
						cbo2.setEnabled(false);

					}
				}
			});

			rdi2 = (RadioButton) dialoge.findViewById(R.id.rdioE122);
			rdi2.setButtonDrawable(R.drawable.radio);
			rdi2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "2";
						rdi2.setTextColor(Color.WHITE);
						rdi1.setTextColor(Color.parseColor("#777777"));
						rdi3.setTextColor(Color.parseColor("#777777"));
						rdi1.setChecked(false);
						rdi3.setChecked(false);
						// Bug 55
						txtHopital.setEnabled(true);
						cbo1.setEnabled(false);
						cbo2.setEnabled(false);
					}
				}
			});

			rdi3 = (RadioButton) dialoge.findViewById(R.id.rdioE123);
			rdi3.setButtonDrawable(R.drawable.radio);
			rdi3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						rdoSearch = "3";
						rdi3.setTextColor(Color.WHITE);
						rdi1.setTextColor(Color.parseColor("#777777"));
						rdi2.setTextColor(Color.parseColor("#777777"));
						rdi1.setChecked(false);
						rdi2.setChecked(false);
						// Bug 55
						txtHopital.setEnabled(false);
						cbo1.setEnabled(true);
						cbo2.setEnabled(true);

					}
				}
			});

			adapter1 = new adapterActivity(dialoge.getContext(),
					R.layout.layoutcomboxe12);
			adapter2 = new adapterActivity(dialoge.getContext(),
					R.layout.layoutcomboxe12);
			cbo1.setAdapter(adapter1);
			cbo1.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapter1.getItem(arg2);
					loadCombobox2(dt.getCodeId());
					SHCASearch = dt.getCodeId();

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			cbo2.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MCode dt = new DTO_MCode();
					dt = adapter2.getItem(arg2);
					AreaCode = dt.getCodeId();

				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			cbo2.setAdapter(adapter2);
			loadCombobox1();
			loadCombobox2(SHCACode);
			// Bug 55
			txtHopital.setEnabled(false);
			cbo1.setEnabled(false);
			cbo2.setEnabled(false);
			rdi1.setChecked(true);
			dialoge.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E012", true);
		} finally {
		}
	}

	// Start thread check GPS
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			while (isruning) {
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						if (isruning)
							checkGPS();
					}
				});

			}
		}
	};

	void checkGPS() {

		Criteria cri = new Criteria();
		String tower = locationManager.getBestProvider(cri, false);
		Location location = locationManager.getLastKnownLocation(tower);

		if (location != null) {
			// lat = (double) (location.getLatitude() * 1E6);
			// longi = (double) (location.getLongitude() * 1E6);
			lat = location.getLatitude();
			longi = location.getLongitude();

			Intent myinten = new Intent(E010.this, E013A.class);
			Bundle b = new Bundle();
			b.putString("FireStationCode", FireStationCode);
			b.putString("CaseCode", CaseCode);
			b.putString("SHCACode", SHCACode);
			b.putString("rdoSearch", rdoSearch);
			b.putString("hopitalname", hopitalname);
			b.putString("SHCASearch", SHCASearch);
			b.putString("AreaCode", AreaCode);
			b.putString("E", "E012");
			b.putDouble("Lat", lat);
			b.putDouble("Longi", longi);
			myinten.putExtras(b);
			startActivityForResult(myinten, 0);
			dialoge.cancel();
			isruning = false;
		}

	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private void loadCombobox1() {
		Cursor cur = null;
		try {
			Cl_Code cl = new Cl_Code(E010.this);
			cur = cl.getMS_SecondaryHealthCareArea();
			if (cur != null && cur.getCount() > 0) {
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("SHCACode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("SHCAName")));
					adapter1.add(code);
				} while (cur.moveToNext());
				for (int i = 0; i < adapter1.getCount(); i++) {
					if (adapter1.getItem(i).getCodeId().equals(SHCACode)) {
						cbo1.setSelection(i);
						break;
					}
				}
			}
			Cl_Code cl2 = new Cl_Code(E010.this);
			cur = cl2.getMS_Area(SHCACode);
			if (cur != null && cur.getCount() > 0) {
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("AreaCode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("AreaName")));
					adapter2.add(code);
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E012", true);
		} finally {
			cur.close();
		}
	}

	private void loadCombobox2(String shcacode) {
		Cursor cur = null;
		try {
			if (adapter2 != null)
				adapter2.clear();
			Cl_Code cl2 = new Cl_Code(E010.this);
			cur = cl2.getMS_Area(shcacode);
			if (cur != null && cur.getCount() > 0) {
				DTO_MCode code1 = new DTO_MCode();
				code1.setCodeId("");
				code1.setCodeName("");
				adapter2.add(code1);
				do {
					DTO_MCode code = new DTO_MCode();
					code.setCodeId(cur.getString(cur.getColumnIndex("AreaCode")));
					code.setCodeName(cur.getString(cur
							.getColumnIndex("AreaName")));
					adapter2.add(code);
				} while (cur.moveToNext());
				// 20121008 Bug 53
				cbo2.setSelection(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E012", true);
		} finally {
			cur.close();
		}
	}

	public class adapterActivity extends ArrayAdapter<DTO_MCode> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, true);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraper mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutcomboxe12, null);
				mwp = new ViewWraper(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraper) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = mwp.getLable();
			DTO_MCode dt = new DTO_MCode();
			dt = this.getItem(position);
			txt.setText("   " + dt.getCodeName());
			LinearLayout layout = mwp.getLayoutrow();
			if (drop) {
				layout.setBackgroundResource(R.drawable.bgrowcboboxe12);
			} else {
				layout.setBackgroundResource(R.drawable.dropdowne12);
			}

			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		LinearLayout layout = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtcboE12);
			}
			return lable1;
		}

		LinearLayout getLayoutrow() {
			if (layout == null) {
				layout = (LinearLayout) base.findViewById(R.id.lrCombox);
			}
			return layout;
		}

	}

	private void loadGroupRadio() {
		Cursor cur = null;
		try {

			Cl_Code code = new Cl_Code(E010.this);
			cur = code.getMS_Code("00004");
			if (cur != null && cur.getCount() > 0) {
				LinearLayout layRadio = (LinearLayout) findViewById(R.id.LayoutE10Radio);
				layRadio.setOnTouchListener(this);
				LinearLayout.LayoutParams paramg = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.MATCH_PARENT);
				paramg.setMargins(20, 10, 20, 10);
				RadioGroup group = new RadioGroup(this);
				group.setLayoutParams(paramg);
				group.setOrientation(RadioGroup.VERTICAL);
				do {

					final String codeId = cur.getString(cur
							.getColumnIndex("CodeID"));
					String codeName = cur.getString(cur
							.getColumnIndex("CodeName"));
					// bug71
					// LinearLayout.LayoutParams paramItem = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.MATCH_PARENT, 90);
					LinearLayout.LayoutParams paramItem = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, 120);
					paramItem.setMargins(0, 15, 0, 15);
					final RadioButton rdio = new RadioButton(this);
					rdio.setText("   " + codeName);
					rdio.setButtonDrawable(R.drawable.radiospace);
					rdio.setTextColor(Color.BLACK);
					rdio.setLayoutParams(paramItem);
					rdio.setBackgroundResource(R.drawable.bgrdiowhite);
					rdio.setOnTouchListener(this);
					group.addView(rdio);
					rdio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								CL_TRCase tr = new CL_TRCase(E010.this);
								tr.updateColumsTRCase(FireStationCode,
										CaseCode, TR_CaseCount.PatientLevel_CD,
										codeId);
								rdio.setBackgroundResource(R.drawable.bgrdiogreen);
								rdio.setTextColor(Color.WHITE);
							} else {
								rdio.setBackgroundResource(R.drawable.bgrdiowhite);
								rdio.setTextColor(Color.BLACK);
							}
						}
					});
					if (codeIdold != null && codeIdold.equals(codeId)) {
						rdio.setChecked(true);
					}

				} while (cur.moveToNext());
				layRadio.addView(group);
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E010", true);
		} finally {
			cur.close();
		}
	}

	private void loadControlParent(final String DiseaseFLG,
			final String ExternalWoundFLG, String ParentStandardClass,
			String ParentStandardCode, String ParentStandardBranchCode,
			final LinearLayout layout, final int left, final int right) {
		Cursor cor2 = null;
		Cursor cor = null;

		Cl_FireStationStandard cl = new Cl_FireStationStandard(E010.this);

		if (ParentStandardClass == null)
			cor = cl.getMS_FireStationStandard(DiseaseFLG, ExternalWoundFLG,
					SHCACode);
		else
			cor = cl.getMS_FireStationStandardChild(DiseaseFLG,
					ExternalWoundFLG, SHCACode, ParentStandardClass,
					ParentStandardCode, ParentStandardBranchCode);
		try {
			if (cor != null && cor.getCount() > 0) {
				// get Old data set status checkbox
				listmap = new HashMap<String, String>();
				Cl_FireStationStandard fire = new Cl_FireStationStandard(this);
				Cursor corold = fire.getTR_FireStationStandard(FireStationCode,
						CaseCode, DiseaseFLG, ExternalWoundFLG, SHCACode);
				if (corold != null && corold.getCount() > 0) {

					do {

						String StandardClass = corold.getString(corold
								.getColumnIndex("StandardClass"));
						String StandardCode = corold.getString(corold
								.getColumnIndex("StandardCode"));
						String StandardBranchCode = corold.getString(corold
								.getColumnIndex("StandardBranchCode"));

						String key = StandardClass + "," + StandardCode + ","
								+ StandardBranchCode;
						listmap.put(key, key);

					} while (corold.moveToNext());
				}
				corold.close();
				do {

					// Thread.sleep(100);
					String StandardClass = cor.getString(cor
							.getColumnIndex("StandardClass"));
					String StandardCode = cor.getString(cor
							.getColumnIndex("StandardCode"));
					String StandardBranchCode = cor.getString(cor
							.getColumnIndex("StandardBranchCode"));
					String StandardBranchName = cor.getString(cor
							.getColumnIndex("StandardBranchName"));
					final String key = StandardClass + "," + StandardCode + ","
							+ StandardBranchCode;
					final DTO_FireStationStandard dt = new DTO_FireStationStandard();
					dt.StandardClass = StandardClass;
					dt.StandardCode = StandardCode;
					dt.StandardBranchCode = StandardBranchCode;
					dt.StandardBranchName = StandardBranchName;
					dt.Checked = "0";
					dt.PId = String.valueOf(Id);

					// list.add(dt);
					Cl_FireStationStandard cl2 = new Cl_FireStationStandard(
							E010.this);
					cor2 = cl2.getMS_FireStationStandardChild(DiseaseFLG,
							ExternalWoundFLG, SHCACode, StandardClass,
							StandardCode, StandardBranchCode);
					if (cor2 != null && cor2.getCount() > 0) {
						LinearLayout.LayoutParams paramlayout = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paramlayout.setMargins(0, 15, 0, 15);
						LinearLayout lparent = new LinearLayout(E010.this);
						lparent.setOrientation(LinearLayout.VERTICAL);
						lparent.setLayoutParams(paramlayout);
						lparent.setBackgroundResource(R.drawable.border);

						// LinearLayout.LayoutParams paramChh = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT, 90);
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, 100);
						paramChh.setMargins(6, 6, 6, 0);
						final TextView btn = new TextView(E010.this);
						btn.setText("   " + StandardBranchName);
						btn.setId(Id);
						// btn.setHeight(78);
						btn.setHeight(100);
						btn.setTextSize(24);
						btn.setBackgroundResource(R.drawable.btngreydown);
						btn.setLayoutParams(paramChh);
						btn.setVisibility(View.VISIBLE);
						btn.setGravity(Gravity.CENTER | Gravity.LEFT);
						btn.setTextColor(Color.WHITE);
						btn.setOnTouchListener(this);
						// layout.addView(btn);

						LinearLayout.LayoutParams paraml = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paraml.setMargins(6, 6, 6, 0);
						final LinearLayout l = new LinearLayout(E010.this);
						l.setOrientation(LinearLayout.VERTICAL);
						l.setLayoutParams(paraml);
						lparent.addView(btn);
						lparent.addView(l);
						layout.addView(lparent);
						btn.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								// TODO Auto-generated method stub
								if (l.getChildCount() == 0) {

									loadControl(DiseaseFLG, ExternalWoundFLG,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode, l, left + 5,
											right + 5);
									btn.setBackgroundResource(R.drawable.btngreyup);
								} else {
									btn.setBackgroundResource(R.drawable.btngreydown);
									l.removeAllViews();
								}
							}
						});

					} else {

						LinearLayout.LayoutParams paramlayout = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paramlayout.setMargins(0, 15, 0, 15);
						LinearLayout lparent = new LinearLayout(E010.this);
						lparent.setOrientation(LinearLayout.VERTICAL);
						lparent.setLayoutParams(paramlayout);
						lparent.setBackgroundResource(R.drawable.border);

						// Bug 86
						// LinearLayout.LayoutParams paramChh = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT,
						// LinearLayout.LayoutParams.WRAP_CONTENT);
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paramChh.setMargins(left + 10, 10, 0, 10);
						//

						final CheckBox chk = new CheckBox(E010.this);
						chk.setLayoutParams(paramChh);
						chk.setGravity(Gravity.CENTER | Gravity.LEFT);
						chk.setTextColor(Color.BLACK);
						chk.setId(Id);
						chk.setOnTouchListener(this);
						chk.setText(StandardBranchName);
						chk.setTextSize(18);
						chk.setButtonDrawable(this.getResources().getDrawable(
								R.drawable.checkbox));
						if (listmap != null && listmap.size() > 0
								&& listmap.containsKey(key)) {
							chk.setChecked(true);
						}
						// chk.setBackgroundResource(R.drawable.chke9);
						chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							public void onCheckedChanged(
									CompoundButton buttonView, boolean isChecked) {
								// TODO Auto-generated method stub
								Cl_FireStationStandard fs = new Cl_FireStationStandard(
										E010.this);
								if (isChecked) {

									fs.createTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "1";
									if (!listmap.containsKey(key)) {
										listmap.put(key, key);
									}
								} else {
									fs.deleteTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "0";
									if (listmap.containsKey(key)) {
										listmap.remove(key);
									}
								}
							}
						});
						// layout.addView(chk);
						lparent.addView(chk);
						layout.addView(lparent);
					}
					Id++;

				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E010", true);
		} finally {
			if (cor2 != null)
				cor2.close();
			if (cor != null)
				cor.close();
		}
	}

	private void loadControl(final String DiseaseFLG,
			final String ExternalWoundFLG, String ParentStandardClass,
			String ParentStandardCode, String ParentStandardBranchCode,
			final LinearLayout layout, final int left, final int right) {
		Cursor cor2 = null;
		Cursor cor = null;

		Cl_FireStationStandard cl = new Cl_FireStationStandard(E010.this);

		if (ParentStandardClass == null)
			cor = cl.getMS_FireStationStandard(DiseaseFLG, ExternalWoundFLG,
					SHCACode);
		else
			cor = cl.getMS_FireStationStandardChild(DiseaseFLG,
					ExternalWoundFLG, SHCACode, ParentStandardClass,
					ParentStandardCode, ParentStandardBranchCode);
		try {
			if (cor != null && cor.getCount() > 0) {
				// get Old data set status checkbox
				listmap = new HashMap<String, String>();
				Cl_FireStationStandard fire = new Cl_FireStationStandard(this);
				Cursor corold = fire.getTR_FireStationStandard(FireStationCode,
						CaseCode, DiseaseFLG, ExternalWoundFLG, SHCACode);
				if (corold != null && corold.getCount() > 0) {

					do {

						String StandardClass = corold.getString(corold
								.getColumnIndex("StandardClass"));
						String StandardCode = corold.getString(corold
								.getColumnIndex("StandardCode"));
						String StandardBranchCode = corold.getString(corold
								.getColumnIndex("StandardBranchCode"));

						String key = StandardClass + "," + StandardCode + ","
								+ StandardBranchCode;
						listmap.put(key, key);

					} while (corold.moveToNext());
				}
				corold.close();
				do {

					// Thread.sleep(100);
					String StandardClass = cor.getString(cor
							.getColumnIndex("StandardClass"));
					String StandardCode = cor.getString(cor
							.getColumnIndex("StandardCode"));
					String StandardBranchCode = cor.getString(cor
							.getColumnIndex("StandardBranchCode"));
					String StandardBranchName = cor.getString(cor
							.getColumnIndex("StandardBranchName"));
					final String key = StandardClass + "," + StandardCode + ","
							+ StandardBranchCode;
					final DTO_FireStationStandard dt = new DTO_FireStationStandard();
					dt.StandardClass = StandardClass;
					dt.StandardCode = StandardCode;
					dt.StandardBranchCode = StandardBranchCode;
					dt.StandardBranchName = StandardBranchName;
					dt.Checked = "0";
					dt.PId = String.valueOf(Id);

					// list.add(dt);
					Cl_FireStationStandard cl2 = new Cl_FireStationStandard(
							E010.this);
					cor2 = cl2.getMS_FireStationStandardChild(DiseaseFLG,
							ExternalWoundFLG, SHCACode, StandardClass,
							StandardCode, StandardBranchCode);
					if (cor2 != null && cor2.getCount() > 0) {
						// LinearLayout.LayoutParams paramChh = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT, 78);
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, 100);
						paramChh.setMargins(left + 10, 5, 0, 10);
						final TextView btn = new TextView(E010.this);
						btn.setText("   " + StandardBranchName);
						btn.setId(Id);
						btn.setOnTouchListener(this);
						// btn.setHeight(78);
						btn.setHeight(100);
						btn.setTextSize(24);
						btn.setBackgroundResource(R.drawable.btngreydown);
						btn.setLayoutParams(paramChh);
						btn.setVisibility(View.VISIBLE);
						btn.setGravity(Gravity.CENTER | Gravity.LEFT);
						btn.setTextColor(Color.WHITE);
						layout.addView(btn);

						LinearLayout.LayoutParams paraml = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paraml.setMargins(left + 10, 5, 0, 10);
						final LinearLayout l = new LinearLayout(E010.this);
						l.setOrientation(LinearLayout.VERTICAL);
						l.setLayoutParams(paraml);
						layout.addView(l);
						btn.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								// TODO Auto-generated method stub
								if (l.getChildCount() == 0) {

									loadControl(DiseaseFLG, ExternalWoundFLG,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode, l, left + 5,
											right + 5);
									btn.setBackgroundResource(R.drawable.btngreyup);
								} else {
									btn.setBackgroundResource(R.drawable.btngreydown);
									l.removeAllViews();
								}
							}
						});

					} else {
						// Bug 86 20121009
						// LinearLayout.LayoutParams paramChh = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT,
						// LinearLayout.LayoutParams.WRAP_CONTENT);
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						//
						paramChh.setMargins(left + 10, 5, 0, 10);

						final CheckBox chk = new CheckBox(E010.this);
						chk.setLayoutParams(paramChh);
						chk.setGravity(Gravity.CENTER | Gravity.LEFT);
						chk.setTextColor(Color.BLACK);
						chk.setId(Id);
						chk.setOnTouchListener(this);
						chk.setText(StandardBranchName);
						chk.setTextSize(18);
						chk.setButtonDrawable(this.getResources().getDrawable(
								R.drawable.checkbox));
						if (listmap != null && listmap.size() > 0
								&& listmap.containsKey(key)) {
							chk.setChecked(true);
						}
						// chk.setBackgroundResource(R.drawable.chke9);
						chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							public void onCheckedChanged(
									CompoundButton buttonView, boolean isChecked) {
								// TODO Auto-generated method stub
								Cl_FireStationStandard fs = new Cl_FireStationStandard(
										E010.this);
								if (isChecked) {

									fs.createTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "1";
									if (!listmap.containsKey(key)) {
										listmap.put(key, key);
									}
								} else {
									fs.deleteTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "0";
									if (listmap.containsKey(key)) {
										listmap.remove(key);
									}
								}
							}
						});
						layout.addView(chk);
					}
					Id++;

				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E010.this, e.getMessage(), "E010", true);
		} finally {
			if (cor2 != null)
				cor2.close();
			if (cor != null)
				cor.close();
		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
