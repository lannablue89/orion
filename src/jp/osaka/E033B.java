﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

public class E033B extends Activity implements OnClickListener, OnTouchListener {
	DTO_NightClinic dt;
	String FireStationCode;
	String CaseCode;
	String MICode = "99999";
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	String E = "";// Bug 339 20121114
	String KamokuName01 = "", KamokuName02 = "", KamokuName03 = "",
			EMSUnitCode = "";
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E033B.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute033b);

		try {

			// GEt EMSUnitCode
			LinearLayout lbt = (LinearLayout) findViewById(R.id.layoutButton);
			lbt.setOnTouchListener(this);
			CL_User clus = new CL_User(this);
			EMSUnitCode = clus.getUserName();
			// /
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();

			dt = (DTO_NightClinic) b.getSerializable("myobject");
			MICode = dt.MICode;
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			MNETCount = b.getInt("MNETCount");
			ThirdCoordinateCount = b.getInt("ThirdCoordinateCount");
			MNetURL = b.getString("MNetURL");

			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			if (dt != null) {
				TextView txtMiType = (TextView) findViewById(R.id.txtSTT);
				txtMiType.setOnTouchListener(this);
				TextView txtName = (TextView) findViewById(R.id.txtName);
				txtName.setOnTouchListener(this);
				TextView txtaddress = (TextView) findViewById(R.id.txtAddress);
				txtaddress.setOnTouchListener(this);
				if (dt.MIType != null && dt.MIType.equals("5")) {
					txtMiType.setText("");
					txtMiType.setBackgroundResource(R.drawable.headerrow13c);
				} else if (dt.MIType != null && dt.MIType.equals("3")) {
					txtMiType.setText(dt.MIType);
					txtMiType.setBackgroundResource(R.drawable.headerrowred);
				} else if (dt.MIType != null && dt.MIType.equals("2")) {
					txtMiType.setText(dt.MIType);
					txtMiType.setBackgroundResource(R.drawable.headerrowe13);
				} else {
					txtMiType.setVisibility(View.INVISIBLE);
					txtMiType.setText(dt.MIType);
				}
				txtName.setText(dt.MIName_Kanji);
				txtaddress.setText(dt.Address_Kanji);
				Button btnNext = (Button) findViewById(R.id.btnNextE15);
				btnNext.setOnTouchListener(this);
				btnNext.setOnClickListener(this);
				loadPhoneMICode();
			}

			// createMenu();
			ScrollView layoutmain = (ScrollView) findViewById(R.id.laymainE14A);
			layoutmain.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E033B", true);
		}

	}

	private void loadPhoneMICode() {
		Cursor cur = null;
		try {
			CL_FireStationOriginalMI cl = new CL_FireStationOriginalMI(this);
			cur = cl.getMS_FireStationOriginalMIDetail(FireStationCode,
					dt.MICode);
			if (cur != null && cur.getCount() > 0) {
				LinearLayout layout = (LinearLayout) findViewById(R.id.layoutButton);
				layout.setOnTouchListener(this);
				do {
					String Mname = cur.getString(cur
							.getColumnIndex("MIDetailName"));
					final String telno = cur.getString(cur
							.getColumnIndex("TransportationTelNo"));

					LinearLayout.LayoutParams parambtn = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);

					parambtn.setMargins(0, 14, 0, 0);
					Button btn = new Button(this);
					btn.setPadding(40, 0, 0, 0);
					btn.setOnTouchListener(this);
					btn.setLayoutParams(parambtn);
					if (Mname != null && !Mname.equals("null")) {
						btn.setText(Mname + "：" + telno);
					} else {
						btn.setText(telno);
					}
					btn.setBackgroundResource(R.drawable.btncall);
					btn.setTextColor(Color.WHITE);
					btn.setTextSize(24);
					btn.setSingleLine(true);
					btn.setGravity(Gravity.LEFT | Gravity.CENTER);
					btn.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							callPhone(telno);
						}
					});
					layout.addView(btn);

				} while (cur.moveToNext());
			}
		} catch (Exception ex) {
			E022.saveErrorLog(ex.getMessage(), E033B.this);
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btnNextE15:
			E = "E033B";
			showE015();
			break;
		default:
			break;
		}

	}

	private void callPhone(String phonenumber) {
		try {
			phonenumber = phonenumber.replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", "").trim();
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + phonenumber));
			startActivity(callIntent);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E033B", true);
		}
	}

	String codeIdSelect = "";
	int mycountId = 1;
	boolean chk15 = false;
	CheckBox chk;

	private void showE015() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute015);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		TextView textViewcontactcount = (TextView) dialog
				.findViewById(R.id.textViewcontactcount);
		DTO_TRCase dtcase = new DTO_TRCase();
		int ContactCount = 0;
		if (dtcase.getContactCount() == null) {
			Cl_ContactResultRecord cl_new = new Cl_ContactResultRecord(
					E033B.this);
			ContactCount = Integer.parseInt(cl_new.getContactCount(
					FireStationCode, CaseCode));
		} else {
			ContactCount = Integer.parseInt(dtcase.getContactCount());
		}
		textViewcontactcount.setText(ContactCount + 1 + "");
		Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String note = "";
				if (codeIdSelect.equals(BaseCount.Other)) {
					EditText txtother = (EditText) dialog.findViewById(99999);
					note = txtother.getText().toString();
				}
				Cl_ContactResultRecord cl1 = new Cl_ContactResultRecord(
						E033B.this);
				DTO_TRCase dtcase = new DTO_TRCase();
				int ContactCount = 0;
				if (dtcase.getContactCount() == null) {
					ContactCount = Integer.parseInt(cl1.getContactCount(
							FireStationCode, CaseCode));
				} else {
					ContactCount = Integer.parseInt(dtcase.getContactCount());
				}
				Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
						E033B.this);
				// Bug 339 20121114
				// long res = cl.createTR_ContactResultRecord(FireStationCode,
				// CaseCode, ContactCount + 1, MICode, codeIdSelect, note);
				long res = cl.createTR_ContactResultRecord(FireStationCode,
						CaseCode, ContactCount + 1, MICode, codeIdSelect, note,
						E, dt.MIName_Kanji, chk15);
				// end
				if (res >= 0) {
					dtcase.setContactCount(ContactCount + 1 + "");
					if (codeIdSelect.equals("00001")) {
						// E015 up = new E015();
						// JSONObject json = new JSONObject();
						// json.put("FireStationCode", FireStationCode);
						// json.put("CaseCode", CaseCode);
						// json.put("ContactCount", ContactCount + 1);
						// json.put("MICode", MICode);
						// json.put("ContactResult_CD", codeIdSelect);
						// json.put("ContactResult_TEXT", note);
						// json.put("EMSUnitCode", EMSUnitCode);
						// json.put("KamokuName01", KamokuName01);
						// json.put("KamokuName02", KamokuName02);
						// json.put("KamokuName03", KamokuName03);
						// json.put("Movement_DATETIME",
						// Utilities.getDateTimeNow());
						// json.put("Flag", "0");
						// String val = json.toString();
						// up.upLoadDataToServer(val, E033B.this);
						E015_0001 up = new E015_0001();
						up.UpdateLoad(E033B.this, FireStationCode, CaseCode);
					}
					MICode = dt.MICode;
					if (codeIdSelect.equals("00001")) {
						dialog.cancel();
						Intent myinten = new Intent(v.getContext(), E005.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString(BaseCount.IsOldData, "1");
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						finish();
					} else {
						Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
								E033B.this);
						int count = clc.countTR_ContactResultRecord(
								FireStationCode, CaseCode);
						// 20121006: daitran Bug 045
						// if (count >= ThirdCoordinateCount) {
						// showE017();
						// } else
						if (count == MNETCount) {
							dialog.cancel();
							ShowE016();
						} else {
							dialog.cancel();
							finish();
						}
					}
				}
			}
		});
		// dialog.setTitle("不搬送理由");
		// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
		// R.drawable.o);
		Cl_Code code = new Cl_Code(E033B.this);
		Cursor cor = code.getMS_Code("00002");
		if (cor != null && cor.getCount() > 0) {
			mycountId = 1;
			final RadioGroup group = (RadioGroup) dialog
					.findViewById(R.id.radioGroup1);
			LinearLayout layout = (LinearLayout) dialog
					.findViewById(R.id.layoutE73);
			final HashMap<String, String> listItem = new HashMap<String, String>();
			do {
				final String codeId = cor.getString(cor
						.getColumnIndex("CodeID"));
				String codeName = cor.getString(cor.getColumnIndex("CodeName"));
				listItem.put(codeId, codeName);
				LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				paramCheck.setMargins(0, 10, 0, 10);
				RadioButton rdi = new RadioButton(this);
				rdi.setLayoutParams(paramCheck);
				rdi.setText(codeName);
				rdi.setTextColor(Color.BLACK);
				rdi.setButtonDrawable(R.drawable.radio);
				rdi.setTextSize(24);
				rdi.setId(mycountId);
				mycountId++;

				rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							if (!codeId.equals("00001")) {
								RadioButton radio = (RadioButton) dialog
										.findViewById(1);
								radio.setChecked(false);
								chk.setChecked(false);
								chk.setEnabled(false);
							} else {
								chk.setEnabled(true);
								group.clearCheck();
							}
							codeIdSelect = codeId;
							// 20121008 Bug 079
							if (listItem.containsKey(BaseCount.Other)) {
								EditText txtother = (EditText) dialog
										.findViewById(99999);
								txtother.setEnabled(false);
								if (codeIdSelect.equals("99999")) {

									txtother.setEnabled(true);
								} else {
									txtother.setText("");
								}
							}
							//
						}
					}
				});
				// listItem.put(String.valueOf(id), codeId);
				if (codeId.equals("00001")) {
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout lay = new LinearLayout(this);
					lay.setLayoutParams(paramrow);
					lay.setOrientation(LinearLayout.HORIZONTAL);
					chk = new CheckBox(this);
					chk.setLayoutParams(paramrow);
					chk.setTextColor(Color.BLACK);
					chk.setText("三次コーディネート");
					chk.setTextSize(18);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub
							chk15 = arg1;
						}
					});
					lay.addView(rdi);
					lay.addView(chk);
					group.addView(lay);
				} else {
					group.addView(rdi);
				}
				if (codeId.equals("00001")) {
					rdi.setChecked(true);
					codeIdSelect = codeId;
				}
				if (codeId.equals("99999")) {

					LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtext.setMargins(0, 5, 20, 0);
					EditText txtOther = new EditText(this);
					txtOther.setLayoutParams(paramtext);
					txtOther.setBackgroundResource(R.drawable.inpute7);
					txtOther.setTextColor(Color.BLACK);
					txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
							200) });
					txtOther.setSingleLine(true);//
					txtOther.setEnabled(false);//
					txtOther.setId(99999);
					layout.addView(txtOther);
				}
			} while (cor.moveToNext());
			dialog.show();
		}

	}

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}

	private void ShowE016() {
		getConfig();
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				finish();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!MNetURL.startsWith("http://")
						&& !MNetURL.startsWith("https://"))
					MNetURL = "http://" + MNetURL;
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(MNetURL));
				startActivity(browserIntent);
				dialog.cancel();
				CL_TRCase tr = new CL_TRCase(E033B.this);
				tr.updateKin_kbn(FireStationCode, CaseCode, "0");

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.OpenBrowes);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
