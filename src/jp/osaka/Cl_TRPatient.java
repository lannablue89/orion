﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_TRPatient {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_TRPatient(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	// TR_Patient
		public long createTR_Patient(String FireStationCode, String CaseNo,
				String PatientBackground_CD, String PatientBackground_TEXT) {
			long result = -1;
			try {
				ContentValues values = new ContentValues();
				values.put("FireStationCode", FireStationCode);
				values.put("CaseNo", CaseNo);
				values.put("PatientBackground_CD", PatientBackground_CD);
				if (PatientBackground_CD.equals(BaseCount.Other)) {
					values.put("PatientBackground_TEXT", PatientBackground_TEXT);
				}
				values.put("Update_DATETIME", Utilities.getDateNow());
				result = database.insert("TR_Patient", null, values);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				database.close();
			}
			return result;
		}

		public long updateTR_Patient(String FireStationCode, String CaseNo,
				String PatientBackground_CD, String PatientBackground_TEXT) {
			long result = -1;
			try {
				ContentValues values = new ContentValues();
				if(PatientBackground_TEXT!=null&& !PatientBackground_TEXT.equals(""))
				{
				values.put("PatientBackground_TEXT", PatientBackground_TEXT);
				}
				else
				{
					values.putNull("PatientBackground_TEXT");
				}
				values.put("Update_DATETIME", Utilities.getDateNow());
				String where = "FireStationCode=? and CaseNo=? and PatientBackground_CD=?";
				String[] args = { FireStationCode, CaseNo, PatientBackground_CD };

				result = database.update("TR_Patient", values, where, args);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				database.close();
			}
			return result;
		}

		public Cursor getTR_Patient(String FireStationCode, String CaseNo) {
			Cursor mCursor = null;
			try {
				String[] args = { FireStationCode, CaseNo };
				String sql = "Select * from TR_Patient Where FireStationCode=? and CaseNo=? ";
				mCursor = database.rawQuery(sql, args);
				if(mCursor!=null)
				{
					mCursor.moveToFirst();
				}
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			} finally {
				database.close();
			}
			return mCursor;
		}

		public boolean deleteTR_Patient(String FireStationCode, String CaseNo,
				String PatientBackground_CD) {
			boolean result = false;
			try {
				String where = "FireStationCode=? and CaseNo=? and PatientBackground_CD=?";
				String[] args = { FireStationCode, CaseNo, PatientBackground_CD };
				result = database.delete("TR_Patient", where, args) > 0;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				database.close();
			}
			return result;

		}
}
