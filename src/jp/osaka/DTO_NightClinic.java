﻿package jp.osaka;

import java.io.Serializable;

public class DTO_NightClinic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String MICode, MIType, MIName_Kanji, TelNo1,TelNo2,
			FireStationOriginalCode, FireStationCode, Address_Kanji, Latitude,
			Longitude,Dictrict="",Memo;
	public double Distance;
	public int SortNo;
}
