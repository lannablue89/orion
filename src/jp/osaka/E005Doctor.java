package jp.osaka;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import jp.osaka.ContantTable.TR_CaseCount;

import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class E005Doctor {

	Context ac;
	String FireStationCode, CaseCode;

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			int res = b.getInt("Result");

			if (res == 10)// Bug 116
			{
				updateCaseCode();

			} else if (res == 12) {
				showDialogERR(ContantMessages.ExistTR_Case);
			} else {
				E022.saveErrorLog("Send data error when meet doctor", ac);
			}

		}

	};

	public void showDialogERR(String code) {
		try {

			final Dialog dialog = new Dialog(ac);
			String error = "";
			CL_Message m = new CL_Message(ac);
			String meg = m.getMessage(code);
			if (meg != null && !meg.equals(""))
				error = meg;

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layouterror22);
			Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog.cancel();

				}
			});

			TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
			if (error != null && !error.equals(""))
				lbmg.setText(error);
			else
				lbmg.setText("Unknow...");
			TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
			if (code != null && !code.equals("")) {
				lbcode.setText(code);
			}
			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), ac);
		}
	}

	private void updateCaseCode() {
		try {
			CL_TRCase cl = new CL_TRCase(ac);
			int STT = cl.getMaxJANNumber(FireStationCode,
					CaseCode.substring(0, 18));

			int res = 0;
			String newcascode = "";
			do {
				STT++;
				String num = Utilities.format4Number(STT);
				newcascode = CaseCode.substring(0, 18) + num;
				CL_TRCase tr = new CL_TRCase(ac);
				res = tr.changeCase(FireStationCode, CaseCode, newcascode);
			} while (res == 0);

			CaseCode = newcascode;
			// CL_TRCase tr = new CL_TRCase(ac);
			startThread();

		} catch (Exception ex) {
			// exitProccess();
		} finally {

			// dShow = false;
		}
	}

	public void UpdateLoad(Context con, String fireStationCode, String caseCode) {
		ac = con;

		FireStationCode = fireStationCode;
		CaseCode = caseCode;

		// if (isOnline()) {
		startThread();
		// }
	}

	// *************************
	// Send Data
	// **************************
	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			boolean isruning = true;
			// if (isruning)//20121018 check send tr_mA
			while (isruning) {
				if (isOnline()) {
					Cursor cur = null;

					try {

						CL_TRCase tr = new CL_TRCase(ac);
						cur = tr.getItemTRcaseUpdateServer(FireStationCode,
								CaseCode);
						if (cur != null && cur.getCount() > 0 && isOnline()) {
							JSONArray array = new JSONArray();
							JSONObject trcase = ParseToJson(cur, "TR_Case");
							array.put(trcase);

							Cl_Adminssion ad = new Cl_Adminssion(ac);
							cur = ad.getAdmission0002(FireStationCode, CaseCode);
							JSONObject jAdd = ParseToJson(cur,
									"TR_MedicalInstitutionAdmission");
							array.put(jAdd);

							int res = uploadDataToServer(array.toString());

							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putInt("Result", res);
							mg.setData(b);
							handler.sendMessage(mg);

						} else {
							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							// b.putSerializable("DTO", null);
							b.putInt("Result", 0);
							mg.setData(b);
							handler.sendMessage(mg);

						}
					} catch (Exception e) {
						// TODO: handle exception
						// E022.showErrorDialog(E013.this, e.getMessage(),
						// "E013");
						Message mg = handler.obtainMessage();
						Bundle b = new Bundle();
						// b.putSerializable("DTO", null);
						b.putInt("Result", 0);
						mg.setData(b);
						handler.sendMessage(mg);
					} finally {

						if (cur != null)
							cur.close();
						isruning = false;
					}
				} else {
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}
	};

	public JSONObject ParseToJson(Cursor cor, String table) {
		JSONObject ob = new JSONObject();
		JSONArray arr = new JSONArray();
		try {
			if (cor != null && cor.getCount() > 0) {
				do {
					JSONObject json = new JSONObject();
					for (int i = 0; i < cor.getColumnCount(); i++) {
						String colname = cor.getColumnName(i);
						String value = cor.getString(i);

						if (colname.equals("patientstate")
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "00");
							}
						} else if (colname.equals(TR_CaseCount.ContactCount)
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
						} else {
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "");
							}
						}
					}
					arr.put(json);
				} while (cor.moveToNext());
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			E022.saveErrorLog(e1.getMessage(), ac);
		}
		try {
			ob.put(table, arr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return ob;
	}

	// Bug 116 Boolean -> int
	public int uploadDataToServer(String json) {
		// myadapter.clear();

		String encoded = "";
		try {
			encoded = URLEncoder.encode(json, "UTF-8");
			// String de= URLDecoder.decode(encoded, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
		}
		int result = 0;
		TrustManagerManipulator.allowAllSSL();
		String NAMESPACE = ContantSql.NameSpace;
		String METHOD_NAME = ContantSql.selectDoctor;
		String URL = ContantSystem.Endpoint;
		String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		request.addProperty("Json", encoded);
		// Param log 20130103
		CL_User cl = new CL_User(ac);
		String user = cl.getUserName();
		String loginfo = getLogInfo(SOAP_ACTIONS, "OK", user, "確認");
		request.addProperty("smpLog", loginfo);
		//
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		// envelope.dotNet=true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
				ContantSystem.TimeOut);

		try {
			androidhttpTranport.call(SOAP_ACTIONS, envelope);
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			result = 0;
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			result = 0;
		}
		Object responseBody = null;
		try {
			responseBody = envelope.getResponse();
			String t = responseBody.toString();
			if (t.equals("1")) {
				result = 1;
			} else if (t.equals("10")) {
				result = 10;
			} else if (t.equals("12")) {
				result = 12;
			}
		} catch (SoapFault e2) {
			// TODO Auto-generated catch block
			result = 0;
		}

		return result;
	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), ac);
		}
		return log;
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) ac
//					.getApplicationContext().getSystemService(
//							Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Get Ip
	@SuppressLint("DefaultLocale")
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) ac
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), ac);
		}
		return "";
	}

	public void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) ac
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}
