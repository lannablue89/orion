package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_AllReqMedicalInstitution {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_AllReqMedicalInstitution(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createAllReqMedicalInstitution(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("TR_AllReqMedicalInstitution", null,
					values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean CopyAllReqMedicalInstitution(String FireStationCode,
			String CaseNo) {
		boolean result = false;
		try {
			//Mote table result
			String[] args = { FireStationCode, CaseNo };
			String sql = "Insert Into TR_AllReqMedicalInstitution_Result(FireStationCode,CaseNo,MICode,EMSTransmission_Datetime,kin_kbn) Select FireStationCode,CaseNo,MICode,EMSTransmission_Datetime,kin_kbn From TR_AllReqMedicalInstitution Where FireStationCode=? and CaseNo=?";
			database.execSQL(sql, args);

			//Delete old data
			String where = "FireStationCode=? and CaseNo=?  ";
			result = database
					.delete("TR_AllReqMedicalInstitution", where, args) > 0;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteAllReqMedicalInstitution(String FireStationCode,
			String CaseNo, String MICode) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and MICode=? ";
			String[] args = { FireStationCode, CaseNo, MICode };
			result = database
					.delete("TR_AllReqMedicalInstitution", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getAllReqMedicalInstitution(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_AllReqMedicalInstitution Where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getAllReqMedicalInstitution_Result(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_AllReqMedicalInstitution_Result Where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getAllReqMedicalInstitution_Kahi(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_AllReqMedicalInstitution_kahi Where FireStationCode=? and CaseNo=? and ukeire_kahi=1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
