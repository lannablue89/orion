﻿package jp.osaka;

import java.io.Serializable;

public class DTO_ContactResultRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firestationcode;
	private String caseno;
	private int contactcount;
	private String micode;
	private String contact_datetime;
	private String contactresult_cd;
	private String contactresult_name;
	private String contactresult_text;
	private String miname_kanji;
	private String mitype;
	private double distance;
	
	public String getContactresult_name() {
		return contactresult_name;
	}

	public void setContactresult_name(String contactresult_name) {
		this.contactresult_name = contactresult_name;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getMiname_kanji() {
		return miname_kanji;
	}

	public void setMiname_kanji(String miname_kanji) {
		this.miname_kanji = miname_kanji;
	}

	public String getMitype() {
		return mitype;
	}

	public void setMitype(String mitype) {
		this.mitype = mitype;
	}

	public String getFirestationcode() {
		return firestationcode;
	}

	public void setFirestationcode(String firestationcode) {
		this.firestationcode = firestationcode;
	}

	public String getCaseno() {
		return caseno;
	}

	public void setCaseno(String caseno) {
		this.caseno = caseno;
	}

	public int getContactcount() {
		return contactcount;
	}

	public void setContactcount(int contactcount) {
		this.contactcount = contactcount;
	}

	public String getMicode() {
		return micode;
	}

	public void setMicode(String micode) {
		this.micode = micode;
	}

	public String getContact_datetime() {
		return contact_datetime;
	}

	public void setContact_datetime(String contact_datetime) {
		this.contact_datetime = contact_datetime;
	}

	public String getContactresult_cd() {
		return contactresult_cd;
	}

	public void setContactresult_cd(String contactresult_cd) {
		this.contactresult_cd = contactresult_cd;
	}

	public String getContactresult_text() {
		return contactresult_text;
	}

	public void setContactresult_text(String contactresult_text) {
		this.contactresult_text = contactresult_text;
	}

	public String getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(String update_datetime) {
		this.update_datetime = update_datetime;
	}

	private String update_datetime;
}
