package jp.osaka;

public class DTO_FileInfo {
	public String FireStationCode, CaseNo, MultimediaFileNo,
			FileRegistered_DateTime, FileName, ThumbnailFileName, FileSize,
			FileType, FileTime, FileSort, FileComment;
	public boolean bHide = false;
}
