package jp.osaka;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.Overlay;

public class EMaps2 extends FragmentActivity implements OnClickListener {
	// GoogleMap mMap;
	List<Overlay> mapOverlays;
	GeoPoint point1, point2;
	LocationManager locManager;
	Drawable drawable;
	Document document;
	GMapV2GetRouteDirection v2GetRouteDirection;
	LatLng fromPosition;
	LatLng toPosition;
	GoogleMap mGoogleMap;
	MarkerOptions markerOptions;
	Location location;
	ArrayList<Marker> markers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoutmap2);
		try {

			mGoogleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			markers = new ArrayList<Marker>();
			v2GetRouteDirection = new GMapV2GetRouteDirection();
			// LatLng latlngPhamHung = new LatLng(10.803120, 106.738280);
			// mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngPhamHung,
			// 15));
			// 5. draw:

			// MarkerOptions options = new MarkerOptions();
			// options.position(latlngPhamHung);
			// Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
			// R.drawable.pointa);
			// options.icon(BitmapDescriptorFactory.fromBitmap(icon));
			// options.title("Diem Awwwwwwwwwwwwwwwwwwwwwwww");
			// mGoogleMap.addMarker(options);
			// mMap.addMarker(new
			// MarkerOptions().position(latlngPhamHung).title(
			// "Phạm Hùng"));

			// SupportMapFragment supportMapFragment = (SupportMapFragment)
			// getSupportFragmentManager()
			// .findFragmentById(R.id.map);
			// mGoogleMap = supportMapFragment.getMap();

			// Enabling MyLocation in Google Map
			mGoogleMap.setMyLocationEnabled(true);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);
			// mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
			markerOptions = new MarkerOptions();

			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();

			String Flat = b.getString("Flat");
			String Flongi = b.getString("Flongi");
			String Tlat = b.getString("Tlat");
			String Tlongi = b.getString("Tlongi");
			double src_lat = Double.parseDouble(Flat);
			double src_long = Double.parseDouble(Flongi);
			double dest_lat = Double.parseDouble(Tlat);
			double dest_long = Double.parseDouble(Tlongi);

			fromPosition = new LatLng(src_lat, src_long);
			toPosition = new LatLng(dest_lat, dest_long);
			// fromPosition = new LatLng(10.803120, 106.738280);
			// toPosition = new LatLng(10.803120, 106.838280);
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(toPosition,
					15));

			GetRouteTask getRoute = new GetRouteTask();
			getRoute.execute();

			Marker marker = mGoogleMap.addMarker(new MarkerOptions()
					.position(fromPosition));
			markers.add(marker);
			marker = mGoogleMap.addMarker(new MarkerOptions()
					.position(toPosition));
			markers.add(marker);
			if (src_lat != 0 || src_long != 0) {
			zoomToAllMarker();
			}
			// Zoom map to center

			Button btnStreet = (Button) findViewById(R.id.btnStreet);
			btnStreet.setOnClickListener(this);
			Button btnSate = (Button) findViewById(R.id.btnSatellite);
			btnSate.setOnClickListener(this);
			Button btnTraffic = (Button) findViewById(R.id.btnTraffic);
			btnTraffic.setOnClickListener(this);
			Button btnSearch = (Button) findViewById(R.id.btnSearch);
			btnSearch.setOnClickListener(this);
			Button btnView = (Button) findViewById(R.id.btnView);
			btnView.setOnClickListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * 
	 * @author VIJAYAKUMAR M This class Get Route on the map
	 * 
	 */
	private class GetRouteTask extends AsyncTask<String, Void, String> {

		private ProgressDialog Dialog;
		String response = "";

		@Override
		protected void onPreExecute() {
			Dialog = new ProgressDialog(EMaps2.this);
			Dialog.setMessage("Loading route...");
			Dialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			// Get All Route values
			document = v2GetRouteDirection.getDocument(fromPosition,
					toPosition, GMapV2GetRouteDirection.MODE_DRIVING);
			response = "Success";
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			mGoogleMap.clear();
			try {

			if (response.equalsIgnoreCase("Success")) {
				ArrayList<LatLng> directionPoint = v2GetRouteDirection
						.getDirection(document);
				PolylineOptions rectLine = new PolylineOptions().width(10)
						.color(Color.RED);

				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
				}
				// Adding route on the map
				mGoogleMap.addPolyline(rectLine);

				Bitmap icon = BitmapFactory.decodeResource(
						EMaps2.this.getResources(), R.drawable.pointa);
					markerOptions
							.icon(BitmapDescriptorFactory.fromBitmap(icon));
				markerOptions.position(fromPosition);
				markerOptions.title(v2GetRouteDirection
						.getStartAddress(document));
				mGoogleMap.addMarker(markerOptions);

				markerOptions = new MarkerOptions();
					icon = BitmapFactory.decodeResource(
							EMaps2.this.getResources(), R.drawable.pointb);
					markerOptions
							.icon(BitmapDescriptorFactory.fromBitmap(icon));
				markerOptions.position(toPosition);
					markerOptions.title(v2GetRouteDirection
							.getEndAddress(document));
				markerOptions.draggable(true);
				mGoogleMap.addMarker(markerOptions);

			}

			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			Dialog.dismiss();
		}
	}
	}

	void zoomToAllMarker() {
		try {

			LatLngBounds.Builder builder = new LatLngBounds.Builder();

			for (Marker marker : markers) {
				builder.include(marker.getPosition());
			}
			LatLngBounds bounds = builder.build();
			int padding = 0; // offset from edges of the map in pixels
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			int height = metrics.heightPixels;
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
					width, height, padding);

			// mGoogleMap.moveCamera(cu);
			mGoogleMap.animateCamera(cu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// 1. some variables:

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
	}

	private static final double EARTH_RADIUS = 6378100.0;
	private int offset;

	// 2. convert meters to pixels between 2 points in current zoom:

	private int convertMetersToPixels(double lat, double lng,
			double radiusInMeters) {

		double lat1 = radiusInMeters / EARTH_RADIUS;
		double lng1 = radiusInMeters
				/ (EARTH_RADIUS * Math.cos((Math.PI * lat / 180)));

		double lat2 = lat + lat1 * 180 / Math.PI;
		double lng2 = lng + lng1 * 180 / Math.PI;

		Point p1 = mGoogleMap.getProjection().toScreenLocation(
				new LatLng(lat, lng));
		Point p2 = mGoogleMap.getProjection().toScreenLocation(
				new LatLng(lat2, lng2));

		return Math.abs(p1.x - p2.x);
	}

	// 3. bitmap creation:

	private Bitmap getBitmap(double lat, double lng) {

		// fill color
		Paint paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint1.setColor(0x110000FF);
		paint1.setStyle(Style.FILL);

		// stroke color
		Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint2.setColor(0xFF0000FF);
		paint2.setStyle(Style.STROKE);

		// icon
		Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.pointa);

		// circle radius - 200 meters
		int radius = offset = convertMetersToPixels(lat, lng, 200);

		// if zoom too small
		if (radius < icon.getWidth() / 2) {

			radius = icon.getWidth() / 2;
		}

		// create empty bitmap
		Bitmap b = Bitmap
				.createBitmap(radius * 2, radius * 2, Config.ARGB_8888);
		Canvas c = new Canvas(b);

		// draw blue area if area > icon size
		if (radius != icon.getWidth() / 2) {

			c.drawCircle(radius, radius, radius, paint1);
			c.drawCircle(radius, radius, radius, paint2);
		}

		// draw icon
		c.drawBitmap(icon, radius - icon.getWidth() / 2,
				radius - icon.getHeight() / 2, new Paint());

		return b;
	}

	// 4. calculate image offset:

	private LatLng getCoords(double lat, double lng) {

		LatLng latLng = new LatLng(lat, lng);

		Projection proj = mGoogleMap.getProjection();
		Point p = proj.toScreenLocation(latLng);
		p.set(p.x, p.y + offset);

		return proj.fromScreenLocation(p);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnStreet:
			mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			break;
		case R.id.btnSatellite:
			mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			break;
		case R.id.btnTraffic:
			mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			break;
		case R.id.btnView:
			mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			break;
		case R.id.btnSearch:
			finish();
			break;
		default:
			break;
		}
	}

}
