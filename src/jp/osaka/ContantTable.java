﻿package jp.osaka;

public class ContantTable {
	public static class BaseCount {
		public static String SortNo = "SortNo";
		public static String DeleteFLG = "DeleteFLG";
		public static String Insert_DATETIME = "Insert_DATETIME";
		public static String Update_DATETIME = "Update_DATETIME";
		// public static String Nu = "1";
		// public static String Man = "0";
		public static String Nu = "2";
		public static String Man = "1";
		public static String Other = "99999";
		public static String IsOldData = "IsOldData";
		public static String Flag = "Flag";
		public static String StartService = "StartService";
		public static String DateTimeFormat = "yyyyMMddHHmmss";
		public static String DateNowGC="DateNowGC";
	}

	public static class M_UserCount {
		public static String TableName = "M_User";
		public static String Id = "Id";
		public static String UserName = "UserName";
		public static String Password = "Password";
		public static String Status = "Status";
	}

	public static class MS_SecondaryHealthCareAreaCount {
		public static String TableName = "MS_SecondaryHealthCareArea";
		public static String SHCACode = "SHCACode";
		public static String SHCAName = "SHCAName";
		public static String SortNo = "SortNo";
		public static String DeleteFLG = "DeleteFLG";
		public static String Update_DATETIME = "Update_DATETIME";
	}

	public static class MS_AreaCount {
		public static String TableName = "MS_Area";
		public static String AreaCode = "AreaCode";
		public static String AreaName = "AreaName";
		public static String SHCACode = "SHCACode";
		public static String SortNo = "SortNo";
		public static String DeleteFLG = "DeleteFLG";
		public static String Update_DATETIME = "Update_DATETIME";

	}

	public static class MS_SettingCount {
		public static String TableName = "MS_Setting";
		public static String Id = "Id";
		public static String MNETCount = "MNETCount";
		public static String MNETTIME = "MNETTIME";
		public static String ThirdCoordinateCount = "ThirdCoordinateCount";
		public static String ThirdCoordinateTime = "ThirdCoordinateTime";
		public static String DBTime = "DBTime";
		public static String CaseCount = "CaseCount";
		public static String PastCaseCount = "PastCaseCount";
		public static String ResultCount = "ResultCount";
		public static String Update_DATETIME = "Update_DATETIME";
		public static String MNET_URL = "MNET_URL";
		public static String FTP_IP = "FTP_IP";
		public static String ScreenSensitivity = "ScreenSensitivity";
	}

	public static class TR_CaseCount {
		public static String ContactCount = "ContactCount";
		public static String TableName = "TR_Case";
		public static String FireStationCode = "FireStationCode";
		public static String CaseCode = "CaseCode";
		public static String JianNo="JianNo";
		public static String EMSUnitCode = "EMSUnitCode";
		public static String IncidentScene = "IncidentScene";
		public static String Latitude_IS = "Latitude_IS";
		public static String Longitude_IS = "Longitude_IS";
		public static String Sex = "Sex";
		public static String Age = "Age";
		public static String VitalSign_JCS = "VitalSign_JCS";
		public static String VitalSign_GCS_E = "VitalSign_GCS_E";
		public static String VitalSign_GCS_V = "VitalSign_GCS_V";
		public static String VitalSign_GCS_M = "VitalSign_GCS_M";
		public static String VitalSign_Pulse = "VitalSign_Pulse";
		public static String VitalSign_Breath = "VitalSign_Breath";
		public static String VitalSign_BloodPressure = "VitalSign_BloodPressure";
		public static String VitalSign_SpO2 = "VitalSign_SpO2";
		public static String VitalSign_Temperature = "VitalSign_Temperature";
		public static String PatientLevel_CD = "PatientLevel_CD";
		public static String Notice_CD = "Notice_CD";
		public static String Notice_TEXT = "Notice_TEXT";
		public static String NotTransportation_CD = "NotTransportation_CD";
		public static String NotTransportation_TEXT = "NotTransportation_TEXT";
		public static String SearchType_CD = "SearchType_CD";
		public static String BedStatus_MALE = "BedStatus_MALE";
		public static String BedStatus_FEMALE = "BedStatus_FEMALE";
		public static String Status = "Status";
		public static String DeleteFLG = "DeleteFLG";
		public static String Insert_DATETIME = "Insert_DATETIME";
		public static String Update_DATETIME = "Update_DATETIME";

	}

}
