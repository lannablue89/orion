﻿package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_EMSUnit {

	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_EMSUnit(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public Cursor getSHCACode() {
		Cursor mCursor = null;
		try {
			// String[] cols = new String[] { M_UserCount.Id,
			// M_UserCount.UserName,
			// M_UserCount.Password, M_UserCount.Status };
			mCursor = database.rawQuery(
					"Select MS_FireStation.SHCACode from M_User Inner join MS_EMSUnit on MS_EMSUnit.EMSUnitCode=M_User.UserName" +
					" Inner Join MS_FireStation on MS_FireStation.FireStationCode=MS_EMSUnit.FireStationCode  Limit 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}
}
