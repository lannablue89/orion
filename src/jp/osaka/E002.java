﻿package jp.osaka;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;
import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

@SuppressLint({ "HandlerLeak" })
public class E002 extends Activity implements OnTouchListener, LocationListener {

	MyAdapter mydapter;
	ListView list;
	Calendar c = Calendar.getInstance();
	Dialog dialogedit;
	boolean bEdit = false;
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat dfDate = new SimpleDateFormat("yyyy/MM/dd");
	String today;
	boolean dShow = false;
	boolean updateJA = false;
	String InsertDate = "99999999999999";
	String InsertDateFirst = "";
	String Limit = "10";
	String FireStationCode = "";
	String CaseCode = "";
	String JianNo;
	String EMSUnitCode = "";
	DTO_TRCase dt;
	ProgressDialog p;
	Handler handlerlist;
	HashMap<String, String> listSex;
	Button btnDisasster;
	LocationManager locationManager;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");
			int res = b.getInt("Result");
			// showProccess();
			DTO_TRCase dt = new DTO_TRCase();
			dt = (DTO_TRCase) b.getSerializable("DTO");

			if (dt != null) {
				mydapter.remove(dt);
				CL_TRCase cl = new CL_TRCase(E002.this);
				cl.updateStatusTRCase(FireStationCode, CaseCode, "1");
				DTO_TRCase dtcase = new DTO_TRCase();
				dtcase.setContactCount("0");
				// Bug 127
				loadDataforGrid1(InsertDate);
			} else {
				exitProccess();
				if (res == 10)// Bug 116
				{
					updateCaseCode();
					// updateJA = true;
					// showDialogERR(ContantMessages.ExitJiAn);

				} else if (res == 11) {
					updateJA = true;
					showDialogERR(ContantMessages.ExitJiAn);

				} else {
					E022.showErrorDialog(E002.this,
							ContantMessages.UploadNotSucessfull,
							ContantMessages.UploadNotSucessfull, false);
				}
			}
			if (key.equals("1")) {

				mydapter.notifyDataSetChanged();
				exitProccess();
			}
		}

	};
	ThreadSendGPS gps = new ThreadSendGPS();

	public void showDialogERR(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				Editor e = sh.edit();
				e.putString("ThirdCoordinateTime", "");
				e.putString("MNETTIME", "");
				// 20121005: bug 045
				e.putString("ShowMNETTIME", "0");
				e.putString("ShowThirdCoordinateTime", "0");
				// end
				e.commit();

				Intent myinten = new Intent(E002.this, E005.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putString(TR_CaseCount.JianNo, JianNo);
				b.putString(BaseCount.IsOldData, "1");
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();

			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}

	void exitProccess() {
		p.dismiss();
		if (bEdit) {
			bEdit = false;
			dialogedit.cancel();
		}
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E002.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E002.this);
		String mg = m.getMessage(ContantMessages.DataUploading);
		p.setMessage(mg);
		p.setCanceledOnTouchOutside(false);
		p.show();
	}

	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E002.this,
					progressBar, pro_2, "", "");
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	ThreadCheckDisaster thcheck;

	// ThreadCheckNotification thcheckMg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.layoute002);
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		//
		getConfig();
		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		int mode = sh.getInt("Mode", 0);
		if (mode != 0) {
			gps.SendGPS(this, locationManager);
		}
		thcheck = new ThreadCheckDisaster();
		thcheck.checkDisaster(this);

//		Cl_CreateDB cr = new Cl_CreateDB(this);
//		String fire = cr.getFireStationCode(getPhoneNumber());
		// thcheckMg = new ThreadCheckNotification();
		// thcheckMg.checkNotification(this, fire);

		utl.setMenu_open(false);
		initSileMenu();
		Cursor cur = null;
		dShow = false;
		try {
			//
			getListSex();
			E022.ishow = false;
			CL_User us = new CL_User(this);
			String FName = us.getEMSUnitName();
			TextView txtNameF = (TextView) findViewById(R.id.txtNameFirestation);
			// txtNameF.setOnTouchListener(this);
			txtNameF.setText(FName);
			us = new CL_User(this);
			EMSUnitCode = us.getUserName();
			TextView txtUnitcode = (TextView) findViewById(R.id.txtEMSUnitCode);
			// txtUnitcode.setOnTouchListener(this);
			txtUnitcode.setText(EMSUnitCode);
			//
			CL_MSSetting cl = new CL_MSSetting(E002.this);
			cur = cl.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				Limit = cur.getString(cur
						.getColumnIndex(MS_SettingCount.CaseCount));
			}

			CL_TRCase tr = new CL_TRCase(this);
			InsertDateFirst = tr.getTR_CaseFirst("0");

			mydapter = new MyAdapter(this, R.layout.grid002);
			list = (ListView) findViewById(R.id.list002);
			list.setAdapter(mydapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View v, int arg2,
						long arg3) {
					// TODO Auto-generated method stub
					DTO_TRCase dto = new DTO_TRCase();

					dto = mydapter.getItem(arg2);
					try {

						if (dto != null) {
							SharedPreferences sh = getSharedPreferences("app",
									MODE_PRIVATE);
							Editor e = sh.edit();
							e.putString("ThirdCoordinateTime", "");
							e.putString("MNETTIME", "");
							// 20121005: bug 045
							e.putString("ShowMNETTIME", "0");
							e.putString("ShowThirdCoordinateTime", "0");
							// end
							e.commit();
							String firestationcode = dto.getFireStationCode();
							String casecode = dto.getCaseCode();
							CL_TRCase cl = new CL_TRCase(E002.this);
							Cursor cr = cl.getItemTRcase(firestationcode,
									casecode);
							dto.setContactCount(cr.getString(cr
									.getColumnIndex("ContactCount")));
							Intent myinten = new Intent(v.getContext(),
									E005.class);
							Bundle b = new Bundle();
							b.putString("FireStationCode", firestationcode);
							b.putString("CaseCode", casecode);
							b.putString(TR_CaseCount.JianNo, dto.getJianNo());
							b.putString(BaseCount.IsOldData, "1");
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);

						} else {
							loadDataforGrid(InsertDate);
						}
					} catch (Exception e) {
						// TODO: handle exception
						E022.saveErrorLog(e.getMessage(), E002.this);
					}
				}
			});
			list.setOnTouchListener(this);
			this.loadDataforGrid(InsertDate);
			LinearLayout layout_title = (LinearLayout) findViewById(R.id.layout_title);
			layout_title.setOnTouchListener(this);
			Button btne002 = (Button) findViewById(R.id.btnE002NewJAN);
			btne002.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					updateJA = false;
					if (!dShow) {
						dShow = true;
						showDiaglogE003();
					}
				}
			});
			btne002.setOnTouchListener(this);
			Button btn2e002 = (Button) findViewById(R.id.btn2E002);
			btn2e002.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten = new Intent(v.getContext(), E004.class);
					// Bundle b = new Bundle();
					// b.putInt("LoadLast", 1);
					// myinten.putExtras(b);
					startActivityForResult(myinten, 0);
				}
			});
			btn2e002.setOnTouchListener(this);
			// Button btnlogout = (Button) findViewById(R.id.btnE2Logout);
			// btnlogout.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			//
			// confirmLogout();
			//
			// }
			// });
			btnDisasster = (Button) findViewById(R.id.btnDisaster);
			btnDisasster.setOnLongClickListener(new OnLongClickListener() {

				public boolean onLongClick(View arg0) {
					// TODO Auto-generated method stub
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e = sh.edit();
					e.putString("kin_no", "");
					e.putString("kin_branch_no", "");
					e.commit();
					btnDisasster.setVisibility(View.INVISIBLE);

					return true;
				}
			});
			// SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
			String kinno = sh.getString("kin_no", "");
			if (kinno.equals("")) {
				btnDisasster.setVisibility(View.INVISIBLE);
			} else {
				btnDisasster.setVisibility(View.VISIBLE);
				btnDisasster.setText(kinno);

			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void getListSex() {
		listSex = new HashMap<String, String>();
		Cl_Code cl = new Cl_Code(this);
		Cursor cur = cl.getMS_Code("00013");
		if (cur != null && cur.getCount() > 0) {
			do {
				String code = cur.getString(cur.getColumnIndex("CodeID"));
				String name = cur.getString(cur.getColumnIndex("CodeName"));
				listSex.put(code, name);
			} while (cur.moveToNext());
		}
		cur.close();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		// Bug 038
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			dShow = false;
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		bt.setRunning(false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		bt.showButtonAuto(this, "", "", true);
		E034.SetActivity(this);
		// Bug 166
		boolean gps_enabled = false, network_enabled = false;

		try {
			gps_enabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}
		// locationManager.requestLocationUpdates(tower, 0, 0, this);// Bug 181
		if (gps_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 1, this);
		}
		if (network_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 0, 1, this);
		}
	}

	private void updateCaseCode() {
		try {
			CL_TRCase cl = new CL_TRCase(this);
			int STT = cl.getMaxJANNumber(FireStationCode,
					CaseCode.substring(0, 18));

			int res = 0;
			String newcascode = "";
			do {
				STT++;
				String num = Utilities.format4Number(STT);
				newcascode = CaseCode.substring(0, 18) + num;
				CL_TRCase tr = new CL_TRCase(E002.this);
				res = tr.changeJIAN(FireStationCode, CaseCode, newcascode,
						JianNo);
			} while (res == 0);
			updateJA = false;
			for (int i = 0; i < mydapter.getCount(); i++) {
				DTO_TRCase dt = new DTO_TRCase();
				dt = mydapter.getItem(i);
				if (dt.getFireStationCode().equals(FireStationCode)
						&& dt.getCaseCode().equals(CaseCode)) {

					dt.setCaseCode(newcascode);
					mydapter.notifyDataSetChanged();

					break;
				}
			}
			CaseCode = newcascode;
			CL_TRCase tr = new CL_TRCase(E002.this);
			if (tr.checkKanyou(FireStationCode, CaseCode)) {
				startThread();
			} else { // Bug 98

				E022.showErrorDialog(E002.this, ContantMessages.KanYou,
						ContantMessages.KanYou, false);
			}
		} catch (Exception ex) {
			exitProccess();
		} finally {

			// dShow = false;
		}
	}

	private void showDiaglogE003() {

		try {
			final Dialog dialoge3;
			dialoge3 = new Dialog(E002.this);
			dialoge3.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialoge3.setContentView(R.layout.layoutdialoge003);
			// dialoge3.setCanceledOnTouchOutside(false);
			dialoge3.setCancelable(false);
			Button btnCacnel = (Button) dialoge3.findViewById(R.id.btnE3Cancel);
			btnCacnel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dShow = false;
					// dialoge3.cancel();
					dialoge3.dismiss();
				}
			});
			final TextView txtDateNow = (TextView) dialoge3
					.findViewById(R.id.txtE3Date);

			final TextView txtCaseCodee3 = (TextView) dialoge3
					.findViewById(R.id.txtCaseCode);
			final EditText txtJianNO = (EditText) dialoge3
					.findViewById(R.id.txtJianNo);
			txtJianNO.setText(getJianoInit());
			Button btnCrate = (Button) dialoge3.findViewById(R.id.btnE3Create);
			btnCrate.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dShow = false;
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e = sh.edit();
					e.putString("ThirdCoordinateTime", "");
					e.putString("MNETTIME", "");
					// 20121005: bug 045
					e.putString("ShowMNETTIME", "0");
					e.putString("ShowThirdCoordinateTime", "0");
					// end
					e.commit();
					String phone = getPhoneNumber();
					Cl_CreateDB cl = new Cl_CreateDB(E002.this);
					Cursor cur = cl.getFireStationCodeByPhone(phone);
					if (cur != null && cur.getCount() > 0) {
						String firestationcode = cur.getString(cur
								.getColumnIndex("FireStationCode"));
						String casecode = txtCaseCodee3.getText().toString()
								.replace("-", "");

						String jianno = txtJianNO.getText().toString();

						if (!firestationcode.equals("")) {
							if (!updateJA) {
								ContentValues values = new ContentValues();
								if (jianno != null && !jianno.equals("")) {
									CL_TRCase clt = new CL_TRCase(E002.this);
									boolean res = clt.checkJianNo(jianno,
											firestationcode);
									if (res) {
										CL_Message ms = new CL_Message(
												E002.this);
										String mg = ms
												.getMessage(ContantMessages.JianNoExist);
										E022.showErrorDialog(E002.this, mg,
												ContantMessages.JianNoExist,
												false);

										return;
									}
									values.put(TR_CaseCount.JianNo, jianno);
								} else {
									values.putNull(TR_CaseCount.JianNo);
								}

								values.put(TR_CaseCount.FireStationCode,
										firestationcode);
								values.put(TR_CaseCount.CaseCode, casecode);

								values.put(TR_CaseCount.EMSUnitCode,
										EMSUnitCode);

								values.put(TR_CaseCount.Status, "0");
								values.put(TR_CaseCount.DeleteFLG, "0");

								String indate = Utilities.getDateTimeNow();
								values.put(TR_CaseCount.Insert_DATETIME, indate);
								values.put(TR_CaseCount.Update_DATETIME, indate);
								values.put(TR_CaseCount.ContactCount, "0");
								CL_TRCase tr = new CL_TRCase(E002.this);
								if (tr.checkExistTRcase(firestationcode,
										casecode)) {
									// Toast.makeText(E002.this,
									// ContantMessages.CaseCodeExist,
									// Toast.LENGTH_SHORT).show();
									CL_Message ms = new CL_Message(E002.this);
									String mg = ms
											.getMessage(ContantMessages.CaseCodeExist);
									String content = MessageFormat.format(mg,
											casecode);
									E022.showErrorDialog(E002.this, content,
											ContantMessages.CaseCodeExist,
											false);

									return;
								}
								tr = new CL_TRCase(E002.this);
								long result = tr.createTRCase(values);
								if (result <= 0) {
									// Toast.makeText(E002.this,
									// ContantMessages.Insertnotsuccessfull,
									// Toast.LENGTH_SHORT).show();
									E022.showErrorDialog(
											E002.this,
											ContantMessages.Insertnotsuccessfull,
											ContantMessages.Insertnotsuccessfull,
											false);
								} else {
									DTO_TRCase dtcase = new DTO_TRCase();
									dtcase.setContactCount("0");
									Intent myinten = new Intent(v.getContext(),
											E005.class);
									Bundle b = new Bundle();
									b.putString("FireStationCode",
											firestationcode);
									b.putString("CaseCode", casecode);
									b.putString(TR_CaseCount.JianNo, jianno);
									b.putString(BaseCount.IsOldData, "0");
									myinten.putExtras(b);
									startActivityForResult(myinten, 0);
								}
							} else {
								CL_TRCase clt = new CL_TRCase(E002.this);
								boolean res = clt.checkJianNo(FireStationCode,
										CaseCode, jianno);
								if (res) {
									CL_Message ms = new CL_Message(E002.this);
									String mg = ms
											.getMessage(ContantMessages.JianNoExist);
									E022.showErrorDialog(E002.this, mg,
											ContantMessages.JianNoExist, false);

									return;
								}
								CL_TRCase tr = new CL_TRCase(E002.this);
								tr.changeJIAN(FireStationCode, CaseCode,
										casecode, jianno);
								updateJA = false;
								for (int i = 0; i < mydapter.getCount(); i++) {
									DTO_TRCase dt = new DTO_TRCase();
									dt = mydapter.getItem(i);
									if (dt.getFireStationCode().equals(
											FireStationCode)
											&& dt.getCaseCode()
													.equals(CaseCode)) {

										dt.setCaseCode(casecode);
										dt.setJianNo(jianno);
										dt.setCaseCode1(txtDateNow.getText()
												.toString());
										mydapter.notifyDataSetChanged();

										break;
									}
								}

							}
						}
					}
					cur.close();
					// dialoge3.cancel();
					dialoge3.dismiss();
				}
			});
			// get max jian by FireStationCode and date
			Cl_CreateDB cr = new Cl_CreateDB(this);
			final String fire = cr.getFireStationCode(getPhoneNumber());

			//

			String tmpESMcode = EMSUnitCode;
			if (tmpESMcode.length() < 10) {
				while (tmpESMcode.length() < 10) {
					tmpESMcode = "0" + tmpESMcode;
				}
			}
			CL_TRCase cl = new CL_TRCase(this);
			String STT = cl.getMaxJAN(fire,
					tmpESMcode.concat(Utilities.getDateNow()));
			txtCaseCodee3.setText(tmpESMcode + "-" + Utilities.getDateNow()
					+ "-" + STT);
			// 20121005 daitran add:B-005
			c = Calendar.getInstance();
			// End
			txtDateNow.setText(dfDate.format(c.getTime()));
			today = txtDateNow.getText().toString();
			//final EditText txtJianNo = (EditText) dialoge3
			//		.findViewById(R.id.txtJianNo);
			if (updateJA) {

				txtJianNO.setText(JianNo);
			}
			txtJianNO.setOnEditorActionListener(new OnEditorActionListener() {

				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_SEARCH
							|| actionId == EditorInfo.IME_ACTION_DONE
							|| event.getAction() == KeyEvent.ACTION_DOWN
							&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(txtJianNO.getWindowToken(),
								0);
						return true;
					}
					return false;
				}
			});
			Button btnNext = (Button) dialoge3.findViewById(R.id.btnNext);
			btnNext.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (txtDateNow.getText().toString().equals(today)) {
						return;
					} else {
						c.add(Calendar.DAY_OF_MONTH, 1);
						txtDateNow.setText(dfDate.format(c.getTime()));

						CL_TRCase cl = new CL_TRCase(E002.this);
						String date = txtDateNow.getText().toString()
								.replace("/", "");
						String STT = cl.getMaxJAN(fire,
								EMSUnitCode.concat(date));
						txtCaseCodee3.setText(EMSUnitCode + "-" + date + "-"
								+ STT);
					}

				}
			});
			Button btnPre = (Button) dialoge3.findViewById(R.id.btnPre);
			btnPre.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					c.add(Calendar.DAY_OF_MONTH, -1);
					txtDateNow.setText(dfDate.format(c.getTime()));
					//
					CL_TRCase cl = new CL_TRCase(E002.this);
					String date = txtDateNow.getText().toString()
							.replace("/", "");
					String STT = cl.getMaxJAN(fire, EMSUnitCode.concat(date));
					txtCaseCodee3.setText(EMSUnitCode + "-" + date + "-" + STT);
				}
			});
			dialoge3.show();
			txtJianNO.setSelection(txtJianNO.getText().toString().length());
		} catch (Exception ex) {
		} finally {

			// dShow = false;
		}
	}

	// void updateListView() {
	// myrunalbelistview able = new myrunalbelistview();
	// Thread th = new Thread(able);
	// th.start();
	//
	// }
	//
	// class myrunalbelistview implements Runnable {
	//
	// public void run() {
	// // TODO Auto-generated method stub
	//
	// handlerlist.post(new Runnable() {
	//
	// public void run() {
	// // TODO Auto-generated method stub
	// mydapter.notifyDataSetChanged();
	// }
	// });
	//
	// }
	//
	// }

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) getApplicationContext()
//					.getSystemService(Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Bug 127
	private void loadDataforGrid1(String insertDate) {
		Cursor c2 = null;
		try {

			CL_TRCase tr = new CL_TRCase(E002.this);
			c2 = tr.fetchTRCase(insertDate, "1");
			// int row=c2.getCount();
			if (c2 != null && c2.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_TRCase dto = new DTO_TRCase();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_TRCase tc = new DTO_TRCase();
					String caseCode = c2.getString(c2
							.getColumnIndex(TR_CaseCount.CaseCode));
					tc.setCaseCode(caseCode);
					tc.setJianNo(c2.getString(c2
							.getColumnIndex(TR_CaseCount.JianNo)));
					// tc.setCaseCode1(caseCode.substring(0, 4) + "/"
					// + caseCode.substring(4, 6) + "/"
					// + caseCode.substring(6, 8));
					// tc.setCaseCode2(caseCode.substring(8, 12));
					// tc.setCaseCode3(caseCode.substring(12, 14));
					// tc.setCaseCode4(caseCode.substring(14, 16));

					tc.setAge(c2.getString(c2.getColumnIndex(TR_CaseCount.Age)));
					tc.setSex(c2.getString(c2.getColumnIndex(TR_CaseCount.Sex)));
					tc.setNotice_TEXT(c2.getString(c2
							.getColumnIndex(TR_CaseCount.Notice_TEXT)));
					tc.setFireStationCode(c2.getString(c2
							.getColumnIndex(TR_CaseCount.FireStationCode)));
					tc.setContactCount(c2.getString(c2
							.getColumnIndex(TR_CaseCount.ContactCount)));
					// HashMap<String, String> map = new HashMap<String,
					// String>();
					// map = getHopitalName(tc.getFireStationCode(),
					// tc.getCaseCode());
					// if (map.containsKey("MIName_Kanji"))
					// tc.setMIName_Kanji(map.get("MIName_Kanji"));
					String HoptalName = c2.getString(c2
							.getColumnIndex("FacilitiesName"));
					tc.setMIName_Kanji(HoptalName);
					// 20121006: dai modify get address jian: bug 80
					// if (map.containsKey("Address_Kanji"))
					// tc.setAddress_Kanji(map.get("Address_Kanji"));
					String add = c2.getString(c2
							.getColumnIndex(TR_CaseCount.IncidentScene));
					tc.setIncidentScene(add);
					// end
					InsertDate = c2.getString(c2
							.getColumnIndex(TR_CaseCount.Insert_DATETIME));
					tc.setInsert_DATETIME(InsertDate);
					tc.kin_kbn = c2.getString(c2.getColumnIndex("kin_kbn"));
					tc.kin_no = c2.getString(c2.getColumnIndex("kin_no"));
					tc.kin_branch_no = c2.getString(c2
							.getColumnIndex("kin_branch_no"));
					mydapter.add(tc);

				} while (c2.moveToNext());

				if (!InsertDate.equals(InsertDateFirst)) {
					mydapter.add(null);
				}
				mydapter.notifyDataSetChanged();
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E002.this, e.getMessage(), "E002", true);
		} finally {
			if (c2 != null)
				c2.close();
		}
	}

	private void loadDataforGrid(String insertDate) {
		Cursor c2 = null;
		try {

			CL_TRCase tr = new CL_TRCase(E002.this);
			c2 = tr.fetchTRCase(insertDate, Limit);
			// int row=c2.getCount();
			if (c2 != null && c2.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_TRCase dto = new DTO_TRCase();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_TRCase tc = new DTO_TRCase();
					String caseCode = c2.getString(c2
							.getColumnIndex(TR_CaseCount.CaseCode));
					tc.setCaseCode(caseCode);
					tc.setJianNo(c2.getString(c2
							.getColumnIndex(TR_CaseCount.JianNo)));
					// tc.setCaseCode1(caseCode.substring(0, 4) + "/"
					// + caseCode.substring(4, 6) + "/"
					// + caseCode.substring(6, 8));
					// tc.setCaseCode2(caseCode.substring(8, 12));
					// tc.setCaseCode3(caseCode.substring(12, 14));
					// tc.setCaseCode4(caseCode.substring(14, 16));

					tc.setAge(c2.getString(c2.getColumnIndex(TR_CaseCount.Age)));
					tc.setSex(c2.getString(c2.getColumnIndex(TR_CaseCount.Sex)));
					tc.setNotice_TEXT(c2.getString(c2
							.getColumnIndex(TR_CaseCount.Notice_TEXT)));
					tc.setFireStationCode(c2.getString(c2
							.getColumnIndex(TR_CaseCount.FireStationCode)));
					tc.setContactCount(c2.getString(c2
							.getColumnIndex(TR_CaseCount.ContactCount)));
					// HashMap<String, String> map = new HashMap<String,
					// String>();
					// map = getHopitalName(tc.getFireStationCode(),
					// tc.getCaseCode());
					// if (map.containsKey("MIName_Kanji"))
					// tc.setMIName_Kanji(map.get("MIName_Kanji"));
					String HoptalName = c2.getString(c2
							.getColumnIndex("FacilitiesName"));
					tc.setMIName_Kanji(HoptalName);
					// 20121006: dai modify get address jian: bug 80
					// if (map.containsKey("Address_Kanji"))
					// tc.setAddress_Kanji(map.get("Address_Kanji"));
					String add = c2.getString(c2
							.getColumnIndex(TR_CaseCount.IncidentScene));
					tc.setIncidentScene(add);
					// end
					InsertDate = c2.getString(c2
							.getColumnIndex(TR_CaseCount.Insert_DATETIME));
					tc.setInsert_DATETIME(InsertDate);
					tc.kin_kbn = c2.getString(c2.getColumnIndex("kin_kbn"));
					tc.kin_no = c2.getString(c2.getColumnIndex("kin_no"));
					tc.kin_branch_no = c2.getString(c2
							.getColumnIndex("kin_branch_no"));

					mydapter.add(tc);

				} while (c2.moveToNext());

				if (!InsertDate.equals(InsertDateFirst)) {
					mydapter.add(null);
				}
				mydapter.notifyDataSetChanged();
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E002.this, e.getMessage(), "E002", true);
		} finally {
			if (c2 != null)
				c2.close();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		Editor e = sh.edit();
		e.putInt("UpdateName", 0);
		e.commit();
		// bt.setRunning(false);
		thcheck.StopCheck();
		locationManager.removeUpdates(this);
		// thcheckMg.StopCheck();
	}

	private void confirmDelete(final Dialog dialogedit) {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				dialogedit.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				CL_TRCase cl = new CL_TRCase(E002.this);
				boolean result = cl.deleteJIAN(FireStationCode, CaseCode);
				if (result) {
					mydapter.remove(dt);
					mydapter.notifyDataSetChanged();
				}
				dialog.cancel();
				dialogedit.cancel();
			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteJian);
		lbmg.setText(mg);
		dialog.show();
	}

	private String getCaseCodeNew() {
		String tmpESMcode = EMSUnitCode;
		if (tmpESMcode.length() < 10) {
			while (tmpESMcode.length() < 10) {
				tmpESMcode = "0" + tmpESMcode;
			}
		}
		CL_TRCase cl = new CL_TRCase(E002.this);
		String STT = cl.getMaxJAN(FireStationCode,
				tmpESMcode.concat(Utilities.getDateNow()));
		return tmpESMcode + Utilities.getDateNow() + STT;
	}

	private void editJian() {

		dialogedit = new Dialog(this);
		dialogedit.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogedit.setContentView(R.layout.layoutee002copyjian);
		dialogedit.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialogedit.findViewById(R.id.btnBack);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialogedit.cancel();
			}
		});
		Button btnSendJian = (Button) dialogedit.findViewById(R.id.btnSendJian);
		btnSendJian.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				CL_TRCase tr = new CL_TRCase(E002.this);
				if (tr.checkKanyou(FireStationCode, CaseCode)) {
					bEdit = true;
					startThread();

				} else { // Bug 98

					E022.showErrorDialog(E002.this, ContantMessages.KanYou,
							ContantMessages.KanYou, false);
				}
			}
		});
		Button btnCopyJian = (Button) dialogedit.findViewById(R.id.btnCopyJian);
		btnCopyJian.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				CL_TRCase cl = new CL_TRCase(E002.this);
				DTO_TRCase dt = cl.CopyJIAN(FireStationCode, CaseCode,
						getCaseCodeNew());
				if (dt != null) {
					mydapter.insert(dt, 0);
					InsertDate = dt.getInsert_DATETIME();
					mydapter.notifyDataSetChanged();
					dialogedit.cancel();

				}
			}
		});
		Button btnDeleteJian = (Button) dialogedit.findViewById(R.id.btnDelete);
		btnDeleteJian.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmDelete(dialogedit);

			}
		});
		Button btnLink = (Button) dialogedit.findViewById(R.id.btnLinkKinNo);
		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		String kin_no = sh.getString("kin_no", "");
		if (kin_no.equals("")) {
			btnLink.setBackgroundResource(R.drawable.btndisablelink);
		}
		btnLink.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				String kin_no = sh.getString("kin_no", "");
				if (kin_no.equals("") == false) {
					String kin_branch_no = sh.getString("kin_branch_no", "");
					CL_TRCase cl = new CL_TRCase(E002.this);
					int res = cl.LinkToKinNo(FireStationCode, CaseCode, kin_no,
							kin_branch_no);
					if (res > 0) {
						CL_TRCase tr = new CL_TRCase(E002.this);
						int up = tr.updateKin_kbn(FireStationCode, CaseCode,
								"1");
						if (dt != null) {
							if (up > 0)
								dt.kin_kbn = "1";
							dt.kin_no = kin_no;
							dt.kin_branch_no = kin_branch_no;
							mydapter.notifyDataSetChanged();
						}
						dialogedit.cancel();
					}
				}

			}
		});
		dialogedit.show();
	}

	public class MyAdapter extends ArrayAdapter<DTO_TRCase> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.grid002, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}

			TextView txtJianNo = myw.getJianNo();
			TextView txtCasecode = myw.getCasecode();
			TextView btnDelete = myw.getbtnDelete();// add 20130305
			TextView txtStatus = myw.getStatus();
			TextView txtSex = myw.getSex();
			TextView txtAge = myw.getAge();
			// TextView txtNext = myw.getNext();
			TextView txtAddress = myw.getAddress();
			TextView txtHopital = myw.getHopital();
			TextView txtTitleSex = myw.getTitleSex();
			TextView txtTitleAge = myw.getTitleAge();
			TextView txtKBN = myw.getKBN();
			LinearLayout row = myw.getRow();
			if (this.getItem(position) != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(10, 0, 10, 0);
				row.setLayoutParams(param);
				row.setBackgroundResource(R.drawable.gridbg);
				txtJianNo.setVisibility(View.VISIBLE);
				txtCasecode.setVisibility(View.VISIBLE);
				btnDelete.setVisibility(View.VISIBLE);// add 20130305
				txtStatus.setVisibility(View.VISIBLE);
				txtSex.setVisibility(View.VISIBLE);
				txtAge.setVisibility(View.VISIBLE);
				txtAddress.setVisibility(View.VISIBLE);
				txtHopital.setVisibility(View.VISIBLE);
				txtTitleSex.setVisibility(View.VISIBLE);
				txtTitleAge.setVisibility(View.VISIBLE);
				txtKBN.setVisibility(View.VISIBLE);
				txtJianNo.setText(this.getItem(position).getJianNo());
				String casecode = this.getItem(position).getCaseCode();
				if (casecode.length() == 22) {

					casecode = casecode.substring(0, 10) + "-"
							+ casecode.substring(10, 18) + "-"
							+ casecode.substring(18, 22);
				}
				txtCasecode.setText(casecode);
				txtStatus.setText("完了");

				String sex = "";
				if (this.getItem(position).getSex() != null) {
					txtTitleSex.setText("性");
					// if (this.getItem(position).getSex().equals(BaseCount.Nu))
					// {
					// sex = "女";
					// } else {
					// sex = "男";
					// }
					if (listSex.containsKey(this.getItem(position).getSex())) {
						sex = listSex.get(this.getItem(position).getSex());
					}
				} else {
					txtTitleSex.setText("");
				}
				txtSex.setText(sex);
				if (this.getItem(position).getAge() != null) {
					txtTitleAge.setText("歳");
					txtAge.setText(this.getItem(position).getAge());
				} else {
					txtTitleAge.setText("");
					txtAge.setText("");
				}
				txtAddress.setText(this.getItem(position).getIncidentScene());
				txtHopital.setText(this.getItem(position).getMIName_Kanji());
				btnDelete.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						dt = mydapter.getItem(position);
						FireStationCode = dt.getFireStationCode();
						CaseCode = dt.getCaseCode();
						// confirmDelete();
						// 404_0
						JianNo = dt.getJianNo();
						editJian();
					}
				});
				if (this.getItem(position).kin_kbn != null) {
					if (this.getItem(position).kin_kbn.equals("1")) {
						txtKBN.setText(this.getItem(position).kin_no);
						txtKBN.setBackgroundResource(R.drawable.btndisaster);
					} else if (this.getItem(position).kin_kbn.equals("0")) {
						txtKBN.setText("まもって");
						txtKBN.setBackgroundResource(R.drawable.btnnanomet);
					} else if (this.getItem(position).kin_kbn.equals("2")) {
						txtKBN.setText("一斉");
						txtKBN.setBackgroundResource(R.drawable.btnsended);
					} else {
						txtKBN.setVisibility(View.INVISIBLE);
					}

				} else {
					txtKBN.setVisibility(View.INVISIBLE);
				}
				txtKBN.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dt = mydapter.getItem(position);
						if (dt.kin_kbn.equals("1")) {
							CL_TRCase cl = new CL_TRCase(E002.this);
							cl.ClearKinNo(dt.getFireStationCode(),
									dt.getCaseCode());
							dt.kin_kbn = "";
							dt.kin_no = "";
							dt.kin_branch_no = "";
							dt.kinLink_DateTime = "";
							mydapter.notifyDataSetChanged();
						}
					}
				});
				txtStatus.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Toast.makeText(E002.this,
						// "Update Status "+mydapter.getItem(position).getCaseCode(),
						// Toast.LENGTH_SHORT).show();
						bEdit = false;
						dt = mydapter.getItem(position);
						FireStationCode = dt.getFireStationCode();
						CaseCode = dt.getCaseCode();
						JianNo = dt.getJianNo();
						CL_TRCase tr = new CL_TRCase(E002.this);
						if (tr.checkKanyou(FireStationCode, CaseCode)) {
							startThread();
						} else { // Bug 98

							E022.showErrorDialog(E002.this,
									ContantMessages.KanYou,
									ContantMessages.KanYou, false);
						}
						// CL_TRCase cl = new CL_TRCase(E002.this);
						// cl.updateStatusTRCase(firestationCode, casecode,
						// "1");
						// // loadDataforGrid();
						// for (int i = 0; i < mydapter.getCount(); i++) {
						// DTO_TRCase dt = new DTO_TRCase();
						// dt = mydapter.getItem(i);
						// if (dt.getFireStationCode().equals(firestationCode)
						// && dt.getCaseCode().equals(casecode)) {
						// mydapter.remove(dt);
						// break;
						// }
						// }
						// mydapter.notifyDataSetChanged();

					}
				});
			} else {

				// row.removeAllViews();
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				row.setLayoutParams(param);
				txtJianNo.setVisibility(View.INVISIBLE);
				txtCasecode.setVisibility(View.INVISIBLE);
				btnDelete.setVisibility(View.INVISIBLE);// add 20130305
				txtStatus.setVisibility(View.INVISIBLE);
				txtSex.setVisibility(View.INVISIBLE);
				txtAge.setVisibility(View.INVISIBLE);
				txtAddress.setVisibility(View.INVISIBLE);
				txtHopital.setVisibility(View.INVISIBLE);
				txtTitleSex.setVisibility(View.INVISIBLE);
				txtTitleAge.setVisibility(View.INVISIBLE);
				txtKBN.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow);
				// row.setOnClickListener(new OnClickListener() {
				//
				// public void onClick(View v) {
				// // TODO Auto-generated method stub
				// loadDataforGrid(InsertDate);
				//
				// }
				// });
			}
			return v;
		}

	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		TextView lable2 = null;
		TextView lable3 = null;
		TextView lable4 = null;
		TextView lable5 = null;
		TextView lable6 = null;
		TextView lable7 = null;
		TextView lable8 = null;
		TextView lable9 = null;
		TextView lable10 = null;
		TextView lable11 = null;
		LinearLayout layoutrow = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getJianNo() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtJianNo);
			}
			return lable1;
		}

		TextView getCasecode() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtCasecode);
			}
			return lable2;
		}

		TextView getbtnDelete() {
			if (lable3 == null) {
				lable3 = (TextView) base.findViewById(R.id.btnDelete);
			}
			return lable3;
		}

		TextView getStatus() {
			if (lable4 == null) {
				lable4 = (TextView) base.findViewById(R.id.txtStatus);
			}
			return lable4;
		}

		TextView getSex() {
			if (lable5 == null) {
				lable5 = (TextView) base.findViewById(R.id.txtSex);
			}
			return lable5;
		}

		TextView getAge() {
			if (lable8 == null) {
				lable8 = (TextView) base.findViewById(R.id.txtAge);
			}
			return lable8;
		}

		TextView getHopital() {
			if (lable6 == null) {
				lable6 = (TextView) base.findViewById(R.id.txtHopital);
			}
			return lable6;
		}

		TextView getAddress() {
			if (lable7 == null) {
				lable7 = (TextView) base.findViewById(R.id.txtAddress);
			}
			return lable7;
		}

		TextView getTitleSex() {
			if (lable9 == null) {
				lable9 = (TextView) base.findViewById(R.id.txtTilteSex);
			}
			return lable9;
		}

		TextView getTitleAge() {
			if (lable10 == null) {
				lable10 = (TextView) base.findViewById(R.id.txtTilteAge);
			}
			return lable10;
		}

		TextView getKBN() {
			if (lable11 == null) {
				lable11 = (TextView) base.findViewById(R.id.btnKBN);
			}
			return lable11;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;

			try {

				CL_TRCase tr = new CL_TRCase(E002.this);
				cur = tr.getItemTRcaseUpdateServer(FireStationCode, CaseCode);
				if (cur != null && cur.getCount() > 0 && isOnline()) {
					JSONArray array = new JSONArray();
					JSONObject trcase = ParseToJson(cur, "TR_Case");
					array.put(trcase);

					Cl_ContactResultRecord ctr = new Cl_ContactResultRecord(
							E002.this);
					cur = ctr.getTR_ContactResultRecordPatient(FireStationCode,
							CaseCode);
					JSONObject contact = ParseToJson(cur,
							"TR_ContactResultRecord");
					array.put(contact);

					Cl_FireStationStandard fs = new Cl_FireStationStandard(
							E002.this);
					cur = fs.getTR_FireStationStandardPatient(FireStationCode,
							CaseCode);
					JSONObject fire = ParseToJson(cur, "TR_FireStationStandard");
					array.put(fire);

					Cl_TRPatient pa = new Cl_TRPatient(E002.this);
					cur = pa.getTR_Patient(FireStationCode, CaseCode);
					JSONObject patient = ParseToJson(cur, "TR_Patient");
					array.put(patient);

					Cl_MovementTime mom = new Cl_MovementTime(E002.this);
					cur = mom.getTR_MovementTimeAll(FireStationCode, CaseCode);
					JSONObject movemnt = ParseToJson(cur, "TR_MovementTime");
					array.put(movemnt);

					Cl_FirstAid fir = new Cl_FirstAid(E002.this);
					cur = fir.getTR_FirstAid(FireStationCode, CaseCode);
					JSONObject FA = ParseToJson(cur, "TR_FirstAid");
					array.put(FA);

					Cl_FireStationStandard fs2 = new Cl_FireStationStandard(
							E002.this);
					cur = fs2.getTR_SelectionKamoku(FireStationCode, CaseCode);
					JSONObject select = ParseToJson(cur, "TR_SelectionKamoku");
					array.put(select);
					CL_ErrorLog er = new CL_ErrorLog(E002.this);
					String phone = getPhoneNumber();
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					String Output_DATETIME = sh.getString("Output_DATETIME",
							"00010101010101");
					cur = er.getTR_ErrorLog(phone, Output_DATETIME);
					JSONObject log = ParseToJson(cur, "TR_ErrorLog");
					array.put(log);

					CL_FileInfo file = new CL_FileInfo(E002.this);
					cur = file.getTR_FileInfo(FireStationCode, CaseCode);
					JSONObject jfile = ParseToJson(cur, "TR_FileManageInfo");
					array.put(jfile);

					CL_TR_TriagePATMethod me = new CL_TR_TriagePATMethod(
							E002.this);
					cur = me.getTR_TriagePATMethod(FireStationCode, CaseCode);
					JSONObject jMeThod = ParseToJson(cur, "TR_TriagePATMethod");
					array.put(jMeThod);

					me = new CL_TR_TriagePATMethod(E002.this);
					cur = me.getTR_TriagePATMethod_Result(FireStationCode,
							CaseCode);
					JSONObject jMeThod_Res = ParseToJson(cur,
							"TR_TriagePATMethod_Result");
					array.put(jMeThod_Res);

					CL_TR_TriageSTARTMethod sm = new CL_TR_TriageSTARTMethod(
							E002.this);
					cur = sm.getTR_TriageSTARTMethod(FireStationCode, CaseCode);
					JSONObject jStartMeThod = ParseToJson(cur,
							"TR_TriageSTARTMethod");
					array.put(jStartMeThod);

					sm = new CL_TR_TriageSTARTMethod(E002.this);
					cur = sm.getTR_TriageSTARTMethod_Result(FireStationCode,
							CaseCode);
					JSONObject jStartMeThod_Res = ParseToJson(cur,
							"TR_TriageSTARTMethod_Result");
					array.put(jStartMeThod_Res);

					int res = uploadDataToServer(array.toString());
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					if (res == 1) {
						b.putSerializable("DTO", dt);

						Editor e = sh.edit();
						e.putString("Output_DATETIME",
								Utilities.getDateTimeNow());
						e.commit();
					} else {
						b.putSerializable("DTO", null);
					}
					b.putInt("Result", res);// Bug 116
					b.putString("Key", "1");
					mg.setData(b);
					handler.sendMessage(mg);

				} else {
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					b.putSerializable("DTO", null);
					b.putString("Key", "1");
					mg.setData(b);
					handler.sendMessage(mg);

				}
			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);
			} finally {

				if (cur != null)
					cur.close();
			}

		}
	};

	public JSONObject ParseToJson(Cursor cor, String table) {
		JSONObject ob = new JSONObject();
		JSONArray arr = new JSONArray();
		try {
			if (cor != null && cor.getCount() > 0) {
				do {
					JSONObject json = new JSONObject();
					for (int i = 0; i < cor.getColumnCount(); i++) {
						String colname = cor.getColumnName(i);
						String value = cor.getString(i);

						if (colname.equals("patientstate")
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "00");
							}
						} else if (colname.equals(TR_CaseCount.ContactCount)
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
						} else if (colname.equals(TR_CaseCount.Status)
								&& table.equals(TR_CaseCount.TableName)) {
							json.put(colname, "1");
						} else {
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "");
							}
						}
					}
					arr.put(json);
				} while (cor.moveToNext());
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			E022.saveErrorLog(e1.getMessage(), E002.this);
		}
		try {
			ob.put(table, arr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return ob;
	}

	// Bug 116 Boolean -> int
	public int uploadDataToServer(String json) {
		// myadapter.clear();

		String encoded = "";
		try {
			encoded = URLEncoder.encode(json, "UTF-8");
			// String de= URLDecoder.decode(encoded, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
		}
		int result = 0;
		TrustManagerManipulator.allowAllSSL();
		String NAMESPACE = ContantSql.NameSpace;
		String METHOD_NAME = ContantSql.updateTRTable;
		String URL = ContantSystem.Endpoint;
		String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		request.addProperty("Json", encoded);
		// Param log 20130103
		CL_User cl = new CL_User(E002.this);
		String user = cl.getUserName();
		String loginfo = getLogInfo(SOAP_ACTIONS, "完了", user, getResources()
				.getString(R.string.txtTitleE2));
		request.addProperty("smpLog", loginfo);
		//
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		// envelope.dotNet=true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
				ContantSystem.TimeOut);

		try {
			androidhttpTranport.call(SOAP_ACTIONS, envelope);
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			result = 0;
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			result = 0;
		}
		Object responseBody = null;
		try {
			responseBody = envelope.getResponse();
			String t = responseBody.toString();
			if (t.equals("1")) {
				result = 1;
			} else if (t.equals("10")) {
				result = 10;
			} else if (t.equals("11")) {
				result = 11;
			}
		} catch (SoapFault e2) {
			// TODO Auto-generated catch block
			result = 0;
		}

		return result;
	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();
		showProccess();

	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), E002.this);
		}
		return log;
	}

	// Get Ip
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), E002.this);
		}
		return "";
	}

	// private String getMachineID() {
	// TelephonyManager tManager = (TelephonyManager) getApplicationContext()
	// .getSystemService(Context.TELEPHONY_SERVICE);
	// String uid = tManager.getDeviceId();
	// return uid;
	//
	// }
	String JianNo_InitialFLG, JianNo_Initial;

	private void getInfoUser() {
		Cursor cur = null;
		try {
			CL_User cl = new CL_User(this);
			cur = cl.getUserJianoNo_InitFLG();
			if (cur != null && cur.getCount() > 0) {

				JianNo_InitialFLG = cur.getString(cur
						.getColumnIndex("JianNo_InitialFLG"));
				JianNo_Initial = cur.getString(cur
						.getColumnIndex("JianNo_Initial"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	@SuppressLint("DefaultLocale")
	private String getJianoInit() {
		String jiannoInit = "";
		try {
			getInfoUser();
			if (JianNo_InitialFLG != null && JianNo_InitialFLG.equals("1")) {
				jiannoInit = JianNo_Initial;
			}
			if (jiannoInit == null || jiannoInit.equals("")) {
				CL_User cl1 = new CL_User(E002.this);
				String firestationcode = cl1.getFirestationCode();
				cl1 = new CL_User(E002.this);
				jiannoInit = cl1.getFirestationJianoNo_Init(firestationcode);
			}
			int d = 0;
			int m = 0;
			int y = 0;
			int jiannoInit_start_y = -1;
			int jiannoInit_start_m = -1;
			int jiannoInit_start_d = -1;
			boolean check_y = false;
			boolean check_d = false;
			boolean check_m = false;
			boolean check_process = true;
			for (int test_i = jiannoInit.length(); test_i > 0; test_i--) {
				String char_init = jiannoInit.substring(test_i - 1, test_i)
						.toLowerCase();
				// d
				if (test_i == 1) {
					check_d = true;
					if (char_init.equals("d") || char_init.equals("ｄ"))
						d++;
					check_m = true;
					if (char_init.equals("m") || char_init.equals("ｍ"))
						m++;
					check_y = true;
					if (char_init.equals("y") || char_init.equals("ｙ"))
						y++;
					check_process = true;
				} else if ((d > 0 && (!char_init.equals("d") && !char_init
						.equals("ｄ"))) || test_i == 1) {
					check_d = true;
				} else if (char_init.equals("d") || char_init.equals("ｄ")) {
					d++;
					if (d == 1) {
						jiannoInit_start_d = test_i;
					}
					if (check_m || check_y) {
						check_process = false;
					}
				}
				if (check_d && check_process) {
					if (d > 1) {
						jiannoInit = getText_jiannoInit(jiannoInit, test_i,
								jiannoInit_start_d, "d");
					}
					check_d = false;
					d = 0;
					check_process = true;
				}
				// m
				if ((m > 0 && (!char_init.equals("m") && !char_init.equals("ｍ")))
						|| test_i == 1) {
					check_m = true;
				} else if (char_init.equals("m") || char_init.equals("ｍ")) {
					m++;
					if (m == 1) {
						jiannoInit_start_m = test_i;
					}
					if (check_d || check_y) {
						check_process = false;
					}
				}
				if (check_m && check_process) {
					if (m > 1) {
						jiannoInit = getText_jiannoInit(jiannoInit, test_i,
								jiannoInit_start_m, "m");
					}
					check_m = false;
					m = 0;
					check_process = true;
				}
				// y
				if ((y > 0 && (!char_init.equals("y") && !char_init.equals("ｙ")))
						|| test_i == 1) {
					check_y = true;
				} else if (char_init.equals("y") || char_init.equals("ｙ")) {
					y++;
					if (y == 1) {
						jiannoInit_start_y = test_i;
					}
					if (check_m || check_d) {
						check_process = false;
					}
				}
				if (check_y && check_process) {
					if (y >= 4) {
						jiannoInit = getText_jiannoInit(jiannoInit, test_i,
								jiannoInit_start_y, "yyyy");
					} else if (y >= 2) {
						jiannoInit = getText_jiannoInit(jiannoInit, test_i,
								jiannoInit_start_y, "yy");
					} else if (y == 1) {
						jiannoInit = getText_jiannoInit(jiannoInit, test_i,
								test_i, "y");
					}
					check_y = false;
					y = 0;
					check_process = true;
				}
			}

			// if (!jiannoInit.equals("")) {
			// jiannoInit = jiannoInit.replace("YYYY", Utilities.getYYYY());
			// jiannoInit = jiannoInit.replace("yyyy", Utilities.getYYYY());
			// jiannoInit = jiannoInit.replace("YY", Utilities.getYY());
			// jiannoInit = jiannoInit.replace("yy", Utilities.getYY());
			// jiannoInit = jiannoInit.replace("Y", Utilities.getY());
			// jiannoInit = jiannoInit.replace("y", Utilities.getY());
			//
			// jiannoInit = jiannoInit.replace("MM", Utilities.getMM());
			// jiannoInit = jiannoInit.replace("mm", Utilities.getMM());
			// jiannoInit = jiannoInit.replace("DD", Utilities.getDD());
			// jiannoInit = jiannoInit.replace("dd", Utilities.getDD());
			// }
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E002.this);
		}
		return jiannoInit;
	}

	public String getText_jiannoInit(String jiannoInit, int test_i,
			int jiannoInit_start, String type) {
		String jiannoInit_temp1, jiannoInit_temp2 = "";
		String jiannoInit_change = "";
		int jiannoInit_end = 0;
		if (type.equals("d")) {
			jiannoInit_end = jiannoInit_start - 2;
			jiannoInit_change = Utilities.getDD();
		} else if (type.equals("m")) {
			jiannoInit_end = jiannoInit_start - 2;
			jiannoInit_change = Utilities.getMM();
		} else if (type.equals("y")) {
			jiannoInit_end = jiannoInit_start - 1;
			jiannoInit_change = Utilities.getY();
		} else if (type.equals("yy")) {
			jiannoInit_end = jiannoInit_start - 2;
			jiannoInit_change = Utilities.getYY();
		} else if (type.equals("yyyy")) {
			jiannoInit_end = jiannoInit_start - 4;
			jiannoInit_change = Utilities.getYYYY();
		}
		if (jiannoInit_start < jiannoInit.length()) {
			jiannoInit_temp1 = jiannoInit.substring(0, jiannoInit_end);
			jiannoInit_temp2 = jiannoInit.substring(jiannoInit_start,
					jiannoInit.length());
		} else {
			jiannoInit_temp1 = jiannoInit.substring(0, jiannoInit_end);
		}
		jiannoInit = jiannoInit_temp1 + jiannoInit_change + jiannoInit_temp2;
		return jiannoInit;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != requestCode || resultCode == 0)
			return;
		Bundle b = data.getExtras();
		DTO_Kin_Youse dtKin = (DTO_Kin_Youse) b.getSerializable("TR_Kin_Youse");
		LinearLayout lHeader = (LinearLayout) findViewById(R.id.bgHeader);
		SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
		Editor e = sh.edit();
		switch (requestCode) {
		case 201:
			btnDisasster.setText(dtKin.kin_no);
			btnDisasster.setVisibility(View.VISIBLE);

			e.putString("kin_no", dtKin.kin_no);
			e.putString("kin_branch_no", dtKin.kin_branch_no);
			e.putInt("Mode", 1);
			e.commit();

			lHeader.setBackgroundColor(Color
					.parseColor(HeaderBackcolorOnEmergency));
			if (gps.getRunning() == false) {
				gps.SendGPS(this, locationManager);
			}
			break;
		case 202:

			// btnDisasster.setText(dtKin.kin_no);
			// btnDisasster.setVisibility(View.VISIBLE);
			// e.putString("kin_no", dtKin.kin_no);
			// e.putString("kin_branch_no", dtKin.kin_branch_no);
			// e.putInt("Mode", 1);
			// e.commit();
			// lHeader.setBackgroundColor(Color
			// .parseColor(HeaderBackcolorOnDisaster));
			break;
		default:
			break;
		}
	}

	String HeaderBackcolorOnDisaster = "";
	String HeaderBackcolorOnEmergency = "";

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {

				HeaderBackcolorOnDisaster = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnDisaster"));
				HeaderBackcolorOnEmergency = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnEmergency"));
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				int mode = sh.getInt("Mode", 0);
				LinearLayout lHeader = (LinearLayout) findViewById(R.id.bgHeader);
				if (mode == 2) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnEmergency));
				} else if (mode == 1) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnDisaster));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {
			if (gps.getRunning()) {
				gps.setLocation(location);
			}
		}
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
