package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_LaucherItem {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_LaucherItem(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public Cursor getMS_LauncherItem() {
		Cursor mCursor = null;
		try {
			String sql = "Select * from MS_LauncherItem order by SortNo ASC";
			mCursor = database.rawQuery(sql,null);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int updateJianNoInit(String JianNoInit) {
		int result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put("JianNo_Initial", JianNoInit);

			//String where = "";
			result = database.update("MS_EMSUnitTerminalManagement", values,
					null, null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}
}
