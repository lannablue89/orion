﻿package jp.osaka;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TimePicker;

import com.google.android.maps.GeoPoint;

public class E005 extends Activity implements OnClickListener,
		LocationListener, OnTouchListener {

	static String FireStationCode;
	static String CaseCode;
	String JianNo;
	String oldData = "0";
	String KamokuName01 = "", KamokuName02 = "", KamokuName03 = "",
			EMSUnitCode = "";

	int id = 5001;
	String codeIdSelect = "";
	String codeold = "";
	String note = "";
	String nodeold = "";
	static Calendar c = Calendar.getInstance();
	static TextView txtTime;
	boolean isEx0 = false;
	boolean isEx1 = false;
	boolean isEx2 = false;
	boolean isEx3 = false;
	boolean dShow = false;
	static String MovementTimeCode;
	Button btnE5Expand0;
	Button btnE5Expand1;
	Button btnE5Expand2;
	Button btnE5Expand3;
	HashMap<String, String> listDate;
	static Context con;
	// String MICode = "";
	String ContactCount;
	int mycountId = 1;
	boolean isShow15 = false;
	LocationManager locationManager;
	Location location;
	// String tower;
	private boolean gps_enabled = false;
	private boolean network_enabled = false;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	boolean refesh = true;// Q065
	EditText txtJianNo;
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean InstructorUseFlg = false;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E005.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute005);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		listDate = new HashMap<String, String>();
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		JianNo = b.getString(TR_CaseCount.JianNo);

		// Bug 287 20121103
		// oldData = b.getString(BaseCount.IsOldData);
		// Sile menu
		utl.setMenu_open(false);
		initSileMenu();

		oldData = "1";
		//
		con = this;
		Cursor cur = null;
		dShow = false;
		E022.ishow = false;
		refesh = false;// Q065 20121027
		isEx0 = false; // Q137
		try {

			// Bug 181 20121019
			locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			// Criteria cri = new Criteria();
			// tower = locationManager.getBestProvider(cri, false);
			// location = locationManager.getLastKnownLocation(tower);

			// createMenu();// menu
			getConfig();

			// Testmenu
			// TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			ScrollView layoutmain = (ScrollView) findViewById(R.id.scrollView1);
			layoutmain.setOnTouchListener(this);

			if (oldData.equals("1")) {
				CL_TRCase tr = new CL_TRCase(E005.this);
				Cursor cor = tr.getItemTRcase(FireStationCode, CaseCode);
				if (cor != null && cor.getCount() > 0) {
					codeIdSelect = cor.getString(cor
							.getColumnIndex(TR_CaseCount.NotTransportation_CD));
					codeold = codeIdSelect;
					note = cor
							.getString(cor
									.getColumnIndex(TR_CaseCount.NotTransportation_TEXT));
					nodeold = note;
					JianNo = cor.getString(cor
							.getColumnIndex(TR_CaseCount.JianNo));
				}
				Cl_MovementTime mo = new Cl_MovementTime(E005.this);
				cor = mo.getTR_MovementTimeAll(FireStationCode, CaseCode);
				if (cor != null && cor.getCount() > 0) {
					do {
						String MovementCode = cor.getString(cor
								.getColumnIndex("MovementCode"));
						String date = cor.getString(cor
								.getColumnIndex("Movement_DATETIME"));
						String hour = date.substring(8, 10);
						String minute = date.substring(10, 12);
						listDate.put(MovementCode, hour + ":" + minute);
					} while (cor.moveToNext());
				}
				cor.close();
			}
			String TransferFLG = "0";
			String ControlType = "1";
			this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Group1);
			TransferFLG = "0";
			ControlType = "2";
			this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Group2);
			TransferFLG = "0";
			ControlType = "3";
			this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Group3);
			// 20121006 : dai add : bug 043
			TransferFLG = "0";
			ControlType = "4";
			this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Group4);
			// end
			// chuyen chuyen 2 man hinh
			Cl_ContactResultRecord cl = new Cl_ContactResultRecord(this);
			cur = cl.countTR_ContactResultRecord00001(FireStationCode, CaseCode);
			Button btnShowE015 = (Button) findViewById(R.id.btnShowE015);
			btnShowE015.setOnTouchListener(this);
			Button btnE008 = (Button) findViewById(R.id.btnShowE008);
			btnE008.setOnTouchListener(this);
			btnE008.setOnClickListener(this);
			// LinearLayout ly15 = (LinearLayout)
			// findViewById(R.id.layoutShow15);
			// LinearLayout ly08 = (LinearLayout)
			// findViewById(R.id.layoutShow08);
			// MICode = "";//Bug 339
			if (cur != null && cur.getCount() > 0) {
				btnShowE015.setVisibility(View.VISIBLE);
				LinearLayout.LayoutParams param15 = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT, 120);
				param15.setMargins(0, 0, 0, 10);
				btnShowE015.setLayoutParams(param15);
				btnShowE015.setOnClickListener(this);
				// 20121006: Bug 013
				// btnE008.setEnabled(false);
				btnShowE015.setBackgroundResource(R.drawable.btnblack5);
				btnShowE015.setText("搬送キャンセル");
				btnShowE015.setGravity(Gravity.CENTER);
				isShow15 = true;
				// end
				SlideMenu.MICode = cur.getString(cur.getColumnIndex("MICode"));
				ContactCount = cur
						.getString(cur.getColumnIndex("ContactCount"));
				String flg = cur.getString(cur
						.getColumnIndex("InstructorUseFlg"));
				if (flg != null && flg.equals("1"))
					InstructorUseFlg = true;
			} else {

				// 20121006: Bug 013
				// btnE008.setOnClickListener(this);
				// btnShowE015.setEnabled(false);
				// btnShowE015.setOnClickListener(this);
				// btnShowE015.setBackgroundResource(R.drawable.btngreennext);
				// btnShowE015.setText("   観察");
				// btnShowE015.setGravity(Gravity.CENTER | Gravity.LEFT);
				LinearLayout.LayoutParams param15 = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT, 0);
				param15.setMargins(0, 0, 0, 0);
				btnShowE015.setLayoutParams(param15);
				btnShowE015.setVisibility(View.INVISIBLE);
				isShow15 = false;
			}
			// createMenu();// menu
			txtJianNo = (EditText) findViewById(R.id.txtJianNo);
			txtJianNo.setOnTouchListener(this);
			txtJianNo.setText(JianNo);
			txtJianNo.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable arg0) {
					// TODO Auto-generated method stub
					String jianno = txtJianNo.getText().toString();
					CL_TRCase cl = new CL_TRCase(E005.this);
					cl.updateColumsTRCase(FireStationCode, CaseCode,
							TR_CaseCount.JianNo, jianno);

				}
			});
			txtJianNo.setOnEditorActionListener(new OnEditorActionListener() {

				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_SEARCH
							|| actionId == EditorInfo.IME_ACTION_DONE
							|| event.getAction() == KeyEvent.ACTION_DOWN
							&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						String jianno = txtJianNo.getText().toString();
						if (jianno != null && !jianno.equals("")) {
							CL_TRCase clt = new CL_TRCase(E005.this);
							boolean res = clt.checkJianNo(FireStationCode,
									CaseCode, jianno);
							if (res) {
								CL_Message ms = new CL_Message(E005.this);
								String mg = ms
										.getMessage(ContantMessages.JianNoExist);
								E022.showErrorDialog(E005.this, mg,
										ContantMessages.JianNoExist, false);
								return true;
							}
						}

						@SuppressWarnings("static-access")
						InputMethodManager inputManager = (InputMethodManager) getSystemService(E005.this.INPUT_METHOD_SERVICE);
						inputManager.hideSoftInputFromWindow(E005.this
								.getCurrentFocus().getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
						return true;
					}
					return false;
				}
			});

			String casecode = CaseCode;
			TextView txtCasecode = (TextView) findViewById(R.id.txtCaseCode);
			txtCasecode.setOnTouchListener(this);
			if (casecode.length() == 22) {

				casecode = casecode.substring(0, 10) + "-"
						+ casecode.substring(10, 18) + "-"
						+ casecode.substring(18, 22);
			}
			txtCasecode.setText(casecode);
			Button btnShowE07 = (Button) findViewById(R.id.btnShowE007);
			btnShowE07.setOnClickListener(this);
			btnE5Expand0 = (Button) findViewById(R.id.btnE5Expand0);
			btnE5Expand0.setOnClickListener(this);
			btnE5Expand0.setOnTouchListener(this);
			loadJian();
			btnE5Expand1 = (Button) findViewById(R.id.btnE5Expand1);
			btnE5Expand1.setOnClickListener(this);
			btnE5Expand1.setOnTouchListener(this);
			btnE5Expand2 = (Button) findViewById(R.id.btnE5Expand2);
			btnE5Expand2.setOnClickListener(this);
			btnE5Expand2.setOnTouchListener(this);
			btnE5Expand3 = (Button) findViewById(R.id.btnE5Expand3);
			btnE5Expand3.setOnClickListener(this);
			btnE5Expand3.setOnTouchListener(this);
			Button btnE5Back = (Button) findViewById(R.id.btnE5Back);
			btnE5Back.setOnClickListener(this);
			btnE5Back.setOnTouchListener(this);
			Button btnE027 = (Button) findViewById(R.id.btnE027);
			btnE027.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten4 = new Intent(v.getContext(), E027.class);
					Bundle b4 = new Bundle();
					b4.putString("FireStationCode", FireStationCode);
					b4.putString("CaseCode", CaseCode);
					myinten4.putExtras(b4);
					startActivityForResult(myinten4, 0);
				}
			});
			btnE027.setOnTouchListener(this);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E005.this, e.getMessage(), "E005", true);
		} finally {
			cur.close();
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);
		// locationManager.removeUpdates(this);// Bug 181

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// if (notificationManager!= null) {
		// notificationManager.cancel(GPSNotifiID);
		// }
		locationManager.removeUpdates(this);// Bug 181
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(E005.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// SharedPreferences sh = this.getSharedPreferences("app",
		// this.MODE_PRIVATE);
		E034.SetActivity(this);
		String syndb = getStatusSysDB();// sh.getString("SysDB", "0");
		if (refesh && syndb.equals("0")) {

			onCreate(null);
		}
		refesh = true;// Q065 20121027
		th.showConfirmAuto(this, FireStationCode, CaseCode);

		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		// Bug 166
		try {
			gps_enabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}
		// locationManager.requestLocationUpdates(tower, 0, 0, this);// Bug 181
		if (gps_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, this);
		}
		if (network_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 0, 0, this);
		}
		// End
	}

	private void getAdmisstion00001Info(String TerminalNo) {
		Cursor cur = null;
		try {
			Cl_ContactResultRecord cl = new Cl_ContactResultRecord(this);
			cur = cl.getTR_MedicalInstitutionAdmission00001(FireStationCode,
					CaseCode, SlideMenu.MICode, TerminalNo);
			if (cur != null && cur.getCount() > 0) {
				EMSUnitCode = cur.getString(cur.getColumnIndex("EMSUnitCode"));
				KamokuName01 = cur
						.getString(cur.getColumnIndex("KamokuName01"));
				KamokuName02 = cur
						.getString(cur.getColumnIndex("KamokuName02"));
				KamokuName03 = cur
						.getString(cur.getColumnIndex("KamokuName03"));
			}
		} catch (Exception ex) {
			E022.saveErrorLog(ex.getMessage(), this);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	// Bug 149
	private void confirmDeleteTime() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtTime.setVisibility(View.INVISIBLE);
				// img1.setVisibility(View.INVISIBLE);
				Cl_MovementTime trmo = new Cl_MovementTime(E005.this);
				trmo.deleteTR_MovementTime(FireStationCode, CaseCode,
						MovementTimeCode);
				//
				if (listDate.containsKey(MovementTimeCode)) {
					listDate.remove(MovementTimeCode);
				}
				if (MovementTimeCode.equals("00001")) {
					CL_TRCase tr = new CL_TRCase(E005.this);
					tr.updateSitearrival_datetime(FireStationCode, CaseCode, "");

				} else if (MovementTimeCode.equals("00005")
						|| MovementTimeCode.equals("00007")
						|| MovementTimeCode.equals("00009")
						|| MovementTimeCode.equals("00011")) {
					trmo = new Cl_MovementTime(E005.this);
					int num = trmo.countTR_MovementTimeDoctor(FireStationCode,
							CaseCode);
					if (num == 0) {
						Cl_ContactResultRecord ter = new Cl_ContactResultRecord(
								E005.this);
						String TerminalNo = ter.getTerminalNo();
						ter = new Cl_ContactResultRecord(E005.this);
						int res = ter
								.deleteTR_Admisstion(FireStationCode, CaseCode,
										SlideMenu.MICode, TerminalNo, "00002");
						if (res > 0) {
							CL_TRCase tr = new CL_TRCase(E005.this);
							tr.updateDoctortakeover_datetime(FireStationCode,
									CaseCode, "");
							try {
								E015 up = new E015();
								JSONObject json = new JSONObject();
								json.put("FireStationCode", FireStationCode);
								json.put("CaseCode", CaseCode);
								json.put("ContactCount", "");
								json.put("MICode", SlideMenu.MICode);
								json.put("ContactResult_CD", "00002");
								json.put("ContactResult_TEXT", "");
								json.put("Flag", "3");
								String val = json.toString();
								up.upLoadDataToServer(val, E005.this);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								E022.showErrorDialog(E005.this, e.getMessage(),
										"E005", true);
							}
						}
					}
				}
				dialog.cancel();
			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteItem);
		lbmg.setText(mg);

		dialog.show();
	}

	private void loadGroup1(String TransferFLG, String ControlType, int layoutId) {
		// String TransferFLG = "0";
		// String ControlType = "1";
		Cl_MovementTime mo = new Cl_MovementTime(E005.this);
		Cursor cor = mo.getMS_MovementTime(FireStationCode, TransferFLG,
				ControlType);
		try {
			if (cor != null && cor.getCount() > 0) {
				LinearLayout layoutGroup1 = (LinearLayout) findViewById(layoutId);
				HashMap<String, String> list1 = new HashMap<String, String>();
				int id = 1;
				do {
					final String MovementTimeCode_CD = cor.getString(cor
							.getColumnIndex("MovementTimeCode_CD"));
					String MovementTimeName = cor.getString(cor
							.getColumnIndex("MovementTimeName"));
					list1.put(String.valueOf(id), MovementTimeCode_CD);
					// Bug 171
					// LinearLayout.LayoutParams paramg1 = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.MATCH_PARENT, 90);
					LinearLayout.LayoutParams paramg1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, 126);
					paramg1.setMargins(0, 15, 0, 15);
					final LinearLayout layout1 = new LinearLayout(E005.this);
					layout1.setOrientation(LinearLayout.HORIZONTAL);
					layout1.setLayoutParams(paramg1);
					layout1.setGravity(Gravity.LEFT);
					// LinearLayout.LayoutParams parambtn1 = new
					// LinearLayout.LayoutParams(
					// 270, 90);
					LinearLayout.LayoutParams parambtn1 = new LinearLayout.LayoutParams(
							280, 120);
					parambtn1.setMargins(3, 0, 3, 0);
					TextView btn1 = new TextView(this);
					btn1.setLayoutParams(parambtn1);
					btn1.setText("   " + MovementTimeName);
					btn1.setBackgroundResource(R.drawable.btngreye5);
					btn1.setTextColor(Color.WHITE);
					btn1.setId(id);
					// btn1.setWidth(100);
					// btn1.setHeight(45);
					// Bug 90
					// btn1.setTextSize(20);
					btn1.setTextSize(24);
					//
					btn1.setGravity(Gravity.LEFT | Gravity.CENTER);
					// LinearLayout.LayoutParams paramtxt1 = new
					// LinearLayout.LayoutParams(
					// 272, 78);
					LinearLayout.LayoutParams paramtxt1 = new LinearLayout.LayoutParams(
							270, 100);
					paramtxt1.setMargins(10, 0, 0, 0);
					final TextView txt1 = new TextView(this);
					txt1.setLayoutParams(paramtxt1);
					txt1.setTextColor(Color.WHITE);
					if (oldData.equals("1")
							&& listDate.containsKey(MovementTimeCode_CD)) {
						txt1.setText(listDate.get(MovementTimeCode_CD));
						txt1.setVisibility(View.VISIBLE);
					} else {
						txt1.setText("00:00");
						txt1.setVisibility(View.INVISIBLE);
					}
					txt1.setGravity(Gravity.CENTER);
					txt1.setBackgroundResource(R.drawable.txttime);
					txt1.setTextSize(24);
					// LinearLayout.LayoutParams paramImg = new
					// LinearLayout.LayoutParams(
					// LinearLayout.LayoutParams.WRAP_CONTENT,
					// LinearLayout.LayoutParams.WRAP_CONTENT);
					// final ImageButton img1 = new ImageButton(this);
					// img1.setLayoutParams(paramImg);
					// img1.setBackgroundResource(R.drawable.iclock);
					// img1.setVisibility(View.INVISIBLE);
					layout1.addView(btn1);
					layout1.addView(txt1);
					// layout1.addView(img1);
					layoutGroup1.addView(layout1);
					id++;
					txt1.setOnTouchListener(this);
					btn1.setOnTouchListener(this);
					btn1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							// if (mMenu.isShowing()) {
							// mMenu.hide();
							// }
							if (txt1.getVisibility() == View.INVISIBLE) {
								// Bug 152
								int tmp = 0;
								try {
									tmp = Integer.parseInt(MovementTimeCode_CD);
								} catch (Exception ex) {

								}
								if (!isShow15
										&& (tmp == 5 || tmp == 7 || tmp == 9 || tmp == 11)) {
									E022.showErrorDialog(E005.this,
											ContantMessages.NotSelectHopital,
											ContantMessages.NotSelectHopital,
											false);
									return;
								}
								if (tmp == 5 || tmp == 7 || tmp == 9
										|| tmp == 11) {
									Cl_ContactResultRecord ter = new Cl_ContactResultRecord(
											E005.this);
									String TerminalNo = ter.getTerminalNo();
									getAdmisstion00001Info(TerminalNo);
									ter = new Cl_ContactResultRecord(E005.this);
									long res = ter
											.insertTR_MedicalInstitutionAdmission(
													FireStationCode, CaseCode,
													SlideMenu.MICode,
													TerminalNo, "00002",
													EMSUnitCode, KamokuName01,
													KamokuName02, KamokuName03);
									if (res > 0) {
										CL_TRCase tr = new CL_TRCase(E005.this);
										tr.updateDoctortakeover_datetime(
												FireStationCode, CaseCode,
												Utilities.getDateTimeNow());
										// E015 up = new E015();
										// JSONObject json = new JSONObject();
										// json.put("FireStationCode",
										// FireStationCode);
										// json.put("CaseCode", CaseCode);
										// json.put("ContactCount", "");
										// json.put("MICode", MICode);
										// json.put("ContactResult_CD",
										// "00002");
										// json.put("ContactResult_TEXT", "");
										// json.put("EMSUnitCode", EMSUnitCode);
										// json.put("KamokuName01",
										// KamokuName01);
										// json.put("KamokuName02",
										// KamokuName02);
										// json.put("KamokuName03",
										// KamokuName03);
										// json.put("Movement_DATETIME",
										// Utilities.getDateTimeNow());
										// json.put("Flag", "0");
										// String val = json.toString();
										// up.upLoadDataToServer(val,
										// E005.this);
										E005Doctor up = new E005Doctor();
										up.UpdateLoad(E005.this,
												FireStationCode, CaseCode);
									}
								}
								//
								c = Calendar.getInstance();
								String text = formatNumber(c
										.get(Calendar.HOUR_OF_DAY))
										+ ":"
										+ formatNumber(c.get(Calendar.MINUTE));
								txt1.setText(text);
								txt1.setVisibility(View.VISIBLE);
								// img1.setVisibility(View.VISIBLE);
								String date = Utilities.getDateNow()
										+ txt1.getText().toString()
												.replace(":", "") + "00";
								Cl_MovementTime trmo = new Cl_MovementTime(
										E005.this);
								trmo.createTR_MovementTime(FireStationCode,
										CaseCode, MovementTimeCode_CD, date);
								//
								listDate.put(MovementTimeCode_CD, text);

								oldData = "1";// Bug 155
								// 20121006 dai add: bug 80
								if (MovementTimeCode_CD.equals("00001")) {
									startThread();
									// MyAsyncTask task = new MyAsyncTask();
									// task.execute();
									CL_TRCase tr = new CL_TRCase(E005.this);
									tr.updateSitearrival_datetime(
											FireStationCode, CaseCode,
											Utilities.getDateTimeNow());
								}
								// end

							} else {
								// String date = Utilities.getDateNow()
								// + txt1.getText().toString();
								// cl.updateTR_MovementTime(FireStationCode,
								// CaseCode, MovementTimeCode_CD, date);
								// Bug 152
								int tmp = 0;
								try {
									tmp = Integer.parseInt(MovementTimeCode_CD);
								} catch (Exception ex) {

								}
								if (!isShow15
										&& (tmp == 5 || tmp == 7 || tmp == 9 || tmp == 11)) {
									E022.showErrorDialog(E005.this,
											ContantMessages.NotSelectHopital,
											ContantMessages.NotSelectHopital,
											false);
									return;
								}

								//
								txtTime = txt1;
								MovementTimeCode = MovementTimeCode_CD;
								// showTimePickerDialog1(v);
								if (!dShow) {
									dShow = true;
									showTimer();
								}
							}
						}
					});
					txt1.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							txtTime = txt1;
							MovementTimeCode = MovementTimeCode_CD;
							// showTimePickerDialog1(v);
							if (!dShow) {
								dShow = true;
								showTimer();
							}
						}
					});
					txt1.setOnLongClickListener(new OnLongClickListener() {

						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							txtTime = txt1;
							MovementTimeCode = MovementTimeCode_CD;
							confirmDeleteTime();
							// AlertDialog.Builder b = new AlertDialog.Builder(
							// E005.this);
							// CL_Message me = new CL_Message(E005.this);
							// String mg = me
							// .getMessage(ContantMessages.DeleteItem);
							// b.setMessage(mg);
							// b.setNegativeButton("No",
							// new DialogInterface.OnClickListener() {
							//
							// public void onClick(
							// DialogInterface dialog,
							// int which) {
							// // TODO Auto-generated method stub
							// dialog.cancel();
							// }
							// });
							// b.setPositiveButton("Yes",
							// new DialogInterface.OnClickListener() {
							//
							// public void onClick(
							// DialogInterface dialog,
							// int which) {
							// // TODO Auto-generated method stub
							// txt1.setVisibility(View.INVISIBLE);
							// // img1.setVisibility(View.INVISIBLE);
							// Cl_MovementTime trmo = new Cl_MovementTime(
							// E005.this);
							// trmo.deleteTR_MovementTime(
							// FireStationCode, CaseCode,
							// MovementTimeCode_CD);
							// //
							// if (listDate
							// .containsKey(MovementTimeCode_CD)) {
							// listDate.remove(MovementTimeCode_CD);
							// }
							// }
							// });
							// AlertDialog a = b.create();
							// a.show();
							return true;
						}
					});

				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E005.this, e.getMessage(), "E005", true);
		} finally {
			cor.close();
		}
	}

	// 20121006 daitran add : bug 161
	// class MyAsyncTask extends AsyncTask<Void, Integer, Void> {
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// // TODO Auto-generated method stub
	//
	// return null;
	// }
	//
	// // 20121018 bug 167
	// private boolean isOnline() {
	// ConnectivityManager cm = (ConnectivityManager) con
	// .getSystemService(Context.CONNECTIVITY_SERVICE);
	// NetworkInfo netInfo = cm.getActiveNetworkInfo();
	// if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	// return true;
	// }
	// return false;
	// }//
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// try {
	// LocationManager locationManager;
	// locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	// Criteria cri = new Criteria();
	// String tower = locationManager.getBestProvider(cri, false);
	// // Location location =
	// // locationManager.getLastKnownLocation(tower);
	// LocationListener locationListenerGps = new LocationListener() {
	// public void onLocationChanged(Location location) {
	//
	// if (location != null) {
	// double lat = location.getLatitude();
	// double longi = location.getLongitude();
	// String addressjian = "";
	// String country = "";
	// if (isOnline()) {
	// GeoPoint p;
	// Geocoder geocoder = new Geocoder(
	// getBaseContext(), Locale.getDefault());
	// p = new GeoPoint(
	// (int) (location.getLatitude() * 1E6),
	// (int) (location.getLongitude() * 1E6));
	// List<Address> add = null;
	// try {
	// add = geocoder.getFromLocation(
	// p.getLatitudeE6() / 1E6,
	// p.getLongitudeE6() / 1E6, 1);
	// } catch (Exception ex) {
	// }
	// if (add != null && add.size() > 0) {
	// // Get address
	// String post = "";
	// for (int i = 0; i <= add.get(0)
	// .getMaxAddressLineIndex(); i++) {
	// country = add.get(0).getCountryName();
	// post = add.get(0).getPostalCode();
	// if (!add.get(0).getAddressLine(i)
	// .equals(country)) {
	// if (addressjian != null
	// && !addressjian.equals("")) {
	// addressjian = addressjian
	// + ", ";
	// }
	// addressjian += add.get(0)
	// .getAddressLine(i);
	// }
	// }
	// if (post != null
	// && addressjian.contains(post)) {
	// addressjian = addressjian.replace(post,
	// "");
	// }
	// }
	// }
	// CL_TRCase cl = new CL_TRCase(E005.this);
	// // Bug 160 Remove country
	// cl.updateAddressJIAN(FireStationCode, CaseCode,
	// String.valueOf(lat), String.valueOf(longi),
	// addressjian);
	// }
	// }
	//
	// public void onProviderDisabled(String provider) {
	// }
	//
	// public void onProviderEnabled(String provider) {
	// }
	//
	// public void onStatusChanged(String provider, int status,
	// Bundle extras) {
	// }
	// };
	//
	// locationManager.requestLocationUpdates(tower, 0, 0,
	// locationListenerGps);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	//
	// }
	// }
	//
	// }

	// 20121018 bug 167
	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}//

	// 20121006 daitran add : bug 80
	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			getAddressJian();
		}

	}

	private void getAddressJian() {
		try {
			// LocationManager locationManager;
			GeoPoint p;
			// locationManager = (LocationManager)
			// getSystemService(LOCATION_SERVICE);
			// Criteria cri = new Criteria();
			// String tower = locationManager.getBestProvider(cri, false);
			// Location location = locationManager.getLastKnownLocation(tower);
			if (location != null) {
				double lat = location.getLatitude();
				double longi = location.getLongitude();
				String addressjian = "";
				String country = "";
				if (isOnline()) {
					Geocoder geocoder = new Geocoder(getBaseContext(),
							Locale.getDefault());
					p = new GeoPoint((int) (location.getLatitude() * 1E6),
							(int) (location.getLongitude() * 1E6));
					List<Address> add = null;
					try {
						add = geocoder.getFromLocation(p.getLatitudeE6() / 1E6,
								p.getLongitudeE6() / 1E6, 1);
					} catch (Exception ex) {
					}
					if (add != null && add.size() > 0) {
						// Get address
						String post = "";
						for (int i = 0; i <= add.get(0)
								.getMaxAddressLineIndex(); i++) {
							country = add.get(0).getCountryName();
							post = add.get(0).getPostalCode();
							if (!add.get(0).getAddressLine(i).equals(country)) {
								if (addressjian != null
										&& !addressjian.equals("")) {
									addressjian = addressjian + ", ";
								}
								addressjian += add.get(0).getAddressLine(i);
							}
						}
						if (post != null && addressjian.contains(post)) {
							addressjian = addressjian.replace(post, "");
						}
					}
				}
				if (addressjian != null && !addressjian.equals("")
						&& !addressjian.equals("〒")) {
					CL_TRCase cl = new CL_TRCase(E005.this);
					// Bug 160 Remove country
					cl.updateAddressJIAN(FireStationCode, CaseCode,
							String.valueOf(lat), String.valueOf(longi),
							addressjian);
				}
			}
		} catch (Exception ex) {

		} finally {

		}
	}

	// end
	// public void showTimePickerDialog1(View v) {
	//
	// DialogFragment newFragment = new TimePickerFragment();
	// newFragment.show(getFragmentManager(), "timePicker");
	//
	// }

	// public static class TimePickerFragment extends DialogFragment implements
	// TimePickerDialog.OnTimeSetListener {
	//
	// @Override
	// public Dialog onCreateDialog(Bundle savedInstanceState) {
	// // Use the current time as the default values for the picker
	// // final Calendar c = Calendar.getInstance();
	// String time = txtTime.getText().toString();
	// String[] s = time.split(":");
	// int hour = Integer.parseInt(s[0]);
	// int minute = Integer.parseInt(s[1]);
	//
	// // Create a new instance of TimePickerDialog and return it
	// // 24h
	// final TimePickerDialog timer = new TimePickerDialog(getActivity(),
	// this, hour, minute, true);
	// // String text = formatNumber(hour) + ":" + formatNumber(minute);
	// timer.setTitle("時刻編集");
	// timer.setIcon(R.drawable.o);
	// timer.setButton(TimePickerDialog.BUTTON_NEGATIVE, "OK", timer);
	// timer.setButton(TimePickerDialog.BUTTON_POSITIVE, "Cancel",
	// new DialogInterface.OnClickListener() {
	//
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// timer.cancel();
	// }
	//
	// });
	//
	// return timer;
	// // 12h
	// // return new TimePickerDialog(getActivity(), this, hour, minute,
	// // DateFormat.is24HourFormat(getActivity()));
	// }
	//
	// public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	// // Do something with the time chosen by the user
	// c.set(Calendar.HOUR_OF_DAY, hourOfDay);
	// c.set(Calendar.MINUTE, minute);
	// String text = setTimer(hourOfDay, minute);
	// txtTime.setText(text);
	// updateDateTime(hourOfDay, minute);
	// }
	// }

	private void updateDateTime(int hour, int minute) {
		String date = Utilities.getDateNow() + Utilities.formatNumber(hour)
				+ Utilities.formatNumber(minute) + "00";
		Cl_MovementTime trmo = new Cl_MovementTime(con);
		trmo.updateTR_MovementTime(FireStationCode, CaseCode, MovementTimeCode,
				date);
		//
		//

		listDate.put(MovementTimeCode, Utilities.formatNumber(hour) + ":"
				+ Utilities.formatNumber(minute));

		// 20121006 dai add: bug 80
		if (MovementTimeCode.equals("00001")) {
			// Bug 161
			startThread();
			// MyAsyncTask task = new MyAsyncTask();
			// task.execute();
			//
		}
		// end

	}

	private boolean checkVersion41() {
		try {
			if (Double.parseDouble(Build.VERSION.RELEASE.substring(0, 3)) < 4.1) {
				return false;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return true;
	}

	private void showTimer() {
		try {
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute006);
			dialog.setCancelable(false);
			final TimePicker timer = (TimePicker) dialog
					.findViewById(R.id.timePicker1);
			timer.setIs24HourView(true);
			if (checkVersion41() == false) {
				ViewGroup v = (ViewGroup) timer.getChildAt(0);
				ViewGroup numberPicker1 = (ViewGroup) v.getChildAt(0);
				ViewGroup numberPicker2 = (ViewGroup) v.getChildAt(1);
				EditText txthours = ((EditText) numberPicker1.getChildAt(1));
				EditText txtmins = ((EditText) numberPicker2.getChildAt(1));
				txthours.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						2) });
				txtmins.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						2) });
			}
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// dialog.cancel();
					dShow = false;
					dialog.dismiss();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOk);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Update E007
					dShow = false;
					timer.clearFocus();
					c.set(Calendar.HOUR_OF_DAY, timer.getCurrentHour());
					c.set(Calendar.MINUTE, timer.getCurrentMinute());

					String text = setTimer(timer.getCurrentHour(),
							timer.getCurrentMinute());
					txtTime.setText(text);
					updateDateTime(timer.getCurrentHour(),
							timer.getCurrentMinute());
					if (MovementTimeCode.equals("00005")
							|| MovementTimeCode.equals("00007")
							|| MovementTimeCode.equals("00009")
							|| MovementTimeCode.equals("00011")) {
						Cl_ContactResultRecord ter = new Cl_ContactResultRecord(
								E005.this);
						String TerminalNo = ter.getTerminalNo();
						getAdmisstion00001Info(TerminalNo);
						ter = new Cl_ContactResultRecord(E005.this);
						long res = ter.insertTR_MedicalInstitutionAdmission(
								FireStationCode, CaseCode, SlideMenu.MICode,
								TerminalNo, "00002", EMSUnitCode, KamokuName01,
								KamokuName02, KamokuName03);
						if (res > 0) {
							// E015 up = new E015();
							// JSONObject json = new JSONObject();
							// json.put("FireStationCode", FireStationCode);
							// json.put("CaseCode", CaseCode);
							// json.put("ContactCount", "");
							// json.put("MICode", MICode);
							// json.put("ContactResult_CD", "00002");
							// json.put("ContactResult_TEXT", "");
							// json.put("EMSUnitCode",EMSUnitCode);
							// json.put("KamokuName01", KamokuName01);
							// json.put("KamokuName02",KamokuName02);
							// json.put("KamokuName03",KamokuName03);
							// json.put("Movement_DATETIME",Utilities.getDateTimeNow());
							// json.put("Flag", "0");
							// String val = json.toString();
							// up.upLoadDataToServer(val, E005.this);
							E005Doctor up = new E005Doctor();
							up.UpdateLoad(E005.this, FireStationCode, CaseCode);
						}
					}
					// dialog.cancel();
					dialog.dismiss();
				}
			});
			String time = txtTime.getText().toString();
			String[] s = time.split(":");
			int hour = Integer.parseInt(s[0]);
			int minute = Integer.parseInt(s[1]);
			timer.setCurrentHour(hour);
			timer.setCurrentMinute(minute);
			dialog.show();
		} catch (Exception e) {
			E022.showErrorDialog(E005.this, e.getMessage(), "E005", true);
		}
	}

	private static String setTimer(int hour, int minute) {
		return formatNumber(hour) + ":" + formatNumber(minute);
	}

	private static String formatNumber(int num) {
		if (num < 10) {
			return "0" + String.valueOf(num);
		}
		return String.valueOf(num);
	}

	private void loadJian() {
		LinearLayout layo = (LinearLayout) findViewById(R.id.layoutE5Expand0);
		layo.setOnTouchListener(this);
		if (isEx0) {
			LayoutParams param = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
			layo.setLayoutParams(param);
			layo.setVisibility(View.VISIBLE);
			btnE5Expand0.setBackgroundResource(R.drawable.btngreyup);
			isEx0 = false;
		} else {
			String jianno = txtJianNo.getText().toString();
			if (jianno != null && !jianno.equals("")) {
				CL_TRCase clt = new CL_TRCase(E005.this);
				boolean res = clt
						.checkJianNo(FireStationCode, CaseCode, jianno);
				if (res) {
					CL_Message ms = new CL_Message(E005.this);
					String mg = ms.getMessage(ContantMessages.JianNoExist);
					E022.showErrorDialog(E005.this, mg,
							ContantMessages.JianNoExist, false);

					return;
				}
			}
			LayoutParams param = new LayoutParams(LayoutParams.MATCH_PARENT, 0);
			layo.setLayoutParams(param);
			layo.setVisibility(View.INVISIBLE);
			btnE5Expand0.setBackgroundResource(R.drawable.btngreydown);
			isEx0 = true;
		}
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnE5Expand0:
			loadJian();
			break;
		case R.id.btnShowE007:
			if (isShow15) {
				E022.showErrorDialog(E005.this, ContantMessages.DeleteMiCode,
						ContantMessages.DeleteMiCode, false);
			} else {
				if (!dShow) {
					dShow = true;
					showE007();
				}
			}
			break;
		// case R.id.btnE008: //Bug -013
		// Intent myinten = new Intent(v.getContext(), E008.class);
		// Bundle b = new Bundle();
		// b.putString("FireStationCode", FireStationCode);
		// b.putString("CaseCode", CaseCode);
		// myinten.putExtras(b);
		// startActivityForResult(myinten, 0);
		// break;
		case R.id.btnE5Expand1:
			if (!isEx1) {
				isEx1 = true;
				String TransferFLG = "1";
				String ControlType = "1";
				this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Expand1);
				btnE5Expand1.setBackgroundResource(R.drawable.btngreyup);
			} else {
				isEx1 = false;
				hideLayout(R.id.layoutE5Expand1);
				btnE5Expand1.setBackgroundResource(R.drawable.btngreydown);
			}
			break;
		case R.id.btnE5Expand2:
			if (!isEx2) {
				isEx2 = true;
				String TransferFLG = "1";
				String ControlType = "2";
				this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Expand2);
				btnE5Expand2.setBackgroundResource(R.drawable.btngreyup);
			} else {
				isEx2 = false;
				hideLayout(R.id.layoutE5Expand2);
				btnE5Expand2.setBackgroundResource(R.drawable.btngreydown);
			}
			break;
		case R.id.btnE5Expand3:
			if (!isEx3) {
				isEx3 = true;
				String TransferFLG = "1";
				String ControlType = "3";
				this.loadGroup1(TransferFLG, ControlType, R.id.layoutE5Expand3);
				btnE5Expand3.setBackgroundResource(R.drawable.btngreyup);
			} else {
				isEx3 = false;
				hideLayout(R.id.layoutE5Expand3);
				btnE5Expand3.setBackgroundResource(R.drawable.btngreydown);
			}
			break;
		case R.id.btnE5Back:
			Intent intent = new Intent(v.getContext(), E002.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.btnShowE015:
			if (isShow15) {
				// showE015();
				// if (mMenu.isShowing()) {
				// mMenu.hide();
				// }
				showE015Cancel();
			} else {
				// if (codeold != null && !codeold.equals("")) {
				// confirmDeletePatient();
				//
				// } else {
				// // Add bug:013
				// Intent myinten = new Intent(v.getContext(), E008.class);
				// Bundle b = new Bundle();
				// b.putString("FireStationCode", FireStationCode);
				// b.putString("CaseCode", CaseCode);
				// myinten.putExtras(b);
				// startActivityForResult(myinten, 0);
				// }

			}
			break;
		case R.id.btnShowE008:
			if (codeold != null && !codeold.equals("")) {
				confirmDeletePatient();

			} else {
				// Add bug:013
				Intent myinten = new Intent(v.getContext(), E008.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putBoolean("isShow15", isShow15);
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
			}
			break;
		default:
			break;
		}
	}

	private void confirmDeletePatient() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				CL_TRCase tr = new CL_TRCase(E005.this);
				tr.updateE007TRCase(FireStationCode, CaseCode, "", "");
				Intent myinten = new Intent(v.getContext(), E008.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putBoolean("isShow15", isShow15);
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();
				codeold = "";

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.ConfirmCancelPatient);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	private void hideLayout(int LayoutId) {
		LinearLayout layoutEx = (LinearLayout) findViewById(LayoutId);
		layoutEx.removeAllViews();
	}

	private void showE007() {
		try {
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute007);
			dialog.setCancelable(false);//
			Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					codeIdSelect = "";
					// dialog.cancel();
					dShow = false;
					dialog.dismiss();
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Update E007
					dShow = false;
					if (codeIdSelect != null && !codeIdSelect.equals("")) {
						codeold = codeIdSelect;
						if (codeIdSelect.equals(BaseCount.Other)) {
							EditText txtother = (EditText) dialog
									.findViewById(99999);
							note = txtother.getText().toString();
							nodeold = note;
						}
						CL_TRCase tr = new CL_TRCase(E005.this);
						tr.updateE007TRCase(FireStationCode, CaseCode,
								codeIdSelect, note);
						oldData = "1";
						// dialog.cancel();
						dialog.dismiss();
					}
				}
			});
			// dialog.setTitle("不搬送理由");
			// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
			// R.drawable.o);
			Cl_Code code = new Cl_Code(E005.this);
			Cursor cor = code.getMS_Code("00006");
			if (cor != null && cor.getCount() > 0) {
				id = 1;

				LinearLayout layoutE71 = (LinearLayout) dialog
						.findViewById(R.id.layoutE71);
				LinearLayout layoutE72 = (LinearLayout) dialog
						.findViewById(R.id.layoutE72);
				layoutE71.setOrientation(LinearLayout.VERTICAL);
				layoutE72.setOrientation(LinearLayout.HORIZONTAL);
				final HashMap<String, String> listItem = new HashMap<String, String>();
				do {
					String codeId = cor.getString(cor.getColumnIndex("CodeID"));
					String codeName = cor.getString(cor
							.getColumnIndex("CodeName"));

					LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT, 60);
					paramCheck.setMargins(40, 15, 0, 15);
					final RadioButton rdi = new RadioButton(this);
					rdi.setLayoutParams(paramCheck);
					rdi.setText(codeName);
					rdi.setId(id);
					rdi.setTextColor(Color.parseColor("#777777"));
					rdi.setButtonDrawable(R.drawable.radio);
					rdi.setTextSize(24);
					// No default
					// if (oldData.equals("0") &&
					// String.valueOf(id).equals("2")) {
					// rdi.setChecked(true);
					// codeIdSelect = codeId;
					// }
					if (oldData.equals("1") && codeId.equals(codeold)) {
						rdi.setChecked(true);
						codeIdSelect = codeold;
					}
					listItem.put(String.valueOf(id), codeId);
					id++;
					rdi.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							for (int i = 1; i < id; i++) {
								RadioButton radio = (RadioButton) dialog
										.findViewById(i);
								radio.setChecked(false);

							}
							rdi.setChecked(true);
							codeIdSelect = (String) listItem.get(String
									.valueOf(rdi.getId()));
							// 20121008 Bug 079
							EditText txtother = (EditText) dialog
									.findViewById(99999);
							txtother.setEnabled(false);
							if (codeIdSelect.equals("99999")) {

								txtother.setEnabled(true);
							} else {
								txtother.setText("");
							}
							// end
						}
					});

					if (codeId.equals("99999")) {

						LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						// paramtext.setMargins(0, 0, 5, 0);
						paramtext.setMargins(0, 0, 15, 0);
						EditText txtOther = new EditText(this);
						txtOther.setLayoutParams(paramtext);
						txtOther.setBackgroundResource(R.drawable.inpute7);
						txtOther.setTextColor(Color.BLACK);
						txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
								200) });
						// 20121008 Bug: 031
						txtOther.setSingleLine(true);
						//
						if (codeId.equals(codeIdSelect)) {
							txtOther.setText(nodeold);
						} else {
							txtOther.setEnabled(false);
						}
						txtOther.setId(99999);
						layoutE72.addView(rdi);
						layoutE72.addView(txtOther);
					} else {
						layoutE71.addView(rdi);
					}
				} while (cor.moveToNext());
				dialog.show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {

		}
	}

	// Bug 339 20121114
	String codeIdSelect15 = "";
	boolean chk15 = false;
	CheckBox chk;

	// private void showE015
	private void showE015Cancel() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute015);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		TextView textViewcontactcount = (TextView) dialog
				.findViewById(R.id.textViewcontactcount);
		textViewcontactcount.setText(ContactCount + "");
		Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!codeIdSelect15.equals("00001")) {
					String note = "";
					if (codeIdSelect15.equals(BaseCount.Other)) {
						EditText txtother = (EditText) dialog
								.findViewById(99999);
						note = txtother.getText().toString();
					}
					Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
							E005.this);
					long res = cl.updateTR_ContactResultRecord(FireStationCode,
							CaseCode, ContactCount, SlideMenu.MICode,
							codeIdSelect15, note, InstructorUseFlg);
					if (res >= 0) {
						try {
							E015 up = new E015();
							JSONObject json = new JSONObject();
							json.put("FireStationCode", FireStationCode);
							json.put("CaseCode", CaseCode);
							json.put("ContactCount", ContactCount);
							json.put("MICode", SlideMenu.MICode);
							json.put("ContactResult_CD", codeIdSelect15);
							json.put("ContactResult_TEXT", note);
							json.put("Flag", "1");
							String val = json.toString();
							up.upLoadDataToServer(val, E005.this);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							E022.showErrorDialog(E005.this, e.getMessage(),
									"E015", true);
						}
						// 20121006: daitran Bug 045
						Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
								E005.this);
						int count = clc.countTR_ContactResultRecord(
								FireStationCode, CaseCode);
						if (count == MNETCount) {
							dialog.cancel();
							// ShowE016();
							E015Dialog dg = new E015Dialog();
							dg.ShowE016(E005.this, FireStationCode, CaseCode,
									SlideMenu.MICode);
						} else {
							dialog.cancel();
						}
						//
						onCreate(null);

					}
				} else {
					dialog.cancel();
				}
			}
		});
		// dialog.setTitle("不搬送理由");
		// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
		// R.drawable.o);
		Cl_Code code = new Cl_Code(E005.this);
		Cursor cor = code.getMS_Code("00002");
		if (cor != null && cor.getCount() > 0) {
			mycountId = 1;
			final RadioGroup group = (RadioGroup) dialog
					.findViewById(R.id.radioGroup1);
			LinearLayout layout = (LinearLayout) dialog
					.findViewById(R.id.layoutE73);
			final HashMap<String, String> listItem = new HashMap<String, String>();

			do {
				final String codeId = cor.getString(cor
						.getColumnIndex("CodeID"));
				String codeName = cor.getString(cor.getColumnIndex("CodeName"));
				listItem.put(codeId, codeName);
				LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				paramCheck.setMargins(0, 10, 0, 10);
				RadioButton rdi = new RadioButton(this);
				rdi.setLayoutParams(paramCheck);
				rdi.setText(codeName);
				rdi.setTextColor(Color.BLACK);
				rdi.setButtonDrawable(R.drawable.radio);
				rdi.setTextSize(24);
				rdi.setId(mycountId);
				mycountId++;
				rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							if (!codeId.equals("00001")) {
								RadioButton radio = (RadioButton) dialog
										.findViewById(1);
								radio.setChecked(false);
								chk.setChecked(false);
								chk.setEnabled(false);

							} else {
								chk.setEnabled(true);
								group.clearCheck();
								// RadioButton radio = (RadioButton) dialog
								// .findViewById(1);
								// radio.setChecked(true);
							}
							codeIdSelect15 = codeId;
							// 20121008 Bug 079
							if (listItem.containsKey(BaseCount.Other)) {
								EditText txtother = (EditText) dialog
										.findViewById(99999);
								txtother.setEnabled(false);
								if (codeIdSelect15.equals("99999")) {

									txtother.setEnabled(true);
								} else {
									txtother.setText("");
								}
							}
							//
						}
					}
				});

				// listItem.put(String.valueOf(id), codeId);
				if (codeId.equals("00001")) {
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout lay = new LinearLayout(this);
					lay.setLayoutParams(paramrow);
					lay.setOrientation(LinearLayout.HORIZONTAL);
					chk = new CheckBox(this);
					chk.setLayoutParams(paramrow);
					chk.setTextColor(Color.BLACK);
					chk.setText("三次コーディネート");
					chk.setTextSize(16);
					chk.setChecked(InstructorUseFlg);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub
							chk15 = arg1;
							InstructorUseFlg = arg1;
						}
					});
					lay.addView(rdi);
					lay.addView(chk);
					group.addView(lay);
				} else {
					group.addView(rdi);
				}
				if (codeId.equals("00001")) {
					rdi.setChecked(true);
					codeIdSelect15 = codeId;

				}
				if (codeId.equals("99999")) {

					LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtext.setMargins(0, 5, 20, 0);
					EditText txtOther = new EditText(this);
					txtOther.setLayoutParams(paramtext);
					txtOther.setBackgroundResource(R.drawable.inpute7);
					txtOther.setTextColor(Color.BLACK);
					txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
							200) });
					// 20121008 Bug: 031
					txtOther.setSingleLine(true);
					txtOther.setEnabled(false);
					//
					txtOther.setId(99999);
					layout.addView(txtOther);
				}
			} while (cor.moveToNext());
			dialog.show();
		}

	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) { // Bug 004 +114
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
			} else {
				Intent intent = new Intent(E005.this, E002.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				dShow = false;
			}
			return true; // always eat it!
		}
		return super.onKeyDown(keyCode, event);
	}

	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}

	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

		if (arg0 != null)
			location = arg0;
	}

	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
}
