package jp.osaka;

import jp.osaka.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DialogsManager {

	public static void showDialog41_Ostrat(Context context) {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_41_ostrat);
		Button btnOK = (Button) dialog.findViewById(R.id.dialogButtonOK);
		Button btnCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// do st...
				dialog.dismiss();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	public static void showDialog42_Opat(Context context) {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_42_opat);
		Button btnOK = (Button) dialog.findViewById(R.id.dialogButtonOK);
		Button btnCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// do st...
				dialog.dismiss();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
