﻿package jp.osaka;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class E013C extends Activity implements LocationListener,
		OnTouchListener {
	ListView list;
	MyAdapter mydapter;
	String FireStationCode;
	String CaseCode;
	String SHCACode;
	ArrayList<DTO_MedicalInstitution> listItem;
	double lat = 0;
	double longi = 0;
	String destition = "";

	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";

	LocationManager locationManager;
	Location locationA;
	String tower;
	ProgressDialog p;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	int numberitem = 0;
	int numberDisplay = 0;
	int limit = 10;
	boolean refesh = true;// Q065
	int lastPosition = 0;
	int topPostion = 0;
	String HeaderBackcolorOnDisaster, HeaderBackcolorOnEmergency;
	HashMap<String, String> listAllReq;
	HashMap<String, String> listAllReqTarget;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");

			// showProccess();
			DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
			dt = (DTO_MedicalInstitution) b.getSerializable("DTO");
			if (dt != null)
				listItem.add(dt);
			// mydapter.add(dt);

			if (key.equals("1")) {
				// E013C.this.sortAdapter();
				// mydapter.notifyDataSetChanged();
				//
				sortList();
				loadData();
				exitProccess();
				if (mydapter != null && mydapter.getCount() < 1) {
					E022.showErrorDialog(E013C.this, ContantMessages.NotData,
							ContantMessages.NotData, false);
				}
			}
		}

	};

	void exitProccess() {
		p.dismiss();
		if (lastPosition > 0 && mydapter.getCount() >= lastPosition) {
			// list.setSelection(lastPosition);
			list.setSelectionFromTop(lastPosition, topPostion);
		}
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E013C.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E013C.this);
		String mg = m.getMessage(ContantMessages.DataLoading);
		p.setMessage(mg);
		// 20121127 bug 393
		// p.setCancelable(false);
		p.setCanceledOnTouchOutside(false);
		//
		p.show();
	}

	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E013C.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute013);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		// Get param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		// String e = b.getString("E");
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");

		// Slide menu
		utl.setMenu_open(false);
		initSileMenu();
		Cursor cur = null;
		Cursor cur2 = null;
		refesh = false;// Q065
		numberitem = 0;
		lastPosition = 0;
		listItem = new ArrayList<DTO_MedicalInstitution>();
		listAllReq = new HashMap<String, String>();
		listAllReqTarget = new HashMap<String, String>();
		loadAllReq();
		try {
			// bug 393 20121128

			CL_TRCase cl = new CL_TRCase(E013C.this);
			cur2 = cl.getItemTRcase(FireStationCode, CaseCode);
			if (cur2 != null && cur2.getCount() > 0) {
				String lat2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat2 != null && longi2 != null) {
					locationA = new Location("A");
					locationA.setLatitude(Double.parseDouble(lat2));
					locationA.setLongitude(Double.parseDouble(longi2));
					lat = locationA.getLatitude();
					longi = locationA.getLongitude();
				}
			}
			// /
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				HeaderBackcolorOnDisaster = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnDisaster"));
				HeaderBackcolorOnEmergency = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnEmergency"));
				// Bug 179 20121019
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				//
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				int mode = sh.getInt("Mode", 0);
				LinearLayout lHeader = (LinearLayout) findViewById(R.id.bgHeader);
				if (mode == 2) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnEmergency));
				} else if (mode == 1) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnDisaster));
				}
			}

			numberDisplay = limit;
			mydapter = new MyAdapter(E013C.this, R.layout.layoutgrid13c);
			list = (ListView) findViewById(R.id.list013);
			list.setAdapter(mydapter);
			list.setOnTouchListener(this);
			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					if (isOnline() && !check) {
						DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
						dt = mydapter.getItem(arg2);
						Intent myinten = new Intent(E013C.this, EMaps2.class);
						Bundle b = new Bundle();
						b.putString("Flat", String.valueOf(lat));
						b.putString("Flongi", String.valueOf(longi));
						b.putString("Tlat", dt.getLatitude());
						b.putString("Tlongi", dt.getLongitude());

						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
					}
					return false;
				}
			});

			list.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
					dt = mydapter.getItem(arg2);
					lastPosition = list.getFirstVisiblePosition();
					View v = list.getChildAt(0);
					topPostion = (v == null) ? 0 : v.getTop();
					if (dt != null) {
						Intent myinten = new Intent(E013C.this, E014C.class);
						Bundle b = new Bundle();
						b.putSerializable("myobject", dt);
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putInt("MNETCount", MNETCount);
						b.putInt("ThirdCoordinateCount", ThirdCoordinateCount);
						b.putString("MNetURL", MNetURL);
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						// if (mMenu.isShowing()) {
						// mMenu.hide();
						// }
					} else {
						numberDisplay += limit;
						loadData();
					}
				}
			});

			Button btnBack = (Button) findViewById(R.id.btnE013Back1);
			btnBack.setOnTouchListener(this);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showE015();
					E015Dialog dg = new E015Dialog();
					dg.ShowE015(E013C.this, FireStationCode, CaseCode, MICode,
							"E017");

					Intent callIntent = new Intent(Intent.ACTION_DIAL);
					callIntent.setData(Uri.parse("tel:"));
					startActivity(callIntent);

				}
			});
			Button btnBack2 = (Button) findViewById(R.id.btnE013Back2);
			btnBack2.setOnTouchListener(this);
			btnBack2.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showE015();
					E015Dialog dg = new E015Dialog();
					dg.ShowE015(E013C.this, FireStationCode, CaseCode, MICode,
							"E017");

					Intent callIntent = new Intent(Intent.ACTION_DIAL);
					callIntent.setData(Uri.parse("tel:"));
					startActivity(callIntent);

				}
			});

			TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			txtHeader.setOnTouchListener(this);
			txtHeader.setText("病院検索結果(救命C)");
			// Testmenu
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			TextView txtOption = (TextView) findViewById(R.id.txtOptions13);
			txtOption.setOnTouchListener(this);
			txtOption.setText("現在地から直近順で検索");

			if (mydapter != null && mydapter.getCount() > 0) {
			} else
				showProccess();

			startThread();
			// createMenu();
			LinearLayout laymain = (LinearLayout) findViewById(R.id.layout_main);
			laymain.setOnTouchListener(this);

			Button btnShow34 = (Button) findViewById(R.id.btnShowE34);
			btnShow34.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (listAllReqTarget.size() == 0) {
						E022.showErrorDialog(E013C.this,
								ContantMessages.NotSelectMICode,
								ContantMessages.NotSelectMICode, false);
					} else {
						showDialogSendFile();
					}
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E013C", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();

		}
	}

	private void loadAllReq() {
		Cursor cur = null;
		try {
			CL_AllReqMedicalInstitution cl = new CL_AllReqMedicalInstitution(
					E013C.this);
			cur = cl.getAllReqMedicalInstitution(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {
					String MICode = cur.getString(cur.getColumnIndex("MICode"));
					if (listAllReq.containsKey(MICode) == false) {
						listAllReq.put(MICode, MICode);
					}
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	// 20131115 Bug 45: refesh to last postion
	private void refeshScreen() {
		Cursor cur = null;
		Cursor cur2 = null;
		numberitem = 0;
		if (mydapter != null)
			mydapter.clear();
		listItem = new ArrayList<DTO_MedicalInstitution>();
		try {

			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				// Bug 179 20121019
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				//
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}
			showProccess();
			startThread();

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E013C", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();

		}
	}

	// Get distance for list items
	// private void getDistanceList()
	// {
	//
	// }
	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;
			Cursor cur2 = null;
			Cursor cur3 = null;
			try {
				String dayofweek = Utilities.getDayOfWeek();
				Cl_FireStationStandard cl1 = new Cl_FireStationStandard(
						E013C.this);
				if (cl1.checkHoliday()) {
					// ko co format 5 so
					// dayofweek = "00008";
					dayofweek = "8";
				}
				Cl_FireStationStandard cl = new Cl_FireStationStandard(
						E013C.this);
				cur = cl.getHopitalInfo13C(FireStationCode, dayofweek);
				if (cur != null && cur.getCount() > 0) {
					int idx = 0;
					do {
						// 20121006 bug 044
						// try {
						// Thread.sleep(100);
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

						String MICode = cur.getString(cur
								.getColumnIndex("MICode"));
						String MIName_Kanji = cur.getString(cur
								.getColumnIndex("MIName_Kanji"));
						String Latitude = cur.getString(cur
								.getColumnIndex("Latitude"));
						String Longitude = cur.getString(cur
								.getColumnIndex("Longitude"));
						String MIType = cur.getString(cur
								.getColumnIndex("MIType"));
						String Address_Kanji = cur.getString(cur
								.getColumnIndex("Address_Kanji"));
						String TelNo1 = cur.getString(cur
								.getColumnIndex("TelNo1"));
						String TelNo2 = cur.getString(cur
								.getColumnIndex("TelNo2"));
						String CCUTelNo = cur.getString(cur
								.getColumnIndex("CCUTelNo"));
						String SCUTelNo = cur.getString(cur
								.getColumnIndex("SCUTelNo"));
						String HotLineTelNo = cur.getString(cur
								.getColumnIndex("HotLineTelNo"));
						String HelicopterTelNo = cur.getString(cur
								.getColumnIndex("HelicopterTelNo"));
						DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
						dt.setMICode(MICode);
						dt.setMIName_Kanji(MIName_Kanji);
						dt.setLatitude(Latitude);
						dt.setLongitude(Longitude);
						dt.setMIType(MIType);
						dt.setAddress_Kanji(Address_Kanji);
						dt.setTelNo1(TelNo1);
						dt.setTelNo2(TelNo2);
						dt.setCCUTelNo(CCUTelNo);
						dt.setSCUTelNo(SCUTelNo);
						dt.setHotLineTelNo(HotLineTelNo);
						dt.setHelicopterTelNo(HelicopterTelNo);
						dt.setImg1(0);
						dt.setImg2(0);
						dt.setImg3(0);
						dt.setImg4(0);
						if (listAllReq.containsKey(MICode)) {
							dt.bKahi = true;
							listAllReqTarget.put(MICode, MICode);
						}
						// load img
						Cl_ContactResultRecord cl2 = new Cl_ContactResultRecord(
								E013C.this);
						// 20121005 Bug 48,49 : dai modify start
						// cur2 = cl2.getTR_ContactResultRecord(FireStationCode,
						// CaseCode, MICode);
						cur2 = cl2.getJianInHopital(dt.getMICode());
						if (cur2 != null && cur2.getCount() > 0) {
							int idx2 = 1;
							do {
								// add code
								String fires = cur2.getString(cur2
										.getColumnIndex("FireStationCode"));
								String caseno = cur2.getString(cur2
										.getColumnIndex("CaseNo"));
								Cl_MovementTime mov = new Cl_MovementTime(
										E013C.this);
								cur3 = mov.getTR_MovementTimeDoctorInAdmission(
										fires, caseno);
								boolean yell = false;
								boolean red = false;
								if (cur3 != null && cur3.getCount() > 0) {
									String Movement_DATETIME = cur3
											.getString(cur3
													.getColumnIndex("Update_DATETIME"));
									if (Movement_DATETIME != null
											&& !Movement_DATETIME.equals("")
											&& Movement_DATETIME.length() == 14) {
										DateFormat formatter = new SimpleDateFormat(
												BaseCount.DateTimeFormat);
										Date date1 = (Date) formatter
												.parse(Movement_DATETIME);
										Date date2 = (Date) formatter
												.parse(Utilities
														.getDateTimeNow());
										long diff = date2.getTime()
												- date1.getTime();
										double time = diff * 1.0
												/ (1000 * 60 * 60);
										if (time <= 0.5)
											yell = true;
									}
								} else if (!yell) {
									String InsertTIME = cur2.getString(cur2
											.getColumnIndex("Insert_DATETIME"));
									if (InsertTIME != null
											&& !InsertTIME.equals("")
											&& InsertTIME.length() == 14) {
										DateFormat formatter = new SimpleDateFormat(
												BaseCount.DateTimeFormat);
										Date date1 = (Date) formatter
												.parse(InsertTIME);
										Date date2 = (Date) formatter
												.parse(Utilities
														.getDateTimeNow());
										long diff = date2.getTime()
												- date1.getTime();
										double time = diff * 1.0
												/ (1000 * 60 * 60);
										if (time <= 0.5)
											red = true;
									}
								}

								switch (idx2) {
								case 1:
									if (red) {
										dt.setImg4(1);
									} else if (yell) {
										dt.setImg4(2);
									} else {
										dt.setImg4(3);
									}
									break;
								case 2:
									if (red) {
										dt.setImg3(1);
									} else if (yell) {
										dt.setImg3(2);
									} else {
										dt.setImg3(3);
									}
									break;
								case 3:
									if (red) {
										dt.setImg2(1);
									} else if (yell) {
										dt.setImg2(2);
									} else {
										dt.setImg2(3);
									}
									break;
								case 4:
									if (red) {
										dt.setImg1(1);
									} else if (yell) {
										dt.setImg1(2);
									} else {
										dt.setImg1(3);
									}
									break;
								default:
									break;
								}
								idx2++;
								// }
							} while (cur2.moveToNext());
						}

						// Bug 044 20121012 Dont get from google
						// if (isOnline()) {
						// String dis = getKhoangCach(String.valueOf(lat),
						// String.valueOf(longi), Latitude, Longitude);
						// double distance = Double.parseDouble(dis);
						// dt.setDistance(distance);
						// } else {
						if (locationA != null) {
							Location locationB = new Location("point B");
							locationB.setLatitude(Double.parseDouble(Latitude));
							locationB.setLongitude(Double
									.parseDouble(Longitude));
							double distance = (double) locationA
									.distanceTo(locationB);
							distance = Utilities
									.roundTwoDecimals(distance / 1000);
							dt.setDistance(distance);
						} else {
							dt.setDistance(0);
						}
						// }
						// if(destition!=null &&!destition.equals(""))
						// {
						// destition+="|";
						// }
						// destition=Latitude+","+Longitude;
						idx++;
						Message mg = handler.obtainMessage();
						Bundle b = new Bundle();
						b.putSerializable("DTO", dt);
						if (idx == cur.getCount()) {
							b.putString("Key", "1");
						} else {
							b.putString("Key", "0");
						}
						mg.setData(b);
						handler.sendMessage(mg);

					} while (cur.moveToNext());

				} else {
					Message mg = handler.obtainMessage();
					Bundle b = new Bundle();
					b.putSerializable("DTO", null);
					b.putString("Key", "1");
					mg.setData(b);
					handler.sendMessage(mg);

				}
			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);
			} finally {
				if (cur3 != null)
					cur3.close();
				if (cur2 != null)
					cur2.close();
				if (cur != null)
					cur.close();

			}

		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	void sortAdapter() {
		try {
			if (mydapter != null && mydapter.getCount() > 1) {
				for (int i = 0; i < mydapter.getCount() - 1; i++) {
					DTO_MedicalInstitution dti = new DTO_MedicalInstitution();
					dti = mydapter.getItem(i);
					for (int j = i + 1; j < mydapter.getCount(); j++) {
						DTO_MedicalInstitution dtj = new DTO_MedicalInstitution();
						dtj = mydapter.getItem(j);
						if (dtj.getDistance() > 0
								&& dtj.getDistance() < dti.getDistance()) {
							// DTO_MedicalInstitution dttmp =new
							// DTO_MedicalInstitution();
							// dttmp=dti;
							String code = dti.getMICode();
							String name = dti.getMIName_Kanji();
							double distance = dti.getDistance();
							String lat = dti.getLatitude();
							String longi = dti.getLongitude();
							String MIType = dti.getMIType();
							String Address_Kanji = dti.getAddress_Kanji();
							String TelNo1 = dti.getTelNo1();
							String TelNo2 = dti.getTelNo2();
							String CCUTelNo = dti.getCCUTelNo();
							String SCUTelNo = dti.getSCUTelNo();
							String HotLineTelNo = dti.getHotLineTelNo();
							String HelicopterTelNo = dti.getHelicopterTelNo();
							int img1 = dti.getImg1();
							int img2 = dti.getImg2();
							int img3 = dti.getImg3();
							int img4 = dti.getImg4();

							dti.setMICode(dtj.getMICode());
							dti.setMIName_Kanji(dtj.getMIName_Kanji());
							dti.setDistance(dtj.getDistance());
							dti.setLatitude(dtj.getLatitude());
							dti.setLongitude(dtj.getLongitude());
							dti.setMIType(dtj.getMIType());
							dti.setAddress_Kanji(dtj.getAddress_Kanji());
							dti.setTelNo1(dtj.getTelNo1());
							dti.setTelNo2(dtj.getTelNo2());
							dti.setCCUTelNo(dtj.getCCUTelNo());
							dti.setSCUTelNo(dtj.getSCUTelNo());
							dti.setHotLineTelNo(dtj.getHotLineTelNo());
							dti.setHelicopterTelNo(dtj.getHelicopterTelNo());
							dti.setImg1(dtj.getImg1());
							dti.setImg2(dtj.getImg2());
							dti.setImg3(dtj.getImg3());
							dti.setImg4(dtj.getImg4());

							dtj.setMICode(code);
							dtj.setMIName_Kanji(name);
							dtj.setDistance(distance);
							dtj.setLatitude(lat);
							dtj.setLongitude(longi);
							dtj.setMIType(MIType);
							dtj.setAddress_Kanji(Address_Kanji);
							dtj.setTelNo1(TelNo1);
							dtj.setTelNo2(TelNo2);
							dtj.setCCUTelNo(CCUTelNo);
							dtj.setSCUTelNo(SCUTelNo);
							dtj.setHotLineTelNo(HotLineTelNo);
							dtj.setHelicopterTelNo(HelicopterTelNo);
							dtj.setImg1(img1);
							dtj.setImg2(img2);
							dtj.setImg3(img3);
							dtj.setImg4(img4);
							mydapter.notifyDataSetChanged();
						}
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void loadData() {
		// int old = numberitem;
		if (mydapter.getCount() > 0) {
			DTO_MedicalInstitution dto = new DTO_MedicalInstitution();
			dto = mydapter.getItem(mydapter.getCount() - 1);
			if (dto == null) {
				mydapter.remove(dto);
			}
		}
		for (int i = 0; i < listItem.size(); i++) {
			if (i == numberDisplay) {
				break;
			}
			if (i >= numberitem) {
				numberitem++;
				DTO_MedicalInstitution dti = new DTO_MedicalInstitution();
				dti = listItem.get(i);
				mydapter.add(dti);
			}

		}
		if (listItem.size() > numberitem) {
			mydapter.add(null);
		}
		mydapter.notifyDataSetChanged();
	}

	private void sortList() {
		try {
			if (listItem != null && listItem.size() > 1) {
				for (int i = 0; i < listItem.size() - 1; i++) {
					DTO_MedicalInstitution dti = new DTO_MedicalInstitution();
					dti = listItem.get(i);
					for (int j = i + 1; j < listItem.size(); j++) {
						DTO_MedicalInstitution dtj = new DTO_MedicalInstitution();
						dtj = listItem.get(j);
						if (dtj.getDistance() > 0
								&& dtj.getDistance() < dti.getDistance()) {
							// DTO_MedicalInstitution dttmp =new
							// DTO_MedicalInstitution();
							// dttmp=dti;
							String code = dti.getMICode();
							String name = dti.getMIName_Kanji();
							double distance = dti.getDistance();
							String lat = dti.getLatitude();
							String longi = dti.getLongitude();
							String MIType = dti.getMIType();
							String Address_Kanji = dti.getAddress_Kanji();
							String TelNo1 = dti.getTelNo1();
							String TelNo2 = dti.getTelNo2();
							String CCUTelNo = dti.getCCUTelNo();
							String SCUTelNo = dti.getSCUTelNo();
							String HotLineTelNo = dti.getHotLineTelNo();
							String HelicopterTelNo = dti.getHelicopterTelNo();
							int img1 = dti.getImg1();
							int img2 = dti.getImg2();
							int img3 = dti.getImg3();
							int img4 = dti.getImg4();

							dti.setMICode(dtj.getMICode());
							dti.setMIName_Kanji(dtj.getMIName_Kanji());
							dti.setDistance(dtj.getDistance());
							dti.setLatitude(dtj.getLatitude());
							dti.setLongitude(dtj.getLongitude());
							dti.setMIType(dtj.getMIType());
							dti.setAddress_Kanji(dtj.getAddress_Kanji());
							dti.setTelNo1(dtj.getTelNo1());
							dti.setTelNo2(dtj.getTelNo2());
							dti.setCCUTelNo(dtj.getCCUTelNo());
							dti.setSCUTelNo(dtj.getSCUTelNo());
							dti.setHotLineTelNo(dtj.getHotLineTelNo());
							dti.setHelicopterTelNo(dtj.getHelicopterTelNo());
							dti.setImg1(dtj.getImg1());
							dti.setImg2(dtj.getImg2());
							dti.setImg3(dtj.getImg3());
							dti.setImg4(dtj.getImg4());

							dtj.setMICode(code);
							dtj.setMIName_Kanji(name);
							dtj.setDistance(distance);
							dtj.setLatitude(lat);
							dtj.setLongitude(longi);
							dtj.setMIType(MIType);
							dtj.setAddress_Kanji(Address_Kanji);
							dtj.setTelNo1(TelNo1);
							dtj.setTelNo2(TelNo2);
							dtj.setCCUTelNo(CCUTelNo);
							dtj.setSCUTelNo(SCUTelNo);
							dtj.setHotLineTelNo(HotLineTelNo);
							dtj.setHelicopterTelNo(HelicopterTelNo);
							dtj.setImg1(img1);
							dtj.setImg2(img2);
							dtj.setImg3(img3);
							dtj.setImg4(img4);

						}
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public class MyAdapter extends ArrayAdapter<DTO_MedicalInstitution> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.layoutgrid13c, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}

			TextView txtMiType = myw.getSTT();
			TextView txtHopital = myw.getName();
			TextView btnShowE026 = myw.getbtnShowE026();
			ImageView img1 = myw.getIMG1();
			ImageView img2 = myw.getIMG2();
			ImageView img3 = myw.getIMG3();
			ImageView img4 = myw.getIMG4();
			LinearLayout row = myw.getRow();
			CheckBox chkSelect = myw.getchkSelect();
			final DTO_MedicalInstitution dt = this.getItem(position);
			if (dt != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(0, 0, 0, 0);
				row.setLayoutParams(param);
				Cl_ContactResultRecord tr = new Cl_ContactResultRecord(
						E013C.this);
				boolean exist = tr.checkExistTR_ContactResultRecord(
						FireStationCode, CaseCode, this.getItem(position)
								.getMICode());
				if (exist) {
					row.setBackgroundResource(R.drawable.bgselected13c);
				} else
					row.setBackgroundResource(R.drawable.bggrid13c);

				txtMiType.setVisibility(View.VISIBLE);
				txtHopital.setVisibility(View.VISIBLE);
				img1.setVisibility(View.VISIBLE);
				img2.setVisibility(View.VISIBLE);
				img3.setVisibility(View.VISIBLE);
				img4.setVisibility(View.VISIBLE);
				btnShowE026.setVisibility(View.VISIBLE);
				chkSelect.setVisibility(View.VISIBLE);
				if (dt.bKahi)
					chkSelect.setChecked(true);
				else
					chkSelect.setChecked(false);
				chkSelect.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						CheckBox cb = (CheckBox) v;
						if (cb.isChecked()) {
							// Toast.makeText(E013C.this, "Add",
							// Toast.LENGTH_SHORT).show();
							// CL_AllReqMedicalInstitution cl = new
							// CL_AllReqMedicalInstitution(
							// E013C.this);
							// ContentValues values = new ContentValues();
							// values.put("FireStationCode", FireStationCode);
							// values.put("CaseNo", CaseCode);
							// values.put("MICode", dt.getMICode());
							// values.put("kin_kbn", "");
							// cl.createAllReqMedicalInstitution(values);
							if (listAllReqTarget.containsKey(dt.getMICode()) == false)
								listAllReqTarget.put(dt.getMICode(),
										dt.getMICode());
							dt.bKahi = true;
						} else {
							// Toast.makeText(E013C.this,
							// "Delete" + dt.getMIName_Kanji(),
							// Toast.LENGTH_SHORT).show();
							// CL_AllReqMedicalInstitution cl = new
							// CL_AllReqMedicalInstitution(
							// E013C.this);
							// cl.deleteAllReqMedicalInstitution(FireStationCode,
							// CaseCode, dt.getMICode());
							if (listAllReqTarget.containsKey(dt.getMICode()) == true)
								listAllReqTarget.remove(dt.getMICode());
							dt.bKahi = false;
						}
					}
				});

				if (this.getItem(position).getMIType() != null
						&& this.getItem(position).getMIType().equals("5")) {
					txtMiType.setText("");
					txtMiType.setBackgroundResource(R.drawable.headerrow13c);
				} else if (this.getItem(position).getMIType() != null
						&& this.getItem(position).getMIType().equals("3")) {

					txtMiType.setText(this.getItem(position).getMIType());
					txtMiType.setBackgroundResource(R.drawable.headerrowred);
				} else if (this.getItem(position).getMIType() != null
						&& this.getItem(position).getMIType().equals("2")) {

					txtMiType.setText(this.getItem(position).getMIType());
					txtMiType.setBackgroundResource(R.drawable.headerrowe13);
				}

				txtHopital.setText(this.getItem(position).getMIName_Kanji());

				if (dt.getImg1() == 0) {
					// 20130205
					img1.setBackgroundResource(R.drawable.oblue);
					img1.setVisibility(View.VISIBLE);
				} else if (dt.getImg1() == 1) {
					img1.setBackgroundResource(R.drawable.ored);
					img1.setVisibility(View.VISIBLE);
				} else if (dt.getImg1() == 2) {
					img1.setBackgroundResource(R.drawable.oyellow);
					img1.setVisibility(View.VISIBLE);
				} else // if (dt.getImg1() == 3)
				{
					img1.setBackgroundResource(R.drawable.oblue);
					img1.setVisibility(View.VISIBLE);
				}

				if (dt.getImg2() == 0) {
					img2.setBackgroundResource(R.drawable.oblue);
					img2.setVisibility(View.VISIBLE);
				} else if (dt.getImg2() == 1) {
					img2.setBackgroundResource(R.drawable.ored);
					img2.setVisibility(View.VISIBLE);
				} else if (dt.getImg2() == 2) {
					img2.setBackgroundResource(R.drawable.oyellow);
					img2.setVisibility(View.VISIBLE);
				} else // if (dt.getImg2() == 3)
				{
					img2.setBackgroundResource(R.drawable.oblue);
					img2.setVisibility(View.VISIBLE);
				}

				if (dt.getImg3() == 0) {
					img3.setBackgroundResource(R.drawable.oblue);
					img3.setVisibility(View.VISIBLE);
				} else if (dt.getImg3() == 1) {
					img3.setBackgroundResource(R.drawable.ored);
					img3.setVisibility(View.VISIBLE);
				} else if (dt.getImg3() == 2) {
					img3.setBackgroundResource(R.drawable.oyellow);
					img3.setVisibility(View.VISIBLE);
				} else // if (dt.getImg3() == 3)
				{
					img3.setBackgroundResource(R.drawable.oblue);
					img3.setVisibility(View.VISIBLE);
				}

				if (dt.getImg4() == 0) {
					img4.setBackgroundResource(R.drawable.oblue);
					img4.setVisibility(View.VISIBLE);
				} else if (dt.getImg4() == 1) {
					img4.setBackgroundResource(R.drawable.ored);
					img4.setVisibility(View.VISIBLE);
				} else if (dt.getImg4() == 2) {
					img4.setBackgroundResource(R.drawable.oyellow);
					img4.setVisibility(View.VISIBLE);
				} else // if (dt.getImg4() == 3)
				{
					img4.setBackgroundResource(R.drawable.oblue);
					img4.setVisibility(View.VISIBLE);
				}
				TextView txtKm = myw.getTXTKM();
				double dis = this.getItem(position).getDistance();
				if (dis > 0) {
					txtKm.setText(String.valueOf(dis) + " km");
				} else {
					txtKm.setText("? km");
				}
				btnShowE026.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						String MICode_Select = dt.getMICode();
						E026 e26 = new E026();
						e26.ShowE026(E013C.this, MICode_Select, CaseCode);

					}
				});
			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				row.setLayoutParams(param);
				txtMiType.setVisibility(View.INVISIBLE);
				txtHopital.setVisibility(View.INVISIBLE);
				img1.setVisibility(View.INVISIBLE);
				img2.setVisibility(View.INVISIBLE);
				img3.setVisibility(View.INVISIBLE);
				img4.setVisibility(View.INVISIBLE);
				btnShowE026.setVisibility(View.INVISIBLE);
				chkSelect.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow);
				// row.setOnClickListener(new OnClickListener() {
				//
				// public void onClick(View v) {
				// // TODO Auto-generated method stub
				// loadData();
				//
				// }
				// });
			}

			return v;
		}

	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		TextView lable2 = null;
		TextView lable18 = null;
		TextView lable19 = null;
		ImageView img1 = null;
		ImageView img2 = null;
		ImageView img3 = null;
		ImageView img4 = null;
		LinearLayout layoutrow = null;
		CheckBox chk = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}

		ImageView getIMG1() {
			if (img1 == null) {
				img1 = (ImageView) base.findViewById(R.id.imgGrid1311);
			}
			return img1;
		}

		ImageView getIMG2() {
			if (img2 == null) {
				img2 = (ImageView) base.findViewById(R.id.imgGrid1312);
			}
			return img2;
		}

		ImageView getIMG3() {
			if (img3 == null) {
				img3 = (ImageView) base.findViewById(R.id.imgGrid1313);
			}
			return img3;
		}

		ImageView getIMG4() {
			if (img4 == null) {
				img4 = (ImageView) base.findViewById(R.id.imgGrid1314);
			}
			return img4;
		}

		TextView getSTT() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtGrid13STT);
			}
			return lable1;
		}

		TextView getName() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtGrid13Name);
			}
			return lable2;
		}

		TextView getTXTKM() {
			if (lable18 == null) {
				lable18 = (TextView) base.findViewById(R.id.txtGrid13117);
			}
			return lable18;
		}

		TextView getbtnShowE026() {
			if (lable19 == null) {
				lable19 = (TextView) base.findViewById(R.id.btnShowE026);
			}
			return lable19;
		}

		CheckBox getchkSelect() {
			if (chk == null) {
				chk = (CheckBox) base.findViewById(R.id.chkSelect);
			}
			return chk;
		}
	}

	private void loaddataAgain() {
		Cursor cur2 = null;
		Cursor cur3 = null;
		try {
			if (mydapter != null) {
				for (int i = 0; i < mydapter.getCount(); i++) {
					DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
					dt = mydapter.getItem(i);
					// load img
					Cl_ContactResultRecord cl2 = new Cl_ContactResultRecord(
							E013C.this);

					// Cursor cur2 = cl2.getTR_ContactResultRecord(
					// FireStationCode, CaseCode, dt.getMICode());
					// 20121005 Bug 48,49 : dai modify start
					// cur2 = cl2.getTR_ContactResultRecord(FireStationCode,
					// CaseCode, MICode);
					cur2 = cl2.getJianInHopital(dt.getMICode());
					if (cur2 != null && cur2.getCount() > 0) {
						int idx2 = 1;
						do {
							// add code
							String fires = cur2.getString(cur2
									.getColumnIndex("FireStationCode"));
							String caseno = cur2.getString(cur2
									.getColumnIndex("CaseNo"));
							Cl_MovementTime mov = new Cl_MovementTime(
									E013C.this);
							cur3 = mov.getTR_MovementTimeDoctor(fires, caseno);
							boolean yell = false;
							boolean red = false;
							if (cur3 != null && cur3.getCount() > 0) {
								String Movement_DATETIME = cur3.getString(cur3
										.getColumnIndex("Movement_DATETIME"));
								if (Movement_DATETIME != null
										&& !Movement_DATETIME.equals("")) {
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter
											.parse(Movement_DATETIME);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double time = diff * 1.0 / (1000 * 60 * 60);
									if (time <= 0.5)
										yell = true;
								}
							} else if (!yell) {
								String InsertTIME = cur2.getString(cur2
										.getColumnIndex("Insert_DATETIME"));
								if (InsertTIME != null
										&& !InsertTIME.equals("")) {
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter
											.parse(InsertTIME);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double time = diff * 1.0 / (1000 * 60 * 60);
									if (time <= 0.5)
										red = true;
								}
							}

							switch (idx2) {
							case 1:
								if (red) {
									dt.setImg4(1);
								} else if (yell) {
									dt.setImg4(2);
								} else {
									dt.setImg4(3);
								}
								break;
							case 2:
								if (red) {
									dt.setImg3(1);
								} else if (yell) {
									dt.setImg3(2);
								} else {
									dt.setImg3(3);
								}
								break;
							case 3:
								if (red) {
									dt.setImg2(1);
								} else if (yell) {
									dt.setImg2(2);
								} else {
									dt.setImg2(3);
								}
								break;
							case 4:
								if (red) {
									dt.setImg1(1);
								} else if (yell) {
									dt.setImg1(2);
								} else {
									dt.setImg1(3);
								}
								break;
							default:
								break;
							}
							idx2++;
							// }
						} while (cur2.moveToNext());
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur2 != null)
				cur2.close();
			if (cur3 != null)
				cur3.close();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == resultCode && resultCode != 0) {
			if (resultCode == 133) {
				Bundle b = data.getExtras();
				totalfilesize = b.getInt("TotalSize");
				if (dialogSend != null) {
					TextView txtFileSize = (TextView) dialogSend
							.findViewById(R.id.txtFileSize);
					txtFileSize.setText(formatSize(totalfilesize));
					TextView txtError1 = (TextView) dialogSend
							.findViewById(R.id.lblMessageErr1);
					TextView txtError2 = (TextView) dialogSend
							.findViewById(R.id.lblMessageErr2);
					if (totalfilesize > maxsize) {
						txtFileSize.setTextColor(Color.RED);
					} else {
						txtError1.setText("");
						txtError2.setText("");
					}
					TextView txtUnit = (TextView) dialogSend
							.findViewById(R.id.txtUnit);
					if (totalfilesize < 1024)
						txtUnit.setText("KB");
					else
						txtUnit.setText("MB");
				}
			}
		} else {
			this.loaddataAgain();
			mydapter.notifyDataSetChanged();
		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	Dialog dialogSend;
	int totalfilesize;
	int maxsize;
	int Outbound_No;
	ArrayList<DTO_MedicalInstitution> listHopital;

	public void showDialogSendFile() {
		try {
			listHopital = new ArrayList<DTO_MedicalInstitution>();
			// Total file size
			CL_FileInfo cl = new CL_FileInfo(E013C.this);
			totalfilesize = cl.getTotalSize(FireStationCode, CaseCode);

			// Limit size
			CL_Linespeeddecision line = new CL_Linespeeddecision(E013C.this);
			maxsize = line.getMaxFileNo(getNetWorkType());

			// Outbound_No
			CL_TRCase tr = new CL_TRCase(E013C.this);
			Outbound_No = tr.getOutbound_No(FireStationCode, CaseCode);

			dialogSend = new Dialog(E013C.this);
			dialogSend.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogSend.setContentView(R.layout.layoute34);
			dialogSend.setCanceledOnTouchOutside(false);

			TextView txtOutbound_No = (TextView) dialogSend
					.findViewById(R.id.txtOutbound_No);
			txtOutbound_No.setText(String.valueOf(Outbound_No));
			TextView txtFileSize = (TextView) dialogSend
					.findViewById(R.id.txtFileSize);
			txtFileSize.setText(formatSize(totalfilesize));
			TextView txtError1 = (TextView) dialogSend
					.findViewById(R.id.lblMessageErr1);
			TextView txtError2 = (TextView) dialogSend
					.findViewById(R.id.lblMessageErr2);
			if (totalfilesize > maxsize) {
				txtFileSize.setTextColor(Color.RED);
			} else {
				txtError1.setText("");
				txtError2.setText("");
			}
			TextView txtUnit = (TextView) dialogSend.findViewById(R.id.txtUnit);
			if (totalfilesize < 1024)
				txtUnit.setText("KB");
			else
				txtUnit.setText("MB");

			ListView lvMiName = (ListView) dialogSend
					.findViewById(R.id.lvMiName);
			final E034Adapter adapter = new E034Adapter(E013C.this,
					R.layout.layoutrowminame);
			lvMiName.setAdapter(adapter);
			for (int i = 0; i < listItem.size(); i++) {
				DTO_MedicalInstitution dt = listItem.get(i);
				if (listAllReqTarget.containsKey(dt.getMICode())) {
					adapter.add(dt);
					listHopital.add(dt);
				}
			}

			Button btnOk = (Button) dialogSend.findViewById(R.id.btnOK34);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					CL_TRCase tr = new CL_TRCase(E013C.this);
					tr.updateKin_kbn(FireStationCode, CaseCode, "2");

					CL_AllReqMedicalInstitution cl = new CL_AllReqMedicalInstitution(
							E013C.this);
					cl.CopyAllReqMedicalInstitution(FireStationCode, CaseCode);

					tr = new CL_TRCase(E013C.this);
					String kin_kbn = tr.getKin_kbn(FireStationCode, CaseCode);
					for (int i = 0; i < adapter.getCount(); i++) {
						DTO_MedicalInstitution item = adapter.getItem(i);

						ContentValues values = new ContentValues();
						values.put("FireStationCode", FireStationCode);
						values.put("CaseNo", CaseCode);
						values.put("MICode", item.getMICode());
						values.put("EMSTransmission_Datetime",
								Utilities.getDateTimeNow());
						values.put("kin_kbn", kin_kbn);
						cl = new CL_AllReqMedicalInstitution(E013C.this);
						cl.createAllReqMedicalInstitution(values);

					}
					E034 e34 = new E034();
					e34.UpdateLoad(E013C.this, FireStationCode, CaseCode,
							totalfilesize, Outbound_No, listHopital);
					dialogSend.cancel();
				}
			});
			Button btnCancel = (Button) dialogSend
					.findViewById(R.id.btnCancel34);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialogSend.cancel();
				}
			});
			Button btnShow30 = (Button) dialogSend
					.findViewById(R.id.btnShowE30);
			btnShow30.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten = new Intent(E013C.this, E030.class);
					Bundle b = new Bundle();
					b.putString("FireStationCode", FireStationCode);
					b.putString("CaseCode", CaseCode);
					b.putInt("MaxSize", maxsize);
					b.putInt("Key", 133);
					myinten.putExtras(b);
					startActivityForResult(myinten, 133);
				}
			});

			dialogSend.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E013C.this);
		}
	}

	private String formatSize(int size) {
		if (size < 1024)
			return String.valueOf(size);
		else {
			double res = Utilities.roundTwoDecimals(size / 1024);
			return String.valueOf(res);
		}
	}

	private int getNetWorkType() {
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		int networkType = tm.getNetworkType();
		switch (networkType) {
		case TelephonyManager.NETWORK_TYPE_GPRS:
		case TelephonyManager.NETWORK_TYPE_EDGE:
		case TelephonyManager.NETWORK_TYPE_CDMA:
		case TelephonyManager.NETWORK_TYPE_1xRTT:
		case TelephonyManager.NETWORK_TYPE_IDEN:
			return 2;
		case TelephonyManager.NETWORK_TYPE_UMTS:
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
		case TelephonyManager.NETWORK_TYPE_EVDO_B:
		case TelephonyManager.NETWORK_TYPE_EHRPD:
			return 3;
		case TelephonyManager.NETWORK_TYPE_HSUPA:
		case TelephonyManager.NETWORK_TYPE_HSPA:
		case TelephonyManager.NETWORK_TYPE_HSDPA:
		case TelephonyManager.NETWORK_TYPE_HSPAP:
			return 3;
			// return "3.5G";
		case TelephonyManager.NETWORK_TYPE_LTE:
			return 4;
		default:
			return 0;
		}
		// return "";
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// locationManager.removeUpdates(this); //Bug 166 20121025
		th.setRunning(false);
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(E013C.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// locationManager.requestLocationUpdates(tower, 500, 1, this);//Bug 166
		// 20121025
		// SharedPreferences sh = this.getSharedPreferences("app",
		// this.MODE_PRIVATE);
		String syndb = getStatusSysDB();// sh.getString("SysDB", "0");
		if (refesh && syndb.equals("0")) {
			// onCreate(null);
			refeshScreen();
		}
		refesh = true;// Q065 20121027
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		E034.SetActivity(this);
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		// Bug 166 20121025
		// lat = location.getLatitude();
		// longi = location.getLongitude();

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
