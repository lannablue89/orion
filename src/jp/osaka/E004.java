﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class E004 extends Activity implements OnTouchListener {
	MyAdapter mydapter;

	String InsertDate = "99999999999999";
	String InsertDateFirst = "";
	String Limit = "10";
	HashMap<String, String> listSex;
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			progressBar = (ProgressBar) findViewById(R.id.pro);
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E004.this,
					progressBar, pro_2, "", "");
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	ThreadAutoShowButton bt = new ThreadAutoShowButton();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute004);
		utl.setMenu_open(false);
		initSileMenu();
		Cursor cur = null;
		try {
			getListSex();// 20121102
			CL_MSSetting cl = new CL_MSSetting(E004.this);
			cur = cl.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				Limit = cur.getString(cur
						.getColumnIndex(MS_SettingCount.CaseCount));
			}

			CL_TRCase tr = new CL_TRCase(this);
			InsertDateFirst = tr.getTR_CaseFirst("1");

			mydapter = new MyAdapter(this, R.layout.grid004);
			ListView list = (ListView) findViewById(R.id.list004);
			list.setAdapter(mydapter);
			list.setOnTouchListener(this);
			Button btnBack = (Button) findViewById(R.id.btnE04Back);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten = new Intent(v.getContext(), E002.class);
					startActivityForResult(myinten, 0);
					finish();
				}
			});
			btnBack.setOnTouchListener(this);
			this.loadDataforGrid(InsertDate);

		} catch (Exception e) {
			// TODO: handle exception
		} finally {

		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		bt.setRunning(false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		bt.showButtonAuto(this, "", "", false);
	}

	private void getListSex() {
		listSex = new HashMap<String, String>();
		Cl_Code cl = new Cl_Code(this);
		Cursor cur = cl.getMS_Code("00013");
		if (cur != null && cur.getCount() > 0) {
			do {
				String code = cur.getString(cur.getColumnIndex("CodeID"));
				String name = cur.getString(cur.getColumnIndex("CodeName"));
				listSex.put(code, name);
			} while (cur.moveToNext());
		}
		cur.close();
	}

	// Q065 20121027
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(E004.this, E002.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
			}
			return true; // always eat it!
		}
		return super.onKeyDown(keyCode, event);
	}

	private void loadDataforGrid(String insertDate) {
		Cursor c = null;
		try {

			CL_TRCase tr = new CL_TRCase(E004.this);
			c = tr.fetchTRCaseInActy(insertDate, Limit);
			if (c != null && c.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_TRCase dto = new DTO_TRCase();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_TRCase tc = new DTO_TRCase();
					String caseCode = c.getString(c
							.getColumnIndex(TR_CaseCount.CaseCode));
					tc.setCaseCode(caseCode);
					tc.setJianNo(c.getString(c
							.getColumnIndex(TR_CaseCount.JianNo)));

					tc.setAge(c.getString(c.getColumnIndex(TR_CaseCount.Age)));
					tc.setSex(c.getString(c.getColumnIndex(TR_CaseCount.Sex)));
					tc.setNotice_TEXT(c.getString(c
							.getColumnIndex(TR_CaseCount.Notice_TEXT)));
					tc.setFireStationCode(c.getString(c
							.getColumnIndex(TR_CaseCount.FireStationCode)));
					tc.setContactCount(c.getString(c
							.getColumnIndex(TR_CaseCount.ContactCount)));
					// String kanji = getHopitalName(tc.getFireStationCode(),
					// tc.getCaseCode());
					// tc.setMIName_Kanji(kanji);
					String HoptalName = c.getString(c
							.getColumnIndex("FacilitiesName"));
					tc.setMIName_Kanji(HoptalName);
					// bug 104
					String add = c.getString(c
							.getColumnIndex(TR_CaseCount.IncidentScene));
					tc.setIncidentScene(add);
					//
					InsertDate = c.getString(c
							.getColumnIndex(TR_CaseCount.Insert_DATETIME));
					tc.setInsert_DATETIME(InsertDate);
					tc.kin_kbn = c.getString(c.getColumnIndex("kin_kbn"));
					tc.kin_no = c.getString(c.getColumnIndex("kin_no"));
					tc.kin_branch_no = c.getString(c
							.getColumnIndex("kin_branch_no"));
					mydapter.add(tc);
				} while (c.moveToNext());
				if (!InsertDate.equals(InsertDateFirst)) {
					mydapter.add(null);
				}
				mydapter.notifyDataSetChanged();
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E004.this, e.getMessage(), "E004", true);
		} finally {
			if (c != null)
				c.close();

		}
	}

	private void loadDataforGrid1(String insertDate) {
		Cursor c = null;
		try {

			CL_TRCase tr = new CL_TRCase(E004.this);
			c = tr.fetchTRCaseInActy(insertDate, "1");
			if (c != null && c.getCount() > 0) {
				if (mydapter.getCount() > 0) {
					DTO_TRCase dto = new DTO_TRCase();
					dto = mydapter.getItem(mydapter.getCount() - 1);
					if (dto == null) {
						mydapter.remove(dto);
					}
				}
				do {
					DTO_TRCase tc = new DTO_TRCase();
					String caseCode = c.getString(c
							.getColumnIndex(TR_CaseCount.CaseCode));
					tc.setCaseCode(caseCode);
					tc.setJianNo(c.getString(c
							.getColumnIndex(TR_CaseCount.JianNo)));

					tc.setAge(c.getString(c.getColumnIndex(TR_CaseCount.Age)));
					tc.setSex(c.getString(c.getColumnIndex(TR_CaseCount.Sex)));
					tc.setNotice_TEXT(c.getString(c
							.getColumnIndex(TR_CaseCount.Notice_TEXT)));
					tc.setFireStationCode(c.getString(c
							.getColumnIndex(TR_CaseCount.FireStationCode)));
					tc.setContactCount(c.getString(c
							.getColumnIndex(TR_CaseCount.ContactCount)));
					// String kanji = getHopitalName(tc.getFireStationCode(),
					// tc.getCaseCode());
					// tc.setMIName_Kanji(kanji);
					String HoptalName = c.getString(c
							.getColumnIndex("FacilitiesName"));
					tc.setMIName_Kanji(HoptalName);
					// bug 104
					String add = c.getString(c
							.getColumnIndex(TR_CaseCount.IncidentScene));
					tc.setIncidentScene(add);
					//
					InsertDate = c.getString(c
							.getColumnIndex(TR_CaseCount.Insert_DATETIME));
					tc.setInsert_DATETIME(InsertDate);
					tc.kin_kbn = c.getString(c.getColumnIndex("kin_kbn"));
					tc.kin_no = c.getString(c.getColumnIndex("kin_no"));
					tc.kin_branch_no = c.getString(c
							.getColumnIndex("kin_branch_no"));
					mydapter.add(tc);
				} while (c.moveToNext());
				if (!InsertDate.equals(InsertDateFirst)) {
					mydapter.add(null);
				}
				mydapter.notifyDataSetChanged();
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E004.this, e.getMessage(), "E004", true);
		} finally {
			if (c != null)
				c.close();

		}
	}

	// private String getHopitalName(String FireStationCode, String CaseNo) {
	// String result = "";
	// CL_TRCase cl = new CL_TRCase(E004.this);
	// Cursor cur = cl.getMIName_Kanji(FireStationCode, CaseNo);
	// try {
	// if (cur != null && cur.getCount() > 0) {
	// result = cur.getString(cur.getColumnIndex("MIName_Kanji"));
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// E022.showErrorDialog(E004.this, e.getMessage(), "E004", true);
	// } finally {
	// cur.close();
	// }
	// return result;
	//
	// }

	public class MyAdapter extends ArrayAdapter<DTO_TRCase> {
		Context c;

		public MyAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
			c = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return super.getCount();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = convertView;
			ViewWraper myw;
			if (v == null) {

				LayoutInflater l = LayoutInflater.from(c);
				v = l.inflate(R.layout.grid004, null);
				myw = new ViewWraper(v);
				v.setTag(myw);

			} else {
				myw = (ViewWraper) convertView.getTag();

			}

			TextView txtJianNO = myw.getJianNo();
			TextView txtCasecode = myw.getCasecode();
			// TextView txtCasecode3 = myw.getCasecode3();
			TextView txtStatus = myw.getStatus();
			TextView txtSex = myw.getSex();
			TextView txtAge = myw.getAge();
			TextView txtAddress = myw.getAddress();// 20121010 Bug 104
			TextView txtHopital = myw.getHopital();
			TextView txtTitleSex = myw.getTitleSex();
			TextView txtTitleAge = myw.getTitleAge();
			TextView txtKBN = myw.getKBN();
			LinearLayout row = myw.getRow();
			if (this.getItem(position) != null) {

				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				param.setMargins(10, 0, 10, 0);
				row.setLayoutParams(param);
				row.setBackgroundResource(R.drawable.gridbg);
				txtJianNO.setVisibility(View.VISIBLE);
				txtCasecode.setVisibility(View.VISIBLE);
				// txtCasecode3.setVisibility(View.VISIBLE);
				txtStatus.setVisibility(View.VISIBLE);
				txtSex.setVisibility(View.VISIBLE);
				txtAge.setVisibility(View.VISIBLE);
				txtTitleSex.setVisibility(View.VISIBLE);
				txtTitleAge.setVisibility(View.VISIBLE);
				txtAddress.setVisibility(View.VISIBLE);// 20121010 Bug 104
				txtHopital.setVisibility(View.VISIBLE);
				txtKBN.setVisibility(View.VISIBLE);
				txtJianNO.setText(this.getItem(position).getJianNo());
				String casecode = this.getItem(position).getCaseCode();
				if (casecode.length() == 22) {

					casecode = casecode.substring(0, 10) + "-"
							+ casecode.substring(10, 18) + "-"
							+ casecode.substring(18, 22);
				}
				txtCasecode.setText(casecode);
				// txtStatus.setText("復活");
				txtStatus.setText("訂正");
				String sex = "";
				if (this.getItem(position).getSex() != null) {
					txtTitleSex.setText("性");
					// if (this.getItem(position).getSex().equals(BaseCount.Nu))
					// {
					// sex = "女";
					// } else {
					// sex = "男";
					// }
					if (listSex.containsKey(this.getItem(position).getSex())) {
						sex = listSex.get(this.getItem(position).getSex());
					}
				} else {
					txtTitleSex.setText("");
				}

				txtSex.setText(sex);
				if (this.getItem(position).getAge() != null) {
					txtTitleAge.setText("歳");
					txtAge.setText(this.getItem(position).getAge());
				} else {
					txtTitleAge.setText("");
					txtAge.setText("");
				}
				// 20121010 Bug 104
				txtAddress.setText(this.getItem(position).getIncidentScene());
				// End
				txtHopital.setText(this.getItem(position).getMIName_Kanji());
				if (this.getItem(position).kin_kbn != null) {
					if (this.getItem(position).kin_kbn.equals("1")) {
						txtKBN.setText(this.getItem(position).kin_no);
						txtKBN.setBackgroundResource(R.drawable.btndisaster);
					} else if (this.getItem(position).kin_kbn.equals("0")) {
						txtKBN.setText("まもって");
						txtKBN.setBackgroundResource(R.drawable.btnnanomet);
					} else if (this.getItem(position).kin_kbn.equals("2")) {
						txtKBN.setText("一斉");
						txtKBN.setBackgroundResource(R.drawable.btnsended);
					} else {
						txtKBN.setVisibility(View.INVISIBLE);
					}

				} else {
					txtKBN.setVisibility(View.INVISIBLE);
				}
				txtKBN.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						DTO_TRCase dt = mydapter.getItem(position);
						if (dt.kin_kbn.equals("1")) {
							CL_TRCase cl = new CL_TRCase(E004.this);
							cl.ClearKinNo(dt.getFireStationCode(),
									dt.getCaseCode());
							dt.kin_kbn = "";
							dt.kin_no = "";
							dt.kin_branch_no = "";
							dt.kinLink_DateTime = "";
							mydapter.notifyDataSetChanged();
						}
					}
				});
				txtStatus.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Toast.makeText(E002.this,
						// "Update Status "+mydapter.getItem(position).getCaseCode(),
						// Toast.LENGTH_SHORT).show();
						String firestationCode = mydapter.getItem(position)
								.getFireStationCode();
						String casecode = mydapter.getItem(position)
								.getCaseCode();
						CL_TRCase cl = new CL_TRCase(E004.this);
						cl.updateStatusTRCase(firestationCode, casecode, "0");
						// loadDataforGrid();
						// Bug 127
						for (int i = 0; i < mydapter.getCount(); i++) {
							DTO_TRCase dt = new DTO_TRCase();
							dt = mydapter.getItem(i);
							if (dt.getFireStationCode().equals(firestationCode)
									&& dt.getCaseCode().equals(casecode)) {
								mydapter.remove(dt);
								break;
							}
						}
						// mydapter.notifyDataSetChanged();
						loadDataforGrid1(InsertDate);
					}
				});
			} else {
				LinearLayout.LayoutParams param = new LayoutParams(
						LayoutParams.MATCH_PARENT, 68);
				param.setMargins(0, 10, 0, 10);
				row.setLayoutParams(param);
				txtJianNO.setVisibility(View.INVISIBLE);
				txtCasecode.setVisibility(View.INVISIBLE);
				// txtCasecode3.setVisibility(View.INVISIBLE);
				txtStatus.setVisibility(View.INVISIBLE);
				txtSex.setVisibility(View.INVISIBLE);
				txtAge.setVisibility(View.INVISIBLE);
				txtAddress.setVisibility(View.INVISIBLE);
				txtTitleSex.setVisibility(View.INVISIBLE);
				txtTitleAge.setVisibility(View.INVISIBLE);
				txtHopital.setVisibility(View.INVISIBLE);
				txtKBN.setVisibility(View.INVISIBLE);
				row.setBackgroundResource(R.drawable.btnnextrow);
				row.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						loadDataforGrid(InsertDate);

					}
				});
			}
			return v;
		}

	}

	// 20121010 Bug 104
	class ViewWraper {

		View base;
		TextView lable1 = null;
		TextView lable2 = null;
		// TextView lable3 = null;
		TextView lable4 = null;
		TextView lable5 = null;
		TextView lable6 = null;
		TextView lable7 = null;
		TextView lable8 = null;
		TextView lable9 = null;
		TextView lable10 = null;
		TextView lable11 = null;
		LinearLayout layoutrow = null;

		// TextView lable3 = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getJianNo() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtJianNo);
			}
			return lable1;
		}

		TextView getCasecode() {
			if (lable2 == null) {
				lable2 = (TextView) base.findViewById(R.id.txtCasecode);
			}
			return lable2;
		}

		TextView getStatus() {
			if (lable4 == null) {
				lable4 = (TextView) base.findViewById(R.id.txtStatus);
			}
			return lable4;
		}

		TextView getSex() {
			if (lable5 == null) {
				lable5 = (TextView) base.findViewById(R.id.txtSex);
			}
			return lable5;
		}

		TextView getAge() {
			if (lable8 == null) {
				lable8 = (TextView) base.findViewById(R.id.txtAge);
			}
			return lable8;
		}

		TextView getHopital() {
			if (lable6 == null) {
				lable6 = (TextView) base.findViewById(R.id.txtHopital);
			}
			return lable6;
		}

		TextView getAddress() {
			if (lable7 == null) {
				lable7 = (TextView) base.findViewById(R.id.txtAddress);
			}
			return lable7;
		}

		TextView getTitleSex() {
			if (lable9 == null) {
				lable9 = (TextView) base.findViewById(R.id.txtTilteSex);
			}
			return lable9;
		}

		TextView getTitleAge() {
			if (lable10 == null) {
				lable10 = (TextView) base.findViewById(R.id.txtTilteAge);
			}
			return lable10;
		}

		TextView getKBN() {
			if (lable11 == null) {
				lable11 = (TextView) base.findViewById(R.id.btnKBN);
			}
			return lable11;
		}

		LinearLayout getRow() {
			if (layoutrow == null) {
				layoutrow = (LinearLayout) base.findViewById(R.id.layoutRow);
			}
			return layoutrow;
		}
	}
}
