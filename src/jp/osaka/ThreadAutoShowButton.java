package jp.osaka;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ThreadAutoShowButton {

	// Context con;
	private boolean Running = true;
	Activity ac;
	// private boolean bShowDesaster = false;
	// private boolean bShowNotification = false;
	String FireStationCode;
	String CaseCode;
	long TimeIntervalPush;
	String HeaderBackcolorOnDisaster = "";
	String HeaderBackcolorOnEmergency = "";
	long PlaySoundOnDisaster;
	long PlaySoundOnNotification;
	boolean bAllowOpen;
	Bell be;
	String SHCACode;

	public boolean isRunning() {
		return Running;
	}

	public void setRunning(boolean running) {
		Music.stop(ac);
		Running = running;
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);

			Bundle b = msg.getData();
			String key = b.getString("Key");
			if (Running) {
				if (key.equals("1")) {
					// playSound();
					// Button btnMeg=(Button)ac.findViewById(R.id.btnMessage);
					// btnMeg.setVisibility(View.VISIBLE);
					Toast.makeText(ac, ContantMessages.Disaster,
							Toast.LENGTH_LONG).show();
					// bShowDesaster = true;
					// Bell be = new Bell();
					be.Rang(ac, true);

				} else if (key.equals("2")) {
					// playSound();
					// Button btnMeg=(Button)ac.findViewById(R.id.btnMessage);
					// btnMeg.setVisibility(View.VISIBLE);
					// bShowNotification = true;
					be.Rang(ac, false);

				}
			}
		}

	};

	// MediaPlayer mp;
	// private void playSound() {
	// mp = MediaPlayer.create(ac, R.raw.camera);
	// // mp.setOnCompletionListener(new OnCompletionListener() {
	// //
	// // public void onCompletion(MediaPlayer mp) {
	// // // TODO Auto-generated method stub
	// // mp.release();
	// // }
	// // });
	// Handler h = new Handler();
	// Runnable stopPlaybackRun = new Runnable() {
	// public void run(){
	// mp.stop();
	// mp.release();
	// }
	// };
	// h.postDelayed(stopPlaybackRun, 10 * 1000);//10s
	// mp.setLooping(true);
	// mp.start();
	// }
	Button btnBell;
	LinearLayout lHeader;
	boolean isShow = false;

	public void showButtonAuto(Activity act, String fireStationCode,
			String caseCode, boolean open) {
		// con = context;
		try {

			ac = act;
			FireStationCode = fireStationCode;
			CaseCode = caseCode;
			bAllowOpen = open;
			be = new Bell();
			isShow = false;
			lHeader = (LinearLayout) ac.findViewById(R.id.bgHeader);
			lHeader.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Music.stop(ac);
					be.StopHeader();
					SharedPreferences sh = ac.getSharedPreferences("app",
							Context.MODE_PRIVATE);
					int mode = sh.getInt("Mode", 0);
					if (mode == 2) {
						lHeader.setBackgroundColor(Color
								.parseColor(HeaderBackcolorOnEmergency));
					} else if (mode == 1) {
						lHeader.setBackgroundColor(Color
								.parseColor(HeaderBackcolorOnDisaster));
					} else
						lHeader.setBackgroundResource(R.drawable.header);
				}
			});

			btnBell = (Button) ac.findViewById(R.id.btnChuong);
			btnBell.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					// if (bShowDesaster || bShowNotification) {
					CL_TR_kin_yose cl = new CL_TR_kin_yose(ac);
					int res = cl.checkDisasterNotProcess(SHCACode);

					SharedPreferences sh = ac.getSharedPreferences("app",
							Context.MODE_PRIVATE);
					String newsms = sh.getString("NewSMS", "0");

					if (res > 0 || newsms.equals("1")) {
						// if (isShow == false) {
						btnBell.setBackgroundResource(R.drawable.chuong);
						int mode = sh.getInt("Mode", 0);
						if (mode == 2) {
							lHeader.setBackgroundColor(Color
									.parseColor(HeaderBackcolorOnEmergency));
						} else if (mode == 1) {
							lHeader.setBackgroundColor(Color
									.parseColor(HeaderBackcolorOnDisaster));
						} else
							lHeader.setBackgroundResource(R.drawable.header);
						Music.stop(ac);
						be.StopRung();
						isShow = true;
						showDialogButton();

						// } else {
						// isShow = false;
						// dialogMg.dismiss();
						// }
					}

				}
			});
			Running = true;
			getConfigTimeRun();
			startThread();

			SharedPreferences sh = ac.getSharedPreferences("app",
					Context.MODE_PRIVATE);
			int mode = sh.getInt("Mode", 0);
			if (mode == 2) {
				lHeader.setBackgroundColor(Color
						.parseColor(HeaderBackcolorOnEmergency));
			} else if (mode == 1) {
				lHeader.setBackgroundColor(Color
						.parseColor(HeaderBackcolorOnDisaster));
			} else
				lHeader.setBackgroundResource(R.drawable.header);
			//
			Cl_EMSUnit em = new Cl_EMSUnit(ac);
			Cursor cur = em.getSHCACode();
			if (cur != null && cur.getCount() > 0) {
				SHCACode = cur.getString(cur.getColumnIndex("SHCACode"));
			}
			cur.close();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), ac);
		}
	}

	private void getConfigTimeRun() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(ac);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {

				TimeIntervalPush = Long.parseLong(cur.getString(cur
						.getColumnIndex("TimeIntervalPush")));
				PlaySoundOnDisaster = Long.parseLong(cur.getString(cur
						.getColumnIndex("soundplaytimeondisaster")));
				PlaySoundOnNotification = Long.parseLong(cur.getString(cur
						.getColumnIndex("soundplaytimeonnotification")));
				HeaderBackcolorOnDisaster = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnDisaster"));
				HeaderBackcolorOnEmergency = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnEmergency"));

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	Dialog dialogMg;

	private void showDialogButton() {
		dialogMg = new Dialog(ac);
		dialogMg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogMg.setContentView(R.layout.layoutmenubutton);
		// dialogMg.setCanceledOnTouchOutside(false);
		Window window = dialogMg.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		TextView btnDesaster = (TextView) dialogMg
				.findViewById(R.id.btnDesaster);
		btnDesaster.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				// bShowDesaster = false;
				isShow = false;
				if (bAllowOpen) {
					Intent myinten = new Intent(ac, E035.class);
					Bundle b = new Bundle();
					b.putInt("Key", 201);
					myinten.putExtras(b);
					ac.startActivityForResult(myinten, 201);
				}
				dialogMg.cancel();
			}
		});
		TextView btnNomal = (TextView) dialogMg.findViewById(R.id.btnNomal);
		btnNomal.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				// bShowDesaster = false;
				isShow = false;
				// if (bAllowOpen) {
				SharedPreferences sh = ac.getSharedPreferences("app",
						Context.MODE_PRIVATE);
				Editor e = sh.edit();
				e.putInt("Mode", 0);
				e.commit();
				lHeader.setBackgroundResource(R.drawable.header);

				ThreadSendGPS.setRunning(false);
				// if (bAllowOpen) {
				// Intent myinten = new Intent(ac, E035.class);
				// Bundle b = new Bundle();
				// b.putInt("Key", 202);
				// myinten.putExtras(b);
				// ac.startActivityForResult(myinten, 202);
				// }
				// }
				if (ac.getClass().getSimpleName().equals("E008")) {
					Button btnE8ToE91 = (Button) ac
							.findViewById(R.id.btnE8NextE91);
					Button btnE8ToE92 = (Button) ac
							.findViewById(R.id.btnE8NextE92);
					btnE8ToE91.setText("疾病　初期評価");
					btnE8ToE92.setText("外傷　初期評価");

				}
				dialogMg.cancel();
			}
		});
		SharedPreferences sh = ac.getSharedPreferences("app",
				Context.MODE_PRIVATE);
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, 0);
		String kinno = sh.getString("kin_no", "");

		CL_TR_kin_yose cl = new CL_TR_kin_yose(ac);
		int res = cl.checkDisasterNotProcess(kinno, SHCACode);
		if (res == 0) {
			btnDesaster.setLayoutParams(param);
		}
		// if (!bAllowOpen) {
		// int mode = sh.getInt("Mode", 0);
		// if (mode != 0) {
		// btnDesaster.setLayoutParams(param);
		// } else {
		// btnNomal.setLayoutParams(param);
		// }
		// }
		int mode = sh.getInt("Mode", 0);
		if (mode == 0)
			btnNomal.setLayoutParams(param);
		TextView btnNotification = (TextView) dialogMg
				.findViewById(R.id.btnNotification);

		String newsms = sh.getString("NewSMS", "0");
		if (newsms.equals("0"))
			btnNotification.setLayoutParams(param);
		btnNotification.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				isShow = false;
				SharedPreferences sh = ac.getSharedPreferences("app",
						Context.MODE_PRIVATE);
				Editor e = sh.edit();
				e.putString("NewSMS", "0");
				e.commit();
				Intent myinten = new Intent(v.getContext(), E021.class);
				Bundle b7 = new Bundle();
				b7.putString("FireStationCode", FireStationCode);
				b7.putString("CaseCode", CaseCode);
				myinten.putExtras(b7);
				ac.startActivity(myinten);

				dialogMg.cancel();
				if (ac.getClass().getSimpleName().equals("E021")) {
					ac.finish();
				}
			}
		});
		wlp.gravity = Gravity.TOP | Gravity.RIGHT;
		// wlp.x = 100; // x position
		wlp.y = 80; // y position
		wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);

		dialogMg.show();
	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub

			try {

				boolean bDisaster, bNotification = false, bPass = false;
				while (Running) {
					try {

						Thread.sleep(TimeIntervalPush);

						if (bNotification) {
							if (TimeIntervalPush < PlaySoundOnNotification)
								Thread.sleep(PlaySoundOnNotification);
						}
						bDisaster = false;
						bNotification = false;
						@SuppressWarnings("static-access")
						SharedPreferences sh = ac.getSharedPreferences("app",
								ac.MODE_PRIVATE);
						bPass = false;
						// int Disaster = sh.getInt("Disaster", 0);
						CL_TR_kin_yose tr = new CL_TR_kin_yose(ac);
						String date1 = tr.checkExistDisaster(SHCACode);
						Cl_ReportingAll rp = new Cl_ReportingAll(ac);
						String date2 = rp.checkexistTR_ReportingAll();
						if (date1.equals("") == false
								&& date2.equals("") == false) {
							if (Long.parseLong(date1) > Long.parseLong(date2)) {
								bPass = true;
							}
						}
						if (!bPass) {
							// int res = tr.checkExistDisaster();
							if (date1.equals("") == false) {
								tr = new CL_TR_kin_yose(ac);
								tr.UpdateDisaster(SHCACode);
								bDisaster = true;

								Message mg = handler.obtainMessage();
								Bundle b = new Bundle();
								b.putString("Key", "1");
								mg.setData(b);
								handler.sendMessage(mg);

							}

							if (bDisaster) {
								if (TimeIntervalPush < PlaySoundOnDisaster)
									Thread.sleep(PlaySoundOnDisaster);
							}
						}
						if (bDisaster == false) {
							// String newsms = sh.getString("NewSMS", "0");
							if (date2.equals("") == false) {
								rp = new Cl_ReportingAll(ac);
								rp.UpdateReportingAll();
								Editor e = sh.edit();
								e.putString("NewSMS", "1");
								e.commit();
								bNotification = true;

								Message mg = handler.obtainMessage();
								Bundle b = new Bundle();
								b.putString("Key", "2");
								mg.setData(b);
								handler.sendMessage(mg);

							}
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");

			} finally {

			}

		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

}
