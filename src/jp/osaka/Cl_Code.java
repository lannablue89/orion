﻿package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_Code {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_Code(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	// MS_Code
	public long createMS_Code(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("MS_Code", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getMS_Code(String codetype) {
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateNow();
			String[] args = { date, codetype };
			String sql = "Select * from MS_Code Where ? between IfNULL(Start_DATE,'00010101') AND IfNULL(End_DATE,'99999999') and CodeType=? order by SortNo ASC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getMS_CodeName(String codetype, String CodeID) {
		String name = "";
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateNow();
			String[] args = { date, codetype, CodeID };
			String sql = "Select CodeName from MS_Code Where ? between IfNULL(Start_DATE,'00010101') AND IfNULL(End_DATE,'99999999') and CodeType=? and CodeID=? order by SortNo ASC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0)
					name = mCursor
							.getString(mCursor.getColumnIndex("CodeName"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return name;
	}

	// MS_SecondaryHealthCareArea
	public Cursor getMS_SecondaryHealthCareArea() {
		Cursor mCursor = null;
		try {

			String sql = "Select * from MS_SecondaryHealthCareArea Where DeleteFLG=0 order by SortNo ASC";
			mCursor = database.rawQuery(sql, null);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getMS_Area(String SHCACode) {
		Cursor mCursor = null;
		try {
			String[] args = { SHCACode };
			String sql = "Select * from MS_Area Where DeleteFLG=0 and SHCACode=? order by SortNo ASC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
