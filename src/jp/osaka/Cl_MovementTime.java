﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_MovementTime {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_MovementTime(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	// MS_MovementTime
	public Cursor getMS_MovementTime(String FireStationCode,
			String TransferFLG, String ControlType) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, TransferFLG, ControlType };
			String sql = "Select * from MS_MovementTime Where FireStationCode=? and TransferFLG=? and ControlType=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// TR_MovementTime
	public long createTR_MovementTime(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("TR_MovementTime", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long createTR_MovementTime(String FireStationCode, String CaseNo,
			String MovementCode, String Movement_DATETIME) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("MovementCode", MovementCode);
			values.put("Movement_DATETIME", Movement_DATETIME);
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			result = database.insert("TR_MovementTime", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long updateTR_MovementTime(String FireStationCode, String CaseNo,
			String MovementCode, String Movement_DATETIME) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("Movement_DATETIME", Movement_DATETIME);
			values.put("Update_DATETIME", Utilities.getDateNow());
			String where = "FireStationCode=? and CaseNo=? and MovementCode=?";
			String[] args = { FireStationCode, CaseNo, MovementCode };

			result = database.update("TR_MovementTime", values, where, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteTR_MovementTime(String FireStationCode, String CaseNo,
			String MovementCode) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and MovementCode=?";
			String[] args = { FireStationCode, CaseNo, MovementCode };
			result = database.delete("TR_MovementTime", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_MovementTime(String FireStationCode, String CaseNo,
			String MovementCode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, MovementCode };
			String sql = "Select * from TR_MovementTime Where  FireStationCode=? and CaseNo=? and MovementCode=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_MovementTimeAll(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_MovementTime Where  FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String getMinTime(String FireStationCode, String CaseNo) {

		String time = "";
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select Movement_DATETIME from TR_MovementTime Where  FireStationCode=? and CaseNo=? and MovementCode='00001' ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					time = mCursor.getString(mCursor
							.getColumnIndex("Movement_DATETIME"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return time;
	}

	public Cursor getTR_MovementTimeDoctor(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_MovementTime Where  FireStationCode=? and CaseNo=? and MovementCode in ('00005','00011','00009','00007') order by MovementCode DESC Limit 1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int countTR_MovementTimeDoctor(String FireStationCode, String CaseNo) {

		int res = 0;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_MovementTime Where  FireStationCode=? and CaseNo=? and MovementCode in ('00005','00011','00009','00007') order by MovementCode DESC Limit 1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				res = mCursor.getCount();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return res;
	}

	public Cursor getTR_MovementTimeDoctorInAdmission(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select  distinct FireStationCode,CaseNo,Insert_DATETIME,Update_DATETIME from TR_MedicalInstitutionAdmission Where  FireStationCode=? and CaseNo=? and Admission_CD='00002' Limit 1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

}
