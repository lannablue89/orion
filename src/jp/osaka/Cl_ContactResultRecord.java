﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Cl_ContactResultRecord {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_ContactResultRecord(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createTR_ContactResultRecord(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("TR_ContactResultRecord", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long createTR_ContactResultRecord(String FireStationCode,
			String CaseNo, int ContactCount, String MICode,
			String ContactResult_CD, String ContactResult_TEXT, String E,
			String HopitalName, boolean InstructorUseFlg) {
		long result = -1;
		database.beginTransaction();
		try {
			// Bug 150
			String TerminalNo = "0";
			String EMSUnitCode = "";
			String date = Utilities.getDateTimeNow();
			String[] argst = { date, date };
			String sql1 = "Select a.TerminalNo,a.EMSUnitCode from MS_EMSUnitTerminalManagement a Inner Join M_User b On a.EMSUnitCode=b.UserName  Where a.DeleteFLG=0 "
					+ " and ifnull(a.Start_DATETIME,'00010101000000')<=? "
					+ " and ifnull(a.End_DATETIME,'99999999999999')>=? ";
			Cursor mCursor1 = database.rawQuery(sql1, argst);
			if (mCursor1 != null) {
				mCursor1.moveToFirst();
				if (mCursor1.getCount() > 0) {
					TerminalNo = mCursor1.getString(mCursor1
							.getColumnIndex("TerminalNo"));
					EMSUnitCode = mCursor1.getString(mCursor1
							.getColumnIndex("EMSUnitCode"));
				}
			}
			//

			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("ContactCount", ContactCount);
			values.put("MICode", MICode);
			values.put("Contact_DATETIME", Utilities.getDateTimeNow());
			values.put("ContactResult_CD", ContactResult_CD);
			if (ContactResult_TEXT != null && !ContactResult_TEXT.equals(""))
				values.put("ContactResult_TEXT", ContactResult_TEXT);
			else
				values.putNull("ContactResult_TEXT");
			if (InstructorUseFlg)
				values.put("InstructorUseFlg", "1");
			else
				values.put("InstructorUseFlg", "0");
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			result = database.insert("TR_ContactResultRecord", null, values);
			if (ContactResult_CD.equals("00001")) {
				String[] args = { FireStationCode, CaseNo, MICode,
						ContactResult_CD, TerminalNo };
				String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
				Cursor mCursor = database.rawQuery(sql, args);
				if (mCursor != null && mCursor.getCount() > 0) {
					values = new ContentValues();
					// values.put("FireStationCode", FireStationCode);
					// values.put("CaseNo", CaseNo);
					// values.put("MICode", MICode);
					// values.put("Admission_CD", ContactResult_CD);
					// values.put("Insert_DATETIME",
					// Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());
					String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
					String[] whereArgs = { FireStationCode, CaseNo, MICode,
							ContactResult_CD, TerminalNo };
					result = database.update("TR_MedicalInstitutionAdmission",
							values, where, whereArgs);

				} else {

					values = new ContentValues();
					values.put("FireStationCode", FireStationCode);
					values.put("CaseNo", CaseNo);
					values.put("MICode", MICode);
					values.put("Admission_CD", ContactResult_CD);
					values.put("TerminalNo", TerminalNo);// Bug 150
					values.put("Insert_DATETIME", Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());

					result = database.insert("TR_MedicalInstitutionAdmission",
							null, values);
				}
				// Bug 339 20121114

				values = new ContentValues();
				if (E.equals("E014A")) {

					values.put(TR_CaseCount.SearchType_CD, "00001");

				} else if (E.equals("E014B")) {
					values.put(TR_CaseCount.SearchType_CD, "00002");
				} else if (E.equals("E014C")) {
					values.put(TR_CaseCount.SearchType_CD, "00003");
				} else if (E.equals("E017") && !MICode.equals(BaseCount.Other)) {
					values.put(TR_CaseCount.SearchType_CD, "00004");
				} else {
					values.put(TR_CaseCount.SearchType_CD, "00005");
				}
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				values.put("ContactCount", ContactCount);
				values.put("FacilitiesCode", MICode);
				values.put("FacilitiesName", HopitalName);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			} else {
				values = new ContentValues();
				values.put("ContactCount", ContactCount);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			}
			//

			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		} finally {
			database.endTransaction();
			database.close();

		}
		return result;
	}

	public long createTR_ContactResultRecord(String FireStationCode,
			String CaseNo, int ContactCount, String MICode,
			String ContactResult_CD, String ContactResult_TEXT, String E,
			boolean InstructorUseFlg) {
		long result = -1;
		database.beginTransaction();
		try {
			// Bug 150
			String TerminalNo = "0";
			String EMSUnitCode = "";
			String date = Utilities.getDateTimeNow();
			String[] argst = { date, date };
			String sql1 = "Select a.TerminalNo,a.EMSUnitCode from MS_EMSUnitTerminalManagement a Inner Join M_User b On a.EMSUnitCode=b.UserName  Where a.DeleteFLG=0 "
					+ " and ifnull(a.Start_DATETIME,'00010101000000')<=? "
					+ " and ifnull(a.End_DATETIME,'99999999999999')>=? ";
			Cursor mCursor1 = database.rawQuery(sql1, argst);
			if (mCursor1 != null) {
				mCursor1.moveToFirst();
				if (mCursor1.getCount() > 0) {
					TerminalNo = mCursor1.getString(mCursor1
							.getColumnIndex("TerminalNo"));
					EMSUnitCode = mCursor1.getString(mCursor1
							.getColumnIndex("EMSUnitCode"));
				}
			}
			//

			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("ContactCount", ContactCount);
			values.put("MICode", MICode);
			values.put("Contact_DATETIME", Utilities.getDateTimeNow());
			values.put("ContactResult_CD", ContactResult_CD);
			if (ContactResult_TEXT != null && !ContactResult_TEXT.equals(""))
				values.put("ContactResult_TEXT", ContactResult_TEXT);
			else
				values.putNull("ContactResult_TEXT");
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			if (InstructorUseFlg)
				values.put("InstructorUseFlg", "1");
			else
				values.put("InstructorUseFlg", "0");

			result = database.insert("TR_ContactResultRecord", null, values);
			if (ContactResult_CD.equals("00001")) {
				String[] args = { FireStationCode, CaseNo, MICode,
						ContactResult_CD, TerminalNo };
				String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
				Cursor mCursor = database.rawQuery(sql, args);
				if (mCursor != null && mCursor.getCount() > 0) {
					values = new ContentValues();
					// values.put("FireStationCode", FireStationCode);
					// values.put("CaseNo", CaseNo);
					// values.put("MICode", MICode);
					// values.put("Admission_CD", ContactResult_CD);
					// values.put("Insert_DATETIME",
					// Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());
					String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
					String[] whereArgs = { FireStationCode, CaseNo, MICode,
							ContactResult_CD, TerminalNo };
					result = database.update("TR_MedicalInstitutionAdmission",
							values, where, whereArgs);

				} else {

					values = new ContentValues();
					values.put("FireStationCode", FireStationCode);
					values.put("CaseNo", CaseNo);
					values.put("MICode", MICode);
					values.put("Admission_CD", ContactResult_CD);
					values.put("TerminalNo", TerminalNo);// Bug 150
					values.put("Insert_DATETIME", Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());

					result = database.insert("TR_MedicalInstitutionAdmission",
							null, values);
				}
				// Bug 339 20121114

				values = new ContentValues();
				if (E.equals("E014A")) {

					values.put(TR_CaseCount.SearchType_CD, "00001");

				} else if (E.equals("E014B")) {
					values.put(TR_CaseCount.SearchType_CD, "00002");
				} else if (E.equals("E014C")) {
					values.put(TR_CaseCount.SearchType_CD, "00003");
				} else if (E.equals("E017") && !MICode.equals(BaseCount.Other)) {
					values.put(TR_CaseCount.SearchType_CD, "00004");
				} else {
					values.put(TR_CaseCount.SearchType_CD, "00005");
				}
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				values.put("ContactCount", ContactCount);
				values.put("FacilitiesCode", MICode);
				// values.put("FacilitiesName", HopitalName);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			} else {
				values = new ContentValues();
				values.put("ContactCount", ContactCount);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			}
			//

			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		} finally {
			database.endTransaction();
			database.close();

		}
		return result;
	}

	public String getContactCount(String FireStationCode, String CaseNo) {
		String textresult = null;
		Cursor mCursor = null;
		String[] args = { FireStationCode, CaseNo };
		try {
			String sql = "Select ContactCount From tr_case Where FireStationCode=? and CaseCode=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					textresult = mCursor.getString(mCursor
							.getColumnIndex("ContactCount"));
					if (textresult == null)
						textresult = "0";
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return textresult;
	}

	// Bug 340 select hopital for E014 20121114
	public long createTR_ContactResultRecordforE014(String FireStationCode,
			String CaseNo, int ContactCount, String MICode,
			String ContactResult_CD, String ContactResult_TEXT, String E,
			boolean man, boolean female, String EMSUnitCode,
			String KamokuName01, String KamokuName02, String KamokuName03,
			String HopitalName, boolean InstructorUseFlg) {
		long result = -1;
		database.beginTransaction();
		try {
			// Bug 150
			String TerminalNo = "0";
			String date = Utilities.getDateTimeNow();
			String[] argst = { date, date };
			String sql1 = "Select a.TerminalNo from MS_EMSUnitTerminalManagement a Inner Join M_User b On a.EMSUnitCode=b.UserName  Where a.DeleteFLG=0 "
					+ " and ifnull(a.Start_DATETIME,'00010101000000')<=? "
					+ " and ifnull(a.End_DATETIME,'99999999999999')>=? ";
			Cursor mCursor1 = database.rawQuery(sql1, argst);
			if (mCursor1 != null) {
				mCursor1.moveToFirst();
				if (mCursor1.getCount() > 0) {
					TerminalNo = mCursor1.getString(mCursor1
							.getColumnIndex("TerminalNo"));
				}
			}
			//

			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("ContactCount", ContactCount);
			values.put("MICode", MICode);
			values.put("Contact_DATETIME", Utilities.getDateTimeNow());
			values.put("ContactResult_CD", ContactResult_CD);
			if (ContactResult_TEXT != null && !ContactResult_TEXT.equals(""))
				values.put("ContactResult_TEXT", ContactResult_TEXT);
			else
				values.putNull("ContactResult_TEXT");
			if (InstructorUseFlg)
				values.put("InstructorUseFlg", "1");
			else
				values.put("InstructorUseFlg", "0");

			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			result = database.insert("TR_ContactResultRecord", null, values);
			if (ContactResult_CD.equals("00001")) {
				String[] args = { FireStationCode, CaseNo, MICode,
						ContactResult_CD, TerminalNo };
				String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
				Cursor mCursor = database.rawQuery(sql, args);
				if (mCursor != null && mCursor.getCount() > 0) {
					values = new ContentValues();
					// values.put("FireStationCode", FireStationCode);
					// values.put("CaseNo", CaseNo);
					// values.put("MICode", MICode);
					// values.put("Admission_CD", ContactResult_CD);
					// values.put("Insert_DATETIME",
					// Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("KamokuName01", KamokuName01);
					values.put("KamokuName02", KamokuName02);
					values.put("KamokuName03", KamokuName03);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());
					String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
					String[] whereArgs = { FireStationCode, CaseNo, MICode,
							ContactResult_CD, TerminalNo };
					result = database.update("TR_MedicalInstitutionAdmission",
							values, where, whereArgs);

				} else {

					values = new ContentValues();
					values.put("FireStationCode", FireStationCode);
					values.put("CaseNo", CaseNo);
					values.put("MICode", MICode);
					values.put("Admission_CD", ContactResult_CD);
					values.put("TerminalNo", TerminalNo);// Bug 150
					values.put("Insert_DATETIME", Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					values.put("EMSUnitCode", EMSUnitCode);
					values.put("KamokuName01", KamokuName01);
					values.put("KamokuName02", KamokuName02);
					values.put("KamokuName03", KamokuName03);
					values.put("Movement_DATETIME", Utilities.getDateTimeNow());

					result = database.insert("TR_MedicalInstitutionAdmission",
							null, values);
				}
				// Bug 339 20121114

				values = new ContentValues();
				if (E.equals("E014A")) {

					values.put(TR_CaseCount.SearchType_CD, "00001");

				} else if (E.equals("E014B")) {
					values.put(TR_CaseCount.SearchType_CD, "00002");
					// Bug 340 20121114
					if (man)
						values.put(TR_CaseCount.BedStatus_MALE, "1");
					else
						values.put(TR_CaseCount.BedStatus_MALE, "0");
					if (female)
						values.put(TR_CaseCount.BedStatus_FEMALE, "1");
					else
						values.put(TR_CaseCount.BedStatus_FEMALE, "0");
					//
				} else if (E.equals("E014C")) {
					values.put(TR_CaseCount.SearchType_CD, "00003");
				} else if (E.equals("E017") && !MICode.equals(BaseCount.Other)) {
					values.put(TR_CaseCount.SearchType_CD, "00004");
				} else {
					values.put(TR_CaseCount.SearchType_CD, "00005");
				}
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				values.put("ContactCount", ContactCount);
				values.put("FacilitiesCode", MICode);
				values.put("FacilitiesName", HopitalName);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			} else {
				values = new ContentValues();
				values.put("ContactCount", ContactCount);
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			}
			//

			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		} finally {
			database.endTransaction();
			database.close();

		}
		return result;
	}

	public long insertTR_MedicalInstitutionAdmission(String FireStationCode,
			String CaseNo, String MICode, String TerminalNo,
			String Admission_CD, String EMSUnitCode, String KamokuName01,
			String KamokuName02, String KamokuName03) {
		long result = 0;
		try {
			// res = database.replace("TR_MedicalInstitutionAdmission", null,
			// values);

			String[] args = { FireStationCode, CaseNo, MICode, Admission_CD,
					TerminalNo };
			String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
			Cursor mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				ContentValues values = new ContentValues();

				values.put("DeleteFLG", "0");
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				values.put("EMSUnitCode", EMSUnitCode);
				values.put("KamokuName01", KamokuName01);
				values.put("KamokuName02", KamokuName02);
				values.put("KamokuName03", KamokuName03);
				values.put("Movement_DATETIME", Utilities.getDateTimeNow());
				String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
				String[] whereArgs = { FireStationCode, CaseNo, MICode,
						Admission_CD, TerminalNo };
				result = database.update("TR_MedicalInstitutionAdmission",
						values, where, whereArgs);

			} else {

				ContentValues values = new ContentValues();
				values.put("FireStationCode", FireStationCode);
				values.put("CaseNo", CaseNo);
				values.put("MICode", MICode);
				values.put("Admission_CD", Admission_CD);
				values.put("TerminalNo", TerminalNo);// Bug 150
				values.put("Insert_DATETIME", Utilities.getDateTimeNow());
				values.put("DeleteFLG", "0");
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				values.put("EMSUnitCode", EMSUnitCode);
				values.put("KamokuName01", KamokuName01);
				values.put("KamokuName02", KamokuName02);
				values.put("KamokuName03", KamokuName03);
				values.put("Movement_DATETIME", Utilities.getDateTimeNow());

				result = database.insert("TR_MedicalInstitutionAdmission",
						null, values);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {
			database.close();
		}
		return result;

	}

	public int deleteTR_ContactResultRecord(String FireStationCode,
			String CaseNo, String contactcount) {
		int result = 0;
		try {
			String[] args = { FireStationCode, CaseNo, contactcount };
			result = database.delete("tr_contactresultrecord",
					"FireStationCode=? and CaseNo=? and ContactCount=?", args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {
			database.close();
		}
		return result;

	}

	public int deleteTR_Admisstion(String FireStationCode, String CaseNo,
			String MICode, String TerminalNo, String Admission_CD) {
		int res = 0;
		try {
			res = database
					.delete("TR_MedicalInstitutionAdmission",
							"FireStationCode=? and CaseNo=? and MICode=? and TerminalNo=? and  Admission_CD=?",
							new String[] { FireStationCode, CaseNo, MICode,
									TerminalNo, Admission_CD });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {
			database.close();
		}
		return res;
	}

	public int cancelTR_Admisstion(String FireStationCode, String CaseNo,
			String MICode, String TerminalNo, String Admission_CD) {
		int res = 0;
		try {
			ContentValues values = new ContentValues();

			values.put("DeleteFLG", "1");
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
			String[] whereArgs = { FireStationCode, CaseNo, MICode,
					Admission_CD, TerminalNo };
			res = database.update("TR_MedicalInstitutionAdmission", values,
					where, whereArgs);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {
			database.close();
		}
		return res;
	}

	public long updateTR_ContactResultRecord(String FireStationCode,
			String CaseNo, String ContactCount, String MICode,
			String ContactResult_CD, String ContactResult_TEXT,
			boolean InstructorUseFlg) {
		long result = -1;
		database.beginTransaction();
		try {

			// Bug 150
			String TerminalNo = "0";
			String date = Utilities.getDateTimeNow();
			String[] argst = { date, date };
			String sql1 = "Select a.TerminalNo from MS_EMSUnitTerminalManagement a Inner Join M_User b On a.EMSUnitCode=b.UserName  Where a.DeleteFLG=0 "
					+ " and ifnull(a.Start_DATETIME,'00010101000000')<=? "
					+ " and ifnull(a.End_DATETIME,'99999999999999')>=? ";
			Cursor mCursor1 = database.rawQuery(sql1, argst);
			if (mCursor1 != null) {
				mCursor1.moveToFirst();
				if (mCursor1.getCount() > 0) {
					TerminalNo = mCursor1.getString(mCursor1
							.getColumnIndex("TerminalNo"));
				}
			}
			//
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("ContactCount", ContactCount);
			values.put("Contact_DATETIME", Utilities.getDateTimeNow());
			values.put("ContactResult_CD", ContactResult_CD);
			if (ContactResult_TEXT != null && !ContactResult_TEXT.equals(""))
				values.put("ContactResult_TEXT", ContactResult_TEXT);
			else
				values.putNull("ContactResult_TEXT");
			if (ContactResult_CD.equals("00001")) {
				if (InstructorUseFlg)
					values.put("InstructorUseFlg", "1");
				else
					values.put("InstructorUseFlg", "0");
			}
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			String whereClause = "FireStationCode=? and CaseNo=? and ContactCount=?";
			String[] args = { FireStationCode, CaseNo, ContactCount };
			result = database.update("TR_ContactResultRecord", values,
					whereClause, args);
			if (!ContactResult_CD.equals("00001")) {
				result = database
						.delete("TR_MedicalInstitutionAdmission",
								"FireStationCode=? and CaseNo=? and MICode=? and TerminalNo=? and  (Admission_CD='00001' or Admission_CD='00002')",
								new String[] { FireStationCode, CaseNo, MICode,
										TerminalNo });
				result = database
						.delete("TR_MovementTime",
								"FireStationCode=? and CaseNo=? and MovementCode In ('00005','00007','00009','00011','00012','00013')",
								new String[] { FireStationCode, CaseNo });

				// values = new ContentValues();
				// values.put("DeleteFLG", "1");
				// values.put("Update_DATETIME", Utilities.getDateTimeNow());
				// String where =
				// "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=? and  (Admission_CD='00001' or Admission_CD='00002')";
				// String[] whereArgs = { FireStationCode, CaseNo, MICode,
				// TerminalNo };
				// result = database.update("TR_MedicalInstitutionAdmission",
				// values, where, whereArgs);
			}
			if (ContactResult_CD.equals("00001")) {
				String[] args1 = { FireStationCode, CaseNo, MICode,
						ContactResult_CD, TerminalNo };
				String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
				Cursor mCursor = database.rawQuery(sql, args1);
				if (mCursor != null && mCursor.getCount() > 0) {
					values = new ContentValues();
					// values.put("FireStationCode", FireStationCode);
					// values.put("CaseNo", CaseNo);
					// values.put("MICode", MICode);
					values.put("Admission_CD", ContactResult_CD);
					// values.put("Insert_DATETIME",
					// Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					String where = "FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=?";
					String[] whereArgs = { FireStationCode, CaseNo, MICode,
							ContactResult_CD };
					result = database.update("TR_MedicalInstitutionAdmission",
							values, where, whereArgs);

				} else {

					values = new ContentValues();
					values.put("FireStationCode", FireStationCode);
					values.put("CaseNo", CaseNo);
					values.put("MICode", MICode);
					values.put("Admission_CD", ContactResult_CD);
					values.put("Insert_DATETIME", Utilities.getDateTimeNow());
					values.put("DeleteFLG", "0");
					values.put("Update_DATETIME", Utilities.getDateTimeNow());
					result = database.insert("TR_MedicalInstitutionAdmission",
							null, values);
				}
			}
			// Bug 339 20121114
			if (!ContactResult_CD.equals("00001")) {
				values = new ContentValues();
				values.putNull(TR_CaseCount.SearchType_CD);
				values.putNull(TR_CaseCount.BedStatus_FEMALE);
				values.putNull(TR_CaseCount.BedStatus_MALE);
				values.putNull("FacilitiesCode");
				values.putNull("FacilitiesName");
				values.put("Update_DATETIME", Utilities.getDateTimeNow());
				String where = "FireStationCode=? and CaseCode=? ";
				String[] whereArgs = { FireStationCode, CaseNo };
				result = database.update(TR_CaseCount.TableName, values, where,
						whereArgs);
			}
			//
			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		} finally {
			database.endTransaction();
			database.close();

		}
		return result;
	}

	public long updateTR_ContactResultRecordE027(String FireStationCode,
			String CaseNo, String ContactCount, String ContactResult_CD,
			String ContactResult_TEXT) {
		long result = -1;
		database.beginTransaction();
		try {

			// Bug 150
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseNo);
			values.put("ContactCount", ContactCount);
			values.put("Contact_DATETIME", Utilities.getDateTimeNow());
			values.put("ContactResult_CD", ContactResult_CD);
			if (ContactResult_TEXT != null && !ContactResult_TEXT.equals(""))
				values.put("ContactResult_TEXT", ContactResult_TEXT);
			else
				values.putNull("ContactResult_TEXT");
			values.put("Update_DATETIME", Utilities.getDateTimeNow());
			String whereClause = "FireStationCode=? and CaseNo=? and ContactCount=?";
			String[] args = { FireStationCode, CaseNo, ContactCount };
			result = database.update("TR_ContactResultRecord", values,
					whereClause, args);
			database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		} finally {
			database.endTransaction();
			database.close();

		}
		return result;
	}

	public Cursor getTR_ContactResultRecord(String FireStationCode,
			String CaseNo, String MICode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo, MICode };
			String sql = "Select * from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? and MICode=?  order by Contact_DATETIME DESC Limit 4";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_ContactResultRecord027(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select crr.*,mc.codename as ContactResult_Name,"
					+ "coalesce(mi.miname_kanji,ni.miname_kanji,fmi.miname_kanji) as MIName_Kanji,"
					+ "coalesce(mi.mitype,ni.mitype,fmi.mitype) as MIType "
					+ "from TR_ContactResultRecord crr "
					+ "left join ms_medicalinstitution mi on mi.micode = crr.micode "
					+ "left join ms_code mc on mc.codeid = crr.contactresult_cd and mc.codetype = '00002' "
					+ "left join ms_nightclinic ni on ni.micode = crr.micode "
					+ "left join ms_firestationoriginalmi fmi on fmi.micode = crr.micode and fmi.firestationcode = crr.firestationcode and "
					+ "fmi.firestationoriginalcode = (select msmi.firestationoriginalcode from ms_firestationoriginalmi msmi "
					+ "where msmi.firestationcode = crr.firestationcode "
					+ " and msmi.micode = crr.micode "
					+ "order by msmi.sortno limit 1) "
					+ "Where crr.FireStationCode=? and crr.CaseNo=? "
					+ " order by crr.contactcount desc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_ContactResultRecord027_Select(String FireStationCode,
			String CaseNo, String ContactCount) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, ContactCount };
			String sql = "Select * From TR_ContactResultRecord Where FireStationCode=? and CaseNo=? and ContactCount=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_ContactResultRecordPatient(String FireStationCode,
			String CaseNo) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int getMaxContactCount(String FireStationCode, String CaseNo) {

		int result = 0;
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select Max(ContactCount) as ContactCount from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor.getColumnIndex("ContactCount")) != null) {
					result = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("ContactCount")));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public int countTR_ContactResultRecord(String FireStationCode, String CaseNo) {

		int result = 1;
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select count(*) as SL from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor.getColumnIndex("SL")) != null) {
					result = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("SL")));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor countTR_ContactResultRecord00001(String FireStationCode,
			String CaseNo) {

		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_ContactResultRecord Where ContactResult_CD='00001' and FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int checkExistTR_ContactResultRecord00001(String FireStationCode,
			String CaseNo) {
		int res = 0;
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_ContactResultRecord Where ContactResult_CD='00001' and FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				res = mCursor.getCount();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return res;
	}

	// 20121005: dai add : bug 48,49
	// get 4 JIAN nearest of hopital
	public Cursor getJianInHopital(String MICode) {
		Cursor mCursor = null;
		try {

			String[] args = { MICode };
			String sql = "Select distinct FireStationCode,CaseNo,Insert_DATETIME from TR_MedicalInstitutionAdmission Where MICode=? and Admission_CD='00001' and DeleteFLG='0' order by Insert_DATETIME DESC Limit 4";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// Bug 126
	public boolean checkExistTR_ContactResultRecord(String FireStationCode,
			String CaseNo, String MICode) {
		boolean exits = false;
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseNo, MICode };
			String sql = "Select * from TR_ContactResultRecord Where FireStationCode=? and CaseNo=? and MICode=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null && mCursor.getCount() > 0)
				return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return exits;
	}

	//
	public String getTerminalNo() {
		String ter = "0";
		Cursor mCursor = null;
		try {
			String date = Utilities.getDateTimeNow();
			String[] args = { date, date };
			String sql = "Select a.TerminalNo from MS_EMSUnitTerminalManagement a Inner Join M_User b On a.EMSUnitCode=b.UserName  Where a.DeleteFLG=0 "
					+ " and ifnull(a.Start_DATETIME,'00010101000000')<=? "
					+ " and ifnull(a.End_DATETIME,'99999999999999')>=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					ter = mCursor.getString(mCursor
							.getColumnIndex("TerminalNo"));
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return ter;
	}

	public Cursor getTR_MedicalInstitutionAdmission00001(
			String FireStationCode, String CaseNo, String MICode,
			String TerminalNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, MICode, "00001",
					TerminalNo };
			String sql = "Select * from TR_MedicalInstitutionAdmission Where FireStationCode=? and CaseNo=? and MICode=? and Admission_CD=? and TerminalNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return mCursor;
	}
}
