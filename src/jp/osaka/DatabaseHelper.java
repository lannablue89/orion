﻿package jp.osaka;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "APPLIDB";
	private static final int DATABASE_VERSION = 1;

	// private static final String DATABASE_CREATE =
	// "create table M_User  ( Id integer  primary key autoincrement,Name1 text not null,Name2 text not null,Name3 text not null,Name4 text not null);";
	// private static final String DATABASE_CREATE2 =
	// "create table VanChoi  ( Id integer  primary key autoincrement,userId integer not null,diem1 integer not null,diem2 integer not null,diem3 text not null,diem4 integer not null);";

	private static DatabaseHelper mInstance;

	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public static DatabaseHelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseHelper(context);
		}
		return mInstance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			db.execSQL(ContantCreateDatabase.CREATE_M_User1);
			db.execSQL(ContantCreateDatabase.CREATE_MS_SecondaryHealthCareArea2);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Area3);
			db.execSQL(ContantCreateDatabase.CREATE_MS_FireStation4);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Instructor5);
			db.execSQL(ContantCreateDatabase.CREATE_MS_EMSUnit6);
			db.execSQL(ContantCreateDatabase.CREATE_MS_EMSUnitTerminalManagement7);
			db.execSQL(ContantCreateDatabase.CREATE_MS_MedicalInstitution8);
			db.execSQL(ContantCreateDatabase.CREATE_MS_KamokuTelNo9);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Kamoku10);
			db.execSQL(ContantCreateDatabase.CREATE_MS_MedicalInstitutionClosed11);
			db.execSQL(ContantCreateDatabase.CREATE_MS_ViewingPermission12);
			db.execSQL(ContantCreateDatabase.CREATE_MS_MovementTime13);
			db.execSQL(ContantCreateDatabase.CREATE_MS_FireStationStandard14);
			db.execSQL(ContantCreateDatabase.CREATE_MS_FireStationStandardSort15);
			db.execSQL(ContantCreateDatabase.CREATE_MS_SHCAFireStationStandard16);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Category17);
			db.execSQL(ContantCreateDatabase.CREATE_MS_SHCACondition18);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Condition19);
			db.execSQL(ContantCreateDatabase.CREATE_MS_ConditionSort20);
			db.execSQL(ContantCreateDatabase.CREATE_MS_ConditionSupportTime21);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Holiday22);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Code23);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Setting24);
			db.execSQL(ContantCreateDatabase.CREATE_MS_Message25);
			db.execSQL(ContantCreateDatabase.CREATE_TR_Case26);
			db.execSQL(ContantCreateDatabase.CREATE_TR_Patient27);
			db.execSQL(ContantCreateDatabase.CREATE_TR_MovementTime28);
			db.execSQL(ContantCreateDatabase.CREATE_TR_FireStationStandard29);
			db.execSQL(ContantCreateDatabase.CREATE_TR_MedicalInstitutionAdmission30);
			db.execSQL(ContantCreateDatabase.CREATE_TR_SelectionKamoku31);
			db.execSQL(ContantCreateDatabase.CREATE_TR_Rotation32);
			db.execSQL(ContantCreateDatabase.CREATE_TR_Oujyu33);
			db.execSQL(ContantCreateDatabase.CREATE_TR_ContactResultRecord34);
			db.execSQL(ContantCreateDatabase.CREATE_TR_ReportingAll35);
			db.execSQL(ContantCreateDatabase.CREATE_TR_FirstAid36);
			db.execSQL(ContantCreateDatabase.CREATE_TR_ErrorLog37);
			db.execSQL(ContantCreateDatabase.CREATE_MS_LauncherItem38);
			db.execSQL(ContantCreateDatabase.CREATE_MS_NightClinic39);
			db.execSQL(ContantCreateDatabase.CREATE_MS_FireStationOriginalMI40);
			db.execSQL(ContantCreateDatabase.CREATE_TR_File41);
			db.execSQL(ContantCreateDatabase.CREATE_MS_ModelSetting42);
			db.execSQL(ContantCreateDatabase.CREATE_tr_kin_yousei43);
			db.execSQL(ContantCreateDatabase.CREATE_QQ_tr_kin_yousei_kahi44);
			db.execSQL(ContantCreateDatabase.CREATE_TR_AllReqMedicalInstitution_kahi45);
			db.execSQL(ContantCreateDatabase.CREATE_TR_AllReqMedicalInstitution46);
			db.execSQL(ContantCreateDatabase.CREATE_ms_linespeeddecision47);
			db.execSQL(ContantCreateDatabase.CREATE_TR_AllReqMedicalInstitution_Result48);
			db.execSQL(ContantCreateDatabase.CREATE_tr_firestationstandard_oujyu49);
			db.execSQL(ContantCreateDatabase.CREATE_TR_TriagePATMethod);
			db.execSQL(ContantCreateDatabase.CREATE_TR_TriagePATMethod_Result);
			db.execSQL(ContantCreateDatabase.CREATE_TR_TriageSTARTMethod);
			db.execSQL(ContantCreateDatabase.CREATE_TR_TriageSTARTMethod_Result);
			db.execSQL(ContantCreateDatabase.CREATE_tr_emsunitlocationinfo);
			db.execSQL(ContantCreateDatabase.CREATE_tr_emsunitlocationinfo_result);
			db.execSQL(ContantCreateDatabase.CREATE_qq_tr_kin_yousei_block);
			db.execSQL(ContantCreateDatabase.CREATE_qq_tr_kin_yousei_kamk);
			db.execSQL(ContantCreateDatabase.CREATE_qq_tr_kin_yousei_kikan);
			db.execSQL(ContantCreateDatabase.CREATE_tr_case_saisum);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		// Xoa cai cu tao cai moi
		db.execSQL(ContantCreateDatabase.Drop_M_User1);
		db.execSQL(ContantCreateDatabase.Drop_MS_SecondaryHealthCareArea2);
		db.execSQL(ContantCreateDatabase.Drop_MS_Area3);
		db.execSQL(ContantCreateDatabase.Drop_MS_FireStation4);
		db.execSQL(ContantCreateDatabase.Drop_MS_Instructor5);
		db.execSQL(ContantCreateDatabase.Drop_MS_EMSUnit6);
		db.execSQL(ContantCreateDatabase.Drop_MS_EMSUnitTerminalManagement7);
		db.execSQL(ContantCreateDatabase.Drop_MS_MedicalInstitution8);
		db.execSQL(ContantCreateDatabase.Drop_MS_KamokuTelNo9);
		db.execSQL(ContantCreateDatabase.Drop_MS_Kamoku10);
		db.execSQL(ContantCreateDatabase.Drop_MS_MedicalInstitutionClosed11);
		db.execSQL(ContantCreateDatabase.Drop_MS_ViewingPermission12);
		db.execSQL(ContantCreateDatabase.Drop_MS_MovementTime13);
		db.execSQL(ContantCreateDatabase.Drop_MS_FireStationStandard14);
		db.execSQL(ContantCreateDatabase.Drop_MS_FireStationStandardSort15);
		db.execSQL(ContantCreateDatabase.Drop_MS_SHCAFireStationStandard16);
		db.execSQL(ContantCreateDatabase.Drop_MS_Category17);
		db.execSQL(ContantCreateDatabase.Drop_MS_SHCACondition18);
		db.execSQL(ContantCreateDatabase.Drop_MS_Condition19);
		db.execSQL(ContantCreateDatabase.Drop_MS_ConditionSort20);
		db.execSQL(ContantCreateDatabase.Drop_MS_ConditionSupportTime21);
		db.execSQL(ContantCreateDatabase.Drop_MS_Holiday22);
		db.execSQL(ContantCreateDatabase.Drop_MS_Code23);
		db.execSQL(ContantCreateDatabase.Drop_MS_Setting24);
		db.execSQL(ContantCreateDatabase.Drop_MS_Message25);
		db.execSQL(ContantCreateDatabase.Drop_TR_Case26);
		db.execSQL(ContantCreateDatabase.Drop_TR_Patient27);
		db.execSQL(ContantCreateDatabase.Drop_TR_MovementTime28);
		db.execSQL(ContantCreateDatabase.Drop_TR_FireStationStandard29);
		db.execSQL(ContantCreateDatabase.Drop_TR_MedicalInstitutionAdmission30);
		db.execSQL(ContantCreateDatabase.Drop_TR_SelectionKamoku31);
		db.execSQL(ContantCreateDatabase.Drop_TR_Rotation32);
		db.execSQL(ContantCreateDatabase.Drop_TR_Oujyu33);
		db.execSQL(ContantCreateDatabase.Drop_TR_ContactResultRecord34);
		db.execSQL(ContantCreateDatabase.Drop_TR_ReportingAll35);
		db.execSQL(ContantCreateDatabase.Drop_TR_FirstAid36);
		db.execSQL(ContantCreateDatabase.Drop_TR_ErrorLog37);
		db.execSQL(ContantCreateDatabase.Drop_MS_LauncherItem38);
		db.execSQL(ContantCreateDatabase.Drop_MS_NightClinic39);
		db.execSQL(ContantCreateDatabase.Drop_MS_FireStationOriginalMI40);
		db.execSQL(ContantCreateDatabase.Drop_TR_File41);
		db.execSQL(ContantCreateDatabase.Drop_MS_ModelSetting42);
		db.execSQL(ContantCreateDatabase.Drop_tr_kin_yousei43);
		db.execSQL(ContantCreateDatabase.Drop_QQ_tr_kin_yousei_kahi44);
		db.execSQL(ContantCreateDatabase.Drop_TR_AllReqMedicalInstitution_kahi45);
		db.execSQL(ContantCreateDatabase.Drop_TR_AllReqMedicalInstitution46);
		db.execSQL(ContantCreateDatabase.Drop_ms_linespeeddecision47);
		db.execSQL(ContantCreateDatabase.Drop_TR_AllReqMedicalInstitution_Result48);
		db.execSQL(ContantCreateDatabase.Drop_tr_firestationstandard_oujyu49);
		db.execSQL(ContantCreateDatabase.Drop_TR_TriagePATMethod);
		db.execSQL(ContantCreateDatabase.Drop_TR_TriagePATMethod_Result);
		db.execSQL(ContantCreateDatabase.Drop_TR_TriageSTARTMethod);
		db.execSQL(ContantCreateDatabase.Drop_TR_TriageSTARTMethod_Result);
		db.execSQL(ContantCreateDatabase.Drop_tr_emsunitlocationinfo);
		db.execSQL(ContantCreateDatabase.Drop_tr_emsunitlocationinfo_result);
		db.execSQL(ContantCreateDatabase.Drop_qq_tr_kin_yousei_block);
		db.execSQL(ContantCreateDatabase.Drop_qq_tr_kin_yousei_kamk);
		db.execSQL(ContantCreateDatabase.Drop_qq_tr_kin_yousei_kikan);
		db.execSQL(ContantCreateDatabase.Drop_tr_case_saisum);
		onCreate(db);
		// db.execSQL(ContantCreateDatabase.CREATE_TR_ContactResultRecord34);
		// Giu cai cu va update them
		// db.execSQL("alter table M_User Add NgayChoi TIMESTAMP Null;");

	}

}
