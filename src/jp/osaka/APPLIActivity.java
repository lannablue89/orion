﻿package jp.osaka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.M_UserCount;
import jp.osaka.Utilities.TrustManagerManipulator;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class APPLIActivity extends Activity {
	/** Called when the activity is first created. */
	ProgressDialog p;
	PendingIntent pendingIntent;
	int sysdb = 0;

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);
			Bundle b = msg.getData();
			String i = b.getString("currentvalue");
			if (i.equals("1") || i.equals("111")) {
				exitProccess();
				// Change sys db by part 20121121
				if (sysdb == 9) {
					Intent myinten = new Intent(APPLIActivity.this, E001.class);
					startActivityForResult(myinten, 0);

					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					String startservice = sh.getString(BaseCount.StartService,
							"0");
					if (!startservice.equals("1")) {

						Editor e = sh.edit();
						e.putString(BaseCount.StartService, "1");
						e.putInt("Part", sysdb);
						e.commit();
						CancelAlarm(APPLIActivity.this);
						SetAlarm(APPLIActivity.this);

					}
					// bug 38
					finish();
				} else if (!i.equals("111")) {
					// 2012 11 21 start insert data for next part
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e = sh.edit();
					e.putInt("Part", sysdb);
					e.commit();
					Intent intent = new Intent(APPLIActivity.this, SysDB.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();
				}
			}
		}

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.main);
		getWindow()
		.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		// CL_MSSetting set=new CL_MSSetting(this);
		// set.deleteAll();
		// CL_User cl1=new CL_User(this);
		// cl1.deleteAll();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String phonenumber = getPhoneNumber();
		// Toast.makeText(this, phonenumber, Toast.LENGTH_LONG).show();
		// E022.showErrorDialog(this,"Tel:"+ phonenumber, "TestPhone", false);

		// String machineid = getMachineID();
		if (phonenumber != null) {

			String newversion = "1";
			String oldversion = "1";
			if (isOnline()) {
				newversion = ReadXml();
				oldversion = getOldVersion();
			}

			if (newversion != null && !oldversion.equals(newversion)) {
				// Reinstall
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				Editor e = sh.edit();
				e.putString(BaseCount.StartService, "0");
				e.commit();
				// /////////

				AlertDialog.Builder b = new AlertDialog.Builder(
						APPLIActivity.this);
				CL_Message m = new CL_Message(APPLIActivity.this);
				String mg = m.getMessage(ContantMessages.UpdateVersion);
				b.setMessage(mg);
				b.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
								startProgram();
							}
						});
				b.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								updateVersion();

							}
						});
				AlertDialog a = b.create();
				a.show();

			} else {
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				Editor e = sh.edit();
				e.putInt("UpdateName", 0);
				e.commit();
				startProgram();

			}

		} else {
			// E022.showErrorDialog(this, ContantMessages.NotGetSim, "SE0000",
			// false);
			showError(ContantMessages.NotGetSim, "SE0018");
			// System.exit(1);
		}

	}

	void SetAlarm(Context context) {
		Intent intent = new Intent(APPLIActivity.this, ServiceUpdateData.class);
		pendingIntent = PendingIntent.getService(APPLIActivity.this, 0, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.MINUTE, 10);// sau 10 phut thi bat dau
		// calendar.add(Calendar.SECOND, 10);//sau 15 giay thi bat dau

		int dbtime = 10;
		CL_MSSetting cl = new CL_MSSetting(context);
		Cursor c = cl.fetchLastSeting();

		if (c != null && c.getCount() > 0) {
			String DBtimer = c.getString(c.getColumnIndex("DBTime"));
			if (Utilities.IsNumber(DBtimer)) {
				dbtime = Integer.parseInt(DBtimer);
			}
		}
		int time = 1000 * 60 * dbtime;// lap lai sau dbtime phut
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,

		calendar.getTimeInMillis(), time, pendingIntent);
		CL_Message m = new CL_Message(this);
		String mg = m.getMessage(ContantMessages.StartService);
		Toast.makeText(APPLIActivity.this, mg, Toast.LENGTH_LONG).show();
	}

	public void CancelAlarm(Context context) {
		Intent intent = new Intent(context, ServiceUpdateData.class);
		// PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent,
		// 0);
		PendingIntent sender = PendingIntent.getService(context, 0, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);

	}

	// 20121019 Bug 175
	public void showError(String error, String code) {
		final Dialog dialog = new Dialog(this);
		dialog.setCanceledOnTouchOutside(false);

		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				finish();
			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null)
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}

		dialog.show();

	}

	private void startProgram() {

		try {
			CL_MSSetting set = new CL_MSSetting(this);
			Cursor c = set.fetchLastSeting();
			// 20121121 check data had insert yet
			SharedPreferences sh1 = getSharedPreferences("app", MODE_PRIVATE);
			sysdb = sh1.getInt("Part", 0);
			if (c != null && c.getCount() > 0 && sysdb == 9) {
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				String startservice = sh.getString(BaseCount.StartService, "0");
				if (!startservice.equals("1")) {

					Editor e = sh.edit();
					e.putString(BaseCount.StartService, "1");
					e.commit();
					CancelAlarm(APPLIActivity.this);
					SetAlarm(APPLIActivity.this);

				}
				CL_User user = new CL_User(this);
				// user.deleteAll();
				Cursor c2 = user.fetchLastUSer();
				if (c2.getCount() > 0) {
					String status = c2.getString(c2
							.getColumnIndex(M_UserCount.Status));
					// if (status.equals("1") || !isOnline())
					if (status.equals("1")) {
						Intent myinten = new Intent(this, E002.class);
						startActivityForResult(myinten, 0);
						// bug 38
						finish();
					} else {
						Intent myinten = new Intent(this, E001.class);
						startActivityForResult(myinten, 0);
						// bug 38
						finish();
					}
				} else {
					Intent myinten = new Intent(this, E001.class);
					startActivityForResult(myinten, 0);
					// bug 38
					finish();
				}
				c2.close();
			} else {
				if (isOnline()) {
					String phonenumber = getPhoneNumber();
					// String machineid = getMachineID();
					if (checkPhoneNumber(phonenumber)) {
						showProccess();
						startThread();
					} else {
						System.exit(1);
					}
				} else {
					// Bug 175 20121019
					showError(ContantMessages.NotInternet, "SE0019");
				}

			}

			c.close();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	void showProccess() {

		p = new ProgressDialog(this);
		p.setTitle(ContantMessages.PleaseWaiting);
		p.setMessage(ContantMessages.SerializationDB);
		p.setCancelable(false);
		p.show();
	}

	void updateProccess(int value) {
		p.setMessage(String.valueOf(value) + "%");
	}

	void exitProccess() {
		p.dismiss();
	}

	class myrunable implements Runnable {

		public void run() {
			// TODO Auto-generated method stub

			try {
				String dateServer = "";
				SharedPreferences sh1 = getSharedPreferences("app",
						MODE_PRIVATE);
				sysdb = sh1.getInt("Part", 0);
				sysdb = sysdb + 1;
				if (sysdb == 1) {
					JSONObject json = getMarterDB();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					dateServer = cl.updateDataBase1(json);
				} else if (sysdb == 2) {
					// get a lot part data because data is large 20121116
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonTR = getDataThreeTable();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase2(jsonTR);
				} else if (sysdb == 3) {
					// System.gc();
					// Thread.sleep(2000);

					JSONObject jsonSupTime1 = getMSConditionSupportTime1();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase3(jsonSupTime1);
				} else if (sysdb == 4) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonSupTime2 = getMSConditionSupportTime2();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase4(jsonSupTime2);
				} else if (sysdb == 5) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonSupTime3 = getMSConditionSupportTime3();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase5(jsonSupTime3);
				} else if (sysdb == 6) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonMSClose = getDataMSMedicalInstitutionClosed();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase6(jsonMSClose);
				} else if (sysdb == 7) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonMSView = getDataMSViewingPermission();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase7(jsonMSView);
				} else if (sysdb == 8) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject jsonTRotation = getDataTRRotation();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase8(jsonTRotation);
					// System.gc();
				}
				else if (sysdb == 9) {
					// System.gc();
					// Thread.sleep(2000);
					JSONObject json9 = getDataTable9();
					Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					cl.updateDataBase9(json9);
					// System.gc();
				}
				if (sysdb == 1) {
					// if (json != null && jsonTR != null && jsonSupTime1 !=
					// null
					// && jsonSupTime2 != null && jsonSupTime3 != null
					// && jsonMSClose != null && jsonMSView != null
					// && jsonTRotation != null) {
					// Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
					// // String dateServer = cl.updateDataBase(json);
					// String dateServer = cl.updateDataBase(json, jsonTR,
					// jsonSupTime1,jsonSupTime2,jsonSupTime3,jsonMSClose,jsonMSView,jsonTRotation);
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					Editor e = sh.edit();
					e.putString("Update_DATETIME", dateServer);
					e.commit();

				}
				// Start Auto update
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putString("currentvalue", "1");
				mg.setData(b);
				handler.sendMessage(mg);
			} catch (Exception e) {
				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				Editor ed = sh.edit();
				ed.putInt("Part", 0);
				ed.commit();
				// If fail clear data
				Cl_CreateDB cl = new Cl_CreateDB(APPLIActivity.this);
				cl.deleteAllTable();

				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putString("currentvalue", "111");
				mg.setData(b);
				handler.sendMessage(mg);
			}

		}

	}

	private JSONObject getMarterDB() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataAllTable;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	// 20121115 large data
	private JSONObject getDataThreeTable() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataThreeTable;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}
	private JSONObject getDataTable9() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataTable9;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	// 20121115 large data
	private JSONObject getMSConditionSupportTime1() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataMSConditionSupportTime1;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	private JSONObject getMSConditionSupportTime2() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataMSConditionSupportTime2;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	private JSONObject getMSConditionSupportTime3() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataMSConditionSupportTime3;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	private JSONObject getDataMSMedicalInstitutionClosed() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataMSMedicalInstitutionClosed;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	private JSONObject getDataMSViewingPermission() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataMSViewingPermission;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	private JSONObject getDataTRRotation() {
		JSONObject json = null;

		try {
			// MS_SecondaryHealthCareArea
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.getDataTRRotation;
			String SOAP_ACTIONS = ContantSystem.Endpoint + "/" + METHOD_NAME;
			String URL = ContantSystem.Endpoint;
			TrustManagerManipulator.allowAllSSL();
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
					ContantSystem.TimeOut);
			androidhttpTranport.call(SOAP_ACTIONS, envelope);

			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				E022.saveErrorLog(e2.getMessage(), APPLIActivity.this);
			}

			// tra ve json
			// Parse

			try {
				json = new JSONObject(responseBody.toString());

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				// E022.showErrorDialog(APPLIActivity.this, e1.getMessage(), "",
				// true);
				E022.saveErrorLog(e1.getMessage(), APPLIActivity.this);
			}

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
			// true);
			E022.saveErrorLog(e3.getMessage(), APPLIActivity.this);
		}
		return json;

	}

	void startThread() {
		myrunable runable = new myrunable();
		Thread th = new Thread(runable);
		th.start();
	}

	public void appendLog(String text) {
		File logFile = new File("sdcard/log.file");
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "",
						true);
			}
		}
		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}
	}

	// private String getMachineID() {
	// TelephonyManager tManager = (TelephonyManager) getApplicationContext()
	// .getSystemService(Context.TELEPHONY_SERVICE);
	// String uid = tManager.getDeviceId();
	// return uid;
	//
	// }

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) getApplicationContext()
//					.getSystemService(Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	private boolean checkPhoneNumber(String phone) {
		boolean result = false;
		try {
			TrustManagerManipulator.allowAllSSL();
			String NAMESPACE = ContantSql.NameSpace;
			String METHOD_NAME = ContantSql.checkPhoneNumber;
			String URL = ContantSystem.Endpoint;
			String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			request.addProperty("phonenumber", phone);
			// Param log 20130103
			CL_User cl = new CL_User(APPLIActivity.this);
			String user = cl.getUserName();
			String loginfo = getLogInfo(SOAP_ACTIONS, "", user);
			request.addProperty("smpLog", loginfo);
			//
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);

			// envelope.dotNet=true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidhttpTranport = new HttpTransportSE(URL);

			try {
				androidhttpTranport.call(SOAP_ACTIONS, envelope);
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				// Bug 175 20121019
				// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
				// true);
				result = false;

			} catch (XmlPullParserException e3) {
				// TODO Auto-generated catch block
				// Bug 175 20121019
				// E022.showErrorDialog(APPLIActivity.this, e3.getMessage(), "",
				// true);
				result = false;
			}
			Object responseBody = null;
			try {
				responseBody = envelope.getResponse();
				String t = responseBody.toString();
				if (t.equals("1")) {
					result = true;
				}
			} catch (SoapFault e2) {
				// TODO Auto-generated catch block
				// Bug 175 20121019
				// E022.showErrorDialog(APPLIActivity.this, e2.getMessage(), "",
				// true);
				result = false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			// Bug 175 20121019
			// E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "",
			// true);
			result = false;
		} finally {
		}
		return result;
	}

	private String getLogInfo(String url, String buttonName, String user) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", "ORION");
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), APPLIActivity.this);
		}
		return log;
	}

	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), APPLIActivity.this);
		}
		return "";
	}

	private void updateVersion() {
		try {
			DownloadOnSDcard();
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(Environment
					.getExternalStorageDirectory()
					+ "/download/"
					+ ContantSystem.ApkName)),
					"application/vnd.android.package-archive");
			startActivity(intent);
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}
	}

	// Download On My Mobile SDCard or Emulator.
	private void DownloadOnSDcard() {
		try {
			TrustManagerManipulator.allowAllSSL();
			URL url = new URL(ContantSystem.ServerApk); // Your given URL.
			// URL url = new URL("http://10.0.2.2:81/apk/NTTDK.apk"); // Your
			// given URL.
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(true);
			c.connect(); // Connection Complete here.!

			// Toast.makeText(getApplicationContext(),
			// "HttpURLConnection complete.", Toast.LENGTH_SHORT).show();

			String PATH = Environment.getExternalStorageDirectory()
					+ "/download/";
			File file = new File(PATH); // PATH = /mnt/sdcard/download/
			if (!file.exists()) {
				file.mkdirs();
			}
			File outputFile = new File(file, ContantSystem.ApkName);
			if (outputFile.exists()) {
				outputFile.delete();
			}
			FileOutputStream fos = new FileOutputStream(outputFile);

			// Toast.makeText(getApplicationContext(), "SD Card Path: " +
			// outputFile.toString(), Toast.LENGTH_SHORT).show();

			InputStream is = c.getInputStream(); // Get from Server and Catch In
													// Input Stream Object.

			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = is.read(buffer)) != -1) {
				fos.write(buffer, 0, len1); // Write In FileOutputStream.
			}
			fos.close();
			is.close();// till here, it works fine - .apk is download to my
						// sdcard in download file.
			// So plz Check in DDMS tab and Select your Emualtor.

			// Toast.makeText(getApplicationContext(),
			// "Download Complete on SD Card.!", Toast.LENGTH_SHORT).show();
			// download the APK to sdcard then fire the Intent.
		} catch (IOException e) {
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}
	}

	// ////////
	public InputStream getInputStream(URL url) {
		try {
			URLConnection con = url.openConnection();
			con.setConnectTimeout(10000);
			return con.getInputStream();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	private String getOldVersion() {
		PackageInfo pinfo;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			int versionNumber = pinfo.versionCode;
			// String versionName = pinfo.versionName;
			return String.valueOf(versionNumber);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}
		return null;
	}

	// get info version from server
	public String ReadXml() {

		TrustManagerManipulator.allowAllSSL();

		InputStream is = null;
		URL url;
		try {
			url = new URL(ContantSystem.ConfigPath);
			is = getInputStream(url);
			if (is == null) {
				return null;
			}
			DocumentBuilder docBuilder;
			try {
				docBuilder = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				try {
					Document document = docBuilder.parse(is);
					NodeList listNameTag = null;
					listNameTag = document.getElementsByTagName("manifest");

					Node node = listNameTag.item(0);
					int size = node.getAttributes().getLength();
					String ver = null;
					for (int j = 0; j < size; j++) {
						String attrName = node.getAttributes().item(j)
								.getNodeName();
						if (attrName.equalsIgnoreCase("android:versionCode")) {
							ver = node.getAttributes().item(j).getNodeValue();
							break;
						}
					}
					return ver;

				} catch (SAXException e) {
					// TODO Auto-generated catch block
					E022.showErrorDialog(APPLIActivity.this, e.getMessage(),
							"", true);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					E022.showErrorDialog(APPLIActivity.this, e.getMessage(),
							"", true);
				}

			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "",
						true);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			E022.showErrorDialog(APPLIActivity.this, e.getMessage(), "", true);
		}

		return null;

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

}
