﻿package jp.osaka;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import jp.osaka.ContantTable.BaseCount;

public class Cl_ReportingAll {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public Cl_ReportingAll(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createTR_ReportingAll(String Report_DATETIME,
			String EMSUnitCode, String ReportTitle, String ReportText,
			String Destination) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("Report_DATETIME", Report_DATETIME);
			values.put("EMSUnitCode", EMSUnitCode);
			values.put("ReportTitle", ReportTitle);
			values.put("ReportText", ReportText);
			values.put("Destination", Destination);
			values.put(BaseCount.DeleteFLG, "0");
			values.put(BaseCount.Update_DATETIME, Utilities.getDateTimeNow());
			result = database.insert("TR_ReportingAll", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_ReportingAll(String Report_DATETIME, String EMSUnitCode) {
		Cursor mCursor = null;
		try {
			// Bug 170
			String FireStationCode = "0";
			mCursor = database
					.rawQuery(
							"Select b.FireStationCode from M_User a inner join MS_EMSUnit b on a.UserName=b.EMSUnitCode  order by a.Id DESC  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FireStationCode")) != null) {
					FireStationCode = mCursor.getString(mCursor
							.getColumnIndex("FireStationCode"));
				}

			}
			// End
			String[] args = { FireStationCode, Report_DATETIME,
					Report_DATETIME, EMSUnitCode };
			// String sql =
			// "Select a.*,b.EMSUnitName from TR_ReportingAll a Inner Join MS_EMSUnit b On a.EMSUnitCode=b.EMSUnitCode Where  (a.Report_DATETIME<? or ( a.Report_DATETIME=? and a.EMSUnitCode!=?)) order by a.Report_DATETIME DESC,a.EMSUnitCode DESC Limit 10";
			String sql = "Select distinct a.*,b.EMSUnitName from TR_ReportingAll a "
					+ "Inner Join MS_EMSUnit b On a.EMSUnitCode=b.EMSUnitCode "
					+ "Where (a.Destination=9 or b.FireStationCode=?) and   (a.Report_DATETIME<? or ( a.Report_DATETIME=? and a.EMSUnitCode!=?))"
					+ " order by a.Report_DATETIME DESC,a.EMSUnitCode DESC Limit 10";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	// Bug 275 20121031
	public Cursor getTR_ReportingReload(String Report_DATETIME,
			String EMSUnitCode, String Limit) {
		Cursor mCursor = null;
		try {
			// Bug 170
			String FireStationCode = "0";
			mCursor = database
					.rawQuery(
							"Select b.FireStationCode from M_User a inner join MS_EMSUnit b on a.UserName=b.EMSUnitCode  order by a.Id DESC  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FireStationCode")) != null) {
					FireStationCode = mCursor.getString(mCursor
							.getColumnIndex("FireStationCode"));
				}

			}
			// End
			String[] args = { FireStationCode, Report_DATETIME,
					Report_DATETIME, EMSUnitCode };
			// String sql =
			// "Select a.*,b.EMSUnitName from TR_ReportingAll a Inner Join MS_EMSUnit b On a.EMSUnitCode=b.EMSUnitCode Where  (a.Report_DATETIME<? or ( a.Report_DATETIME=? and a.EMSUnitCode!=?)) order by a.Report_DATETIME DESC,a.EMSUnitCode DESC Limit 10";
			String sql = "Select distinct a.*,b.EMSUnitName from TR_ReportingAll a "
					+ "Inner Join MS_EMSUnit b On a.EMSUnitCode=b.EMSUnitCode "
					+ "Where (a.Destination=9 or b.FireStationCode=?) and   (a.Report_DATETIME<? or ( a.Report_DATETIME=? and a.EMSUnitCode!=?))"
					+ " order by a.Report_DATETIME DESC,a.EMSUnitCode DESC Limit "
					+ Limit;
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_ReportingFirst() {
		Cursor mCursor = null;
		try {

			// Bug 170
			String FireStationCode = "0";
			mCursor = database
					.rawQuery(
							"Select b.FireStationCode from M_User a inner join MS_EMSUnit b on a.UserName=b.EMSUnitCode  order by a.Id DESC  Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0
						&& mCursor.getString(mCursor
								.getColumnIndex("FireStationCode")) != null) {
					FireStationCode = mCursor.getString(mCursor
							.getColumnIndex("FireStationCode"));
				}

			}
			// End
			String[] args = { FireStationCode };
			String sql = "Select a.* from TR_ReportingAll a "
					+ "Inner Join MS_EMSUnit b On a.EMSUnitCode=b.EMSUnitCode Where (a.Destination=9 or b.FireStationCode=?) order by Report_DATETIME ASC,EMSUnitCode ASC Limit 1";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public String checkexistTR_ReportingAll() {
		String value = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from TR_ReportingAll where isshow=1 order by Update_DATETIME ASC Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					value = mCursor.getString(mCursor
							.getColumnIndex("Update_DATETIME"));
					// String Report_DATETIME = mCursor.getString(mCursor
					// .getColumnIndex("Report_DATETIME"));
					// String EMSUnitCode = mCursor.getString(mCursor
					// .getColumnIndex("EMSUnitCode"));
					// String where = "Report_DATETIME=? and EMSUnitCode=?";
					// String[] args = { Report_DATETIME,EMSUnitCode };
					// ContentValues values = new ContentValues();
					// values.put("isshow", 0);
					// database.update("TR_ReportingAll", values, where, args);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;

	}

	public String UpdateReportingAll() {
		String value = "";
		Cursor mCursor = null;
		try {
			mCursor = database
					.rawQuery(
							"Select * from TR_ReportingAll where isshow=1 order by Update_DATETIME ASC Limit 1",
							null);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getCount() > 0) {
					value = mCursor.getString(mCursor
							.getColumnIndex("Update_DATETIME"));

					String Report_DATETIME = mCursor.getString(mCursor
							.getColumnIndex("Report_DATETIME"));
					String EMSUnitCode = mCursor.getString(mCursor
							.getColumnIndex("EMSUnitCode"));

					String where = "Report_DATETIME=? and EMSUnitCode=?";
					String[] args = { Report_DATETIME, EMSUnitCode };
					ContentValues values = new ContentValues();
					values.put("isshow", 0);
					int res = database.update("TR_ReportingAll", values, where,
							args);
					if (res > 0) {
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
		return value;

	}

	public void UpdateAllReportingAll() {

		try {
			String where = "isshow=? ";
			String[] args = { "1" };
			ContentValues values = new ContentValues();
			values.put("isshow", 0);
			int res = database.update("TR_ReportingAll", values, where, args);

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			database.close();
		}
	}

	public String updateNotification(JSONObject json, String username) {
		// jsonTR: ms_code,tr_oujyu,ms_medicalinstitution
		String datetime = "";
		boolean newsms = false;
		database.beginTransaction();
		try {
			JSONObject json1 = json.getJSONObject("GetReportingall");
			datetime = json.getString("DateTime");
			JSONArray jArray;
			if (!json1.isNull("TR_ReportingAll".toLowerCase())) {
				jArray = json1.getJSONArray("TR_ReportingAll".toLowerCase());
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject e = jArray.getJSONObject(i);
					// Bug 140

					//
					ContentValues values = new ContentValues();
					values.put("Report_DATETIME",
							e.getString("Report_DATETIME".toLowerCase()));
					values.put("EMSUnitCode",
							e.getString("EMSUnitCode".toLowerCase()));
					values.put("ReportTitle",
							e.getString("ReportTitle".toLowerCase()));
					values.put("ReportText",
							e.getString("ReportText".toLowerCase()));
					values.put(BaseCount.DeleteFLG,
							e.getString(BaseCount.DeleteFLG.toLowerCase()));
					if (e.isNull("Destination".toLowerCase())) {
						values.put("Destination", "9");
					} else
						values.put("Destination",
								e.getString("Destination".toLowerCase()));

					if (!e.getString("EMSUnitCode".toLowerCase()).equals(
							username)) {
						newsms = true;
					}
					String[] args = {
							e.getString("Report_DATETIME".toLowerCase()),
							e.getString("EMSUnitCode".toLowerCase()) };
					String sql = "Select * from TR_ReportingAll Where Report_DATETIME=? and EMSUnitCode=?";
					Cursor mCursor = database.rawQuery(sql, args);
					if (mCursor != null && mCursor.getCount() > 0) {
						String where = "Report_DATETIME=? and EMSUnitCode=?";
						database.update("TR_ReportingAll", values, where, args);
					} else {
						values.put(BaseCount.Update_DATETIME,
								Utilities.getDateTimeNow());
						values.put("isshow", 1);
						database.insert("TR_ReportingAll", null, values);
					}

				}
			}
			database.setTransactionSuccessful();
			if (newsms) {
				datetime += ",1";
			} else {
				datetime += ",0";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.endTransaction();
			database.close();
		}
		return datetime;

	}
}
