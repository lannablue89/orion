﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class SlideMenu {
	TranslateAnimation slide;
	LinearLayout.LayoutParams contentParamsnew;
	LinearLayout content_new, menu_new, menu_new2, layoutMain_new;
	ProgressBar pro_new;
	int animateFromX_new;
	int animateToX_new;
	int marginX_new;
	static boolean menu_open;
	ImageButton menu_buttonnew, pro_2_new;
	Activity context_new;
	TextView txtName;
	Intent myinten = null;
	int isSysDB = 0;

	String FireStationCode;
	String CaseCode;
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	int mycountId = 1;
	static String MICode = "";

	public boolean isMenu_open() {
		return menu_open;
	}

	public void setMenu_open(boolean menu_open) {
		SlideMenu.menu_open = menu_open;
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(context_new);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	Handler handler2;

	class myrunalbeAd implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			// int dbtime = 10;
			// Cursor c = null;
			// try {
			//
			// CL_MSSetting cl = new CL_MSSetting(context_new);
			// c = cl.fetchLastSeting();
			// } catch (Exception e) {
			// // TODO: handle exception
			// }
			// if (c != null && c.getCount() > 0) {
			// String DBtimer = c.getString(c.getColumnIndex("DBTime"));
			// if (Utilities.IsNumber(DBtimer)) {
			// dbtime = Integer.parseInt(DBtimer);
			// }
			// }
			// String TimeS = Utilities.getDateTimeNow();

			boolean res = true;
			while (res) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// String syndb = sh.getString("SysDB", "0");
				String syndb = getStatusSysDB();
				if (syndb.equals("1") || isMyServiceRunning()) {
					// try {
					// DateFormat formatter = new SimpleDateFormat(
					// BaseCount.DateTimeFormat);
					// Date date1 = (Date) formatter.parse(TimeS);
					// Date date2 = (Date) formatter.parse(Utilities
					// .getDateTimeNow());
					// long diff = date2.getTime() - date1.getTime();
					// double time = diff * 1.0 / (1000 * 60);
					// int mm = (int) Math.ceil(time) - 1;
					// if (mm > dbtime) {
					// Editor e = sh.edit();
					// e.putString("SysDB", "0");
					// e.putInt("UpdateName", 0);
					// e.commit();
					// }
					// } catch (Exception e) {
					// // TODO: handle exception
					// }

					continue;
				}
				res = false;
				handler2.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						isSysDB = 0;
						if (txtName != null) {
							SharedPreferences sh = context_new
									.getSharedPreferences("app",
											Context.MODE_PRIVATE);

							String Update_DATETIME = sh.getString(
									"Update_DATETIME",
									Utilities.getDateTimeNow());
							String mm = Update_DATETIME.substring(4, 6);
							String dd = Update_DATETIME.substring(6, 8);
							String h = Update_DATETIME.substring(8, 10) + ":"
									+ Update_DATETIME.substring(10, 12);
							// final String LauncherItemCode_end = "777777";
							String LauncherItemName = "同期処理 (" + mm + "月" + dd
									+ "日 " + h + ")";
							txtName.setText(LauncherItemName);
							pro_new.setVisibility(View.GONE);
							pro_2_new.setVisibility(View.VISIBLE);
							Editor e = sh.edit();
							e.putInt("UpdateName", 0);
							e.commit();
						}
					}
				});
			}

		}
	};

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) context_new
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (ServiceUpdateData.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	void startThreadUpdateName() {
		myrunalbeAd able = new myrunalbeAd();
		Thread th = new Thread(able);
		th.start();

	}

	public void slideMenuIn(int animateFromX, int animateToX,
			LinearLayout content, int marginX,
			LinearLayout.LayoutParams contentParams) {
		marginX_new = marginX;
		animateFromX_new = animateFromX;
		animateToX_new = animateToX;
		contentParamsnew = contentParams;
		content_new = content;
		slide = new TranslateAnimation(animateFromX, animateToX, 0, 0);
		slide.setDuration(300);
		slide.setFillEnabled(true);
		slide.setAnimationListener(new AnimationListener() {

			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			}

			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
			}

			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				contentParamsnew.setMargins(marginX_new, 0, 0, 0);
				content_new.setLayoutParams(contentParamsnew);
			}
		});
		content.startAnimation(slide); // Slide menu in or out
	}

	public void initSileMenu(int animateFromX, int animateToX,
			LinearLayout content, int marginX, LinearLayout menu,
			LinearLayout menu2, LinearLayout.LayoutParams contentParams,
			ImageButton menu_button, LinearLayout layoutMain, Activity context,
			ProgressBar pro, ImageButton pro_2, String fireStationCode,
			String caseCode) {
		Cursor curItem = null;
		try {

			// Sile menu
			handler2 = new Handler();
			pro_new = pro;
			pro_2_new = pro_2;
			context_new = context;
			menu_buttonnew = menu_button;
			marginX_new = marginX;
			animateFromX_new = animateFromX;
			animateToX_new = animateToX;
			menu_new = menu;
			menu_new2 = menu2;
			content_new = content;
			layoutMain_new = layoutMain;
			contentParamsnew = contentParams;
			if (context_new.getClass().getSimpleName().equals("E005") == false) {
				MICode = "99999";
			}
			//
			FireStationCode = fireStationCode;
			CaseCode = caseCode;
			// Position the content at the start of the screen
			contentParamsnew.leftMargin = -(menu_new.getLayoutParams().width);
			content_new.setLayoutParams(contentParamsnew);
			menu_buttonnew.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (contentParamsnew.leftMargin == -(menu_new
							.getLayoutParams().width)) { // Menu
						// is hidden
						animateFromX_new = 0;
						animateToX_new = (menu_new.getLayoutParams().width);
						marginX_new = 0;
						setMenu_open(true);

						if (txtName != null) {
							SharedPreferences sh = context_new
									.getSharedPreferences("app",
											Context.MODE_PRIVATE);
							String Update_DATETIME = sh.getString(
									"Update_DATETIME",
									Utilities.getDateTimeNow());
							String mm = Update_DATETIME.substring(4, 6);
							String dd = Update_DATETIME.substring(6, 8);
							String h = Update_DATETIME.substring(8, 10) + ":"
									+ Update_DATETIME.substring(10, 12);
							// final String LauncherItemCode_end = "777777";
							String LauncherItemName = "同期処理(" + mm + "月" + dd
									+ "日 " + h + ")";
							// String syndb = sh.getString("SysDB", "0");
							if (!isMyServiceRunning()) {
								pro_new.setVisibility(View.GONE);
								pro_2_new.setVisibility(View.VISIBLE);
								txtName.setText(LauncherItemName);
							} else {
								pro_new.setVisibility(View.VISIBLE);
								txtName.setText("同期を開始しました。");
								pro_2_new.setVisibility(View.GONE);
								int isUpdate = sh.getInt("UpdateName", 0);
								if (isUpdate == 0) {
									Editor e = sh.edit();
									e.putInt("UpdateName", 1);
									e.commit();
									startThreadUpdateName();
								}
							}
						}
					} else { // Menu is visible (slide in parameter)
						animateFromX_new = 0;
						animateToX_new = -(menu_new.getLayoutParams().width);
						marginX_new = -(menu_new.getLayoutParams().width);
						setMenu_open(false);
					}
					int marginX_new_temp = marginX_new;
					slideMenuIn(animateFromX_new, animateToX_new, content_new,
							marginX_new, contentParamsnew);
					marginX_new = marginX_new_temp;
				}
			});
			try {
				CL_LaucherItem cl = new CL_LaucherItem(context_new);
				curItem = cl.getMS_LauncherItem();
			} catch (Exception e) {
				// TODO: handle exception
				E022.saveErrorLog(e.getMessage(), context_new);
			}
			int i = 0;
			if (curItem != null && curItem.getCount() > 0) {
				do {
					// check_show_item
					String LauncherStateTransitionType = curItem
							.getString(curItem
									.getColumnIndex("LauncherStateTransitionType"));
					String check_form_open = "";
					if (LauncherStateTransitionType.equals("0")) {
						check_form_open = "E002";
					} else if (LauncherStateTransitionType.equals("1")) {
						check_form_open = "E005,E008,E009,E010,E011,E013A,E013B,E013C,E014A,E014C,E020,E021,E025,E027,E032,E033A,E033B,E035";
					} else if (LauncherStateTransitionType.equals("2")) {
						check_form_open = "E004";
					} else if (LauncherStateTransitionType.equals("4")) {
						check_form_open = "E036,E037,E038,E039";
					}

					String[] slip = check_form_open.split(",");
					int m = 0;
					boolean check_item = false;
					do {
						if (context_new.toString().contains(slip[m].toString())) {
							check_item = true;
							break;
						}
						m++;
					} while (m < slip.length);
					if (!check_item)
						continue;
					// end check_show_item
					// final String LauncherItemCode = curItem.getString(curItem
					// .getColumnIndex("LauncherItemCode"));
					String LauncherItemName = curItem.getString(curItem
							.getColumnIndex("LauncherItemName"));
					String LauncherItemFileName = curItem.getString(curItem
							.getColumnIndex("LauncherItemFileName"));
					final String ScreenTransitionInfo = curItem.getString(
							curItem.getColumnIndex("ScreenTransitionInfo"))
							.toUpperCase();
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramrow.setMargins(10, 10, 0, 10);
					LinearLayout layrow = new LinearLayout(context_new);
					layrow.setOrientation(LinearLayout.HORIZONTAL);
					layrow.setLayoutParams(paramrow);
					layrow.setGravity(Gravity.CENTER | Gravity.LEFT);
					layrow.setId(i);

					LinearLayout.LayoutParams paramimg = new LinearLayout.LayoutParams(
							65, 65);
					ImageView img = new ImageView(context_new);
					img.setLayoutParams(paramimg);

					int resid = context_new.getResources().getIdentifier(
							LauncherItemFileName, "drawable",
							context_new.getPackageName());
					img.setBackgroundResource(resid);
					layrow.addView(img);

					LinearLayout.LayoutParams paramtxt = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtxt.setMargins(10, 0, 0, 0);
					TextView txtName = new TextView(context_new);
					txtName.setLayoutParams(paramtxt);
					txtName.setText(LauncherItemName);
					// txtName.setGravity(Gravity.CENTER | Gravity.LEFT);
					txtName.setTextSize(18);
					layrow.addView(txtName);

					layrow.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							try {

								if (menu_open) {
									if (ScreenTransitionInfo.equals("E001")) {
										confirmLogout();
									} else if (ScreenTransitionInfo
											.equals("E025")) {
										myinten = new Intent(context_new,
												E025.class);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("E028")) {
										myinten = new Intent(context_new,
												E028.class);
										Bundle b = new Bundle();
										b.putString("FireStationCode",
												SlideMenu.this.FireStationCode);
										b.putString("CaseCode",
												SlideMenu.this.CaseCode);
										myinten.putExtras(b);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("E029")) {
										myinten = new Intent(context_new,
												E029.class);
										Bundle b = new Bundle();
										b.putString("FireStationCode",
												SlideMenu.this.FireStationCode);
										b.putString("CaseCode",
												SlideMenu.this.CaseCode);
										myinten.putExtras(b);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("E005")) {
										myinten = new Intent(context_new,
												E005.class);
										Bundle b = new Bundle();
										b.putString("FireStationCode",
												FireStationCode);
										b.putString("CaseCode", CaseCode);
										b.putString(BaseCount.IsOldData, "1");
										myinten.putExtras(b);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("E016")) {
										ShowE016();
									} else if (ScreenTransitionInfo
											.equals("E017")) {
										showE017();
									} else if (ScreenTransitionInfo
											.equals("E019")) {
										showE019();
									} else if (ScreenTransitionInfo
											.equals("E020")) {
										myinten = new Intent(context_new,
												E020.class);
										Bundle b = new Bundle();
										b.putString("FireStationCode",
												FireStationCode);
										b.putString("CaseCode", CaseCode);
										myinten.putExtras(b);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("E021")) {
										myinten = new Intent(context_new,
												E021.class);
										Bundle b = new Bundle();
										b.putString("FireStationCode",
												FireStationCode);
										b.putString("CaseCode", CaseCode);
										myinten.putExtras(b);
										context_new.startActivityForResult(
												myinten, 0);
									} else if (ScreenTransitionInfo
											.equals("ECALL")) {
										Intent showCallLog = new Intent();
										showCallLog
												.setAction(Intent.ACTION_VIEW);
										showCallLog
												.setType(CallLog.Calls.CONTENT_TYPE);
										context_new.startActivity(showCallLog);
									}
									// else {
									// setMyIntent(ScreenTransitionInfo);
									// context_new.startActivityForResult(
									// myinten, 0);
									// }
									slideMenuIn(
											0,
											-(menu_new.getLayoutParams().width),
											content_new,
											-(menu_new.getLayoutParams().width),
											contentParamsnew);
									setMenu_open(false);
								}
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
					});
					LinearLayout.LayoutParams paramtxtLine = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, 2);

					TextView txtLine = new TextView(context_new);
					txtLine.setLayoutParams(paramtxtLine);
					txtLine.setBackgroundResource(R.drawable.linemenu);
					menu.addView(txtLine);
					menu.addView(layrow);

				} while (curItem.moveToNext());
			}
			// end

			// item cuoi cung
			pro_new.setVisibility(View.GONE);
			pro_2_new.setVisibility(View.VISIBLE);
			SharedPreferences sh = context_new.getSharedPreferences("app",
					Context.MODE_PRIVATE);
			String Update_DATETIME = sh.getString("Update_DATETIME",
					Utilities.getDateTimeNow());
			String mm = Update_DATETIME.substring(4, 6);
			String dd = Update_DATETIME.substring(6, 8);
			String h = Update_DATETIME.substring(8, 10) + ":"
					+ Update_DATETIME.substring(10, 12);
			// final String LauncherItemCode_end = "777777";
			String LauncherItemName = "同期処理 (" + mm + "月" + dd + "日 " + h + ")";
			// String LauncherItemFileName = "menu7";
			LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			paramrow.setMargins(10, 10, 0, 10);
			LinearLayout layrow = new LinearLayout(context_new);
			layrow.setOrientation(LinearLayout.HORIZONTAL);
			layrow.setLayoutParams(paramrow);
			layrow.setGravity(Gravity.CENTER | Gravity.LEFT);
			layrow.setId(i);

			LinearLayout.LayoutParams paramtxt = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			paramtxt.setMargins(10, 0, 0, 0);
			txtName = new TextView(context_new);
			txtName.setLayoutParams(paramtxt);
			String syndb = getStatusSysDB(); // sh.getString("SysDB", "0");
			if (syndb.equals("0") && !isMyServiceRunning()) {
				pro_new.setVisibility(View.GONE);
				pro_2_new.setVisibility(View.VISIBLE);
				txtName.setText(LauncherItemName);
			} else {
				pro_new.setVisibility(View.VISIBLE);
				pro_2_new.setVisibility(View.GONE);
				txtName.setText("同期を開始しました。");
				int isUpdate = sh.getInt("UpdateName", 0);
				if (isUpdate == 0) {
					Editor e = sh.edit();
					e.putInt("UpdateName", 1);
					e.commit();
					startThreadUpdateName();
				}
			}
			txtName.setTextColor(Color.parseColor("#B9B9B9"));
			// txtName.setGravity(Gravity.CENTER | Gravity.LEFT);
			txtName.setTextSize(18);
			layrow.addView(txtName);
			layrow.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if (menu_open && isSysDB == 0) {
						isSysDB = 1;
						// process update data
						pro_new.setVisibility(View.VISIBLE);
						pro_2_new.setVisibility(View.GONE);
						txtName.setText("同期を開始しました。");
						String syndb = getStatusSysDB();// sh.getString("SysDB",
														// "0");
						if (syndb.equals("0")) {
							Intent intent = new Intent(context_new,
									ServiceUpdateData.class);
							context_new.startService(intent);

						}
						startThreadUpdateName();
					}
				}
			});
			LinearLayout.LayoutParams paramtxtLine = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT, 2);

			TextView txtLine = new TextView(context_new);
			txtLine.setLayoutParams(paramtxtLine);
			txtLine.setBackgroundResource(R.drawable.linemenu);
			menu2.addView(txtLine);
			menu2.addView(layrow);
			// end item cuoi cung
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), context_new);
		}
	}

	public void confirmLogout() {

		final Dialog dialog = new Dialog(context_new);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				CL_User us = new CL_User(context_new);
				us.Logout();
				Intent intent = new Intent(context_new, E001.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context_new.startActivity(intent);
				dialog.cancel();
				context_new.finish();
			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(context_new);
		String mg = me.getMessage(ContantMessages.Logout);
		lbmg.setText(mg);
		dialog.show();

	}

	public void setMyIntent(String ScreenTransitionInfo) {
		// set class active ScreenTransitionInfo

		if (ScreenTransitionInfo.equals("E001")) {
			myinten = new Intent(context_new, E001.class);
		} else if (ScreenTransitionInfo.equals("E002")) {
			myinten = new Intent(context_new, E002.class);
		} else if (ScreenTransitionInfo.equals("E004")) {
			myinten = new Intent(context_new, E004.class);
		} else if (ScreenTransitionInfo.equals("E005")) {
			myinten = new Intent(context_new, E005.class);
		} else if (ScreenTransitionInfo.equals("E008")) {
			myinten = new Intent(context_new, E008.class);
		} else if (ScreenTransitionInfo.equals("E010")) {
			myinten = new Intent(context_new, E010.class);
		} else if (ScreenTransitionInfo.equals("E011")) {
			myinten = new Intent(context_new, E011.class);
		} else if (ScreenTransitionInfo.equals("E013A")) {
			myinten = new Intent(context_new, E013A.class);
		} else if (ScreenTransitionInfo.equals("E013B")) {
			myinten = new Intent(context_new, E013B.class);
		} else if (ScreenTransitionInfo.equals("E013C")) {
			myinten = new Intent(context_new, E013C.class);
		} else if (ScreenTransitionInfo.equals("E014A")) {
			myinten = new Intent(context_new, E014A.class);
		} else if (ScreenTransitionInfo.equals("E014C")) {
			myinten = new Intent(context_new, E014C.class);
		} else if (ScreenTransitionInfo.equals("E020")) {
			myinten = new Intent(context_new, E020.class);
		} else if (ScreenTransitionInfo.equals("E021")) {
			myinten = new Intent(context_new, E021.class);
		} else if (ScreenTransitionInfo.equals("E025")) {
			myinten = new Intent(context_new, E025.class);
		}
	}

	float downXValue;

	public boolean eventOnTouch(MotionEvent event, int animateFromX,
			int animateToX, int marginX, LinearLayout menu,
			LinearLayout content, LinearLayout.LayoutParams contentParams) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: {
			downXValue = event.getX();
			break;
		}
		case MotionEvent.ACTION_UP:
			float currentX = event.getX();
			float Sensivity = 2 / 10;
			try {
				CL_MSSetting cl = new CL_MSSetting(context_new);
				Cursor c = cl.fetchLastSeting();

				if (c != null && c.getCount() > 0) {

					Sensivity = content.getWidth()
							* Float.parseFloat(c.getString(c
									.getColumnIndex("ScreenSensitivity")));

				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (downXValue < currentX && currentX - downXValue > Sensivity) {
				if (!isMenu_open()) {
					// is hidden
					animateFromX = 0;
					animateToX = (menu.getLayoutParams().width);
					marginX = 0;
					setMenu_open(true);
					slideMenuIn(animateFromX, animateToX, content, marginX,
							contentParams);

					if (txtName != null) {
						SharedPreferences sh = context_new
								.getSharedPreferences("app",
										Context.MODE_PRIVATE);
						String Update_DATETIME = sh.getString(
								"Update_DATETIME", Utilities.getDateTimeNow());
						String mm = Update_DATETIME.substring(4, 6);
						String dd = Update_DATETIME.substring(6, 8);
						String h = Update_DATETIME.substring(8, 10) + ":"
								+ Update_DATETIME.substring(10, 12);
						// final String LauncherItemCode_end = "777777";
						String LauncherItemName = "同期処理(" + mm + "月" + dd
								+ "日 " + h + ")";
						// String syndb = sh.getString("SysDB", "0");
						if (!isMyServiceRunning()) {
							pro_new.setVisibility(View.GONE);
							pro_2_new.setVisibility(View.VISIBLE);
							txtName.setText(LauncherItemName);
						} else {
							pro_new.setVisibility(View.VISIBLE);
							txtName.setText("同期を開始しました。");
							pro_2_new.setVisibility(View.GONE);
							int isUpdate = sh.getInt("UpdateName", 0);
							if (isUpdate == 0) {
								Editor e = sh.edit();
								e.putInt("UpdateName", 1);
								e.commit();
								startThreadUpdateName();
							}
						}
					}
					return true;
				}
			} else if (downXValue > currentX
					&& downXValue - currentX > Sensivity) {
				if (isMenu_open()) {
					// Menu is visible (slide in parameter)
					animateFromX = 0;
					animateToX = -(menu.getLayoutParams().width);
					marginX = -(menu.getLayoutParams().width);
					setMenu_open(false);
					slideMenuIn(animateFromX, animateToX, content, marginX,
							contentParams);

					return true;
				}
			}
			break;
		case MotionEvent.ACTION_SCROLL:
			break;
		default:
			break;
		}
		return false;
	}

	private void ShowE016() {
		getConfig();
		final Dialog dialog = new Dialog(context_new);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!MNetURL.startsWith("http://")
						&& !MNetURL.startsWith("https://"))
					MNetURL = "http://" + MNetURL;
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(MNetURL));
				context_new.startActivity(browserIntent);
				dialog.cancel();
				CL_TRCase tr = new CL_TRCase(context_new);
				tr.updateKin_kbn(FireStationCode, CaseCode, "0");

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(context_new);
		String mg = me.getMessage(ContantMessages.OpenBrowes);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	String phonecall = "";

	private void showE017() {
		Cursor cur = null;
		try {

			final Dialog dialog = new Dialog(context_new);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute017);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel17);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
					// 20121018 Bug 177
					phonecall = "";
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOK17);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!phonecall.equals("")) {
						callPhone(phonecall);
						dialog.cancel();
						// showE015();
						E015Dialog dg = new E015Dialog();
						dg.ShowE015(context_new, FireStationCode, CaseCode,
								MICode, "E017");
					}

				}
			});
			Cl_FireStationStandard cl = new Cl_FireStationStandard(context_new);
			cur = cl.getHopital17();
			if (cur != null && cur.getCount() > 0) {
				mycountId = 1;
				LinearLayout layoutRdi = (LinearLayout) dialog
						.findViewById(R.id.layoutItem17);

				do {
					final String mICode = cur.getString(cur
							.getColumnIndex("MICode"));
					String MIName_Kanji = cur.getString(cur
							.getColumnIndex("MIName_Kanji"));
					String ThirdCoordinateTelNo = cur.getString(cur
							.getColumnIndex("ThirdCoordinateTelNo"));

					LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramCheck.setMargins(0, 30, 0, 0);
					final RadioButton rdi = new RadioButton(context_new);
					rdi.setLayoutParams(paramCheck);
					rdi.setText(ThirdCoordinateTelNo);
					rdi.setTextColor(Color.BLACK);
					rdi.setButtonDrawable(R.drawable.radio);
					rdi.setTextSize(24);
					rdi.setId(mycountId++);
					rdi.setTextColor(Color.parseColor("#777777"));

					LinearLayout.LayoutParams paramtxt = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtxt.setMargins(45, 0, 0, 0);
					TextView txtName = new TextView(context_new);
					txtName.setLayoutParams(paramtxt);
					txtName.setText(MIName_Kanji);
					txtName.setTextColor(Color.parseColor("#777777"));
					txtName.setId(mycountId++);
					txtName.setTextSize(24);
					layoutRdi.addView(rdi);
					layoutRdi.addView(txtName);
					txtName.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							rdi.setChecked(true);
						}
					});
					rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								for (int i = 1; i < mycountId; i = i + 2) {
									RadioButton radio = (RadioButton) dialog
											.findViewById(i);
									if (radio.isChecked() && rdi.getId() != i)
										radio.setChecked(false);
								}
								rdi.setChecked(true);
								rdi.setTextColor(Color.WHITE);
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color.WHITE);
								MICode = mICode;
								phonecall = rdi.getText().toString();
							} else {
								rdi.setTextColor(Color.parseColor("#777777"));
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color
										.parseColor("#777777"));
							}
						}
					});

				} while (cur.moveToNext());
			}

			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(context_new, e.getMessage(), "E017", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	String phone19 = "";

	private void showE019() {
		Cursor cur = null;
		try {

			final Dialog dialog = new Dialog(context_new);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute019);
			dialog.setCanceledOnTouchOutside(false);// Bug 146
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel17);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
					// 20121018 Bug 177
					phone19 = "";
				}
			});
			Button btnOK = (Button) dialog.findViewById(R.id.btnOK17);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!phone19.equals("")) {
						callPhone(phone19);
						dialog.cancel();
						// showE015();
					}

				}
			});
			Cl_FireStationStandard cl = new Cl_FireStationStandard(context_new);
			cur = cl.getHopital19(FireStationCode);
			if (cur != null && cur.getCount() > 0) {
				mycountId = 1;
				LinearLayout layoutRdi = (LinearLayout) dialog
						.findViewById(R.id.layoutItem17);

				do {
					final String mICode = cur.getString(cur
							.getColumnIndex("MICode"));
					String MIName_Kanji = cur.getString(cur
							.getColumnIndex("MIName_Kanji"));
					String ThirdCoordinateTelNo = cur.getString(cur
							.getColumnIndex("MITelNo"));
					String MIDetailName = cur.getString(cur
							.getColumnIndex("MIDetailName"));
					if (!MIDetailName.equals("null"))
						MIName_Kanji = MIName_Kanji + MIDetailName;
					LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramCheck.setMargins(0, 30, 0, 0);
					final RadioButton rdi = new RadioButton(context_new);
					rdi.setLayoutParams(paramCheck);
					rdi.setText(ThirdCoordinateTelNo);
					rdi.setTextColor(Color.BLACK);
					rdi.setButtonDrawable(R.drawable.radio);
					rdi.setTextSize(24);
					rdi.setId(mycountId++);
					rdi.setTextColor(Color.parseColor("#777777"));

					LinearLayout.LayoutParams paramtxt = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtxt.setMargins(45, 0, 0, 0);
					TextView txtName = new TextView(context_new);
					txtName.setLayoutParams(paramtxt);
					txtName.setText(MIName_Kanji);
					txtName.setTextColor(Color.parseColor("#777777"));
					txtName.setId(mycountId++);
					txtName.setTextSize(24);
					layoutRdi.addView(rdi);
					layoutRdi.addView(txtName);
					txtName.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							rdi.setChecked(true);
						}
					});
					rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								for (int i = 1; i < mycountId; i = i + 2) {
									RadioButton radio = (RadioButton) dialog
											.findViewById(i);
									if (radio.isChecked() && rdi.getId() != i)
										radio.setChecked(false);
								}
								rdi.setChecked(true);
								rdi.setTextColor(Color.WHITE);
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color.WHITE);
								MICode = mICode;
								phone19 = rdi.getText().toString();
							} else {
								rdi.setTextColor(Color.parseColor("#777777"));
								TextView txtname = (TextView) dialog
										.findViewById(rdi.getId() + 1);
								txtname.setTextColor(Color
										.parseColor("#777777"));
							}
						}
					});

				} while (cur.moveToNext());
			}

			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(context_new, e.getMessage(), "E019", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void callPhone(String phonenumber) {
		try {
			phonenumber = phonenumber.replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", "").trim();
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + phonenumber));
			context_new.startActivity(callIntent);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(context_new, e.getMessage(), context_new
					.getClass().getSimpleName(), true);
		}
	}

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(context_new);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(context_new);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}
}
