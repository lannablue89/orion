package jp.osaka;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class E028 extends Activity {

	static Context context;
	private Camera mCamera;
	private CameraPreview mPreview;
	private MediaRecorder mMediaRecorder;
	private boolean isRecording = false;
	private Button videoButton;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	boolean bBehide = true;
	Gallery myGallery;
	profileAdapter listImage;
	static String FolderPicture = "multimedia/pictures";
	static String FolderVideo = "multimedia/videos";
	static String PathFolder = "";

	TextView txtTime;
	int time;
	static String lastFile = "";
	FrameLayout preview;
	DTO_FileInfo dtDel;

	boolean brun;
	Handler handler;
	int cameraid = 0;

	MediaPlayer mp;
	adapterActivity listQuality;
	OrientationEventListener myOrientationEventListener;
	String CameraFlash;
	String VideoFlash;
	int QualityId = 5;
	static String FireStationCode;
	static String CaseCode;
	private MediaPlayer mPlayer = null;

	class myrunable implements Runnable {
		public void run() {
			// TODO Auto-generated method stub

			while (brun) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						time++;
						txtTime.setText(Utilities.converttoStringTime(time));
					}
				});
			}
		}
	}

	void startThreadTime() {
		myrunable runable = new myrunable();
		Thread th = new Thread(runable);
		th.start();
	}

	int mode = 1;
	Button btnChangeMode;
	int DefaultPictureSizeWidth = -1, DefaultPictureSizeHeight = -1,
			DefaultVideQuality = -1;
	// Spinner cboQuality;
	TextView txtQuality;
	boolean bShowQuality;
	Dialog dialogQuality = null;

	TextView btnFlash;
	Button button_Rotation;
	LinearLayout lflash;
	int Audio_source = -1, Video_source = -1, Video_outputformat = -1,
			Video_encorder = -1, Video_framerate = -1, AudioEncoder = -1;
	static int Image_outputformat = -1;
	TextView txtZoom;
	int ipoiter;
	float oldDis;
	float newDis;
	int maxZoomLevel, currentZoomLevel;
	int zoomvalue;
	LinearLayout laytime;

	private void getModelSetting() {
		Cursor cur = null;
		try {
			CL_ModelSetting cl = new CL_ModelSetting(E028.this);
			cur = cl.getModelSetting(android.os.Build.MODEL);
			if (cur != null && cur.getCount() > 0) {
				PathFolder = cur.getString(cur.getColumnIndex("Location_Name"));
				if (cur.getString(cur.getColumnIndex("Audio_source")) != null)
					Audio_source = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Audio_source")));
				if (cur.getString(cur.getColumnIndex("Audio_encorder")) != null)
					AudioEncoder = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Audio_encorder")));
				if (cur.getString(cur.getColumnIndex("Video_source")) != null)
					Video_source = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Video_source")));
				if (cur.getString(cur.getColumnIndex("Video_outputformat")) != null)
					Video_outputformat = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Video_outputformat")));
				if (cur.getString(cur.getColumnIndex("Video_encorder")) != null)
					Video_encorder = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Video_encorder")));
				if (cur.getString(cur.getColumnIndex("Video_framerate")) != null)
					Video_framerate = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Video_framerate")));
				if (cur.getString(cur.getColumnIndex("Image_outputformat")) != null)
					Image_outputformat = Integer.parseInt(cur.getString(cur
							.getColumnIndex("Image_outputformat")));
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();

		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		setContentView(R.layout.layoutcamera);
		//upLoadServerUri =ContantSystem.Server+ "/uploadfiles";
		context = E028.this;
		CameraFlash = Parameters.FLASH_MODE_AUTO;
		VideoFlash = Parameters.FLASH_MODE_OFF;

		// Get Param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");
		try {
			getInfoUser();
			getModelSetting();
			getWindow()
					.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			// Load Combobox Quality
			bShowQuality = false;
			laytime = (LinearLayout) findViewById(R.id.layTime);
			laytime.setVisibility(View.INVISIBLE);
			listQuality = new adapterActivity(this, R.layout.layoutrowquality);
			txtQuality = (TextView) findViewById(R.id.txtQuality);
			txtQuality.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					showDialogQuality();
				}
			});
			btnFlash = (TextView) findViewById(R.id.btnFlash);
			btnFlash.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					showDialogFlash();
				}
			});
			lflash = (LinearLayout) findViewById(R.id.layFlash);
			txtZoom = (TextView) findViewById(R.id.txtZoom);
			// cboQuality = (Spinner) findViewById(R.id.cboQuality);
			// cboQuality.setAdapter(listQuality);

			handler = new Handler();
			txtTime = (TextView) findViewById(R.id.txtTime);
			// Create an instance of Camera
			mCamera = getCameraInstance();
			// mCamera.setDisplayOrientation(90);
			setCameraDisplayOrientation(E028.this, cameraid, mCamera);

			// String type = getNetWorkType();
			// setResolutionCamera(resolution);

			mPreview = new CameraPreview(this, mCamera);
			preview = (FrameLayout) findViewById(R.id.camera_preview);
			preview.addView(mPreview);

			Camera.Parameters p = mCamera.getParameters();
			maxZoomLevel = p.getMaxZoom();
			int zoom = p.getZoom();
			txtZoom.setText("x" + String.valueOf(zoom));
			if (zoom == 0)
				txtZoom.setVisibility(View.INVISIBLE);
			Button btnBack = (Button) findViewById(R.id.btnBack);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			btnChangeMode = (Button) findViewById(R.id.btnChange);
			btnChangeMode.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (isRecording)
						return;
					if (mode == 1)
						mode = 2;
					else
						mode = 1;
					loadControlbyMode();
				}
			});
			videoButton = (Button) findViewById(R.id.button_video);
			videoButton.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						if (mode == 1) {
							if (mCamera == null) {
								if (cameraid == CameraInfo.CAMERA_FACING_FRONT)
									mCamera = Camera
											.open(CameraInfo.CAMERA_FACING_FRONT);
								else if (cameraid == CameraInfo.CAMERA_FACING_BACK)
									mCamera = Camera
											.open(CameraInfo.CAMERA_FACING_BACK);
								else
									mCamera = getCameraInstance();
								mCamera.setDisplayOrientation(90);

								mCamera.setPreviewCallback(null);
								mPreview = new CameraPreview(E028.this, mCamera);// set
																					// preview
								preview.removeAllViews();
								preview.addView(mPreview);
								mCamera.startPreview();

							}
							// Capture image
							playSound();
							mCamera.lock();
							System.gc();
							mCamera.takePicture(null, null, mPicture);
							try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							// Video
							if (isRecording) {
								// stop recording and release camera
								brun = false;
								mMediaRecorder.stop(); // stop the recording
								releaseMediaRecorder(); // release the
														// MediaRecorder
														// object
								mCamera.lock(); // take camera access back from
												// MediaRecorder
								// inform the user that recording has stopped
								// videoButton.setText("Video");

								laytime.setVisibility(View.INVISIBLE);
								videoButton
										.setBackgroundResource(R.drawable.btnvideo);
								txtQuality.setVisibility(View.VISIBLE);
								lflash.setVisibility(View.VISIBLE);
								btnChangeMode.setVisibility(View.VISIBLE);
								button_Rotation.setVisibility(View.VISIBLE);
								isRecording = false;
								// getListFile();
								addFiletoList(lastFile);
								// String memory =
								// getAvailableInternalMemorySize();
								// txtMemory.setText(memory);
							} else {

								// initialize video camera
								if (prepareVideoRecorder()) {

									// Camera is available and unlocked,
									// MediaRecorder
									// is
									// prepared,
									// now you can start recording
									laytime.setVisibility(View.VISIBLE);
									txtTime.setText("00:00");
									time = 0;
									brun = true;
									startThreadTime();
									mMediaRecorder.start();

									txtQuality.setVisibility(View.INVISIBLE);
									lflash.setVisibility(View.INVISIBLE);
									btnChangeMode.setVisibility(View.INVISIBLE);
									button_Rotation
											.setVisibility(View.INVISIBLE);
									// inform the user that recording has
									// started
									// videoButton.setText("Stop");
									videoButton
											.setBackgroundResource(R.drawable.btnstopvideo);
									isRecording = true;
								} else {
									// prepare didn't work, release the camera
									releaseMediaRecorder();
									// inform user
								}
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("videoButton", e.getMessage());
					}
				}

			});
			button_Rotation = (Button) findViewById(R.id.btnRostation);
			button_Rotation.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					// Camera camera;
					try {
						if (isRecording)
							return;
						if (Camera.getNumberOfCameras() >= 2) {
							releaseCamera();
							if (bBehide) {
								// if you want to open front facing camera use
								// this
								// line
								mCamera = Camera
										.open(CameraInfo.CAMERA_FACING_FRONT);
								cameraid = CameraInfo.CAMERA_FACING_FRONT;
								// mCamera = getCameraInstance();
								mCamera.setDisplayOrientation(90);
								mCamera.setPreviewCallback(null);
								// mCamera.setPreviewDisplay(null);
								// setResolutionCamera(resolution);
								mPreview = new CameraPreview(E028.this, mCamera);// set
																					// preview
								preview.removeAllViews();
								preview.addView(mPreview);
								mCamera.startPreview();
							} else {
								// if you want to use the back facing camera
								mCamera = Camera
										.open(CameraInfo.CAMERA_FACING_BACK);
								cameraid = CameraInfo.CAMERA_FACING_BACK;
								mCamera.setDisplayOrientation(90);
								mCamera.setPreviewCallback(null);
								// mCamera.setPreviewDisplay(null);
								// setResolutionCamera(resolution);
								mPreview = new CameraPreview(E028.this, mCamera);// set
																					// preview
								preview.removeAllViews();
								preview.addView(mPreview);
								mCamera.startPreview();
							}
							bBehide = !bBehide;
							Camera.Parameters p = mCamera.getParameters();
							if (mode == 1) {
								p.setPictureSize(DefaultPictureSizeWidth,
										DefaultPictureSizeHeight);
								mCamera.setParameters(p);
							}
							txtZoom.setText("x" + String.valueOf(zoomvalue));
							if (zoomvalue > 0)
								txtZoom.setVisibility(View.VISIBLE);
							else
								txtZoom.setVisibility(View.INVISIBLE);

							if (p.isZoomSupported()
									&& p.isSmoothZoomSupported()) {
								// most phones
								mCamera.startSmoothZoom(zoomvalue);
							} else if (p.isZoomSupported()
									&& !p.isSmoothZoomSupported()) {
								p.setZoom(zoomvalue);
								mCamera.setParameters(p);
								mCamera.setParameters(p);
								mCamera.startPreview();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			});
			myGallery = (Gallery) findViewById(R.id.galleryImage);
			listImage = new profileAdapter(this, R.layout.layoutimage);
			myGallery.setAdapter(listImage);
			myGallery.setSpacing(10);
			myGallery.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					dtDel = listImage.getItem(arg2);
					confirmDeleteItem();
					// uploadFile();
					// Intent myinten = new Intent(ECAMERA.this,
					// EPlayVideo.class);
					// Bundle b = new Bundle();
					// b.putString("File", dtDel.FileName);
					// myinten.putExtras(b);
					// startActivityForResult(myinten, 0);

				}
			});

			loadControlbyMode();
			getListFile();

		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(this, "Please restart your phone",
					Toast.LENGTH_SHORT).show();
		}
	}

	// 1 Override OnTouchEvent
	@SuppressLint("FloatMath")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		super.onTouchEvent(event);
		// 2 Get Action on Screen Camera
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		switch (action) {
		case MotionEvent.ACTION_DOWN: {

			// ipoiter = event.getPointerCount();
			// if (ipoiter == 2) {
			//
			// float x = event.getX(0) - event.getX(1);
			// float y = event.getY(0) - event.getY(1);
			// oldDis = FloatMath.sqrt(x * x + y * y);
			//
			// }
			break;
		}
		case MotionEvent.ACTION_MOVE: {
			// Log.d("MultitouchExample", "Action Move");
			// path.lineTo(event.getX(), event.getY());
			// newDis = spacing(event);
			// When move
			ipoiter = event.getPointerCount();
			if (ipoiter == 2) {
				// 5 Get new distance between 2 point when move
				float x = event.getX(0) - event.getX(1);
				float y = event.getY(0) - event.getY(1);
				newDis = FloatMath.sqrt(x * x + y * y);

				// 6 Zoom out
				if (newDis > oldDis) {
					// Check out of max zoom of camera
					if (zoomvalue < maxZoomLevel) {

						// 7 caculator number zoom from distance between 2 point
						float res = newDis - oldDis;
						zoomvalue = (int) (res / 50);

						// 8 add zoom value to current zoom
						Camera.Parameters p = mCamera.getParameters();
						zoomvalue = currentZoomLevel + zoomvalue;

						// If > max value reset to max
						if (zoomvalue > maxZoomLevel)
							zoomvalue = maxZoomLevel;
						// 9 Display to TextView on Screen
						txtZoom.setText("x" + String.valueOf(zoomvalue));
						if (zoomvalue > 0)
							txtZoom.setVisibility(View.VISIBLE);
						else
							txtZoom.setVisibility(View.INVISIBLE);
						// 10 Set zoom value to camera

						if (p.isZoomSupported() && p.isSmoothZoomSupported()) {
							// most phones
							mCamera.startSmoothZoom(zoomvalue);
						} else if (p.isZoomSupported()
								&& !p.isSmoothZoomSupported()) {
							p.setZoom(zoomvalue);
							mCamera.setParameters(p);
							mCamera.setParameters(p);
							mCamera.startPreview();
						}
					}
				} else {
					// 11 Zoom in
					if (zoomvalue > 0) {
						// caculator zoom value
						float res = oldDis - newDis;
						// currentZoomLevel--;
						zoomvalue = (int) (res / 50);
						Camera.Parameters p = mCamera.getParameters();
						zoomvalue = currentZoomLevel - zoomvalue;
						if (zoomvalue < 0)
							zoomvalue = 0;
						// Diaplay to textview
						if (zoomvalue > 0)
							txtZoom.setVisibility(View.VISIBLE);
						else
							txtZoom.setVisibility(View.INVISIBLE);
						txtZoom.setText("x" + String.valueOf(zoomvalue));
						// Set zoom value to camera
						if (p.isZoomSupported() && p.isSmoothZoomSupported()) {
							// most phones
							mCamera.startSmoothZoom(zoomvalue);
						} else if (p.isZoomSupported()
								&& !p.isSmoothZoomSupported()) {
							p.setZoom(zoomvalue);
							mCamera.setParameters(p);
							mCamera.setParameters(p);
							mCamera.startPreview();
						}
					}
				}

			}
			break;
		}
		case MotionEvent.ACTION_POINTER_DOWN: {
			// 3 When Pointer Down
			// Check 2 Poit down
			ipoiter = event.getPointerCount();
			if (ipoiter == 2) {
				// 3 Get current zoom level
				Camera.Parameters p = mCamera.getParameters();
				currentZoomLevel = p.getZoom();

				// 4 caculator distance 2 point down oldDis
				float x = event.getX(0) - event.getX(1);
				float y = event.getY(0) - event.getY(1);
				oldDis = FloatMath.sqrt(x * x + y * y);

			}

			break;
		}
		case MotionEvent.ACTION_POINTER_UP: {
			Log.d("MultitouchExample", "Pointer up");
			break;
		}
		case MotionEvent.ACTION_UP: {
			Log.d("Multitouch", "Action up");
			// path.lineTo(event.getX(), event.getY());
			// Rest value to original
			ipoiter = 0;
			oldDis = 0;
			newDis = 0;

			break;
		}

		}
		return true;
	}

	private void showDialogQuality() {
		try {
			dialogQuality = new Dialog(E028.this);
			dialogQuality.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogQuality.setContentView(R.layout.layoutlistquality);
			// dialogMg.setCanceledOnTouchOutside(false);
			Window window = dialogQuality.getWindow();
			WindowManager.LayoutParams wlp = window.getAttributes();
			wlp.gravity = Gravity.TOP | Gravity.LEFT;
			// wlp.x = 10; // x position
			wlp.y = 65; // y position
			wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
			window.setAttributes(wlp);

			ListView lvQuality = (ListView) dialogQuality
					.findViewById(R.id.lvQuality);
			lvQuality.setAdapter(listQuality);
			lvQuality.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DTO_Quality dt = listQuality.getItem(arg2);
					txtQuality.setText(dt.Name);
					if (mode == 1) {
						Camera.Parameters cp = mCamera.getParameters();
						cp.setPictureSize(dt.w, dt.h);
						mCamera.setParameters(cp);
						DefaultPictureSizeWidth = dt.w;
						DefaultPictureSizeHeight = dt.h;
					} else {
						QualityId = dt.qualityid;
						DefaultVideQuality = dt.qualityid;
					}
					dialogQuality.cancel();

				}
			});
			dialogQuality.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void showDialogFlash() {
		try {
			final Dialog dialogf = new Dialog(E028.this);
			dialogf.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogf.setContentView(R.layout.layoutlistflash);
			// dialogMg.setCanceledOnTouchOutside(false);
			Window window = dialogf.getWindow();
			WindowManager.LayoutParams wlp = window.getAttributes();
			wlp.gravity = Gravity.TOP | Gravity.RIGHT;
			wlp.x = 40; // x position
			wlp.y = 75; // y position
			wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
			window.setAttributes(wlp);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,
					0);
			TextView btnAuto = (TextView) dialogf.findViewById(R.id.btnAuto);
			btnAuto.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					try {

						Camera.Parameters cp = mCamera.getParameters();
						cp.setFlashMode(Parameters.FLASH_MODE_AUTO);
						mCamera.setParameters(cp);
						mCamera.startPreview();
						CameraFlash = Parameters.FLASH_MODE_AUTO;
						btnFlash.setBackgroundResource(R.drawable.flashauto);
					} catch (Exception e) {
						// TODO: handle exception
					}
					dialogf.cancel();
				}
			});
			if (mode == 2) {
				btnAuto.setLayoutParams(param);
			}
			TextView btnOn = (TextView) dialogf.findViewById(R.id.btnFlashon);
			btnOn.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					try {
						Camera.Parameters cp = mCamera.getParameters();
						if (mode == 1)
							cp.setFlashMode(Parameters.FLASH_MODE_ON);
						else
							cp.setFlashMode(Parameters.FLASH_MODE_TORCH);
						mCamera.setParameters(cp);
						mCamera.startPreview();
						if (mode == 1)
							CameraFlash = Parameters.FLASH_MODE_ON;
						else
							VideoFlash = Parameters.FLASH_MODE_TORCH;

						btnFlash.setBackgroundResource(R.drawable.flashon);
					} catch (Exception e) {
						// TODO: handle exception
					}
					dialogf.cancel();
				}
			});
			TextView btnOff = (TextView) dialogf.findViewById(R.id.btnFlashoff);
			btnOff.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					try {

						Camera.Parameters cp = mCamera.getParameters();
						cp.setFlashMode(Parameters.FLASH_MODE_OFF);
						mCamera.setParameters(cp);
						mCamera.startPreview();
						if (mode == 1)
							CameraFlash = Parameters.FLASH_MODE_OFF;
						else
							VideoFlash = Parameters.FLASH_MODE_OFF;
						btnFlash.setBackgroundResource(R.drawable.flashoff);
					} catch (Exception e) {
						// TODO: handle exception
					}
					dialogf.cancel();
				}
			});

			dialogf.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void setCameraDisplayOrientation(Activity activity,
			int cameraId, android.hardware.Camera camera) {
		try {

			android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
			android.hardware.Camera.getCameraInfo(cameraId, info);
			int rotation = activity.getWindowManager().getDefaultDisplay()
					.getRotation();
			int degrees = 0;
			switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
			}

			int result;
			if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				result = (info.orientation + degrees) % 360;
				result = (360 - result) % 360; // compensate the mirror
			} else { // back-facing
				result = (info.orientation - degrees + 360) % 360;
			}
			camera.setDisplayOrientation(result);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void loadControlbyMode() {

		if (mode == 1) {

			getListResolution();
			btnChangeMode.setBackgroundResource(R.drawable.btnstatuscamera);
			videoButton.setBackgroundResource(R.drawable.btncapture);
			boolean bfind = false;
			for (int i = 0; i < listQuality.getCount(); i++) {
				DTO_Quality dt = listQuality.getItem(i);
				if (dt.w == DefaultPictureSizeWidth
						&& dt.h == DefaultPictureSizeHeight) {
					bfind = true;
					txtQuality.setText(dt.Name);
					Camera.Parameters cp = mCamera.getParameters();
					cp.setPictureSize(dt.w, dt.h);
					mCamera.setParameters(cp);
					break;

				}
			}
			if (!bfind) {
				int idx = listQuality.getCount() / 2;
				DTO_Quality dt = listQuality.getItem(idx);
				txtQuality.setText(dt.Name);
				Camera.Parameters cp = mCamera.getParameters();
				cp.setPictureSize(dt.w, dt.h);
				mCamera.setParameters(cp);
				DefaultPictureSizeWidth = dt.w;
				DefaultPictureSizeHeight = dt.h;
			}
			// Flash
			Camera.Parameters cp = mCamera.getParameters();
			cp.setFlashMode(CameraFlash);
			mCamera.setParameters(cp);
			if (CameraFlash.equals(Parameters.FLASH_MODE_AUTO))
				btnFlash.setBackgroundResource(R.drawable.flashauto);
			else if (CameraFlash.equals(Parameters.FLASH_MODE_OFF))
				btnFlash.setBackgroundResource(R.drawable.flashoff);
			else
				btnFlash.setBackgroundResource(R.drawable.flashon);
			mCamera.startPreview();
		} else {

			getListQuality();
			btnChangeMode.setBackgroundResource(R.drawable.btnstatusvideo);
			videoButton.setBackgroundResource(R.drawable.btnvideo);
			boolean bfind = false;
			for (int i = 0; i < listQuality.getCount(); i++) {
				DTO_Quality dt = listQuality.getItem(i);
				if (dt.qualityid == DefaultVideQuality) {
					bfind = true;
					txtQuality.setText(dt.Name);
					QualityId = dt.qualityid;
					break;

				}
			}
			if (!bfind) {
				int idx = listQuality.getCount() / 2;
				DTO_Quality dt = listQuality.getItem(idx);
				txtQuality.setText(dt.Name);
				QualityId = dt.qualityid;
			}
			// Flash
			Camera.Parameters cp = mCamera.getParameters();
			cp.setFlashMode(VideoFlash);
			mCamera.setParameters(cp);
			if (VideoFlash.equals(Parameters.FLASH_MODE_OFF))
				btnFlash.setBackgroundResource(R.drawable.flashoff);
			else
				btnFlash.setBackgroundResource(R.drawable.flashon);
			mCamera.startPreview();
		}
	}

	int[] qId = { 4, 5, 6 };
	String[] qname = { "低", "中", "高" };

	private void getListQuality() {

		if (listQuality != null)
			listQuality.clear();
		for (int i = 0; i < 3; i++) {
			DTO_Quality dt = new DTO_Quality();
			dt.qualityid = qId[i];
			dt.Name = "画質:" + qname[i];
			listQuality.add(dt);
		}
	}

	private void getListResolution() {

		if (listQuality != null)
			listQuality.clear();
		Camera.Parameters cp = mCamera.getParameters();
		List<Size> sl = cp.getSupportedPictureSizes();
		// now that you have the list of supported sizes, pick one and set it
		// back to the parameters...
		int w = 0, h = 0;

		int idx = 0;
		for (Size s : sl) {
			// if s.width meets whatever criteria you want set it to your w
			// and s.height meets whatever criteria you want for your h
			idx++;
			w = s.width;
			h = s.height;
			DTO_Quality dt = new DTO_Quality();
			dt.Id = String.valueOf(idx);
			dt.w = w;
			dt.h = h;
			String res = "解像度 :" + String.valueOf(w) + "x" + String.valueOf(h);
			dt.Name = res;
			listQuality.add(dt);

		}

	}

	private void getInfoUser() {
		Cursor cur = null;
		try {
			CL_User cl = new CL_User(this);
			cur = cl.getUserJianoNo_InitFLG();
			if (cur != null && cur.getCount() > 0) {

				if (cur.getString(cur.getColumnIndex("DefaultPictureSizeWidth")) != null)
					DefaultPictureSizeWidth = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex("DefaultPictureSizeWidth")));
				if (cur.getString(cur
						.getColumnIndex("DefaultPictureSizeHeight")) != null)
					DefaultPictureSizeHeight = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex("DefaultPictureSizeHeight")));
				if (cur.getString(cur.getColumnIndex("DefaultVideQuality")) != null)
					DefaultVideQuality = Integer.parseInt(cur.getString(cur
							.getColumnIndex("DefaultVideQuality")));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	public static String getAvailableExternalMemorySize() {
		if (externalMemoryAvailable()) {
			File path = Environment.getExternalStorageDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long availableBlocks = stat.getAvailableBlocks();
			return formatSize(availableBlocks * blockSize);
		} else {
			return "0";
		}
	}

	public static boolean externalMemoryAvailable() {
		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}

	public static String formatSize(long size) {
		String suffix = null;

		if (size >= 1024) {
			suffix = "KB";
			size /= 1024;
			if (size >= 1024) {
				suffix = "MB";
				size /= 1024;
			}
		}

		StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

		int commaOffset = resultBuffer.length() - 3;
		while (commaOffset > 0) {
			resultBuffer.insert(commaOffset, ',');
			commaOffset -= 3;
		}

		if (suffix != null)
			resultBuffer.append(suffix);
		return resultBuffer.toString();
	}

	private void confirmDeleteItem() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				CL_FileInfo cl = new CL_FileInfo(E028.this);
				cl.deleteTR_FileInfo(FireStationCode, CaseCode,
						dtDel.MultimediaFileNo, dtDel.FileType);

				File f = new File(dtDel.FileName);
				if (f.exists())
					f.delete();

				listImage.remove(dtDel);
				// String memory = getAvailableInternalMemorySize();
				// txtMemory.setText(memory);
				dialog.cancel();
			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.DeleteItem);
		lbmg.setText(mg);

		dialog.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		E034.SetActivity(this);
		try {
			if (mCamera == null) {
				if (cameraid == CameraInfo.CAMERA_FACING_FRONT)
					mCamera = Camera.open(CameraInfo.CAMERA_FACING_FRONT);
				else if (cameraid == CameraInfo.CAMERA_FACING_BACK)
					mCamera = Camera.open(CameraInfo.CAMERA_FACING_BACK);
				else
					mCamera = getCameraInstance();
				mCamera.setDisplayOrientation(90);

				mCamera.setPreviewCallback(null);
				mPreview = new CameraPreview(this, mCamera);// set preview
				preview.removeAllViews();
				preview.addView(mPreview);
				mCamera.startPreview();
				// setResolutionCamera(resolution);
				// mPreview.setOnTouchListener(new OnTouchListener() {
				//
				// public boolean onTouch(View arg0, MotionEvent arg1) {
				// // TODO Auto-generated method stub
				// layoutSlider.setVisibility(View.VISIBLE);
				// return false;
				// }
				// });
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onResume", e.getMessage());
		}
	}

	void getListFile() {

		Cursor cur = null;
		try {
			if (listImage != null)
				listImage.clear();
			CL_FileInfo cl = new CL_FileInfo(E028.this);
			cur = cl.getTR_FileInfoCamera(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {
					String MultimediaFileNo = cur.getString(cur
							.getColumnIndex("MultimediaFileNo"));
					String FileRegistered_DateTime = cur.getString(cur
							.getColumnIndex("FileRegistered_DateTime"));
					String FileName = cur.getString(cur
							.getColumnIndex("FileName"));
					String FileTime = cur.getString(cur
							.getColumnIndex("FileTime"));
					String FileSize = cur.getString(cur
							.getColumnIndex("FileSize"));
					DTO_FileInfo dt = new DTO_FileInfo();
					dt.MultimediaFileNo = MultimediaFileNo;
					dt.FileRegistered_DateTime = FileRegistered_DateTime;
					dt.FileType = cur.getString(cur.getColumnIndex("FileType"));

					String mFileName;
					if (PathFolder == null || PathFolder.equals("")) {
						mFileName = Environment.getExternalStorageDirectory()
								.getAbsolutePath();
						if (dt.FileType.equals("2")) {
							mFileName += "/" + FolderPicture;
						} else {
							mFileName += "/" + FolderVideo;
						}
					} else {
						mFileName = PathFolder;
					}
					dt.FileName = mFileName + "/" + FileName;
					dt.FileSize = FileSize;
					dt.FileTime = FileTime;
					listImage.add(dt);
					dtDel = dt;
					//uploadFile();
				} while (cur.moveToNext());
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}
		if (listImage.getCount() > 4) {
			myGallery.setSelection(3);
		} else if (listImage.getCount() > 1) {
			myGallery.setSelection(1);
		}
	}

	private void addFiletoList(String fileName) {

		try {
			CL_FileInfo cl = new CL_FileInfo(E028.this);
			int maxfileno = cl.getMaxFileNo(FireStationCode, CaseCode);
			ContentValues values = new ContentValues();
			values.put("FireStationCode", FireStationCode);
			values.put("CaseNo", CaseCode);
			values.put("MultimediaFileNo", maxfileno + 1);
			String FileRegistered_DateTime = Utilities.getDateTimeNow();
			values.put("FileRegistered_DateTime", FileRegistered_DateTime);
			File inFile = new File(fileName);
			values.put("FileName", inFile.getName());
			int size = 0;
			if (mode == 1) {
				values.put("FileType", "2");
				// Bitmap bm = BitmapFactory.decodeFile(fileName);
				size = (int) (inFile.length() / 1024);
				values.put("FileSize", size);

			} else {
				values.put("FileType", "3");

				mPlayer = MediaPlayer.create(this, Uri.parse(inFile.getPath()));
				if (mPlayer != null) {
					size = (int) (inFile.length() / 1024);
					values.put("FileTime", time);
					values.put("FileSize", size);
				}
			}

			cl = new CL_FileInfo(E028.this);
			long res = cl.createFileInfo(values);
			if (res > 0) {
				DTO_FileInfo dt = new DTO_FileInfo();
				dt.MultimediaFileNo = String.valueOf(maxfileno + 1);
				dt.FileRegistered_DateTime = FileRegistered_DateTime;
				dt.FileName = fileName;
				dt.FileSize = String.valueOf(size);
				dt.FileTime = String.valueOf(time);
				if (mode == 1)
					dt.FileType = "2";
				else
					dt.FileType = "3";
				listImage.add(dt);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	protected int sizeOf(Bitmap data) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			return data.getRowBytes() * data.getHeight();
		} else {
			return data.getByteCount();
		}
	}

	private PictureCallback mPicture = new PictureCallback() {

		public void onPictureTaken(byte[] data, Camera camera) {
			new SavePhotoTask().execute(data);
			camera.startPreview();

		}
	};

	class SavePhotoTask extends AsyncTask<byte[], String, String> {
		@Override
		protected String doInBackground(byte[]... jpeg) {

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
			if (pictureFile == null) {
				// Log.d(TAG,
				// "Error creating media file, check storage permissions: " +
				// e.getMessage());
				return null;
			}
			if (pictureFile.exists()) {
				pictureFile.delete();
			}
			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(jpeg[0]);
				fos.close();

				// Xoay
				// Bitmap bmp = BitmapFactory.decodeFile(lastFile);
				// Matrix matrix = new Matrix();
				// matrix.postRotate(90);
				// bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
				// bmp.getHeight(), matrix, true);
				//
				// FileOutputStream out = new FileOutputStream(lastFile);
				// bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
				// out.close();

			} catch (FileNotFoundException e) {
				// Log.d(TAG, "File not found: " + e.getMessage());
			} catch (IOException e) {
				// Log.d(TAG, "Error accessing file: " + e.getMessage());
			}

			return (null);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			addFiletoList(lastFile);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
			if (c != null) {
				Camera.Parameters params = c.getParameters();
				params.setRotation(90);
				c.setParameters(params);
			}
		} catch (Exception e) {
			Log.d("DEBUG", "Camera did not open");
			// Camera is not available (in use or does not exist)

		}
		return c; // returns null if camera is unavailable
	}

	private boolean prepareVideoRecorder() {

		mMediaRecorder = new MediaRecorder();

		// Step 1: Unlock and set camera to MediaRecorder
		mCamera.stopPreview();
		mCamera.unlock();
		mMediaRecorder.setCamera(mCamera);
		// mMediaRecorder.setOrientationHint(90);
		try {

			// Step 2: Set sources
			if (Audio_source > -1)
				mMediaRecorder.setAudioSource(Audio_source);
			else
				mMediaRecorder
						.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			if (Video_source > -1)
				mMediaRecorder.setVideoSource(Video_source);
			else
				mMediaRecorder
						.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
			if (Video_outputformat > -1)
				mMediaRecorder.setOutputFormat(Video_outputformat);
			else
				mMediaRecorder
						.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			if (AudioEncoder > -1)
				mMediaRecorder.setAudioEncoder(AudioEncoder);
			else
				mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			if (Video_encorder > -1)
				mMediaRecorder.setVideoEncoder(Video_encorder);
			else
				mMediaRecorder
						.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);

			// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
			if (CamcorderProfile.hasProfile(cameraid, QualityId))
				mMediaRecorder.setProfile(CamcorderProfile.get(QualityId));
			else
				mMediaRecorder.setProfile(CamcorderProfile
						.get(CamcorderProfile.QUALITY_HIGH));
			if (Video_framerate > -1) {
				mMediaRecorder.setVideoFrameRate(Video_framerate);
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), context);
		}
		// Step 4: Set output file
		mMediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO)
				.toString());

		// Step 5: Set the preview output

		mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

		// Step 6: Prepare configured MediaRecorder
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d("DEBUG", "IllegalStateException preparing MediaRecorder: "
					+ e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d("DEBUG",
					"IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
		return true;
	}

	/** Create a file Uri for saving an image or video */
//	private static Uri getOutputMediaFileUri(int type) {
//		return Uri.fromFile(getOutputMediaFile(type));
//	}

	private static File getOutputMediaFile(int type) {

		String mFileName;
		if (PathFolder == null || PathFolder.equals("")) {
			mFileName = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
			if (type == MEDIA_TYPE_IMAGE) {
				mFileName += "/" + FolderPicture;
			} else {
				mFileName += "/" + FolderVideo;
			}
		} else {
			mFileName = PathFolder;
		}
		File mediaStorageDir = new File(mFileName);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
				return null;
			}
		}
		// File mediaStorageDir = context.getDir(FolderApp,
		// Context.MODE_PRIVATE);

		// Create a media file name
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			if (Image_outputformat == 0)
				mediaFile = new File(mediaStorageDir.getPath() + File.separator
						+ FireStationCode + "_" + CaseCode + "_"
						+ Utilities.getDateTimeNow() + ".jpg");
			else if (Image_outputformat == 2)
				mediaFile = new File(mediaStorageDir.getPath() + File.separator
						+ FireStationCode + "_" + CaseCode + "_"
						+ Utilities.getDateTimeNow() + ".webm");
			else
				mediaFile = new File(mediaStorageDir.getPath() + File.separator
						+ FireStationCode + "_" + CaseCode + "_"
						+ Utilities.getDateTimeNow() + ".png");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ FireStationCode + "_" + CaseCode + "_"
					+ Utilities.getDateTimeNow() + ".mp4");
		} else {
			return null;
		}
		lastFile = mediaFile.getPath();
		return mediaFile;
	}

	@Override
	protected void onPause() {
		super.onPause();
		releaseMediaRecorder(); // if you are using MediaRecorder, release it
								// first
		releaseCamera(); // release the camera immediately on pause event
		if (myOrientationEventListener != null)
			myOrientationEventListener.disable();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (myOrientationEventListener != null)
			myOrientationEventListener.disable();
		releaseMediaRecorder(); // if you are using MediaRecorder, release it
		// first
		releaseCamera(); // release the camera immediately on pause event
	}

	private void releaseMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset(); // clear recorder configuration
			mMediaRecorder.release(); // release the recorder object
			mMediaRecorder = null;
			mCamera.setPreviewCallback(null);
			mCamera.lock(); // lock camera for later use
		}
	}

	private void releaseCamera() {
		if (mCamera != null) {
			isRecording = false;
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	private void playSound() {
		mp = MediaPlayer.create(this, R.raw.camera);
		mp.setOnCompletionListener(new OnCompletionListener() {

			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				mp.release();
			}
		});
		mp.start();
	}

	// ***********************************
	// Capture camera
	// ************************************
	public class profileAdapter extends ArrayAdapter<DTO_FileInfo> {

		public profileAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraperDV mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutimage, null);
				mwp = new ViewWraperDV(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraperDV) convertView.getTag();
			}

			// CheckBox chk = mwp.getCheckBox();
			ImageView img = mwp.getImageView();
			ImageView imgVideo = mwp.getImageVideo();
			final DTO_FileInfo dt = this.getItem(position);
			if (dt.FileType.equals("2")) {
				// chk.setText("Image");
				// if (bitmap != null) {
				// bitmap.recycle();
				// }
				// video.setLayoutParams(params02);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				// Bitmap bm = BitmapFactory.decodeFile(dt.FileName);
				img.setScaleType(ImageView.ScaleType.CENTER_CROP);

				// ExifInterface exif;
				try {
					// exif = new ExifInterface(dt.FileName);
					//
					// int orientation = exif.getAttributeInt(
					// ExifInterface.TAG_ORIENTATION, 0);
					// Log.d("EXIF", "Exif: " + orientation);
					// Matrix matrix = new Matrix();
					// if (orientation == 6) {
					// matrix.postRotate(90);
					// Log.d("EXIF", "Exif: " + orientation);
					// } else if (orientation == 3) {
					// matrix.postRotate(180);
					// Log.d("EXIF", "Exif: " + orientation);
					// } else if (orientation == 8) {
					// matrix.postRotate(270);
					// Log.d("EXIF", "Exif: " + orientation);
					// }
					Bitmap bmp = Bitmap
							.createBitmap(decodeSampledBitmapFromResource(
									dt.FileName, 50, 50));
					// bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
					// bmp.getHeight(), matrix, true);
					img.setImageBitmap(bmp);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				imgVideo.setVisibility(View.INVISIBLE);
			} else if (dt.FileType.equals("3")) {
				// img.setLayoutParams(params0);
				// chk.setText("Video");
				Bitmap bm = ThumbnailUtils.createVideoThumbnail(dt.FileName,
						MediaStore.Images.Thumbnails.MINI_KIND);
				img.setScaleType(ImageView.ScaleType.CENTER_CROP);
				img.setImageBitmap(bm);

				imgVideo.setVisibility(View.VISIBLE);
				// video.setVideoPath(dt.FileName);
				// video.requestFocus();
			}

			return v;
		}
	}

	class ViewWraperDV {

		View base;

		ImageView img = null;
		ImageView img2 = null;

		ViewWraperDV(View base) {

			this.base = base;
		}

		ImageView getImageView() {
			if (img == null) {
				img = (ImageView) base.findViewById(R.id.imageView1);
			}
			return img;
		}

		ImageView getImageVideo() {
			if (img2 == null) {
				img2 = (ImageView) base.findViewById(R.id.imageVideo);
			}
			return img2;
		}
		// VideoView getVideoView() {
		// if (video == null) {
		// video = (VideoView) base.findViewById(R.id.videoView1);
		// }
		// return video;
		// }

	}

	public Bitmap decodeSampledBitmapFromResource(String fileName,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// BitmapFactory.decodeResource(res, resId, options);
		BitmapFactory.decodeFile(fileName, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(fileName, options);
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	public class adapterActivity extends ArrayAdapter<DTO_Quality> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		// @Override
		// public View getDropDownView(int position, View convertView,
		// ViewGroup parent) {
		// // TODO Auto-generated method stub
		// return getViewChung(position, convertView, parent, true);
		// }

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraper mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutrowquality, null);
				mwp = new ViewWraper(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraper) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = mwp.getLable();
			DTO_Quality dt = new DTO_Quality();
			dt = this.getItem(position);
			txt.setText(dt.Name);
			// LinearLayout layout = mwp.getLayoutrow();

			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView lable1 = null;

		// LinearLayout layout = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtQuality);
			}
			return lable1;
		}

	}

	// //
	// ProgressDialog dialog = null;
	// String upLoadServerUri = null;
	// int serverResponseCode = 0;
	//
	// private void uploadFile() {
	//
	// if (dialog == null)
	// dialog = ProgressDialog.show(E028.this, "", "Uploading file...",
	// true);
	//
	// // String fileName = sourceFileUri;
	// new UpLoadServer().execute(dtDel.FileName);
	//
	// }
	//
	// class UpLoadServer extends AsyncTask<String, Void, Integer> {
	//
	// // private Exception exception;
	//
	// protected Integer doInBackground(String... urls) {
	// String fileName = urls[0];
	// TrustManagerManipulator.allowAllSSL();
	// HttpURLConnection conn = null;
	// DataOutputStream dos = null;
	// String lineEnd = "\r\n";
	// String twoHyphens = "--";
	// String boundary = "*****";
	// int bytesRead, bytesAvailable, bufferSize;
	// byte[] buffer;
	// int maxBufferSize = 1 * 1024 * 1024;
	// File sourceFile = new File(fileName);
	//
	// if (!sourceFile.isFile()) {
	//
	// return 0;
	//
	// } else {
	// try {
	//
	// // open a URL connection to the Servlet
	// FileInputStream fileInputStream = new FileInputStream(
	// sourceFile);
	// URL url = new URL(upLoadServerUri);
	//
	// // Open a HTTP connection to the URL
	// conn = (HttpURLConnection) url.openConnection();
	// conn.setDoInput(true); // Allow Inputs
	// conn.setDoOutput(true); // Allow Outputs
	// conn.setUseCaches(false); // Don't use a Cached Copy
	// conn.setRequestMethod("POST");
	// conn.setRequestProperty("Connection", "Keep-Alive");
	// conn.setRequestProperty("ENCTYPE", "multipart/form-data");
	// conn.setRequestProperty("Content-Type",
	// "multipart/form-data;boundary=" + boundary);
	// conn.setRequestProperty("uploaded_file", fileName);
	//
	// dos = new DataOutputStream(conn.getOutputStream());
	//
	// dos.writeBytes(twoHyphens + boundary + lineEnd);
	// // dos.writeBytes("Content-Disposition: form-data; name="
	// // + fileName + ";filename=your_image_name.jpeg" + ""
	// // + lineEnd);
	// dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
	// + fileName + "\"" + lineEnd);
	// dos.writeBytes(lineEnd);
	//
	// // create a buffer of maximum size
	// bytesAvailable = fileInputStream.available();
	//
	// bufferSize = Math.min(bytesAvailable, maxBufferSize);
	// buffer = new byte[bufferSize];
	//
	// // read file and write it into form...
	// bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	//
	// while (bytesRead > 0) {
	//
	// dos.write(buffer, 0, bufferSize);
	// bytesAvailable = fileInputStream.available();
	// bufferSize = Math.min(bytesAvailable, maxBufferSize);
	// bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	//
	// }
	//
	// // send multipart form data necesssary after file data...
	// dos.writeBytes(lineEnd);
	// dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	//
	// // Responses from the server (code and message)
	// serverResponseCode = conn.getResponseCode();
	// String serverResponseMessage = conn.getResponseMessage();
	//
	// Log.i("uploadFile", "HTTP Response is : "
	// + serverResponseMessage + ": " + serverResponseCode);
	//
	// fileInputStream.close();
	// dos.flush();
	// dos.close();
	//
	// } catch (MalformedURLException ex) {
	//
	// Log.e("Upload file to server", "error: " + ex.getMessage(),
	// ex);
	// } catch (Exception e) {
	//
	// Log.e("Upload file to server Exception",
	// "Exception : " + e.getMessage(), e);
	// }
	//
	// // End else block
	// }
	//
	// return serverResponseCode;
	// }
	//
	// protected void onPostExecute(Integer result) {
	// // TODO: check this.exception
	// // TODO: do something with the feed
	// if (dialog != null)
	// dialog.dismiss();
	// if (serverResponseCode == 200) {
	//
	// Toast.makeText(E028.this, "File Upload Complete.",
	// Toast.LENGTH_SHORT).show();
	// }
	// }
	// }

}
