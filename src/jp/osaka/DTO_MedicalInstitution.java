﻿package jp.osaka;

import java.io.Serializable;

public class DTO_MedicalInstitution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2922386193803108643L;
	/**
	 * 
	 */
	
	private String MICode;
	private String Opening_DATE;
	private String Closing_DATE;
	private String StopFLG;
	private String MIClass;
	private String SHCACode;
	private String AreaCode;
	private String MIName_Kanji;
	private String MIName_Kana;
	private String PostNo;
	private String Address_Kanji;
	private String Address_Kana;
	private String EMailAddress;
	private String TelNo1;
	private String TelNo2;
	private String TransportationTelNo;
	private String FaxNo ;
	private String ThirdCoordinateTelNo ;
	private String Latitude ;
	private String Longitude;
	private String EmergencyMIFLG;
	private String MIType;
	private String EmergencyCenterFLG;
	private String CalamityMIFLG;
	private String CCUTelNo;
	private String SCUTelNo;
	private String HelicopterTelNo;
	private String HotLineTelNo ;
	private String DeleteFLG;
	private String Update_DATETIME;
	private double distance;
	private String today;
	private String tomorow;
	private String timeToday;
	private String timeTomorow;
	private String ConditionBranchName;
	private int img1;
	private int img2;
	private int img3;
	private int img4;
	private String KamokuName;
	private String Oujyu;
	private String Operation;
	private String KamokuCode;
	private boolean ISOuY;
	private String Otoday;
	private String Otomorow;
	private String ConditionCode;
	public String getKamokuCode() {
		return KamokuCode;
	}
	public void setKamokuCode(String kamokuCode) {
		KamokuCode = kamokuCode;
	}
	public String getKamokuName() {
		return KamokuName;
	}
	public void setKamokuName(String kamokuName) {
		KamokuName = kamokuName;
	}
	public String getOujyu() {
		return Oujyu;
	}
	public void setOujyu(String oujyu) {
		Oujyu = oujyu;
	}
	public String getOperation() {
		return Operation;
	}
	public void setOperation(String operation) {
		Operation = operation;
	}
	public int getImg1() {
		return img1;
	}
	public void setImg1(int img1) {
		this.img1 = img1;
	}
	public int getImg2() {
		return img2;
	}
	public void setImg2(int img2) {
		this.img2 = img2;
	}
	public int getImg3() {
		return img3;
	}
	public void setImg3(int img3) {
		this.img3 = img3;
	}
	public int getImg4() {
		return img4;
	}
	public void setImg4(int img4) {
		this.img4 = img4;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public String getMICode() {
		return MICode;
	}
	public void setMICode(String mICode) {
		MICode = mICode;
	}
	public String getOpening_DATE() {
		return Opening_DATE;
	}
	public void setOpening_DATE(String opening_DATE) {
		Opening_DATE = opening_DATE;
	}
	public String getClosing_DATE() {
		return Closing_DATE;
	}
	public void setClosing_DATE(String closing_DATE) {
		Closing_DATE = closing_DATE;
	}
	public String getStopFLG() {
		return StopFLG;
	}
	public void setStopFLG(String stopFLG) {
		StopFLG = stopFLG;
	}
	public String getMIClass() {
		return MIClass;
	}
	public void setMIClass(String mIClass) {
		MIClass = mIClass;
	}
	public String getSHCACode() {
		return SHCACode;
	}
	public void setSHCACode(String sHCACode) {
		SHCACode = sHCACode;
	}
	public String getAreaCode() {
		return AreaCode;
	}
	public void setAreaCode(String areaCode) {
		AreaCode = areaCode;
	}
	public String getMIName_Kanji() {
		return MIName_Kanji;
	}
	public void setMIName_Kanji(String mIName_Kanji) {
		MIName_Kanji = mIName_Kanji;
	}
	public String getMIName_Kana() {
		return MIName_Kana;
	}
	public void setMIName_Kana(String mIName_Kana) {
		MIName_Kana = mIName_Kana;
	}
	public String getPostNo() {
		return PostNo;
	}
	public void setPostNo(String postNo) {
		PostNo = postNo;
	}
	public String getAddress_Kanji() {
		return Address_Kanji;
	}
	public void setAddress_Kanji(String address_Kanji) {
		Address_Kanji = address_Kanji;
	}
	public String getAddress_Kana() {
		return Address_Kana;
	}
	public void setAddress_Kana(String address_Kana) {
		Address_Kana = address_Kana;
	}
	public String getEMailAddress() {
		return EMailAddress;
	}
	public void setEMailAddress(String eMailAddress) {
		EMailAddress = eMailAddress;
	}
	public String getTelNo1() {
		return TelNo1;
	}
	public void setTelNo1(String telNo1) {
		TelNo1 = telNo1;
	}
	public String getTelNo2() {
		return TelNo2;
	}
	public void setTelNo2(String telNo2) {
		TelNo2 = telNo2;
	}
	public String getTransportationTelNo() {
		return TransportationTelNo;
	}
	public void setTransportationTelNo(String transportationTelNo) {
		TransportationTelNo = transportationTelNo;
	}
	public String getFaxNo() {
		return FaxNo;
	}
	public void setFaxNo(String faxNo) {
		FaxNo = faxNo;
	}
	public String getThirdCoordinateTelNo() {
		return ThirdCoordinateTelNo;
	}
	public void setThirdCoordinateTelNo(String thirdCoordinateTelNo) {
		ThirdCoordinateTelNo = thirdCoordinateTelNo;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getEmergencyMIFLG() {
		return EmergencyMIFLG;
	}
	public void setEmergencyMIFLG(String emergencyMIFLG) {
		EmergencyMIFLG = emergencyMIFLG;
	}
	public String getMIType() {
		return MIType;
	}
	public void setMIType(String mIType) {
		MIType = mIType;
	}
	public String getEmergencyCenterFLG() {
		return EmergencyCenterFLG;
	}
	public void setEmergencyCenterFLG(String emergencyCenterFLG) {
		EmergencyCenterFLG = emergencyCenterFLG;
	}
	public String getCalamityMIFLG() {
		return CalamityMIFLG;
	}
	public void setCalamityMIFLG(String calamityMIFLG) {
		CalamityMIFLG = calamityMIFLG;
	}
	public String getCCUTelNo() {
		return CCUTelNo;
	}
	public void setCCUTelNo(String cCUTelNo) {
		CCUTelNo = cCUTelNo;
	}
	public String getSCUTelNo() {
		return SCUTelNo;
	}
	public void setSCUTelNo(String sCUTelNo) {
		SCUTelNo = sCUTelNo;
	}
	public String getHelicopterTelNo() {
		return HelicopterTelNo;
	}
	public void setHelicopterTelNo(String helicopterTelNo) {
		HelicopterTelNo = helicopterTelNo;
	}
	public String getHotLineTelNo() {
		return HotLineTelNo;
	}
	public void setHotLineTelNo(String hotLineTelNo) {
		HotLineTelNo = hotLineTelNo;
	}
	public String getDeleteFLG() {
		return DeleteFLG;
	}
	public void setDeleteFLG(String deleteFLG) {
		DeleteFLG = deleteFLG;
	}
	public String getUpdate_DATETIME() {
		return Update_DATETIME;
	}
	public void setUpdate_DATETIME(String update_DATETIME) {
		Update_DATETIME = update_DATETIME;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}
	public String getTomorow() {
		return tomorow;
	}
	public void setTomorow(String tomorow) {
		this.tomorow = tomorow;
	}
	public String getTimeToday() {
		return timeToday;
	}
	public void setTimeToday(String timeToday) {
		this.timeToday = timeToday;
	}
	public String getTimeTomorow() {
		return timeTomorow;
	}
	public void setTimeTomorow(String timeTomorow) {
		this.timeTomorow = timeTomorow;
	}
	public String getConditionBranchName() {
		return ConditionBranchName;
	}
	public void setConditionBranchName(String conditionBranchName) {
		ConditionBranchName = conditionBranchName;
	}
	public boolean isISOuY() {
		return ISOuY;
	}
	public void setISOuY(boolean iSOuY) {
		ISOuY = iSOuY;
	}
	public String getOtoday() {
		return Otoday;
	}
	public void setOtoday(String otoday) {
		Otoday = otoday;
	}
	public String getOtomorow() {
		return Otomorow;
	}
	public void setOtomorow(String otomorow) {
		Otomorow = otomorow;
	}
	public String getConditionCode() {
		return ConditionCode;
	}
	public void setConditionCode(String conditionCode) {
		ConditionCode = conditionCode;
	}
	public boolean bKahi=false;
}
