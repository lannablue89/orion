package jp.osaka;

import jp.osaka.db.model.TR_TriageSTARTMethod_Model;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CL_TR_TriageSTARTMethod {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_TR_TriageSTARTMethod(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public Cursor getTR_TriageSTARTMethod(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_TriageSTARTMethod Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getTR_TriageSTARTMethod_Result(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_TriageSTARTMethod_Result Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	
	/*
	 * author Lan Tran 
	 */
	// Select/Insert/Update for tr_triagestartmethod
	
	public TR_TriageSTARTMethod_Model getTR_TriageSTARTMethod_Model(String FireStationCode, String CaseCode) {
		TR_TriageSTARTMethod_Model model = null;
		Cursor cursor = null;
		try {
			database = dbHelper.getWritableDatabase();
			String queryString = String.format(
					"Select * from TR_TriageStartMethod Where FireStationCode=%s and CaseNo=%s ",
					FireStationCode, CaseCode);
			Log.d("db_test", "queryString=" + queryString);
			cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				cursor.moveToFirst();
				model = new TR_TriageSTARTMethod_Model(cursor);
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return model;
	}
	
	public int createTR_TriageSTARTMethod_Model(TR_TriageSTARTMethod_Model model) {
		int value = -1;
		if (model != null) {
			try {
				database = dbHelper.getWritableDatabase();
				value = database.delete("TR_TriageStartMethod", "FireStationCode=? and CaseNo=?", 
						new String[] {model.FireStationCode, model.CaseNo});
				if (value > 0) {
					value = (int) database.insert("TR_TriageStartMethod", null, model.getValues());
				} else {
					value = -1;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close();
			}
		}
		return value;
	}
	
	public int updateTR_TriageSTARTMethod_Model(TR_TriageSTARTMethod_Model model) {
		int value = -1;
		if (model != null) {
			try {
				database = dbHelper.getWritableDatabase();
				value = database.update("TR_TriageStartMethod", model.getValues(), 
						"FireStationCode=? and CaseNo=?", 
						new String[] {model.FireStationCode, model.CaseNo});
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close();
			}
		}
		return value;
	}
	
	// Select/Insert/Update for tr_triagestartmethod_result
	
	public TR_TriageSTARTMethod_Model selectTR_TriageSTARTMethod_result_Model(
			String fireStationCode, String caseNo, String eMSInsert_Datetime) {
		TR_TriageSTARTMethod_Model model = null;
		try {
			database = dbHelper.getWritableDatabase();
			String queryString = String.format(
					"Select * from TR_TriageStartMethod_result " +
							" Where FireStationCode=%s and CaseNo=%s and EMSInsert_Datetime=%s",
							fireStationCode, caseNo, eMSInsert_Datetime);
			Log.d("db_test", "queryString=" + queryString);
			Cursor cursor = database.rawQuery(queryString, null);
			if (cursor != null) {
				cursor.moveToFirst();
				model = new TR_TriageSTARTMethod_Model(cursor);
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return model;
	}
	
	public int insertTR_TriageSTARTMethod_result_Model(TR_TriageSTARTMethod_Model model) {
		int value = -1;
		if (model != null) {
			try {
				database = dbHelper.getWritableDatabase();
				value = (int) database.insert("TR_TriageStartMethod_result", null, model.getValues());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close();
			}
		}
		return value;
	}
	
	public int updateTR_TriageSTARTMethod_result_Model(TR_TriageSTARTMethod_Model model) {
		int value = -1;
		if (model != null) {
			try {
				database = dbHelper.getWritableDatabase();
				value = database.update("TR_TriageStartMethod_result", model.getValues(), 
						"FireStationCode=? and CaseNo=? and EMSInsert_Datetime=?", 
						new String[] {model.FireStationCode, model.CaseNo, model.EMSInsert_Datetime});
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close();
			}
		}
		return value;
	}
	
	public int insertOrUpdateTR_TriageSTARTMethod_result_Model(TR_TriageSTARTMethod_Model model) {
		database = dbHelper.getWritableDatabase();
		Cursor cursor = database.rawQuery("Select * from TR_TriageStartMethod_result " +
				" Where FireStationCode=? and CaseNo=? and EMSInsert_Datetime=?", 
				new String[] {model.FireStationCode, model.CaseNo, model.EMSInsert_Datetime});
	   boolean exists = (cursor.getCount() > 0);
	   cursor.close();
	   
	   if (exists) {
		   return updateTR_TriageSTARTMethod_result_Model(model);
	   } else {
		   return insertTR_TriageSTARTMethod_result_Model(model);
	   }
	}
	// end of Lan Tran 
}
