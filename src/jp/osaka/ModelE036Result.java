package jp.osaka;

import java.util.ArrayList;
import java.util.HashMap;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

public class ModelE036Result {

	private String title;
	private int choosenValue;
	private String subLine1;
	private String subLine2;
	private String subLine3;
	private View view;

	public ModelE036Result(String title, int choosenValue, String subLine1,
			String subLine2, String subLine3) {
		super();
		this.title = title;
		this.choosenValue = choosenValue;
		this.subLine1 = subLine1;
		this.subLine2 = subLine2;
		this.subLine3 = subLine3;
	}

	public String getSubLine1() {
		return subLine1;
	}

	public void setSubLine1(String subLine1) {
		this.subLine1 = subLine1;
	}

	public String getSubLine2() {
		return subLine2;
	}

	public void setSubLine2(String subLine2) {
		this.subLine2 = subLine2;
	}

	public String getSubLine3() {
		return subLine3;
	}

	public void setSubLine3(String subLine3) {
		this.subLine3 = subLine3;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		RefreshView();
	}

	public int getChoosenValue() {
		return choosenValue;
	}

	public void setChoosenValue(int choosenValue) {
		this.choosenValue = choosenValue;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
		initView();
	}

	public void initView() {
		switch (choosenValue) {
		case 0:
			subLine1 = "黒 ： 0";
			subLine2 = "死亡群";
			break;

		case 1:
			subLine1 = "赤 : Ⅰ";
			subLine2 = "緊急治療";
			break;
		case 2:
			subLine1 = "黄：Ⅱ";
			subLine2 = "待機的治療群";
			break;
		case 3:
			subLine1 = "緑： Ⅲ";
			subLine2 = "保留群";
			break;
		default:
			break;
		}
	}

	public void RefreshView() {
		if(choosenValue==-1){
			return;
		}
		TextView subtitle1 = (TextView) view.findViewById(R.id.subtitle1);
		TextView subtitle2 = (TextView) view.findViewById(R.id.subtitle2);
		TextView subtitle3 = (TextView) view.findViewById(R.id.subtitle3);

		subtitle1.setText(subLine1);
		subtitle2.setText(subLine2);
		subtitle3.setText(subLine3);
		View bg_btn = view.findViewById(R.id.bg_btn);
		switch (choosenValue) {
		case 0:
			bg_btn.setBackgroundColor(Color.BLACK);
			break;
		case 1:
			bg_btn.setBackgroundColor(Color.RED);
			break;
		case 2:
			bg_btn.setBackgroundColor(Color.YELLOW);
			break;
		case 3:
			bg_btn.setBackgroundColor(Color.GREEN);
			break;
		default:
			break;
		}
	}
}
