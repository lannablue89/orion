package jp.osaka;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterE036 implements IDataChange {

	LinearLayout header, body;
	static LayoutInflater inflater;
	View parent;
	ViewGroup rootView;
	Context mContext;
	int customheader, custombody;
	ModelE036Root data;
	int tag;

	public AdapterE036(Context context, ModelE036Root data, int layoutHeader,
			int layoutBody) {
		mContext = context;
		this.data = data;
		customheader = layoutHeader;
		custombody = layoutBody;
		// this.rootView=rootView;
		initView();
		initHeader();
		initBody();
		E036.listeners.add(this);
		tag = data.getTag();
	}

	protected void initView() {
		if (inflater == null)
			inflater = LayoutInflater.from(mContext);
		parent = inflater.inflate(R.layout.customexpanable, null);
		header = (LinearLayout) parent.findViewById(R.id.header);
		body = (LinearLayout) parent.findViewById(R.id.body);
	}

	protected void initHeader() {
		View headerview = inflater.inflate(customheader, null);
		TextView title = (TextView) headerview.findViewById(R.id.title);
		title.setText(data.getTitle());
		header.addView(headerview);
		if (data.isBackground()) {
			header.setBackgroundResource(R.drawable.btngreyup);
		} else {
			header.setBackgroundResource(R.drawable.btnredup);
		}

		header.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (body.getVisibility() == View.GONE) {
					expand();
				} else {
					collapse();
				}
			}
		});
	}

	private void initBody() {
		for (Object model : data.getData()) {
			View v = null;
			if (model instanceof ModelE036) {
				v = inflater.inflate(custombody, null);
				((ModelE036) model).setView(v);
				body.addView(v);
			} else if (model instanceof ModelE036Root) {
				AdapterE036 adapter = new AdapterE036(mContext,
						(ModelE036Root) model, customheader, custombody);
				v = adapter.getView();
				body.addView(v);
			} else if (model instanceof ModelE036Result) {
				v = inflater.inflate(R.layout.result_e036, null);
				((ModelE036Result) model).setView(v);
				body.addView(v);
			}
		}
	}

	public void setEnable() {

	}

	public boolean isEnable() {
		return true;
	}

	public void collapse() {
		body.setVisibility(View.GONE);
		
		if (data.isBackground()) {
			header.setBackgroundResource(R.drawable.btngreydown);
		} else {
			header.setBackgroundResource(R.drawable.btnreddown);
		}
		
		

	}

	public void expand() {
		body.setVisibility(View.VISIBLE);
		if (data.isBackground()) {
			header.setBackgroundResource(R.drawable.btngreyup);
		} else {
			header.setBackgroundResource(R.drawable.btnredup);
		}
	}

	public View getView() {
		return parent;
	}

	@Override
	public void OnDataChange(int tag, boolean explan) {
		if (tag == this.tag) {
			if (explan) {
				expand();
			} else {
				collapse();
			}
		}
	}
}
