package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_NightClinic {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_NightClinic(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public Cursor getMS_NightClinic(String HopitalName,String rdiSearch) {
		Cursor mCursor = null;
		try {
			
			String sql = "Select * from MS_NightClinic e";
			sql += " Where DeleteFLG='0' ";
			if (rdiSearch.equals("2") && HopitalName != null
					&& !HopitalName.trim().equals("")) {
				sql += " and  (e.MIName_Kanji Like '%"
						+ HopitalName.replace("'", "''")
						+ "%' or e.MIName_Kana Like '%"
						+ HopitalName.replace("'", "''") + "%') ";
			}
			sql += "  order by sortno asc, MIName_Kanji asc ";
			mCursor = database.rawQuery(sql, null);
			if(mCursor!=null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getMS_NightClinicDetail(String MICode) {
		Cursor mCursor = null;
		try {
			String[] args = { MICode };
			String sql = "Select * from MS_NightClinic ";
			sql += " Where MICode=? ";

			mCursor = database.rawQuery(sql, args);
			if (mCursor != null)
				mCursor.moveToFirst();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
