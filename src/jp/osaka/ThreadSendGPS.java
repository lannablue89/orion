package jp.osaka;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import jp.osaka.ContantTable.TR_CaseCount;
import jp.osaka.ThreadAutoShowButton.myrunalbe;
import jp.osaka.Utilities.TrustManagerManipulator;

import com.google.android.maps.GeoPoint;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;

public class ThreadSendGPS {

	Context con;
	int GPSDBTime;
	private static boolean Running = false;
	String emsunitcode = "";
	LocationManager locationManager;
	static Location location;
	int AmbulanceSpeed = 0;

	public static void setRunning(boolean running) {
		Running = running;
		location = null;
	}

	public boolean getRunning() {
		return Running;
	}

	public void setLocation(Location loc) {
		location = loc;

	}

	private void getAmbulanceSpeed() {
		Cursor cur = null;
		try {
			CL_User cl = new CL_User(con);
			cur = cl.getEMSUnitTerminalManagement();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex("AmbulanceSpeed")) != null)
					AmbulanceSpeed = Integer.parseInt(cur.getString(cur
							.getColumnIndex("AmbulanceSpeed")));

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();

		}
	}

	public void SendGPS(Context c, LocationManager lm) {
		con = c;
		CL_MSSetting cl = new CL_MSSetting(con);
		GPSDBTime = cl.getGPSDBTime();
		Running = true;
		CL_User us = new CL_User(con);
		emsunitcode = us.getUserName();
		locationManager = lm;
		getAmbulanceSpeed();
		startThread();

	}

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub

			try {
				while (Running) {
					try {

						Thread.sleep(GPSDBTime);
						if (location != null) {
							CL_TR_EMSunitlocationinfo cl = new CL_TR_EMSunitlocationinfo(
									con);
							cl.CopyTR_EMSunitlocationinfo();
							CL_TRCase tr = new CL_TRCase(con);
							String MICode = tr.getMICode();
							double latM = 0;
							double longM = 0;
							if (MICode.equals("") == false) {
								tr = new CL_TRCase(con);
								Cursor cur = tr.getHopital(MICode);
								if (cur != null && cur.getCount() > 0) {
									String lt = cur.getString(cur
											.getColumnIndex("Latitude"));
									String li = cur.getString(cur
											.getColumnIndex("Longitude"));
									if (lt != null && lt.equals("") == false)
										latM = Double.parseDouble(lt);
									if (li != null && li.equals("") == false)
										longM = Double.parseDouble(li);
								}
								if(cur!=null)
									cur.close();
							}
							long gpstime = location.getTime();
							Date date = new Date(gpstime);
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyyMMddHHmmss");
							// sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
							String formattedDate = sdf.format(date);

							double lat = location.getLatitude();
							double longi = location.getLongitude();
							float accuracy = location.getAccuracy();
							double altitude = location.getAltitude();
							double speed = location.getSpeed();
							String incidentscene = "";
							String country = "";
							if (isOnline()) {
								GeoPoint p;
								Geocoder geocoder = new Geocoder(con,
										Locale.getDefault());
								p = new GeoPoint(
										(int) (location.getLatitude() * 1E6),
										(int) (location.getLongitude() * 1E6));
								List<Address> add = null;
								try {
									add = geocoder.getFromLocation(
											p.getLatitudeE6() / 1E6,
											p.getLongitudeE6() / 1E6, 1);
								} catch (Exception ex) {
								}
								incidentscene = "";
								if (add != null && add.size() > 0) {
									// Get address
									String post = "";
									for (int i = 0; i <= add.get(0)
											.getMaxAddressLineIndex(); i++) {
										country = add.get(0).getCountryName();
										post = add.get(0).getPostalCode();
										if (!add.get(0).getAddressLine(i)
												.equals(country)) {
											if (incidentscene != null
													&& !incidentscene
															.equals("")) {
												incidentscene = incidentscene
														+ ", ";
											}
											incidentscene += add.get(0)
													.getAddressLine(i);
										}
									}
									if (post != null
											&& incidentscene.contains(post)) {
										incidentscene = incidentscene.replace(
												post, "");
									}
								}
							}
							// //

							String straightlinedistance = "";
							String arrivaltime = "";
							if (MICode.equals("") == false) {
								Location locationB = new Location("point B");
								locationB.setLatitude(latM);
								locationB.setLongitude(longM);
								double distance = (double) location
										.distanceTo(locationB);
								distance = Utilities
										.roundTwoDecimals(distance / 1000);
								straightlinedistance = String.valueOf(distance);
								if (AmbulanceSpeed != 0) {
									double arrtime = distance / AmbulanceSpeed;
									int time = (int) (arrtime * 60 * 60); // change
																			// to s
									Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND, time);
									arrivaltime = Utilities.getDateTimeNow(c);
								}
								
							}

							int networkinfosubtype = getNetWorkType();
							String networkinfotypename = get_networkName();
							ContentValues values = new ContentValues();
							values.put("emsunitcode", emsunitcode);
							values.put("gpsacquisition_datetime", formattedDate);
							values.put("latitude_is", lat);
							values.put("longitude_is", longi);
							values.put("accuracy", accuracy);
							values.put("altitude", altitude);
							values.put("speed", speed);
							values.put("incidentscene", incidentscene);
							values.put("straightlinedistance",
									straightlinedistance);
							values.put("arrivaltime", arrivaltime);
							values.put("networkinfosubtype", networkinfosubtype);
							values.put("networkinfotypename",
									networkinfotypename);
							values.put("update_datetime",
									Utilities.getDateTimeNow());
							CL_TR_EMSunitlocationinfo ems = new CL_TR_EMSunitlocationinfo(
									con);
							long res=ems.createtr_emsunitlocationinfo(values);

						}
						if (isOnline()) {

							JSONArray array = new JSONArray();
							CL_TR_EMSunitlocationinfo ems=new CL_TR_EMSunitlocationinfo(con);
							Cursor cur=ems.gettr_emsunitlocationinfo();
							JSONObject trinfo = ParseToJson(cur, "tr_emsunitlocationinfo");
							array.put(trinfo);
							
							String lastinsert_datetime;
							SharedPreferences sh=con.getSharedPreferences("app", con.MODE_PRIVATE);
							lastinsert_datetime=sh.getString("GPSLastTime", "19990101010101");
							
							
							ems=new CL_TR_EMSunitlocationinfo(con);
							cur =ems.gettr_emsunitlocationinfo(lastinsert_datetime);
							JSONObject jAdd = ParseToJson(cur,
									"tr_emsunitlocationinfo_result");
							array.put(jAdd);
							ems=new CL_TR_EMSunitlocationinfo(con);
							lastinsert_datetime=ems.getlastinsert_datetime();
							Editor e=sh.edit();
							e.putString("GPSLastTime", lastinsert_datetime);
							e.commit();
							
							int res = uploadDataToServer(array.toString());

							//
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					// //
				}

			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");

			} finally {

			}

		}
	};

	private int getNetWorkType() {
		TelephonyManager tm = (TelephonyManager) con
				.getSystemService(Context.TELEPHONY_SERVICE);
		int networkType = tm.getNetworkType();
		switch (networkType) {
		case TelephonyManager.NETWORK_TYPE_GPRS:
		case TelephonyManager.NETWORK_TYPE_EDGE:
		case TelephonyManager.NETWORK_TYPE_CDMA:
		case TelephonyManager.NETWORK_TYPE_1xRTT:
		case TelephonyManager.NETWORK_TYPE_IDEN:
			return 2;
		case TelephonyManager.NETWORK_TYPE_UMTS:
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
		case TelephonyManager.NETWORK_TYPE_EVDO_B:
		case TelephonyManager.NETWORK_TYPE_EHRPD:
			return 3;
		case TelephonyManager.NETWORK_TYPE_HSUPA:
		case TelephonyManager.NETWORK_TYPE_HSPA:
		case TelephonyManager.NETWORK_TYPE_HSDPA:
		case TelephonyManager.NETWORK_TYPE_HSPAP:
			return 3;
			// return "3.5G";
		case TelephonyManager.NETWORK_TYPE_LTE:
			return 4;
		default:
			return 0;
		}
		// return "";
	}

	public String get_networkName() {
		String network_type = "";// maybe usb reverse tethering
		NetworkInfo active_network = ((ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();
		if (active_network != null && active_network.isConnectedOrConnecting()) {
			if (active_network.getType() == ConnectivityManager.TYPE_WIFI) {
				network_type = "WIFI";
			} else if (active_network.getType() == ConnectivityManager.TYPE_MOBILE) {
				network_type = "MOBILE";
			}
		}
		return network_type;
	}

	// 20121018 bug 167
	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public JSONObject ParseToJson(Cursor cor, String table) {
		JSONObject ob = new JSONObject();
		JSONArray arr = new JSONArray();
		try {
			if (cor != null && cor.getCount() > 0) {
				do {
					JSONObject json = new JSONObject();
					for (int i = 0; i < cor.getColumnCount(); i++) {
						String colname = cor.getColumnName(i);
						String value = cor.getString(i);

						if (colname.equals("patientstate")
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "00");
							}
						} else if (colname.equals(TR_CaseCount.ContactCount)
								&& table.equals(TR_CaseCount.TableName)) {
							// Not send ContactCount
						} else {
							if (value != null) {
								json.put(colname, value);
							} else {
								json.put(colname, "");
							}
						}
					}
					arr.put(json);
				} while (cor.moveToNext());
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			E022.saveErrorLog(e1.getMessage(), con);
		}
		try {
			ob.put(table, arr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return ob;
	}

	// Bug 116 Boolean -> int
	public int uploadDataToServer(String json) {
		// myadapter.clear();

		String encoded = "";
		try {
			encoded = URLEncoder.encode(json, "UTF-8");
			// String de= URLDecoder.decode(encoded, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
		}
		int result = 0;
		TrustManagerManipulator.allowAllSSL();
		String NAMESPACE = ContantSql.NameSpace;
		String METHOD_NAME = ContantSql.sendGPSInfo;
		String URL = ContantSystem.Endpoint;
		String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		request.addProperty("Json", encoded);
		// Param log 20130103
		CL_User cl = new CL_User(con);
		String user = cl.getUserName();
		String loginfo = getLogInfo(SOAP_ACTIONS, "", user, "");
		request.addProperty("smpLog", loginfo);
		//
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		// envelope.dotNet=true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidhttpTranport = new HttpTransportSE(URL,
				ContantSystem.TimeOut);

		try {
			androidhttpTranport.call(SOAP_ACTIONS, envelope);
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			result = 0;
		} catch (XmlPullParserException e3) {
			// TODO Auto-generated catch block
			result = 0;
		}
		Object responseBody = null;
		try {
			responseBody = envelope.getResponse();
			String t = responseBody.toString();
			if (t.equals("1")) {
				result = 1;
			} 
		} catch (SoapFault e2) {
			// TODO Auto-generated catch block
			result = 0;
		}

		return result;
	}

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), con);
		}
		return log;
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) con
//					.getApplicationContext().getSystemService(
//							Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Get Ip
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) con
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), con);
		}
		return "";
	}
}
