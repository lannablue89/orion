﻿package jp.osaka;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class EMaps extends MapActivity implements OnClickListener {
	MapView mapView;

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.layoutmaps);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		mapView = (MapView) findViewById(R.id.myMap);
		mapView.setBuiltInZoomControls(true);

		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();

		String Flat = b.getString("Flat");
		String Flongi = b.getString("Flongi");
		String Tlat = b.getString("Tlat");
		String Tlongi = b.getString("Tlongi");
		// double dist = b.getDouble("Distance");

		double src_lat = Double.parseDouble(Flat);
		double src_long = Double.parseDouble(Flongi);
		double dest_lat = Double.parseDouble(Tlat);
		double dest_long = Double.parseDouble(Tlongi);
		GeoPoint srcGeoPoint = new GeoPoint((int) (src_lat * 1E6),
				(int) (src_long * 1E6));
		GeoPoint destGeoPoint = new GeoPoint((int) (dest_lat * 1E6),
				(int) (dest_long * 1E6));

		DrawPath(srcGeoPoint, destGeoPoint, Color.BLUE, mapView);

		mapView.getController().animateTo(destGeoPoint);
		mapView.getController().setZoom(15);

		//Zoom map to center
		mapView.getController()
				.zoomToSpan(
						(srcGeoPoint.getLatitudeE6() > destGeoPoint.getLatitudeE6() ? srcGeoPoint.getLatitudeE6()
								- destGeoPoint.getLatitudeE6()
								: destGeoPoint.getLatitudeE6()
										- srcGeoPoint.getLatitudeE6()),
						(srcGeoPoint.getLongitudeE6() > destGeoPoint
								.getLongitudeE6() ? srcGeoPoint
								.getLongitudeE6()
								- destGeoPoint.getLongitudeE6() : destGeoPoint
								.getLongitudeE6()
								- srcGeoPoint.getLongitudeE6()));
		mapView.getController().animateTo(
				new GeoPoint(srcGeoPoint.getLatitudeE6()
						- ((srcGeoPoint.getLatitudeE6() - destGeoPoint
								.getLatitudeE6()) / 2), srcGeoPoint
						.getLongitudeE6()
						- ((srcGeoPoint.getLongitudeE6() - destGeoPoint
								.getLongitudeE6()) / 2)));
		//End
		Button btnStreet = (Button) findViewById(R.id.btnStreet);
		btnStreet.setOnClickListener(this);
		Button btnSate = (Button) findViewById(R.id.btnSatellite);
		btnSate.setOnClickListener(this);
		Button btnTraffic = (Button) findViewById(R.id.btnTraffic);
		btnTraffic.setOnClickListener(this);
		Button btnSearch = (Button) findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(this);
		Button btnView = (Button) findViewById(R.id.btnView);
		btnView.setOnClickListener(this);

	}

	private class MyItemizedOverlay extends ItemizedOverlay<OverlayItem> {

		private List<OverlayItem> mOverlays = new ArrayList<OverlayItem>();

		public MyItemizedOverlay(Drawable defaultMarker) {
			super(boundCenterBottom(defaultMarker));
		}

		public void addOverlayItem(int lat, int lon, String title,
				Drawable altMarker) {
			GeoPoint point = new GeoPoint(lat, lon);
			OverlayItem overlayItem = new OverlayItem(point, title, null);
			addOverlayItem(overlayItem, altMarker);
		}

		public void addOverlayItem(OverlayItem overlayItem) {
			mOverlays.add(overlayItem);
			populate();
		}

		public void addOverlayItem(OverlayItem overlayItem, Drawable altMarker) {
			overlayItem.setMarker(boundCenterBottom(altMarker));
			addOverlayItem(overlayItem);
		}

		@Override
		protected OverlayItem createItem(int i) {
			return mOverlays.get(i);
		}

		@Override
		public int size() {
			return mOverlays.size();
		}

		@Override
		protected boolean onTap(int index) {
			Toast.makeText(EMaps.this, getItem(index).getTitle(),
					Toast.LENGTH_LONG).show();
			return true;
		}

	}

	public JSONObject getRouter(String url) {
		JSONObject json = null;
		try {
			int TIMEOUT_MILLISEC = 10000; // = 10 seconds
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					TIMEOUT_MILLISEC);
			HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
			HttpClient httpclient = new DefaultHttpClient(httpParams);
			HttpPost httppost = new HttpPost(url);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			try {
				String responseBody = httpclient.execute(httppost,
						responseHandler);
				json = new JSONObject(responseBody);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return null;
			}

		} catch (Throwable t) {
			return null;
		}
		return json;
	}

	private void DrawPath(GeoPoint src, GeoPoint dest, int color,
			MapView mMapView01) {

		try {
			// connect to map web service
			String url = "http://maps.googleapis.com/maps/api/directions/json?";
			url += "origin="
					+ Double.toString((double) src.getLatitudeE6() / 1.0E6)
					+ ","
					+ Double.toString((double) src.getLongitudeE6() / 1.0E6);
			url += "&destination="
					+ Double.toString((double) dest.getLatitudeE6() / 1.0E6)
					+ ","
					+ Double.toString((double) dest.getLongitudeE6() / 1.0E6);
			url += "&sensor=false&language=ja";
			JSONObject json = getRouter(url);
			if (json != null) {
				String status = json.getString("status");
				if (status.equalsIgnoreCase("OK")) {

					JSONArray array = json.getJSONArray("routes");

					JSONObject json1 = array.getJSONObject(0);
					JSONArray arrlegs = json1.getJSONArray("legs");
					int leng = arrlegs.length();
					JSONObject json2 = arrlegs.getJSONObject(0);
					JSONObject duration = json2.getJSONObject("duration");
					JSONObject distance = json2.getJSONObject("distance");
					String dtext = distance.getString("text");
					String start_address = json2.getString("start_address");
					String end_address = json2.getString("end_address");
					JSONArray arrsteps = json2.getJSONArray("steps");
					// mMapView01.getOverlays().add(new MyOverLay(src, src, 1));

					Bitmap bm = BitmapFactory.decodeResource(getResources(),
							R.drawable.pointa);
					MyOverLay mo = new MyOverLay(src, src, 4);
					mo.setBitmap(bm);
					mo.setText(start_address);
					mMapView01.getOverlays().add(mo);
					GeoPoint gp1;
					GeoPoint gp2 = src;
					for (int i = 0; i < arrsteps.length(); i++) {

						JSONObject step = arrsteps.getJSONObject(i);
						JSONObject duration2 = step.getJSONObject("duration");
						JSONObject distance2 = step.getJSONObject("distance");
						JSONObject start_location2 = step
								.getJSONObject("start_location");
						JSONObject end_location2 = step
								.getJSONObject("end_location");
						String html_instructions = step
								.getString("html_instructions");
						JSONObject polyline = step.getJSONObject("polyline");
						String point = polyline.getString("points");
						List<GeoPoint> listpoint = new ArrayList<GeoPoint>();
						listpoint = decodePoly(point);
						String lat1 = start_location2.getString("lat");
						String lng1 = start_location2.getString("lng");
						String lat2 = end_location2.getString("lat");
						String lng2 = end_location2.getString("lng");
						if (listpoint.size() > 0) {
							gp1 = new GeoPoint(
									(int) (Double.parseDouble(lat1) * 1E6),
									(int) (Double.parseDouble(lng1) * 1E6));
							for (int t = 0; t < listpoint.size(); t++) {
								if (t == 0) {
									gp2 = listpoint.get(t);
								} else {
									gp1 = gp2;
									gp2 = listpoint.get(t);
								}
								mMapView01.getOverlays().add(
										new MyOverLay(gp1, gp2, 2, color));

							}
							gp1 = gp2;
							gp2 = new GeoPoint(
									(int) (Double.parseDouble(lat2) * 1E6),
									(int) (Double.parseDouble(lng2) * 1E6));
							mMapView01.getOverlays().add(
									new MyOverLay(gp1, gp2, 2, color));
						}

					}
					// mMapView01.getOverlays().add(new MyOverLay(dest, dest,
					// 3));
					bm = BitmapFactory.decodeResource(getResources(),
							R.drawable.pointb);
					mo = new MyOverLay(dest, dest, 4);
					mo.setBitmap(bm);
					mo.setText(end_address);
					mMapView01.getOverlays().add(mo);

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(EMaps.this, "Don't connect to google map",
					Toast.LENGTH_SHORT).show();
		}
	}

	private List<GeoPoint> decodePoly(String encoded) {

		List<GeoPoint> poly = new ArrayList<GeoPoint>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			GeoPoint p = new GeoPoint((int) (((double) lat / 1E5) * 1E6),
					(int) (((double) lng / 1E5) * 1E6));
			poly.add(p);
		}

		return poly;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnStreet:
			mapView.setStreetView(true);
			mapView.setSatellite(false);
			mapView.setTraffic(false);
			break;
		case R.id.btnSatellite:
			mapView.setSatellite(true);
			mapView.setStreetView(false);
			break;
		case R.id.btnTraffic:
			mapView.setTraffic(true);
			mapView.setStreetView(false);
			mapView.setSatellite(false);
			break;
		case R.id.btnView:
			finish();
			break;
		default:
			break;
		}

	}
}
