package jp.osaka;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.widget.Button;
import android.widget.LinearLayout;

public class Bell {
	Handler handler;
	Activity ac;
	int i;
	Button btnBell;
	int totalTime = 10000;
	int time = 0;
	LinearLayout lHeader;
	String headerColor;
	boolean bshowHeader = false;
	String HeaderBackcolorOnDisaster = "";
	String HeaderBackcolorOnEmergency = "";

	public void Rang(Activity context, boolean bDisaster) {
		try {

			i = 0;
			ac = context;
			handler = new Handler();
			btnBell = (Button) ac.findViewById(R.id.btnChuong);
			lHeader = (LinearLayout) ac.findViewById(R.id.bgHeader);
			getConfigTimeRun(bDisaster);
			bshowHeader = true;
			if (!irung) {
				if (bDisaster)
					RungDisaster();
				else
					RungNotification();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void RungDisaster() {
		irung = true;
		time = 0;
		Music.playDesaster(ac);
		startThreadRung();

	}

	private void getConfigTimeRun(boolean bDesaster) {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(ac);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (bDesaster) {
					totalTime = Integer.parseInt(cur.getString(cur
							.getColumnIndex("soundplaytimeondisaster")));
					headerColor = cur.getString(cur
							.getColumnIndex("HeaderBackcolorOnDisaster"));
				} else {
					totalTime = Integer.parseInt(cur.getString(cur
							.getColumnIndex("soundplaytimeonnotification")));
					headerColor = cur.getString(cur
							.getColumnIndex("HeaderBackcolorOnNotification"));

				}
				HeaderBackcolorOnDisaster = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnDisaster"));
				HeaderBackcolorOnEmergency = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnEmergency"));
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	public void RungNotification() {
		irung = true;
		time = 0;
		Music.playNotification(ac);
		startThreadRung();

	}

	public void StopRung() {
		irung = false;
	}

	public void StopHeader() {
		bshowHeader = false;
	}

	boolean irung = false;

	class myrunalbeBellRung implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			while (irung) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						time += 500;

						if (time > totalTime)
							irung = false;
						if (irung) {
							i++;
							if (i == 1) {
								btnBell.setBackgroundResource(R.drawable.chuong1);
								if (bshowHeader)
									lHeader.setBackgroundColor(Color
											.parseColor(headerColor));
							} else if (i == 2) {
								btnBell.setBackgroundResource(R.drawable.chuong2);
								if (bshowHeader)
									lHeader.setBackgroundResource(R.drawable.header);
							} else if (i == 3) {
								btnBell.setBackgroundResource(R.drawable.chuong3);
								if (bshowHeader)
									lHeader.setBackgroundColor(Color
											.parseColor(headerColor));
							} else {
								btnBell.setBackgroundResource(R.drawable.chuong4);
								if (bshowHeader)
									lHeader.setBackgroundResource(R.drawable.header);
							}
							if (i == 4)
								i = 0;
						} else {
							if (time > totalTime)
								btnBell.setBackgroundResource(R.drawable.chuongmiss);
							else
								btnBell.setBackgroundResource(R.drawable.chuong);
							if (bshowHeader) {
								SharedPreferences sh = ac.getSharedPreferences(
										"app", Context.MODE_PRIVATE);
								int mode = sh.getInt("Mode", 0);
								if (mode == 2) {
									lHeader.setBackgroundColor(Color
											.parseColor(HeaderBackcolorOnEmergency));
								} else if (mode == 1) {
									lHeader.setBackgroundColor(Color
											.parseColor(HeaderBackcolorOnDisaster));
								} else
									lHeader.setBackgroundResource(R.drawable.header);
							}
							Music.stop(ac);
						}
					}
				});
			}

		}
	};

	void startThreadRung() {
		myrunalbeBellRung able = new myrunalbeBellRung();
		Thread th = new Thread(able);
		th.start();

	}
}
