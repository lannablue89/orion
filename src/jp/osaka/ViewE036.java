package jp.osaka;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Checkable;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class ViewE036 {

	public static final int VERTICAL = 1;
	public static final int HORIZONTAL = 2;

	Context mContext;
	ModelE036 data;
	ExpandableListView expandableListView;
	LayoutInflater inflater;
	int mode;
	String title;

	public ViewE036(Context context, ModelE036 data) {
		mContext = context;
		this.data = data;
		inflater = LayoutInflater.from(mContext);
	}

	public View getView() {
		if (expandableListView == null) {
			// init view here
		}
		return expandableListView;
	}

	private class ExpanAdapter extends BaseExpandableListAdapter {

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return data;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			convertView = inflater.inflate(R.layout.inflater_e036, null);
			HashMap<String, Integer> subdata = data.getData();
			RadioButton radioButton1 = (RadioButton) convertView
					.findViewById(R.id.radioVer1);
			RadioButton radioButton2 = (RadioButton) convertView
					.findViewById(R.id.radioVer2);
			int value = data.getChoosenValue();
			for (String key : subdata.keySet()) {
				int type = subdata.get(key);
				if (type == -1) {
					((TextView) convertView.findViewById(R.id.subTitle))
							.setText(key);
					convertView.findViewById(R.id.subTitle).setVisibility(
							View.VISIBLE);
				}
				if (type == 0) {
					radioButton1.setText(key);
				} else {
					radioButton2.setText(key);
				}
			}
			if (value == 0) {
				radioButton1.setChecked(true);
				radioButton2.setChecked(false);
			} else {
				radioButton1.setChecked(false);
				radioButton2.setChecked(true);
			}
			LinearLayout layout = (LinearLayout) convertView
					.findViewById(R.id.lnVer);
			if (mode == VERTICAL) {
				layout.setOrientation(VERTICAL);
			} else {
				layout.setOrientation(HORIZONTAL);
			}

			radioButton1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					data.setChoosenValue(0);
					notifyDataSetChanged();
				}
			});
			radioButton2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					data.setChoosenValue(1);
					notifyDataSetChanged();
				}
			});
			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			
			return null;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return false;
		}

	}
}
