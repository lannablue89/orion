﻿package jp.osaka;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jp.osaka.ContantTable.BaseCount;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class E026 {
	public void ShowE026(Context context, String MICode_Select, String CaseCode) {
		Cursor cur = null;
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layoute026);
			dialog.setCanceledOnTouchOutside(false);
			Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();

				}
			});
			String timepre30="";
			Calendar c=Calendar.getInstance();
			c.add(Calendar.MINUTE,-30);
			timepre30=Utilities.getDateTimeNow(c);
			Cl_Adminssion cl = new Cl_Adminssion(context);
			cur = cl.getAdmissionofHoppital(MICode_Select, CaseCode,timepre30);
			if (cur != null && cur.getCount() > 0) {

				LinearLayout layoutAdmission = (LinearLayout) dialog
						.findViewById(R.id.layoutItem26);

				do {
					String Admission_CD = cur.getString(cur
							.getColumnIndex("Admission_CD"));
					String FireStationCode = cur.getString(cur
							.getColumnIndex("FireStationCode"));
					String CaseNo = cur.getString(cur.getColumnIndex("CaseNo"));
					String MICode = cur.getString(cur.getColumnIndex("MICode"));
					String TerminalNo = cur.getString(cur
							.getColumnIndex("TerminalNo"));
					if (Admission_CD.equals("00001")) {
						Cl_Adminssion cla = new Cl_Adminssion(context);
						boolean res = cla
								.checkTR_MedicalInstitutionAdmission00002(
										FireStationCode, CaseNo, MICode,
										TerminalNo);
						if (res)
							continue;
					}
					String Movement_DATETIME = cur.getString(cur
							.getColumnIndex("Movement_DATETIME"));
					String EMSUnitName = cur.getString(cur
							.getColumnIndex("EMSUnitName"));
					String FireStationName = cur.getString(cur
							.getColumnIndex("FireStationName"));
					String KamokuName01 = cur.getString(cur
							.getColumnIndex("KamokuName01"));
					String KamokuName02 = cur.getString(cur
							.getColumnIndex("KamokuName02"));
					String KamokuName03 = cur.getString(cur
							.getColumnIndex("KamokuName03"));

					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramrow.setMargins(0, 10, 0, 0);
					LinearLayout layrow = new LinearLayout(context);
					layrow.setOrientation(LinearLayout.HORIZONTAL);
					layrow.setLayoutParams(paramrow);
					layrow.setGravity(Gravity.CENTER | Gravity.LEFT);

					LinearLayout.LayoutParams paramrdi = new LinearLayout.LayoutParams(
							25, 25);
					TextView rdi = new TextView(context);
					rdi.setLayoutParams(paramrdi);
					rdi.setText("");
					rdi.setBackgroundResource(R.drawable.ic026);
					layrow.addView(rdi);

					LinearLayout.LayoutParams paramrtxt = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramrtxt.setMargins(10, 0, 0, 0);
					TextView txtHour = new TextView(context);
					txtHour.setLayoutParams(paramrtxt);
					String h = Movement_DATETIME.substring(8, 10) + ":"
							+ Movement_DATETIME.substring(10, 12);
					if (Admission_CD.equals("00001")) {
						h += " 搬送開始";
					} else {
						h += " 医師引継";
					}
					DateFormat formatter = new SimpleDateFormat(
							BaseCount.DateTimeFormat);
					Date date1 = (Date) formatter.parse(Movement_DATETIME);
					Date date2 = (Date) formatter.parse(Utilities
							.getDateTimeNow());
					long diff = date2.getTime() - date1.getTime();
					double time = diff * 1.0 / (1000 * 60);
					int mm = (int) Math.ceil(time);
					h += " " + String.valueOf(mm) + "分経過";
					txtHour.setText(h);
					txtHour.setTextSize(18);
					layrow.addView(txtHour);
					layoutAdmission.addView(layrow);

					LinearLayout.LayoutParams paramrtxt2 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramrtxt2.setMargins(35, 0, 0, 0);
					TextView txtName = new TextView(context);
					txtName.setLayoutParams(paramrtxt2);
					txtName.setText(EMSUnitName + FireStationName);
					txtName.setTextSize(18);
					layoutAdmission.addView(txtName);
					if (KamokuName01 != null) {
						TextView txtKamokuName01 = new TextView(context);
						txtKamokuName01.setLayoutParams(paramrtxt2);
						txtKamokuName01.setText(KamokuName01);
						txtKamokuName01.setTextSize(18);
						layoutAdmission.addView(txtKamokuName01);

					}
					if (KamokuName02 != null && !KamokuName02.equals("")) {
						TextView txtKamokuName02 = new TextView(context);
						txtKamokuName02.setLayoutParams(paramrtxt2);
						txtKamokuName02.setText(KamokuName02);
						txtKamokuName02.setTextSize(18);
						layoutAdmission.addView(txtKamokuName02);
					}
					if (KamokuName03 != null && !KamokuName03.equals("")) {
						TextView txtKamokuName03 = new TextView(context);
						txtKamokuName03.setLayoutParams(paramrtxt2);
						txtKamokuName03.setText(KamokuName03);
						txtKamokuName03.setTextSize(18);
						layoutAdmission.addView(txtKamokuName03);
					}

				} while (cur.moveToNext());
			}

			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(context, e.getMessage(), "E026", true);
		} finally {
			if (cur != null)
				cur.close();
		}
	}
}
