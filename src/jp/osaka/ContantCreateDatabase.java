﻿package jp.osaka;

public class ContantCreateDatabase {

	public static String Drop_M_User1 = "DROP TABLE IF EXISTS M_User;";
	public static String CREATE_M_User1 = "Create table M_User (Id integer  primary key autoincrement,UserName varchar(10) , Password varchar(50),Status varchar(1));";

	public static String Drop_MS_SecondaryHealthCareArea2 = "DROP TABLE IF EXISTS MS_SecondaryHealthCareArea;";
	public static String CREATE_MS_SecondaryHealthCareArea2 = "create table MS_SecondaryHealthCareArea  ( SHCACode varchar(5)  primary key ,SHCAName varchar(40),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14));";

	public static String Drop_MS_Area3 = "DROP TABLE IF EXISTS MS_Area;";
	public static String CREATE_MS_Area3 = "create table MS_Area  ( AreaCode varchar(5)  primary key ,AreaName varchar(40),SHCACode varchar(5),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14));";

	public static String Drop_MS_FireStation4 = "DROP TABLE IF EXISTS MS_FireStation;";
	public static String CREATE_MS_FireStation4 = "create table MS_FireStation  ( FireStationCode varchar(10)  primary key ,FireStationName varchar(100),SHCACode varchar(5),AreaCode varchar(5),ReportFireStationCode varchar(10),TerminalPassword varchar(50),Latitude varchar(20),Longitude varchar(20),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),JianNo_Initial varchar(50), FDMAUpload_Initial_1 varchar(6),FDMAUpload_Initial_2 varchar(6),FDMAUpload_Initial_3 varchar(6),FDMAUpload_Initial_4 varchar(6));";

	public static String Drop_MS_Instructor5 = "DROP TABLE IF EXISTS MS_Instructor;";
	public static String CREATE_MS_Instructor5 = "create table MS_Instructor  ( FireStationCode varchar(10),MICode varchar(10),MITelNo varchar(20),MIDetailName varchar(10),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),primary key(FireStationCode,MICode,MITelNo));";

	public static String Drop_MS_EMSUnit6 = "DROP TABLE IF EXISTS MS_EMSUnit;";// Password
																				// varchar(50)->Password
																				// varchar(8)
																				// 20121108
	public static String CREATE_MS_EMSUnit6 = "create table MS_EMSUnit  ( EMSUnitCode varchar(10) primary key,EMSUnitName  varchar(40),FireStationCode varchar(10),ReportEMSUnitCode varchar(4),Password varchar(8),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14));";

	public static String Drop_MS_EMSUnitTerminalManagement7 = "DROP TABLE IF EXISTS MS_EMSUnitTerminalManagement;";
	public static String CREATE_MS_EMSUnitTerminalManagement7 = "create table MS_EMSUnitTerminalManagement  ( TerminalNo integer primary key,MachineID varchar(50),EMSUnitCode varchar(10),TerminalTelNo varchar(20),Start_DATETIME varchar(14),End_DATETIME varchar(14),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),JianNo_Initial varchar(50),JianNo_InitialFLG varchar(1),LastSynchroUpdate_DATETIME varchar(14),PlaySoundOnDisaster integer,PlaySoundOnNotification integer,AmbulanceSpeed integer, DefaultPictureSizeWidth integer,DefaultPictureSizeHeight integer,DefaultVideQuality integer);";

	public static String Drop_MS_MedicalInstitution8 = "DROP TABLE IF EXISTS MS_MedicalInstitution;";
	public static String CREATE_MS_MedicalInstitution8 = "create table MS_MedicalInstitution  ( MICode varchar(10) primary key,Opening_DATE varchar(8),Closing_DATE varchar(8),StopFLG varchar(1),MIClass varchar(1),SHCACode varchar(5),AreaCode varchar(5),MIName_Kanji varchar(100),MIName_Kana varchar(100),PostNo varchar(10),Address_Kanji varchar(100),Address_Kana varchar(100),EMailAddress varchar(200),TelNo1 varchar(20),TelNo2 varchar(20),TransportationTelNo varchar(20),FaxNo varchar(20),ThirdCoordinateTelNo varchar(20),Latitude varchar(20),Longitude varchar(20),EmergencyMIFLG varchar(1),MIType varchar(1),EmergencyCenterFLG varchar(1),CalamityMIFLG varchar(1),CCUTelNo varchar(20),SCUTelNo varchar(20),HelicopterTelNo varchar(20),HotLineTelNo varchar(20),SortNo integer,Fdmamicode varchar(10),DeleteFLG varchar(1) ,Update_DATETIME varchar(14));";

	public static String Drop_MS_KamokuTelNo9 = "DROP TABLE IF EXISTS MS_KamokuTelNo;";
	public static String CREATE_MS_KamokuTelNo9 = "create table MS_KamokuTelNo  ( MICode varchar(10) ,KamokuCode varchar(2),OujyuTelNo varchar(20),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),primary key(MICode,KamokuCode));";

	public static String Drop_MS_Kamoku10 = "DROP TABLE IF EXISTS MS_Kamoku;";
	public static String CREATE_MS_Kamoku10 = "create table MS_Kamoku  ( KamokuCode varchar(2)  primary key ,KamokuName varchar(40),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14));";

	public static String Drop_MS_MedicalInstitutionClosed11 = "DROP TABLE IF EXISTS MS_MedicalInstitutionClosed;";
	public static String CREATE_MS_MedicalInstitutionClosed11 = "create table MS_MedicalInstitutionClosed  ( MICode varchar(10)  ,Closed_MMDD varchar(4),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),primary key(MICode,Closed_MMDD));";

	public static String Drop_MS_ViewingPermission12 = "DROP TABLE IF EXISTS MS_ViewingPermission;";
	public static String CREATE_MS_ViewingPermission12 = "create table MS_ViewingPermission  ( MICode varchar(10)  ,FireStationCode varchar(10),PermissionFLG varchar(1),SortNo integer ,DeleteFLG varchar(1) ,Update_DATETIME varchar(14),primary key(MICode,FireStationCode));";

	public static String Drop_MS_MovementTime13 = "DROP TABLE IF EXISTS MS_MovementTime;";
	public static String CREATE_MS_MovementTime13 = "create table MS_MovementTime  ( FireStationCode varchar(10)  ,MovementTimeCode_CD varchar(5),MovementTimeName varchar(20),TransferFLG varchar(1),ControlType varchar(1),SortNo integer ,Update_DATETIME varchar(14),primary key(FireStationCode,MovementTimeCode_CD));";

	public static String Drop_MS_FireStationStandard14 = "DROP TABLE IF EXISTS MS_FireStationStandard;";
	public static String CREATE_MS_FireStationStandard14 = "create table MS_FireStationStandard  ( StandardClass varchar(3)  ,StandardCode varchar(5),StandardBranchCode varchar(3),StandardBranchName varchar(60),ParentStandardClass varchar(3),ParentStandardCode varchar(5),ParentStandardBranchCode varchar(3),DiseaseFLG varchar(1),ExternalWoundFLG varchar(1),DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key(StandardClass ,StandardCode,StandardBranchCode));";

	public static String Drop_MS_FireStationStandardSort15 = "DROP TABLE IF EXISTS MS_FireStationStandardSort;";
	public static String CREATE_MS_FireStationStandardSort15 = "create table MS_FireStationStandardSort  ( StandardClass varchar(3)  ,StandardCode varchar(5),StandardBranchCode varchar(3),SHCACode varchar(5),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key(StandardClass ,StandardCode,StandardBranchCode,SHCACode));";

	public static String Drop_MS_SHCAFireStationStandard16 = "DROP TABLE IF EXISTS MS_SHCAFireStationStandard;";
	public static String CREATE_MS_SHCAFireStationStandard16 = "create table MS_SHCAFireStationStandard  ( StandardClass varchar(3)  ,StandardCode varchar(5),StandardBranchCode varchar(3),SHCACode varchar(5),CategoryCode varchar(2),DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key(StandardClass ,StandardCode,StandardBranchCode,SHCACode,CategoryCode));";

	public static String Drop_MS_Category17 = "DROP TABLE IF EXISTS MS_Category;";
	public static String CREATE_MS_Category17 = "create table MS_Category  ( CategoryCode varchar(2)  primary key ,CategoryName varchar(60),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14));";

	public static String Drop_MS_SHCACondition18 = "DROP TABLE IF EXISTS MS_SHCACondition;";
	public static String CREATE_MS_SHCACondition18 = "create table MS_SHCACondition  ( ConditionClass varchar(3) ,ConditionCode varchar(5),ConditionBranchCode varchar(3),SHCACode varchar(5),CategoryCode varchar(2),DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key(ConditionClass,ConditionCode,ConditionBranchCode,SHCACode,CategoryCode));";

	public static String Drop_MS_Condition19 = "DROP TABLE IF EXISTS MS_Condition;";
	public static String CREATE_MS_Condition19 = "create table MS_Condition  ( ConditionClass varchar(3) ,ConditionCode varchar(5),ConditionBranchCode varchar(3),ConditionBranchName varchar(60),ParentConditionClass varchar(3),ParentConditionCode varchar(5),ParentConditionBranchCode varchar(3),DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key(ConditionClass,ConditionCode,ConditionBranchCode));";

	public static String Drop_MS_ConditionSort20 = "DROP TABLE IF EXISTS MS_ConditionSort;";
	public static String CREATE_MS_ConditionSort20 = "create table MS_ConditionSort  ( ConditionClass varchar(3) ,ConditionCode varchar(5),ConditionBranchCode varchar(3),SHCACode varchar(5),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key( ConditionClass ,ConditionCode ,ConditionBranchCode ,SHCACode));";

	public static String Drop_MS_ConditionSupportTime21 = "DROP TABLE IF EXISTS MS_ConditionSupportTime;";
	public static String CREATE_MS_ConditionSupportTime21 = "create table MS_ConditionSupportTime  (MICode varchar(10), ConditionClass varchar(3) ,ConditionCode varchar(5),ConditionBranchCode varchar(3),DayGroupNo varchar(1),Day_CD varchar(5),TimeZoneNo varchar(1),SupportStart_TIME varchar(6),SupportEnd_TIME varchar(6),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key( MICode,ConditionClass	,ConditionCode,ConditionBranchCode,DayGroupNo,Day_CD,TimeZoneNo	));";

	public static String Drop_MS_Holiday22 = "DROP TABLE IF EXISTS MS_Holiday;";
	public static String CREATE_MS_Holiday22 = "create table MS_Holiday  (Holiday_DATE varchar(8)  primary key, HolidayName varchar(20) ,HolidayFLG varchar(1),Update_DATETIME varchar(14));";

	public static String Drop_MS_Code23 = "DROP TABLE IF EXISTS MS_Code;";
	public static String CREATE_MS_Code23 = "create table MS_Code  (CodeType varchar(5) , CodeID varchar(6) ,CodeName varchar(100),SortNo integer,Start_DATE varchar(8),End_DATE varchar(8),Update_DATETIME varchar(14),primary key( CodeType,CodeID));";

	public static String Drop_MS_Setting24 = "DROP TABLE IF EXISTS MS_Setting;";
	public static String CREATE_MS_Setting24 = "create table MS_Setting  ( Id integer  primary key autoincrement,MNET_URL varchar(200),MNETCount varchar(2) ,MNETTIME varchar(3) ,ThirdCoordinateCount varchar(2) ,ThirdCoordinateTime varchar(3) ,DBTime varchar(3) ,CaseCount varchar(2) ,PastCaseCount varchar(2) , ResultCount varchar(2),FTP_IP varchar(20) ,Update_DATETIME varchar(14),ScreenSensitivity varchar(4),soundplaytimeondisaster integer,soundplaytimeonnotification integer,TimerIntervalDisaster integer,TimerIntervalNotification integer,HeaderBackcolorOnDisaster varchar(7),HeaderBackcolorOnNotification varchar(7),TimeIntervalPush integer,HeaderBackcolorOnEmergency varchar(7) ,DisasterDBTime integer,TimeInrvalKahi integer,GPSDBTime integer , startmethodtimer varchar(14));";

	public static String Drop_MS_Message25 = "DROP TABLE IF EXISTS MS_Message;";
	public static String CREATE_MS_Message25 = "create table MS_Message  (MessageID varchar(6) primary key, MessageText varchar(50) ,DeleteFLG varchar(1),Update_DATETIME varchar(14));";
	// IncidentScene 20->100 : save address jian
	public static String Drop_TR_Case26 = "DROP TABLE IF EXISTS TR_Case;";
	public static String CREATE_TR_Case26 = "create table TR_Case  ( FireStationCode varchar(10) not null,CaseCode varchar(22) not null,JianNo varchar(38),EMSUnitCode varchar(10) ,IncidentScene varchar(100) ,Latitude_IS varchar(20) ,Longitude_IS varchar(20) null,Sex varchar(1) null,Age varchar(3) null, VitalSign_JCS varchar(5) null,VitalSign_GCS_E varchar(5)  ,VitalSign_GCS_V varchar(5) ,VitalSign_GCS_M varchar(5) ,VitalSign_Pulse varchar(3) ,VitalSign_Breath varchar(3) ,VitalSign_BloodPressure varchar(3) ,VitalSign_SpO2 varchar(4),VitalSign_Temperature  varchar(4),PatientLevel_CD varchar(5),Notice_CD varchar(5),Notice_TEXT varchar(200),NotTransportation_CD varchar(5),NotTransportation_TEXT varchar(200),SearchType_CD varchar(5),BedStatus_MALE varchar(1),BedStatus_FEMALE varchar(1),Status varchar(1),DeleteFLG varchar(1),Insert_DATETIME varchar(14),Update_DATETIME varchar(14),ContactCount varchar(5),FacilitiesCode varchar(10), FacilitiesName varchar (100),kin_no integer, kin_branch_no integer,kinLink_DateTime varchar(14),kin_kbn varchar(1),Outbound_No Integer,Status_H varchar(1),patientstate varchar(2),forwardmicord1 varchar(10), forwardminame1 varchar(100),forwardmidatetime1 varchar(14),forwardmicord2 varchar(10), forwardminame2 varchar(100),forwardmidatetime2 varchar(14),forwardmicord3 varchar(10), forwardminame3 varchar(100),forwardmidatetime3 varchar(14),nyuin_no varchar(8),nyuinlink_datetime varchar(14),sitearrival_datetime varchar(14),doctortakeover_datetime varchar(14),primary key( FireStationCode,CaseCode));";

	public static String Drop_TR_Patient27 = "DROP TABLE IF EXISTS TR_Patient;";
	public static String CREATE_TR_Patient27 = "create table TR_Patient  (FireStationCode varchar(10), CaseNo varchar(22) ,PatientBackground_CD varchar(5),PatientBackground_TEXT varchar(200),Update_DATETIME varchar(14),primary key( FireStationCode,CaseNo,PatientBackground_CD));";

	public static String Drop_TR_MovementTime28 = "DROP TABLE IF EXISTS TR_MovementTime;";
	public static String CREATE_TR_MovementTime28 = "create table TR_MovementTime  (FireStationCode varchar(10), CaseNo varchar(22) ,MovementCode varchar(5),Movement_DATETIME varchar(14),Update_DATETIME varchar(14),primary key( FireStationCode,CaseNo,MovementCode));";

	public static String Drop_TR_FireStationStandard29 = "DROP TABLE IF EXISTS TR_FireStationStandard;";
	public static String CREATE_TR_FireStationStandard29 = "create table TR_FireStationStandard  (FireStationCode varchar(10), CaseNo varchar(22) ,StandardClass varchar(3),StandardCode varchar(5),StandardBranchCode varchar(3),Update_DATETIME varchar(14),primary key( FireStationCode,CaseNo,StandardClass,StandardCode,StandardBranchCode));";

	public static String Drop_TR_MedicalInstitutionAdmission30 = "DROP TABLE IF EXISTS TR_MedicalInstitutionAdmission;";
	public static String CREATE_TR_MedicalInstitutionAdmission30 = "create table TR_MedicalInstitutionAdmission  (FireStationCode varchar(10), CaseNo varchar(22) ,MICode varchar(10),Admission_CD varchar(5),TerminalNo integer,Insert_DATETIME varchar(14),DeleteFLG varchar(1),Update_DATETIME varchar(14),EMSUnitCode varchar(10),KamokuName01 varchar(6),KamokuName02 varchar(60),KamokuName03 varchar(60),Movement_DATETIME varchar(14),primary key( FireStationCode,CaseNo,MICode,Admission_CD,TerminalNo));";

	public static String Drop_TR_SelectionKamoku31 = "DROP TABLE IF EXISTS TR_SelectionKamoku;";
	public static String CREATE_TR_SelectionKamoku31 = "create table TR_SelectionKamoku  (FireStationCode varchar(10), CaseNo varchar(22) ,KamokuCode varchar(2),KamokuName varchar(40),Update_DATETIME varchar(14),primary key( FireStationCode,CaseNo,KamokuCode));";

	public static String Drop_TR_Rotation32 = "DROP TABLE IF EXISTS TR_Rotation;";// table
																					// master
																					// (la)
	public static String CREATE_TR_Rotation32 = "create table TR_Rotation  (MICode varchar(10), KamokuCode varchar(2) ,Rotation_DATE varchar(8),RotationStart_DATETIME varchar(14),RotationEnd_DATETIME varchar(14),DeleteFLG  varchar(1),Update_DATETIME varchar(14),primary key( MICode,KamokuCode,Rotation_DATE,RotationStart_DATETIME));";

	public static String Drop_TR_Oujyu33 = "DROP TABLE IF EXISTS TR_Oujyu;";// table
																			// master
																			// (la)
	public static String CREATE_TR_Oujyu33 = "create table TR_Oujyu  (MICode varchar(10), KamokuCode varchar(2) ,Oujyu varchar(1),Bed_Male varchar(1),Bed_Female varchar(1),Operation varchar(1),Doctor varchar(100),Remarks varchar(200) ,DeleteFLG  varchar(1),Update_DATETIME varchar(14),primary key( MICode,KamokuCode));";

	public static String Drop_TR_ContactResultRecord34 = "DROP TABLE IF EXISTS TR_ContactResultRecord;";// table
																										// master
																										// (la)
	public static String CREATE_TR_ContactResultRecord34 = "create table TR_ContactResultRecord  (FireStationCode varchar(10),CaseNo varchar(22),ContactCount integer,MICode varchar(10), Contact_DATETIME varchar(14) ,ContactResult_CD varchar(5),ContactResult_TEXT varchar(200),Update_DATETIME varchar(14),InstructorUseFlg varchar(1),primary key( FireStationCode,CaseNo,ContactCount));";

	public static String Drop_TR_ReportingAll35 = "DROP TABLE IF EXISTS TR_ReportingAll;";// table
																							// master
																							// (la)
	public static String CREATE_TR_ReportingAll35 = "create table TR_ReportingAll  (Report_DATETIME varchar(14),EMSUnitCode varchar(10),ReportTitle varchar(50),ReportText varchar(200),Destination varchar(1),DeleteFLG varchar(1),Update_DATETIME varchar(14),isshow integer,primary key( Report_DATETIME,EMSUnitCode));";

	public static String Drop_TR_FirstAid36 = "DROP TABLE IF EXISTS TR_FirstAid;";// table
																					// master
																					// (la)
	public static String CREATE_TR_FirstAid36 = "create table TR_FirstAid  (FireStationCode varchar(10),CaseNo varchar(22),FirstAid_CD varchar(5),FirstAid_TEXT varchar(200),FirstAid_DATETIME varchar(14),Update_DATETIME varchar(14),primary key( FireStationCode,CaseNo,FirstAid_CD));";

	public static String Drop_TR_ErrorLog37 = "DROP TABLE IF EXISTS TR_ErrorLog;";// table
																					// master
																					// (la)
																					// //Bug
																					// 547
																					// 20130204
																					// change
																					// TerminalNo->TerminalTelNo
	public static String CREATE_TR_ErrorLog37 = "create table TR_ErrorLog  (TerminalTelNo varchar(50),SEQNo integer,ErrorLevel varchar(1),ErrorText varchar(200),Output_DATETIME varchar(14),primary key( TerminalTelNo,SEQNo));";

	public static String Drop_MS_LauncherItem38 = "DROP TABLE IF EXISTS MS_LauncherItem;";
	public static String CREATE_MS_LauncherItem38 = "create table MS_LauncherItem  (LauncherItemCode varchar(6) primary key, LauncherItemName varchar(30), LauncherItemFileName varchar(50), LauncherStateTransitionType varchar(1), DisplayModeType varchar(1), ScreenTransitionInfo varchar(1000),  SortNo integer,   DeleteFLG varchar(1),  Update_DATETIME varchar(14));";

	public static String Drop_MS_NightClinic39 = "DROP TABLE IF EXISTS MS_NightClinic;";
	public static String CREATE_MS_NightClinic39 = "create table MS_NightClinic  (MICode varchar(10) primary key,SHCACode varchar(5),AreaCode varchar(5),MIName_Kanji varchar(100),MIName_Kana varchar(100),PostNo varchar(10),Address_Kanji varchar(100),Address_Kana varchar(100),EMailAddress varchar(200),TelNo1 varchar(20),TelNo2 varchar(20),FaxNo varchar(20),Latitude varchar(20),Longitude  varchar(20),MIType varchar(1),KamokuName01 varchar(40),WeekdayOfficeHours01 varchar(40),SaturdayOfficeHours01 varchar(40),HolidayOfficeHours01 varchar(40),KamokuName02 varchar(40),WeekdayOfficeHours02 varchar(40),SaturdayOfficeHours02 varchar(40),HolidayOfficeHours02 varchar(40),KamokuName03 varchar(40),WeekdayOfficeHours03 varchar(40),SaturdayOfficeHours03 varchar(40),HolidayOfficeHours03 varchar(40),KamokuName04 varchar(40),WeekdayOfficeHours04 varchar(40),SaturdayOfficeHours04 varchar(40),HolidayOfficeHours04 varchar(40),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14),Memo varchar(2000));";

	public static String Drop_MS_FireStationOriginalMI40 = "DROP TABLE IF EXISTS MS_FireStationOriginalMI;";
	public static String CREATE_MS_FireStationOriginalMI40 = "create table MS_FireStationOriginalMI  (FireStationCode varchar(10),FireStationOriginalCode integer,MICode varchar(10),TransportationTelNo varchar(20),MIName_Kanji varchar(100),MIDetailName varchar(10),MIName_Kana varchar(100),PostNo varchar(10),Address_Kanji varchar(100),Address_Kana varchar(100),Latitude varchar(20),Longitude  varchar(20),FDMAMICode varchar(2),MIType varchar(1),PrefectureCode varchar(10),SortNo integer,DeleteFLG varchar(1),Update_DATETIME varchar(14),primary key( FireStationCode,FireStationOriginalCode));";

	public static String Drop_TR_File41 = "DROP TABLE IF EXISTS TR_FileManageInfo;";
	public static String CREATE_TR_File41 = "create table TR_FileManageInfo  (FireStationCode varchar(10),CaseNo varchar(22),MultimediaFileNo integer,FileRegistered_DateTime varchar(14),FileName varchar(100),ThumbnailFileName varchar(100),FileSize integer,FileType varchar(1),FileTime integer,FileSort integer,FileComment varchar(100) , SendFlag varchar(1),primary key( FireStationCode,CaseNo,MultimediaFileNo));";

	public static String Drop_MS_ModelSetting42 = "DROP TABLE IF EXISTS MS_ModelSetting;";
	public static String CREATE_MS_ModelSetting42 = "create table MS_ModelSetting  (Model_Name varchar(60),Location_Name varchar(200),Codec_Type varchar(1),File_Type varchar(1),Audio_source varchar(1), Audio_outputformat varchar(1), Audio_encorder varchar(1), Video_source varchar(1),Video_outputformat varchar(1),Video_encorder varchar(1),Video_framerate integer,Image_outputformat varchar(1),primary key( Model_Name));";

	public static String Drop_tr_kin_yousei43 = "DROP TABLE IF EXISTS QQ_tr_kin_yousei;";
	public static String CREATE_tr_kin_yousei43 = "create table QQ_tr_kin_yousei  (kin_no integer, kin_branch_no integer,  kin_kbn integer,  status_flg integer,isshow integer,Update_DateTime varchar(14),Insert_DateTime varchar(14), kin_date varchar(8), kin_time varchar(4), title varchar(200), cre_date varchar(14) , jusho_name varchar(400)  , qqsbt2 varchar(1) , qqsbt3 varchar(1), yousei_level varchar(2) , dmat_yousei_f varchar(1),  PRIMARY KEY (kin_no, kin_branch_no));";

	public static String Drop_QQ_tr_kin_yousei_kahi44 = "DROP TABLE IF EXISTS QQ_tr_kin_yousei_kahi;";
	public static String CREATE_QQ_tr_kin_yousei_kahi44 = "create table QQ_tr_kin_yousei_kahi  (kin_no integer, kin_branch_no integer,kikan_cd varchar(10) not null, ukeire_kahi varchar(8) , entrydate varchar(6)  , entrytime varchar(1)  , bikou varchar(400), tel_flg varchar(1)  , delivery_flg varchar(1), sick_h integer , sick_m integer, sick_l integer  , cre_kikan_cd varchar(10)  , upd_kikan_cd varchar(10), ap_upd_date varchar(7)  , data_upd_date varchar(7)  , cre_date varchar(7), upd_date varchar(7)  , primary key (kin_no,kin_branch_no,kikan_cd));";

	public static String Drop_TR_AllReqMedicalInstitution_kahi45 = "DROP TABLE IF EXISTS TR_AllReqMedicalInstitution_kahi;";
	public static String CREATE_TR_AllReqMedicalInstitution_kahi45 = "create table TR_AllReqMedicalInstitution_kahi  (FireStationCode varchar(10),CaseNo varchar(22),MICode varchar(10),ukeire_kahi varchar(1),Insert_Datetime varchar(14),EmergencyRecognition_kbn varchar(1), primary key (FireStationCode,CaseNo,MICode));";
	public static String Drop_TR_AllReqMedicalInstitution46 = "DROP TABLE IF EXISTS TR_AllReqMedicalInstitution;";
	public static String CREATE_TR_AllReqMedicalInstitution46 = "create table TR_AllReqMedicalInstitution  (FireStationCode varchar(10),CaseNo varchar(22),MICode varchar(10),EMSTransmission_Datetime varchar(14),kin_kbn varchar(1), primary key (FireStationCode,CaseNo,MICode));";
	
	public static String Drop_ms_linespeeddecision47 = "DROP TABLE IF EXISTS ms_linespeeddecision;";
	public static String CREATE_ms_linespeeddecision47 = "create table ms_linespeeddecision  (networkinfosubtype integer primary key, maxsizefile integer);";

	public static String Drop_TR_AllReqMedicalInstitution_Result48 = "DROP TABLE IF EXISTS TR_AllReqMedicalInstitution_Result;";
	public static String CREATE_TR_AllReqMedicalInstitution_Result48  = "create table TR_AllReqMedicalInstitution_Result  (FireStationCode varchar(10),CaseNo varchar(22),MICode varchar(10),EMSTransmission_Datetime varchar(14),kin_kbn varchar(1), primary key (FireStationCode,CaseNo,MICode,EMSTransmission_Datetime));";
	
	public static String Drop_tr_firestationstandard_oujyu49 = "DROP TABLE IF EXISTS tr_firestationstandard_oujyu;";
	public static String CREATE_tr_firestationstandard_oujyu49  = "create table tr_firestationstandard_oujyu  (micode varchar(10),standardclass varchar(3),standardcode varchar(5),standardbranchcode varchar(3),update_datetime varchar(14), primary key (micode, standardclass, standardcode, standardbranchcode));";
	
	public static String Drop_TR_TriagePATMethod = "DROP TABLE IF EXISTS TR_TriagePATMethod;";
	public static String CREATE_TR_TriagePATMethod  = "create table TR_TriagePATMethod  (FireStationCode varchar(10),CaseNo varchar(22),EMSInsert_Datetime varchar(14),walking varchar(1),consciousness varchar(1),respiratorycount1 varchar(5),respiratorycount2 varchar(5),spo2 varchar(3),pulse1 varchar(5),pulse2 varchar(5),systolicbloodpressure varchar(5),diastolicbloodpressure varchar(5),bodytemperature varchar(1),activebleeding varchar(1),estimateresult varchar(1), primary key (FireStationCode,CaseNo));";
	
	public static String Drop_TR_TriagePATMethod_Result = "DROP TABLE IF EXISTS TR_TriagePATMethod_Result;";
	public static String CREATE_TR_TriagePATMethod_Result  = "create table TR_TriagePATMethod_Result  (FireStationCode varchar(10),CaseNo varchar(22),EMSInsert_Datetime varchar(14),walking varchar(1),consciousness varchar(1),respiratorycount1 varchar(5),respiratorycount2 varchar(5),spo2 varchar(3),pulse1 varchar(5),pulse2 varchar(5),systolicbloodpressure varchar(5),diastolicbloodpressure varchar(5),bodytemperature varchar(1),activebleeding varchar(1),estimateresult varchar(1), primary key (FireStationCode,CaseNo,EMSInsert_Datetime));";
	
	public static String Drop_TR_TriageSTARTMethod = "DROP TABLE IF EXISTS TR_TriageSTARTMethod;";
	public static String CREATE_TR_TriageSTARTMethod  = "create table TR_TriageSTARTMethod  (FireStationCode varchar(10),CaseNo varchar(22),EMSInsert_Datetime varchar(14),walking varchar(1),respiratory varchar(1),respiratorycount varchar(1),circulatory varchar(1), response varchar(1), estimateresult varchar(1), primary key (FireStationCode,CaseNo));";
	
	public static String Drop_TR_TriageSTARTMethod_Result = "DROP TABLE IF EXISTS TR_TriageSTARTMethod_Result;";
	public static String CREATE_TR_TriageSTARTMethod_Result  = "create table TR_TriageSTARTMethod_Result  (FireStationCode varchar(10),CaseNo varchar(22),EMSInsert_Datetime varchar(14),walking varchar(1),respiratory varchar(1),respiratorycount varchar(1),circulatory varchar(1), response varchar(1), estimateresult varchar(1), primary key (FireStationCode,CaseNo,EMSInsert_Datetime));";
	
	public static String Drop_tr_emsunitlocationinfo = "DROP TABLE IF EXISTS tr_emsunitlocationinfo;";
	public static String CREATE_tr_emsunitlocationinfo  = "create table tr_emsunitlocationinfo  (emsunitcode varchar(10),gpsacquisition_datetime varchar(14),latitude_is varchar(20),longitude_is varchar(20),accuracy varchar(20),altitude varchar(20),speed varchar(20),incidentscene varchar(20),straightlinedistance varchar(20),arrivaltime varchar(14),networkinfosubtype integer,networkinfotypename varchar(100),update_datetime varchar(14), primary key (emsunitcode));";
	
	public static String Drop_tr_emsunitlocationinfo_result = "DROP TABLE IF EXISTS tr_emsunitlocationinfo_result;";
	public static String CREATE_tr_emsunitlocationinfo_result  = "create table tr_emsunitlocationinfo_result  (emsunitcode varchar(10),gpsacquisition_datetime varchar(14),latitude_is varchar(20),longitude_is varchar(20),accuracy varchar(20),altitude varchar(20),speed varchar(20),incidentscene varchar(20),straightlinedistance varchar(20),arrivaltime varchar(14),networkinfosubtype integer,networkinfotypename varchar(100),insert_datetime varchar(14), primary key (emsunitcode,gpsacquisition_datetime));";

	public static String Drop_qq_tr_kin_yousei_block = "DROP TABLE IF EXISTS qq_tr_kin_yousei_block;";
	public static String CREATE_qq_tr_kin_yousei_block  = "create table qq_tr_kin_yousei_block  (kin_no  integer,kin_branch_no integer,block_cd varchar(5),cre_kikan_cd varchar(10),upd_kikan_cd varchar(10),ap_upd_date varchar(14),data_upd_date varchar(14),cre_date varchar(14),upd_date varchar(14), primary key (kin_no, kin_branch_no, block_cd));";
	public static String Drop_qq_tr_kin_yousei_kamk = "DROP TABLE IF EXISTS qq_tr_kin_yousei_kamk;";
	public static String CREATE_qq_tr_kin_yousei_kamk  = "create table qq_tr_kin_yousei_kamk  (kin_no  integer,kin_branch_no integer,kamoku_cd varchar(5),cre_kikan_cd varchar(10),upd_kikan_cd varchar(10),ap_upd_date varchar(14),data_upd_date varchar(14),cre_date varchar(14),upd_date varchar(14), primary key (kin_no, kin_branch_no, kamoku_cd));";
	public static String Drop_qq_tr_kin_yousei_kikan = "DROP TABLE IF EXISTS qq_tr_kin_yousei_kikan;";
	public static String CREATE_qq_tr_kin_yousei_kikan  = "create table qq_tr_kin_yousei_kikan  (kin_no  integer,kin_branch_no integer,kikan_cd varchar(5),cre_kikan_cd varchar(10),upd_kikan_cd varchar(10),ap_upd_date varchar(14),data_upd_date varchar(14),cre_date varchar(14),upd_date varchar(14), primary key (kin_no, kin_branch_no, kikan_cd));";

	public static String Drop_tr_case_saisum = "DROP TABLE IF EXISTS tr_case_saisum;";
	public static String CREATE_tr_case_saisum  = "create table tr_case_saisum  (FireStationCode varchar(10),CaseNo varchar(22),emsunitcode varchar(10),deleteflg varchar(1),insert_datetime varchar(14),update_datetime varchar(14),status varchar(1),facilitiescode varchar(10),facilitiesname varchar(100),facilities_datetime varchar(14),status_h varchar(1),patientstate varchar(2),starttriageassessment varchar(1),starttriageassessmentdatetime varchar(14),pattriageassessment varchar(1),pattriageassessmentdatetime varchar(14),kin_no integer,kin_branch_no integer,kinlink_datetime varchar(14),kin_kbn varchar(1),lastmicord varchar(10),lastminame varchar(100), primary key (FireStationCode, CaseNo));";

	
}

