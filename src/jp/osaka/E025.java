﻿package jp.osaka;

import jp.osaka.ContantTable.BaseCount;

import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class E025 extends Activity implements OnTouchListener {
	CheckBox chkEnable;
	EditText txtJianNoInit;
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	static boolean check_first;
	String TerminalNo = "", JianNo_Initial;
	String JianNo_InitialFLG = "", AmbulanceSpeed, PlaySoundOnDisaster,
			PlaySoundOnNotification, DefaultPictureSizeWidth,
			DefaultPictureSizeHeight, DefaultVideQuality;
	adapterActivity listQuility;

	public static boolean isCheck_first() {
		return check_first;
	}

	public static void setCheck_first(boolean check_first) {
		E025.check_first = check_first;
	}

	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E025.this,
					progressBar, pro_2, "", "");
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	private void getInfoUser() {
		Cursor cur = null;
		try {
			CL_User cl = new CL_User(this);
			cur = cl.getUserJianoNo_InitFLG();
			if (cur != null && cur.getCount() > 0) {
				TerminalNo = cur.getString(cur.getColumnIndex("TerminalNo"));
				JianNo_InitialFLG = cur.getString(cur
						.getColumnIndex("JianNo_InitialFLG"));
				JianNo_Initial = cur.getString(cur
						.getColumnIndex("JianNo_Initial"));
				AmbulanceSpeed = cur.getString(cur
						.getColumnIndex("AmbulanceSpeed"));
				PlaySoundOnDisaster = cur.getString(cur
						.getColumnIndex("PlaySoundOnDisaster"));
				PlaySoundOnNotification = cur.getString(cur
						.getColumnIndex("PlaySoundOnNotification"));
				DefaultPictureSizeWidth = cur.getString(cur
						.getColumnIndex("DefaultPictureSizeWidth"));
				DefaultPictureSizeHeight = cur.getString(cur
						.getColumnIndex("DefaultPictureSizeHeight"));
				DefaultVideQuality = cur.getString(cur
						.getColumnIndex("DefaultVideQuality"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	EditText txtSpeed, txtWidth, txtHeight;
	CheckBox chkAllowDisaster, chkAllowNotification;
	Spinner cboQuility;
	ThreadAutoShowButton bt = new ThreadAutoShowButton();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute025);
		utl.setMenu_open(false);
		initSileMenu();
		//
		getInfoUser();
		//
		txtJianNoInit = (EditText) findViewById(R.id.txtJianNoInit);

		txtJianNoInit.setSingleLine(true);
		txtJianNoInit.setOnTouchListener(this);
		chkEnable = (CheckBox) findViewById(R.id.chkEnable);
		chkEnable.setOnTouchListener(this);
		chkEnable.setVisibility(View.VISIBLE);
		if (JianNo_InitialFLG != null && JianNo_InitialFLG.equals("1")) {
			chkEnable.setChecked(true);
		} else {
			chkEnable.setChecked(false);
		}

		if (JianNo_Initial == null || JianNo_Initial.equals("")) {
			CL_User cl1 = new CL_User(this);
			String FirestationCode = cl1.getFirestationCode();
			CL_User cl2 = new CL_User(this);
			JianNo_Initial = cl2.getFirestationJianoNo_Init(FirestationCode);
		}

		txtJianNoInit.setText(JianNo_Initial);
		chkEnable.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				ContentValues values = new ContentValues();
				if (isChecked)
					values.put("JianNo_InitialFLG", 1);
				else
					values.put("JianNo_InitialFLG", 0);
				CL_User cl = new CL_User(E025.this);
				cl.updateEMSUnitTerminalManagement(values);
			}
		});
		Button btnOk = (Button) findViewById(R.id.btnOK);
		btnOk.setOnTouchListener(this);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try {
					ContentValues values = new ContentValues();
					values.put("JianNo_Initial", txtJianNoInit.getText()
							.toString());
					if (chkEnable.isChecked())
						values.put("JianNo_InitialFLG", 1);
					else
						values.put("JianNo_InitialFLG", 0);
					if (chkAllowDisaster.isChecked())
						values.put("PlaySoundOnDisaster", 1);
					else
						values.put("PlaySoundOnDisaster", 0);
					if (chkAllowNotification.isChecked())
						values.put("PlaySoundOnNotification", 1);
					else
						values.put("PlaySoundOnNotification", 0);
					values.put("AmbulanceSpeed", txtSpeed.getText().toString());
					values.put("DefaultPictureSizeWidth", txtWidth.getText()
							.toString());
					values.put("DefaultPictureSizeHeight", txtHeight.getText()
							.toString());
					values.put("DefaultVideQuality", DefaultVideQuality);
					CL_User cl = new CL_User(E025.this);
					cl.updateEMSUnitTerminalManagement(values);
					// setCheck_first(true);
					E025Thead e25 = new E025Thead();
					JSONObject json = new JSONObject();
					json.put("TerminalNo", TerminalNo);
					json.put("JianNo_Initial", txtJianNoInit.getText()
							.toString());
					SharedPreferences sh = getSharedPreferences("app",
							MODE_PRIVATE);
					String lasttime = sh.getString(BaseCount.Update_DATETIME,
							"19000101010101");
					json.put("LastSynchroUpdate_DATETIME", lasttime);
					if (chkEnable.isChecked())
						json.put("JianNo_InitialFLG", "1");
					else
						json.put("JianNo_InitialFLG", "0");
					if (chkAllowDisaster.isChecked())
						json.put("PlaySoundOnDisaster", 1);
					else
						json.put("PlaySoundOnDisaster", 0);
					if (chkAllowNotification.isChecked())
						json.put("PlaySoundOnNotification", 1);
					else
						json.put("PlaySoundOnNotification", 0);
					json.put("AmbulanceSpeed", txtSpeed.getText().toString());
					json.put("DefaultPictureSizeWidth", txtWidth.getText()
							.toString());
					json.put("DefaultPictureSizeHeight", txtHeight.getText()
							.toString());
					json.put("DefaultVideQuality", DefaultVideQuality);
					e25.upLoadDataToServer(json.toString(), E025.this);
					finish();
				} catch (Exception e) {
					// TODO: handle exception
					E022.saveErrorLog(e.getMessage(), E025.this);
				}
			}
		});
		Button btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnTouchListener(this);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		listQuility = new adapterActivity(this, R.layout.layoutcomboxe12);
		txtSpeed = (EditText) findViewById(R.id.txtSpeed);
		txtSpeed.setText(AmbulanceSpeed);
		txtWidth = (EditText) findViewById(R.id.txtWidth);
		txtWidth.setText(DefaultPictureSizeWidth);
		txtHeight = (EditText) findViewById(R.id.txtHeight);
		txtHeight.setText(DefaultPictureSizeHeight);
		chkAllowDisaster = (CheckBox) findViewById(R.id.chkAllowDisasterSound);
		if (PlaySoundOnDisaster != null && PlaySoundOnDisaster.equals("1")) {
			chkAllowDisaster.setChecked(true);
		} else {
			chkAllowDisaster.setChecked(false);
		}

		chkAllowNotification = (CheckBox) findViewById(R.id.chkAllowNotificationSound);
		if (PlaySoundOnNotification != null
				&& PlaySoundOnNotification.equals("1")) {
			chkAllowNotification.setChecked(true);
		} else {
			chkAllowNotification.setChecked(false);
		}
		cboQuility = (Spinner) findViewById(R.id.cboQuility);
		cboQuility.setAdapter(listQuility);
		getListQuality();
		cboQuility.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				DTO_MCode dt = listQuility.getItem(arg2);
				DefaultVideQuality = dt.getCodeId();
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	String[] qId = { "4", "5", "6" };
	String[] qname = { "低", "中", "高" };

	void getListQuality() {
		int idxselect = -1;
		if (listQuility != null)
			listQuility.clear();
		for (int i = 0; i < 3; i++) {
			DTO_MCode dt = new DTO_MCode();
			dt.setCodeId(qId[i]);
			dt.setCodeName(qname[i]);
			listQuility.add(dt);
			if (dt.getCodeId().equals(DefaultVideQuality)) {
				idxselect = i;
			}
		}
		if (idxselect < 0) {
			idxselect = 0;
			DefaultVideQuality = listQuility.getItem(0).getCodeId();
		}
		cboQuility.setSelection(idxselect);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
			} else {
				Intent intent = new Intent(E025.this, E002.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
			return true; // always eat it!
		}
		return super.onKeyDown(keyCode, event);
	}

	public class adapterActivity extends ArrayAdapter<DTO_MCode> {

		public adapterActivity(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		// DaiTran Note: for combobox
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, true);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getViewChung(position, convertView, parent, false);
		}

		public View getViewChung(int position, View convertView,
				ViewGroup parent, boolean drop) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			View v = convertView;
			ViewWraper mwp;

			if (v == null) {
				LayoutInflater l = getLayoutInflater();
				v = l.inflate(R.layout.layoutcomboxe12, null);
				mwp = new ViewWraper(v);
				v.setTag(mwp);
			} else {

				mwp = (ViewWraper) convertView.getTag();
			}
			// ImageView img = (ImageView) findViewById(R.id.imageView1);
			TextView txt = mwp.getLable();
			DTO_MCode dt = new DTO_MCode();
			dt = this.getItem(position);
			txt.setText("   " + dt.getCodeName());
			LinearLayout layout = mwp.getLayoutrow();
			if (drop) {
				layout.setBackgroundResource(R.drawable.bgrowcboboxe12);
			} else {
				layout.setBackgroundResource(R.drawable.dropdowne12);
			}

			return v;
		}
	}

	class ViewWraper {

		View base;
		TextView lable1 = null;
		LinearLayout layout = null;

		ViewWraper(View base) {

			this.base = base;
		}

		TextView getLable() {
			if (lable1 == null) {
				lable1 = (TextView) base.findViewById(R.id.txtcboE12);
			}
			return lable1;
		}

		LinearLayout getLayoutrow() {
			if (layout == null) {
				layout = (LinearLayout) base.findViewById(R.id.lrCombox);
			}
			return layout;
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		bt.setRunning(false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		bt.showButtonAuto(this, "", "", false);
		E034.SetActivity(this);
	}

}
