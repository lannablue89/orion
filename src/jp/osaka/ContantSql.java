﻿package jp.osaka;

public class ContantSql {

	public static String NameSpace = "http://osaka.jp";
	public static String getDataAllTable = "getDataAllTable";
	public static String getTR_kin_yousei = "getTR_kin_yousei";
	public static String getTR_ReportingAll = "getTR_ReportingAll";
	public static String getDataTable9 = "getDataTable9";
	public static String getDataMSConditionSupportTime1 = "getDataMSConditionSupportTime1";
	public static String getDataMSConditionSupportTime2 = "getDataMSConditionSupportTime2";
	public static String getDataMSConditionSupportTime3 = "getDataMSConditionSupportTime3";
	public static String getDataThreeTable = "getDataThreeTable";
	public static String getDataMSMedicalInstitutionClosed = "getDataMSMedicalInstitutionClosed";
	public static String getDataMSViewingPermission = "getDataMSViewingPermission";
	public static String getDataTRRotation = "getDataTRRotation";
	public static String getDataAllTableWithDateTime = "getDataAllTableWithDateTime";
	public static String authenticate = "authenticate";
	public static String authenticateEMSUnit = "authenticateEMSUnit";
	public static String checkPhoneNumber = "checkPhoneNumber";
	public static String updateTable = "updateTable";
	public static String updateEMSUnitManagement = "updateEMSUnitManagement";
	public static String insertToTRReportingAll = "insertToTRReportingAll";
	public static String updateTRTable = "updateTRTable";
	public static String sendTRTable = "sendTRTable";
	public static String selectMICode0001 = "selectMICode0001";
	public static String selectDoctor = "selectDoctor";
	public static String sendGPSInfo = "sendGPSInfo";	
	public static String Select_HopitalInfo = "  Select distinct e.* "
			+ " ,c.ConditionCode ,c.ConditionBranchCode,c.ConditionClass,h.ConditionBranchName,d.Day_CD,d.SupportStart_TIME,d.SupportEnd_TIME,d.DayGroupNo,d.TimeZoneNo "
			+ "   From    TR_FireStationStandard a "
			+ "	Inner Join   MS_SHCAFireStationStandard b "
			+ "	On (a.StandardClass=b.StandardClass and a.StandardCode=b.StandardCode "
			+ "and a.StandardBranchCode=b.StandardBranchCode) "
			+ "	Inner Join MS_SHCACondition c On b.CategoryCode=c.CategoryCode "
			+ "	Inner Join MS_Condition h On (h.ConditionClass=c.ConditionClass "
			+ "         and h.ConditionCode=c.ConditionCode "
			+ "         and h.ConditionBranchCode=c.ConditionBranchCode) "
			+ "	Inner Join MS_ConditionSupportTime d "
			+ "        On (c.ConditionClass=d.ConditionClass "
			+ "         and c.ConditionCode=d.ConditionCode "
			+ "         and c.ConditionBranchCode=d.ConditionBranchCode) "
			+ "	Inner Join MS_MedicalInstitution e On d.MICode=e.MICode "
			// + "	Inner Join   MS_ViewingPermission g On e.MICode=g.MICode "
			+ "	left Join   MS_ViewingPermission g On e.MICode=g.MICode " // b.SHCACode
																			// old
																			// conditon
			+ "Where a.FireStationCode=? and a. CaseNo=? and b.SHCACode=? and (e.SHCACode=? or (e.SHCACode !=? and g.DeleteFLG =0 and g.PermissionFLG=1 and g.FireStationCode=?))"
			+ "and b.DeleteFLG=0 and c.DeleteFLG=0 and d.DeleteFLG=0  "
			// +
			// "and ((d.Day_CD=? and ( (ifnull(d.SupportStart_TIME,'000000')<=? and ifnull(d.SupportEnd_TIME,'999999')>=?) or d.SupportStart_TIME=d.SupportEnd_TIME) ) or d.Day_CD=? ) "
			+ "and (d.Day_CD=?  or d.Day_CD=? ) "
			+ "and e.DeleteFLG=0 "
			+ "and ifnull(e.Opening_DATE,'00010101')<=? and ifnull(e.Closing_DATE,'99999999')>=? "
			+ "and e.StopFLG=4 "
			// +
			// "and g.DeleteFLG =0 and g.PermissionFLG=1 and g.FireStationCode=? "
			+ "and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f where f.Closed_MMDD=? and f.DeleteFLG=0)";
	public static String Select_Hopital13C = "Select distinct e.* From MS_MedicalInstitution e"
			// + " MS_ConditionSupportTime d "
			// + " Inner Join MS_MedicalInstitution e On d.MICode=e.MICode "
			// + " Inner Join   MS_ViewingPermission g On e.MICode=g.MICode "
			+ "  Where "
			// " d.DeleteFLG=0 "
			// +
			// "and ((d.Day_CD=? and ( (ifnull(d.SupportStart_TIME,'000000')<=? and ifnull(d.SupportEnd_TIME,'999999')>=?) or d.SupportStart_TIME=d.SupportEnd_TIME) ) or d.Day_CD=? ) "
			+ "  e.DeleteFLG=0 "
			+ "  and ifnull(e.Opening_DATE,'00010101')<=? "
			+ "  and ifnull(e.Closing_DATE,'99999999')>=? "
			+ "  and e.StopFLG=4 " + "  and (e.MIType=3 or e.MIType=5)   "
			// + "   and g.DeleteFLG =0 "
			// + "  and g.PermissionFLG=1  " +// Remove PermissionFLG
			// +"   and g.FireStationCode=? "
			+ "  and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f   where f.Closed_MMDD=? and f.DeleteFLG=0)";
	public static String Select_Hopital13B = "Select distinct e.*,a.KamokuCode,m.KamokuName,c.Oujyu,c.Operation "
			+ "From MS_Kamoku m Inner Join  TR_SelectionKamoku a On m.KamokuCode=a.KamokuCode "
			+ "Inner Join TR_Oujyu c On a.KamokuCode=c.KamokuCode "
			+ "Inner Join MS_MedicalInstitution e On e.MICode=c.MICode "
			// + "Inner Join MS_ConditionSupportTime d   On d.MICode=e.MICode "
			// + "Inner Join   MS_ViewingPermission g On e.MICode=g.MICode "
			// + " Left Join   MS_ViewingPermission g On e.MICode=g.MICode "
			+ " Where  a.FireStationCode=? and a.CaseNo=? "
			+ " and c.DeleteFLG=0 "
			+ " and c.Oujyu=1" // Bug 277 20121102
			// + " and  d.DeleteFLG=0 "
			// +
			// "and ((d.Day_CD=? and ( (ifnull(d.SupportStart_TIME,'000000')<=? and ifnull(d.SupportEnd_TIME,'999999')>=?) or d.SupportStart_TIME=d.SupportEnd_TIME) ) or d.Day_CD=? ) "
			+ " and e.DeleteFLG=0 "
			+ " and ifnull(e.Opening_DATE,'00010101')<=? "
			+ " and ifnull(e.Closing_DATE,'99999999')>=? "
			+ " and e.StopFLG=4 "
			// + " and g.DeleteFLG =0 "
			// + " and g.PermissionFLG=1   and g.FireStationCode=? "
			+ " and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f "
			+ " where f.Closed_MMDD=? and f.DeleteFLG=0)";
	public static String Select_Hopital13B2 = "Select distinct e.*,a.KamokuCode,m.KamokuName "
			+ "From MS_Kamoku m Inner Join  TR_SelectionKamoku a On m.KamokuCode=a.KamokuCode "
			+ "Inner Join TR_Rotation c On a.KamokuCode=c.KamokuCode "
			+ "Inner Join MS_MedicalInstitution e On e.MICode=c.MICode "
			// + "Inner Join MS_ConditionSupportTime d   On d.MICode=e.MICode "
			// + "Inner Join   MS_ViewingPermission g On e.MICode=g.MICode "
			+ " Where  a.FireStationCode=? and a.CaseNo=? "
			+ " and c.DeleteFLG=0 "
			+ " and ifnull(c.RotationStart_DATETIME,'00010101000000')<=? "
			+ " and ifnull(c.RotationEnd_DATETIME,'99999999999999')>=? "
			// + " and  d.DeleteFLG=0 "
			// +
			// "and ((d.Day_CD=? and ( (ifnull(d.SupportStart_TIME,'000000')<=? and ifnull(d.SupportEnd_TIME,'999999')>=?) or d.SupportStart_TIME=d.SupportEnd_TIME) ) or d.Day_CD=? ) "
			+ " and e.DeleteFLG=0 "
			+ " and ifnull(e.Opening_DATE,'00010101')<=? "
			+ " and ifnull(e.Closing_DATE,'99999999')>=? "
			+ " and e.StopFLG=4 "
			// + " and g.DeleteFLG =0 "
			// + " and g.PermissionFLG=1   and g.FireStationCode=? "
			+ " and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f "
			+ " where f.Closed_MMDD=? and f.DeleteFLG=0)";
	public static String Select_Hopital13B_SearchALL = "Select distinct e.*,m.KamokuCode,m.KamokuName,c.Oujyu,c.Operation "
			+ "From MS_Kamoku m "
			+ "Inner Join TR_Oujyu c On m.KamokuCode=c.KamokuCode "
			+ "Inner Join MS_MedicalInstitution e On e.MICode=c.MICode "
			// + " Where  a.FireStationCode=? and a.CaseNo=? "
			+ " Where c.DeleteFLG=0 "
			+ " and c.Oujyu=1"
			+ " and e.DeleteFLG=0 "
			+ " and ifnull(e.Opening_DATE,'00010101')<=? "
			+ " and ifnull(e.Closing_DATE,'99999999')>=? "
			+ " and e.StopFLG=4 "
			+ " and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f "
			+ " where f.Closed_MMDD=? and f.DeleteFLG=0)";
	public static String Select_Hopital13B2_SearchALL = "Select distinct e.*,m.KamokuCode,m.KamokuName "
			+ "From MS_Kamoku m  "
			+ "Inner Join TR_Rotation c On m.KamokuCode=c.KamokuCode "
			+ "Inner Join MS_MedicalInstitution e On e.MICode=c.MICode "
			// + " Where  a.FireStationCode=? and a.CaseNo=? "
			+ " Where c.DeleteFLG=0 "
			+ " and ifnull(c.RotationStart_DATETIME,'00010101000000')<=? "
			+ " and ifnull(c.RotationEnd_DATETIME,'99999999999999')>=? "
			+ " and e.DeleteFLG=0 "
			+ " and ifnull(e.Opening_DATE,'00010101')<=? "
			+ " and ifnull(e.Closing_DATE,'99999999')>=? "
			+ " and e.StopFLG=4 "
			+ " and e.MICode not in (Select f.MICode from MS_MedicalInstitutionClosed f "
			+ " where f.Closed_MMDD=? and f.DeleteFLG=0)";

}
