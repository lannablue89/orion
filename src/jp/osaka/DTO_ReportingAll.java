﻿package jp.osaka;

public class DTO_ReportingAll {

	private String Report_DATETIME;
	private String EMSUnitCode;
	private String ReportTitle;
	private String ReportText;
	private String EMSUnitName;
	public String getReport_DATETIME() {
		return Report_DATETIME;
	}
	public void setReport_DATETIME(String report_DATETIME) {
		Report_DATETIME = report_DATETIME;
	}
	public String getEMSUnitCode() {
		return EMSUnitCode;
	}
	public void setEMSUnitCode(String eMSUnitCode) {
		EMSUnitCode = eMSUnitCode;
	}
	public String getReportTitle() {
		return ReportTitle;
	}
	public void setReportTitle(String reportTitle) {
		ReportTitle = reportTitle;
	}
	public String getReportText() {
		return ReportText;
	}
	public void setReportText(String reportText) {
		ReportText = reportText;
	}
	public String getEMSUnitName() {
		return EMSUnitName;
	}
	public void setEMSUnitName(String eMSUnitName) {
		EMSUnitName = eMSUnitName;
	}

	
}
