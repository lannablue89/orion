package jp.osaka;

import java.io.IOException;

import java.net.URLEncoder;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.Utilities.TrustManagerManipulator;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;

public class ThreadCheckDisaster {
	Context con;
	boolean isruning = true;
	long DisasterDBTime;
	String loginfo = "";

	public void checkDisaster(Context c) {
		isruning = true;
		con = c;
		CL_User us = new CL_User(con);
		String user = us.getUserName();
		String METHOD_NAME = ContantSql.getTR_kin_yousei;
		String URL = ContantSystem.Endpoint;
		String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
		loginfo = getLogInfo(SOAP_ACTIONS, "", user, "");
		CL_MSSetting cl = new CL_MSSetting(con);
		DisasterDBTime = cl.getDisasterDBTime();
		startThread();
	}

	public void StopCheck() {
		isruning = false;
	}

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) con
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (ServiceUpdateData.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {

			// if (isruning)//20121018 check send tr_mA
			while (isruning) {
				try {
					Thread.sleep(DisasterDBTime);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (isOnline()) {
					try {
						// TODO Auto-generated method stub
						TrustManagerManipulator.allowAllSSL();
						String NAMESPACE = ContantSql.NameSpace;
						String METHOD_NAME = ContantSql.getTR_kin_yousei;
						String URL = ContantSystem.Endpoint;
						String SOAP_ACTIONS = URL + "/" + METHOD_NAME;
						SoapObject request = new SoapObject(NAMESPACE,
								METHOD_NAME);
						SharedPreferences sh = con.getSharedPreferences("app",
								con.MODE_PRIVATE);
						String updatedatetime = sh.getString(
								BaseCount.Update_DATETIME, "19990101010101");
						request.addProperty("dateTime", sh.getString(
								"Update_DateTime_Yousei", updatedatetime));

						request.addProperty("smpLog", loginfo);
						//
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						// envelope.dotNet=true;
						envelope.setOutputSoapObject(request);
						HttpTransportSE androidhttpTranport = new HttpTransportSE(
								URL);

						try {
							androidhttpTranport.call(SOAP_ACTIONS, envelope);
						} catch (IOException e3) {
							// TODO Auto-generated catch block
							// Log.i("UploadServer",
							// "Upload is not successfull.");
							E022.saveErrorLog(e3.getMessage(), con);
						} catch (XmlPullParserException e3) {
							// TODO Auto-generated catch block
							// Log.i("UploadServer",
							// "Upload is not successfull.");
							E022.saveErrorLog(e3.getMessage(), con);
						}
						Object responseBody = null;
						try {
							responseBody = envelope.getResponse();
							if (responseBody != null) {
								// Check Result
								JSONObject json = new JSONObject(
										responseBody.toString());
								if (isMyServiceRunning() == false) {
									CL_TR_kin_yose tr = new CL_TR_kin_yose(con);
									String res = tr.updateDisaster(json);
									if (res.equals("") == false) {
										Editor e = sh.edit();
										e.putString("Update_DateTime_Yousei",
												res);
										e.commit();
									}
								}
							}
						} catch (SoapFault e2) {
							// TODO Auto-generated catch block
							// Log.i("UploadServer",
							// "Upload is not successfull.");
							E022.saveErrorLog(e2.getMessage(), con);
						}
					} catch (Exception e) {
						// TODO: handle exception
						// Log.i("UploadServer", e.getMessage());
						E022.saveErrorLog(e.getMessage(), con);
					}
				}
			}
		}
	};

	// Get Log info for server 20130103
	private String getLogInfo(String url, String buttonName, String user,
			String screenname) {
		String log = "";
		try {
			// $userid, $session_id , $computername, $ie_version, $ip_address ,
			// $phoneNumber, $target_page_url, $source_page_url,
			// $source_page_name, $button_name
			JSONObject json = new JSONObject();
			json.put("userid", user);
			json.put("session_id", "");
			String version = Build.MODEL + " Android " + Build.VERSION.RELEASE;
			json.put("computername", version);
			json.put("ie_version", "");
			json.put("ip_address", getLocalIpAddress());
			json.put("phoneNumber", getPhoneNumber());
			json.put("target_page_url", url);
			json.put("source_page_url", url);
			json.put("source_page_name", screenname);
			json.put("button_name", buttonName);
			log = json.toString();
			log = URLEncoder.encode(log, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			E022.saveErrorLog(e.getMessage(), con);
		}
		return log;
	}

	// get phone
	private String getPhoneNumber() {
//		try {
//			TelephonyManager info = (TelephonyManager) con
//					.getApplicationContext().getSystemService(
//							Context.TELEPHONY_SERVICE);
//			String phoneNumber = info.getLine1Number();
//			return phoneNumber;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
		return ContantSystem.HARD_PHONE_NO;
	}

	// Get Ip
	public String getLocalIpAddress() {

		try {
			WifiManager wifiManager = (WifiManager) con
					.getSystemService(con.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ip = wifiInfo.getIpAddress();
			String ipString = String.format("%d.%d.%d.%d", (ip & 0xff),
					(ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));

			return ipString;
		} catch (Exception e) {
			// } catch (SocketException e) {
			E022.saveErrorLog(e.getMessage(), con);
		}
		return "";
	}

	public void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}
