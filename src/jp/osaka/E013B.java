﻿package jp.osaka;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

public class E013B extends Activity implements LocationListener,
		OnTouchListener {
	ScrollView layoutmain;
	LinearLayout layoutItem;
	ArrayList<DTO_MedicalInstitution> listItem;
	// MyAdapter mydapter;
	String FireStationCode;
	String CaseCode;
	String SHCACode;
	String rdoSearch = "1";
	String hopitalname;
	String SHCASearch;
	String AreaCode;
	String KamokuName = "";
	double lat = 0;
	double longi = 0;
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";
	Location locationA;
	LocationManager locationManager;
	String tower;
	ProgressDialog p;
	HashMap<String, String> listday;
	TextView btnNextRecord;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	int numberitem = 0;
	int limit = 10;
	int lastPostionX = 0;
	int lastPostionY = 0;
	boolean refesh = true;// Q065
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);
			Bundle b = msg.getData();
			String key = b.getString("Key");

			DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
			dt = (DTO_MedicalInstitution) b.getSerializable("DTO");
			if (dt != null)
				listItem.add(dt);
			if (key.equals("1")) {
				if (rdoSearch.equals("1")) {
					E013B.this.sortAdapter();
				}
				layoutItem.removeAllViews();
				E013B.this.loadControl();
				exitProccess();
				if (listItem.size() < 1) {
					E022.showErrorDialog(E013B.this, ContantMessages.NotData,
							ContantMessages.NotData, false);
				}
			}
		}

	};

	void exitProccess() {
		p.dismiss();
		if (lastPostionX != 0 && lastPostionY != 0) {
			layoutmain.scrollTo(lastPostionX, lastPostionY);
		}
	}

	void showProccess() {

		p = new ProgressDialog(this);
		CL_Message m = new CL_Message(E013B.this);
		String title = m.getMessage(ContantMessages.PleaseWait);
		p.setTitle(title);
		m = new CL_Message(E013B.this);
		String mg = m.getMessage(ContantMessages.DataLoading);
		p.setMessage(mg);
		// 20121127 bug 393
		// p.setCancelable(false);
		p.setCanceledOnTouchOutside(false);
		//
		p.show();
	}

	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();
	boolean check = false;
	String HeaderBackcolorOnDisaster, HeaderBackcolorOnEmergency;
	HashMap<String, String> listKahi;
	HashMap<String, String> listAllReq;
	HashMap<String, String> listAllReqTarget;
	ArrayList<DTO_ObjectControl> listStatus;
	int TimeInrvalKahi = 30000;

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		check = utl.eventOnTouch(event, animateFromX, animateToX, marginX,
				menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E013B.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	Handler handler2;
	boolean isCheck = true;

	class myrunalbeStatus implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			while (isCheck) {
				try {

					Thread.sleep(TimeInrvalKahi);
					loadKahi();
					handler2.post(new Runnable() {

						public void run() {
							// TODO Auto-generated method stub
							for (int i = 0; i < listStatus.size(); i++) {
								if (!isCheck) {
									break;
								}
								DTO_ObjectControl dt = listStatus.get(i);
								TextView txtStatus = (TextView) findViewById(dt.Id);
								if (txtStatus != null)
									if (listKahi.containsKey(dt.MICode)) {
										txtStatus.setVisibility(View.VISIBLE);
									} else {
										txtStatus.setVisibility(View.INVISIBLE);
									}
							}
						}
					});
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	};

	void startThreadStatus() {
		myrunalbeStatus able = new myrunalbeStatus();
		Thread th = new Thread(able);
		th.start();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute013a);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		layoutItem = (LinearLayout) findViewById(R.id.layoutItem13a);
		listItem = new ArrayList<DTO_MedicalInstitution>();
		Cursor cor = null;
		Cursor cur = null;
		Cursor cur2 = null;
		refesh = false;// Q065
		numberitem = 0;
		lastPostionX = 0;
		lastPostionY = 0;
		try {
			// bug 393 20121128
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			String e = b.getString("E");
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			SHCACode = b.getString("SHCACode");
			KamokuName = b.getString("KamokuName");
			if (e.equals("E012")) {
				rdoSearch = b.getString("rdoSearch");
				hopitalname = b.getString("hopitalname");
				SHCASearch = b.getString("SHCASearch");
				AreaCode = b.getString("AreaCode");
				lat = b.getDouble("Lat");
				longi = b.getDouble("Longi");
			}
			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {

				HeaderBackcolorOnDisaster = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnDisaster"));
				HeaderBackcolorOnEmergency = cur.getString(cur
						.getColumnIndex("HeaderBackcolorOnEmergency"));
				if (cur.getString(cur.getColumnIndex("TimeInrvalKahi")) != null) {
					TimeInrvalKahi = Integer.parseInt(cur.getString(cur
							.getColumnIndex("TimeInrvalKahi")));

				}
				// Bug 179 20121019
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				//
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

				SharedPreferences sh = getSharedPreferences("app", MODE_PRIVATE);
				int mode = sh.getInt("Mode", 0);
				LinearLayout lHeader = (LinearLayout) findViewById(R.id.bgHeader);
				if (mode == 2) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnEmergency));
				} else if (mode == 1) {
					lHeader.setBackgroundColor(Color
							.parseColor(HeaderBackcolorOnDisaster));
				}

			}
			// bug 393 20121128
			// Intent outintent = getIntent();
			// Bundle b = outintent.getExtras();
			// String e = b.getString("E");
			// FireStationCode = b.getString("FireStationCode");
			// CaseCode = b.getString("CaseCode");
			// SHCACode = b.getString("SHCACode");
			// if (e.equals("E012")) {
			// rdoSearch = b.getString("rdoSearch");
			// hopitalname = b.getString("hopitalname");
			// SHCASearch = b.getString("SHCASearch");
			// AreaCode = b.getString("AreaCode");
			// lat = b.getDouble("Lat");
			// longi = b.getDouble("Longi");
			// }
			// Bug 166
			// Criteria cri = new Criteria();
			// locationManager = (LocationManager)
			// getSystemService(LOCATION_SERVICE);
			// tower = locationManager.getBestProvider(cri, false);
			// locationA = locationManager.getLastKnownLocation(tower);
			// if (locationA != null) {
			// // lat = (double) (locationA.getLatitude() * 1E6);
			// // longi = (double) (locationA.getLongitude() * 1E6);
			// lat = locationA.getLatitude();
			// longi = locationA.getLongitude();
			// }
			CL_TRCase cl = new CL_TRCase(E013B.this);
			cur2 = cl.getItemTRcase(FireStationCode, CaseCode);
			if (cur2 != null && cur2.getCount() > 0) {
				String lat2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat2 != null && longi2 != null) {
					locationA = new Location("A");
					locationA.setLatitude(Double.parseDouble(lat2));
					locationA.setLongitude(Double.parseDouble(longi2));
					lat = locationA.getLatitude();
					longi = locationA.getLongitude();
				}
			}
			// end
			// Test data
			// lat = 10.803120;
			// longi = 106.738280;

			TextView txtOption = (TextView) findViewById(R.id.txtOptions13);
			txtOption.setOnTouchListener(this);
			TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
			txtHeader.setOnTouchListener(this);
			txtHeader.setText("病院検索結果(科目)");
			// Testmenu
			// txtHeader.setOnClickListener(new OnClickListener() {
			//
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// doMenu();
			// }
			// });
			numberitem = limit;
			btnNextRecord = (TextView) findViewById(R.id.btnNextRecord);
			btnNextRecord.setOnTouchListener(this);
			btnNextRecord.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					numberitem += limit;
					layoutItem.removeAllViews();
					loadControl();
					if (listItem.size() <= numberitem) {
						btnNextRecord.setVisibility(View.INVISIBLE);
					}
				}
			});
			// Bug 187,188
			if (rdoSearch.equals("1") || rdoSearch.equals("4")) {
				txtOption.setText("現在地から直近順で検索");
			} else if (rdoSearch.equals("2")) {
				txtOption.setText("病院名を指定して検索");
			} else {
				txtOption.setText("地域を指定して検索");
			}

			Button btnBack = (Button) findViewById(R.id.btnE013Back1);
			btnBack.setOnTouchListener(this);
			btnBack.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showE015();
					E015Dialog dg = new E015Dialog();
					dg.ShowE015(E013B.this, FireStationCode, CaseCode, MICode,
							"E017");

					Intent callIntent = new Intent(Intent.ACTION_DIAL);
					callIntent.setData(Uri.parse("tel:"));
					startActivity(callIntent);

				}
			});
			Button btnBack2 = (Button) findViewById(R.id.btnE013Back2);
			btnBack2.setOnTouchListener(this);
			btnBack2.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showE015();
					E015Dialog dg = new E015Dialog();
					dg.ShowE015(E013B.this, FireStationCode, CaseCode, MICode,
							"E017");

					Intent callIntent = new Intent(Intent.ACTION_DIAL);
					callIntent.setData(Uri.parse("tel:"));
					startActivity(callIntent);

				}
			});

			// Get DayInfo
			listday = new HashMap<String, String>();
			Cl_Code code = new Cl_Code(this);
			cor = code.getMS_Code("00001");
			if (cor != null && cor.getCount() > 0) {
				do {
					String codeid = cor.getString(cor.getColumnIndex("CodeID"));
					String codename = cor.getString(cor
							.getColumnIndex("CodeName"));
					listday.put(codeid, codename);
				} while (cor.moveToNext());
			}
			if (listItem.size() > 0) {
			} else
				showProccess();

			startThread();
			// createMenu();
			layoutmain = (ScrollView) findViewById(R.id.laymainE13A);
			layoutmain.setOnTouchListener(this);

			// ////////////
			listKahi = new HashMap<String, String>();
			listAllReq = new HashMap<String, String>();
			listAllReqTarget = new HashMap<String, String>();
			handler2 = new Handler();
			listStatus = new ArrayList<DTO_ObjectControl>();
			startThreadStatus();
			loadAllReq();

			Button btnShow34 = (Button) findViewById(R.id.btnShowE34);
			btnShow34.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (listAllReqTarget.size() == 0) {
						E022.showErrorDialog(E013B.this,
								ContantMessages.NotSelectMICode,
								ContantMessages.NotSelectMICode, false);
					} else {
						showDialogSendFile();
					}
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
			if (cor != null)
				cor.close();
			if (cur2 != null)
				cur2.close();
		}
	}

	private void loadKahi() {
		Cursor cur = null;
		try {
			CL_AllReqMedicalInstitution cl = new CL_AllReqMedicalInstitution(
					E013B.this);
			cur = cl.getAllReqMedicalInstitution_Kahi(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {
					String MICode = cur.getString(cur.getColumnIndex("MICode"));
					if (listKahi.containsKey(MICode) == false) {
						listKahi.put(MICode, MICode);
					}
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	private void loadAllReq() {
		Cursor cur = null;
		try {
			CL_AllReqMedicalInstitution cl = new CL_AllReqMedicalInstitution(
					E013B.this);
			cur = cl.getAllReqMedicalInstitution(FireStationCode, CaseCode);
			if (cur != null && cur.getCount() > 0) {
				do {
					String MICode = cur.getString(cur.getColumnIndex("MICode"));
					if (listAllReq.containsKey(MICode) == false) {
						listAllReq.put(MICode, MICode);
					}
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	// 20131115 Bug 45: refesh to last postion
	private void refeshScreen() {

		listItem = new ArrayList<DTO_MedicalInstitution>();
		Cursor cur = null;
		Cursor cur2 = null;
		try {

			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				// Bug 179 20121019
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ResultCount)) != null) {
					limit = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.ResultCount)));

				}
				//
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;

			}

			CL_TRCase cl = new CL_TRCase(E013B.this);
			cur2 = cl.getItemTRcase(FireStationCode, CaseCode);
			if (cur2 != null && cur2.getCount() > 0) {
				String lat2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Latitude_IS));
				String longi2 = cur2.getString(cur2
						.getColumnIndex(TR_CaseCount.Longitude_IS));
				if (lat2 != null && longi2 != null) {
					locationA = new Location("A");
					locationA.setLatitude(Double.parseDouble(lat2));
					locationA.setLongitude(Double.parseDouble(longi2));
					lat = locationA.getLatitude();
					longi = locationA.getLongitude();
				}
			}

			showProccess();
			startThread();

		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E013B.this);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();
		}
	}

	private void loadControl() {
		Cursor cur = null;
		Cursor cur2 = null;
		try {
			loadKahi();
			listStatus = new ArrayList<DTO_ObjectControl>();
			int Id = 100;
			if (listItem != null && listItem.size() > 0) {
				for (int i = 0; i < listItem.size(); i++) {
					if (i == numberitem) {
						break;
					}
					final DTO_MedicalInstitution dt = listItem.get(i);
					LinearLayout.LayoutParams paramlay = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);

					LinearLayout layrow = new LinearLayout(this);
					layrow.setOrientation(LinearLayout.VERTICAL);
					layrow.setLayoutParams(paramlay);
					layrow.setId(i);
					layrow.setOnTouchListener(this);
					Cl_ContactResultRecord trc = new Cl_ContactResultRecord(
							this);
					if (trc.checkExistTR_ContactResultRecord(FireStationCode,
							CaseCode, dt.getMICode())) {
						layrow.setBackgroundResource(R.drawable.bgselected);
					} else
						layrow.setBackgroundResource(R.drawable.bggrid13);
					// con1
					LinearLayout.LayoutParams paramlay2 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramlay2.setMargins(10, 20, 0, 0);
					LinearLayout layrow2 = new LinearLayout(this);
					layrow2.setOrientation(LinearLayout.HORIZONTAL);
					layrow2.setLayoutParams(paramlay2);

					// layrow2.setBackgroundColor(Color.parseColor("#E7E8EA"));
					// con2
					LinearLayout.LayoutParams paramtxt1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					TextView txtSTT = new TextView(this);
					txtSTT.setLayoutParams(paramtxt1);
					txtSTT.setTextColor(Color.WHITE);
					txtSTT.setTextSize(12);
					txtSTT.setGravity(Gravity.CENTER);
					if (dt.getMIType().equals("2")) {
						txtSTT.setText(dt.getMIType());
						txtSTT.setBackgroundResource(R.drawable.headerrowe13);
					} else if (dt.getMIType().equals("3")) {
						txtSTT.setText(dt.getMIType());
						txtSTT.setBackgroundResource(R.drawable.headerrowred);
					} else if (dt.getMIType().equals("5")) {
						txtSTT.setText("");
						txtSTT.setBackgroundResource(R.drawable.headerrow13c);
					} else {
						txtSTT.setText(dt.getMIType());
						txtSTT.setBackgroundResource(R.drawable.headerrowe13);
						txtSTT.setVisibility(View.INVISIBLE);
					}

					layrow2.addView(txtSTT);

					LinearLayout.LayoutParams paramtxt2 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					TextView txtName = new TextView(this);
					txtName.setLayoutParams(paramtxt2);
					txtName.setText(dt.getMIName_Kanji());
					txtName.setTextColor(Color.BLACK);
					txtName.setTextSize(18);
					// txtName.setSingleLine(true);//Bug 373 20121122
					layrow2.addView(txtName);

					Cl_FireStationStandard fe = new Cl_FireStationStandard(this);
					boolean rot = fe.checkTR_Rotation(dt.getMICode(),
							dt.getKamokuCode());
					if (dt.isISOuY() || rot) {
						txtName.setWidth(460);
						LinearLayout.LayoutParams paramlay22 = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paramlay22.setMargins(0, 10, 0, 0);
						LinearLayout layrow22 = new LinearLayout(this);
						layrow22.setOrientation(LinearLayout.HORIZONTAL);
						layrow22.setLayoutParams(paramlay22);
						layrow22.setGravity(Gravity.RIGHT);
						LinearLayout.LayoutParams paramtxtNote = new LinearLayout.LayoutParams(
								198, LinearLayout.LayoutParams.WRAP_CONTENT);
						TextView txtNote = new TextView(this);
						txtNote.setLayoutParams(paramtxtNote);
						txtNote.setText("  産婦人科輪番");
						txtNote.setTextColor(Color.WHITE);
						txtNote.setTextSize(12);
						txtNote.setGravity(Gravity.CENTER);
						txtNote.setBackgroundResource(R.drawable.txtnamehopital13);
						layrow22.addView(txtNote);
						layrow2.addView(layrow22);

					}

					layrow.addView(layrow2);

					// Add button show E026
					LinearLayout.LayoutParams paramrow1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout layrow1 = new LinearLayout(this);
					layrow1.setOrientation(LinearLayout.HORIZONTAL);
					layrow1.setLayoutParams(paramrow1);

					LinearLayout.LayoutParams paramrow11 = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout layrow11 = new LinearLayout(this);
					layrow11.setOrientation(LinearLayout.VERTICAL);
					layrow11.setLayoutParams(paramrow11);

					String kamokuname = dt.getKamokuName();
					String[] Arrname = kamokuname.split(",");
					String[] Ou = dt.getOujyu().split(",");
					String[] Op = dt.getOperation().split(",");
					if (Arrname.length > 0) {
						for (int row = 0; row < Arrname.length; row++) {

							// 20121017 Bug 164: show 3 row
							if (row > 2) {
								break;
							}
							//
							if (row > 0) {
								LinearLayout.LayoutParams param0 = new LinearLayout.LayoutParams(
										520, 1);
								param0.setMargins(10, 0, 0, 0);
								TextView txtLine = new TextView(this);
								txtLine.setLayoutParams(param0);
								txtLine.setBackgroundResource(R.drawable.linerow13);
								layrow11.addView(txtLine);

							}
							// con1
							LinearLayout.LayoutParams paramlay3 = new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.MATCH_PARENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							paramlay3.setMargins(10, 0, 0, 0);
							LinearLayout layrow3 = new LinearLayout(this);
							layrow3.setOrientation(LinearLayout.HORIZONTAL);
							layrow3.setLayoutParams(paramlay3);
							// layrow3.setBackgroundColor(Color.parseColor("#E7E8EA"));
							// con2
							LinearLayout.LayoutParams paramt1 = new LinearLayout.LayoutParams(
									172, LinearLayout.LayoutParams.WRAP_CONTENT);
							TextView txtConName = new TextView(this);
							txtConName.setLayoutParams(paramt1);
							txtConName.setText(Arrname[row]);
							txtConName.setTextSize(12);
							txtConName
									.setTextColor(Color.parseColor("#3A6892"));
							layrow3.addView(txtConName);

							LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(
									50, LinearLayout.LayoutParams.WRAP_CONTENT);
							param2.setMargins(18, 0, 0, 0);
							TextView txtToday = new TextView(this);
							txtToday.setLayoutParams(param2);
							// Bug 143 20121012
							// txtToday.setText("診療");
							txtToday.setText("応需");
							//
							txtToday.setTextSize(12);
							layrow3.addView(txtToday);

							LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
									115, LinearLayout.LayoutParams.WRAP_CONTENT);
							TextView txtTimeToday = new TextView(this);
							txtTimeToday.setLayoutParams(param3);
							txtTimeToday.setText(Ou[row]);
							txtTimeToday.setTextSize(12);

							if (Ou[row].equals("Ｘ") || Ou[row].equals("-")) {
								txtTimeToday.setTextColor(Color
										.parseColor("#777777"));
								txtToday.setTextColor(Color
										.parseColor("#777777"));
							} else {
								txtTimeToday.setTextColor(Color
										.parseColor("#3A6892"));
								txtToday.setTextColor(Color
										.parseColor("#3A6892"));
							}

							layrow3.addView(txtTimeToday);

							LinearLayout.LayoutParams param4 = new LinearLayout.LayoutParams(
									50, LinearLayout.LayoutParams.WRAP_CONTENT);
							param4.setMargins(10, 0, 0, 0);
							TextView txtTomorow = new TextView(this);
							txtTomorow.setLayoutParams(param4);
							txtTomorow.setText("手術");
							txtTomorow.setTextSize(12);
							layrow3.addView(txtTomorow);

							LinearLayout.LayoutParams param5 = new LinearLayout.LayoutParams(
									115, LinearLayout.LayoutParams.WRAP_CONTENT);
							TextView txtTimeTomorow = new TextView(this);
							txtTimeTomorow.setLayoutParams(param5);
							txtTimeTomorow.setText(Op[row]);
							txtTimeTomorow.setTextSize(12);
							if (Op[row].equals("Ｘ") || Op[row].equals("-")) {
								txtTimeTomorow.setTextColor(Color
										.parseColor("#777777"));
								txtTomorow.setTextColor(Color
										.parseColor("#777777"));
							} else {
								txtTimeTomorow.setTextColor(Color
										.parseColor("#3A6892"));
								txtTomorow.setTextColor(Color
										.parseColor("#3A6892"));
							}

							layrow3.addView(txtTimeTomorow);

							layrow11.addView(layrow3);
						}
					}
					layrow1.addView(layrow11);
					// add button
					LinearLayout.LayoutParams paramlay21 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramlay21.setMargins(0, 0, 10, 0);
					LinearLayout layrow21 = new LinearLayout(this);
					layrow21.setOrientation(LinearLayout.HORIZONTAL);
					layrow21.setLayoutParams(paramlay21);
					// layrow21.setGravity(Gravity.RIGHT);
					LinearLayout.LayoutParams paramchk = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramchk.setMargins(100, 0, 0, 0);
					CheckBox chk = new CheckBox(this);
					chk.setLayoutParams(paramchk);
					chk.setText("");
					if (listAllReq.containsKey(dt.getMICode())) {
						chk.setChecked(true);
						listAllReqTarget.put(dt.getMICode(), dt.getMICode());
					}
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								// CL_AllReqMedicalInstitution cl = new
								// CL_AllReqMedicalInstitution(
								// E013B.this);
								// ContentValues values = new ContentValues();
								// values.put("FireStationCode",
								// FireStationCode);
								// values.put("CaseNo", CaseCode);
								// values.put("MICode", dt.getMICode());
								// values.put("kin_kbn", "");
								// cl.createAllReqMedicalInstitution(values);
								if (listAllReqTarget.containsKey(dt.getMICode()) == false)
									listAllReqTarget.put(dt.getMICode(),
											dt.getMICode());
							} else {
								// CL_AllReqMedicalInstitution cl = new
								// CL_AllReqMedicalInstitution(
								// E013B.this);
								// cl.deleteAllReqMedicalInstitution(
								// FireStationCode, CaseCode,
								// dt.getMICode());
								if (listAllReqTarget.containsKey(dt.getMICode()) == true)
									listAllReqTarget.remove(dt.getMICode());
							}
						}
					});
					layrow21.addView(chk);

					layrow1.addView(layrow21);

					layrow.addView(layrow1);

					// Add line
					LinearLayout.LayoutParams param0 = new LinearLayout.LayoutParams(
							520, 1);
					param0.setMargins(10, 0, 0, 0);
					TextView txtLine = new TextView(this);
					txtLine.setLayoutParams(param0);
					txtLine.setBackgroundResource(R.drawable.linerow13);
					txtLine.setOnTouchListener(this);
					layrow.addView(txtLine);
					// con1
					LinearLayout.LayoutParams paramlay4 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramlay4.setMargins(0, 6, 0, 10);
					LinearLayout layrow4 = new LinearLayout(this);
					layrow4.setOrientation(LinearLayout.HORIZONTAL);
					layrow4.setLayoutParams(paramlay4);
					// layrow4.setBackgroundColor(Color.parseColor("#E7E8EA"));
					// con2

					LinearLayout.LayoutParams paramstatus = new LinearLayout.LayoutParams(
							130, 50);
					TextView txtSatus = new TextView(this);
					txtSatus.setOnTouchListener(this);
					txtSatus.setLayoutParams(paramstatus);
					txtSatus.setBackgroundResource(R.drawable.btnstatus13);
					txtSatus.setText("受入可");
					txtSatus.setTextColor(Color.WHITE);
					txtSatus.setTextSize(12);
					txtSatus.setGravity(Gravity.CENTER);
					txtSatus.setId(Id);
					Id++;
					if (listKahi.containsKey(dt.getMICode()) == false) {
						txtSatus.setVisibility(View.INVISIBLE);
					}

					DTO_ObjectControl dtControl = new DTO_ObjectControl();
					dtControl.MICode = dt.getMICode();
					dtControl.Id = txtSatus.getId();
					listStatus.add(dtControl);
					layrow4.addView(txtSatus);

					LinearLayout.LayoutParams parambtnE026 = new LinearLayout.LayoutParams(
							130, 50);
					parambtnE026.setMargins(105, 0, 0, 0);
					TextView btnShowE026 = new TextView(this);
					btnShowE026.setOnTouchListener(this);
					btnShowE026.setLayoutParams(parambtnE026);
					btnShowE026.setBackgroundResource(R.drawable.btnshowe026);
					btnShowE026.setText("受入状況");
					btnShowE026.setTextColor(Color.WHITE);
					btnShowE026.setTextSize(12);
					btnShowE026.setGravity(Gravity.CENTER);
					btnShowE026.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							String MICode_Select = dt.getMICode();
							E026 e26 = new E026();
							e26.ShowE026(E013B.this, MICode_Select, CaseCode);
						}
					});
					layrow4.addView(btnShowE026);

					LinearLayout.LayoutParams paramI1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramI1.setMargins(20, 4, 0, 0);
					ImageView img1 = new ImageView(this);
					img1.setLayoutParams(paramI1);
					img1.setBackgroundResource(R.drawable.oblue);
					// img1.setVisibility(View.INVISIBLE);
					layrow4.addView(img1);

					LinearLayout.LayoutParams paramI2 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramI2.setMargins(10, 4, 0, 0);
					ImageView img2 = new ImageView(this);
					img2.setLayoutParams(paramI2);
					img2.setBackgroundResource(R.drawable.oblue);
					// img2.setVisibility(View.INVISIBLE);
					layrow4.addView(img2);
					LinearLayout.LayoutParams paramI3 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramI3.setMargins(10, 4, 0, 0);
					ImageView img3 = new ImageView(this);
					img3.setLayoutParams(paramI3);
					img3.setBackgroundResource(R.drawable.oblue);
					// img3.setVisibility(View.INVISIBLE);
					layrow4.addView(img3);
					LinearLayout.LayoutParams paramI4 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramI4.setMargins(10, 4, 0, 0);
					ImageView img4 = new ImageView(this);
					img4.setBackgroundResource(R.drawable.oblue);
					img4.setLayoutParams(paramI4);
					// img4.setVisibility(View.INVISIBLE);
					layrow4.addView(img4);

					// load img
					Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
							E013B.this);
					// 20121005 Bug 48,49 : dai modify start
					// cur = cl.getTR_ContactResultRecord(FireStationCode,
					// CaseCode, dt.getMICode());
					cur = cl.getJianInHopital(dt.getMICode());
					if (cur != null && cur.getCount() > 0) {
						int idx = 1;
						do {
							// add code
							String fires = cur.getString(cur
									.getColumnIndex("FireStationCode"));
							String caseno = cur.getString(cur
									.getColumnIndex("CaseNo"));
							Cl_MovementTime mov = new Cl_MovementTime(
									E013B.this);
							cur2 = mov.getTR_MovementTimeDoctorInAdmission(
									fires, caseno);
							boolean yell = false;
							boolean red = false;
							if (cur2 != null && cur2.getCount() > 0) {
								String Movement_DATETIME = cur2.getString(cur2
										.getColumnIndex("Update_DATETIME"));
								if (Movement_DATETIME != null
										&& !Movement_DATETIME.equals("")
										&& Movement_DATETIME.length() == 14) {
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter
											.parse(Movement_DATETIME);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double time = diff * 1.0 / (1000 * 60 * 60);
									if (time <= 0.5)
										yell = true;
								}
							} else if (!yell) {
								String InsertTIME = cur.getString(cur
										.getColumnIndex("Insert_DATETIME"));
								if (InsertTIME != null
										&& !InsertTIME.equals("")
										&& InsertTIME.length() == 14) {
									DateFormat formatter = new SimpleDateFormat(
											BaseCount.DateTimeFormat);
									Date date1 = (Date) formatter
											.parse(InsertTIME);
									Date date2 = (Date) formatter
											.parse(Utilities.getDateTimeNow());
									long diff = date2.getTime()
											- date1.getTime();
									double time = diff * 1.0 / (1000 * 60 * 60);
									if (time <= 0.5)
										red = true;
								}
							}
							// String Contact_DATETIME = cur.getString(cur
							// .getColumnIndex("Contact_DATETIME"));
							// if (Contact_DATETIME != null
							// && !Contact_DATETIME.equals("")) {
							// DateFormat formatter = new SimpleDateFormat(
							// BaseCount.DateTimeFormat);
							// Date date1 = (Date) formatter
							// .parse(Contact_DATETIME);
							// Date date2 = (Date) formatter.parse(Utilities
							// .getDateTimeNow());
							// long diff = date2.getTime() - date1.getTime();
							// double time = diff * 1.0 / (1000 * 60 * 60);
							// switch (idx) {
							// case 1:
							// img4.setVisibility(View.VISIBLE);
							// if (time <= 0.5) {
							// img4.setBackgroundResource(R.drawable.ored);
							//
							// } else if (time <= 1) {
							// img4.setBackgroundResource(R.drawable.oyellow);
							// } else {
							// img4.setBackgroundResource(R.drawable.oblue);
							// }
							// break;
							// case 2:
							// img3.setVisibility(View.VISIBLE);
							// if (time <= 0.5) {
							// img3.setBackgroundResource(R.drawable.ored);
							//
							// } else if (time <= 1) {
							// img3.setBackgroundResource(R.drawable.oyellow);
							// } else {
							// img3.setBackgroundResource(R.drawable.oblue);
							// }
							// break;
							// case 3:
							// img2.setVisibility(View.VISIBLE);
							// if (time <= 0.5) {
							// img2.setBackgroundResource(R.drawable.ored);
							//
							// } else if (time <= 1) {
							// img2.setBackgroundResource(R.drawable.oyellow);
							// } else {
							// img2.setBackgroundResource(R.drawable.oblue);
							// }
							// break;
							// case 4:
							// img1.setVisibility(View.VISIBLE);
							// if (time <= 0.5) {
							// img1.setBackgroundResource(R.drawable.ored);
							//
							// } else if (time <= 1) {
							// img1.setBackgroundResource(R.drawable.oyellow);
							// } else {
							// img1.setBackgroundResource(R.drawable.oblue);
							// }
							// break;
							// default:
							// break;
							// }
							switch (idx) {
							case 1:
								img4.setVisibility(View.VISIBLE);
								if (yell) {
									img4.setBackgroundResource(R.drawable.oyellow);

								} else if (red) {
									img4.setBackgroundResource(R.drawable.ored);
								} else {
									img4.setBackgroundResource(R.drawable.oblue);
								}
								break;
							case 2:
								img3.setVisibility(View.VISIBLE);
								if (yell) {
									img3.setBackgroundResource(R.drawable.oyellow);

								} else if (red) {
									img3.setBackgroundResource(R.drawable.ored);
								} else {
									img3.setBackgroundResource(R.drawable.oblue);
								}
								break;
							case 3:
								img2.setVisibility(View.VISIBLE);
								if (yell) {
									img2.setBackgroundResource(R.drawable.oyellow);

								} else if (red) {
									img2.setBackgroundResource(R.drawable.ored);
								} else {
									img2.setBackgroundResource(R.drawable.oblue);
								}
								break;
							case 4:
								img1.setVisibility(View.VISIBLE);
								if (yell) {
									img1.setBackgroundResource(R.drawable.oyellow);

								} else if (red) {
									img1.setBackgroundResource(R.drawable.ored);
								} else {
									img1.setBackgroundResource(R.drawable.oblue);
								}
								break;
							default:
								break;
							}
							idx++;

						} while (cur.moveToNext());
					}
					LinearLayout.LayoutParams paramlay5 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout layoutKM = new LinearLayout(this);
					layoutKM.setLayoutParams(paramlay5);

					LinearLayout.LayoutParams paramI5 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramI5.setMargins(80, 4, 0, 0);
					TextView txtKm = new TextView(this);
					txtKm.setLayoutParams(paramI5);
					txtKm.setTextSize(12);
					txtKm.setTextColor(Color.parseColor("#767676"));
					if (dt.getDistance() > 0) {
						txtKm.setText(String.valueOf(dt.getDistance()) + " km");
					} else {
						txtKm.setText("? km");
					}
					layoutKM.addView(txtKm);
					layrow4.addView(layoutKM);

					layrow.addView(layrow4);
					layrow.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							lastPostionX = layoutmain.getScrollX();
							lastPostionY = layoutmain.getScrollY();
							DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
							dt = listItem.get(v.getId());
							Intent myinten = new Intent(E013B.this, E014A.class);
							Bundle b = new Bundle();
							b.putSerializable("myobject", dt);
							b.putString("FireStationCode", FireStationCode);
							b.putString("CaseCode", CaseCode);
							b.putString("E", "E013B");
							b.putInt("MNETCount", MNETCount);
							b.putInt("ThirdCoordinateCount",
									ThirdCoordinateCount);
							b.putString("MNetURL", MNetURL);
							b.putString("KamokuName", KamokuName);
							myinten.putExtras(b);
							startActivityForResult(myinten, 0);
							// if (mMenu.isShowing()) {
							// mMenu.hide();
							// }
						}
					});
					layrow.setOnLongClickListener(new OnLongClickListener() {

						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							if (isOnline() && !check) {
								DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
								dt = listItem.get(v.getId());
								Intent myinten = new Intent(E013B.this,
										EMaps2.class);
								Bundle b = new Bundle();
								b.putString("Flat", String.valueOf(lat));
								b.putString("Flongi", String.valueOf(longi));
								b.putString("Tlat", dt.getLatitude());
								b.putString("Tlongi", dt.getLongitude());

								myinten.putExtras(b);
								startActivityForResult(myinten, 0);
							}
							return false;
						}
					});
					layoutItem.addView(layrow);

				}
			}
			if (listItem.size() <= numberitem) {
				btnNextRecord.setVisibility(View.INVISIBLE);
			} else {
				btnNextRecord.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
			// TODO: handle exception
			exitProccess();
			E022.showErrorDialog(this, e.getMessage(), "E013B", true);
		} finally {
			if (cur != null)
				cur.close();
			if (cur2 != null)
				cur2.close();
		}
	}

	// Start thread
	class myrunalbe implements Runnable {

		public void run() {
			// TODO Auto-generated method stub
			Cursor cur = null;
			try {
				String dayofweek = Utilities.getDayOfWeek();
				Cl_FireStationStandard cl1 = new Cl_FireStationStandard(
						E013B.this);
				if (cl1.checkHoliday()) {
					// ko co format 5 so
					// dayofweek = "00008";
					dayofweek = "8";
				}

				HashMap<String, String> listhopital = new HashMap<String, String>();
				// 20131115 Bug 46 Hopital exist all Kamoku
				Cl_FireStationStandard clcheck = new Cl_FireStationStandard(
						E013B.this);
				int soluong = clcheck.checkHopitalValid13B(FireStationCode,
						CaseCode);

				Cl_FireStationStandard cl = new Cl_FireStationStandard(
						E013B.this);
				if (soluong > 0)
					cur = cl.getHopitalInfo13B(FireStationCode, CaseCode,
							dayofweek, rdoSearch, hopitalname, SHCASearch,
							AreaCode);
				else
					cur = cl.getHopitalInfo13B_SearchALL(dayofweek, rdoSearch,
							hopitalname, SHCASearch, AreaCode);
				if (cur != null && cur.getCount() > 0) {
					// int idx = 0;
					// Bug 099 check box or condition

					do {
						// 20121006 : bug 044
						// try {
						// Thread.sleep(500);
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						try {
							String MICode = cur.getString(cur
									.getColumnIndex("MICode"));
							String MIName_Kanji = cur.getString(cur
									.getColumnIndex("MIName_Kanji"));
							String Latitude = cur.getString(cur
									.getColumnIndex("Latitude"));
							String Longitude = cur.getString(cur
									.getColumnIndex("Longitude"));
							String MIType = cur.getString(cur
									.getColumnIndex("MIType"));
							String MIClass = cur.getString(cur
									.getColumnIndex("MIClass"));
							String TelNo1 = cur.getString(cur
									.getColumnIndex("TelNo1"));
							String TelNo2 = cur.getString(cur
									.getColumnIndex("TelNo2"));
							String TransportationTelNo = cur.getString(cur
									.getColumnIndex("TransportationTelNo"));
							String Address_Kanji = cur.getString(cur
									.getColumnIndex("Address_Kanji"));
							String KamokuName = cur.getString(cur
									.getColumnIndex("KamokuName"));
							String KamokuCode = "'"
									+ cur.getString(cur
											.getColumnIndex("KamokuCode"))
									+ "'";
							String Oujyu = "";
							if (cur.getString(cur.getColumnIndex("Oujyu")) != null
									&& !cur.getString(
											cur.getColumnIndex("Oujyu"))
											.equals("")) {
								if (cur.getString(cur.getColumnIndex("Oujyu"))
										.equals("1")) {
									Oujyu += "○";
								} else {
									Oujyu += "Ｘ";
								}
							} else {
								Oujyu += "-";
							}

							String Operation = "";
							if (cur.getString(cur.getColumnIndex("Operation")) != null
									&& !cur.getString(
											cur.getColumnIndex("Operation"))
											.equals("")) {
								if (cur.getString(
										cur.getColumnIndex("Operation"))
										.equals("1")) {
									Operation += "○";
								} else {
									Operation += "Ｘ";
								}
							} else {
								Operation += "-";
							}
							int mSL = 1;
							while (cur.moveToNext()) {
								// idx++;
								if (cur.getString(cur.getColumnIndex("MICode"))
										.equals(MICode)) {
									mSL++;
									String s1 = "," + KamokuName + ",";
									String s2 = ","
											+ cur.getString(cur
													.getColumnIndex("KamokuName"))
											+ ",";
									if (!s1.contains(s2)) {
										KamokuName += ","
												+ cur.getString(cur
														.getColumnIndex("KamokuName"));
										KamokuCode += ",'"
												+ cur.getString(cur
														.getColumnIndex("KamokuCode"))
												+ "'";
										if (cur.getString(cur
												.getColumnIndex("Oujyu")) != null
												&& !cur.getString(
														cur.getColumnIndex("Oujyu"))
														.equals("")) {
											if (cur.getString(
													cur.getColumnIndex("Oujyu"))
													.equals("1")) {
												Oujyu += "," + "○";
											} else {
												Oujyu += "," + "Ｘ";
											}
										} else {
											Oujyu += "," + "-";
										}

										if (cur.getString(cur
												.getColumnIndex("Operation")) != null
												&& !cur.getString(
														cur.getColumnIndex("Operation"))
														.equals("")) {
											if (cur.getString(
													cur.getColumnIndex("Operation"))
													.equals("1")) {
												Operation += "," + "○";
											} else {
												Operation += "," + "Ｘ";
											}
										} else {
											Operation += "," + "-";
										}

									}

								} else {
									cur.moveToPrevious();
									// idx--;
									break;
								}
							}
							// idx++;
							// Bug 099 remove soluong
							// if (mSL < soluong) {
							// // if (idx == cur.getCount()) {
							// //
							// // Message mg = handler.obtainMessage();
							// // Bundle b = new Bundle();
							// // b.putSerializable("DTO", null);
							// // b.putString("Key", "1");
							// // mg.setData(b);
							// // handler.sendMessage(mg);
							// // }
							// continue;
							// }
							// 20131115 Bug 46 Hopital exist all Kamoku
							if (mSL < soluong) {
								continue;
							}
							listhopital.put(MICode, KamokuCode);
							DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
							dt.setMICode(MICode);
							dt.setMIName_Kanji(MIName_Kanji);
							dt.setLatitude(Latitude);
							dt.setLongitude(Longitude);
							dt.setAddress_Kanji(Address_Kanji);
							dt.setMIClass(MIClass);
							dt.setMIType(MIType);
							dt.setTelNo1(TelNo1);
							dt.setTelNo2(TelNo2);
							dt.setTransportationTelNo(TransportationTelNo);
							dt.setKamokuName(KamokuName);
							dt.setOujyu(Oujyu);
							dt.setOperation(Operation);
							dt.setKamokuCode(KamokuCode);
							dt.setISOuY(false);
							// Bug 044 20121012 Don't get from google
							// if (isOnline()) {
							// String dis = getKhoangCach(String.valueOf(lat),
							// String.valueOf(longi), Latitude,
							// Longitude);
							// double distance = Double.parseDouble(dis);
							// dt.setDistance(distance);
							// } else {
							if (locationA != null) {
								Location locationB = new Location("point B");
								locationB.setLatitude(Double
										.parseDouble(Latitude));
								locationB.setLongitude(Double
										.parseDouble(Longitude));
								double distance = (double) locationA
										.distanceTo(locationB);
								distance = Utilities
										.roundTwoDecimals(distance / 1000);
								dt.setDistance(distance);
							}

							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putSerializable("DTO", dt);
							// if (idx == cur.getCount()) {
							// b.putString("Key", "1");
							// } else {
							b.putString("Key", "0");
							// }
							mg.setData(b);
							handler.sendMessage(mg);
						} catch (Exception e) {
							// TODO: handle exception
						}

					} while (cur.moveToNext());

					// try {
					// Thread.sleep(500);
					// } catch (InterruptedException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }

				} else {

					// Message mg = handler.obtainMessage();
					// Bundle b = new Bundle();
					// b.putSerializable("DTO", null);
					// b.putString("Key", "1");
					// mg.setData(b);
					// handler.sendMessage(mg);

				}
				// ////////
				cl = new Cl_FireStationStandard(E013B.this);
				if (soluong > 0)
					cur = cl.getHopitalInfo13B2(FireStationCode, CaseCode,
							dayofweek, rdoSearch, hopitalname, SHCASearch,
							AreaCode);
				else
					cur = cl.getHopitalInfo13B2_SearchAll(dayofweek, rdoSearch,
							hopitalname, SHCASearch, AreaCode);
				if (cur != null && cur.getCount() > 0) {
					// int idx = 0;
					// Cl_FireStationStandard clcheck = new
					// Cl_FireStationStandard(
					// E013B.this);
					// int soluong =
					// clcheck.checkHopitalValid13B(FireStationCode,
					// CaseCode);
					do {
						// 20121006 : bug 044
						// try {
						// Thread.sleep(500);
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						try {
							String MICode = cur.getString(cur
									.getColumnIndex("MICode"));
							// If exit in tr_oujyu , don't get.
							if (listhopital.containsKey(MICode)) {
								continue;
							}
							//

							String MIName_Kanji = cur.getString(cur
									.getColumnIndex("MIName_Kanji"));
							String Latitude = cur.getString(cur
									.getColumnIndex("Latitude"));
							String Longitude = cur.getString(cur
									.getColumnIndex("Longitude"));
							String MIType = cur.getString(cur
									.getColumnIndex("MIType"));
							String MIClass = cur.getString(cur
									.getColumnIndex("MIClass"));
							String TelNo1 = cur.getString(cur
									.getColumnIndex("TelNo1"));
							String TelNo2 = cur.getString(cur
									.getColumnIndex("TelNo2"));
							String TransportationTelNo = cur.getString(cur
									.getColumnIndex("TransportationTelNo"));
							String Address_Kanji = cur.getString(cur
									.getColumnIndex("Address_Kanji"));
							String KamokuName = cur.getString(cur
									.getColumnIndex("KamokuName"));
							String KamokuCode = "'"
									+ cur.getString(cur
											.getColumnIndex("KamokuCode"))
									+ "'";

							String Oujyu = "";
							String Operation = "";
							Cl_FireStationStandard clf = new Cl_FireStationStandard(
									E013B.this);
							HashMap<String, String> list = new HashMap<String, String>();
							list = clf.getOujuyInfo(MICode, cur.getString(cur
									.getColumnIndex("KamokuCode")));
							if (list != null && list.containsKey("Oujyu"))
								Oujyu = list.get("Oujyu");
							else
								Oujyu = "-";
							if (list != null && list.containsKey("Operation"))
								Operation = list.get("Operation");
							else
								Operation = "-";
							int mSL = 1;
							while (cur.moveToNext()) {
								// idx++;
								if (cur.getString(cur.getColumnIndex("MICode"))
										.equals(MICode)) {
									mSL++;
									String s1 = "," + KamokuName + ",";
									String s2 = ","
											+ cur.getString(cur
													.getColumnIndex("KamokuName"))
											+ ",";
									if (!s1.contains(s2)) {
										KamokuName += ","
												+ cur.getString(cur
														.getColumnIndex("KamokuName"));
										KamokuCode += ",'"
												+ cur.getString(cur
														.getColumnIndex("KamokuCode"))
												+ "'";

										clf = new Cl_FireStationStandard(
												E013B.this);
										list = new HashMap<String, String>();
										list = clf
												.getOujuyInfo(
														MICode,
														cur.getString(cur
																.getColumnIndex("KamokuCode")));
										if (list != null
												&& list.containsKey("Oujyu"))
											Oujyu += "," + list.get("Oujyu");
										else
											Oujyu += "," + "-";
										if (list != null
												&& list.containsKey("Operation"))
											Operation += ","
													+ list.get("Operation");
										else
											Operation += "," + "-";

									}

								} else {
									cur.moveToPrevious();
									// idx--;
									break;
								}
							}
							// idx++;
							if (mSL < soluong) {
								continue;
							}
							DTO_MedicalInstitution dt = new DTO_MedicalInstitution();
							dt.setMICode(MICode);
							dt.setMIName_Kanji(MIName_Kanji);
							dt.setLatitude(Latitude);
							dt.setLongitude(Longitude);
							dt.setAddress_Kanji(Address_Kanji);
							dt.setMIClass(MIClass);
							dt.setMIType(MIType);
							dt.setTelNo1(TelNo1);
							dt.setTelNo2(TelNo2);
							dt.setTransportationTelNo(TransportationTelNo);
							dt.setKamokuName(KamokuName);
							dt.setOujyu(Oujyu);
							dt.setOperation(Operation);
							dt.setKamokuCode(KamokuCode);
							dt.setISOuY(true);

							// Bug 044
							// if (isOnline()) {
							// String dis = getKhoangCach(String.valueOf(lat),
							// String.valueOf(longi), Latitude,
							// Longitude);
							// double distance = Double.parseDouble(dis);
							// dt.setDistance(distance);
							// } else {

							if (locationA != null) {
								Location locationB = new Location("point B");
								locationB.setLatitude(Double
										.parseDouble(Latitude));
								locationB.setLongitude(Double
										.parseDouble(Longitude));
								double distance = (double) locationA
										.distanceTo(locationB);
								distance = Utilities
										.roundTwoDecimals(distance / 1000);
								dt.setDistance(distance);
							}

							Message mg = handler.obtainMessage();
							Bundle b = new Bundle();
							b.putSerializable("DTO", dt);
							b.putString("Key", "0");
							mg.setData(b);
							handler.sendMessage(mg);
						} catch (Exception e) {
							// TODO: handle exception
						}

					} while (cur.moveToNext());

				}

				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);

			} catch (Exception e) {
				// TODO: handle exception
				// E022.showErrorDialog(E013.this, e.getMessage(), "E013");
				Message mg = handler.obtainMessage();
				Bundle b = new Bundle();
				b.putSerializable("DTO", null);
				b.putString("Key", "1");
				mg.setData(b);
				handler.sendMessage(mg);
			} finally {
				cur.close();
			}

		}
	};

	void startThread() {
		myrunalbe able = new myrunalbe();
		Thread th = new Thread(able);
		th.start();

	}

	private void sortAdapter() {
		try {
			if (listItem != null && listItem.size() > 1) {
				for (int i = 0; i < listItem.size() - 1; i++) {
					DTO_MedicalInstitution dti = new DTO_MedicalInstitution();
					dti = listItem.get(i);
					for (int j = i + 1; j < listItem.size(); j++) {
						DTO_MedicalInstitution dtj = new DTO_MedicalInstitution();
						dtj = listItem.get(j);
						if (dtj.getDistance() > 0
								&& dtj.getDistance() < dti.getDistance()) {
							// DTO_MedicalInstitution dttmp =new
							// DTO_MedicalInstitution();
							// dttmp=dti;
							String code = dti.getMICode();
							String name = dti.getMIName_Kanji();
							double distance = dti.getDistance();
							String lat = dti.getLatitude();
							String longi = dti.getLongitude();
							String MIType = dti.getMIType();
							String MIClass = dti.getMIClass();
							String TelNo1 = dti.getTelNo1();
							String TelNo2 = dti.getTelNo2();
							String TransportationTelNo = dti
									.getTransportationTelNo();
							String Address_Kanji = dti.getAddress_Kanji();
							String KamokuName = dti.getKamokuName();
							String Oujyu = dti.getOujyu();
							String Operation = dti.getOperation();
							String KamokuCode = dti.getKamokuCode();
							boolean isouju = dti.isISOuY();

							dti.setMICode(dtj.getMICode());
							dti.setMIName_Kanji(dtj.getMIName_Kanji());
							dti.setDistance(dtj.getDistance());
							dti.setLatitude(dtj.getLatitude());
							dti.setLongitude(dtj.getLongitude());
							dti.setMIClass(dtj.getMIClass());
							dti.setMIType(dtj.getMIType());
							dti.setTelNo1(dtj.getTelNo1());
							dti.setTelNo2(dtj.getTelNo2());
							dti.setTransportationTelNo(dtj
									.getTransportationTelNo());
							dti.setAddress_Kanji(dtj.getAddress_Kanji());
							dti.setKamokuName(dtj.getKamokuName());
							dti.setOujyu(dtj.getOujyu());
							dti.setOperation(dtj.getOperation());
							dti.setKamokuCode(dtj.getKamokuCode());
							dti.setISOuY(dtj.isISOuY());

							dtj.setMICode(code);
							dtj.setMIName_Kanji(name);
							dtj.setDistance(distance);
							dtj.setLatitude(lat);
							dtj.setLongitude(longi);
							dtj.setAddress_Kanji(Address_Kanji);
							dtj.setMIClass(MIClass);
							dtj.setMIType(MIType);
							dtj.setTelNo1(TelNo1);
							dtj.setTelNo2(TelNo2);
							dtj.setTransportationTelNo(TransportationTelNo);
							dtj.setKamokuName(KamokuName);
							dtj.setOujyu(Oujyu);
							dtj.setOperation(Operation);
							dtj.setKamokuCode(KamokuCode);
							dtj.setISOuY(isouju);

						}
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == resultCode && resultCode != 0) {
			if (resultCode == 132) {
				Bundle b = data.getExtras();
				totalfilesize = b.getInt("TotalSize");
				if (dialogSend != null) {
					TextView txtFileSize = (TextView) dialogSend
							.findViewById(R.id.txtFileSize);
					txtFileSize.setText(formatSize(totalfilesize));
					TextView txtError1 = (TextView) dialogSend
							.findViewById(R.id.lblMessageErr1);
					TextView txtError2 = (TextView) dialogSend
							.findViewById(R.id.lblMessageErr2);
					if (totalfilesize > maxsize) {
						txtFileSize.setTextColor(Color.RED);
					} else {
						txtError1.setText("");
						txtError2.setText("");
					}
					TextView txtUnit = (TextView) dialogSend
							.findViewById(R.id.txtUnit);
					if (totalfilesize < 1024)
						txtUnit.setText("KB");
					else
						txtUnit.setText("MB");
				}
			}
		} else {
			layoutItem.removeAllViews();
			loadControl();
		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	Dialog dialogSend;
	int totalfilesize;
	int maxsize;
	int Outbound_No;
	ArrayList<DTO_MedicalInstitution> listHopital;

	public void showDialogSendFile() {
		try {
			listHopital = new ArrayList<DTO_MedicalInstitution>();
			// Total file size
			CL_FileInfo cl = new CL_FileInfo(E013B.this);
			totalfilesize = cl.getTotalSize(FireStationCode, CaseCode);

			// Limit size
			CL_Linespeeddecision line = new CL_Linespeeddecision(E013B.this);
			maxsize = line.getMaxFileNo(getNetWorkType());

			// Outbound_No
			CL_TRCase tr = new CL_TRCase(E013B.this);
			Outbound_No = tr.getOutbound_No(FireStationCode, CaseCode);

			dialogSend = new Dialog(E013B.this);
			dialogSend.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogSend.setContentView(R.layout.layoute34);
			dialogSend.setCanceledOnTouchOutside(false);

			TextView txtOutbound_No = (TextView) dialogSend
					.findViewById(R.id.txtOutbound_No);
			txtOutbound_No.setText(String.valueOf(Outbound_No));
			TextView txtFileSize = (TextView) dialogSend
					.findViewById(R.id.txtFileSize);
			txtFileSize.setText(formatSize(totalfilesize));
			TextView txtError1 = (TextView) dialogSend
					.findViewById(R.id.lblMessageErr1);
			TextView txtError2 = (TextView) dialogSend
					.findViewById(R.id.lblMessageErr2);
			if (totalfilesize > maxsize) {
				txtFileSize.setTextColor(Color.RED);
			} else {
				txtError1.setText("");
				txtError2.setText("");
			}
			TextView txtUnit = (TextView) dialogSend.findViewById(R.id.txtUnit);
			if (totalfilesize < 1024)
				txtUnit.setText("KB");
			else
				txtUnit.setText("MB");

			ListView lvMiName = (ListView) dialogSend
					.findViewById(R.id.lvMiName);
			final E034Adapter adapter = new E034Adapter(E013B.this,
					R.layout.layoutrowminame);
			lvMiName.setAdapter(adapter);
			for (int i = 0; i < listItem.size(); i++) {
				DTO_MedicalInstitution dt = listItem.get(i);
				if (listAllReqTarget.containsKey(dt.getMICode())) {
					adapter.add(dt);
					listHopital.add(dt);
				}
			}

			Button btnOk = (Button) dialogSend.findViewById(R.id.btnOK34);
			btnOk.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					CL_TRCase tr = new CL_TRCase(E013B.this);
					tr.updateKin_kbn(FireStationCode, CaseCode, "2");

					CL_AllReqMedicalInstitution cl = new CL_AllReqMedicalInstitution(
							E013B.this);
					cl.CopyAllReqMedicalInstitution(FireStationCode, CaseCode);

					tr = new CL_TRCase(E013B.this);
					String kin_kbn = tr.getKin_kbn(FireStationCode, CaseCode);
					for (int i = 0; i < adapter.getCount(); i++) {
						DTO_MedicalInstitution item = adapter.getItem(i);

						ContentValues values = new ContentValues();
						values.put("FireStationCode", FireStationCode);
						values.put("CaseNo", CaseCode);
						values.put("MICode", item.getMICode());
						values.put("EMSTransmission_Datetime",
								Utilities.getDateTimeNow());

						values.put("kin_kbn", kin_kbn);
						cl = new CL_AllReqMedicalInstitution(E013B.this);
						cl.createAllReqMedicalInstitution(values);

					}
					E034 e34 = new E034();
					e34.UpdateLoad(E013B.this, FireStationCode, CaseCode,
							totalfilesize, Outbound_No, listHopital);
					dialogSend.cancel();
				}
			});
			Button btnCancel = (Button) dialogSend
					.findViewById(R.id.btnCancel34);
			btnCancel.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialogSend.cancel();
				}
			});
			Button btnShow30 = (Button) dialogSend
					.findViewById(R.id.btnShowE30);
			btnShow30.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent myinten = new Intent(E013B.this, E030.class);
					Bundle b = new Bundle();
					b.putString("FireStationCode", FireStationCode);
					b.putString("CaseCode", CaseCode);
					b.putInt("MaxSize", maxsize);
					b.putInt("Key", 132);
					myinten.putExtras(b);
					startActivityForResult(myinten, 132);
				}
			});

			dialogSend.show();
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E013B.this);
		}
	}

	private String formatSize(int size) {
		if (size < 1024)
			return String.valueOf(size);
		else {
			double res = Utilities.roundTwoDecimals(size / 1024);
			return String.valueOf(res);
		}
	}

	private int getNetWorkType() {
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		int networkType = tm.getNetworkType();
		switch (networkType) {
		case TelephonyManager.NETWORK_TYPE_GPRS:
		case TelephonyManager.NETWORK_TYPE_EDGE:
		case TelephonyManager.NETWORK_TYPE_CDMA:
		case TelephonyManager.NETWORK_TYPE_1xRTT:
		case TelephonyManager.NETWORK_TYPE_IDEN:
			return 2;
		case TelephonyManager.NETWORK_TYPE_UMTS:
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
		case TelephonyManager.NETWORK_TYPE_EVDO_B:
		case TelephonyManager.NETWORK_TYPE_EHRPD:
			return 3;
		case TelephonyManager.NETWORK_TYPE_HSUPA:
		case TelephonyManager.NETWORK_TYPE_HSPA:
		case TelephonyManager.NETWORK_TYPE_HSDPA:
		case TelephonyManager.NETWORK_TYPE_HSPAP:
			return 3;
			// return "3.5G";
		case TelephonyManager.NETWORK_TYPE_LTE:
			return 4;
		default:
			return 0;
		}
		// return "";
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// locationManager.removeUpdates(this); //Bug 166 20121025
		th.setRunning(false);
	}

	private String getStatusSysDB() {
		String syndb = "0";
		try {
			CL_User cl = new CL_User(E013B.this);
			cl.getUserName();
		} catch (Exception e) {
			// TODO: handle exception
			return "1";
		}
		return syndb;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// locationManager.requestLocationUpdates(tower, 500, 1, this); //Bug
		// SharedPreferences sh = this.getSharedPreferences("app",
		// this.MODE_PRIVATE);
		String syndb = getStatusSysDB();// sh.getString("SysDB", "0");
		if (refesh && syndb.equals("0")) {
			// onCreate(null);
			refeshScreen();
		}
		refesh = true;// Q065 20121027
		// 166 20121025
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		E034.SetActivity(this);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isCheck = false;
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		// Bug 166 20121025
		// lat = location.getLatitude();
		// longi = location.getLongitude();

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
