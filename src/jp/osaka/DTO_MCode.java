﻿package jp.osaka;

public class DTO_MCode {
	
	private String CodeType;
	private String CodeId;
	private String CodeName;
	private String Datetime;
	private String Flag;
	public String getCodeType() {
		return CodeType;
	}

	public void setCodeType(String codeType) {
		CodeType = codeType;
	}

	public String getCodeId() {
		return CodeId;
	}

	public void setCodeId(String codeId) {
		CodeId = codeId;
	}

	public String getCodeName() {
		return CodeName;
	}

	public void setCodeName(String codeName) {
		CodeName = codeName;
	}

	public String getDatetime() {
		return Datetime;
	}

	public void setDatetime(String datetime) {
		Datetime = datetime;
	}

	public String getFlag() {
		return Flag;
	}

	public void setFlag(String flag) {
		Flag = flag;
	}

}
