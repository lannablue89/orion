package jp.osaka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_FileInfo {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_FileInfo(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public long createFileInfo(ContentValues values) {
		long result = -1;
		try {
			result = database.insert("TR_FileManageInfo", null, values);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public boolean deleteTR_FileInfo(String FireStationCode, String CaseNo,
			String MultimediaFileNo, String FileType) {
		boolean result = false;
		try {
			String where = "FireStationCode=? and CaseNo=? and MultimediaFileNo=? and FileType=?";
			String[] args = { FireStationCode, CaseNo, MultimediaFileNo,
					FileType };
			result = database.delete("TR_FileManageInfo", where, args) > 0;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long updateTR_FileInfo(String FireStationCode, String CaseNo,
			String MultimediaFileNo, String FileType, String comment) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FileComment", comment);

			String where = "FireStationCode=? and CaseNo=? and MultimediaFileNo=? and FileType=?";
			String[] args = { FireStationCode, CaseNo, MultimediaFileNo,
					FileType };

			result = database.update("TR_FileManageInfo", values, where, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long updateTR_FileInfoSort(String FireStationCode, String CaseNo,
			String MultimediaFileNo, String FileType, String FileSort) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("FileSort", FileSort);

			String where = "FireStationCode=? and CaseNo=? and MultimediaFileNo=? and FileType=?";
			String[] args = { FireStationCode, CaseNo, MultimediaFileNo,
					FileType };

			result = database.update("TR_FileManageInfo", values, where, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}
	public long updateTR_FileInfoFlag(String FireStationCode, String CaseNo) {
		long result = -1;
		try {
			ContentValues values = new ContentValues();
			values.put("SendFlag", "1");

			String where = "FireStationCode=? and CaseNo=? ";
			String[] args = { FireStationCode, CaseNo};
			result = database.update("TR_FileManageInfo", values, where, args);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public long updateTR_FileInfoSortFrom(String FireStationCode,
			String CaseNo, String FileSort) {
		long result = -1;
		try {
			String[] args = { FireStationCode, CaseNo, FileSort };
			String sql = "Update TR_FileManageInfo Set FileSort=FileSort-1 where FireStationCode=? and CaseNo=? and FileSort>?";
			database.rawQuery(sql, args);
			result = 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public Cursor getTR_FileInfo(String FireStationCode, String CaseNo,
			String FileType) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo, FileType };
			String sql = "Select * from TR_FileManageInfo where FireStationCode=? and CaseNo=? and FileType=? order by MultimediaFileNo DESC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public Cursor getTR_FileInfoCamera(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_FileManageInfo where FireStationCode=? and CaseNo=? and (FileType=2 or FileType=3 ) order by MultimediaFileNo ASC";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int getMaxFileNo(String FireStationCode, String CaseNo) {
		int fileno = 0;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select max(MultimediaFileNo) as FileNo from TR_FileManageInfo where FireStationCode=? and CaseNo=?";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor != null && mCursor.getCount() > 0) {
					String FileNo = mCursor.getString(mCursor
							.getColumnIndex("FileNo"));

					if (FileNo != null && !FileNo.equals("")) {
						fileno = Integer.parseInt(FileNo);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return fileno;
	}

	public Cursor getTR_FileInfo(String FireStationCode, String CaseNo) {
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select * from TR_FileManageInfo  where FireStationCode=? and CaseNo=? ORDER BY CASE WHEN FileSort Is NULL Then 1 Else 0 End,FileSort asc, FileRegistered_DateTime asc";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}

	public int getTotalSize(String FireStationCode, String CaseNo) {
		int total = 0;
		Cursor mCursor = null;
		try {
			String[] args = { FireStationCode, CaseNo };
			String sql = "Select sum(FileSize) as Total from TR_FileManageInfo  where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor.getColumnIndex("Total")) != null) {
					total = Integer.parseInt(mCursor.getString(mCursor
							.getColumnIndex("Total")));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return total;
	}

	public void close() {
		dbHelper.close();
	}
}
