package jp.osaka;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CL_TR_TriagePATMethod {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase database;

	public CL_TR_TriagePATMethod(Context context) {

		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}
	public Cursor getTR_TriagePATMethod(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_TriagePATMethod Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
	public Cursor getTR_TriagePATMethod_Result(String FireStationCode, String CaseCode) {
		Cursor mCursor = null;
		try {

			String[] args = { FireStationCode, CaseCode };
			String sql = "Select * from TR_TriagePATMethod_Result Where FireStationCode=? and CaseNo=? ";
			mCursor = database.rawQuery(sql, args);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			database.close();
		}
		return mCursor;
	}
}
