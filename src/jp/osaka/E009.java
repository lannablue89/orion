﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.TR_CaseCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

public class E009 extends Activity implements OnTouchListener {

	String flag = "1";
	String FireStationCode;
	String CaseCode;
	String SHCACode;
	String KamokuName = "";
	String strDiseaseFLG = "";
	String strExternalWoundFLG = "";
	// MyAdapter adapter;
	// ArrayList<DTO_FireStationStandard> list;
	HashMap<String, String> listmap;
	int Id = 1;
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	ThreadAutoShowButton bt = new ThreadAutoShowButton();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E009.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute009);

		// list = new ArrayList<DTO_FireStationStandard>();
		// get param
		Intent outintent = getIntent();
		Bundle b = outintent.getExtras();
		FireStationCode = b.getString("FireStationCode");
		CaseCode = b.getString("CaseCode");// "2012080800010101";//
		flag = b.getString(BaseCount.Flag);
		// Slidemenu
		utl.setMenu_open(false);
		initSileMenu();
		//

		// createMenu();// menu
		// getConfig();
		// Testmenu
		TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
		// txtHeader.setOnClickListener(new OnClickListener() {
		//
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// doMenu();
		// }
		// });
		ScrollView layoutmain = (ScrollView) findViewById(R.id.scrollView1);
		layoutmain.setOnTouchListener(this);

		Cl_EMSUnit em = new Cl_EMSUnit(E009.this);
		Cursor cur = em.getSHCACode();
		if (cur != null && cur.getCount() > 0) {
			SHCACode = cur.getString(cur.getColumnIndex("SHCACode"));
		}
		cur.close();
		// New Object
		Button btnTop = (Button) findViewById(R.id.btnE9Top);
		btnTop.setOnTouchListener(this);
		String DiseaseFLG = "1";
		String ExternalWoundFLG = "0";
		btnTop.setText("疾病 初期評価");
		txtHeader.setText("初期評価(疾病)");
		if (flag.equals("2")) {
			DiseaseFLG = "0";
			ExternalWoundFLG = "1";
			btnTop.setText("外傷 初期評価");
			txtHeader.setText("初期評価(外傷)");
		}
		strDiseaseFLG = DiseaseFLG;
		strExternalWoundFLG = ExternalWoundFLG;
		LinearLayout layout = (LinearLayout) findViewById(R.id.LayoutE9CheckBox);
		layout.setOnTouchListener(this);
		this.loadControl(DiseaseFLG, ExternalWoundFLG, null, null, null,
				layout, 0, 0);
		Button btnNext = (Button) findViewById(R.id.btnE9Next);
		btnNext.setOnTouchListener(this);
		btnNext.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				getKamokuName();
				if (listmap != null && listmap.size() > 0) {
					// Bug 166 20121031
					Location location = null;
					CL_TRCase clr = new CL_TRCase(E009.this);
					Cursor cur = clr.getItemTRcase(FireStationCode, CaseCode);
					if (cur != null && cur.getCount() > 0) {
						String lat = cur.getString(cur
								.getColumnIndex(TR_CaseCount.Latitude_IS));
						String longi = cur.getString(cur
								.getColumnIndex(TR_CaseCount.Longitude_IS));
						if (lat != null && longi != null) {
							location = new Location("A");
							location.setLatitude(Double.parseDouble(lat));
							location.setLongitude(Double.parseDouble(longi));
						}
					}
					if (cur != null)
						cur.close();

					if (flag.equals("2")) {
						Cl_FireStationStandard cl = new Cl_FireStationStandard(
								E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "0", "0");
						cl = new Cl_FireStationStandard(E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "1", "0");
					} else {
						Cl_FireStationStandard cl = new Cl_FireStationStandard(
								E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "0", "0");
						cl = new Cl_FireStationStandard(E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "0", "1");

					}
					if (location == null) {

						showDialogERR(ContantMessages.GPSDisiable);

					} else {
						Intent myinten = new Intent(E009.this, E013A.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString("SHCACode", SHCACode);
						b.putString("KamokuName", KamokuName);
						b.putString("E", "E009");
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
					}
				} else {

					if (flag.equals("2")) {
						Cl_FireStationStandard cl = new Cl_FireStationStandard(
								E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "1", "0");
					} else {
						Cl_FireStationStandard cl = new Cl_FireStationStandard(
								E009.this);
						cl.deleteTR_FireStationStandardDONTSearch(
								FireStationCode, CaseCode, "0", "1");
					}
					Intent myinten = new Intent(E009.this, E010.class);
					Bundle b = new Bundle();
					b.putString("FireStationCode", FireStationCode);
					b.putString("CaseCode", CaseCode);
					b.putString("SHCACode", SHCACode);
					b.putString("KamokuName", KamokuName);
					myinten.putExtras(b);
					startActivityForResult(myinten, 0);
				}

			}
		});

	}

	private void getKamokuName() {
		Cursor cur = null;
		try {
			KamokuName = "";
			Cl_FireStationStandard cl = new Cl_FireStationStandard(E009.this);
			cur = cl.get3TR_FireStationStandard(FireStationCode, CaseCode,
					strDiseaseFLG, strExternalWoundFLG, SHCACode);
			if (cur != null && cur.getCount() > 0) {
				do {

					String name = cur.getString(cur
							.getColumnIndex("StandardBranchName"));
					if (KamokuName.equals(""))
						KamokuName += name;
					else
						KamokuName += "," + name;
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), E009.this);
		} finally {
			if (cur != null)
				cur.close();
		}
	}

	public void showDialogERR(String code) {
		final Dialog dialog = new Dialog(this);
		String error = "";
		CL_Message m = new CL_Message(this);
		String meg = m.getMessage(code);
		if (meg != null && !meg.equals(""))
			error = meg;
		// dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layouterror22);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myinten = new Intent(E009.this, E013A.class);
				Bundle b = new Bundle();
				b.putString("FireStationCode", FireStationCode);
				b.putString("CaseCode", CaseCode);
				b.putString("SHCACode", SHCACode);
				b.putString("E", "E009");
				b.putString("KamokuName", KamokuName);
				myinten.putExtras(b);
				startActivityForResult(myinten, 0);
				dialog.cancel();

			}
		});

		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		if (error != null && !error.equals(""))
			lbmg.setText(error);
		else
			lbmg.setText("Unknow...");
		TextView lbcode = (TextView) dialog.findViewById(R.id.lblErrorcode);
		if (code != null && !code.equals("")) {
			lbcode.setText(code);
		}
		dialog.show();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);
		bt.setRunning(false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		bt.showButtonAuto(this, FireStationCode, CaseCode, false);
		E034.SetActivity(this);
	}

	private void loadControl(final String DiseaseFLG,
			final String ExternalWoundFLG, String ParentStandardClass,
			String ParentStandardCode, String ParentStandardBranchCode,
			final LinearLayout layout, final int left, final int right) {
		Cursor cor2 = null;
		Cursor cor = null;

		Cl_FireStationStandard cl = new Cl_FireStationStandard(E009.this);

		if (ParentStandardClass == null)
			cor = cl.getMS_FireStationStandard(DiseaseFLG, ExternalWoundFLG,
					SHCACode);
		else
			cor = cl.getMS_FireStationStandardChild(DiseaseFLG,
					ExternalWoundFLG, SHCACode, ParentStandardClass,
					ParentStandardCode, ParentStandardBranchCode);
		try {
			if (cor != null && cor.getCount() > 0) {
				// get Old data set status checkbox
				listmap = new HashMap<String, String>();
				Cl_FireStationStandard fire = new Cl_FireStationStandard(this);
				Cursor corold = fire.getTR_FireStationStandard(FireStationCode,
						CaseCode, DiseaseFLG, ExternalWoundFLG, SHCACode);
				if (corold != null && corold.getCount() > 0) {

					do {

						String StandardClass = corold.getString(corold
								.getColumnIndex("StandardClass"));
						String StandardCode = corold.getString(corold
								.getColumnIndex("StandardCode"));
						String StandardBranchCode = corold.getString(corold
								.getColumnIndex("StandardBranchCode"));

						String key = StandardClass + "," + StandardCode + ","
								+ StandardBranchCode;
						listmap.put(key, key);

					} while (corold.moveToNext());
				}
				corold.close();
				do {

					// Thread.sleep(100);
					String StandardClass = cor.getString(cor
							.getColumnIndex("StandardClass"));
					String StandardCode = cor.getString(cor
							.getColumnIndex("StandardCode"));
					String StandardBranchCode = cor.getString(cor
							.getColumnIndex("StandardBranchCode"));
					String StandardBranchName = cor.getString(cor
							.getColumnIndex("StandardBranchName"));
					final String key = StandardClass + "," + StandardCode + ","
							+ StandardBranchCode;
					final DTO_FireStationStandard dt = new DTO_FireStationStandard();
					dt.StandardClass = StandardClass;
					dt.StandardCode = StandardCode;
					dt.StandardBranchCode = StandardBranchCode;
					dt.StandardBranchName = StandardBranchName;
					dt.Checked = "0";
					dt.PId = String.valueOf(Id);

					// list.add(dt);
					Cl_FireStationStandard cl2 = new Cl_FireStationStandard(
							E009.this);
					cor2 = cl2.getMS_FireStationStandardChild(DiseaseFLG,
							ExternalWoundFLG, SHCACode, StandardClass,
							StandardCode, StandardBranchCode);
					if (cor2 != null && cor2.getCount() > 0) {
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, 78);
						paramChh.setMargins(left + 10, 5, 0, 10);
						final TextView btn = new TextView(E009.this);
						btn.setText("   " + StandardBranchName);
						btn.setId(Id);
						btn.setHeight(78);
						btn.setTextSize(24);
						btn.setBackgroundResource(R.drawable.btngreydown);
						btn.setLayoutParams(paramChh);
						btn.setVisibility(View.VISIBLE);
						btn.setGravity(Gravity.CENTER | Gravity.LEFT);
						btn.setTextColor(Color.WHITE);
						btn.setOnTouchListener(this);
						layout.addView(btn);

						LinearLayout.LayoutParams paraml = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						paraml.setMargins(left + 10, 5, 0, 10);
						final LinearLayout l = new LinearLayout(E009.this);
						l.setOrientation(LinearLayout.VERTICAL);
						l.setLayoutParams(paraml);
						layout.addView(l);
						btn.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								// TODO Auto-generated method stub
								if (l.getChildCount() == 0) {

									loadControl(DiseaseFLG, ExternalWoundFLG,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode, l, left + 5,
											right + 5);
									btn.setBackgroundResource(R.drawable.btngreyup);
								} else {
									btn.setBackgroundResource(R.drawable.btngreydown);
									l.removeAllViews();
								}
							}
						});

					} else {
						// Bug 86
						// LinearLayout.LayoutParams paramChh = new
						// LinearLayout.LayoutParams(
						// LinearLayout.LayoutParams.MATCH_PARENT,
						// LinearLayout.LayoutParams.WRAP_CONTENT);
						LinearLayout.LayoutParams paramChh = new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.WRAP_CONTENT);
						//
						final CheckBox chk = new CheckBox(E009.this);
						chk.setLayoutParams(paramChh);
						chk.setGravity(Gravity.CENTER | Gravity.LEFT);
						chk.setTextColor(Color.BLACK);
						chk.setId(Id);
						chk.setOnTouchListener(this);
						chk.setText(StandardBranchName);
						chk.setTextSize(18);
						chk.setButtonDrawable(this.getResources().getDrawable(
								R.drawable.checkbox));
						if (listmap != null && listmap.size() > 0
								&& listmap.containsKey(key)) {
							chk.setChecked(true);
						}
						// chk.setBackgroundResource(R.drawable.chke9);
						chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							public void onCheckedChanged(
									CompoundButton buttonView, boolean isChecked) {
								// TODO Auto-generated method stub
								Cl_FireStationStandard fs = new Cl_FireStationStandard(
										E009.this);
								if (isChecked) {

									fs.createTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "1";
									if (!listmap.containsKey(key)) {
										listmap.put(key, key);
									}

								} else {
									fs.deleteTR_FireStationStandard(
											FireStationCode, CaseCode,
											dt.StandardClass, dt.StandardCode,
											dt.StandardBranchCode);
									dt.Checked = "0";
									if (listmap.containsKey(key)) {
										listmap.remove(key);
									}

								}
							}
						});
						layout.addView(chk);
					}
					Id++;

				} while (cor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(E009.this, e.getMessage(), "E009", true);
		} finally {
			if (cor2 != null)
				cor2.close();
			if (cor != null)
				cor.close();
		}
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
