﻿package jp.osaka;

import java.util.HashMap;

import jp.osaka.ContantTable.BaseCount;
import jp.osaka.ContantTable.MS_SettingCount;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

public class E014A extends Activity implements OnClickListener, OnTouchListener {

	DTO_MedicalInstitution dt;
	String MITelNo = "";
	String FireStationCode;
	String CaseCode;
	LinearLayout layoutCondition;
	LinearLayout layouj;
	int MNETCount = 0;
	int ThirdCoordinateCount = 0;
	String MNetURL = "https://google.jp";
	String MICode = "99999";
	// Bug 339 20121114
	boolean is14B = false;
	String E = "";
	//
	// Bug 340 20121114
	boolean man = false;
	boolean feman = false;
	//
	String KamokuName01 = "", KamokuName02 = "", KamokuName03 = "",
			EMSUnitCode = "";
	ThreadAutoShowE016 th = new ThreadAutoShowE016();
	// menu
	ProgressBar progressBar;
	LinearLayout content, menu, menu2, layoutMain;
	LinearLayout.LayoutParams contentParams;
	ImageButton menu_button, pro_2;
	TranslateAnimation slide;
	int marginX, animateFromX, animateToX, marginX_temp = 0;
	SlideMenu utl = new SlideMenu();

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		// if (mMenu.isShowing()) {
		// mMenu.hide();
		// }
		boolean check = utl.eventOnTouch(event, animateFromX, animateToX,
				marginX, menu, content, contentParams);
		if (check && utl.isMenu_open())
			marginX = 0;
		else if (check && !utl.isMenu_open())
			marginX = -(menu.getLayoutParams().width);
		return check;
	}

	public void slideMenuIn(int animateFromX, int animateToX, int marginX) {
		marginX_temp = marginX;
		utl.slideMenuIn(animateFromX, animateToX, content, marginX,
				contentParams);
		marginX = marginX_temp;
	}

	private void initSileMenu() {
		try {
			pro_2 = (ImageButton) findViewById(R.id.pro_2);
			progressBar = (ProgressBar) findViewById(R.id.pro);
			menu = (LinearLayout) findViewById(R.id.menu);
			menu2 = (LinearLayout) findViewById(R.id.menu2);
			content = (LinearLayout) findViewById(R.id.layout_main);
			contentParams = (LinearLayout.LayoutParams) content
					.getLayoutParams();
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			contentParams.width = width;
			menu_button = (ImageButton) findViewById(R.id.menu_button);
			layoutMain = (LinearLayout) findViewById(R.id.layoutmain_new);
			layoutMain.setOnTouchListener(this);
			utl.initSileMenu(animateFromX, animateToX, content, marginX, menu,
					menu2, contentParams, menu_button, layoutMain, E014A.this,
					progressBar, pro_2, FireStationCode, CaseCode);
		} catch (Exception e) {
			// TODO: handle exception
			E022.saveErrorLog(e.getMessage(), this);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.layoute014a);

		// Cursor cur = null;
		try {

			// Get EMSUnitCode
			CL_User clus = new CL_User(this);
			EMSUnitCode = clus.getUserName();
			//
			Intent outintent = getIntent();
			Bundle b = outintent.getExtras();
			dt = new DTO_MedicalInstitution();
			dt = (DTO_MedicalInstitution) b.getSerializable("myobject");
			MICode = dt.getMICode();
			FireStationCode = b.getString("FireStationCode");
			CaseCode = b.getString("CaseCode");
			MNETCount = b.getInt("MNETCount");
			ThirdCoordinateCount = b.getInt("ThirdCoordinateCount");
			MNetURL = b.getString("MNetURL");
			String KamokuName = b.getString("KamokuName");
			getKamokuName(KamokuName);
			String e = b.getString("E");

			// Slide menu
			utl.setMenu_open(false);
			initSileMenu();

			layoutCondition = (LinearLayout) findViewById(R.id.layoutCondition);
			layoutCondition.setOnTouchListener(this);
			layouj = (LinearLayout) findViewById(R.id.layoutOuj);
			layouj.setOnTouchListener(this);
			if (dt != null) {
				TextView txtMiType = (TextView) findViewById(R.id.txt14STT);
				txtMiType.setOnTouchListener(this);
				TextView txtName = (TextView) findViewById(R.id.txt14Name);
				txtName.setOnTouchListener(this);
				TextView txtaddress = (TextView) findViewById(R.id.txtE14AAddress);
				txtaddress.setOnTouchListener(this);
				if (dt.getMIType() != null && dt.getMIType().equals("5")) {
					txtMiType.setText("");
					txtMiType.setBackgroundResource(R.drawable.headerrow13c);
				} else if (dt.getMIType() != null && dt.getMIType().equals("3")) {
					txtMiType.setText(dt.getMIType());
					txtMiType.setBackgroundResource(R.drawable.headerrowred);
				} else if (dt.getMIType() != null && dt.getMIType().equals("2")) {
					txtMiType.setText(dt.getMIType());
					txtMiType.setBackgroundResource(R.drawable.headerrowe13);
				} else {
					txtMiType.setVisibility(View.INVISIBLE);
					txtMiType.setText(dt.getMIType());
				}
				txtName.setText(dt.getMIName_Kanji());
				txtaddress.setText(dt.getAddress_Kanji());
				Button btnNext = (Button) findViewById(R.id.btnE14CNextE15);
				btnNext.setOnTouchListener(this);
				btnNext.setOnClickListener(this);
				Button btnCall1 = (Button) findViewById(R.id.btnCall1);
				btnCall1.setOnTouchListener(this);
				Button btnCall2 = (Button) findViewById(R.id.btnCall2);
				btnCall2.setOnTouchListener(this);
				Button btnCall3 = (Button) findViewById(R.id.btnCall3);
				btnCall3.setOnTouchListener(this);
				// Button btnCall4 = (Button) findViewById(R.id.btnCall4);

				if (dt.getTelNo1() != null && !dt.getTelNo1().equals("")) {
					btnCall1.setText("      昼：" + dt.getTelNo1());
					btnCall1.setOnClickListener(this);
				} else {
					// btnCall1.setEnabled(false);
				}
				if (dt.getTelNo2() != null && !dt.getTelNo2().equals("")) {
					btnCall2.setText("      夜：" + dt.getTelNo2());
					btnCall2.setOnClickListener(this);
				} else {
					// btnCall2.setEnabled(false);
				}
				if (dt.getTransportationTelNo() != null
						&& !dt.getTransportationTelNo().equals("")) {
					btnCall3.setText("   救急：" + dt.getTransportationTelNo());
					btnCall3.setOnClickListener(this);
				} else {
					// btnCall3.setEnabled(false);
				}
				// Bug 144
				// Cl_FireStationStandard cl = new Cl_FireStationStandard(this);
				// cur = cl.getMITElNo(FireStationCode, dt.getMICode());
				// if (cur != null && cur.getCount() > 0) {
				// MITelNo = cur.getString(cur.getColumnIndex("MITelNo"));
				// }
				// if (MITelNo != null && !MITelNo.equals("")) {
				// btnCall4.setText("   内科：" + MITelNo);
				// btnCall4.setOnClickListener(this);
				// } else {
				// btnCall4.setEnabled(false);
				// }
				loadPhoneMICode(dt.getMICode());
				// 20140206: Bug 029
				loadPhoneFirestationLMI(dt.getMICode());
				//
				if (e.equals("E013A")) {
					// Bug 340 20121113
					is14B = false;
					//
					this.loadCondition();
				} else {
					TextView txtnamecon = (TextView) findViewById(R.id.txtNameConditon);
					TextView txtLine = (TextView) findViewById(R.id.txtLine14A);
					LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
							1, 1);
					llp.setMargins(50, 0, 0, 0); // llp.setMargins(left, top,
													// right, bottom);
					txtnamecon.setLayoutParams(llp);
					txtLine.setLayoutParams(llp);
					// txtnamecon.setLayoutParams(paramI4);
					// txtLine.setLayoutParams(paramI4);
					txtnamecon.setVisibility(View.INVISIBLE);
					txtLine.setVisibility(View.INVISIBLE);
					// Bug 340 20121113
					is14B = true;
					//

				}
				this.loadOujyu();

				// createMenu();
				ScrollView layoutmain = (ScrollView) findViewById(R.id.laymainE14A);
				layoutmain.setOnTouchListener(this);
			}
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E014A", true);
		} finally {
			// cur.close();
		}
	}

	void getKamokuName(String KamokuName) {
		String[] Arrname = KamokuName.split(",");
		for (int row = 0; row < Arrname.length; row++) {
			if (row > 2) {
				break;
			}
			//
			if (row == 0)
				KamokuName01 = Arrname[row];
			else if (row == 1)
				KamokuName02 = Arrname[row];
			else if (row == 2)
				KamokuName03 = Arrname[row];
		}
	}

	private void loadPhoneMICode(String mICode) {
		Cursor cur = null;
		try {
			Cl_FireStationStandard cl = new Cl_FireStationStandard(this);
			cur = cl.getoujyutelno(mICode);
			if (cur != null && cur.getCount() > 0) {
				LinearLayout layout = (LinearLayout) findViewById(R.id.layoutButtonKamoku);
				layout.setOnTouchListener(this);
				do {
					String Mname = cur.getString(cur
							.getColumnIndex("KamokuName"));
					final String telno = cur.getString(cur
							.getColumnIndex("OujyuTelNo"));

					LinearLayout.LayoutParams parambtn = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					parambtn.setMargins(0, 14, 0, 0);
					Button btn = new Button(this);
					btn.setLayoutParams(parambtn);
					btn.setText("   " + Mname + "：" + telno);
					btn.setPadding(0, 0, 0, 0); // Bug 154
					btn.setBackgroundResource(R.drawable.btncall);
					btn.setTextColor(Color.WHITE);
					btn.setTextSize(24);
					btn.setSingleLine(true);
					btn.setOnTouchListener(this);
					btn.setGravity(Gravity.LEFT | Gravity.CENTER);
					btn.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							callPhone(telno);
						}
					});
					layout.addView(btn);

				} while (cur.moveToNext());
			}
		} catch (Exception ex) {
			E022.saveErrorLog(ex.getMessage(), E014A.this);
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	private void loadPhoneFirestationLMI(String mICode) {
		Cursor cur = null;
		try {
			CL_FireStationOriginalMI cl = new CL_FireStationOriginalMI(this);
			cur = cl.getMS_FireStationOriginalMIGID014(FireStationCode, mICode);
			if (cur != null && cur.getCount() > 0) {
				LinearLayout layout = (LinearLayout) findViewById(R.id.layoutButtonFirestationlmi);
				layout.setOnTouchListener(this);
				do {
					String MIDetailName = cur.getString(cur
							.getColumnIndex("MIDetailName"));
					final String telno = cur.getString(cur
							.getColumnIndex("TransportationTelNo"));
					if (MIDetailName == null)
						MIDetailName = "";
					LinearLayout.LayoutParams parambtn = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					parambtn.setMargins(0, 14, 0, 0);
					Button btn = new Button(this);
					btn.setLayoutParams(parambtn);
					if (MIDetailName.equals(""))
						btn.setText("   " + telno);
					else
						btn.setText("   " + MIDetailName + "　:　" + telno);
					btn.setPadding(0, 0, 0, 0); // Bug 154
					btn.setBackgroundResource(R.drawable.btncall);
					btn.setTextColor(Color.WHITE);
					btn.setTextSize(24);
					btn.setSingleLine(true);
					btn.setOnTouchListener(this);
					btn.setGravity(Gravity.LEFT | Gravity.CENTER);
					btn.setOnClickListener(new OnClickListener() {

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							callPhone(telno);
						}
					});
					layout.addView(btn);

				} while (cur.moveToNext());
			}
		} catch (Exception ex) {
			E022.saveErrorLog(ex.getMessage(), E014A.this);
		} finally {
			if (cur != null)
				cur.close();
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		th.setRunning(false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		th.showConfirmAuto(this, FireStationCode, CaseCode);
		E034.SetActivity(this);
	}

	private void loadOujyu() {
		Cursor cur = null;
		try {
			Cl_FireStationStandard cl = new Cl_FireStationStandard(this);
			cur = cl.getInfo14A(dt.getMICode());
			if (cur != null && cur.getCount() > 0) {
				TextView txtLastedit = (TextView) findViewById(R.id.txtLastEdit14A);
				TextView txtRemark = (TextView) findViewById(R.id.txtRemark14A);
				TextView txtMan = (TextView) findViewById(R.id.txtMan14);
				TextView txtFemale = (TextView) findViewById(R.id.txtFemale14);
				String lastedit = cur.getString(cur
						.getColumnIndex("Update_DATETIME"));
				if (lastedit != null && !lastedit.equals("")) {
					String date = lastedit.substring(0, 4) + "/"
							+ lastedit.substring(4, 6) + "/"
							+ lastedit.substring(6, 8) + " "
							+ lastedit.substring(8, 10) + ":"
							+ lastedit.substring(10, 12);
					txtLastedit.setText(date);
				}
				String remark = "";
				// Bug 340 20121114
				// boolean man = false;
				// boolean feman = false;
				// Bug 340 end
				LinearLayout.LayoutParams paramlay2 = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				// paramlay2.setMargins(10, 20, 0, 0);
				LinearLayout layrow2 = new LinearLayout(this);
				layrow2.setOrientation(LinearLayout.HORIZONTAL);
				layrow2.setLayoutParams(paramlay2);
				layrow2.setOnTouchListener(this);
				// layrow2.setBackgroundColor(Color.parseColor("#E7E8EA"));
				// con2
				LinearLayout.LayoutParams paramtxt1 = new LinearLayout.LayoutParams(
						200, LinearLayout.LayoutParams.WRAP_CONTENT);
				TextView txtTitel1 = new TextView(this);
				txtTitel1.setLayoutParams(paramtxt1);
				txtTitel1.setTextColor(Color.parseColor("#777777"));
				txtTitel1.setTextSize(18);
				txtTitel1.setText("応需科目");
				layrow2.addView(txtTitel1);

				LinearLayout.LayoutParams paramtxt2 = new LinearLayout.LayoutParams(
						150, LinearLayout.LayoutParams.WRAP_CONTENT);
				paramtxt2.setMargins(20, 0, 0, 0);
				TextView txtTitel2 = new TextView(this);
				txtTitel2.setLayoutParams(paramtxt2);
				txtTitel2.setTextColor(Color.parseColor("#777777"));
				txtTitel2.setTextSize(18);
				txtTitel2.setGravity(Gravity.CENTER);// Bug 136
				txtTitel2.setText("応需");
				layrow2.addView(txtTitel2);

				LinearLayout.LayoutParams paramtxt3 = new LinearLayout.LayoutParams(
						150, LinearLayout.LayoutParams.WRAP_CONTENT);
				TextView txtTitel3 = new TextView(this);
				txtTitel3.setLayoutParams(paramtxt3);
				txtTitel3.setTextColor(Color.parseColor("#777777"));
				txtTitel3.setTextSize(18);
				txtTitel3.setGravity(Gravity.CENTER);// Bug 136
				txtTitel3.setText("手術");
				layrow2.addView(txtTitel3);
				layouj.addView(layrow2);
				int numberrow = 0;
				do {
					numberrow++;
					if (cur.getString(cur.getColumnIndex("Remarks")) != null) {
						// Bug 485 20130109
						// if (!remark.equals("")) {
						// remark += ",";
						// }
						// remark +=
						// cur.getString(cur.getColumnIndex("Remarks"));
						if (remark.equals("")) {
							remark += cur.getString(cur
									.getColumnIndex("Remarks"));
						}
						// End
					}
					if (cur.getString(cur.getColumnIndex("Bed_Male")) != null
							&& cur.getString(cur.getColumnIndex("Bed_Male"))
									.equals("1")) {
						man = true;
					}
					if (cur.getString(cur.getColumnIndex("Bed_Female")) != null
							&& cur.getString(cur.getColumnIndex("Bed_Female"))
									.equals("1")) {
						feman = true;
					}
					// 20121017 Bug 164: show 3 row
					if (numberrow > 3) {
						continue;
					}
					//
					// con1
					LinearLayout.LayoutParams paramlay3 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					// paramlay3.setMargins(10, 0, 0, 0);
					LinearLayout layrow3 = new LinearLayout(this);
					layrow3.setOrientation(LinearLayout.HORIZONTAL);
					layrow3.setLayoutParams(paramlay3);
					layrow3.setOnTouchListener(this);
					// layrow3.setBackgroundColor(Color.parseColor("#E7E8EA"));
					// con2

					LinearLayout.LayoutParams paramt1 = new LinearLayout.LayoutParams(
							200, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramt1.setMargins(20, 0, 0, 0);
					TextView KamokuName = new TextView(this);
					KamokuName.setLayoutParams(paramt1);
					KamokuName.setText(cur.getString(cur
							.getColumnIndex("KamokuName")));
					KamokuName.setTextSize(14);
					KamokuName.setTextColor(Color.parseColor("#777777"));

					layrow3.addView(KamokuName);

					LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(
							150, LinearLayout.LayoutParams.WRAP_CONTENT);
					// param2.setMargins(8, 0, 0, 0);
					TextView txt1 = new TextView(this);
					txt1.setLayoutParams(param2);
					if (cur.getString(cur.getColumnIndex("Oujyu")) != null) {

						if (cur.getString(cur.getColumnIndex("Oujyu")).equals(
								"1")) {
							// Bug 145 don't check doctor
							// if (cur.getString(cur.getColumnIndex("Doctor"))
							// != null
							// && !cur.getString(
							// cur.getColumnIndex("Doctor"))
							// .equals("")) {
							// txt1.setText("◎");
							// } else {
							txt1.setText("○");
							// }
						} else {
							txt1.setText("Ｘ");
						}
					} else {
						txt1.setText("ー");
					}
					txt1.setTextSize(14);
					txt1.setGravity(Gravity.CENTER);// Bug 136
					txt1.setTextColor(Color.parseColor("#777777"));
					layrow3.addView(txt1);

					LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
							150, LinearLayout.LayoutParams.WRAP_CONTENT);
					// param2.setMargins(8, 0, 0, 0);
					TextView txt2 = new TextView(this);
					txt2.setLayoutParams(param3);
					if (cur.getString(cur.getColumnIndex("Operation")) != null) {

						if (cur.getString(cur.getColumnIndex("Operation"))
								.equals("1")) {
							// Bug 145 don't check doctor
							// if (cur.getString(cur.getColumnIndex("Doctor"))
							// != null
							// && !cur.getString(
							// cur.getColumnIndex("Doctor"))
							// .equals("")) {
							// txt2.setText("◎");
							// } else {
							txt2.setText("○");
							// }
						} else {
							txt2.setText("Ｘ");
						}
					} else {
						txt2.setText("ー");
					}
					txt2.setTextSize(14);
					txt2.setGravity(Gravity.CENTER);// Bug 136
					txt2.setTextColor(Color.parseColor("#777777"));
					layrow3.addView(txt2);

					layouj.addView(layrow3);

				} while (cur.moveToNext());

				txtRemark.setText(remark);
				if (man) {
					txtMan.setText("あり");
				} else {
					txtMan.setText("なし");
				}
				if (feman) {
					txtFemale.setText("あり");
				} else {
					txtFemale.setText("なし");
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E014A", true);
		} finally {
			cur.close();
		}
	}

	private void loadCondition() {
		try {
			String Conditionname = dt.getConditionBranchName();
			String[] Arrname = Conditionname.split(",");
			String[] Arrtoday = dt.getTimeToday().split(",");
			String[] Arrtomorow = dt.getTimeTomorow().split(",");
			// String[] ArrConcode = dt.getKamokuCode().split(";");
			String[] ArrConcode = dt.getConditionCode().split(";");
			// sort
			for (int n = 0; n < Arrtoday.length - 1; n++) {
				String timetoday = Arrtoday[n].trim();
				if (timetoday.equals("-")) {
					for (int k = n + 1; k < Arrtoday.length; k++) {
						String timetoday2 = Arrtoday[k].trim();
						if (!timetoday2.equals("-")) {
							Arrtoday[n] = timetoday2;
							Arrtoday[k] = timetoday;
							String tmp = Arrname[n];
							Arrname[n] = Arrname[k];
							Arrname[k] = tmp;
							tmp = Arrtomorow[n];
							Arrtomorow[n] = Arrtomorow[k];
							Arrtomorow[k] = tmp;
						}
					}
				}
			}
			//
			if (Arrname.length > 0) {
				for (int row = 0; row < Arrname.length; row++) {
					// 20121017 Bug 164: show 3 row
					if (row > 2) {
						break;
					}
					//
					LinearLayout.LayoutParams paramt1 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					TextView txtConName = new TextView(this);
					txtConName.setLayoutParams(paramt1);
					txtConName.setText(Arrname[row]);
					txtConName.setTextSize(18);
					txtConName.setTextColor(Color.parseColor("#777777"));
					layoutCondition.addView(txtConName);
					// con1

					LinearLayout.LayoutParams paramlay3 = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					paramlay3.setMargins(15, 0, 0, 0);
					LinearLayout layrow3 = new LinearLayout(this);
					layrow3.setOrientation(LinearLayout.HORIZONTAL);
					layrow3.setLayoutParams(paramlay3);
					layrow3.setOnTouchListener(this);
					// layrow3.setBackgroundColor(Color.parseColor("#E7E8EA"));
					// con2

					LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(
							25, LinearLayout.LayoutParams.WRAP_CONTENT);
					// param2.setMargins(18, 0, 0, 0);
					TextView txtToday = new TextView(this);
					txtToday.setLayoutParams(param2);
					txtToday.setText(dt.getOtoday());
					txtToday.setTextSize(14);
					txtToday.setTextColor(Color.parseColor("#777777"));
					layrow3.addView(txtToday);

					LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
							155, LinearLayout.LayoutParams.WRAP_CONTENT);
					param3.setMargins(5, 0, 0, 0);
					TextView txtTimeToday = new TextView(this);
					txtTimeToday.setLayoutParams(param3);
					txtTimeToday.setTextSize(14);
					txtTimeToday.setTextColor(Color.parseColor("#777777"));
					if (Arrtoday[row].equals("-")) {
						// txtTimeToday.setText("x");
						txtTimeToday.setText("Ｘ");

					}

					String[] s = Arrtoday[row].split("-");
					if (s.length > 0) {
						if (s[0].trim().equals("")) {
							txtTimeToday.setText("Ｘ");

						} else {

							if (s.length > 1) {
								// && s[0].trim().equals("000000")
								if (s[0].trim().equals(s[1].trim())) {
									txtTimeToday.setText("○");
								} else {
									String hour1 = s[0].substring(0, 2) + ":"
											+ s[0].substring(2, 4);
									String hour2 = s[1].substring(0, 2) + ":"
											+ s[1].substring(2, 4);
									txtTimeToday.setText(hour1 + "-" + hour2);
								}
							}
						}
					}

					layrow3.addView(txtTimeToday);

					LinearLayout.LayoutParams param4 = new LinearLayout.LayoutParams(
							25, LinearLayout.LayoutParams.WRAP_CONTENT);
					param4.setMargins(15, 0, 0, 0);
					TextView txtTomorow = new TextView(this);
					txtTomorow.setLayoutParams(param4);
					txtTomorow.setText(dt.getOtomorow());
					txtTomorow.setTextSize(14);
					txtTomorow.setTextColor(Color.parseColor("#777777"));

					layrow3.addView(txtTomorow);
					LinearLayout.LayoutParams param5 = new LinearLayout.LayoutParams(
							155, LinearLayout.LayoutParams.WRAP_CONTENT);
					param5.setMargins(5, 0, 0, 0);
					TextView txtTimeTomorow = new TextView(this);
					txtTimeTomorow.setLayoutParams(param5);
					txtTimeTomorow.setTextSize(14);
					txtTimeTomorow.setTextColor(Color.parseColor("#777777"));
					if (Arrtomorow[row].equals("-")) {
						txtTimeTomorow.setText("Ｘ");

					}
					s = Arrtomorow[row].split("-");
					if (s.length > 0) {
						if (s[0].trim().equals("")) {
							txtTimeTomorow.setText("Ｘ");

						} else {
							if (s.length > 1) {
								// && s[0].trim().equals("000000")
								if (s[0].trim().equals(s[1].trim())) {
									txtTimeTomorow.setText("○");
								} else {
									String hour1 = s[0].substring(0, 2) + ":"
											+ s[0].substring(2, 4);
									String hour2 = s[1].substring(0, 2) + ":"
											+ s[1].substring(2, 4);
									txtTimeTomorow.setText(hour1 + "-" + hour2);
								}
							}
						}
					}

					layrow3.addView(txtTimeTomorow);

					LinearLayout.LayoutParams param22 = new LinearLayout.LayoutParams(
							28, LinearLayout.LayoutParams.WRAP_CONTENT);
					param22.setMargins(15, 0, 0, 0);
					TextView txtToday2 = new TextView(this);
					txtToday2.setLayoutParams(param22);
					txtToday2.setText("祝");
					txtToday2.setTextSize(14);
					txtToday2.setTextColor(Color.parseColor("#777777"));
					layrow3.addView(txtToday2);

					LinearLayout.LayoutParams param32 = new LinearLayout.LayoutParams(
							155, LinearLayout.LayoutParams.WRAP_CONTENT);
					param32.setMargins(5, 0, 0, 0);
					TextView txtTimeToday2 = new TextView(this);
					txtTimeToday2.setLayoutParams(param32);
					txtTimeToday2.setTextSize(14);
					txtTimeToday2.setTextColor(Color.parseColor("#777777"));

					if (ArrConcode.length > row) {
						String code = ArrConcode[row];

						String[] arr = code.split(",");
						if (arr.length == 3) {
							Cl_FireStationStandard fi = new Cl_FireStationStandard(
									this);
							String time = fi.getTimeOfHoliday(dt.getMICode(),
									arr[0], arr[1], arr[2]);
							if (time != null)
								txtTimeToday2.setText(time);
						}
					}
					layrow3.addView(txtTimeToday2);
					layoutCondition.addView(layrow3);

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnCall1:
			callPhone(dt.getTelNo1());
			break;
		case R.id.btnCall2:
			callPhone(dt.getTelNo2());
			break;
		case R.id.btnCall3:
			callPhone(dt.getTransportationTelNo());
			break;
		// case R.id.btnCall4:
		// callPhone(MITelNo);
		// break;
		case R.id.btnE14CNextE15:
			// Bug 339 and 340 20121114
			if (is14B) {
				E = "E014B";
			} else {
				E = "E014A";
			}
			// end
			showE015();
			break;
		default:
			break;
		}

	}

	private void callPhone(String phonenumber) {
		try {
			phonenumber = phonenumber.replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", "").trim();
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + phonenumber));
			startActivity(callIntent);
		} catch (Exception e) {
			// TODO: handle exception
			E022.showErrorDialog(this, e.getMessage(), "E014A", true);
		}
	}

	String codeIdSelect = "";
	int mycountId = 1;
	boolean chk15 = false;
	CheckBox chk;

	private void showE015() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute015);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		TextView textViewcontactcount = (TextView) dialog
				.findViewById(R.id.textViewcontactcount);
		DTO_TRCase dtcase = new DTO_TRCase();
		int ContactCount = 0;
		if (dtcase.getContactCount() == null) {
			Cl_ContactResultRecord cl_new = new Cl_ContactResultRecord(
					E014A.this);
			ContactCount = Integer.parseInt(cl_new.getContactCount(
					FireStationCode, CaseCode));
		} else {
			ContactCount = Integer.parseInt(dtcase.getContactCount());
		}
		textViewcontactcount.setText(ContactCount + 1 + "");
		Button btnCancel = (Button) dialog.findViewById(R.id.btnE7Cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnE7Ok);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String note = "";
				if (codeIdSelect.equals(BaseCount.Other)) {
					EditText txtother = (EditText) dialog.findViewById(99999);
					note = txtother.getText().toString();
				}
				Cl_ContactResultRecord cl1 = new Cl_ContactResultRecord(
						E014A.this);
				DTO_TRCase dtcase = new DTO_TRCase();
				int ContactCount = 0;
				if (dtcase.getContactCount() == null) {
					ContactCount = Integer.parseInt(cl1.getContactCount(
							FireStationCode, CaseCode));
				} else {
					ContactCount = Integer.parseInt(dtcase.getContactCount());
				}
				Cl_ContactResultRecord cl = new Cl_ContactResultRecord(
						E014A.this);
				// Bug 339,340 20121114
				// long res = cl.createTR_ContactResultRecord(FireStationCode,
				// CaseCode, ContactCount + 1, MICode, codeIdSelect, note);
				long res = cl.createTR_ContactResultRecordforE014(
						FireStationCode, CaseCode, ContactCount + 1, MICode,
						codeIdSelect, note, E, man, feman, EMSUnitCode,
						KamokuName01, KamokuName02, KamokuName03,
						dt.getMIName_Kanji(), chk15);
				// end
				if (res >= 0) {
					dtcase.setContactCount(ContactCount + 1 + "");
					if (codeIdSelect.equals("00001")) {
						E015_0001 up = new E015_0001();
						up.UpdateLoad(E014A.this, FireStationCode, CaseCode);
					}
					MICode = dt.getMICode();

					if (codeIdSelect.equals("00001")) {
						dialog.cancel();
						Intent myinten = new Intent(v.getContext(), E005.class);
						Bundle b = new Bundle();
						b.putString("FireStationCode", FireStationCode);
						b.putString("CaseCode", CaseCode);
						b.putString(BaseCount.IsOldData, "1");
						myinten.putExtras(b);
						startActivityForResult(myinten, 0);
						finish();
					} else {
						Cl_ContactResultRecord clc = new Cl_ContactResultRecord(
								E014A.this);
						int count = clc.countTR_ContactResultRecord(
								FireStationCode, CaseCode);
						// 20121006 B-016:Add condition =
						// 20121006: daitran Bug 045
						// if (count >= ThirdCoordinateCount) {
						// showE017();
						// } else
						if (count == MNETCount) {
							dialog.cancel();
							ShowE016();

						} else {
							dialog.cancel();
							finish();
						}
					}
				}
			}
		});
		// dialog.setTitle("不搬送理由");
		// dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
		// R.drawable.o);
		Cl_Code code = new Cl_Code(E014A.this);
		Cursor cor = code.getMS_Code("00002");
		if (cor != null && cor.getCount() > 0) {
			mycountId = 1;
			final RadioGroup group = (RadioGroup) dialog
					.findViewById(R.id.radioGroup1);
			LinearLayout layout = (LinearLayout) dialog
					.findViewById(R.id.layoutE73);
			final HashMap<String, String> listItem = new HashMap<String, String>();
			do {
				final String codeId = cor.getString(cor
						.getColumnIndex("CodeID"));
				String codeName = cor.getString(cor.getColumnIndex("CodeName"));
				listItem.put(codeId, codeName);
				LinearLayout.LayoutParams paramCheck = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				paramCheck.setMargins(0, 10, 0, 10);
				RadioButton rdi = new RadioButton(this);
				rdi.setLayoutParams(paramCheck);
				rdi.setText(codeName);
				rdi.setTextColor(Color.parseColor("#777777"));
				rdi.setButtonDrawable(R.drawable.radio);
				rdi.setTextSize(24);
				rdi.setId(mycountId);
				mycountId++;

				rdi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							if (!codeId.equals("00001")) {
								RadioButton radio = (RadioButton) dialog
										.findViewById(1);
								radio.setChecked(false);
								chk.setChecked(false);
								chk.setEnabled(false);
							} else {
								chk.setEnabled(true);
								group.clearCheck();
							}
							codeIdSelect = codeId;
							// 20121008 Bug 079
							if (listItem.containsKey(BaseCount.Other)) {
								EditText txtother = (EditText) dialog
										.findViewById(99999);
								txtother.setEnabled(false);
								if (codeIdSelect.equals("99999")) {

									txtother.setEnabled(true);
								} else {
									txtother.setText("");
								}
							}
							//
						}
					}
				});
				// listItem.put(String.valueOf(id), codeId);
				if (codeId.equals("00001")) {
					LinearLayout.LayoutParams paramrow = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					LinearLayout lay = new LinearLayout(this);
					lay.setLayoutParams(paramrow);
					lay.setOrientation(LinearLayout.HORIZONTAL);
					chk = new CheckBox(this);
					chk.setLayoutParams(paramrow);
					chk.setTextColor(Color.BLACK);
					chk.setText("三次コーディネート");
					chk.setTextSize(16);
					chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub
							chk15 = arg1;
						}
					});
					lay.addView(rdi);
					lay.addView(chk);
					group.addView(lay);
				} else {
					group.addView(rdi);
				}
				if (codeId.equals("00001")) {
					rdi.setChecked(true);
					codeIdSelect = codeId;
				}
				if (codeId.equals("99999")) {

					LinearLayout.LayoutParams paramtext = new LinearLayout.LayoutParams(
							520, LinearLayout.LayoutParams.WRAP_CONTENT);
					paramtext.setMargins(0, 5, 20, 0);
					EditText txtOther = new EditText(this);
					txtOther.setLayoutParams(paramtext);
					txtOther.setBackgroundResource(R.drawable.inpute7);
					txtOther.setTextColor(Color.BLACK);
					txtOther.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
							200) });
					txtOther.setSingleLine(true);//
					txtOther.setEnabled(false);//
					txtOther.setId(99999);
					layout.addView(txtOther);
				}
			} while (cor.moveToNext());
			dialog.show();
		}

	}

	private void getConfig() {
		Cursor cur = null;
		try {
			CL_MSSetting set = new CL_MSSetting(this);
			cur = set.fetchLastSeting();
			if (cur != null && cur.getCount() > 0) {
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNETCount)) != null) {
					MNETCount = Integer.parseInt(cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNETCount)));

				}
				if (cur.getString(cur
						.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)) != null) {
					ThirdCoordinateCount = Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(MS_SettingCount.ThirdCoordinateCount)));

				}
				if (cur.getString(cur.getColumnIndex(MS_SettingCount.MNET_URL)) != null) {
					MNetURL = cur.getString(cur
							.getColumnIndex(MS_SettingCount.MNET_URL));
				}
				CL_TRCase tr = new CL_TRCase(this);
				String url = tr.getTR_URL(FireStationCode, CaseCode);
				MNetURL = MNetURL + "?" + url;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			cur.close();
		}
	}

	private void ShowE016() {
		getConfig();
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layoute016);
		dialog.setCanceledOnTouchOutside(false);// Bug 146
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel16);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				finish();
			}
		});
		Button btnOK = (Button) dialog.findViewById(R.id.btnOk16);
		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!MNetURL.startsWith("http://")
						&& !MNetURL.startsWith("https://"))
					MNetURL = "http://" + MNetURL;
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(MNetURL));
				startActivity(browserIntent);
				dialog.cancel();
				CL_TRCase tr = new CL_TRCase(E014A.this);
				tr.updateKin_kbn(FireStationCode, CaseCode, "0");

			}
		});
		// 20121006: bug 027
		TextView lbmg = (TextView) dialog.findViewById(R.id.lblMessageErr);
		CL_Message me = new CL_Message(this);
		String mg = me.getMessage(ContantMessages.OpenBrowes);
		if (mg != null && !mg.equals("")) {
			lbmg.setText(mg);
		}
		dialog.show();
	}

	/**
	 * Snarf the menu key.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (utl.isMenu_open()) {
				slideMenuIn(0, -(menu.getLayoutParams().width),
						-(menu.getLayoutParams().width));
				utl.setMenu_open(false);
				return true; // always eat it!
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
